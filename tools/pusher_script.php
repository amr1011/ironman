<?php
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
		//include the pusher publisher library
		include 'Pusher.php';

		$pusher = new Pusher(
			'5455533625918c2b14bf', //APP KEY
			'39e72c67904560a6495d', //APP SECRET
			'23540' //APP ID
		);

		//get the message posted by our ajax call
		$message = $_REQUEST['x'].",".$_REQUEST['y'];

		//trim and filter it
		$message = trim(filter_var($message, FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES));

		//wrap it with the user's name when we display

		//trigger the 'new_message' event in our channel, 'presence-nettuts'
		$channel = $_REQUEST['c'];
		$pusher->trigger(
			$channel, //the channel
			'new_data', //the event
			$message.",".$_REQUEST['m'].",".$_REQUEST['uiclass'] //the data to send
		);

		//echo the success array for the ajax call
		echo json_encode(array(
			'message' => $message,
			'success' => true
		));
		exit();
?>