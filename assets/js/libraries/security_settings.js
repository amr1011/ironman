(function () {
    "use strict";
    var uiIPWhitelist,
        uiIPBlacklist
    ;

    CPlatform.prototype.security_settings = {

        'initialize' : function () {
            
            $("select").transformDD();

            admin.security_settings.fetch_ip_whitelist(iConstantSbuId);
            //admin.security_settings.fetch_ip_blacklist(iConstantSbuId);

            //prevent from alpha input
            $("input.input_numeric").off("keyup").on("keyup",function (event) {
                $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            });

            //time input mask
            $("input.input_time").off("keyup").on("keyup",function (event) {
                $(this).val($(this).val().replace(/^([01]?\d|2[0-3])(:[0-5]\d){1,2}$/,''));
            });

            uiIPWhitelist = $("div.security_settings").find('div.ip-whitelist');
            uiIPBlacklist = $("div.security_settings").find('div.ip-blacklist');

            //for the ip whitelist
            uiIPWhitelist.on('click','a.edit-values', function(){
                //console.log($(this));
                $(this).addClass('hidden');
                uiIPWhitelist.find("button").removeClass('hidden');
                uiIPWhitelist.find("button").removeClass('hidden');
                uiIPWhitelist.find("table.ip-whitelist-table-editable").removeClass('hidden');
                uiIPWhitelist.find("table.ip-whitelist-table-display").addClass('hidden');

                uiIPWhitelist.find("a.add-ip-address-to-list").addClass('hidden');
                uiIPWhitelist.find("table.ip-whitelist-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                uiIPWhitelist.find("button.add-value").addClass('hidden');
                uiIPWhitelist.find("button.cancel-add").addClass('hidden');
    
            });

            uiIPWhitelist.on('click','button.save-values', function(){
                var uiEditable  = uiIPWhitelist.find("table.ip-whitelist-table-editable tbody.sbu_settings_editable_container tr:not(.template)");
                var uiDisplaytable  = uiIPWhitelist.find("table.ip-whitelist-table-display tbody.sbu_settings_container");
                
                //this is for the validation of fields
                var iError = 0;
                $.each(uiEditable.find('input.required'), function(key ,value) {
                   if($(this).val() =='')
                   {
                      $(this).css({'border-color':'red'});
                      iError++;
                   }else{
                      $(this).css({'border-color':'#ddd'});    
                   }    
                });
                
                if(iError==0)
                { 
                        admin.security_settings.show_spinner($(this),true);
                        var arrRules = [];
                        $.each(uiEditable, function(key ,value) {
                            if(typeof($(this).attr('data-rule-id')!="undefined"))
                            {
                                var id = $(this).attr('data-rule-id');
                                var ip_address = $(this).find('input[type="text"]').val();
                                var name = $(this).find('td[data-label="rule-text"]').text();
                                arrRules.push( { 'id': id , 'ip_address' : ip_address, 'description' : name } );

                                uiDisplaytable.find('tr[data-rule-id="'+id+'"]').find('td[data-label="ip_address"]').text(ip_address);
                            }
                        });

                        //console.log(arrRules);     
                        var oParams = {
                                "params": {
                                    "data" : arrRules,
                                    "sbu_id" : iConstantSbuId
                                },
                                "user_id" : iConstantUserId
                            }
                        //console.log(oParams);
                        admin.security_settings.update_ip_whitelist_settings(oParams,$(this));
                 }
                
            });

            uiIPWhitelist.on('click','button.cancel-edit', function(){
                uiIPWhitelist.find("a.edit-values").removeClass('hidden');
                uiIPWhitelist.find("button").addClass('hidden');
                uiIPWhitelist.find("table.ip-whitelist-table-editable").addClass('hidden');
                uiIPWhitelist.find("table.ip-whitelist-table-display").removeClass('hidden');

                uiIPWhitelist.find("a.add-ip-address-to-list").removeClass('hidden');
                uiIPWhitelist.find("table.ip-whitelist-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                uiIPWhitelist.find("button.add-value").addClass('hidden');
                uiIPWhitelist.find("button.cancel-add").addClass('hidden');
            });

            uiIPWhitelist.on('click','a.add-ip-address-to-list', function(){
                //console.log($(this));
                $(this).addClass('hidden');
                uiIPWhitelist.find("table.ip-whitelist-table-display tbody.sbu_settings_container_add_row").removeClass('hidden');
                uiIPWhitelist.find("button.add-value").removeClass('hidden');
                uiIPWhitelist.find("button.cancel-add").removeClass('hidden');
    
            });

            uiIPWhitelist.on('click','button.add-value', function(){
                var uiAddField  = uiIPWhitelist.find("table.ip-whitelist-table-display tbody.sbu_settings_container_add_row");

                //this is for the validation of fields
                var iError = 0;
                $.each(uiAddField.find('input.required'), function(key ,value) {
                   if($(this).val() =='')
                   {
                      $(this).css({'border-color':'red'});
                      iError++;
                   }else{
                      $(this).css({'border-color':'#ddd'});    
                   }    
                });
                
                if(iError==0)
                { 
                        admin.security_settings.show_spinner($(this),true);
                        var sRuleText = uiAddField.find('input[name="description"]').val().capitalize();
                        var sRuleValue = uiAddField.find('input[name="ip_address"]').val();

                        var oParams = {
                                "params": {
                                    "data" : { 'description': sRuleText , 'ip_address' : sRuleValue ,"sbu_id" : iConstantSbuId },
                                    "sbu_id" : iConstantSbuId
                                },
                                "user_id" : iConstantUserId
                            }
                        //console.log(oParams);
                        admin.security_settings.add_ip_whitelist_settings(oParams,$(this));
                }
            });

            uiIPWhitelist.on('click','button.cancel-add', function(){
                uiIPWhitelist.find("a.add-ip-address-to-list").removeClass('hidden');
                uiIPWhitelist.find("table.ip-whitelist-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                uiIPWhitelist.find("button.add-value").addClass('hidden');
                uiIPWhitelist.find("button.cancel-add").addClass('hidden');
            });
            
            //for the ip blacklist
            uiIPBlacklist.on('click','a.edit-values', function(){
                //console.log($(this));
                $(this).addClass('hidden');
                uiIPBlacklist.find("button").removeClass('hidden');
                uiIPBlacklist.find("button").removeClass('hidden');
                uiIPBlacklist.find("table.ip-blacklist-table-editable").removeClass('hidden');
                uiIPBlacklist.find("table.ip-blacklist-table-display").addClass('hidden');

                uiIPBlacklist.find("a.add-ip-address-to-list").addClass('hidden');
                uiIPBlacklist.find("table.ip-blacklist-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                uiIPBlacklist.find("button.add-value").addClass('hidden');
                uiIPBlacklist.find("button.cancel-add").addClass('hidden');
    
            });

            uiIPBlacklist.on('click','button.save-values', function(){
                var uiEditable  = uiIPBlacklist.find("table.ip-blacklist-table-editable tbody.sbu_settings_editable_container tr:not(.template)");
                var uiDisplaytable  = uiIPBlacklist.find("table.ip-blacklist-table-display tbody.sbu_settings_container");
                
                //this is for the validation of fields
                var iError = 0;
                $.each(uiEditable.find('input.required'), function(key ,value) {
                   if($(this).val() =='')
                   {
                      $(this).css({'border-color':'red'});
                      iError++;
                   }else{
                      $(this).css({'border-color':'#ddd'});    
                   }    
                });
                
                if(iError==0)
                { 
                        admin.security_settings.show_spinner($(this),true);
                        var arrRules = [];
                        $.each(uiEditable, function(key ,value) {
                            if(typeof($(this).attr('data-rule-id')!="undefined"))
                            {
                                var id = $(this).attr('data-rule-id');
                                var ip_address = $(this).find('input[type="text"]').val();
                                var name = $(this).find('td[data-label="rule-text"]').text();
                                arrRules.push( { 'id': id , 'ip_address' : ip_address, 'description' : name } );

                                uiDisplaytable.find('tr[data-rule-id="'+id+'"]').find('td[data-label="ip_address"]').text(ip_address);
                            }
                        });

                        //console.log(arrRules);     
                        var oParams = {
                                "params": {
                                    "data" : arrRules,
                                    "sbu_id" : iConstantSbuId
                                },
                                "user_id" : iConstantUserId
                            }
                        //console.log(oParams);
                        admin.security_settings.update_ip_blacklist_settings(oParams,$(this));
                 }
                
            });

            uiIPBlacklist.on('click','button.cancel-edit', function(){
                uiIPBlacklist.find("a.edit-values").removeClass('hidden');
                uiIPBlacklist.find("button").addClass('hidden');
                uiIPBlacklist.find("table.ip-blacklist-table-editable").addClass('hidden');
                uiIPBlacklist.find("table.ip-blacklist-table-display").removeClass('hidden');

                uiIPBlacklist.find("a.add-ip-address-to-list").removeClass('hidden');
                uiIPBlacklist.find("table.ip-blacklist-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                uiIPBlacklist.find("button.add-value").addClass('hidden');
                uiIPBlacklist.find("button.cancel-add").addClass('hidden');
            });

            uiIPBlacklist.on('click','a.add-ip-address-to-list', function(){
                //console.log($(this));
                $(this).addClass('hidden');
                uiIPBlacklist.find("table.ip-blacklist-table-display tbody.sbu_settings_container_add_row").removeClass('hidden');
                uiIPBlacklist.find("button.add-value").removeClass('hidden');
                uiIPBlacklist.find("button.cancel-add").removeClass('hidden');
    
            });

            uiIPBlacklist.on('click','button.add-value', function(){
                var uiAddField  = uiIPBlacklist.find("table.ip-blacklist-table-display tbody.sbu_settings_container_add_row");

                //this is for the validation of fields
                var iError = 0;
                $.each(uiAddField.find('input.required'), function(key ,value) {
                   if($(this).val() =='')
                   {
                      $(this).css({'border-color':'red'});
                      iError++;
                   }else{
                      $(this).css({'border-color':'#ddd'});    
                   }    
                });
                
                if(iError==0)
                { 
                        admin.security_settings.show_spinner($(this),true);
                        var sRuleText = uiAddField.find('input[name="description"]').val().capitalize();
                        var sRuleValue = uiAddField.find('input[name="ip_address"]').val();

                        var oParams = {
                                "params": {
                                    "data" : { 'description': sRuleText , 'ip_address' : sRuleValue ,"sbu_id" : iConstantSbuId },
                                    "sbu_id" : iConstantSbuId
                                },
                                "user_id" : iConstantUserId
                            }
                        //console.log(oParams);
                        admin.security_settings.add_ip_blacklist_settings(oParams,$(this));
                }
            });

            uiIPBlacklist.on('click','button.cancel-add', function(){
                uiIPBlacklist.find("a.add-ip-address-to-list").removeClass('hidden');
                uiIPBlacklist.find("table.ip-blacklist-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                uiIPBlacklist.find("button.add-value").addClass('hidden');
                uiIPBlacklist.find("button.cancel-add").addClass('hidden');
            });

            //fetching of whitelist excluded users
            $('ul.main_nav#main_nav_headers').on('click', 'li[content="security_settings"]', function() {
                var oAgents = cr8v.get_from_storage('agents');
                var oStoreUsers = cr8v.get_from_storage('store_users');
                if(typeof(oAgents) != 'undefined' && typeof(oStoreUsers) != 'undefined')
                {
                    var oExcludedAgents = oAgents.filter(function(el) {
                        return el['is_ip_whitelist_excluded'] == 1
                    })

                    var oExcludedStoreUsers = oStoreUsers.filter(function(el) {
                        return el['is_ip_whitelist_excluded'] == 1
                    })

                    if(typeof(oExcludedAgents) != 'undefined' || typeof(oExcludedStoreUsers) != 'undefined')
                    {
                        var $uiExcludedContainer = $('table.ip_whitelist_excluded_table').find('tbody.ip_whitelist_excluded_container');
                        $uiExcludedContainer.find('tr.ip-whitelist-user:not(.template)').remove()
                        $uiExcludedContainer.find('tr.ip-whitelist-store-user:not(.template)').remove()
                        $.each(oExcludedAgents, function(key, value) {
                            var $uiExcludedTemplate = $('tr.ip-whitelist-user.template').clone().removeClass('template');
                            $uiExcludedTemplate.find('[data-label="user_id"]').html(value.user_id)
                            $uiExcludedTemplate.find('[data-label="username"]').html(value.username)
                            $uiExcludedContainer.append($uiExcludedTemplate);
                        })
                        $.each(oExcludedStoreUsers, function(key, value) {
                            var $uiExcludedTemplate = $('tr.ip-whitelist-store-user.template').clone().removeClass('template');
                            $uiExcludedTemplate.find('[data-label="user_id"]').html(value.user_id)
                            $uiExcludedTemplate.find('[data-label="username"]').html(value.username)
                            $uiExcludedContainer.append($uiExcludedTemplate);
                        })
                    }

                    //for the blacklisted users
                    var oBlacklistedAgents = oAgents.filter(function(el) {
                        return el['is_blacklisted'] == 1
                    })

                    var oBlacklistedStoreUsers = oStoreUsers.filter(function(el) {
                        return el['is_blacklisted'] == 1
                    })

                    if(typeof(oBlacklistedAgents) != 'undefined' || typeof(oBlacklistedStoreUsers) != 'undefined')
                    {
                        var $uiBlacklistedContainer = $('table.blacklisted_users_table').find('tbody.blacklisted_user_container');
                        $uiBlacklistedContainer.find('tr.blacklisted-user:not(.template)').remove()
                        $uiBlacklistedContainer.find('tr.blacklisted-store-user:not(.template)').remove()
                        $.each(oBlacklistedAgents, function(i, bAgent) {
                            var $uiBlacklistedTemplate = $('tr.blacklisted-user.template').clone().removeClass('template');
                            $uiBlacklistedTemplate.find('[data-label="user_id"]').html(bAgent.user_id)
                            $uiBlacklistedTemplate.find('[data-label="username"]').html(bAgent.username)
                            $uiBlacklistedContainer.append($uiBlacklistedTemplate);
                        })
                        $.each(oBlacklistedStoreUsers, function(i, bStoreUser) {
                            var $uiBlacklistedTemplate = $('tr.blacklisted-store-user.template').clone().removeClass('template');
                            $uiBlacklistedTemplate.find('[data-label="user_id"]').html(bStoreUser.user_id)
                            $uiBlacklistedTemplate.find('[data-label="username"]').html(bStoreUser.username)
                            $uiBlacklistedContainer.append($uiBlacklistedTemplate);
                        })
                    }
                }
            })
            
            var uiTrClicked;
            $('table.ip_whitelist_excluded_table').on('click', 'a.modal-trigger', function() {
                $('div[modal-id="confirm-remove-user"]').addClass('showed');
                var uiTr = $(this).parents('tr.ip-whitelist-user:first');
                var iUserId = uiTr.find('td[data-label="user_id"]').text();

                $('div[modal-id="confirm-remove-user"]').find('button.remove_user_btn_confirm').attr('data-user-id',iUserId);
                uiTrClicked = uiTr;
            })

            $('div[modal-id="confirm-remove-user"]').on('click','button.remove_user_btn_confirm',function(){
                var uiTr = uiTrClicked;
                var iUserId = $(this).attr('data-user-id');  
                var oData = {
                    "user_id" : iUserId,        
                    "updated_by": iConstantUserId,
                    "data"   : [
                        {
                            field: "u.is_ip_whitelist_excluded",
                            value: 0
                        }
                    ],

                    "qualifiers": [{
                        field: 'u.id',
                        value: iUserId
                    }]
                }

                var oAddAgentAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oData,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : admin.config('url.api.jfc.users') + "/users/add_user",
                    "success": function (oData) {
                        if(typeof(oData.data[0]) != 'undefined')
                        {
                            var iFoundIndex;
                            var oAgents = cr8v.get_from_storage('agents');

                            $.each(oAgents, function(i, agent) {
                                if(agent.user_id == oData.data[0].user_id)
                                {
                                    iFoundIndex = i;
                                    return false;
                                }
                            });

                            cr8v.get_from_storage('agents').splice(iFoundIndex, 1);

                            if(typeof(oData.data[0]) != 'undefined')
                            {
                                cr8v.get_from_storage('agents').unshift(oData.data[0]);
                            }

                            admin.agents.render_agents_list(oData.data, true);
                            uiTr.remove();
                        }

                    }
                }

                admin.agents.ajax(oAddAgentAjaxConfig)     
            });
            
            $('table.ip_whitelist_excluded_table').on('click', 'a.delete', function() {
                var uiTr = $(this).parents('tr.ip-whitelist-user:first');
                var iUserId = uiTr.find('td[data-label="user_id"]').text();
                var oData = {
                    "user_id" : iUserId,        
                    "updated_by": iConstantUserId,
                    "data"   : [
                        {
                            field: "u.is_ip_whitelist_excluded",
                            value: 0
                        }
                    ],

                    "qualifiers": [{
                        field: 'u.id',
                        value: iUserId
                    }]
                }

                var oAddAgentAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oData,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : admin.config('url.api.jfc.users') + "/users/add_user",
                    "success": function (oData) {
                        if(typeof(oData.data[0]) != 'undefined')
                        {
                            var iFoundIndex;
                            var oAgents = cr8v.get_from_storage('agents');

                            $.each(oAgents, function(i, agent) {
                                if(agent.user_id == oData.data[0].user_id)
                                {
                                    iFoundIndex = i;
                                    return false;
                                }
                            });

                            cr8v.get_from_storage('agents').splice(iFoundIndex, 1);

                            if(typeof(oData.data[0]) != 'undefined')
                            {
                                cr8v.get_from_storage('agents').unshift(oData.data[0]);
                            }

                            admin.agents.render_agents_list(oData.data, true);
                            uiTr.remove();
                        }

                    }
                }

                admin.agents.ajax(oAddAgentAjaxConfig)


            })

            $('table.ip_whitelist_excluded_table').on('click', 'a.modal-trigger-store-user', function() {
                $('div[modal-id="confirm-remove-store-user"]').addClass('showed');
                var uiTr = $(this).parents('tr.ip-whitelist-store-user:first');
                var iUserId = uiTr.find('td[data-label="user_id"]').text();

                $('div[modal-id="confirm-remove-store-user"]').find('button.remove_user_btn_confirm').attr('data-user-id',iUserId);
                uiTrClicked = uiTr;
            })

            $('div[modal-id="confirm-remove-store-user"]').on('click','button.remove_user_btn_confirm',function(){
                var uiTr = uiTrClicked;
                var iUserId = uiTr.find('td[data-label="user_id"]').text();
                
                var oParams = {
                        "params":{
                                "data" : [
                                        {
                                         "field" : "u.is_ip_whitelist_excluded",
                                         "value" : 0
                                        },
                                         ],
                                    "qualifiers" : [
                                        {
                                         "field" : "user_id",
                                         "value" : iUserId,
                                         "operator" : "="
                                        }
                                    ]
                        },
                        "user_id" : iUserId
                    };

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : admin.config('url.api.jfc.users') + "/users/update_store_user",
                    "success": function (oData) {
                        if(typeof(oData.data[0]) != 'undefined')
                        {
                            var iFoundIndex;
                            var oStoreUsers = cr8v.get_from_storage('store_users');

                            $.each(oStoreUsers, function(i, store) {
                                if(store.user_id == oData.data[0].user_id)
                                {
                                    iFoundIndex = i;
                                    return false;
                                }
                            });

                            cr8v.get_from_storage('store_users').splice(iFoundIndex, 1);

                            if(typeof(oData.data[0]) != 'undefined')
                            {
                                cr8v.get_from_storage('store_users').unshift(oData.data[0]);
                            }
                            
                            var oStores = cr8v.get_from_storage('stores');
                            admin.stores.render_store_list(oStores);
                            uiTr.remove();
                        }

                    }
                }

                admin.agents.ajax(oAjaxConfig)
            });

            $('table.ip_whitelist_excluded_table').on('click', 'a.delete_store_user', function() {
                var uiTr = $(this).parents('tr.ip-whitelist-store-user:first');
                var iUserId = uiTr.find('td[data-label="user_id"]').text();
                
                var oParams = {
                        "params":{
                                "data" : [
                                        {
                                         "field" : "u.is_ip_whitelist_excluded",
                                         "value" : 0
                                        },
                                         ],
                                    "qualifiers" : [
                                        {
                                         "field" : "user_id",
                                         "value" : iUserId,
                                         "operator" : "="
                                        }
                                    ]
                        },
                        "user_id" : iUserId
                    };

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : admin.config('url.api.jfc.users') + "/users/update_store_user",
                    "success": function (oData) {
                        if(typeof(oData.data[0]) != 'undefined')
                        {
                            var iFoundIndex;
                            var oStoreUsers = cr8v.get_from_storage('store_users');

                            $.each(oStoreUsers, function(i, store) {
                                if(store.user_id == oData.data[0].user_id)
                                {
                                    iFoundIndex = i;
                                    return false;
                                }
                            });

                            cr8v.get_from_storage('store_users').splice(iFoundIndex, 1);

                            if(typeof(oData.data[0]) != 'undefined')
                            {
                                cr8v.get_from_storage('store_users').unshift(oData.data[0]);
                            }
                            
                            var oStores = cr8v.get_from_storage('stores');
                            admin.stores.render_store_list(oStores);
                            uiTr.remove();
                        }

                    }
                }

                admin.agents.ajax(oAjaxConfig)

            })

            $('table.blacklisted_users_table').on('click', 'a.modal-trigger', function() {
                $('div[modal-id="confirm-remove-blacklisted-user"]').addClass('showed');
                var uiTr = $(this).parents('tr.blacklisted-user:first');
                var iUserId = uiTr.find('td[data-label="user_id"]').text();

                $('div[modal-id="confirm-remove-blacklisted-user"]').find('button.remove_user_btn_confirm').attr('data-user-id',iUserId);
                uiTrClicked = uiTr;
            })

            $('div[modal-id="confirm-remove-blacklisted-user"]').on('click','button.remove_user_btn_confirm',function(){
                var uiTr = uiTrClicked;
                var iUserId = $(this).attr('data-user-id');  
                var oData = {
                    "user_id": iUserId,
                    "updated_by": iConstantUserId,
                    "data"   : [
                        {
                            field: "u.is_blacklisted",
                            value: 0
                        }
                    ],

                    "qualifiers": [{
                        field: 'u.id',
                        value: iUserId
                    }]
                }

                var oAddAgentAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oData,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : admin.config('url.api.jfc.users') + "/users/add_user",
                    "success": function (oData) {
                        if(typeof(oData.data[0]) != 'undefined')
                        {
                            var iFoundIndex;
                            var oAgents = cr8v.get_from_storage('agents');

                            $.each(oAgents, function(i, agent) {
                                if(agent.user_id == oData.data[0].user_id)
                                {
                                    iFoundIndex = i;
                                    return false;
                                }
                            });

                            cr8v.get_from_storage('agents').splice(iFoundIndex, 1);

                            if(typeof(oData.data[0]) != 'undefined')
                            {
                                cr8v.get_from_storage('agents').unshift(oData.data[0]);
                            }

                            admin.agents.render_agents_list(oData.data, true);
                            uiTr.remove();
                        }

                    }
                }

                admin.agents.ajax(oAddAgentAjaxConfig) 
            });

            $('table.blacklisted_users_table').on('click', 'a.delete', function() {
                var uiTr = $(this).parents('tr.blacklisted-user:first');
                var iUserId = uiTr.find('td[data-label="user_id"]').text();
                var oData = {
                    "user_id": iUserId,
                    "updated_by": iConstantUserId,
                    "data"   : [
                        {
                            field: "u.is_blacklisted",
                            value: 0
                        }
                    ],

                    "qualifiers": [{
                        field: 'u.id',
                        value: iUserId
                    }]
                }

                var oAddAgentAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oData,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : admin.config('url.api.jfc.users') + "/users/add_user",
                    "success": function (oData) {
                        if(typeof(oData.data[0]) != 'undefined')
                        {
                            var iFoundIndex;
                            var oAgents = cr8v.get_from_storage('agents');

                            $.each(oAgents, function(i, agent) {
                                if(agent.user_id == oData.data[0].user_id)
                                {
                                    iFoundIndex = i;
                                    return false;
                                }
                            });

                            cr8v.get_from_storage('agents').splice(iFoundIndex, 1);

                            if(typeof(oData.data[0]) != 'undefined')
                            {
                                cr8v.get_from_storage('agents').unshift(oData.data[0]);
                            }

                            admin.agents.render_agents_list(oData.data, true);
                            uiTr.remove();
                        }

                    }
                }

                admin.agents.ajax(oAddAgentAjaxConfig)


            })

            $('table.blacklisted_users_table').on('click', 'a.modal-trigger-store-user', function() {
                $('div[modal-id="confirm-remove-blacklisted-store-user"]').addClass('showed');
                var uiTr = $(this).parents('tr.blacklisted-store-user:first');
                var iUserId = uiTr.find('td[data-label="user_id"]').text();

                $('div[modal-id="confirm-remove-blacklisted-store-user"]').find('button.remove_user_btn_confirm').attr('data-user-id',iUserId);
                uiTrClicked = uiTr;
            })

            $('div[modal-id="confirm-remove-blacklisted-store-user"]').on('click','button.remove_user_btn_confirm',function(){
                var uiTr = uiTrClicked;
                var iUserId = uiTr.find('td[data-label="user_id"]').text();
                
                 var oParams = {
                        "params":{
                                "data" : [
                                        {
                                         "field" : "u.is_blacklisted",
                                         "value" : 0
                                        },
                                         ],
                                    "qualifiers" : [
                                        {
                                         "field" : "user_id",
                                         "value" : iUserId,
                                         "operator" : "="
                                        }
                                    ]
                        },
                        "user_id" : iUserId
                    };

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : admin.config('url.api.jfc.users') + "/users/update_store_user",
                    "success": function (oData) {
                        if(typeof(oData.data[0]) != 'undefined')
                        {
                            var iFoundIndex;
                            var oStoreUsers = cr8v.get_from_storage('store_users');

                            $.each(oStoreUsers, function(i, store) {
                                if(store.user_id == oData.data[0].user_id)
                                {
                                    iFoundIndex = i;
                                    return false;
                                }
                            });

                            cr8v.get_from_storage('store_users').splice(iFoundIndex, 1);

                            if(typeof(oData.data[0]) != 'undefined')
                            {
                                cr8v.get_from_storage('store_users').unshift(oData.data[0]);
                            }
                            
                            var oStores = cr8v.get_from_storage('stores');
                            admin.stores.render_store_list(oStores);
                            uiTr.remove();
                        }

                    }
                }

                admin.agents.ajax(oAjaxConfig)

            });

            $('table.blacklisted_users_table').on('click', 'a.delete_store_user', function() {
                var uiTr = $(this).parents('tr.blacklisted-store-user:first');
                var iUserId = uiTr.find('td[data-label="user_id"]').text();
                
                var oParams = {
                        "params":{
                                "data" : [
                                        {
                                         "field" : "u.is_blacklisted",
                                         "value" : 0
                                        },
                                         ],
                                    "qualifiers" : [
                                        {
                                         "field" : "user_id",
                                         "value" : iUserId,
                                         "operator" : "="
                                        }
                                    ]
                        },
                        "user_id" : iUserId
                    };

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : admin.config('url.api.jfc.users') + "/users/update_store_user",
                    "success": function (oData) {
                        if(typeof(oData.data[0]) != 'undefined')
                        {
                            var iFoundIndex;
                            var oStoreUsers = cr8v.get_from_storage('store_users');

                            $.each(oStoreUsers, function(i, store) {
                                if(store.user_id == oData.data[0].user_id)
                                {
                                    iFoundIndex = i;
                                    return false;
                                }
                            });

                            cr8v.get_from_storage('store_users').splice(iFoundIndex, 1);

                            if(typeof(oData.data[0]) != 'undefined')
                            {
                                cr8v.get_from_storage('store_users').unshift(oData.data[0]);
                            }
                            
                            var oStores = cr8v.get_from_storage('stores');
                            admin.stores.render_store_list(oStores);
                            uiTr.remove();
                        }

                    }
                }

                admin.agents.ajax(oAjaxConfig)

            })

            
        },//end initialize,

        'show_spinner': function (uiElem, bShow) {
            if (bShow == true) {
                uiElem.find('i.fa-spinner').removeClass('hidden');
                uiElem.prop('disabled', true);
            }
            else {
                uiElem.find('i.fa-spinner').addClass('hidden');
                uiElem.prop('disabled', false);
            }
        },
        
         'fetch_ip_whitelist' : function(iConstantSbuId) {
            var oAjaxConfig = {
                "type"   : "Get",
                "data"   : {
                    "params" : {
                        "sbu_id" : iConstantSbuId
                    }
                },

                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : admin.config('url.api.jfc.store') + "/stores/ip_whitelist",
                "success": function (oData) {
                    if( oData.data.length > 0)
                    {
                        var oRules = oData.data;
                        //console.log(oDiscounts);
                        admin.security_settings.render_ip_whitelist_settings(oRules);
                        //prevent from alpha input
                        $("input.input_numeric").off("keyup").on("keyup",function (event) {
                            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
                        });
                    }
                }
            }

            admin.agents.ajax(oAjaxConfig);
        },

        'render_ip_whitelist_settings' : function(oRules) {
            if(typeof(oRules) != 'undefined') {
                var uiRuleTemplate = $('table.ip-whitelist-table-display').find('tr.rule_tr.template');
                var uiRuleEditableTemplate = $('table.ip-whitelist-table-editable').find('tr.rule_editable_tr.template');
                var uiContainer = $('table.ip-whitelist-table-display').find('tbody.sbu_settings_container')
                var uiEditableContainer = $('table.ip-whitelist-table-editable').find('tbody.sbu_settings_editable_container')
                $.each(oRules, function(index ,rule) {
                    var uiTemplate = uiRuleTemplate.clone().removeClass('template');
                    for (var key in rule) {
                        if (rule.hasOwnProperty(key)) {
                            uiTemplate.attr('data-rule-id',rule.id);
                            uiTemplate.find('[data-label="' + key + '"]').text(rule[key])
                        }
                    }

                    var uiEditableTemplate = uiRuleEditableTemplate.clone().removeClass('template');
                    for (var key in rule) {
                        if (rule.hasOwnProperty(key)) {
                            uiEditableTemplate.attr('data-rule-id',rule.id);
                            uiEditableTemplate.find('[data-label="' + key + '"]').text(rule[key])
                            uiEditableTemplate.find('[data-input="' + key + '"]').val(rule[key])
                            uiEditableTemplate.find('[data-input-name="' + key + '"]').attr('name', rule[key] )
                        }
                    }

                    uiContainer.append(uiTemplate)
                    uiEditableContainer.append(uiEditableTemplate)
                })
            }
        },
        
        'update_ip_whitelist_settings': function (oParams,uiButton) {
            if (typeof(oParams) != 'undefined') {
                var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                        "url"    : admin.config('url.api.jfc.store') + "/stores/ip_whitelist_setting_update",
                        "success": function (data) {
                            //console.log(data);
                            if(typeof(data) !== 'undefined')
                            {                      
                                //console.log(data);
                                admin.security_settings.show_spinner(uiButton,false);
                                uiIPWhitelist.find("a.edit-values").removeClass('hidden');
                                uiIPWhitelist.find("button").addClass('hidden');
                                uiIPWhitelist.find("table.ip-whitelist-table-editable").addClass('hidden');
                                uiIPWhitelist.find("table.ip-whitelist-table-display").removeClass('hidden');

                                uiIPWhitelist.find("a.add-ip-address-to-list").removeClass('hidden');
                                uiIPWhitelist.find("table.ip-whitelist-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                                uiIPWhitelist.find("button.add-value").addClass('hidden');
                                uiIPWhitelist.find("button.cancel-add").addClass('hidden');
                            }

                        }
                    }

               admin.agents.ajax(oAjaxConfig);
            }
        },

        'add_ip_whitelist_settings': function (oParams,uiButton) {
            if (typeof(oParams) != 'undefined') {
                var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                        "url"    : admin.config('url.api.jfc.store') + "/stores/ip_whitelist_setting_add",
                        "success": function (data) {
                            //console.log(data);
                            if(typeof(data) !== 'undefined')
                            {                      
                                console.log(data);
                                //admin.security_settings.show_spinner(uiButton,false);
                                var oRules = data.data;
                                var uiRuleTemplate = $('table.ip-whitelist-table-display').find('tr.rule_tr.template');
                                var uiRuleEditableTemplate = $('table.ip-whitelist-table-editable').find('tr.rule_editable_tr.template');
                                var uiContainer = $('table.ip-whitelist-table-display').find('tbody.sbu_settings_container')
                                var uiEditableContainer = $('table.ip-whitelist-table-editable').find('tbody.sbu_settings_editable_container')

                                var uiTemplate = uiRuleTemplate.clone().removeClass('template');

                                uiTemplate.attr('data-rule-id',oRules.id);
                                uiTemplate.find('td[data-label="rule-text"]').text(oRules.description);
                                uiTemplate.find('td[data-label="ip_address"]').text(oRules.ip_address);


                                var uiEditableTemplate = uiRuleEditableTemplate.clone().removeClass('template');

                                uiEditableTemplate.attr('data-rule-id',oRules.id);
                                uiEditableTemplate.find('[data-label="rule-text"]').text(oRules.description)
                                uiEditableTemplate.find('[data-input="ip_address"]').val(oRules.ip_address)
        
                                uiContainer.append(uiTemplate);
                                uiEditableContainer.append(uiEditableTemplate);

                                admin.security_settings.show_spinner(uiButton,false);

                                uiIPWhitelist.find("a.add-ip-address-to-list").removeClass('hidden');
                                uiIPWhitelist.find("table.ip-whitelist-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                                uiIPWhitelist.find("table.ip-whitelist-table-display tbody.sbu_settings_container_add_row").find('input[type="text"]').val('');
                                uiIPWhitelist.find("button.add-value").addClass('hidden');
                                uiIPWhitelist.find("button.cancel-add").addClass('hidden');

                            }

                        }
                    }

               admin.agents.ajax(oAjaxConfig);
            }
        },

        //for the ip blacklist
         'fetch_ip_blacklist' : function(iConstantSbuId) {
            var oAjaxConfig = {
                "type"   : "Get",
                "data"   : {
                    "params" : {
                        "sbu_id" : iConstantSbuId
                    }
                },

                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : admin.config('url.api.jfc.store') + "/stores/ip_blacklist",
                "success": function (oData) {
                    if( oData.data.length > 0)
                    {
                        var oRules = oData.data;
                        //console.log(oDiscounts);
                        admin.security_settings.render_ip_blacklist_settings(oRules);
                        //prevent from alpha input
                        $("input.input_numeric").off("keyup").on("keyup",function (event) {
                            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
                        });
                    }
                }
            }

            admin.agents.ajax(oAjaxConfig);
        },

        'render_ip_blacklist_settings' : function(oRules) {
            if(typeof(oRules) != 'undefined') {
                var uiRuleTemplate = $('table.ip-blacklist-table-display').find('tr.rule_tr.template');
                var uiRuleEditableTemplate = $('table.ip-blacklist-table-editable').find('tr.rule_editable_tr.template');
                var uiContainer = $('table.ip-blacklist-table-display').find('tbody.sbu_settings_container')
                var uiEditableContainer = $('table.ip-blacklist-table-editable').find('tbody.sbu_settings_editable_container')
                $.each(oRules, function(index ,rule) {
                    var uiTemplate = uiRuleTemplate.clone().removeClass('template');
                    for (var key in rule) {
                        if (rule.hasOwnProperty(key)) {
                            uiTemplate.attr('data-rule-id',rule.id);
                            uiTemplate.find('[data-label="' + key + '"]').text(rule[key])
                        }
                    }

                    var uiEditableTemplate = uiRuleEditableTemplate.clone().removeClass('template');
                    for (var key in rule) {
                        if (rule.hasOwnProperty(key)) {
                            uiEditableTemplate.attr('data-rule-id',rule.id);
                            uiEditableTemplate.find('[data-label="' + key + '"]').text(rule[key])
                            uiEditableTemplate.find('[data-input="' + key + '"]').val(rule[key])
                            uiEditableTemplate.find('[data-input-name="' + key + '"]').attr('name', rule[key] )
                        }
                    }

                    uiContainer.append(uiTemplate)
                    uiEditableContainer.append(uiEditableTemplate)
                })
            }
        },
        
        'update_ip_blacklist_settings': function (oParams,uiButton) {
            if (typeof(oParams) != 'undefined') {
                var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                        "url"    : admin.config('url.api.jfc.store') + "/stores/ip_blacklist_setting_update",
                        "success": function (data) {
                            //console.log(data);
                            if(typeof(data) !== 'undefined')
                            {                      
                                console.log(data);
                                admin.security_settings.show_spinner(uiButton,false);
                                uiIPBlacklist.find("a.edit-values").removeClass('hidden');
                                uiIPBlacklist.find("button").addClass('hidden');
                                uiIPBlacklist.find("table.ip-blacklist-table-editable").addClass('hidden');
                                uiIPBlacklist.find("table.ip-blacklist-table-display").removeClass('hidden');

                                uiIPBlacklist.find("a.add-ip-address-to-list").removeClass('hidden');
                                uiIPBlacklist.find("table.ip-blacklist-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                                uiIPBlacklist.find("button.add-value").addClass('hidden');
                                uiIPBlacklist.find("button.cancel-add").addClass('hidden');
                            }

                        }
                    }

               admin.agents.ajax(oAjaxConfig);
            }
        },

        'add_ip_blacklist_settings': function (oParams,uiButton) {
            if (typeof(oParams) != 'undefined') {
                var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                        "url"    : admin.config('url.api.jfc.store') + "/stores/ip_blacklist_setting_add",
                        "success": function (data) {
                            //console.log(data);
                            if(typeof(data) !== 'undefined')
                            {                      
                                console.log(data);
                                //admin.security_settings.show_spinner(uiButton,false);
                                var oRules = data.data;
                                var uiRuleTemplate = $('table.ip-blacklist-table-display').find('tr.rule_tr.template');
                                var uiRuleEditableTemplate = $('table.ip-blacklist-table-editable').find('tr.rule_editable_tr.template');
                                var uiContainer = $('table.ip-blacklist-table-display').find('tbody.sbu_settings_container')
                                var uiEditableContainer = $('table.ip-blacklist-table-editable').find('tbody.sbu_settings_editable_container')

                                var uiTemplate = uiRuleTemplate.clone().removeClass('template');

                                uiTemplate.attr('data-rule-id',oRules.id);
                                uiTemplate.find('td[data-label="rule-text"]').text(oRules.description);
                                uiTemplate.find('td[data-label="ip_address"]').text(oRules.ip_address);


                                var uiEditableTemplate = uiRuleEditableTemplate.clone().removeClass('template');

                                uiEditableTemplate.attr('data-rule-id',oRules.id);
                                uiEditableTemplate.find('[data-label="rule-text"]').text(oRules.description)
                                uiEditableTemplate.find('[data-input="ip_address"]').val(oRules.ip_address)
        
                                uiContainer.append(uiTemplate);
                                uiEditableContainer.append(uiEditableTemplate);

                                admin.security_settings.show_spinner(uiButton,false);

                                uiIPBlacklist.find("a.add-ip-address-to-list").removeClass('hidden');
                                uiIPBlacklist.find("table.ip-blacklist-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                                uiIPBlacklist.find("table.ip-blacklist-table-display tbody.sbu_settings_container_add_row").find('input[type="text"]').val('');
                                uiIPBlacklist.find("button.add-value").addClass('hidden');
                                uiIPBlacklist.find("button.cancel-add").addClass('hidden');

                            }

                        }
                    }

               admin.agents.ajax(oAjaxConfig);
            }
        },
       

    }//end platform prototype

    String.prototype.capitalize = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }

}());

$(window).load(function () {
    admin.security_settings.initialize();
});