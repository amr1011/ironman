/**
 *  Customer Class
 *
 *
 *
 */
(function () {
    "use strict";


    CPlatform.prototype.gis = {

        'add_error_message': function (sMessages, uiContainer) {
            $('p.error_messages').remove();
            if (sMessages.length > 0) {
                if (typeof(uiContainer) != 'undefined') {
                    uiContainer.show();
                    uiContainer.append(sMessages);
                }

            }
        },

        'update_address' : function(iStoreID, iAddressID, iDeliveryTime) {
            if(typeof(iStoreID) != 'undefined' && typeof(iAddressID) != 'undefined' && typeof(iDeliveryTime) != 'undefined')
            {
                       var oParams = {
                            "params": {
                                "data" : [{
                                            "field" : "store_id",
                                            "value" : iStoreID
                                },
                                {
                                            "field" : "delivery_time",
                                            "value" : iDeliveryTime
                                }],
                                "qualifiers" : [{
                                     "field" : "ca.id",
                                     "operator" : "=",
                                     "value" :   iAddressID    
                                }]
                            }
                        };

                        var oAjaxConfig = {
                            "type"   : "POST",
                            "data"   : oParams,
                            "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                            "url"    : callcenter.config('url.api.jfc.customers') + "/customers/update_address",
                            "success": function (oData) {
                                    
                            }
                        };

                        callcenter.call_orders.get(oAjaxConfig)         
            }

            
        },

        /*function that will hold responses of find RTA*/
        'rta_return'       : function (iRetailFound, iStoreID, iDeliveryTime) {
            /*
             *  1 - Found Retail (Normal Order)
             *  4 - Offline Store (Manual Order)
             *  5 - Out of Delivery Area (Parked Order)
             * */

            var uiButton;
            var uiAddMealButton;
            if ($('section.content div#add_customer_content').is(':visible') == true) //if in add customer find RTA
            {
                uiButton = $('section.content div#add_customer_content').find('button.rta_proceed');
                if (iRetailFound == 1) //Normal Order
                {
                    uiButton.text('Proceed to Order').attr('data-order-rta', iRetailFound);
                    $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('23');
                }
                else if (iRetailFound == 4) {
                    uiButton.text('Proceed to Order (Manual Order)').attr('data-order-rta', iRetailFound);
                    $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
                }
                else if (iRetailFound == 5) {
                    uiButton.text('Proceed to Order (Parked Order)').attr('data-order-rta', iRetailFound);
                    $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('22');
                }
                
                $('section.content #order_process_content').find('form#order_customer_information').find('input[name="customer_type"]').val('new');
                uiButton.prop('disabled', false);

                if(iRetailFound == 4 || iRetailFound == 1)
                {
                    cr8v.add_to_storage('add_customer_rta_store_id', iStoreID);
                    cr8v.add_to_storage('add_customer_rta_store_time', iDeliveryTime);
                    var iAddressID = cr8v.get_from_storage('current_address_id', '');
                    if(typeof(iAddressID) != 'undefined')
                    {
                         iAddressID = iAddressID[0];
                         if(typeof(iAddressID) != 'undefined') {
                             iAddressID = iAddressID[0];
                             if(typeof(iAddressID) != 'undefined')
                             {
                                 callcenter.gis.update_address(iStoreID, iAddressID.address_id, iDeliveryTime);        
                             }
                         }
                    }
                }
            }
            else if ($('section.content div#search_customer_content').is(':visible') == true) //if in add customer find RTA
            {
                uiButton = $('section.content div#search_customer_content').find('button.proceed_order_from_search');
                uiAddMealButton = $('section.content div#search_customer_content').find('button.add_last_meal_to_order');
                if (iRetailFound == 1) //Normal Order
                {
                    uiAddMealButton.text('Add Last Order to Cart').attr('data-order-rta', iRetailFound);
                    uiButton.text('Proceed to Order').attr('data-order-rta', iRetailFound);
                    $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('23');
                }
                else if (iRetailFound == 4) {
                    uiAddMealButton.text('Add Last Order to Cart (Manual Order)').attr('data-order-rta', iRetailFound);
                    uiButton.text('Proceed to Order (Manual Order)').attr('data-order-rta', iRetailFound);
                    $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
                }
                else if (iRetailFound == 5) {
                    uiAddMealButton.text('Add Last Order to Cart (Parked Order)').attr('data-order-rta', iRetailFound);  
                    uiButton.text('Proceed to Order (Parked Order)').attr('data-order-rta', iRetailFound);
                    $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('22');
                }

                $('section.content #order_process_content').find('form#order_customer_information').find('input[name="customer_type"]').val('existing');
                uiAddMealButton.prop('disabled', false);
                uiButton.prop('disabled', false);
               
                if(iRetailFound == 4 || iRetailFound == 1)
                {
                    if(typeof(iStoreID) != 'undefined' && typeof(iDeliveryTime) != 'undefined')
                    {
                          cr8v.add_to_storage('add_customer_rta_store_id', iStoreID);
                                cr8v.add_to_storage('add_customer_rta_store_time', iDeliveryTime);
                                var iAddressID = $('section.content div#search_customer_content').find('div.customer_address_container div[address_id]:visible').attr('address_id');
                                if(typeof(iAddressID) != 'undefined')
                                {
                                    callcenter.gis.update_address(iStoreID, iAddressID, iDeliveryTime);        
                                }       
                    }            
                }
               
                
            }
            else if ($('form#coordinator_update_address').is(':visible') == true) //if in update address find RTA
            {
                uiButton = $('form#coordinator_update_address').find('button.confirm_update_address');
                if (iRetailFound == 1) //Normal Order
                {
                    uiButton.text('Send Order');
                    uiButton.attr('data-order-status','23');
                }
                else if (iRetailFound == 4) {
                    uiButton.text('Manual Order');
                    uiButton.attr('data-order-status','17');
                }
                else if (iRetailFound == 5) {
                    uiButton.text('Parked Order');
                    uiButton.attr('data-order-status','22');
                }
                // $('section.content #order_process_content').find('form#order_customer_information').find('input[name="customer_type"]').val('existing');
                uiButton.prop('disabled', false);
                callcenter.gis.show_spinner($('a.find_retail_search'), false);
            }

            else if($('div[modal-id="process-order"]:visible'))
            {
                uiButton = $('div[modal-id="process-order"]').find('button.confirm_process');
                if (iRetailFound == 1) //Normal Order
                {
                    uiButton.text('Send Order').prop('disabled', false);
                    $('div[modal-id="process-order"]').attr('order_status', '23');
                    $('div[modal-id="process-order"]').attr('store_id', iStoreID);
                }
                else if (iRetailFound == 4) {
                    uiButton.text('Manual Order').prop('disabled', false);
                    $('div[modal-id="process-order"]').attr('order_status', '17');
                    $('div[modal-id="process-order"]').attr('store_id', iStoreID);
                }
                else if (iRetailFound == 5) {
                    uiButton.text('Parked Order').prop('disabled', false);
                    $('div[modal-id="process-order"]').attr('order_status', '22');
                    $('div[modal-id="process-order"]').attr('store_id', iStoreID);
                }

                uiButton.prop('disabled', false);
            }

            var uiModalOrderComplete = $('div[modal-id="manual-order-complete"]').find('h4.order_type');

            if (iRetailFound == 1) //Normal Order
            {
                uiModalOrderComplete.text('Regular Order Complete');
            }
            else if (iRetailFound == 4)
            {
                uiModalOrderComplete.text('Manual Order Complete');
            }
            else if (iRetailFound == 5)
            {
                uiModalOrderComplete.text('Parked Order Complete');
            }



            if (typeof(iStoreID) != 'undefined') {
                callcenter.gis.fetch_product_unavailability(iStoreID);
                var oSession = {
                    'current_store_id' :  iStoreID   
                }
                if(callcenter.get_from_storage('active_storage', '') !== undefined)
                {
                      callcenter.pop('active_storage');  
                }
                                 
                callcenter.add_to_storage('active_storage', oSession)
                //oSock.bind(
                //    {
                //        'event'   : 'product_unavailability',
                //        'channel' : 'private-' + iStoreID,
                //        'callback': function (sData) {
                //             var oSession = callcenter.get_from_storage('active_storage', '');
                //             var aUnavailableCoreIds = callcenter.get_from_storage('unavailable_core_id', '');
                //             if(oSession !== undefined && aUnavailableCoreIds !== undefined && sData !== undefined)
                //             {
                //                 var oData = $.parseJSON(sData)    ;
                //                 oSession = oSession[0];
                //                 aUnavailableCoreIds = aUnavailableCoreIds[0];
                //                 if(oData.store_id == oSession.current_store_id)
                //                 {
                //
                //                        if(oData.set_available == '1') //if set avaiable
                //                        {
                //
                //                            if($.inArray(oData.item_id, aUnavailableCoreIds) > -1) //if in array
                //                            {
                //                                var index = aUnavailableCoreIds.indexOf(oData.item_id)
                //                                aUnavailableCoreIds.splice(index, 1);
                //                            }
                //                        }
                //                        else
                //                        {
                //                            if($.inArray(oData.item_id, aUnavailableCoreIds) < 0) //if in array
                //                            {
                //                                aUnavailableCoreIds.push(oData.item_id)
                //                            }
                //                        }
                //
                //                        cr8v.pop('unavailable_core_id');
                //                        cr8v.add_to_storage('unavailable_core_id', aUnavailableCoreIds)
                //                        callcenter.order_process.check_products_unavailability($('section.content div#order_process_content').find('div.product_search_category_result'));
                //                 }
                //             }
                //
                //
                //
                //        }
                //    }
                //)
                //
                //oSock.bind({
                //    'event'   : 'pusher:member_added',
                //    'channel' : 'presence-' + iStoreID,
                //    'callback': function (oData) {
                //        var bAvailability = callcenter.gis.check_store_availability(iStoreID);
                //
                //        if ($('section.content div#add_customer_content').is(':visible') == true) //if in add customer find RTA
                //        {
                //            if(bAvailability){
                //                if($('div.rta-address-info-container:visible').length > 0)
                //                {
                //                    var sStoreName = $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text()
                //                    $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
                //                    uiButton.text('Proceed to Order').attr('data-order-rta', 1);
                //                }
                //            }
                //            else
                //            {
                //                if($('div.rta-address-info-container:visible').length > 0)
                //                {
                //                    var sStoreName = $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text()
                //                    sStoreName = sStoreName.replace(' (Offline)', '');
                //                    console.log()
                //                    $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text(sStoreName + ' (Offline)').css('color', 'red');
                //                    uiButton.text('Proceed to Order (Manual Order)').attr('data-order-rta', 4);
                //                }
                //                //manual order
                //            }
                //        }
                //        else if ($('section.content div#search_customer_content').is(':visible') == true) //if in add customer find RTA
                //        {
                //            uiButton = $('section.content div#search_customer_content').find('button.proceed_order_from_search');
                //            uiAddMealButton = $('section.content div#search_customer_content').find('button.add_last_meal_to_order');
                //
                //            if(bAvailability){
                //                if($('div.rta-address-info-container:visible').length > 0)
                //                {
                //                    var sStoreName = $('div.rta-address-info-container:visible').find('span.retail-store-name').text()
                //                    $('div.rta-address-info-container:visible').find('span.retail-store-name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
                //                    uiAddMealButton.text('Add Last Order to Cart').attr('data-order-rta', 1);
                //                    uiButton.text('Proceed to Order').attr('data-order-rta', 1);
                //                }
                //            }
                //            else
                //            {
                //                if($('div.rta-address-info-container:visible').length > 0)
                //                {
                //                    var sStoreName = $('div.rta-address-info-container:visible').find('span.retail-store-name').text()
                //                    sStoreName = sStoreName.replace(' (Offline)', '');
                //                    console.log()
                //                    $('div.rta-address-info-container:visible').find('span.retail-store-name').text(sStoreName + ' (Offline)').css('color', 'red');
                //                    uiAddMealButton.text('Add Last Order to Cart (Manual Order)').attr('data-order-rta', 4);
                //                    uiButton.text('Proceed to Order (Manual Order)').attr('data-order-rta', 4);
                //                }
                //            }
                //        }
                //
                //        else if($('section.content div#order_process_content').is(':visible') == true)
                //        {
                //            uiButton = $('section.content div#order_process_content').find('button[modal-target="manual-order-summary"]');
                //            var orderType = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_type"]').val()
                //            var customerType = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="customer_type"]').val()
                //            var total_bill = $('div.cart-container table.cart-table-table').find('td.total_bill').attr('data-total-bill');
                //            // var existingCustomerConfigVal = parseInt('2500');
                //            var newCustomerConfigVal = parseInt('2500');
                //            // orderType = iRetailFound;
                //            // $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_type"]').val(iRetailFound);
                //
                //            if(bAvailability){
                //                //if online
                //                if($('div.rta-container div.rta_store:visible').length > 0)
                //                {
                //                    var sStoreName = $('div.rta-container div.rta_store:visible').find('strong.store_name').text()
                //                    $('div.rta-container div.rta_store:visible').find('strong.store_name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
                //                }
                //
                //                // if(customerType === 'existing')
                //                // {
                //                //     if(total_bill > existingCustomerConfigVal)
                //                //     {
                //                //         $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                //                //         $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
                //                //         $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                //                //     }
                //                //     else
                //                //     {
                //                //         $('section.content #order_process_content').find('button.send_order').text('Send Order');
                //                //         $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
                //                //         $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('23');
                //                //     }
                //                // }
                //                if(customerType === 'new')
                //                {
                //                    if(total_bill > newCustomerConfigVal)
                //                    {
                //                        $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                //                        $('section.content #order_process_content').find('button.advance_order')/*.text('Verify Advance Order')*/.hide();
                //                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                //                    }
                //                    else
                //                    {
                //                        $('section.content #order_process_content').find('button.send_order').text('Send Order');
                //                        $('section.content #order_process_content').find('button.advance_order').text('Advance Order').show();
                //                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
                //                    }
                //                }
                //
                //                var uiModalOrderComplete = $('div[modal-id="manual-order-complete"]').find('h4.order_type');
                //                uiModalOrderComplete.text('Regular Order Complete');
                //            }
                //            else
                //            {
                //                //if offline
                //                if($('div.rta-container div.rta_store:visible').length > 0)
                //                {
                //                    var sStoreName = $('div.rta-container div.rta_store:visible').find('strong.store_name').text()
                //                    sStoreName = sStoreName.replace(' (Offline)', '');
                //                    $('div.rta-container div.rta_store:visible').find('strong.store_name').text(sStoreName + ' (Offline)').css('color', 'red');
                //                }
                //
                //
                //                // if(customerType === 'existing')
                //                // {
                //                //     if(total_bill > existingCustomerConfigVal)
                //                //     {
                //                //         $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                //                //         $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
                //                //         $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                //                //     }
                //                //     else
                //                //     {
                //                //         $('section.content #order_process_content').find('button.send_order').text('Manual Order');
                //                //         $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
                //                //         $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
                //                //     }
                //                // }
                //                if(customerType === 'new')
                //                {
                //                    if(total_bill > newCustomerConfigVal)
                //                    {
                //                        $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                //                        $('section.content #order_process_content').find('button.advance_order')/*.text('Verify Advance Order')*/.hide();
                //                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                //                    }
                //                    else
                //                    {
                //                        $('section.content #order_process_content').find('button.send_order').text('Manual Order');
                //                        $('section.content #order_process_content').find('button.advance_order').text('Advance Order').show();
                //                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
                //                    }
                //                }
                //                var uiModalOrderComplete = $('div[modal-id="manual-order-complete"]').find('h4.order_type');
                //                uiModalOrderComplete.text('Manual Order Complete');
                //            }
                //        }
                //
                //        else if ($('form#coordinator_update_address').is(':visible') == true) //if in update address find RTA
                //        {
                //            if(bAvailability){
                //                //if online
                //                if($('div.rta-container div.rta_store:visible').length > 0)
                //                {
                //                    var sStoreName = $('div.rta-container div.rta_store:visible').find('strong.store_name').text()
                //                    $('div.rta-container div.rta_store:visible').find('strong.store_name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
                //                }
                //
                //                // if(customerType === 'existing')
                //                // {
                //                //     if(total_bill > existingCustomerConfigVal)
                //                //     {
                //                //         $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                //                //         $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
                //                //         $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                //                //     }
                //                //     else
                //                //     {
                //                //         $('section.content #order_process_content').find('button.send_order').text('Send Order');
                //                //         $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
                //                //         $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('23');
                //                //     }
                //                // }
                //                if(customerType === 'new')
                //                {
                //                    if(total_bill > newCustomerConfigVal)
                //                    {
                //                        $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                //                        $('section.content #order_process_content').find('button.advance_order')/*.text('Verify Advance Order')*/.hide();
                //                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                //                    }
                //                    else
                //                    {
                //                        $('section.content #order_process_content').find('button.send_order').text('Send Order');
                //                        $('section.content #order_process_content').find('button.advance_order').text('Advance Order').show();
                //                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
                //                    }
                //                }
                //
                //                var uiModalOrderComplete = $('div[modal-id="manual-order-complete"]').find('h4.order_type');
                //                uiModalOrderComplete.text('Regular Order Complete');
                //            }
                //            else
                //            {
                //                //if offline
                //                if($('div.rta-container div.rta_store:visible').length > 0)
                //                {
                //                    var sStoreName = $('div.rta-container div.rta_store:visible').find('strong.store_name').text()
                //                    sStoreName = sStoreName.replace(' (Offline)', '');
                //                    $('div.rta-container div.rta_store:visible').find('strong.store_name').text(sStoreName + ' (Offline)').css('color', 'red');
                //                }
                //
                //
                //                // if(customerType === 'existing')
                //                // {
                //                //     if(total_bill > existingCustomerConfigVal)
                //                //     {
                //                //         $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                //                //         $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
                //                //         $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                //                //     }
                //                //     else
                //                //     {
                //                //         $('section.content #order_process_content').find('button.send_order').text('Manual Order');
                //                //         $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
                //                //         $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
                //                //     }
                //                // }
                //                if(customerType === 'new')
                //                {
                //                    if(total_bill > newCustomerConfigVal)
                //                    {
                //                        $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                //                        $('section.content #order_process_content').find('button.advance_order')/*.text('Verify Advance Order')*/.hide();
                //                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                //                    }
                //                    else
                //                    {
                //                        $('section.content #order_process_content').find('button.send_order').text('Manual Order');
                //                        $('section.content #order_process_content').find('button.advance_order').text('Advance Order').show();
                //                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
                //                    }
                //                }
                //                var uiModalOrderComplete = $('div[modal-id="manual-order-complete"]').find('h4.order_type');
                //                uiModalOrderComplete.text('Manual Order Complete');
                //            }
                //        }
                //
                //        else if ($('form#coordinator_update_address').is(':visible') == true) //if in update address find RTA
                //        {
                //             if(bAvailability){
                //
                //             }
                //             else
                //             {
                //
                //             }
                //        }
                //        else if($('div[modal-id="process-order"]:visible'))
                //        {
                //             var uiModal = $('div[modal-id="process-order"]');
                //             var uiButton = uiModal.find('button.confirm_process');
                //             if(bAvailability)
                //             {
                //                    var sStore = uiModal.find('span.retail-store-name').text();
                //                    sStore = sStore.replace(' (Offline)', '');
                //                    uiModal.find('span.retail-store-name').text(sStore).css('color', 'black');
                //                    uiButton.text('Send Order');
                //                    uiModal.attr('order_status', '23')
                //             }
                //             else
                //             {
                //                    var sStore = uiModal.find('span.retail-store-name').text();
                //                    sStore += " (Offline)";
                //                    uiModal.find('span.retail-store-name').text(sStore).css('color', 'red');
                //                    uiButton.text('Manual Order');
                //                    uiModal.attr('order_status', '17')
                //             }
                //        }
                //
                //        //alert(JSON.stringify(oData))
                //    }
                //});
                //
                //oSock.bind({
                //    'event'   : 'pusher:member_removed',
                //    'channel' : 'presence-' + iStoreID,
                //    'callback': function (oData) {
                //        var bAvailability = callcenter.gis.check_store_availability(iStoreID);
                //        if ($('section.content div#add_customer_content').is(':visible') == true) //if in add customer find RTA
                //        {
                //            if(bAvailability){
                //                if($('div.rta-address-info-container div.found-retail:visible').length > 0)
                //                {
                //                    var sStoreName = $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text()
                //                    $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
                //                    uiButton.text('Proceed to Order').attr('data-order-rta', iRetailFound);
                //                }
                //            }
                //            else
                //            {
                //                if($('div.rta-address-info-container div.found-retail:visible').length > 0)
                //                {
                //                    var sStoreName = $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text()
                //                    sStoreName = sStoreName.replace(' (Offline)', '');
                //                    $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text(sStoreName + ' (Offline)').css('color', 'red');
                //                    uiButton.text('Proceed to Order (Manual Order)').attr('data-order-rta', iRetailFound);
                //                }
                //            }
                //        }
                //        else if ($('section.content div#search_customer_content').is(':visible') == true) //if in add customer find RTA
                //        {
                //            uiButton = $('section.content div#search_customer_content').find('button.proceed_order_from_search');
                //            uiAddMealButton = $('section.content div#search_customer_content').find('button.add_last_meal_to_order');
                //
                //            if(bAvailability){
                //                if($('div.rta-address-info-container:visible').length > 0)
                //                {
                //                    var sStoreName = $('div.rta-address-info-container:visible').find('span.retail-store-name').text()
                //                    $('div.rta-address-info-container:visible').find('span.retail-store-name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
                //                    uiAddMealButton.text('Add Last Order to Cart').attr('data-order-rta', 1);
                //                    uiButton.text('Proceed to Order').attr('data-order-rta', 1);
                //                }
                //            }
                //            else
                //            {
                //                if($('div.rta-address-info-container:visible').length > 0)
                //                {
                //                    var sStoreName = $('div.rta-address-info-container:visible').find('span.retail-store-name').text()
                //                    sStoreName = sStoreName.replace(' (Offline)', '');
                //                    // console.log()
                //                    $('div.rta-address-info-container:visible').find('span.retail-store-name').text(sStoreName + ' (Offline)').css('color', 'red');
                //                    uiAddMealButton.text('Add Last Order to Cart (Manual Order)').attr('data-order-rta', 4);
                //                    uiButton.text('Proceed to Order (Manual Order)').attr('data-order-rta', 4);
                //                }
                //            }
                //        }
                //
                //        else if($('section.content div#order_process_content').is(':visible') == true)
                //        {
                //            uiButton = $('section.content div#order_process_content').find('button[modal-target="manual-order-summary"]');
                //            var orderType = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_type"]').val()
                //            var customerType = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="customer_type"]').val()
                //            var total_bill = $('div.cart-container table.cart-table-table').find('td.total_bill').attr('data-total-bill');
                //            // var existingCustomerConfigVal = parseInt('2500');
                //            var newCustomerConfigVal = parseInt('2500');
                //            // orderType = iRetailFound;
                //            // $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_type"]').val(iRetailFound);
                //
                //            if(bAvailability){
                //                //if online
                //                if($('div.rta-container div.rta_store:visible').length > 0)
                //                {
                //                    var sStoreName = $('div.rta-container div.rta_store:visible').find('strong.store_name').text()
                //                    $('div.rta-container div.rta_store:visible').find('strong.store_name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
                //                }
                //
                //                // if(customerType === 'existing')
                //                // {
                //                //     if(total_bill > existingCustomerConfigVal)
                //                //     {
                //                //         $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                //                //         $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
                //                //         $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                //                //     }
                //                //     else
                //                //     {
                //                //         $('section.content #order_process_content').find('button.send_order').text('Send Order');
                //                //         $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
                //                //         $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('23');
                //                //     }
                //                // }
                //                if(customerType === 'new')
                //                {
                //                    if(total_bill > newCustomerConfigVal)
                //                    {
                //                        $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                //                        $('section.content #order_process_content').find('button.advance_order')/*.text('Verify Advance Order')*/.hide();
                //                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                //                    }
                //                    else
                //                    {
                //                        $('section.content #order_process_content').find('button.send_order').text('Send Order');
                //                        $('section.content #order_process_content').find('button.advance_order').text('Advance Order').show();
                //                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('23');
                //                    }
                //                }
                //                var uiModalOrderComplete = $('div[modal-id="manual-order-complete"]').find('h4.order_type');
                //                uiModalOrderComplete.text('Regular Order Complete');
                //            }
                //            else
                //            {
                //                //if offline
                //                if($('div.rta-container div.rta_store:visible').length > 0)
                //                {
                //                    var sStoreName = $('div.rta-container div.rta_store:visible').find('strong.store_name').text()
                //                    sStoreName = sStoreName.replace(' (Offline)', '');
                //                    $('div.rta-container div.rta_store:visible').find('strong.store_name').text(sStoreName + ' (Offline)').css('color', 'red');
                //                }
                //
                //
                //                // if(customerType === 'existing')
                //                // {
                //                //     if(total_bill > existingCustomerConfigVal)
                //                //     {
                //                //         $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                //                //         $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
                //                //         $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                //                //     }
                //                //     else
                //                //     {
                //                //         $('section.content #order_process_content').find('button.send_order').text('Manual Order');
                //                //         $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
                //                //         $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
                //                //     }
                //                // }
                //                if(customerType === 'new')
                //                {
                //                    if(total_bill > newCustomerConfigVal)
                //                    {
                //                        $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                //                        $('section.content #order_process_content').find('button.advance_order')/*.text('Verify Advance Order')*/.hide();
                //                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                //                    }
                //                    else
                //                    {
                //                        $('section.content #order_process_content').find('button.send_order').text('Manual Order');
                //                        $('section.content #order_process_content').find('button.advance_order').text('Advance Order').show();
                //                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
                //                    }
                //                }
                //                var uiModalOrderComplete = $('div[modal-id="manual-order-complete"]').find('h4.order_type');
                //                uiModalOrderComplete.text('Manual Order Complete');
                //            }
                //        }
                //
                //        else if ($('form#coordinator_update_address').is(':visible') == true) //if in update address find RTA
                //        {
                //            if(bAvailability){
                //
                //            }
                //            else
                //            {
                //
                //            }
                //        }
                //        else if($('div[modal-id="process-order"]:visible'))
                //        {
                //             var uiModal = $('div[modal-id="process-order"]');
                //             var uiButton = uiModal.find('button.confirm_process');
                //             if(bAvailability)
                //             {
                //                    var sStore = uiModal.find('span.retail-store-name').text();
                //                    sStore = sStore.replace(' (Offline)', '');
                //                    uiModal.find('span.retail-store-name').text(sStore).css('color', 'black');
                //                    uiButton.text('Send Order');
                //                    uiModal.attr('order_status', '23')
                //             }
                //             else
                //             {
                //                    var sStore = uiModal.find('span.retail-store-name').text();
                //                    sStore += " (Offline)";
                //                    uiModal.find('span.retail-store-name').text(sStore).css('color', 'red');
                //                    uiButton.text('Manual Order');
                //                    uiModal.attr('order_status', '17')
                //             }
                //        }
                //    }
                //});

            }


        },
        
        'render_store_messages'       : function (oData) {
            //console.log(oData);
            var uiContainer = $('div.rta_store').find('div.store_messages');
            uiContainer.html("");
            var sHtml = "";
            for (var i in oData) {
                var item = oData[i];
                sHtml +="<div class='notify-msg margin-top-10'>"+ item.message +"</div>";
            }
            uiContainer.append(sHtml);
        },

        'initialize': function () {

            //$('div.error-msg').hide();
            $('div.success-msg').hide();
            $('div.no-search-result').hide();

            var uiButton;
            var uiAddMealButton;
            /*temp*/
            //oSock.bind({
            //    'event'   : 'pusher:member_removed',
            //    'channel' : 'presence-345',
            //    'callback': function (oData) {
            //        var bAvailability = callcenter.gis.check_store_availability(iStoreID);
            //        if ($('section.content div#add_customer_content').is(':visible') == true) //if in add customer find RTA
            //        {
            //            if(bAvailability){
            //                if($('div.rta-address-info-container div.found-retail:visible').length > 0)
            //                {
            //                    var sStoreName = $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text()
            //                    $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
            //                    uiButton.text('Proceed to Order').attr('data-order-rta', iRetailFound);
            //                }
            //            }
            //            else
            //            {
            //                if($('div.rta-address-info-container div.found-retail:visible').length > 0)
            //                {
            //                    var sStoreName = $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text()
            //                    sStoreName = sStoreName.replace(' (Offline)', '');
            //                    $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text(sStoreName + ' (Offline)').css('color', 'red');
            //                    uiButton.text('Proceed to Order (Manual Order)').attr('data-order-rta', iRetailFound);
            //                }
            //            }
            //        }
            //        else if ($('section.content div#search_customer_content').is(':visible') == true) //if in add customer find RTA
            //        {
            //            uiButton = $('section.content div#search_customer_content').find('button.proceed_order_from_search');
            //            uiAddMealButton = $('section.content div#search_customer_content').find('button.add_last_meal_to_order');
            //
            //            if(bAvailability){
            //                if($('div.rta-address-info-container:visible').length > 0)
            //                {
            //                    var sStoreName = $('div.rta-address-info-container:visible').find('span.retail-store-name').text()
            //                    $('div.rta-address-info-container:visible').find('span.retail-store-name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
            //                    uiAddMealButton.text('Add Last Order to Cart').attr('data-order-rta', 1);
            //                    uiButton.text('Proceed to Order').attr('data-order-rta', 1);
            //                }
            //            }
            //            else
            //            {
            //                if($('div.rta-address-info-container:visible').length > 0)
            //                {
            //                    var sStoreName = $('div.rta-address-info-container:visible').find('span.retail-store-name').text()
            //                    sStoreName = sStoreName.replace(' (Offline)', '');
            //                    // console.log()
            //                    $('div.rta-address-info-container:visible').find('span.retail-store-name').text(sStoreName + ' (Offline)').css('color', 'red');
            //                    uiAddMealButton.text('Add Last Order to Cart (Manual Order)').attr('data-order-rta', 4);
            //                    uiButton.text('Proceed to Order (Manual Order)').attr('data-order-rta', 4);
            //                }
            //            }
            //        }
            //
            //        else if($('section.content div#order_process_content').is(':visible') == true)
            //        {
            //            uiButton = $('section.content div#order_process_content').find('button[modal-target="manual-order-summary"]');
            //            var orderType = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_type"]').val()
            //            var customerType = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="customer_type"]').val()
            //            var total_bill = $('div.cart-container table.cart-table-table').find('td.total_bill').attr('data-total-bill');
            //            // var existingCustomerConfigVal = parseInt('2500');
            //            var newCustomerConfigVal = parseInt('2500');
            //            // orderType = iRetailFound;
            //            // $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_type"]').val(iRetailFound);
            //
            //            if(bAvailability){
            //                //if online
            //                if($('div.rta-container div.rta_store:visible').length > 0)
            //                {
            //                    var sStoreName = $('div.rta-container div.rta_store:visible').find('strong.store_name').text()
            //                    $('div.rta-container div.rta_store:visible').find('strong.store_name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
            //                }
            //
            //                // if(customerType === 'existing')
            //                // {
            //                //     if(total_bill > existingCustomerConfigVal)
            //                //     {
            //                //         $('section.content #order_process_content').find('button.send_order').text('Verify Order');
            //                //         $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
            //                //         $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
            //                //     }
            //                //     else
            //                //     {
            //                //         $('section.content #order_process_content').find('button.send_order').text('Send Order');
            //                //         $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
            //                //         $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('23');
            //                //     }
            //                // }
            //                if(customerType === 'new')
            //                {
            //                    if(total_bill > newCustomerConfigVal)
            //                    {
            //                        $('section.content #order_process_content').find('button.send_order').text('Verify Order');
            //                        $('section.content #order_process_content').find('button.advance_order')/*.text('Verify Advance Order')*/.hide();
            //                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
            //                    }
            //                    else
            //                    {
            //                        $('section.content #order_process_content').find('button.send_order').text('Send Order');
            //                        $('section.content #order_process_content').find('button.advance_order').text('Advance Order').show();
            //                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('23');
            //                    }
            //                }
            //                var uiModalOrderComplete = $('div[modal-id="manual-order-complete"]').find('h4.order_type');
            //                uiModalOrderComplete.text('Regular Order Complete');
            //            }
            //            else
            //            {
            //                //if offline
            //                if($('div.rta-container div.rta_store:visible').length > 0)
            //                {
            //                    var sStoreName = $('div.rta-container div.rta_store:visible').find('strong.store_name').text()
            //                    sStoreName = sStoreName.replace(' (Offline)', '');
            //                    $('div.rta-container div.rta_store:visible').find('strong.store_name').text(sStoreName + ' (Offline)').css('color', 'red');
            //                }
            //
            //
            //                // if(customerType === 'existing')
            //                // {
            //                //     if(total_bill > existingCustomerConfigVal)
            //                //     {
            //                //         $('section.content #order_process_content').find('button.send_order').text('Verify Order');
            //                //         $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
            //                //         $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
            //                //     }
            //                //     else
            //                //     {
            //                //         $('section.content #order_process_content').find('button.send_order').text('Manual Order');
            //                //         $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
            //                //         $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
            //                //     }
            //                // }
            //                if(customerType === 'new')
            //                {
            //                    if(total_bill > newCustomerConfigVal)
            //                    {
            //                        $('section.content #order_process_content').find('button.send_order').text('Verify Order');
            //                        $('section.content #order_process_content').find('button.advance_order')/*.text('Verify Advance Order')*/.hide();
            //                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
            //                    }
            //                    else
            //                    {
            //                        $('section.content #order_process_content').find('button.send_order').text('Manual Order');
            //                        $('section.content #order_process_content').find('button.advance_order').text('Advance Order').show();
            //                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
            //                    }
            //                }
            //                var uiModalOrderComplete = $('div[modal-id="manual-order-complete"]').find('h4.order_type');
            //                uiModalOrderComplete.text('Manual Order Complete');
            //            }
            //        }
            //
            //        else if ($('form#coordinator_update_address').is(':visible') == true) //if in update address find RTA
            //        {
            //            if(bAvailability){
            //
            //            }
            //            else
            //            {
            //
            //            }
            //        }
            //
            //        else if($('div[modal-id="process-order"]:visible'))
            //            {
            //                 var uiModal = $('div[modal-id="process-order"]');
            //                 var uiButton = uiModal.find('button.confirm_process');
            //                 if(bAvailability)
            //                 {
            //                        var sStore = uiModal.find('span.retail-store-name').text();
            //                        sStore = sStore.replace(' (Offline)', '');
            //                        uiModal.find('span.retail-store-name').text(sStore).css('color', 'black');
            //                        uiButton.text('Send Order');
            //                        uiModal.attr('order_status', '23')
            //                 }
            //                 else
            //                 {
            //                        var sStore = uiModal.find('span.retail-store-name').text();
            //                        sStore += " (Offline)";
            //                        uiModal.find('span.retail-store-name').text(sStore).css('color', 'red');
            //                        uiButton.text('Manual Order');
            //                        uiModal.attr('order_status', '17')
            //                 }
            //            }
            //    }
            //});
            //
            //oSock.bind({
            //    'event'   : 'pusher:member_added',
            //    'channel' : 'presence-345',
            //    'callback': function (oData) {
            //        var bAvailability = callcenter.gis.check_store_availability(iStoreID);
            //
            //        if ($('section.content div#add_customer_content').is(':visible') == true) //if in add customer find RTA
            //        {
            //            if(bAvailability){
            //                if($('div.rta-address-info-container:visible').length > 0)
            //                {
            //                    var sStoreName = $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text()
            //                    $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
            //                    uiButton.text('Proceed to Order').attr('data-order-rta', 1);
            //                }
            //            }
            //            else
            //            {
            //                if($('div.rta-address-info-container:visible').length > 0)
            //                {
            //                    var sStoreName = $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text()
            //                    sStoreName = sStoreName.replace(' (Offline)', '');
            //                    console.log()
            //                    $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text(sStoreName + ' (Offline)').css('color', 'red');
            //                    uiButton.text('Proceed to Order (Manual Order)').attr('data-order-rta', 4);
            //                }
            //                //manual order
            //            }
            //        }
            //        else if ($('section.content div#search_customer_content').is(':visible') == true) //if in add customer find RTA
            //        {
            //            uiButton = $('section.content div#search_customer_content').find('button.proceed_order_from_search');
            //            uiAddMealButton = $('section.content div#search_customer_content').find('button.add_last_meal_to_order');
            //
            //            if(bAvailability){
            //                if($('div.rta-address-info-container:visible').length > 0)
            //                {
            //                    var sStoreName = $('div.rta-address-info-container:visible').find('span.retail-store-name').text()
            //                    $('div.rta-address-info-container:visible').find('span.retail-store-name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
            //                    uiAddMealButton.text('Add Last Order to Cart').attr('data-order-rta', 1);
            //                    uiButton.text('Proceed to Order').attr('data-order-rta', 1);
            //                }
            //            }
            //            else
            //            {
            //                if($('div.rta-address-info-container:visible').length > 0)
            //                {
            //                    var sStoreName = $('div.rta-address-info-container:visible').find('span.retail-store-name').text()
            //                    sStoreName = sStoreName.replace(' (Offline)', '');
            //                    console.log()
            //                    $('div.rta-address-info-container:visible').find('span.retail-store-name').text(sStoreName + ' (Offline)').css('color', 'red');
            //                    uiAddMealButton.text('Add Last Order to Cart (Manual Order)').attr('data-order-rta', 4);
            //                    uiButton.text('Proceed to Order (Manual Order)').attr('data-order-rta', 4);
            //                }
            //            }
            //        }
            //
            //        else if($('section.content div#order_process_content').is(':visible') == true)
            //        {
            //            uiButton = $('section.content div#order_process_content').find('button[modal-target="manual-order-summary"]');
            //            var orderType = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_type"]').val()
            //            var customerType = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="customer_type"]').val()
            //            var total_bill = $('div.cart-container table.cart-table-table').find('td.total_bill').attr('data-total-bill');
            //            // var existingCustomerConfigVal = parseInt('2500');
            //            var newCustomerConfigVal = parseInt('2500');
            //            // orderType = iRetailFound;
            //            // $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_type"]').val(iRetailFound);
            //
            //            if(bAvailability){
            //                //if online
            //                if($('div.rta-container div.rta_store:visible').length > 0)
            //                {
            //                    var sStoreName = $('div.rta-container div.rta_store:visible').find('strong.store_name').text()
            //                    $('div.rta-container div.rta_store:visible').find('strong.store_name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
            //                }
            //
            //                // if(customerType === 'existing')
            //                // {
            //                //     if(total_bill > existingCustomerConfigVal)
            //                //     {
            //                //         $('section.content #order_process_content').find('button.send_order').text('Verify Order');
            //                //         $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
            //                //         $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
            //                //     }
            //                //     else
            //                //     {
            //                //         $('section.content #order_process_content').find('button.send_order').text('Send Order');
            //                //         $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
            //                //         $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('23');
            //                //     }
            //                // }
            //                if(customerType === 'new')
            //                {
            //                    if(total_bill > newCustomerConfigVal)
            //                    {
            //                        $('section.content #order_process_content').find('button.send_order').text('Verify Order');
            //                        $('section.content #order_process_content').find('button.advance_order')/*.text('Verify Advance Order')*/.hide();
            //                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
            //                    }
            //                    else
            //                    {
            //                        $('section.content #order_process_content').find('button.send_order').text('Send Order');
            //                        $('section.content #order_process_content').find('button.advance_order').text('Advance Order').show();
            //                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
            //                    }
            //                }
            //
            //                var uiModalOrderComplete = $('div[modal-id="manual-order-complete"]').find('h4.order_type');
            //                uiModalOrderComplete.text('Regular Order Complete');
            //            }
            //            else
            //            {
            //                //if offline
            //                if($('div.rta-container div.rta_store:visible').length > 0)
            //                {
            //                    var sStoreName = $('div.rta-container div.rta_store:visible').find('strong.store_name').text()
            //                    sStoreName = sStoreName.replace(' (Offline)', '');
            //                    $('div.rta-container div.rta_store:visible').find('strong.store_name').text(sStoreName + ' (Offline)').css('color', 'red');
            //                }
            //
            //
            //                // if(customerType === 'existing')
            //                // {
            //                //     if(total_bill > existingCustomerConfigVal)
            //                //     {
            //                //         $('section.content #order_process_content').find('button.send_order').text('Verify Order');
            //                //         $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
            //                //         $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
            //                //     }
            //                //     else
            //                //     {
            //                //         $('section.content #order_process_content').find('button.send_order').text('Manual Order');
            //                //         $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
            //                //         $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
            //                //     }
            //                // }
            //                if(customerType === 'new')
            //                {
            //                    if(total_bill > newCustomerConfigVal)
            //                    {
            //                        $('section.content #order_process_content').find('button.send_order').text('Verify Order');
            //                        $('section.content #order_process_content').find('button.advance_order')/*.text('Verify Advance Order')*/.hide();
            //                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
            //                    }
            //                    else
            //                    {
            //                        $('section.content #order_process_content').find('button.send_order').text('Manual Order');
            //                        $('section.content #order_process_content').find('button.advance_order').text('Advance Order').show();
            //                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
            //                    }
            //                }
            //                var uiModalOrderComplete = $('div[modal-id="manual-order-complete"]').find('h4.order_type');
            //                uiModalOrderComplete.text('Manual Order Complete');
            //            }
            //        }
            //
            //        else if ($('form#coordinator_update_address').is(':visible') == true) //if in update address find RTA
            //        {
            //            if(bAvailability){
            //
            //            }
            //            else
            //            {
            //
            //            }
            //        }
            //
            //        else if($('div[modal-id="process-order"]:visible'))
            //            {
            //                 var uiModal = $('div[modal-id="process-order"]');
            //                 var uiButton = uiModal.find('button.confirm_process');
            //                 if(bAvailability)
            //                 {
            //                        var sStore = uiModal.find('span.retail-store-name').text();
            //                        sStore = sStore.replace(' (Offline)', '');
            //                        uiModal.find('span.retail-store-name').text(sStore).css('color', 'black');
            //                        uiButton.text('Send Order');
            //                        uiModal.attr('order_status', '23')
            //                 }
            //                 else
            //                 {
            //                        var sStore = uiModal.find('span.retail-store-name').text();
            //                        sStore += " (Offline)";
            //                        uiModal.find('span.retail-store-name').text(sStore).css('color', 'red');
            //                        uiButton.text('Manual Order');
            //                        uiModal.attr('order_status', '17')
            //                 }
            //            }
            //
            //        //alert(JSON.stringify(oData))
            //    }
            //});

            $('body').on('click', 'div#add_customer_content div.rta-address-info-container button.find_retail', function () {
                //$('button.find_retail').remove();

                var uiContainer = $('div#add_customer_content');

                if(uiContainer.find('input[name="province"]').val().length != 0 && uiContainer.find('input[name="city"]').val().length != 0)
                {
                    callcenter.gis.show_spinner($('button.find_retail'), true);
                    callcenter.gis.find_rta(uiContainer);
                }
                else
                {
                    //callcenter.gis.show_spinner($('button.find_retail'), false);
                }


            });

            $('body').on('click', 'button.find_retail_search', function () {
                callcenter.gis.show_spinner($('button.find_retail_search'), true);
                var uiContainer = $(this).parents('div.search_result_container:first');
                callcenter.gis.find_rta(uiContainer);
            });

            $('body').on('click', 'a.find_retail_search', function () {
                callcenter.gis.show_spinner($('a.find_retail_search'), true);
                var uiContainer = $(this).parents('div.search_result_container:first');
                callcenter.gis.find_rta(uiContainer);
            });

            $('body').on('click', 'div[modal-id="process-order"] a.find_retail_search', function () {
                callcenter.gis.show_spinner($('a.find_retail_search'), true);
                var uiContainer = $(this).parents('div[modal-id="process-order"]:first');
                callcenter.gis.find_rta(uiContainer);
            });

            $('body').on('click', 'div.rta-address-info-container a.show_map', function () {
                // var uiContainer = $('div.rta-address-info-container');
                var uiContainerClass = 'rta-address-info-container';
                var mode = 1;
                callcenter.gis.show_map(mode, uiContainerClass);
            });

            $('body').on('click', 'div.rta-address-info-container button.show_map', function () {
                // var uiContainer = $('div.rta-address-info-container');
                var uiContainerClass = 'rta-address-info-container';
                var mode = 1;
                callcenter.gis.show_map(mode, uiContainerClass);
            });

            //var iStoreID = '225';
            //oSock.bind(
            //    {
            //        'event'   : 'product_unavailability',
            //        'channel' : 'private-' + iStoreID,
            //        'callback': function (sData) {
            //
            //        }
            //    }
            //)

            //oSock.bind(
            //    {
            //    'event'   : 'pusher:member_added',
            //    'channel' : 'presence-' + iStoreID,
            //    'callback': function (oData) {
            //
            //    }
            //});
            //
            //oSock.bind(
            //    {
            //    'event'   : 'pusher:member_removed',
            //    'channel' : 'presence-' + iStoreID,
            //    'callback': function (oData) {
            //
            //    }
            //});

        },

        'find_rta': function (uiContainer, callback) {

            var sStreetName = uiContainer.find('input[name="street"]').val();
            //var sStreetCity			= uiContainer.find('div#city_select input[name="city"]').val();
            var sSubdivisionName = uiContainer.find('input[name="subdivision"]').val();
            //var sSubdivisionCity	= uiContainer.find('div#city_select input[name="city"]').val();
            var sBuildingName = uiContainer.find('input[name="building"]').val();
            //var sBuildingCity		= uiContainer.find('div#city_select input[name="city"]').val();
            var sBarangayName = uiContainer.find('input[name="barangay"]').val();
            //var sBarangayCity 		= uiContainer.find('div#city_select input[name="city"]').val();
            // alert('sBuildingCity ' + sBuildingCity);

            var sCity = uiContainer.find('input[name="city"]').val();

            var street1 = 0;
            var street2 = 0;
            var building = 0;
            var subdivision = 0;
            var barangay = 0;

            var check_street1 = 0;
            var check_street2 = 1;
            var check_building = 0;
            var check_subdivision = 0;
            var check_barangay = 0;
            
            if(sStreetName !== undefined)
            {
                if (sStreetName.length > 2) {

                var oParams = {
                    "city": sCity,
                    "name": sStreetName,
                    "type": 2
                };

                callcenter.gis.find_rta_data(oParams, function (output) {
                    // here you use the output
                    street1 = output;
                    check_street1 = 1;
                });
            } else {
                check_street1 = 1;
            }    
            }
            
            if(typeof(sSubdivisionName) != 'undefined')
            {
                if (sSubdivisionName.length > 2) {

                var oParams = {
                    "city": sCity,
                    "name": sSubdivisionName,
                    "type": 3
                };

                callcenter.gis.find_rta_data(oParams, function (output) {
                    // here you use the output
                    subdivision = output;
                    check_subdivision = 1;
                });
            } else {
                check_subdivision = 1;
            }    
            }    
            
            if(typeof(sBuildingName) != 'undefined')
            {
                 if (sBuildingName.length > 2) {

                var oParams = {
                    // "city" : 'Manila City - NCR',
                    "city": sCity,
                    "name": sBuildingName,
                    "type": 1
                };

                callcenter.gis.find_rta_data(oParams, function (output) {
                    // here you use the output
                    // alert(output);
                    building = output;
                    check_building = 1;
                });
            } else {
                check_building = 1;
            }   
            }    
            
            if(typeof(sBuildingName) != 'undefined')
            {
            if (sBarangayName.length > 2) {

                var oParams = {
                    "city": sCity,
                    "name": sBarangayName,
                    "type": 1
                };

                callcenter.gis.find_rta_data(oParams, function (output) {
                    // here you use the output
                    barangay = output;
                    check_barangay = 1;
                });
            } else {
                check_barangay = 1;
            }
            }

            /* alert('check_street1 ' + check_street1);
             alert('check_street2 ' + check_street2);
             alert('check_subdivision ' + check_subdivision);
             alert('check_building ' + check_building);
             alert('check_barangay ' + check_barangay); */

            var rta_check_timer = 0;
            rta_check_timer = setInterval(function () {

                if (check_street1 == 1 && check_street2 == 1 && check_subdivision == 1 && check_building == 1 && check_barangay == 1) {
                    clearInterval(rta_check_timer);
                    callcenter.gis.store_scoring_model(street1, street2, building, subdivision, barangay, uiContainer, callback);
                }
            }, 1000);


        },

        'find_rta_data': function (oParams, sReturn) {

            var oAjaxConfig = {
                "type"   : "Post",
                "data"   : oParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : callcenter.config('url.api.jfc.gis') + "/gis/find_rta_data",
                // "url"     : callcenter.config('url.api.jfc.gis') + "/gis/find_rta_data",
                "success": function (oData) {
                    // //console.log(oData);
                    if (typeof(oData) != 'undefined') {
                        if (oData.status == false && count(oData.message) > 0) //failed
                        {
                            /* var uiContainer = $('div.address-error-msg');
                             var sError = '';
                             $.each(oData.message, function (k, v) {
                             sError += '<p class="error_messages"> ' + v + '</p>';
                             });
                             callcenter.gis.add_error_message(sError, uiContainer); */
                        }
                        else {
                            sReturn(oData.data);
                            cr8v.add_to_storage('rta_data', oData.data);
                            //return sStreetData;
                        }
                        // callcenter.gis.show_spinner($('button.add_address_button'), false);
                    }
                }
            };

            callcenter.gis.get(oAjaxConfig)
        },

        'store_scoring_model': function (street1, street2, building, subdivision, barangay, uiContainer, callback) {
            //alert('building ' + building);


            /* if(check_building == 1 && check_street2 == 1 && check_street1 == 1 && check_subdivision == 1 && check_barangay == 1)
             { */

            if (street1 != 0) {
                var rta_data1 = street1.split(":");
                var store1 = rta_data1[0];
            }
            else {
                var store1 = 0;
            }
            if (street2 != 0) {
                var rta_data2 = street2.split(":");
                var store2 = rta_data2[0];
            }
            else {
                var store2 = 0
            }
            if (building != 0) {
                var rta_data3 = building.split(":");
                var store3 = rta_data3[0];
            }
            else {
                var store3 = 0;
            }

            if (subdivision != 0) {
                var rta_data4 = subdivision.split(":");
                var store4 = rta_data4[0];
            }
            else {
                var store4 = 0;
            }

            if (barangay != 0) {
                var rta_data5 = barangay.split(":");
                var store5 = rta_data5[0];
            }
            else {
                var store5 = 0;
            }

            var oParams = {
                'param_name_1': store1,
                'param_name_2': store2,
                'param_name_3': store3,
                'param_name_4': store4,
                'param_name_5': store5
            }
            var oAjaxConfig = {
                "type"   : "Post",
                "data"   : oParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : callcenter.config('url.api.jfc.gis') + "/gis/store_scoring_model",
                // "url"     : callcenter.config('url.api.jfc.gis') + "/gis/store_scoring_model",
                "error" : function () {
                    callcenter.gis.show_spinner($('button.find_retail_search'), false);        
                },
                "success": function (oData) {
                    // //console.log(oData);
                    if (typeof(oData) != 'undefined') {
                        callcenter.gis.show_spinner($('button.find_retail_search'), false);
                        callcenter.gis.show_spinner($('button.find_retail'), false);
                        callcenter.gis.show_spinner($('a.find_retail_search'), false);
                        if (oData.status == false && count(oData.message) > 0) //failed
                        {
                            /* var uiContainer = $('div.address-error-msg');
                             var sError = '';
                             $.each(oData.message, function (k, v) {
                             sError += '<p class="error_messages"> ' + v + '</p>';
                             });
                             callcenter.gis.add_error_message(sError, uiContainer); */
                        }
                        else {
                            // oData.data;
                            //return sStreetData;

                            // $("div.find-retail").hide();
                            if(typeof(callback) == 'function')
                            {
                                var rta_data_all = [{rta_data1, rta_data2, rta_data3, rta_data4, rta_data5}];
                                var store_data_all = [{store1, store2, store3, store4, store5}];
                                callback(oData, rta_data_all, store_data_all);
                            }
                            
                            uiContainer.find("div.find-rta-container").hide();
                            uiContainer.find("div.found-retail").show();

                            if (oData.message != 0) {
                                if (oData.message == store3) {
                                    if (oData.message != 1) {
                                        var bAvailability = callcenter.gis.check_store_availability(oData.data.store_id);
                                        if (bAvailability) {
                                            uiContainer.find(".retail-store-name").html(oData.data.name).attr({
                                                'store_code': oData.data.code,
                                                'store_id'  : oData.data.store_id,
                                                'store_name': oData.data.name,
                                                'store_time': rta_data3[2]
                                            }).css('color', 'black');
                                            callcenter.gis.rta_return(1, oData.data.store_id, rta_data3[2]);
                                            //for the rendering of store messages
                                            callcenter.gis.render_store_messages(oData.data_store_messages);
                                        }
                                        else {
                                            uiContainer.find(".retail-store-name").html(oData.data.name + " (Offline)").attr({
                                                'store_code': oData.data.code,
                                                'store_id'  : oData.data.store_id,
                                                'store_name': oData.data.name,
                                                'store_time': rta_data3[2]
                                            }).css('color', 'red');
                                            callcenter.gis.rta_return(4, oData.data.store_id, rta_data3[2]);
                                            //for the rendering of store messages
                                            callcenter.gis.render_store_messages(oData.data_store_messages);
                                        }

                                    } else {
                                        uiContainer.find(".retail-store-name").html('Delivery Not Available This Time');
                                        callcenter.gis.rta_return(4);
                                    }
                                    uiContainer.find(".retail-store-time-label").html("Delivery Time:");
                                    uiContainer.find(".retail-store-time").html(rta_data3[2] + ' Minutes');
                                }
                                else if (oData.message == store1) {
                                    if (oData.message != 1) {
                                        var bAvailability = callcenter.gis.check_store_availability(oData.data.store_id);
                                        if (bAvailability) {
                                            uiContainer.find(".retail-store-name").html(oData.data.name).attr({
                                                'store_code': oData.data.code,
                                                'store_id'  : oData.data.store_id,
                                                'store_name': oData.data.name,
                                                'store_time': rta_data1[2]
                                            }).css('color', 'black');
                                            callcenter.gis.rta_return(1, oData.data.store_id, rta_data1[2])
                                        }
                                        else {
                                            uiContainer.find(".retail-store-name").html(oData.data.name + " (Offline)").attr({
                                                'store_code': oData.data.code,
                                                'store_id'  : oData.data.store_id,
                                                'store_name': oData.data.name,
                                                'store_time': rta_data1[2]
                                            }).css('color', 'red');
                                            callcenter.gis.rta_return(4, oData.data.store_id, rta_data1[2])
                                        }
                                    } else {
                                        uiContainer.find(".retail-store-name").html('Delivery Not Available This Time');
                                        callcenter.gis.rta_return(4)
                                    }
                                    uiContainer.find(".retail-store-time-label").html("Delivery Time:");
                                    uiContainer.find(".retail-store-time").html(rta_data1[2] + ' Minutes');
                                }
                                else if (oData.message == store2) {
                                    if (oData.message != 1) {
                                        var bAvailability = callcenter.gis.check_store_availability(oData.data.store_id);
                                        if (bAvailability) {
                                            uiContainer.find(".retail-store-name").html(oData.data.name).attr({
                                                'store_code': oData.data.code,
                                                'store_id'  : oData.data.store_id,
                                                'store_name': oData.data.name,
                                                'store_time': rta_data2[2]
                                            }).css('color', 'black');
                                            callcenter.gis.rta_return(1, oData.data.store_id, rta_data2[2])
                                        }
                                        else {
                                            uiContainer.find(".retail-store-name").html(oData.data.name + " (Offline)").attr({
                                                'store_code': oData.data.code,
                                                'store_id'  : oData.data.store_id,
                                                'store_name': oData.data.name,
                                                'store_time': rta_data2[2]
                                            }).css('color', 'red');
                                            callcenter.gis.rta_return(4, oData.data.store_id)
                                        }
                                    } else {
                                        uiContainer.find(".retail-store-name").html('Delivery Not Available This Time');
                                        callcenter.gis.rta_return(4)
                                    }
                                    uiContainer.find(".retail-store-time-label").html("Delivery Time:");
                                    uiContainer.find(".retail-store-time").html(rta_data2[2] + ' Minutes');
                                }
                                else if (oData.message == store4) {
                                    if (oData.message != 1) {
                                        var bAvailability = callcenter.gis.check_store_availability(oData.data.store_id);
                                        if (bAvailability) {
                                            uiContainer.find(".retail-store-name").html(oData.data.name).attr({
                                                'store_code': oData.data.code,
                                                'store_id'  : oData.data.store_id,
                                                'store_name': oData.data.name,
                                                'store_time': rta_data4[2]
                                            }).css('color', 'black');
                                            callcenter.gis.rta_return(1, oData.data.store_id, rta_data4[2])
                                        }
                                        else {
                                            uiContainer.find(".retail-store-name").html(oData.data.name + " (Offline)").attr({
                                                'store_code': oData.data.code,
                                                'store_id'  : oData.data.store_id,
                                                'store_name': oData.data.name,
                                                'store_time': rta_data4[2]
                                            }).css('color', 'red');
                                            callcenter.gis.rta_return(4, oData.data.store_id, rta_data4[2])
                                        }
                                    } else {
                                        uiContainer.find(".retail-store-name").html('Delivery Not Available This Time');
                                        callcenter.gis.rta_return(4)
                                    }
                                    uiContainer.find(".retail-store-time-label").html("Delivery Time:");
                                    uiContainer.find(".retail-store-time").html(rta_data4[2] + ' Minutes');
                                }
                                else if (oData.message == store5) {
                                    if (oData.message != 1) {
                                        var bAvailability = callcenter.gis.check_store_availability(oData.data.store_id);
                                        if (bAvailability) {
                                            uiContainer.find(".retail-store-name").html(oData.data.name).attr({
                                                'store_code': oData.data.code,
                                                'store_id'  : oData.data.store_id,
                                                'store_name': oData.data.name,
                                                'store_time': rta_data5[2]
                                            }).css('color', 'black');
                                            callcenter.gis.rta_return(1, oData.data.store_id, rta_data5[2])
                                        }
                                        else {
                                            uiContainer.find(".retail-store-name").html(oData.data.name + " (Offline)").attr({
                                                'store_code': oData.data.code,
                                                'store_id'  : oData.data.store_id,
                                                'store_name': oData.data.name,
                                                'store_time': rta_data5[2]
                                            }).css('color', 'red');
                                            callcenter.gis.rta_return(4, oData.data.store_id, rta_data5[2])
                                        }


                                    } else {
                                        uiContainer.find(".retail-store-name").html('Delivery Not Available This Time');
                                        callcenter.gis.rta_return(4)
                                    }
                                    uiContainer.find(".retail-store-time-label").html("Delivery Time:");
                                    uiContainer.find(".retail-store-time").html(rta_data5[2] + ' Minutes');
                                }
                                else {
                                    uiContainer.find(".retail-store-name").html('RTA Data Conflict');
                                    uiContainer.find(".retail-store-time").html("");
                                    uiContainer.find(".retail-store-time-label").html("");
                                    callcenter.gis.rta_return(6)

                                }
                            }
                            else {
                                uiContainer.find(".retail-store-name").html('Out of Delivery Area').removeAttr('store_code store_id store_name store_time').css('color', 'black');
                                uiContainer.find(".retail-store-time").html("");
                                uiContainer.find(".retail-store-time-label").html("");
                                callcenter.gis.rta_return(5)
                            }


                        }


                    }
                }
            };

            callcenter.gis.get(oAjaxConfig)

            // }

        },
        'show_map'           : function (mode, uiContainer) {

            var curHOur = '22:10:30';
            var arrHour = curHOur.split(':');
            window.open('http://173.255.206.64/leaflet/index.php?c=gis_' + iUserId + '&m=' + mode + '&s=' + iSBUId + '&p=' + iProvinceId + '&t=' + arrHour[0] + '&uiclass=' + uiContainer, 'Maps', 'width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=400,top=100');

        },
        'gis_new_data'       : function (data) {

            var coor = data.split(",");

            var uiContainer = $('div.' + coor[4]);
            //var uiContainer = $('div.rta-address-info-container');

            var oParams = {'coor_x': coor[0], 'coor_y': coor[1]};

            if (coor[2] == '1') {
                var oAjaxConfig = {
                    "type"   : "Post",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.gis') + "/gis/find_rta_map",
                    // "url"     : callcenter.config('url.api.jfc.gis') + "/gis/find_rta_map",
                    "success": function (oData) {
                        // //console.log(oData);
                        if (typeof(oData) != 'undefined') {
                            if (oData.status == false && count(oData.message) > 0) //failed
                            {
                                /* var uiContainer = $('div.address-error-msg');
                                 var sError = '';
                                 $.each(oData.message, function (k, v) {
                                 sError += '<p class="error_messages"> ' + v + '</p>';
                                 });
                                 callcenter.gis.add_error_message(sError, uiContainer); */
                            }
                            else {
                                //console.log(oData.data);

                                var rta_data = oData.message.split(":");
                                if (rta_data[0] != 0) {
                                    /* if(rta_data[0] != 1)
                                     $("#rta_id").html('<option value="'+rta_data[0]+':'+rta_data[1]+':'+rta_data[3]+'">'+client_data_name[rta_data[0]]+' - GRID:'+rta_data[1]+' LAT/LNG:'+rta_data[3]+'</option>');
                                     else
                                     $("#rta_id").html('<option value="'+rta_data[0]+':'+rta_data[1]+':'+rta_data[3]+'">Delivery Not Available This Time</option>');
                                     $("#dstl_id").val(rta_data[2]); */


                                    if (rta_data[0] != 1) {
                                        uiContainer.find(".retail-store-name").html(oData.data.name);
                                    } else {
                                        uiContainer.find(".retail-store-name").html('Delivery Not Available This Time');
                                    }
                                    uiContainer.find(".retail-store-time-label").html("Delivery Time:");
                                    uiContainer.find(".retail-store-time").html(rta_data3[2] + ' Minutes');


                                }
                                else {
                                    uiContainer.find(".retail-store-name").html('Out of Delivery Area');
                                    uiContainer.find(".retail-store-time").html("");
                                    uiContainer.find(".retail-store-time-label").html("");
                                }
                            }
                            // callcenter.gis.show_spinner($('button.add_address_button'), false);
                        }
                    }
                };

                callcenter.gis.get(oAjaxConfig)
            }

        },
        'find_rta_map'       : function (coor_x, coor_y) {

        },
        'show_spinner'       : function (uiElem, bShow) {
            if (bShow == true) {
                uiElem.find('i.fa-spinner').removeClass('hidden');
                uiElem.prop('disabled', true);
            }
            else {
                uiElem.find('i.fa-spinner').addClass('hidden');
                uiElem.prop('disabled', false);
            }
        },

        'get': function (oAjaxConfig) {
            if (typeof(oAjaxConfig) != 'undefined') {
                callcenter.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        /*check store online offline*/

        /**
         * @description : this function will check for the store id if it is exisitng or subscribed ito one of the presence channel, meaning it is online, will stop loop once store is found
         * @author : lorenz
         * @params : iStoreID (int)
         * @return : bOnlineStore (bool)
         * */

        'check_store_availability': function (iStoreID) {
            var bOnlineStore = false;
            if (typeof(iStoreID) && typeof(oPusher) != undefined) {
                console.log('checking store availability', iStoreID);
            }

            $.each(oPusher.channels.channels, function (channel_name, channel) {
                if (channel_name == 'presence-ironman') {

                    channel.members.each(function (member) {
                        if (member.info.hasOwnProperty('store_id')) //if this member is a store
                        {
                            if (member.info.store_id == iStoreID) {
                                bOnlineStore = true;
                                return false;
                            }
                        }
                    });
                }
            })
            console.log(bOnlineStore)
            return bOnlineStore;
        },


        /**
         * @description : this function will fetch for core products and will store unavailable core product ids in the cr8v storage as 'unavailable_core_id'
         * @author : lorenz
         * @params : iStoreID(int)
         * @return : undefined
         * */
        'fetch_product_unavailability': function (iStoreID) {

            if (typeof(iStoreID) != 'undefined') {
                var oParams = {
                    "sbu_id": "1"
                }

                var aUnavailableCoreIds = [];
                var aUnavailableCoreProducts = [];
                var aCoreProducts = [];

                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.products') + "/products/jollibee/core",
                    "success": function (oData) {
                        if (typeof(oData) != 'undefined') {
                            //console.log(oData);
                            $.each(oData, function (key, core) {
                                if (core.unavailable != 'undefined') {
                                    if (core.unavailable == "1") {
                                        aUnavailableCoreIds.push(core.id);
                                        aUnavailableCoreProducts.push({ 'id':core.id , 'name':core.name });
                                    }
                                    aCoreProducts.push({ 'id':core.id , 'name':core.name });
                                }
                            });

                            if(typeof(cr8v.get_from_storage('unavailable_core_id', '') != 'undefined'))
                            {
                                var aUnavalable = cr8v.get_from_storage('unavailable_core_id', '');
                                if(typeof(aUnavalable) != 'undefined')
                                {
                                    cr8v.pop('unavailable_core_id');
                                }

                            }
                            cr8v.add_to_storage('unavailable_core_id', aUnavailableCoreIds);
                            cr8v.add_to_storage('unavailable_core_products', aUnavailableCoreProducts);
                            cr8v.add_to_storage('core_products', aCoreProducts);
                            cr8v.gis.render_unavailable_products();
                        }
                    }
                };

                callcenter.CconnectionDetector.ajax(oAjaxConfig)


            }
        },
        'show_spinner': function (uiElem, bShow) {
            if (bShow == true) {
                uiElem.find('i.fa-spinner').removeClass('hidden');
                uiElem.prop('disabled', true);
            }
            else {
                uiElem.find('i.fa-spinner').addClass('hidden');
                uiElem.prop('disabled', false);
            }
        },

        'render_unavailable_products': function () {
            var arUnavailableProducts =   cr8v.get_from_storage('unavailable_core_products', '');
            //console.log(arUnavailableProducts);
            if(typeof(arUnavailableProducts)!="undefined"){
            var uiContainer = $('div.rta_store div.unavailable_products').find('div.unavailable_products_container');
            uiContainer.html("");     
               if(arUnavailableProducts[0].length!==0){
                        
                        uiContainer.parent("div.unavailable_products").removeClass("hidden");
                        var sHtml = "";

                        $.each(arUnavailableProducts[0], function(key,value) {
                                    sHtml +="<div class='notify-msg margin-top-10' data-core-id='"+value.id+"'>"+ value.name +" (Unavailable)</div>";
                        });
                        uiContainer.append(sHtml);
                        }else{
                         uiContainer.parent("div.unavailable_products").removeClass("hidden");
                        }       
               }
            
            

            
        }

    }

}());

$(window).load(function () {
    callcenter.gis.initialize();
});


