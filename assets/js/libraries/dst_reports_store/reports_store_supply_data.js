/**
 * Created by Developer on 10/27/2015.
 */
(function () {
    "use strict";

    var $dstForStoreTable,
        $dstForStoreTableDataContainer,
        $dstForStoreNoDataContainer,
        $sectionDstForStore,
        $sectionDstForStoreViewData,
        $dstForStoreTableOrdersTemplate,
        $dstForStoreTableRowTemplate,
        $dstReportsForStoresOrdersContainer;

    var warn = (console.warn) ? console.warn : console.log;

    var DEFAULT_FORMATTED_DATE = 'MMMM DD, YYYY',
        DISPLAY_FORMATTED_DATE = 'MMMM DD, YYYY | HH:mm:ss';

    function addPercentSign(sNo) {
        sNo = parseFloat(sNo).toFixed(2);
        if(! /\%/.test(sNo)) {
            sNo = sNo + '%';
        }

        return sNo;
    }

    function addMoneySign(sMoney) {
        return sMoney + ' PHP';
    }

    function changeDateFormat(sDate, sFormat) {
        sFormat = (sFormat) ? sFormat : DEFAULT_FORMATTED_DATE;

        return moment(sDate).format(sFormat);
    }

    function validateStoreInfo(oStoreInfo) {
        if(! oStoreInfo.hasOwnProperty('id')) {
            warn('Store ID must be present');
        }

        if( ! oStoreInfo.hasOwnProperty('name')) {
            warn('Store Name must be present')
        }
    }

    function validateOrderInfo(oStoreOrderInfo) {

    }

    function scrollToTop() {
        $(document).scrollTop(0);
    }

    function parseDisplayData(oOrder) {
        return $.parseJSON(oOrder);
    }

    function datepicker(){
        if ($().datetimepicker !== undefined) {
            var dateTimePickerOptions = {
                format  : 'MMMM DD, YYYY',
                pickTime: false
            };

            var datepickerDateFrom = $('#dst_for_store_date_from_dp')
            var datepickerDateTo = $('#dst_for_store_date_to_dp')
            //set date picker options
            datepickerDateFrom.datetimepicker(dateTimePickerOptions);
            datepickerDateTo.datetimepicker(dateTimePickerOptions);

            //change min date of date to not allow to pass
            datepickerDateFrom.on("dp.change", function (e) {
                datepickerDateTo.data('DateTimePicker').setMinDate(e.date);
            });

            datepickerDateTo.on("dp.change", function (e) {
                datepickerDateFrom.data('DateTimePicker').setMaxDate(e.date);
            });
        }
    }

    CPlatform.prototype.reports_store_supply_data = {
        initialize: function () {
            $dstForStoreTable = $('#dst_for_store_table');
            $dstForStoreTableDataContainer = $('#dst_for_store_table_data_container');
            $dstForStoreNoDataContainer = $('#dst_for_store_no_data_container');
            $sectionDstForStore = $('#dst_for_store');
            $sectionDstForStoreViewData = $('#dst_for_store_view_data');
            $dstForStoreTableOrdersTemplate = $('#dst_reports_for_stores_orders_template');
            $dstForStoreTableRowTemplate = $('tr[data-template="dst_for_store_one_row"]');
            $dstReportsForStoresOrdersContainer = $('#dst_reports_for_stores_orders_container');

            /*$('table#dst_for_store_table').on('click', 'a.viewdata', function() {

            })*/


            $('body').on("click", '.modal-trigger',function(){
                $("body").css({overflow:'hidden'});
                var tm = $(this).attr("modal-target");

                $("div[modal-id~='"+tm+"']").addClass("showed");

                $("div[modal-id~='"+tm+"'] .close-me").on("click",function(){
                    $("body").css({'overflow-y':'initial'});
                    $("div[modal-id~='"+tm+"']").removeClass("showed");
                });

                // click outside to modal
                // $("div[modal-id~='"+tm+"']").on("mouseup",function(e){
                // 	var md = $("div[modal-id~='"+tm+"'] .modal-body")

                // 	if(!md.is(e.target) && md.has(e.target).length === 0){
                // 		$(this).removeClass("showed");
                // 	}
                // });
            });

            $('div#dst_reports_for_stores_orders_container').on('click', 'button.transaction-logs', function (e) {
                var iOrderID = $(this).parents('section.section-order').attr('data-order-id')
                console.log(iOrderID);

                var uiAgentLogsContainer = $('div[modal-id="order-logs"]').find('tbody.logs_container');
                uiAgentLogsContainer.find('tr:not(.has_spinner)').remove();
                uiAgentLogsContainer.find('tr.has_spinner i.fa-spinner').removeClass('hidden');
                uiAgentLogsContainer.find('tr.has_spinner').removeClass('hidden');

                var oParams = {
                    "params": {
                        "where": [
                            {
                                'field'   : 'opl.order_id',
                                'operator': '=',
                                'value'   : iOrderID,
                            }
                        ],
                        "limit" : 9999999999999
                    }
                }

                var oAjaxConfig = {
                    'type'   : "GET",
                    'headers': {"X-API-KEY": "IEYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    'url'    : admin.config('url.api.jfc.orders') + "/orders/logs",
                    'data'   : oParams,
                    'success': function (oData) {
                        //console.log(oData);
                        var sAgentLogsHtml = '';
                        var sStoreLogsHtml = '';
                        var uiAgentLogsContainer = $('div[modal-id="order-logs"]').find('tbody.logs_container');
                        
                        uiAgentLogsContainer.find('tr:not(.has_spinner)').remove();
                        uiAgentLogsContainer.find('tr.has_spinner i.fa-spinner').addClass('hidden');
                        uiAgentLogsContainer.find('tr.has_spinner').addClass('hidden');

                        if (typeof(oData) !== 'undefined') {
                            $.each(oData.data, function (key, log) {
                                var sAgentLogsHtml = '';
                                var date_processed_diff = '';
                                // if(key > 0)
                                // {
                                //     var date_processed_prev = oData.data[key - 1].date_processed;
                                //     var d1 = moment(date_processed_prev).format('MM-DD-YYYY H:mm');
                                //     var d2 = moment(log.date_processed).format('MM-DD-YYYY H:mm');
                                //     var iMinDiff = moment(d2, 'MM-DD-YYYY H:mm').diff(moment(d1, 'MM-DD-YYYY H:mm'), 'minutes');
                                
                                //     date_processed_diff = iMinDiff;
                                
                                //     if(date_processed_diff > 1)
                                //     {
                                //         date_processed_diff = '| '+ date_processed_diff +' mins';
                                //     }
                                //     else
                                //     {
                                //         date_processed_diff = '| '+ date_processed_diff +' min';
                                //     }
                                // }

                                var uiPreviousTr = uiAgentLogsContainer.find('tr:not(.has_spinner):not([is_followup="1"]):last');
                                if(uiPreviousTr.length > 0 && log.action !== 'Followed up Order')
                                {
                                    var date_processed_prev = uiPreviousTr.attr('date_processed');
                                    var d1 = moment(date_processed_prev).format('MM-DD-YYYY H:mm');
                                    var d2 = moment(log.date_processed).format('MM-DD-YYYY H:mm');
                                    var iMinDiff = moment(d2, 'MM-DD-YYYY H:mm').diff(moment(d1, 'MM-DD-YYYY H:mm'), 'minutes');
                                    
                                    date_processed_diff = iMinDiff

                                    if(date_processed_diff > 1)
                                    {
                                        date_processed_diff = '| '+ date_processed_diff +' mins';
                                    }
                                    else
                                    {
                                        date_processed_diff = '| '+ date_processed_diff +' min';
                                    }
                                }

                                var isFollowUp = 0;
                                if(log.action == 'Followed up Order')
                                {
                                    isFollowUp = 1;
                                }

                                if (log.type == 1 || log.type == 2) {
                                    sAgentLogsHtml = '<tr is_followup="'+isFollowUp+'" date_processed="'+log.date_processed+'"><td>'
                                     + log.action + '</td><td class="text-center">' + moment(log.date_processed).format('MM-DD-YYYY | hh:mm a ') + date_processed_diff + '</td><td class="text-center">' 
                                     + log.full_name + '</td>';
                                }
                                else if (log.type == 3) {
                                    sAgentLogsHtml = '<tr is_followup="'+isFollowUp+'" date_processed="'+log.date_processed+'"><td>'
                                     + log.action + '</td><td class="text-center">' + moment(log.date_processed).format('MM-DD-YYYY | hh:mm a ') + date_processed_diff + '</td><td class="text-center">' 
                                     + log.username.toUpperCase() + '</td></tr>'
                                }
                                uiAgentLogsContainer.append(sAgentLogsHtml);
                            })

                            $('div[modal-id="order-logs"]').addClass('showed');
                            //uiStoreLogsContainer.html("").append(sStoreLogsHtml);
                        }
                    }
                }
                admin.CconnectionDetector.ajax(oAjaxConfig);
            })

            datepicker();

            $('div#dst_reports_for_stores_orders_container').on('click', 'section.section-order', function () {
                $(this).find('div.order-overview').toggleClass('hidden')
                $(this).find('div.order-detailed').toggleClass('hidden')
            });

            $('div.sort-container').on('click', 'strong.dst-sort-store', function() {
                var uiOrderContainers = $('div#dst_reports_for_stores_orders_container');
                var sSortKey = $(this).attr('sort');
                var iSortDirectionAsc;
                var iSortDirectionDesc;

                $(this).toggleClass('asc desc');

                if($(this).hasClass('asc') == true)
                {
                    iSortDirectionAsc = -1;
                    iSortDirectionDesc = 1;
                    $(this).siblings('img.asc').removeClass('hidden');
                    $(this).siblings('img.desc').addClass('hidden');
                }
                else
                {
                    iSortDirectionAsc = 1;
                    iSortDirectionDesc = -1;
                    $(this).siblings('img.asc').addClass('hidden');
                    $(this).siblings('img.desc').removeClass('hidden');
                }

                if(typeof(uiOrderContainers) != 'undefined')
                {
                    $.each(uiOrderContainers, function(key, container) {
                        $(container).children('section.section-order:not(.template)').sortDomElements(function(a,b){
                            var akey = $(a).attr(sSortKey);
                            var bkey = $(b).attr(sSortKey);
                            if (akey == bkey) return 0;
                            if (akey < bkey) return iSortDirectionAsc;
                            if (akey > bkey) return iSortDirectionDesc;
                        })
                    })
                }

            });

            $('section#dst_for_store_view_data').on('click', 'li.page', function() {
                var uiThis = $(this);
                var _view_data = admin.reports_store_supply_data.view_data;
                if(uiThis.hasClass('active') == false)
                {

                    uiThis.siblings('li.page').removeClass('page-active');

                    var iStart = uiThis.attr('page-start');
                    var iEnd = uiThis.attr('page-end');

                    var oStoredOrders = cr8v.get_from_storage('dst_orders');
                    oStoredOrders = oStoredOrders.filter(function( el,i) {
                        return i >= iStart &&  i <= iEnd;
                    })

                     _view_data.clear();
                    $('tbody.dst_reports_specific_content_table_container').find('tr.detailed:not(.template)').remove();
                    for(var i = 0; i < oStoredOrders.length; i++) {
                        admin.reports_store_supply_data.view_data.supply_append_orders(oStoredOrders[i]);
                    }
                    //uiThis.addClass('page-active')
                    $('div#dst_for_store_view_data').find('li.page[page-start="'+iStart+'"][page-end="'+iEnd+'"]').addClass('page-active');

                }

            });
        },

        get_formatted_date: function (sDate, sFormat) {
            return changeDateFormat(sDate, sFormat);
        },

        render_pagination : function(oOrders) {

            var uiOrdersContainer = $('div#dst_for_store_view_data');
            var iPageCount = oOrders.length / 10;
            var uiPaginationContainer = uiOrdersContainer.find('ul.paganation-3');
            var uiPaginationContainerHidden = $('ul.paganations-3-hidden');
            var sPaginationHtmlhidden = '<li class="page-prev"><i class="fa fa-angle-left"></i></li>';
            var sPaginationHtml = '<li class="page-prev"><i class="fa fa-angle-left"></i></li>';
            iPageCount += 1;

            if (iPageCount > 0) {
                for (i = 0; i < parseInt(iPageCount); i++) {
                    var iPage = i + 1;
                    var iStart, iEnd;
                    if (i == 0) {
                        iStart = 0;
                        iEnd = 10;
                    }
                    else {
                        iStart = i * 10;
                        iEnd = ((i + 1) * 10);
                    }


                    sPaginationHtmlhidden += '<li class="page" order="" page-start="' + iStart + '" page-end="' + iEnd + '" page-end="50" data-no="' + iPage + '">' + iPage + '</li>';
                    sPaginationHtml += '<li class="pages order" page-start="' + iStart + '" page-end="' + iEnd + '">' + iPage + '</li>';
                }
            }
            else {
                sPaginationHtml += '<li class="page page-active" page-start="0" page-end="50" data-no="1">1</li>';
            }

            uiPaginationContainer.html('');
            uiPaginationContainer.append(sPaginationHtml);
            uiPaginationContainerHidden.html('');
            uiPaginationContainerHidden.append(sPaginationHtmlhidden)

            $('#pagination-3').twbsPagination({
                totalPages  : iPageCount,
                visiblePages: 15,
                onPageClick : function (event, page) {
                   $('li.page[order][data-no="' + page + '"').trigger('click');
                }
            });
        },

        view_data: {
            show: function () {
                $sectionDstForStoreViewData.removeClass('hidden');
                $sectionDstForStore.addClass('hidden');
            },
            change_date_range: function () {
                var oGettedDateFormatted = admin.reports_store_core.get_date_range();

                $sectionDstForStoreViewData.find('[data-label="date_from"]').text(oGettedDateFormatted.dateFrom);
                $sectionDstForStoreViewData.find('[data-label="date_to"]').text(oGettedDateFormatted.dateTo);
            },
            change_total_hitrate_per: function () {
                var oTotalPercentage = admin.reports_store_core.get_grand_total();

                $sectionDstForStoreViewData.find('[data-label="total_hitrate_percentage"]')
                    .text(addPercentSign(oTotalPercentage.grand_total_hitrate_per));
            },


            supply_append_orders: function (order) {
                //var $clonedOrderTemplate = $dstForStoreTableOrdersTemplate.clone()
                //                            .removeAttr('id').removeClass('template'),
                //    oDisplayData = parseDisplayData(oOrder.display_data);
                if (typeof(order) != 'undefined') {
                    var oOrderTemplate = $('section.section-order.template').eq(0).clone().removeClass('template');
                    //oDisplayData = parseDisplayData(oOrder.display_data);



                    //$clonedOrderTemplate.find('[data-label="name"]').text(oDisplayData.customer_name);
                    //$clonedOrderTemplate.find('[data-label="order_id"]').text(oOrder.id);
                    //$clonedOrderTemplate.find('[data-label="contact_num"]').text(oDisplayData.contact_number);
                    //$clonedOrderTemplate.find('[data-label="date_time_accepted"]').text(changeDateFormat(oOrder.date_added, DISPLAY_FORMATTED_DATE));
                    //$clonedOrderTemplate.find('[data-label="date_time_delivered"]').text(changeDateFormat(oOrder.delivery_date, DISPLAY_FORMATTED_DATE));
                    //$clonedOrderTemplate.find('[data-label="hitrate"]').text(addPercentSign(oOrder.hitrate));
                    //$clonedOrderTemplate.find('[data-label="total_cost"]').text(addMoneySign(oDisplayData.total_cost));
                    //$clonedOrderTemplate.find('[data-label="added_vat"]').text(addMoneySign(oDisplayData.added_vat));
                    //$clonedOrderTemplate.find('[data-label="delivery_charge"]').text(addMoneySign(oDisplayData.delivery_charge));
                    //$clonedOrderTemplate.find('[data-label="total_bill"]').text(addMoneySign(oDisplayData.total_bill));

                    oOrderTemplate.find('.data-order-id').text(order.id)

                    if (typeof(order.display_data) != 'undefined') {
                        var oDisplayData = $.parseJSON(order.display_data);
                        var oOrderTemplateExcel = $('tr.detailed.template').clone().removeClass('template notthis');
                        if (typeof(oOrderTemplateExcel) != 'undefined' && typeof(order) != 'undefined') {
                            var sItems = '';

                            $.each(oDisplayData.products, function (k, product) {
                                if (typeof(product) !== "undefined" && product != '' && product != null) {
                                    //console.log(product);
                                    sItems += product.quantity + " " + product.product_name + " @ " + product.price + " = " + product.sub_total + ", ";

                                    if (typeof(product.addons) != 'undefined' && product.addons != null) {
                                        $.each(product.addons, function (k_a, addon) {
                                            sItems += addon.quantity + " " + addon.product_name + " @ " + addon.price + " = " + addon.sub_total + ", ";
                                        })
                                    }

                                    if (typeof(product.special) != 'undefined' && product.special != null) {
                                        $.each(product.special, function (k_s, special) {
                                            sItems += special.quantity + " " + special.product_name + " @ " + special.price + " = " + special.sub_total + ", ";
                                        })
                                    }

                                    if (typeof(product.upgrades) != 'undefined' && product.upgrades != null) {
                                        $.each(product.upgrades, function (k_u, upgrade) {
                                            sItems += upgrade.quantity + " " + upgrade.product_name + " @ " + upgrade.price + " = " + upgrade.sub_total + ", ";
                                        })
                                    }
                                }

                            });

                            var dAccepted = order.logs.find(function (el) {
                                return el['action'] == 'Order Accepted'
                            });
                            dAccepted = (typeof(dAccepted) != 'undefined') ? moment(dAccepted.date_processed, 'YYYY-MM-DD HH:mm:ss').format('YYYY/MM/DD h:mm a') : '';

                            var dAssembled = order.logs.find(function (el) {
                                return el['action'] == 'Order Assembled'
                            });
                            dAssembled = (typeof(dAssembled) != 'undefined') ? moment(dAssembled.date_processed, 'YYYY-MM-DD HH:mm:ss').format('YYYY/MM/DD h:mm a') : '';

                            var dDriveOut = order.logs.find(function (el) {
                                return el['action'] == 'Order Dispatched'
                            });
                            dDriveOut = (typeof(dDriveOut) != 'undefined') ? moment(dDriveOut.date_processed, 'YYYY-MM-DD HH:mm:ss').format('YYYY/MM/DD h:mm a') : '';

                            var dDelivery = order.logs.find(function (el) {
                                return el['action'] == 'Order Delivered'
                            });
                            dDelivery = (typeof(dDelivery) != 'undefined') ? moment(dDelivery.date_processed, 'YYYY-MM-DD HH:mm:ss').format('YYYY/MM/DD h:mm a') : '';

                            oOrderTemplateExcel.find(".no").text(order.id);
                            oOrderTemplateExcel.find(".order_id").text(order.id);
                            oOrderTemplateExcel.find(".customer_name").text(oDisplayData.customer_name);
                            oOrderTemplateExcel.find(".customer_number").text(oDisplayData.contact_number);
                            oOrderTemplateExcel.find(".customer_address").text(oDisplayData.full_address);
                            oOrderTemplateExcel.find(".details").text(sItems);
                            oOrderTemplateExcel.find(".gross_sales").text(oDisplayData.total_bill);
                            oOrderTemplateExcel.find(".food_sales").text(parseFloat(oDisplayData.total_cost) - parseFloat(oDisplayData.added_vat));
                            oOrderTemplateExcel.find(".grid").text(oDisplayData.rta_grid);
                            oOrderTemplateExcel.find(".transaction_time").text(order.date_added);
                            oOrderTemplateExcel.find(".transaction_accepted").text(dAccepted);
                            oOrderTemplateExcel.find(".assembled").text(dAssembled);
                            oOrderTemplateExcel.find(".drive_out").text(dDriveOut);
                            oOrderTemplateExcel.find(".date_time_delivered").text(dDelivery);
                            oOrderTemplateExcel.find(".drive_in").text(dDriveOut);
                            oOrderTemplateExcel.find(".kitchen_time").text('Kitchen Time');
                            oOrderTemplateExcel.find(".delivery_time").text(dDelivery);
                            if (order.is_advanced != 1 && order.is_bulk != 1 && order.is_manually_relayed != 1) {
                                oOrderTemplateExcel.find(".20mins").text(function () {
                                    if (order.hitrate == 100 && oDisplayData.serving_time == '20') {
                                        return 'Y'
                                    } else {
                                        return ''
                                    }
                                });
                                oOrderTemplateExcel.find(".30mins").text(function () {
                                    if (order.hitrate == 100 && oDisplayData.serving_time == '30') {
                                        return 'Y'
                                    } else {
                                        return ''
                                    }
                                });
                                oOrderTemplateExcel.find(".45mins").text(function () {
                                    if (order.hitrate == 100 && oDisplayData.serving_time == '45') {
                                        return 'Y'
                                    } else {
                                        return ''
                                    }
                                });
                            }

                            if (order.is_advanced == 1) {
                                oOrderTemplateExcel.find(".advanced").text('Y');
                            }

                            if (order.is_bulk == 1) {
                                oOrderTemplateExcel.find(".big_rder").text('Y');
                            }

                            if (order.is_manually_relayed == 1) {
                                oOrderTemplateExcel.find(".exempted").text('Y');
                            }

                            oOrderTemplateExcel.find(".branch").text(oDisplayData.store_summary);
                            oOrderTemplateExcel.find(".serving_time").text(oDisplayData.serving_time + ' Minutes');
                            oOrderTemplateExcel.find(".hit").text(function () {
                                if ((order.hitrate == 100 && order.is_advanced != 1 && order.is_bulk != 1 && order.is_manually_relayed != 1) || order.is_advanced == 1 || order.is_bulk == 1 || order.is_manually_relayed == 1) {
                                    return 'Y'
                                } else {
                                    return ''
                                }
                            });

                            $('tbody.dst_reports_specific_content_table_container').append(oOrderTemplateExcel)
                            oOrderTemplate.find('.data-full-name').text(oDisplayData.customer_name)

                            oOrderTemplate.attr('data-customer-name', oDisplayData.customer_name)
                            oOrderTemplate.attr('data-order-id', order.id)
                            oOrderTemplate.attr('data-hit-rate', order.hitrate)

                            if (typeof(order.hitrate) != 'undefined') {
                                if (order.is_advanced == 1) {
                                    oOrderTemplate.find('.data-hit-rate').text('Advanced');
                                }
                                else if (order.is_bulk == 1) {
                                    oOrderTemplate.find('.data-hit-rate').text('Big Order');
                                }
                                else if (order.is_manually_relayed == 1) {
                                    oOrderTemplate.find('.data-hit-rate').text('Exempted');
                                }
                                else if (order.hitrate == '100') {
                                    oOrderTemplate.find('.data-hit-rate').text('Yes');
                                }
                                else {
                                    oOrderTemplate.find('.data-hit-rate').text('No');
                                }
                            }
                            else {
                                oOrderTemplate.find('.data-hit-rate').text('Serving Time Unavailable');
                            }

                            oOrderTemplate.find('.data-order-date').text(order.date_added);
                            oOrderTemplate.find('.data-contact-number').text(oDisplayData.contact_number);
                            oOrderTemplate.find('.data-full-address').text(oDisplayData.full_address)

                            oOrderTemplate.find('info.grid').text(oDisplayData.rta_grid)
                            oOrderTemplate.find('info.serving_time').text(oDisplayData.serving_time + ' Minutes')
                            //oOrderTemplate.find('.data-full-address').text(oDisplayData.full_address)
                            oOrderTemplate.find('info.change_for').text(order.change_for)
                            if (typeof(oDisplayData.total_cost) != 'undefined') {
                                oOrderTemplate.find('[data-label="total_cost"]').text(parseFloat(oDisplayData.total_cost).toFixed(2) + ' PHP')
                            }

                            if (typeof(oDisplayData.total_bill) != 'undefined') {
                                oOrderTemplate.find('[data-label="total_bill"]').text(parseFloat(oDisplayData.total_bill).toFixed(2) + ' PHP')
                            }

                            iGrandTotal += parseFloat(oDisplayData.total_bill);

                            if (typeof(oDisplayData.delivery_charge) != 'undefined') {
                                oOrderTemplate.find('[data-label="delivery_charge"]').text(parseFloat(oDisplayData.delivery_charge).toFixed(2) + ' PHP')
                            }


                            /* oOrderTemplate.find('[data-label="added_vat"]').text(oDisplayData.added_vat.toFixed(2) + ' PHP')
                             oOrderTemplate.find('[data-label="total_bill"]').text(oDisplayData.total_bill + ' PHP')
                             oOrderTemplate.find('[data-label="delivery_charge"]').text(oDisplayData.delivery_charge.toFixed(2) + ' PHP')*/

                            if (typeof(oDisplayData.happy_plus) != 'undefined') {
                                $.each(oDisplayData.happy_plus, function (k, v) {
                                    var uiHappyPlusTemplate = oOrderTemplate.find('div.happy_plus_cards.template').clone().removeClass('template');
                                    uiHappyPlusTemplate.find('p[data-label="card_number"]').text(v.number);
                                    uiHappyPlusTemplate.find('p[data-label="card_expiration"]').text(v.date);
                                    oOrderTemplate.find('div.happy_plus_cards_container').append(uiHappyPlusTemplate);
                                });
                            }

                            if (typeof(oDisplayData.products) != 'undefined') {
                                $.each(oDisplayData.products, function (key, product) {
                                    if (typeof(product) != 'undefined' && product != "" && product != null) {
                                        var uiProductContainer = oOrderTemplate.find('tbody[data-transaction-orders="detailed_orders"]');
                                        var uiProduct = oOrderTemplate.find('tr[data-template-transaction-for="order"].template').clone().removeClass('template');
                                        uiProduct.find('td[data-label="quantity"]').text(product.quantity);
                                        uiProduct.find('td[data-label="product"]').text(product.product_name)
                                        uiProduct.find('td[data-label="price"]').text(product.price)
                                        uiProduct.find('td[data-label="sub_total"]').text(product.sub_total)
                                        uiProductContainer.append(uiProduct)

                                        if (typeof(product.addons) != 'undefined') {
                                            $.each(product.addons, function (key, addon) {
                                                var uiAddon = oOrderTemplate.find('tr[data-template-transaction-for="order"].template').clone().removeClass('template');
                                                uiAddon.find('td[data-label="quantity"]').text(addon.quantity);
                                                uiAddon.find('td[data-label="product"]').text(addon.product_name)
                                                uiAddon.find('td[data-label="price"]').text(addon.price + ' PHP')
                                                uiAddon.find('td[data-label="sub_total"]').text(addon.sub_total)
                                                uiProductContainer.append(uiAddon)
                                            })


                                        }

                                        if (typeof(product.special) != 'undefined') {
                                            $.each(product.special, function (key, spcl) {
                                                var uiSpecial = oOrderTemplate.find('tr[data-template-transaction-for="order"].template').clone().removeClass('template');
                                                uiSpecial.find('td[data-label="quantity"]').text(spcl.quantity);
                                                uiSpecial.find('td[data-label="product"]').text(spcl.product_name)
                                                uiSpecial.find('td[data-label="price"]').text(spcl.price)
                                                uiSpecial.find('td[data-label="sub_total"]').text(spcl.sub_total)
                                                uiProductContainer.append(uiSpecial)
                                            })


                                        }

                                        if (typeof(product.upgrades) != 'undefined') {
                                            $.each(product.upgrades, function (key, upgrade) {
                                                var uiUpgrades = oOrderTemplate.find('tr[data-template-transaction-for="order"].template').clone().removeClass('template');
                                                uiUpgrades.find('td[data-label="quantity"]').text(upgrade.quantity);
                                                uiUpgrades.find('td[data-label="product"]').text(upgrade.product_name)
                                                uiUpgrades.find('td[data-label="price"]').text(upgrade.price)
                                                uiUpgrades.find('td[data-label="sub_total"]').text(upgrade.sub_total)
                                                uiProductContainer.append(uiUpgrades)
                                            })
                                        }
                                    }
                                })


                            }
                        }

                        $dstReportsForStoresOrdersContainer.append(oOrderTemplate);
                    }

                }


            },

            clear: function () {
                $dstReportsForStoresOrdersContainer.children('section.section-order').not('.template').remove();
            }
        },

        main_report: {
            show: function () {
                $sectionDstForStore.removeClass('hidden');
                $sectionDstForStoreViewData.addClass('hidden');
            },
            table_data: {
                show: function() {
                    $dstForStoreTableDataContainer.removeClass('hidden');
                    admin.reports_store_supply_data.main_report.no_data.hide();
                    scrollToTop();
                },
                hide: function() {
                    $dstForStoreTableDataContainer.add('hidden');
                    admin.reports_store_supply_data.main_report.no_data.show();
                    scrollToTop();
                }
            },
            no_data: {
                show: function() {
                    $dstForStoreNoDataContainer.removeClass('hidden');
                },
                hide: function() {
                    $dstForStoreNoDataContainer.addClass('hidden');
                }
            },
            summary_box: {
                date_range: function (sFrom, sTo) {
                    var
                        $dateRangeCon = $('#dst_for_store_date_range'),
                        $dateFrom = $dateRangeCon.find('p[data-label="date_from"]'),
                        $dateTo = $dateRangeCon.find('p[data-label="date_to"]'),
                        sFormattedDateFrom = changeDateFormat(sFrom),
                        sFormattedDateTo = changeDateFormat(sTo);

                    $dateFrom.text(sFormattedDateFrom);
                    $dateTo.text(sFormattedDateTo);

                    admin.reports_store_core.set_date_range(sFormattedDateFrom, sFormattedDateTo);
                },
                store_min: function (minutes, sYes, sNo, sTotal, sPercentage) {
                    var $storeMinCon = $('#dst_for_store_' + minutes + '_mins'),
                        $yes = $storeMinCon.find('[data-label="yes"]'),
                        $no = $storeMinCon.find('[data-label="no"]'),
                        $total = $storeMinCon.find('[data-label="total"]'),
                        $percentage = $storeMinCon.find('[data-label="percentage"]');

                    $yes.text(sYes);
                    $no.text(sNo);
                    $total.text(sTotal);
                    $percentage.text(addPercentSign(sPercentage));
                },
                advance_order: function (noOfOrder) {
                    var $advanceOrderCon = $('#dst_for_store_advance_order'),
                        $advanceOrder = $advanceOrderCon.find('[data-label="no_advance_order"]');

                    $advanceOrder.text(noOfOrder);
                },
                bulk_order: function (noOfOrder) {
                    var $bulkOrderCon = $('#dst_for_store_bulk_order'),
                        $bulkOrder = $bulkOrderCon.find('[data-label="no_bulk_order"]');

                    $bulkOrder.text(noOfOrder);
                },
                order_exception: function (sException) {
                    var $orderExceptionCon = $('#dst_for_store_exception'),
                        $orderException = $orderExceptionCon.find('[data-label="no_exception"]');

                    $orderException.text(sException);
                },
                total_hitrate_percentage: function (sHitratePer) {
                    var $totalHitratePercentageCon = $('#dst_for_store_total_hitrate_percentage'),
                        $totalHitratePercentage = $totalHitratePercentageCon.find('[data-label="total_hitrate_percentage"]');

                    $totalHitratePercentage.text(addPercentSign(sHitratePer));
                }
            },
            tr_store : {
                clear: function () {
                    $dstForStoreTable.children('tbody').children('tr').not('.template').remove();
                },
                /**
                 * Accept only single array|object in each parameter
                 *
                 * @param oStoreInfo
                 * @param oStoreOrderComputation
                 */
                supply_append: function(oStoreInfo, oStoreOrderComputation) {
                    var $singleRowTemplate = $dstForStoreTableRowTemplate.clone()
                        .removeAttr('data-template').removeClass('template');

                    var $excelTemplate = $('table#dst_for_store_table_hidden').find('tr.template.excel_row').clone().removeClass('template').removeClass('notthis');

                    validateStoreInfo(oStoreInfo);
                    validateOrderInfo(oStoreOrderComputation);

                    $singleRowTemplate.find('[data-label="store_name"]').text(oStoreInfo.name);
                    $excelTemplate.find('[data-label="store_name"]').text(oStoreInfo.name);
                    $singleRowTemplate.find('a.viewdata').attr('store_id', oStoreInfo.id)
                    $singleRowTemplate.find('[data-label="20_rate_yes"]').text(oStoreOrderComputation.sum_yes_20);
                    $singleRowTemplate.find('[data-label="20_rate_no"]').text(oStoreOrderComputation.sum_no_20);
                    $singleRowTemplate.find('[data-label="20_rate_total"]').text(oStoreOrderComputation.sum_order_20);
                    $singleRowTemplate.find('[data-label="20_rate_per"]').text(addPercentSign(oStoreOrderComputation.sum_percent_20));

                    $excelTemplate.find('[data-label="20_rate_yes_excel"]').text(oStoreOrderComputation.sum_yes_20 + '/' + oStoreOrderComputation.sum_order_20);
                    $excelTemplate.find('[data-label="30_rate_yes_excel"]').text(oStoreOrderComputation.sum_yes_30 + '/' + oStoreOrderComputation.sum_order_30);
                    $excelTemplate.find('[data-label="45_rate_yes_excel"]').text(oStoreOrderComputation.sum_yes_45 + '/' + oStoreOrderComputation.sum_order_45);
                    $excelTemplate.find('[data-label="advance_orders_excel"]').text(oStoreOrderComputation.sum_advance_order + '/' + oStoreOrderComputation.sum_advance_order);
                    $excelTemplate.find('[data-label="bulk_orders_excel"]').text('0/0');
                    $excelTemplate.find('[data-label="exceptions_excel"]').text('0/0');

                    $excelTemplate.find('[data-label="total_rate_yes_excel"]').text(oStoreOrderComputation.sum_yes_all + '/' + oStoreOrderComputation.sum_total_all);

                    if(oStoreOrderComputation.sum_yes_all > 0 && oStoreOrderComputation.sum_total_all > 0)
                    {
                        $excelTemplate.find('[data-label="total_rate_yes_excel_per"]').text((oStoreOrderComputation.sum_yes_all / oStoreOrderComputation.sum_total_all) * 100 + '%');
                    }
                    else
                    {
                        $excelTemplate.find('[data-label="total_rate_yes_excel_per"]').text('0%');
                    }



                    $singleRowTemplate.find('[data-label="30_rate_yes"]').text(oStoreOrderComputation.sum_yes_30);
                    $singleRowTemplate.find('[data-label="30_rate_no"]').text(oStoreOrderComputation.sum_no_30);
                    $singleRowTemplate.find('[data-label="30_rate_total"]').text(oStoreOrderComputation.sum_order_30);
                    $singleRowTemplate.find('[data-label="30_rate_per"]').text(addPercentSign(oStoreOrderComputation.sum_percent_30));

                    $singleRowTemplate.find('[data-label="45_rate_yes"]').text(oStoreOrderComputation.sum_yes_45);
                    $singleRowTemplate.find('[data-label="45_rate_no"]').text(oStoreOrderComputation.sum_no_45);
                    $singleRowTemplate.find('[data-label="45_rate_total"]').text(oStoreOrderComputation.sum_order_45);
                    $singleRowTemplate.find('[data-label="45_rate_per"]').text(addPercentSign(oStoreOrderComputation.sum_percent_45));

                    $singleRowTemplate.find('[data-label="advance_orders"]').text(oStoreOrderComputation.sum_advance_order);
                    $singleRowTemplate.find('[data-label="bulk_orders"]').text(oStoreOrderComputation.sum_bulk_order);
                    $singleRowTemplate.find('[data-label="exceptions"]').text(oStoreOrderComputation.sum_exempted_order);

                    $singleRowTemplate.find('[data-label="total_rate_yes"]').text(oStoreOrderComputation.sum_yes_all);
                    $singleRowTemplate.find('[data-label="total_rate_no"]').text(oStoreOrderComputation.sum_no_all);
                    $singleRowTemplate.find('[data-label="total_rate_total"]').text(oStoreOrderComputation.sum_total_all);
                    $singleRowTemplate.find('[data-label="total_rate_per"]').text(addPercentSign(oStoreOrderComputation.sum_percent_all));

                    $singleRowTemplate.find('[data-label="total_hitrate_per"]').text(addPercentSign(oStoreOrderComputation.total_hitrate_per));

                    $dstForStoreTable.children('tbody').append($singleRowTemplate);
                    $('table#dst_for_store_table_hidden').find('tbody').append($excelTemplate);
                }
            }
        }
    }
})();

$(window).on('load', function () {
    admin.reports_store_supply_data.initialize();
});
