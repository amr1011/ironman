/**
 * Includes Computation of no of orders and storing of data
 */
(function () {
    "use strict";
    var oGrandComputation = {
            grand_yes_20: 0,
            grand_no_20: 0,
            grand_order_20: 0,
            grand_percent_20: 0,

            grand_yes_30: 0,
            grand_no_30: 0,
            grand_order_30: 0,
            grand_percent_30: 0,

            grand_yes_45: 0,
            grand_no_45: 0,
            grand_order_45: 0,
            grand_percent_45: 0,

            grand_yes_all: 0,
            grand_no_all: 0,
            grand_total_all: 0,
            grand_percent_all: 0,

            grand_total_hitrate_per: 0,

            grand_advance_order: 0,
            grand_bulk_order: 0,
            grand_exempted_order: 0

        },
        arrStoredOrders = [],
        oStoredDateFormatted = {
            dateFrom: '',
            dateTo: ''
        },
        arrStoredReport = [];

    function computeEachStoreTotalHitratePer(yesRate, totalRate) {
        var numTotalPer;

        yesRate = parseInt(yesRate);
        totalRate = parseInt(totalRate);

        numTotalPer = yesRate / totalRate;

        if(isNaN(numTotalPer)) {
            numTotalPer = 0
        }
        
        return numTotalPer * 100;
    }

    function resetGrandTotal() {
        for(var key in oGrandComputation) {
            if(oGrandComputation.hasOwnProperty(key)) {
                oGrandComputation[key] = 0;
            }
        }
    }

    function addToGrandTotal(oSubComputation) {
        oGrandComputation.grand_yes_20 += oSubComputation.sum_yes_20;
        oGrandComputation.grand_no_20 += oSubComputation.sum_no_20;
        oGrandComputation.grand_order_20 += oSubComputation.sum_order_20;
        oGrandComputation.grand_percent_20 += oSubComputation.sum_percent_20;

        oGrandComputation.grand_yes_30 += oSubComputation.sum_yes_30;
        oGrandComputation.grand_no_30 += oSubComputation.sum_no_30;
        oGrandComputation.grand_order_30 += oSubComputation.sum_order_30;
        oGrandComputation.grand_percent_30 += oSubComputation.sum_percent_30;

        oGrandComputation.grand_yes_45 += oSubComputation.sum_yes_45;
        oGrandComputation.grand_no_45 += oSubComputation.sum_no_45;
        oGrandComputation.grand_order_45 += oSubComputation.sum_order_45;
        oGrandComputation.grand_percent_45 += oSubComputation.sum_percent_45;

        oGrandComputation.grand_yes_all += oSubComputation.sum_yes_all;
        oGrandComputation.grand_no_all += oSubComputation.sum_no_all;
        oGrandComputation.grand_total_all += oSubComputation.sum_total_all;
        oGrandComputation.grand_percent_all += oSubComputation.sum_percent_all;

        oGrandComputation.grand_advance_order += oSubComputation.sum_advance_order;
        oGrandComputation.grand_bulk_order += oSubComputation.sum_bulk_order;
        oGrandComputation.grand_exempted_order += oSubComputation.sum_exempted_order;
    }

    CPlatform.prototype.reports_store_core = {

        get_grand_total: function () {
            oGrandComputation.grand_total_hitrate_per = computeEachStoreTotalHitratePer(oGrandComputation.grand_yes_all, oGrandComputation.grand_total_all);

            return oGrandComputation;
        },

        reset_grand_total: function () {
            resetGrandTotal();
        },

        /**
         * @todo Bulk order computation yet and exceptions
         *
         * @param arrOrdersInfo
         */
        compute_store_order: function (arrOrdersInfo) {
            var numOrdersLength = arrOrdersInfo.length,
                oReturnComputation = {
                    sum_yes_20: 0,
                    sum_no_20: 0,
                    sum_order_20: 0,
                    sum_percent_20: 0,

                    sum_yes_30: 0,
                    sum_no_30: 0,
                    sum_order_30: 0,
                    sum_percent_30: 0,

                    sum_yes_45: 0,
                    sum_no_45: 0,
                    sum_order_45: 0,
                    sum_percent_45: 0,

                    sum_yes_all: 0,
                    sum_no_all: 0,
                    sum_total_all: 0,
                    sum_percent_all: 0,

                    total_hitrate_per: 0,

                    sum_advance_order: 0,
                    sum_bulk_order: 0,
                    sum_exempted_order: 0
                };

            for(var i = 0; i < numOrdersLength; i++) {
                oReturnComputation.sum_yes_20 += parseInt(arrOrdersInfo[i].yes_20);
                oReturnComputation.sum_no_20 += parseInt(arrOrdersInfo[i].no_20);
                oReturnComputation.sum_order_20 += parseInt(arrOrdersInfo[i].order_20);
                oReturnComputation.sum_percent_20 += parseFloat(arrOrdersInfo[i].percent_20);

                oReturnComputation.sum_yes_30 += parseInt(arrOrdersInfo[i].yes_30);
                oReturnComputation.sum_no_30 += parseInt(arrOrdersInfo[i].no_30);
                oReturnComputation.sum_order_30 += parseInt(arrOrdersInfo[i].order_30);
                oReturnComputation.sum_percent_30 += parseFloat(arrOrdersInfo[i].percent_30);

                oReturnComputation.sum_yes_45 += parseInt(arrOrdersInfo[i].yes_45);
                oReturnComputation.sum_no_45 += parseInt(arrOrdersInfo[i].no_45);
                oReturnComputation.sum_order_45 += parseInt(arrOrdersInfo[i].order_45);
                oReturnComputation.sum_percent_45 += parseFloat(arrOrdersInfo[i].percent_45);

                oReturnComputation.sum_yes_all += parseInt(arrOrdersInfo[i].yes_all);
                oReturnComputation.sum_no_all += parseInt(arrOrdersInfo[i].no_all);
                oReturnComputation.sum_total_all += parseInt(arrOrdersInfo[i].total_all);
                oReturnComputation.sum_percent_all += parseFloat(arrOrdersInfo[i].percent_all);

                oReturnComputation.sum_advance_order += parseInt(arrOrdersInfo[i].advance_order);
                oReturnComputation.sum_bulk_order += parseInt(arrOrdersInfo[i].bulk_order);
                oReturnComputation.sum_exempted_order += parseInt(arrOrdersInfo[i].exempted_order);
            }

            oReturnComputation.total_hitrate_per = computeEachStoreTotalHitratePer(oReturnComputation.sum_yes_all, oReturnComputation.sum_total_all);

            addToGrandTotal(oReturnComputation);

            return oReturnComputation;
        },

        clear_stored_orders: function () {
            arrStoredOrders = [];
        },

        /**
         * This will store orders based on each store
         */
        set_orders: function (arrStoreOrders) {
            var numStoreOrdersLength = arrStoreOrders.length,
                numOrdersLength, i, ii;

            for (i = 0; i < numStoreOrdersLength; i++) {
                numOrdersLength = arrStoreOrders[i].orders.length;
                for (ii = 0; ii < numOrdersLength; ii++) {
                    arrStoredOrders.push(arrStoreOrders[i].orders[ii]);
                }
            }
        },
        get_stored_orders: function () {
            return arrStoredOrders;
        },
        set_date_range: function (dateFrom, dateTo) {
            oStoredDateFormatted.dateFrom = dateFrom;
            oStoredDateFormatted.dateTo = dateTo;
        },
        get_date_range: function () {
            return oStoredDateFormatted;
        },
        store_data_report_per_store: function () {
        }
    }
})();