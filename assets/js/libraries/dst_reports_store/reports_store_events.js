/**
 * Created by Developer on 10/27/2015.
 */
(function () {
    "use strict";

    var $sectionDstForStore,
        $sectionDstForStoreViewData;

    var sReportsApi = admin.config('url.api.jfc.reports');

    var formatDate = admin.reports_store_supply_data.get_formatted_date;

    var standardDateFormat = 'YYYY-MM-DD';

    var oAjaxConfig = {
        'type'   : "GET",
        'headers': {"X-API-KEY": "IEYRtW2cb7A5Gs54A1wKElECBL65GVls"}
    };

    var _btnEventsForMain = {
        generateReport: function () {
            $sectionDstForStore.on('click', '#btn_dst_report_for_store_generate_report', function () {
                var uiThis = $(this),
                    $dstForStoreDownloadExcelFile = $('#dst_for_store_download_excel_file'),
                    oDateFromTo = getDateFromAndTo(),
                    sDateFrom = formatDate(oDateFromTo.date_from, standardDateFormat),
                    sDateTo = formatDate(oDateFromTo.date_to, standardDateFormat),
                    _reports_store_core = admin.reports_store_core,
                    _main_report = admin.reports_store_supply_data.main_report,
                    _tr_store = _main_report.tr_store;
                
                if(sDateTo === 'Invalid date' || sDateFrom === 'Invalid date'){
                    console.warn('Supply date range');
                    return;
                }
                
                admin.agents.show_spinner(uiThis, true);
                $dstForStoreDownloadExcelFile.prop('disabled', true);

                _tr_store.clear();
                _reports_store_core.clear_stored_orders();
                _reports_store_core.reset_grand_total();

                oAjaxConfig = {
                    data : {
                        params: {
                            "range_from": sDateFrom,
                            "range_to": sDateTo,
                            "store_id"  : $('#dst_per_store_store').attr('data-value'),
                            "province_id" : $('#dst_per_store_province').attr('data-value'),
                            "order_type" : $('#dst_per_store_type').attr('data-value'),
                            "callcenter_id" : $('#dst_per_store_callcenter').attr('data-value'),
                        },
                        user_id : iConstantUserId
                    },
                    url: sReportsApi + '/reports/dst_per_store',
                    success: function(oData) {
                        var _summaryBox = _main_report.summary_box,
                            _tableData = _main_report.table_data,
                            oStoreKey,
                            oStoreData = oData.data,
                            oComputationResult,
                            oGrandTotal;

                        admin.agents.show_spinner(uiThis, false);
                        _summaryBox.date_range(sDateFrom, sDateTo);

                        if(oData.status) {
                            _tableData.show();
                            for(oStoreKey in oStoreData) {
                                if(oStoreData.hasOwnProperty(oStoreKey)) {
                                    oComputationResult = _reports_store_core.compute_store_order(oStoreData[oStoreKey].store_orders);
                                    _reports_store_core.set_orders(oStoreData[oStoreKey].store_orders);
                                    _tr_store.supply_append(oStoreData[oStoreKey].store_info, oComputationResult);
                                }
                            }
                            oGrandTotal = admin.reports_store_core.get_grand_total();

                            _summaryBox.store_min('20', oGrandTotal.grand_yes_20, oGrandTotal.grand_no_20, oGrandTotal.grand_order_20, oGrandTotal.grand_percent_20);
                            _summaryBox.store_min('30', oGrandTotal.grand_yes_30, oGrandTotal.grand_no_30, oGrandTotal.grand_order_30, oGrandTotal.grand_percent_30);
                            _summaryBox.store_min('45', oGrandTotal.grand_yes_45, oGrandTotal.grand_no_45, oGrandTotal.grand_order_45, oGrandTotal.grand_percent_45);
                            _summaryBox.store_min('total', oGrandTotal.grand_yes_all, oGrandTotal.grand_no_all, oGrandTotal.grand_total_all, oGrandTotal.grand_percent_all);

                            _summaryBox.advance_order(oGrandTotal.grand_advance_order);
                            _summaryBox.bulk_order(oGrandTotal.grand_bulk_order);
                            _summaryBox.order_exception(oGrandTotal.grand_exempted_order);

                            _summaryBox.total_hitrate_percentage(oGrandTotal.grand_total_hitrate_per);

                            $dstForStoreDownloadExcelFile.prop('disabled', false);
                        } else {
                            _tableData.hide();
                            
                            $dstForStoreDownloadExcelFile.prop('disabled', false);
                        }

                        admin.reports_store_events.recompute();
                    }
                };
                admin.CconnectionDetector.ajax(oAjaxConfig);

                unsetDataAndSuccess();

            });

            
        },
        viewData: function () {
            $sectionDstForStore.on('click', '#btn_dst_for_store_view_data', function () {
                var _view_data = admin.reports_store_supply_data.view_data,
                    arrOrders = admin.reports_store_core.get_stored_orders(),
                    arrOrdersLength = arrOrders.length;

                _view_data.show();
                _view_data.clear();
                _view_data.change_date_range();
                _view_data.change_total_hitrate_per();

                console.log(arrOrders)

                var oStoredOrders = cr8v.get_from_storage('dst_orders');
                if(typeof(oStoredOrders) != 'undefined')
                {
                    cr8v.pop('dst_orders');
                }
                cr8v.add_to_storage('dst_orders', arrOrders);
                iGrandTotal = 0;

                var iProvince = $('div#dst_for_store_table_data_container').find('div.per_store_province').find('input[type="text"]:first').attr('data-value');
                var sOrderType =  $('div#dst_for_store_table_data_container').find('div.per_store_transactions').find('input[type="text"]:first').attr('data-value');
                var iCallcenter = $('div#dst_for_store_table_data_container').find('div.per_store_callcenter').find('input[type="text"]:first').attr('data-value');

                if(iProvince > 0)
                {
                   arrOrders = arrOrders.filter(function(el) {
                       return el['province_id'] == iProvince;
                   }) 
                }

                if(sOrderType != 'Show All Transaction')
                {
                   arrOrders = arrOrders.filter(function(el) {
                       return el['channel_code'] == sOrderType;
                   }) 
                }

                if(iCallcenter > 0)
                {
                   arrOrders = arrOrders.filter(function(el) {
                       return el['callcenter_id'] == iCallcenter;
                   }) 
                }

                admin.reports_store_supply_data.render_pagination(arrOrders);
                $('tbody.dst_reports_specific_content_table_container').find('tr.detailed:not(.template)').remove();
                for(var i = 0; i < arrOrdersLength; i++) {
                    admin.reports_store_supply_data.view_data.supply_append_orders(arrOrders[i]);
                }

                $('section#dst_for_store_view_data').find('p[data-label="total_hitrate_percentage"]').text(parseFloat(iGrandTotal).toFixed(2));
                $('section#dst_for_store_view_data').find('p[data-label="total_hitrate_percentage"]').prev('p.margin-bottom-10').text('Total Amount')
            });
        },
        downloadExcelFile: function () {
            $sectionDstForStore.on('click', '#dst_for_store_download_excel_file', function () {
                $('#dst_for_store_table_hidden').table2excel({
                    filename: moment().format("YYYY-MM-DD") + "-" + moment(),
                    exclude: ".notthis"
                });
            });
        }
    };

    var _btnEventsForViewData = {
        backToDstReportForStore: function () {
            $sectionDstForStoreViewData.on('click', '.dst-for-store-view-data-back-to-main', function (e) {
                e.preventDefault();
                admin.reports_store_supply_data.main_report.show();
            })
        }
    };

    function unsetDataAndSuccess() {
        //bkt mo dndelete??????
        //delete oAjaxConfig.data;
        //delete oAjaxConfig.success;
    }

    function getDateFromAndTo() {
        var oDateFromTo = {};

        oDateFromTo.date_from = $('#dst_for_store_date_from').val();
        oDateFromTo.date_to = $('#dst_for_store_date_to').val();

        return oDateFromTo;
    }

    CPlatform.prototype.reports_store_events = {
        initialize: function () {
            var ev;
            $sectionDstForStore = $('#dst_for_store');
            $sectionDstForStoreViewData = $('#dst_for_store_view_data');

            for(ev in _btnEventsForMain) {
                if(_btnEventsForMain.hasOwnProperty(ev)) {
                    _btnEventsForMain[ev]();
                }
            }

            for(ev in _btnEventsForViewData) {
                if(_btnEventsForViewData.hasOwnProperty(ev)) {
                    _btnEventsForViewData[ev]();
                }
            }

            $('div#dst_for_store_table_data_container').on('click', 'a.viewdata', function () {
                var _view_data = admin.reports_store_supply_data.view_data,
                    arrOrders = admin.reports_store_core.get_stored_orders(),
                    arrOrdersLength = arrOrders.length;

                var uiThis = $(this);
                var iStoreId = uiThis.attr('store_id');
                _view_data.show();
                _view_data.clear();
                _view_data.change_date_range();
                _view_data.change_total_hitrate_per();

                console.log(arrOrders)

                var oStoredOrders = cr8v.get_from_storage('dst_orders');
                if(typeof(oStoredOrders) != 'undefined')
                {
                    cr8v.pop('dst_orders');
                }
                cr8v.add_to_storage('dst_orders', arrOrders);
                iGrandTotal = 0;

                //var iProvince = $('div#dst_for_store_table_data_container').find('div.per_store_province').find('input[type="text"]:first').attr('data-value');
                //var sOrderType =  $('div#dst_for_store_table_data_container').find('div.per_store_transactions').find('input[type="text"]:first').attr('data-value');
                // var iCallcenter = $('div#dst_for_store_table_data_container').find('div.per_store_callcenter').find('input[type="text"]:first').attr('data-value');

                if(iStoreId > 0)
                {
                    arrOrders = arrOrders.filter(function(el) {
                        return el['store_id'] == iStoreId;
                    })
                }

                /* if(sOrderType != 'Show All Transaction')
                 {
                 arrOrders = arrOrders.filter(function(el) {
                 return el['channel_code'] == sOrderType;
                 })
                 }

                 if(iCallcenter > 0)
                 {
                 arrOrders = arrOrders.filter(function(el) {
                 return el['callcenter_id'] == iCallcenter;
                 })
                 }*/

                admin.reports_store_supply_data.render_pagination(arrOrders);
                $('tbody.dst_reports_specific_content_table_container').find('tr.detailed:not(.template)').remove();


                for(var i = 0; i < arrOrdersLength; i++) {
                    admin.reports_store_supply_data.view_data.supply_append_orders(arrOrders[i]);
                }

                $('section#dst_for_store_view_data').find('p[data-label="total_hitrate_percentage"]').text(parseFloat(iGrandTotal).toFixed(2));
                $('section#dst_for_store_view_data').find('p[data-label="total_hitrate_percentage"]').prev('p.margin-bottom-10').text('Total Amount')
            });
        },

        recompute : function() {
            var i20_yes = $('#dst_for_store_20_mins').find('[data-label="yes"]').text();
            var i20_total = $('#dst_for_store_20_mins').find('[data-label="total"]').text();
            $('#dst_for_store_20_mins').find('[data-label="percentage"]').text(parseFloat(parseInt(i20_yes)/parseInt(i20_total)).toFixed(2) * 100 + '%')

            var i30_yes = $('#dst_for_store_30_mins').find('[data-label="yes"]').text();
            var i30_total = $('#dst_for_store_30_mins').find('[data-label="total"]').text();
            $('#dst_for_store_30_mins').find('[data-label="percentage"]').text(parseFloat(parseInt(i30_yes)/parseInt(i30_total)).toFixed(2) * 100 + '%')

            var i45_yes = $('#dst_for_store_45_mins').find('[data-label="yes"]').text();
            var i45_total = $('#dst_for_store_45_mins').find('[data-label="total"]').text();
            $('#dst_for_store_45_mins').find('[data-label="percentage"]').text(parseFloat(parseInt(i45_yes)/parseInt(i45_total)).toFixed(2) * 100 + '%')
        }
    }
})();

$(window).on('load', function () {
    admin.reports_store_events.initialize();
});
