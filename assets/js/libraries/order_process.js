/**
 2 *  Search Order Class
 *
 *
 *
 */
(function () {
    "use strict";
    var uiPage = 'order_process';
    var iProductCount = 0;
    var iCycles = 0;

	
	/**
	 * The method that will be used to compute the senior citizen discount.
	 *	@param: (float) price - the total due of the cart.
	 *	
	 *	@formula: total_due = (total_due/1.12) - ((total_due/1.12)*0.20)
	*/
	function compute_src (fPrice)
	{
		console.log("PRICE:" + fPrice);
		var 
			fDeliveryPrice = 0,
			fVat = 0,
			fSrcDiscount = 151.8 * 0.2,
			fSalesWithoutDeliveryCharge = 0,
			fVat = 0,
			fTotal = 0
			;

		// price less delivery charge
		fDeliveryPrice = (fPrice / 1.1) *  0.1;
		fDeliveryPrice = fDeliveryPrice.toFixed(2);
	
		fSalesWithoutDeliveryCharge = fPrice - fDeliveryPrice;
		fSalesWithoutDeliveryCharge = fSalesWithoutDeliveryCharge.toFixed(2);
		
		fVat = (fSalesWithoutDeliveryCharge - fSrcDiscount) * 0.12;
		fVat = fVat.toFixed(2);
		

		fTotal = parseFloat(fSalesWithoutDeliveryCharge) + parseFloat(fVat) - fSrcDiscount ; 
		fTotal = fTotal.toFixed(2);
		console.log({
			'senior_discount' : fSrcDiscount,
			'total' : fTotal,
			'delivery_charge' : fDeliveryPrice,
			'vat' : fVat
		});
		return {
			'senior_discount' : fSrcDiscount,
			'total' : fTotal,
			'delivery_charge' : fDeliveryPrice,
			'vat' : fVat
		};
	}
	
    CPlatform.prototype.order_process = {
        'get_products_count' : function () {
            var oAjaxConfig = {
                "type"   : "GET",
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "data"   : {
                    "params": {
                        "table": "products",
                        "sbu_id" : "1"
                    }
                },
                "url"    : callcenter.config('url.api.jfc.products') + "/products/num_rows",
                "success": function (ProductCount) {
                    iProductCount = ProductCount;
                    callcenter.order_process.get_all_products(0);
                }
            }

            callcenter.call_orders.get(oAjaxConfig)
        },

        'get_special_options' : function() {
            var oAjaxConfig = {
                "type"   : "GET",
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "data"   : {
                    "params": {}
                },
                "url"    : callcenter.config('url.api.jfc.products') + "/products/special_options",
                "success": function (oData) {
                    if(typeof(oData != 'undefined')) {
                        if(typeof (oData.data) != 'undefined') {
                            if(oData.data.length > 0)
                            {
                                cr8v.add_to_storage('special_options', oData.data);
                            }
                        }
                    }
                }
            }

            callcenter.call_orders.get(oAjaxConfig)
        },

        'search' : function(sString) {
            if (sString.length > 2) {
                callcenter.coordinator_management.show_spinner($(this), true)

                var sSearchEval = " el['id'] != '0' ";
                sSearchEval += " && (el['name'].toLowerCase().indexOf('" + sVal.toLowerCase() + "') > -1) ";

                var oProducts = cr8v.get_from_storage('products', '');
                if (typeof(oProducts) != 'undefined') {
                    oProducts = oProducts[0];
                    if (typeof(sSearchEval) != 'undefined') {
                        oProducts = oProducts.filter(function (el) {
                            return eval(sSearchEval);
                        })
                        $('section.content #order_process_content div.product_search_category_result').html("");
                        var uiProductContainer = $('div.product_search_result');
                        uiProductContainer.html("")
                        if (typeof(oProducts) != 'undefined' && count(oProducts) > 0) {
                            callcenter.order_process.manipulate_product_template(oProducts, uiProductContainer);
                        }

                        callcenter.order_process.check_products_unavailability(uiProductContainer);
                    }
                }

                callcenter.coordinator_management.show_spinner($(this), false)
            }
            else
            {
                $('section.content #order_process_content div.product_search_result').html("");
            }
        },

        'initialize': function () {
            callcenter.order_process.get_mode_of_payment();
            callcenter.order_process.get_categories();
            callcenter.order_process.get_products_count();
            callcenter.order_process.get_special_options();

            $('section.content #order_process_content').on('click', 'button.search_product_button', function () {
                var sVal = $('section.content #order_process_content #search-products').val();
                if (sVal.length > 1) {
                    callcenter.coordinator_management.show_spinner($(this), true)

                    var sSearchEval = " el['id'] != '0' ";
                    sSearchEval += " && (el['name'].toLowerCase().indexOf('" + sVal.toLowerCase() + "') > -1)";
                    sSearchEval += " || (el['menu_board_code'].toLowerCase().indexOf('" + sVal.toLowerCase() + "') > -1)";

                    var oProducts = cr8v.get_from_storage('products', '');
                    if (typeof(oProducts) !== 'undefined') {
                        oProducts = oProducts[0];
                        if (typeof(sSearchEval) !== 'undefined') {
                            oProducts = oProducts.filter(function (el) {
                                return eval(sSearchEval);
                            })
                            $('section.content #order_process_content div.product_search_category_result').html("");
                            var uiProductContainer = $('div.product_search_result');
                            uiProductContainer.html("")
                            if (typeof(oProducts) !== 'undefined' && count(oProducts) > 0) {
                                callcenter.order_process.manipulate_product_template(oProducts, uiProductContainer);
                            }

                            callcenter.order_process.check_products_unavailability(uiProductContainer);
                        }
                    }

                    callcenter.coordinator_management.show_spinner($(this), false);
                    $('section.content #order_process_content #search-products').val("");
                }

            })

            $('section.content #order_process_content').on('keyup', 'input#search-products', function () {
                var sVal = $(this).val();
                var typingTimer;                //timer identifier
                var iDoneTypingInterval = 500;  //time in ms, 5 second for example

                var iMinLength = 2;
                if(sVal.length > iMinLength)
                {
                    clearTimeout(typingTimer);
                    typingTimer = setTimeout(callcenter.order_process.search(sVal), iDoneTypingInterval);
                    $('section.content #order_process_content div.product_search_result').html("");
                }


            })
            var iMinLength = 2;
            $('section.content #order_process_content').on('keydown', 'input#search-products', function () {
                if($(this).length > iMinLength)
                {
                    clearTimeout(typingTimer);
                    $('section.content #order_process_content div.product_search_result').html("");
                }

            })


            $('section.content #order_process_content').on('click', 'div.payment-type-option', function () {
                var iPaymentType = $(this).attr('data-value');
                $('section.content #order_process_content').find('input[name="pricing"]').attr('data-value', iPaymentType);
                var uiCart = $('section.content #order_process_content').find('table.cart-table-table');
                var sPaymentType = $(this).text();
                $('div[modal-id="manual-order-summary"]').find('p.pricing').text(sPaymentType)

                callcenter.order_process.compute_bill_breakdown(uiCart, iPaymentType);

            })

            $('section.content #order_process_content').on('click', 'div.general-info', function (e) {
                var sTargetId = $(this).attr("product-id"),
                    iTop = $("#" + sTargetId).offset().top,
                    iFinalTopValue = iTop;

                $('html,body').animate({
                    scrollTop: iFinalTopValue
                }, 100);
                
                callcenter.order_process.expand_product_info($(this).attr('product-id'), $(this));
                
                if($('section.content #order_process_content').find('div.special-option-container').is(":visible"))
                {
                    $('section.content #order_process_content').find('div.product-details:visible').find('div.meal-special-option:first').trigger('click')
                    var uiBucketMeals = $('section.content #order_process_content').find('div.product-details:visible');
                    if(typeof(uiBucketMeals) != 'undefined')
                    {
                                $.each(uiBucketMeals, function(key, element) {
                                            var uiBucketInnerMeals  = $(element).find('div.meal');
                                            if(typeof(uiBucketInnerMeals) != 'undefined')
                                            {
                                                 $.each(uiBucketInnerMeals, function(i, innermeal) {
                                                        if(typeof($(element).attr('data-special-option-quantity')) != 'undefined')
                                                            {
                                                                $(innermeal).find('div.special-option-container input.extra-special-quantity:first').val($(element).attr('data-special-option-quantity')).trigger('change')
                                                            }

                                                        callcenter.order_process.compute_breakdown_price($(innermeal));
                                                        callcenter.order_process.compute_product_total($(innermeal));         
                                                 })       
                                            }
                                })
                    }
                }
            });

            $('section.content #order_process_content').on('click', 'button.close_product_info-details', function () {
                var uiProduct = $(this).parents('div.product-details');
                var sProductID = uiProduct.attr('product-id');
                callcenter.order_process.close_product_info(sProductID, uiProduct)
            });

            $('section.content #order_process_content').on('click', 'div.category-box', function () {
                var iCategoryID = $(this).attr('data-category-id');
                callcenter.order_process.get_product_categories(iCategoryID);

                //for the hiding of un available products in right panel
                $('section.content div.unavailable_products_container').find('div.notify-msg').each(function(){
                        $(this).addClass("hidden");          
                });

                var arrBrowsedProducts = [];
                $('section.content div.product_search_category_result div.general-info').each(function(){
                   if(typeof($(this).attr('core-id'))!=='undefined'){
                         //console.log($(this).attr('core-id'));    
                        var iVisibleCoreId = $(this).attr('core-id').split(",");
                        //console.log(iVisibleCoreId); 
                        $.each(iVisibleCoreId, function( i, v ){
                          arrBrowsedProducts.push(v);

                        });

                   }

                });

                //console.log(unique(arrBrowsedProducts));
                arrBrowsedProducts = unique(arrBrowsedProducts);
                if(arrBrowsedProducts.length > 0){
                          $.each(arrBrowsedProducts, function( i, v ){
                                    $('section.content div.unavailable_products_container').find('div[data-core-id="'+v+'"]').removeClass("hidden");
                        });  
                }

            });

            function unique(list) {
                var result = [];
                $.each(list, function(i, e) {
                    if ($.inArray(e, result) == -1) result.push(e);
                });
                return result;
            }

            /*meal number functionality*/
            $('section.content #order_process_content').on('click', 'div.meal-number > div.minus', function (e) {
                var uiThis = $(this);
                var uiInput = uiThis.next('input');
                var uiVal = uiInput.val();
                var iInnerMealTotal = $(this).parents('')
                var fCallback = function () {
                    var uiParents = uiThis.parents('div.product-details:first');
                    if(typeof(uiParents.attr('data-special-option-quantity'))!='undefined'){
                    var iSpecialOptionsQtyOriginal = parseInt(uiParents.attr('data-special-option-quantity-original-value'));
                    var iSpecialOptionsQty = parseInt(uiParents.attr('data-special-option-quantity'));
                        if(iSpecialOptionsQty > iSpecialOptionsQtyOriginal){
                        var iNewQty = iSpecialOptionsQty - iSpecialOptionsQtyOriginal;
                        uiParents.attr('data-special-option-quantity',iNewQty);
                        }
                    }

                    var uiMealContainer = uiParents.find('div.meal-container');
                    var iMealLength = uiParents.find('div.meal').length;
                    var lastMealContainerInput = uiMealContainer.find('div.meal:last').find('input.meal-number-inner-input')
                    if (iMealLength == 1) {
                        if(lastMealContainerInput.val() > 1)
                        {
                            lastMealContainerInput.val(lastMealContainerInput.val() - 1);
                        }
                    }
                    else {
                        if(lastMealContainerInput.val() == 1)
                        {
                            uiMealContainer.find('hr.meal-border:last').remove();
                            uiMealContainer.find('div.meal:last').remove();
                        }
                        if(lastMealContainerInput.val() > 1)
                        {
                            lastMealContainerInput.val(lastMealContainerInput.val() - 1);
                        }
                    }

                    callcenter.order_process.compute_product_total(uiParents);
                };
                
                callcenter.order_process.counter(uiThis, uiInput, uiVal, fCallback, e.originalEvent)
            });

            $('section.content #order_process_content').on('click', 'div.meal-number > div.plus', function (e) {
                var uiThis = $(this);
                var uiInput = uiThis.prev('input');
                var uiVal = uiInput.val();

                var fCallback = function () {
                    var uiParents = uiThis.parents('div.product-details:visible');
                    if(typeof(uiParents.attr('data-special-option-quantity'))!='undefined'){
                    var iSpecialOptionsQtyOriginal = parseInt(uiParents.attr('data-special-option-quantity-original-value'));
                    var iSpecialOptionsQty = parseInt(uiParents.attr('data-special-option-quantity'));
                    var iNewQty = iSpecialOptionsQtyOriginal + iSpecialOptionsQty;
                    uiParents.attr('data-special-option-quantity',iNewQty);

                    }

                    var uiMealContainer = uiParents.find('div.meal-container');
                    var uiMeal = uiParents.find('div.meal:first').clone();
                    uiMealContainer.append('<hr class="meal-border">');
                    var iMealMainLength = uiParents.find('div.meal').length;
                    iMealMainLength += 1;
                    uiMeal.find('strong.meal-number-collapse').text(iMealMainLength + '.')

                    if(typeof(uiParents.attr('data-special-option-quantity'))!='undefined'){
                                uiMeal.find('input.extra-quantity').val('');
                                uiMeal.find('input[name="meal-add-ons"]').val('No Addons');
                                uiMeal.find('input[name="meal-upgrades"]').val('No Upgrades');
                                //uiMeal.find('tr.product-breakdown-item[data-add-on="1"][data-special-product-id!="1"]').remove();
                                uiMeal.find('tr.product-breakdown-item[data-upgrade="1"]:last').remove();
                                //uiMeal.find('tr.product-breakdown-item[data-add-on="0"]:last').attr('data-product-quantity', 1).find('p.product-quantity').text(1);
                                uiMeal.find('input.meal-number-inner-input').val(1);
                    }
                    


                        
                    var oItemBreakDown = {
                        sProductName : uiParents.find('strong.product_name').text(),
                        iProductPrice: parseFloat(uiParents.attr('product-price')),
                        iQuantity    : 1,//parseFloat(uiMeal.find('input.meal-number-inner-input').val()),
                        iProductID   : uiParents.attr('product-id')
                    }

                    if(typeof(uiParents.attr('data-special-option-quantity'))=='undefined'){
                        uiMeal = callcenter.order_process.reset_template_value(uiMeal);
                        uiMeal = callcenter.order_process.regenerate_ids(uiMeal);
                        uiMeal = callcenter.order_process.attach_item_breakdown(uiMeal, oItemBreakDown);
                         
                    }

                    

                    uiMeal.find('button.close-panel').removeClass('disabled');
                    callcenter.order_process.compute_breakdown_price(uiMeal);
                    uiMealContainer.append(uiMeal);
                    callcenter.order_process.compute_product_total(uiParents);
                };
                callcenter.order_process.counter(uiThis, uiInput, uiVal, fCallback, e.originalEvent);

            });

            $('section.content #order_process_content').on('click', 'div.meal-number-inner > div.minus', function () {
                var uiThis = $(this);
                var uiInput = uiThis.next('input');
                var uiVal = uiInput.val();
                var uiMeal = uiThis.parents('div.meal:first');

                var fCallback = function (uiVal) {
                    var iInnerMealTotal = callcenter.order_process.check_all_inner_meal_total();
                    ////////console.log('inner meal');
                    ////////console.log(iInnerMealTotal,  $('input.number_of_meals:visible').val())
                    var uiParents = uiThis.parents('div.product-details:first');
                    var uiMeal = uiThis.parents('div.meal:first');
                    if (iInnerMealTotal < $('input.number_of_meals:visible').val()) {
                        //                                                 var uiParents = uiThis.parents('div.product-details:first');
                        //                                                 var uiMealContainer = uiParents.find('div.meal-container');
                        //                                                 var uiMeal = uiParents.find('div.meal:first').clone();

                        //                                                 uiMealContainer.append('<hr class="meal-border">');
                        //                                                 uiMeal = callcenter.order_process.reset_template_value(uiMeal);
                        //                                                 uiMeal = callcenter.order_process.regenerate_ids(uiMeal);
                        //                                                 uiMeal.find('button.close-panel').removeClass('disabled');
                        //                                                 uiMealContainer.append(uiMeal);
                    }
                    uiMeal.find('tbody.main_product_container').find('p.product-quantity').text(uiVal);
                    uiMeal.find('tbody.main_product_container').find('tr[class="product-breakdown-item"][class!="template"]').attr('data-product-quantity', uiVal);
                    uiMeal.find('tbody.add_ons_container').find('tr[data-special-product-id!="1"]').find('p.product-quantity').text(uiVal);
                    uiMeal.find('tbody.add_ons_container').find('tr[class="product-breakdown-item"][data-special-product-id!="1"][class!="template"]').attr('data-product-quantity', uiVal);
                    if(typeof(uiMeal.find('tbody.add_ons_container').find('tr[data-special-product-id]'))=='undefined'){
                        uiMeal.find('tbody.add_ons_container').find('p.product-quantity').text(uiVal)
                        uiMeal.find('tbody.add_ons_container').find('tr[class="product-breakdown-item"][class!="template"]').attr('data-product-quantity', uiVal)
                    }
                    callcenter.order_process.compute_breakdown_price(uiMeal);
                    callcenter.order_process.compute_product_total(uiParents);
                };
                callcenter.order_process.counter_inner_meal(uiThis, uiInput, uiVal, fCallback)
            });

            $('section.content #order_process_content').on('click', 'div.meal-number-inner > div.plus', function () {
                var uiThis = $(this);
                var uiInput = uiThis.prev('input');
                var uiVal = uiInput.val();
                var uiParents = $(this).parents('div.product-details:first:visible');
                var uiMeal = $(this).parents('div.meal:first:visible');
                ////////console.log(uiVal);
                var fCallback = function (uiVal) {
                    var iInnerMealTotal = callcenter.order_process.check_all_inner_meal_total();
                    ////////console.log(iInnerMealTotal);

                    if (iInnerMealTotal > $('input.number_of_meals:visible').val()) {
                        /*var uiParents = uiThis.parents('div.product-details:first');
                         var uiMealContainer = uiParents.find('div.meal-container');
                         uiMealContainer.find('hr.meal-border:last').remove();
                         uiMealContainer.find('div.meal:last').remove();*/
                    }

                    uiMeal.find('tbody.main_product_container').find('p.product-quantity').text(uiVal);
                    uiMeal.find('tbody.main_product_container').find('tr[class="product-breakdown-item"][class!="template"]').attr('data-product-quantity', uiVal);
                    uiMeal.find('tbody.add_ons_container').find('tr[data-special-product-id!="1"]').find('p.product-quantity').text(uiVal);
                    uiMeal.find('tbody.add_ons_container').find('tr[class="product-breakdown-item"][data-special-product-id!="1"][class!="template"]').attr('data-product-quantity', uiVal);
                    if(typeof(uiMeal.find('tbody.add_ons_container').find('tr[data-special-product-id]'))=='undefined'){
                        uiMeal.find('tbody.add_ons_container').find('tr[class="product-breakdown-item"][data-special-product-id!="1"]').find('p.product-quantity').text(uiVal);
                        uiMeal.find('tbody.add_ons_container').find('tr[class="product-breakdown-item"][data-special-product-id!="1"][class!="template"]').attr('data-product-quantity', uiVal);      
                    }
                    callcenter.order_process.compute_breakdown_price(uiMeal);
                    callcenter.order_process.compute_product_total(uiParents);
                };

                callcenter.order_process.counter_inner_meal(uiThis, uiInput, uiVal, fCallback);

            });

            $('section.content #order_process_content').on('click', 'button.close-panel', function () {
                //alert($(this).parent('div.meal-number input[name="number_of_meals"]').val());
                var uiParents = $(this).parents('div.product-details:first');
                
                if(typeof(uiParents.attr('data-special-option-quantity'))!='undefined'){
                    var iSpecialOptionsQtyOriginal = parseInt(uiParents.attr('data-special-option-quantity-original-value'));
                    var iSpecialOptionsQty = parseInt(uiParents.attr('data-special-option-quantity'));
                        if(iSpecialOptionsQty > iSpecialOptionsQtyOriginal){
                        var iNewQty = iSpecialOptionsQty - iSpecialOptionsQtyOriginal;
                        uiParents.attr('data-special-option-quantity',iNewQty);
                        }
                    }

                var iMealNewVal = uiParents.find('div.meal-number input[name="number_of_meals"]').val() - 1;
                uiParents.find('div.meal-number input[name="number_of_meals"]').val(iMealNewVal);
                $(this).parents('div.meal:first').prev('hr.meal-border').remove();
                $(this).parents('div.meal:first').remove();
                callcenter.order_process.reiterate_meal_number(uiParents);
                callcenter.order_process.compute_product_total(uiParents);
            })

            $('section.content #order_process_content').on('click', 'label.drinks', function () {
                $(this).prev('input').prop('checked', true)
            })

            $('section.content #order_process_content').on('click', 'button.add_to_cart', function () {
                 var uiThis = $(this),
                 uiProductContainer = uiThis.parents('div.product-details:first');
                 var iReturn = callcenter.order_process.add_to_cart(uiThis, uiProductContainer);
                 var uiProduct = $(this).parents('div.product-details');
                 var sProductID = uiProductContainer.attr('product-id');
                 if(typeof(iReturn) == 'undefined')
                 {
                     callcenter.order_process.close_product_info(sProductID, uiProductContainer);
                     $('div.product_search_result').html("");     
                 }
                 
            });
			
			
			 $('section.content #order_process_content').on('click', "[name='shopping-cart']", function(e){		 
				 console.log("clicked the shopping cart");
				
				/* adding senior citizen computation */
				var fOriginalPrice = $('section.content #order_process_content').find('table.cart-table-table').find('td.total_cost').attr("data-total-cost");
		
				if ( $("#shopping-cart-senior").is(":checked"))
				{
					var fSeniorCitizenDiscount = 0;
					var oSrcComputation = {};
					oSrcComputation = compute_src(fOriginalPrice);
					fSeniorCitizenDiscount = parseFloat(oSrcComputation.total);
					$('section.content #order_process_content').find('table.cart-table-table').find('td.total_bill').attr('data-total-bill', fSeniorCitizenDiscount).text(fSeniorCitizenDiscount.toFixed(2) + ' PHP');
				} else {
					
					fOriginalPrice = parseFloat(fOriginalPrice);
					$('section.content #order_process_content').find('table.cart-table-table').find('td.total_bill').attr('data-total-bill', fOriginalPrice).text(fOriginalPrice.toFixed(2) + ' PHP');
				}
			 });

            $('section.content #order_process_content table.cart-table-table').on('click', 'i.fa-times-circle', function () {
                $(this).parents('tr.cart-item:first').nextUntil('tr.cart-item.main', 'tr.cart-item.addon').remove();
                $(this).parents('tr.cart-item:first').nextUntil('tr.cart-item.main', 'tr.cart-item.special').remove();
                $(this).parents('tr.cart-item:first').nextUntil('tr.cart-item.main', 'tr.cart-item.upgrade').remove();
                $(this).parents('tr.cart-item:first').remove();
                var uiCart = $('section.content #order_process_content').find('table.cart-table-table');
                callcenter.order_process.compute_bill_breakdown(uiCart);
            });

            $('section.content #order_process_content table.cart-table-table').on('click', 'div.arrow-down', function () {
                $(this).parents('tr.cart-item.main').nextUntil('tr.cart-item.main', 'tr.cart-item.addon').toggle();
            })

            $('div.order_process button.show_bill_breakdown').prop('disabled', true);

            $('section.content #order_process_content').on('click', 'button.show_bill_breakdown', function () {
                var uiCart = $('table.cart-table-table:first').clone();

                uiCart.find('i.fa.fa-times-circle').hide();

                $('div[modal-id="manual-order-summary"]').find('p.transaction_time').text('');
                var sCurrent = moment().format('MMMM DD, YYYY hh:mm A');
                setInterval(function () {
                    sCurrent = moment().format('MMMM DD, YYYY | hh:mm A');
                    $('div[modal-id="manual-order-summary"]').find('p.transaction_time').text(sCurrent);
                }, 1000);

                $('div[modal-id="manual-order-summary"]').find('p.payment_type').text($('section.content #order_process_content').find('input[name="payment_type[]"]:visible').val())
                $('div[modal-id="manual-order-summary"]').find('p.pricing_diff').text($('section.content #order_process_content').find('input[name="pricing"]:visible').val())
                $('div[modal-id="manual-order-summary"]').find('p.order_mode').text(($('section.content #order_process_content').find('input[name="order-mode"]:checked').val() == 1) ? 'For Pick-up' : 'Delivery')
                $('div[modal-id="manual-order-summary"]').find('input[name="change_for"]').parents('div.change_for:first').removeClass('has-error');
                $('div[modal-id="manual-order-summary"]').find('input[name="delivery_time"]').parents('div.date-pickertime').removeClass('has-error');
                $('div[modal-id="manual-order-summary"]').find('input[name="delivery_date"]').parents('div.date-picker').removeClass('has-error');
                $('div[modal-id="manual-order-summary"]').find('div.cart-container').find('table').remove();
                $('div[modal-id="manual-order-summary"]').find('div.manual-order-error-msg').hide();
                $('div[modal-id="manual-order-summary"]').find('div.cart-container').prepend(uiCart);

            })

            $('div[modal-id="manual-order-summary"]').on('click', 'button.confirm_order', function () {
                var uiThis = $(this);

                var uiCart = $('div[modal-id="manual-order-summary"]').find('table.cart-table-table:first');
                var totalBill = parseFloat(uiCart.find('td.total_bill').attr('data-total-bill'));
                var minCost = parseFloat("200.00");
                var changeFor = parseFloat(uiCart.parents('div[modal-id="manual-order-summary"]').find('input[name="change_for"]').val());
                var deliveryDateInput = uiCart.parents('div[modal-id="manual-order-summary"]').find('input[name="delivery_date"]').val();
                var deliveryTimeInput = uiCart.parents('div[modal-id="manual-order-summary"]').find('input[name="delivery_time"]').val();
                var error = 0;

                var date = new Date(),
                    yearNow =  parseInt(moment().format('YYYY')),
                    monthNow =  parseInt(moment().format('MM')),
                    dayNow =  parseInt(moment().format('DD')),
                    hourNow = parseInt(moment().format('h')),
                    minNow = parseInt(moment().format('mm')),
                    amPmNow = moment().format('A');

                if (deliveryTimeInput != '') {
                    var deliveryTimeInputAmPm = deliveryTimeInput.slice(-2),
                        deliveryTimeInput = deliveryTimeInput.split(':'),
                        deliveryTimeHour = deliveryTimeInput[0],
                        deliveryTimeMin = deliveryTimeInput[1].substring(0, deliveryTimeInput[1].length - 3);
                    uiCart.parents('div[modal-id="manual-order-summary"]').find('input[name="delivery_time"]').parents('div.date-pickertime').removeClass('has-error');
                }
                if (deliveryDateInput != '') {
                    deliveryDateInput = deliveryDateInput.split('/');
                    uiCart.parents('div[modal-id="manual-order-summary"]').find('input[name="delivery_date"]').parents('div.date-picker').removeClass('has-error');
                }

                if (totalBill < minCost) {
                    uiCart.parents('div[modal-id="manual-order-summary"]').find('div.manual-order-error-msg').empty().html('<i class="fa fa-exclamation-triangle"></i> Minimum order is P200.00').show();
                    error++;
                }
                else if (changeFor < totalBill) {
                    uiCart.parents('div[modal-id="manual-order-summary"]').find('input[name="change_for"]').parents('div.change_for:first').addClass('has-error');
                    uiCart.parents('div[modal-id="manual-order-summary"]').find('div.manual-order-error-msg').empty().html('<i class="fa fa-exclamation-triangle"></i> Change should be larger than the total bill').show();
                    error++;
                }
                else if (uiCart.parents('div[modal-id="manual-order-summary"]').find('input[name="delivery_date"]').is(':visible') && uiCart.parents('div[modal-id="manual-order-summary"]').find('input[name="delivery_time"]').is(':visible')) {
                    if (deliveryDateInput == '') {
                        uiCart.parents('div[modal-id="manual-order-summary"]').find('div.manual-order-error-msg').empty().html('<i class="fa fa-exclamation-triangle"></i> Please select date').show();
                        uiCart.parents('div[modal-id="manual-order-summary"]').find('input[name="delivery_date"]').parents('div.date-picker').addClass('has-error');
                        error++;
                    }
                    else if (parseInt(deliveryDateInput[2]) < yearNow || (parseInt(deliveryDateInput[2]) <= yearNow && parseInt(deliveryDateInput[0]) < monthNow) || (parseInt(deliveryDateInput[2]) <= yearNow && parseInt(deliveryDateInput[0]) <= monthNow && parseInt(deliveryDateInput[1]) < dayNow)) {
                        uiCart.parents('div[modal-id="manual-order-summary"]').find('div.manual-order-error-msg').empty().html('<i class="fa fa-exclamation-triangle"></i> Cannot select past dates').show();
                        uiCart.parents('div[modal-id="manual-order-summary"]').find('input[name="delivery_date"]').parents('div.date-picker').addClass('has-error');
                        error++;
                    }
                    else if (deliveryTimeInput == '') {
                        uiCart.parents('div[modal-id="manual-order-summary"]').find('div.manual-order-error-msg').empty().html('<i class="fa fa-exclamation-triangle"></i> Please select time').show();
                        uiCart.parents('div[modal-id="manual-order-summary"]').find('input[name="delivery_time"]').addClass('has-error');
                        error++;
                    }
                    if (parseInt(deliveryDateInput[2]) == yearNow && parseInt(deliveryDateInput[0]) == monthNow && parseInt(deliveryDateInput[1]) == dayNow) {
                        /*for am*/
                        // console.log( ( amPmNow == 'AM' && parseInt(deliveryTimeHour) < (parseInt(hourNow)+parseInt('1')) && deliveryTimeInputAmPm == amPmNow && parseInt(hourNow) != parseInt('12') ) )
                        // console.log( ( amPmNow == 'AM' && parseInt(deliveryTimeHour) == parseInt('12') && deliveryTimeInputAmPm == amPmNow ) )
                        // console.log( ( amPmNow == 'AM' && parseInt(deliveryTimeHour) == parseInt('12') && deliveryTimeInputAmPm == amPmNow && parseInt(deliveryTimeMin) <= parseInt(minNow) ) )
                        // console.log( ( amPmNow == 'AM' && parseInt(deliveryTimeHour) <= (parseInt(hourNow)+parseInt('1')) && parseInt(deliveryTimeMin) <= parseInt(minNow) && deliveryTimeInputAmPm == amPmNow ) )

                        /*for pm*/
                        // console.log( ( amPmNow == 'PM' && parseInt(deliveryTimeHour) < (parseInt(hourNow)+parseInt('1')) && deliveryTimeInputAmPm == amPmNow && parseInt(hourNow) != parseInt('12') ) )
                        // console.log( ( amPmNow == 'PM' && parseInt(deliveryTimeHour) == parseInt('12') && deliveryTimeInputAmPm == amPmNow ) )
                        // console.log( ( amPmNow == 'PM' && parseInt(deliveryTimeHour) == parseInt('12') && deliveryTimeInputAmPm == amPmNow && parseInt(deliveryTimeMin) <= parseInt(minNow) ) )
                        // console.log( ( amPmNow == 'PM' && parseInt(deliveryTimeHour) <= (parseInt(hourNow)+parseInt('1')) && parseInt(deliveryTimeMin) <= parseInt(minNow) && deliveryTimeInputAmPm == amPmNow ) )

                        // console.log( (deliveryTimeInputAmPm != amPmNow && deliveryTimeInputAmPm == 'AM') )

                        /*for am*/
                        if (( amPmNow == 'AM' && parseInt(deliveryTimeHour) < (parseInt(hourNow) + parseInt('1')) && deliveryTimeInputAmPm == amPmNow && parseInt(hourNow) != parseInt('12') ) ||
                            ( amPmNow == 'AM' && parseInt(deliveryTimeHour) == parseInt('12') && deliveryTimeInputAmPm == amPmNow ) ||
                            ( amPmNow == 'AM' && parseInt(deliveryTimeHour) == parseInt('12') && deliveryTimeInputAmPm == amPmNow && parseInt(deliveryTimeMin) <= parseInt(minNow) ) ||
                            ( amPmNow == 'AM' && parseInt(deliveryTimeHour) <= (parseInt(hourNow) + parseInt('1')) && parseInt(deliveryTimeMin) <= parseInt(minNow) && deliveryTimeInputAmPm == amPmNow )) {
                            uiCart.parents('div[modal-id="manual-order-summary"]').find('div.manual-order-error-msg').empty().html('<i class="fa fa-exclamation-triangle"></i> Delivery time should be one hour more than the transaction time.').show();
                            uiCart.parents('div[modal-id="manual-order-summary"]').find('input[name="delivery_time"]').parents('div.date-pickertime').addClass('has-error');
                            error++;
                        }
                        /*for pm*/
                        else if (( amPmNow == 'PM' && parseInt(deliveryTimeHour) < (parseInt(hourNow) + parseInt('1')) && deliveryTimeInputAmPm == amPmNow && parseInt(hourNow) != parseInt('12') ) ||
                                 ( amPmNow == 'PM' && parseInt(deliveryTimeHour) == parseInt('12') && deliveryTimeInputAmPm == amPmNow ) ||
                                 ( amPmNow == 'PM' && parseInt(deliveryTimeHour) == parseInt('12') && deliveryTimeInputAmPm == amPmNow && parseInt(deliveryTimeMin) <= parseInt(minNow) ) ||
                                 ( amPmNow == 'PM' && parseInt(deliveryTimeHour) <= (parseInt(hourNow) + parseInt('1')) && parseInt(deliveryTimeMin) <= parseInt(minNow) && deliveryTimeInputAmPm == amPmNow )) {
                            uiCart.parents('div[modal-id="manual-order-summary"]').find('div.manual-order-error-msg').empty().html('<i class="fa fa-exclamation-triangle"></i> Delivery time should be one hour more than the transaction time.').show();
                            uiCart.parents('div[modal-id="manual-order-summary"]').find('input[name="delivery_time"]').parents('div.date-pickertime').addClass('has-error');
                            error++;
                        }
                        else if (( parseInt(deliveryTimeHour) > parseInt('12') ) || ( parseInt(deliveryTimeMin) > parseInt('59') )) {
                            uiCart.parents('div[modal-id="manual-order-summary"]').find('div.manual-order-error-msg').empty().html('<i class="fa fa-exclamation-triangle"></i> Invalid time.').show();
                            uiCart.parents('div[modal-id="manual-order-summary"]').find('input[name="delivery_time"]').parents('div.date-pickertime').addClass('has-error');
                            error++;
                        }
                        else if ((deliveryTimeInputAmPm != amPmNow && deliveryTimeInputAmPm == 'AM')) {
                            uiCart.parents('div[modal-id="manual-order-summary"]').find('div.manual-order-error-msg').empty().html('<i class="fa fa-exclamation-triangle"></i> Delivery time should be one hour more than the transaction time.').show();
                            uiCart.parents('div[modal-id="manual-order-summary"]').find('input[name="delivery_time"]').parents('div.date-pickertime').addClass('has-error');
                            error++;
                        }
                    }
                }
                if($.find('tr.unavailable:visible').length > 0)
                {
                    $('div[modal-id="manual-order-summary"]').find('div.error-msg').html('<i class="fa fa-exclamation-triangle margin-right-10"></i>Sorry, red marked product(s) is unavailable.').show()
                    error++;
                }

                if (error == 0 /*&& $.find('tr.unavailable:visible').length == 0*/) {
                    if ($('section#order-process').find('input[name="order_id"]:hidden').val() != '') {
                        var oOrderData = cr8v.get_from_storage('orders', '')[0].filter(function (el){
                                        return el['id'] == $('section#order-process').find('input[name="order_id"]:hidden').val();
                                    });
                        
                        if(oOrderData != 'undefined')
                        {
                            var orderDateExact = moment(oOrderData[0].date_added_exact);
                            var date = moment().format('YYYY-MM-DD HH:mm:ss')
                            var time = moment(date);
                            
                            // console.log(time.diff(orderDateExact, 'seconds'))

                            if(time.diff(orderDateExact, 'seconds') < parseInt('300')) //five minutes in seconds, edit order if less than five minutes
                            {
                                // console.log("run edit order")
                                callcenter.coordinator_management.show_spinner(uiThis, true);
                                $('div[modal-id="manual-order-summary"]').find('div.error-msg').html('<i class="fa fa-exclamation-triangle margin-right-10"></i>Please fill up all requred fields.').hide()
                                callcenter.order_process.assemble_order_update_information(uiCart, uiThis);
                            }
                            else
                            {
                                $('div[modal-id="manual-order-summary"]').find('div.error-msg').html('<i class="fa fa-exclamation-triangle margin-right-10"></i>Edit order failed, five minutes has passed.').show()
                            }
                        }
                    }
                    else {
                        callcenter.order_process.assemble_order_information(uiCart, uiThis);
                        $('div[modal-id="manual-order-summary"]').find('div.error-msg').html('<i class="fa fa-exclamation-triangle margin-right-10"></i>Please fill up all requred fields.').hide()
                        callcenter.coordinator_management.show_spinner(uiThis, true);
                    }
                }
            });

            $('section.content #search_customer_content').on('click', 'button.proceed_order_from_search', function () {

                var uiContainer = $(this).parents('div.search_result_container:first');
                //$('section.content #order_process_content').find('tr.cart-item:not(.template)').remove();
                var uiAddress = uiContainer.find('div.customer_address_container').find('div.customer_address:not(.template):first');
                var uiRTA = uiContainer.find('div.found-retail').find('span.retail-store-name');
                var uiCart = $('section.content #order_process_content').find('table.cart-table-table');
                callcenter.order_process.check_products_unavailability(uiCart);
                var iStoreCode = (typeof(uiRTA.attr('store_code')) !== 'undefined') ? uiRTA.attr('store_code') : 0;
                var iStoreID = (typeof(uiRTA.attr('store_id')) !== 'undefined') ? uiRTA.attr('store_id') : 0;
                var iStoreTime = (typeof(uiRTA.attr('store_time')) !== 'undefined') ? uiRTA.attr('store_time') : 0;
                var sStoreName = (typeof(uiRTA.attr('store_name')) !== 'undefined') ? uiRTA.attr('store_name') : '';

                var iAddressID = (typeof(uiAddress.attr('address_id')) !== 'undefined') ? uiAddress.attr('address_id') : 0;
                var sAddressText = uiAddress.find('p.info_address_complete').text();
                //uiAddressTemplate.find('p.info_address_complete').attr('data-address','/##/' + house_number + '/##/' + building + '/##/' + floor + '/##/' + street + '/##/' + barangay + '/##/' + subdivision + '/##/' + city + '/##/' + province);
                var sAddressData = uiAddress.find('p.info_address_complete').attr('data-address');
                var sAddressLandmark = uiAddress.find('p.info_address_landmark').text();
                var sImage = uiAddress.find('img.info_address_type').clone();
                var sAddressLabel = uiAddress.find('strong.info_address_label').text();
                var sAddressType = uiAddress.find('strong.info_address_type').text();
                var sCustomerName = uiContainer.find('div.header-search strong.info_name').text();
                var sCustomerMobile = uiContainer.find('strong.contact_number').text();
                var sIsPWD = (typeof(uiContainer.attr('is_pwd')) != 'undefined') ? uiContainer.attr('is_pwd') : 0 ;
                var sIsSenior = (typeof(uiContainer.attr('is_senior')) != 'undefined') ? uiContainer.attr('is_senior') : 0 ;

                var altMobile;
                $.each(uiContainer.find('div.alternate_contact_number div.frm-custom-dropdown-option div.option:not(:first)'), function (key, value){
                    altMobile += '<p>'+$(value).text()+',</p>';
                })
                // altMobile = altMobile.replace('undefined','')
                var sCustomerMobileAlternate = (typeof(altMobile) !== 'undefined')? altMobile.replace('undefined','') : '';
                // var sCustomerMobileAlternate = uiContainer.find('div.alternate_contact_number div.frm-custom-dropdown-option div.option:not(:first)').text();
                var iCustomerID = uiContainer.attr('data-customer-id');
                sAddressData = (typeof(sAddressData) !== 'undefined') ? sAddressData.split('/##/') : '';

                var sHappyPlusData = [];
                if(uiContainer.find('div.happy_plus_card_container div.happy_plus_card:not(.template)').length > 0)
                {
                    var hpcContainer = uiContainer.find('div.happy_plus_card_container div.happy_plus_card:not(.template)');
                    $.each(hpcContainer, function (key, value){
                        sHappyPlusData.push({'id' : $(value).attr('hpc_id'), 'number' : $(value).attr('hpc_number'), 'date' : $(value).attr('hpc_date'), 'remarks' : $(value).attr('hpc_remarks')})
                    });
                }

                var iOrderType = $(this).attr('data-order-rta');
                var oOrderInfo = {
                    'address_id'               : iAddressID,
                    'address_text'             : sAddressText,
                    'address_landmark'         : sAddressLandmark,
                    'address_label'            : sAddressLabel,
                    'address_type'             : sAddressType,
                    'address_type_image'       : sImage,
                    'customer_full_name'       : sCustomerName,
                    'customer_mobile'          : sCustomerMobile.replace('Contact Num:', ''),
                    'customer_mobile_alternate': sCustomerMobileAlternate,
                    'customer_id'              : iCustomerID,
                    'store_code'               : iStoreCode,
                    'store_id'                 : iStoreID,
                    'store_time'               : iStoreTime,
                    'store_name'               : sStoreName,
                    'house_number'             : /*sAddressData[1]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[1] : ''),
                    'building'                 : /*sAddressData[2]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[2] : ''),
                    'floor'                    : /*sAddressData[3]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[3] : ''),
                    'street'                   : /*sAddressData[4]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[4] : ''),
                    'second_street'            : /*sAddressData[5]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[5] : ''),
                    'barangay'                 : /*sAddressData[6]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[6] : ''),
                    'subdivision'              : /*sAddressData[7]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[7] : ''),
                    'city'                     : /*sAddressData[8]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[8] : ''),
                    'province'                 : /*sAddressData[9]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[9] : ''),
                    'order_type'               : iOrderType,
                    'hpc_data'                 : sHappyPlusData,
                    'is_pwd'                   : sIsPWD,
                    'is_senior'                : sIsSenior
                }

                callcenter.order_process.populate_order_information(oOrderInfo);
                $('section.content #order_process_content').find('button.show_last_order').prop('disabled', false);
                callcenter.call_orders.get_order_history($('section.content #order_process_content div.meal-record-container'), iCustomerID);
                callcenter.order_process.identify_order_rta($(this).attr('data-order-rta'));

            });

            $('section.content #order_process_content').on('click', 'div.product-details div.meal-add-ons-option', function () {
                var uiParentInput = $(this).parents('div.frm-custom-dropdown:first').find('input[name="meal-add-ons"]');
                var uiParentInputValue = uiParentInput.val();
                if(uiParentInputValue != $(this).attr('data-value'))
                {
                var uiMeal = $(this).parents('div.meal:first')
                var iAddonQuantity = $(this).parents('div.meal:first').find('input.meal-number-inner-input').val();
                var oItemBreakDown = {
                    sProductName : $(this).attr('data-value'),
                    iProductPrice: $(this).attr('data-price'),
                    iQuantity    : iAddonQuantity,
                    iProductID   : $(this).attr('data-product-id'),
                    iGroupID     : $(this).attr('data-addon-group-id')
                }

                //add additional attribute if this is addon from upgrades
                if($(this).hasClass('option-upgrades') == true)
                {
                    oItemBreakDown.upgradeInput = $(this).attr('data-input');
                    uiMeal.find('tr.product-breakdown-item[data-input="'+oItemBreakDown.upgradeInput+'"]').remove();        
                }

                var uiProductBreakdownRow = uiMeal.find('tr[data-addon-group-id="' + $(this).attr('data-addon-group-id') + '"]');
                if (uiProductBreakdownRow.length > 0 && $(this).hasClass('option-upgrades') == false) {
                    uiProductBreakdownRow.remove();
                }

                if(oItemBreakDown.iProductID != 3361 && oItemBreakDown.iProductID != 3398 && oItemBreakDown.iProductID != 3817) //if No Upgrades is selected
                {
                    callcenter.order_process.attach_item_breakdown(uiMeal, oItemBreakDown, true);
                }
                else //make quantity to zero
                {
                    var uiFormDropDownParent = $(this).parents('div.frm-custom-dropdown:first');
                    var uiDropdown = $(this).parents('div.add-ons-select-container:first').find('div.frm-custom-dropdown');
                    var iDropdownIndex = uiDropdown.index(uiFormDropDownParent);
                    iDropdownIndex += 1;
                    uiMeal.find('input.addon-count-' + iDropdownIndex).val(0);
                }



                callcenter.order_process.compute_breakdown_price(uiMeal);
                callcenter.order_process.compute_product_total(uiMeal);       
                }        
                

            });

            $('section.content #order_process_content').on('click', 'div.product-details div.meal-upgrades', function () {
                var uiParentInput = $(this).parents('div.frm-custom-dropdown:first').find('input[name="meal-upgrades"]');
                var uiParentInputValue = uiParentInput.val();
                if(uiParentInputValue != $(this).attr('data-value'))
                {
                      var iProductID = $(this).attr('data-product-id');
                var uiMeal = $(this).parents('div.meal:first')
                var iMealQuantity = uiMeal.find('input.meal-number-inner-input').val();
                var oItemBreakDown = {
                    sProductName : $(this).attr('data-value'),
                    iProductPrice: $(this).attr('data-price'),
                    iQuantity    : iMealQuantity,
                    bUpgrade : true,
                    iUpgradeID   : $(this).attr('data-upgrade-product-id'),
                    iProductID   : $(this).attr('data-product-id'),
                    //iGroupID     : $(this).attr('data-addon-group-id')
                }

                if(typeof(uiMeal) != 'undefined' && iProductID != 'undefined')
                {
                    var oProducts = cr8v.get_from_storage('products', '');
                    if(typeof(oProducts) != 'undefined')
                    {
                        oProducts = oProducts[0];
                        var oFilteredProducts = oProducts.filter(function(el){
                            return el['id'] == iProductID

                        })


                        var iAddon = 0;
                        if(typeof(oFilteredProducts) != 'undefined')
                        {
                            $.each(oFilteredProducts, function(key, product) {
                                if(product.add_ons != null)
                                {
                                    product.add_ons_detail = {};
                                    $.each(product.add_ons.group, function(key, addon) {
                                        if(typeof(addon[Object.keys(addon)[0]]) != 'undefined')
                                        {
                                            var filtered = oProducts.filter(function (el) {
                                                return $.inArray(el['id'], addon[Object.keys(addon)[0]]) > -1
                                            });
                                            var addongroupid = Object.getOwnPropertyNames(addon)[0];
                                            product.add_ons_detail[addongroupid] = filtered
                                            iAddon++;
                                        }

                                    })
                                }
                            })

                        }
                    }


                }

                //populate addons of upgrades
                if(typeof(oFilteredProducts) != 'undefined')
                {
                    oFilteredProducts = oFilteredProducts[0]

                    if (typeof(uiMeal) != 'undefined' && typeof(oFilteredProducts) != 'undefined') {
                        var sHtml = '';
                        /*$.each(oProductResult.add_ons, function (key, value) {
                         sHtml += '<div class="option meal-add-ons-option" data-input="addon-count-1" data-product-id="' + value[0].id + '" data-value="' + value[0].name + '" data-price="' + value[0].price[0].price + '">' + value[0].name + '</div>'
                         })*/
                        var oAddons = [];
                        //var iAddon = 0;
                        var oSpecialOptions = [];
                        var iSpecialOptions = 0;
                        var sAddons = '';
                        if (typeof(oFilteredProducts.add_ons_detail) != 'undefined' && oFilteredProducts.add_ons_detail != null) {
                            $.each(oFilteredProducts.add_ons_detail, function (key, addon_group_id) {
                                //iAddon++;
                                oAddons[key] = '';
                                sAddons += '<div class="option option-upgrades meal-add-ons-option" data-addon-group-id="' + key + '" data-input="" data-product-id="3398" data-value="No Addons" data-price="0.00">No Addons</div>';
                                $.each(addon_group_id, function (addon_key, addon_details) {
                                    sAddons += '<div class="option option-upgrades meal-add-ons-option" data-addon-group-id="' + key + '" data-input="addon-count-'+iAddon+'" data-product-id="' + addon_details.id + '" data-value="' + addon_details.name + '" data-price="' + addon_details.price[0].price + '">' + addon_details.name + '</div>';
                                })
                                

                            })
                        }
                    }

                    var oNewAddons = [];
                    var iNewAddons = 0;
                    if (typeof(oAddons) !== 'undefined') {
                        $.each(oAddons, function (key, value) {
                            if (typeof(value) !== 'undefined') {
                                iNewAddons++;
                                oNewAddons[iNewAddons] = value;
                            }

                        })
                    }

                }

                if(iAddon > 0)
                {
                    uiMeal.find('div.addon-container').removeClass('hidden');
                    var uiAddonButton  = uiMeal.find('a.add_add_on');
                    if (typeof(iAddon) != 'undefined') {

                        while (iAddon > 1) {
                            uiAddonButton.trigger('click');
                            iAddon--;
                        }
                        var uiSelect = uiMeal.find('select[name="meal-add-ons"]');
                        //console.log(uiAddonTemplate.add_ons)
                        $.each(uiSelect, function (key, select) {
                            // console.log(uiAddonTemplate.add_ons[key + 1]);
                            if (typeof(sAddons) != 'undefined') {
                                $(select).prev('div.frm-custom-dropdown').find('div.frm-custom-dropdown-option').html(sAddons);
                                $(select).prev('div.frm-custom-dropdown').find('div.frm-custom-dropdown-option div.option').attr('data-input', 'addon-count-' + (key + 1))
                            }
                        })



                    }
                }


                var uiProductBreakdownRow = uiMeal.find('tr[data-product-id="' + $(this).attr('data-product-id') + '"]');
                if (uiProductBreakdownRow.length > 0) {
                    uiProductBreakdownRow.remove();
                }


                callcenter.order_process.attach_item_breakdown(uiMeal, oItemBreakDown, true);


                callcenter.order_process.compute_breakdown_price(uiMeal);
                callcenter.order_process.compute_product_total(uiMeal);       
                }
               


            });

            $('section.content #order_process_content').on('click', 'div.product-details div.meal-special-option', function () {

                var uiMeal = $(this).parents('div.meal:first');

                if($(this).hasClass('no-special') == true) {
                    var sAttr = $(this).siblings('div.meal-special-option:first').attr('data-input');
                    var iAddons = uiMeal.find('tbody.add_ons_container').find('tr.product-breakdown-item');
                    if(typeof(iAddons) != 'undefined')
                    {
                                $.each(iAddons, function(key, element) {
                                      if($(element).attr('data-input') == sAttr)
                                      {
                                           $(element).remove();
                                      }
                                })
                    }
                }
                else
                {
                var oItemBreakDown = {
                    sProductName : $(this).attr('data-value'),
                    iProductPrice: 0,
                    iQuantity    : 0,
                    iProductID   : 0,
                    iSpecialProductID : $(this).attr('data-special-option-id'),
                    bSpecial     : true,
                    sSpecialOption : $(this).attr('data-input')
                }

                var uiProductBreakdownRow = uiMeal.find('tr[data-input="' + $(this).attr('data-input') + '"]');
                if (uiProductBreakdownRow.length > 0) {
                    uiProductBreakdownRow.remove();
                }

                callcenter.order_process.attach_item_breakdown(uiMeal, oItemBreakDown, false);


                }


                callcenter.order_process.compute_breakdown_price(uiMeal);
                callcenter.order_process.compute_product_total(uiMeal);


            });


            $('section.content #order_process_content').on('click', 'a.add_add_on', function () {
                var uiThis = $(this);
                var uiMeal = $(this).parents('div.meal:first');
                var iDropdowncount = uiMeal.find('div.add-ons-select-container').find('div.frm-custom-dropdown-option').length + 1;
                var oOptions = uiMeal.find('div.add-ons-select-container').find('div.frm-custom-dropdown-option:first > div.option.meal-add-ons-option').clone();
                uiMeal.find('div.add-ons-select-container').append('<select name="meal-add-ons"><option value="No Add Ons">No Add-Ons</option></select>');
                uiMeal.find('div.add-on-quantity-container').append('<br><input class="xsmall display-inline-top extra-quantity addon-count-' + iDropdowncount + '" type="text" disabled />');
                $('select[name="meal-add-ons"]').transformDD();
                $.each(oOptions, function (key, value) {
                    $(value).attr('data-input', 'addon-count-' + iDropdowncount)
                })
                uiMeal.find('div.add-ons-select-container').find('div.frm-custom-dropdown-option:last').html('').append(oOptions);
            });

            $('section.content #order_process_content').on('click', 'a.special-option', function () {
                var uiThis = $(this);
                var uiMeal = $(this).parents('div.meal:first');
                var iDropdowncount = uiMeal.find('div.special-option-select-container').find('div.frm-custom-dropdown-option').length + 1;
                var oOptions = uiMeal.find('div.special-option-select-container').find('div.frm-custom-dropdown-option:first > div.option.meal-special-option').clone();
                uiMeal.find('div.special-option-select-container').append('<select name="meal-special-option"><option value="No Special Options">No Special Options</option></select>');
                uiMeal.find('div.special-option-quantity-container').append('<br><input class="xsmall display-inline-top extra-special-quantity" data-input="special-option-count-' + iDropdowncount + '" type="text" disabled />');
                $('select[name="meal-special-option"]').transformDD();
                $.each(oOptions, function (key, value) {
                    $(value).attr('data-input', 'special-option-count-' + iDropdowncount)
                })
                uiMeal.find('div.special-option-select-container').find('div.frm-custom-dropdown-option:last').html('').append(oOptions);
            });

            $('section.content #order_process_content').on('click', 'div.meal-add-ons-option', function () {
                var uiMeal = $(this).parents('div.meal:first');
                var uiThis = $(this);
                var uiInputTarget = uiThis.attr('data-input');
                if(uiThis.attr('data-product-id') != 3361 && uiThis.attr('data-product-id') != 3398 && uiThis.attr('data-product-id') != 3817) //if No Upgrades is selected
                {

                    uiMeal.find('input.' + uiInputTarget + '').val(uiMeal.find('input.meal-number-inner-input').val()).prop('disabled', true).attr('data-product-id', uiThis.attr('data-product-id'));
                }
                else //make quantity of no upgrades 0
                {
                    uiMeal.find('input.' + uiInputTarget + '').val(0).prop('disabled', true).attr('data-product-id', uiThis.attr('data-product-id'));
                }
                //uiMeal.find('input.' + uiInputTarget + '').val(1).prop('disabled', false).attr('data-product-id', uiThis.attr('data-product-id'));

                var uiMealParent = $(this).parents('div.product-details:first');
                callcenter.order_process.compute_product_total(uiMealParent);
            });

            $('section.content #order_process_content').on('click', 'div.meal-special-option', function () {
                var uiMeal = $(this).parents('div.meal:first');
                var uiThis = $(this);
                var uiInputTarget = uiThis.attr('data-input');
                uiMeal.find('input[data-input="'+uiInputTarget+'"]').val(1).prop('disabled', false).attr('data-product-id', uiThis.attr('data-product-id'));
                //uiMeal.find('input.' + uiInputTarget + '').val(1).prop('disabled', true).attr('data-product-id', uiThis.attr('data-product-id'));
                var uiMealParent = $(this).parents('div.product-details:first');
                callcenter.order_process.compute_product_total(uiMealParent);
            });

            $('section.content #order_process_content').on('click', 'a.add_extra_item', function () {
                var uiThis = $(this);
                var uiMeal = $(this).parents('div.meal:first');
                var iDropdowncount = uiMeal.find('div.extra-items-select-container').find('div.frm-custom-dropdown-option').length + 1;
                var oOptions = uiMeal.find('div.extra-items-select-container').find('div.frm-custom-dropdown-option:first > div.option.meal-add-ons-option').clone();
                uiMeal.find('div.extra-items-select-container').append('<select name="meal-extra-items"><option value="No Add Ons">No Extra Items</option></select>');
                uiMeal.find('div.extra-items-quantity-container').append('<br><input class="xsmall display-inline-top extra-quantity extra-item-count-' + iDropdowncount + '" type="text" disabled />');
                $('select[name="meal-extra-items"]').transformDD();
                $.each(oOptions, function (key, value) {
                    $(value).attr('data-input', 'extra-item-count-' + iDropdowncount)
                })
                uiMeal.find('div.extra-items-select-container').find('div.frm-custom-dropdown-option:last').html('').append(oOptions);
            });

            $('section.content #order_process_content').on('click', 'div.meal-extra-items-option', function () {
                var uiMeal = $(this).parents('div.meal:first');
                var uiThis = $(this);
                var uiInputTarget = uiThis.attr('data-input');
                uiMeal.find('input.' + uiInputTarget + '').val(1).prop('disabled', false).attr('data-product-id', uiThis.attr('data-product-id'));
                var uiMealParent = $(this).parents('div.product-details:first');
                callcenter.order_process.compute_product_total(uiMealParent);
            });

            $('section.content #order_process_content').on('change keyup', 'input.extra-quantity', function () {
                var uiMeal = $(this).parents('div.meal:first');
                var uiMealParent = $(this).parents('div.product-details:first');
                var iProductID = $(this).attr('data-product-id');
                var uiProductBreakdownRow = uiMeal.find('tr[data-product-id="' + iProductID + '"]')
                uiProductBreakdownRow.attr('data-product-quantity', $(this).val())
                uiProductBreakdownRow.find('p.product-quantity').text($(this).val())
                callcenter.order_process.compute_breakdown_price(uiMeal);
                callcenter.order_process.compute_product_total(uiMealParent);
            });

            

            $('body').on('change keyup', 'input.extra-special-quantity', function () {
                var uiMeal = $(this).parents('div.meal:first');
                var uiMealParent = $(this).parents('div.product-details:first');
                var iSpecialRequiredSpecialItemQuantity = parseInt(uiMealParent.attr('data-special-option-quantity'));
                var sInput = $(this).attr('data-input');
                
                var iSpecialOptionsVal = 0;
                 $('section.content #order_process_content input.extra-special-quantity:not(:disabled)').each(function (i, elem) {
                    if($(this).val()!=""){
                       var iValue = parseInt($(this).val());   
                        iSpecialOptionsVal = iSpecialOptionsVal + iValue;      
                    } 

                    if(iSpecialOptionsVal > iSpecialRequiredSpecialItemQuantity){
                         $(this).val(0); 
                         $(this).trigger('change');
                         $(this).trigger('keyup');
                    }
//                     else if($(this).val() < iSpecialRequiredSpecialItemQuantity)
//                     {
//                         $(this).siblings('input.extra-special-quantity:first').val(parseInt(iSpecialRequiredSpecialItemQuantity - iSpecialOptionsVal ))        
//                     }

                 });

                 

                if($(this).val() > 0)
                {
                    var uiProductBreakdownRow = uiMeal.find('tr[data-input="' + sInput + '"]').removeClass("removed-upgrade").show();
                    uiProductBreakdownRow.attr('data-product-quantity', $(this).val())
                    uiProductBreakdownRow.find('p.product-quantity').text($(this).val())
                    callcenter.order_process.compute_breakdown_price(uiMeal);
                    callcenter.order_process.compute_product_total(uiMealParent);
                }
                else
                {
                    //$(this).val(1); 
                    var uiProductBreakdownRow = uiMeal.find('tr[data-input="' + sInput + '"]').addClass("removed-upgrade").hide();
                    uiProductBreakdownRow.attr('data-product-quantity', $(this).val())
                    uiProductBreakdownRow.find('p.product-quantity').text($(this).val())
                    callcenter.order_process.compute_breakdown_price(uiMeal);
                    callcenter.order_process.compute_product_total(uiMealParent);       
                }


            });

//             $('section.content #order_process_content').on('keyup change', 'input.extra-special-quantity', function(e) {
//                    var iQuantity = $(this).parents('div[data-special-option-quantity]').attr('data-special-option-quantity');   
//                    //console.log(e.originalEvent)
//                    if(e.originalEvent)
//                    {
//                          var sInput = $(this).attr('data-input');
//                          var sProductName = $(this).parents('div.meal').find('div.meal-special-option[data-input="'+sInput+'"]').attr('data-value');      
//                          var uiPartner = $(this).siblings('input.extra-special-quantity:first'); 
//                          var sPartnerInput = uiPartner.attr('data-input');
//                          var sPartnerProductName = uiPartner.parents('div.meal').find('div.meal-special-option[data-input="'+sPartnerInput+'"]').attr('data-value');      
//                          if($(this).val() >= iQuantity)
//                          {
//                               $(this).val(iQuantity).siblings('input.extra-special-quantity').val(0);        
//                               $(this).parents('div.meal').find('tr[data-product-name="'+sProductName+'"]').attr('data-product-quantity', iQuantity) 
//                               $(this).parents('div.meal').find('tr[data-product-name="'+sProductName+'"]').find('p.product-quantity').text(iQuantity)
//                              // $(this).siblings('input.extra-special-quantity').val(iQuantity - $(this).val());
//                          }
//                          else if($(this).val() < iQuantity)
//                          {
//                               $(this).siblings('input.extra-special-quantity').val(iQuantity - parseInt(sPartnerInput));        
//                               $(this).parents('div.meal').find('tr[data-product-name="'+sProductName+'"]').attr('data-product-quantity', $(this).val()) 
//                               $(this).parents('div.meal').find('tr[data-product-name="'+sProductName+'"]').find('p.product-quantity').text($(this).val())
//                               $(this).parents('div.meal').find('tr[data-product-name="'+sPartnerProductName+'"]').attr('data-product-quantity', iQuantity - $(this).val()) 
//                               $(this).parents('div.meal').find('tr[data-product-name="'+sPartnerProductName+'"]').find('p.product-quantity').text(iQuantity - $(this).val())
//                              // $(this).siblings('input.extra-special-quantity').val(iQuantity - $(this).val());
                              



//                               //$(this).parents('div.meal').find('tr[data-product-name="'+sProductName+'"]').attr('data-product-quantity', iQuantity) 
//                               //$(this).parents('div.meal').find('tr[data-product-name="'+sProductName+'"]').find('p.product-quantity').text(iQuantity)
//                               $(this).siblings('input.extra-special-quantity').val(iQuantity - $(this).val());       
                                  
//                              // $(this).siblings('input.extra-special-quantity').val(iQuantity - $(this).val());         
//                          }      
//                    }        
//             }),

            $("section.content #order_process_content").on('click', 'div.arrow-right', function () {
                if ($("section.content #order_process_content div.order_history_container div:visible").next().length != 0) {
                    $("section.content #order_process_content div.order_history_container div:visible").next().show().prev().hide();
                }
                else {
                    $("section.content #order_process_content div.order_history_container div:visible").hide();
                    $("section.content #order_process_content div.order_history_container div:first").show();

                }
                var uiShown = $("section.content #order_process_content div.order_history_container div:visible");
                var iOrderID = uiShown.attr('data-order-id');
                var sDate = uiShown.parents('div.content-container:first').find('div.order_date_option.option[data-value="' + iOrderID + '"]:first').text();
                uiShown.parents('div.content-container:first').find('div.frm-custom-dropdown-txt').find('input').val(sDate);
                return false;
            });

            $("section.content #order_process_content").on('click', 'div.arrow-left', function () {
                if ($("section.content #order_process_content div.order_history_container div:visible").prev().length != 0)
                    $("section.content #order_process_content div.order_history_container div:visible").prev().show().next().hide();
                else {
                    $("section.content #order_process_content div.order_history_container div:visible").hide();
                    $("section.content #order_process_content div.order_history_container div:last").show();

                }
                var uiShown = $("section.content #order_process_content div.order_history_container div:visible");
                var iOrderID = uiShown.attr('data-order-id');

                var sDate = uiShown.parents('div.content-container:first').find('div.order_date_option.option[data-value="' + iOrderID + '"]:first').text();
                uiShown.parents('div.content-container:first').find('div.frm-custom-dropdown-txt').find('input').val(sDate);
                return false;
            });

            $('section.content #order_process_content').on('click', 'div.order_date_option', function () {
                var iOrderID = $(this).attr('data-value');
                $('section.content #order_process_content').find('div.order_history_container').children('div').hide();
                $('section.content #order_process_content').find('div.order_history_container').find('div[data-order-id="' + iOrderID + '"]').show();
            });

            $('section.content #order_process_content').on('click', 'button.show_last_order', function () {
                $(this).toggleText('Show Last Order', 'Hide Last Order');
                $(this).parents('div.meal-record-container:first').find('div.last_order').toggle();
            })

            $('section.content #order_process_content').on('keyup', 'input.meal-number-inner-input', function () {
                if ($(this).val() != "0") {
                    var uiParents = $(this).parents('div.product-details:first:visible');
                    var uiMeal = $(this).parents('div.meal:first:visible');
                    uiMeal.find('tbody.main_product_container').find('tr.product-breakdown-item').attr('data-product-quantity', $(this).val()).find('p.product-quantity').text($(this).val());
                    var iInnerMealTotal = callcenter.order_process.check_all_inner_meal_total();
                    $('input.number_of_meals:visible').val(iInnerMealTotal);

                    uiMeal.find('tbody.main_product_container').find('p.product-quantity').text(iInnerMealTotal)
                    uiMeal.find('tbody.main_product_container').find('tr[class="product-breakdown-item"][class!="template"]').attr('data-product-quantity', iInnerMealTotal)
                    uiMeal.find('tbody.add_ons_container').find('tr[data-special-product-id!="1"]').find('p.product-quantity').text(iInnerMealTotal)
                    uiMeal.find('tbody.add_ons_container').find('tr[class="product-breakdown-item"][data-special-product-id!="1"][class!="template"]').attr('data-product-quantity', iInnerMealTotal)
                    uiMeal.find('input.extra-quantity').val($(this).val())                
                    callcenter.order_process.compute_breakdown_price(uiMeal);
                    callcenter.order_process.compute_product_total(uiParents);
                }
                else {
                    $(this).val(1);
                }


            })

            $('section.content #order_process_content').on('change', 'input.number_of_meals', function () {
                if ($(this).val() != 0) {
                    var iMealCount = $(this).parents('div.product-details:first').find('div.meal').length;
                    var uiVal = $(this).val();
                    var uiPlus = $(this).parents('div.meal-number:first').find('div.plus');
                    var uiMinus = $(this).parents('div.meal-number:first').find('div.minus');
                    var iInnerMealTotal = callcenter.order_process.check_all_inner_meal_total();
                    if (uiVal < iInnerMealTotal) {
                        $(this).val(iInnerMealTotal);
                    }
                    else {
                        if (uiVal > iInnerMealTotal) {
                            var count = uiVal - iInnerMealTotal;
                            while (count != 0) {
                                uiPlus.trigger('click');
                                count--;
                            }


                        }
                        else if (uiVal < iInnerMealTotal) {
                            var count = iInnerMealTotal - uiVal;
                            while (count != 0) {
                                uiMinus.trigger('click');
                                count--;
                            }

                            //minus
                        }
                    }
                }
                else {
                    $(this).val(1)
                }


            })

            $('section.content #order_process_content').on('change', 'input.meal-number-inner-input', function () {
                var iInnerMealTotal = callcenter.order_process.check_all_inner_meal_total();
                var uiPlus = $(this).parents('div.product-details:first').find('div.plus');

                var iOuterMealQuantity = $(this).parents('div.product-details:first').find('input.number_of_meals').val();

                if (iInnerMealTotal > iOuterMealQuantity) {
                    $(this).parents('div.product-details:first').find('input.number_of_meals').val(iInnerMealTotal);
                }
                else if (iOuterMealQuantity > iInnerMealTotal) {
                    //                       var diff =   iOuterMealQuantity - iInnerMealTotal;
                    //                       diff-= parseInt($(this).val());
                    //                       while (diff != 0) {
                    //                         uiPlus.trigger('click');
                    //                         diff--;
                    //                     }
                }
            });

            $('section.content #order_process_content').on('click', 'div.option.payment_type', function () {
                var iPaymentType = $(this).attr('data-value');
                //alert(iPaymentType);
                if(iPaymentType == 1)
                {
                    $('section.content #order_process_content').find('input[name="change_for"]').val('').parents('div.change_for:first').show();        
                    $('div[modal-id="manual-order-summary"]').find('input[name="change_for"]').val('').parents('div.change_for:first').show().prev('label').show();          
                }
                else
                {
                    $('section.content #order_process_content').find('input[name="change_for"]').val('').parents('div.change_for:first').hide();        
                    $('div[modal-id="manual-order-summary"]').find('input[name="change_for"]').val('').parents('div.change_for:first').hide().prev('label').hide();  
                }
                $('section.content #order_process_content #order_customer_information').find('input[name="payment_type"]').val(iPaymentType);
            });

            $('div[modal-id="manual-order-complete"]').on('click', 'div.close-me, button.close-me', function () {
                $(this).parents('div[modal-id="manual-order-complete"]').removeClass('showed');
                $("body").css({overflow: 'auto'});
            })

            $('div[modal-id="manual-order-complete"]').on('click', 'button.back_to_customer_search', function () {
                callcenter.call_orders.content_panel_toggle('search_customer');
                $('section#order-process div.search_customer').addClass('hidden');
            })

            $('section.content #order_process_content').on('change', 'input[name="change_for"]', function () {
                var reg = /^\d*\.?\d*$/;

                if (reg.test($(this).val())) {
                    $(this).parents('div.change_for:first').removeClass('has-error');
                    $(this).attr('value', $(this).val())
                    $('div[modal-id="manual-order-summary"]').find('input[name="change_for"]').val($(this).val());
                    $('div[modal-id="manual-order-summary"]').find('input[name="change_for"]').attr('value', $(this).val());
                }
                else {
                    $(this).parents('div.change_for:first').addClass('has-error');
                }
            })

            $('div[modal-id="manual-order-summary"]').on('change', 'input[name="change_for"]', function () {
                var reg = /^\d*\.?\d*$/;

                if (reg.test($(this).val())) {
                    $(this).parents('div.change_for:first').removeClass('has-error');
                    $(this).attr('value', $(this).val())
                    $('section.content #order_process_content').find('input[name="change_for"]').val($(this).val());
                    $('section.content #order_process_content').find('input[name="change_for"]').attr('value', $(this).val());
                }
                else {
                    $(this).parents('div.change_for:first').addClass('has-error');
                }
            })

            $('section.content #order_process_content').on('click', 'div.change_for div.option', function () {
                var dataValue = $(this).attr("data-value")
                $(this).parents('div.change_for:first').find('input[name="change_for"]').attr('value', dataValue);
                $('div[modal-id="manual-order-summary"]').find('input[name="change_for"]').attr('value', dataValue);
                $('div[modal-id="manual-order-summary"]').find('input[name="change_for"]').val(dataValue);
            })

            $('div[modal-id="manual-order-summary"]').on('click', 'div.change_for div.option', function () {
                var dataValue = $(this).attr("data-value")
                $(this).parents('div.change_for:first').find('input[name="change_for"]').attr('value', dataValue);
                $('section.content #order_process_content').find('input[name="change_for"]').attr('value', dataValue);
                $('section.content #order_process_content').find('input[name="change_for"]').val(dataValue);
            })

            $('section.content #order_process_content').on('focus', 'input[name="change_for"]', function () {
                $(this).addClass('input_numeric')
            })

            $('div[modal-id="manual-order-summary"]').on('focus', 'input[name="change_for"]', function () {
                $(this).addClass('input_numeric')
            })


            $('section.content #order_process_content').on('click', 'button.cancel_order', function () {
                callcenter.call_orders.content_panel_toggle('promo')
            })

            $('section.content #order_process_content').on('click', 'button.add_last_meal', function () {
                var uiLastMeal = $('section.content #order_process_content').find('div.order_history_container').find('div.order_history:visible');
                var items = uiLastMeal.find('tr.product-breakdown-item:not(.template)');

                callcenter.order_process.add_to_cart(null, uiLastMeal);
            })

            $('section.content #search_customer_content').on('click', 'button.add_last_meal_to_order', function () {
                var uiLastMeal = $('section.content #search_customer_content').find('div.order_history_container').find('div.order_history:visible');
                //////console.log(uiLastMeal)
                //$('section.content #order_process_content').find('tr.cart-item:not(.template)').remove();
                var items = uiLastMeal.find('tr.product-breakdown-item:not(.template)');
                callcenter.call_orders.content_panel_toggle('order_process');
                $('section.content #order_process_content').find('div.meal-record-container').find('div.no-order-cart').addClass('hidden');
                $('section.content #order_process_content').find('div.meal-record-container').find('div.cart-table').removeClass('hidden');
                $('section.content #order_process_content').find('div.meal-record-container').find('table.cart-table-table').removeClass('hidden').show();

                $('section.content #order_process_content').find('button.show_bill_breakdown').prop('disabled', false);
                $('section.content #order_process_content').find('button.advance_order').prop('disabled', false);

                var uiContainer = $(this).parents('div.search_result_container:first');
                var uiAddress = uiContainer.find('div.customer_address_container').find('div.customer_address:not(.template):first');
                var uiRTA = uiContainer.find('div.found-retail').find('span.retail-store-name');

                var iStoreCode = (typeof(uiRTA.attr('store_code')) !== 'undefined') ? uiRTA.attr('store_code') : 0;
                var iStoreID = (typeof(uiRTA.attr('store_id')) !== 'undefined') ? uiRTA.attr('store_id') : 0;
                var iStoreTime = (typeof(uiRTA.attr('store_time')) !== 'undefined') ? uiRTA.attr('store_time') : 0;
                var sStoreName = (typeof(uiRTA.attr('store_name')) !== 'undefined') ? uiRTA.attr('store_name') : '';

                var iAddressID = (typeof(uiAddress.attr('address_id')) !== 'undefined') ? uiAddress.attr('address_id') : 0;
                var sAddressText = uiAddress.find('p.info_address_complete').text();
                //uiAddressTemplate.find('p.info_address_complete').attr('data-address','/##/' + house_number + '/##/' + building + '/##/' + floor + '/##/' + street + '/##/' + barangay + '/##/' + subdivision + '/##/' + city + '/##/' + province);
                var sAddressData = uiAddress.find('p.info_address_complete').attr('data-address');
                var sAddressLandmark = uiAddress.find('p.info_address_landmark').text();
                var sAddressType = uiAddress.find('strong.info_address_type').text();
                var sImage = uiAddress.find('img.info_address_type').clone();
                var sAddressLabel = uiAddress.find('strong.info_address_label').text();
                var sCustomerName = uiContainer.find('strong.info_name:first').text();
                var sCustomerMobile = uiContainer.find('strong.contact_number:first').text();
                var sIsPWD = (typeof(uiContainer.attr('is_pwd')) != 'undefined') ? uiContainer.attr('is_pwd') : 0;
                var sIsSenior = (typeof(uiContainer.attr('is_senior')) != 'undefined') ? uiContainer.attr('is_senior') : 0;

                var altMobile;
                $.each(uiContainer.find('div.alternate_contact_number div.frm-custom-dropdown-option div.option:not(:first)'), function (key, value){
                    altMobile += '<p>'+$(value).text()+',</p>';
                })
                // altMobile = altMobile.replace('undefined','')
                var sCustomerMobileAlternate = (typeof(altMobile) !== 'undefined')? altMobile.replace('undefined','') : '';
                // var sCustomerMobileAlternate = uiContainer.find('div.alternate_contact_number div.frm-custom-dropdown-option div.option:not(:first)').text();
                var iCustomerID = uiContainer.attr('data-customer-id');
                sAddressData = (typeof(sAddressData) !== 'undefined') ? sAddressData.split('/##/') : '';

                var sHappyPlusData = [];
                if(uiContainer.find('div.happy_plus_card_container div.happy_plus_card:not(.template)').length > 0)
                {
                    var hpcContainer = uiContainer.find('div.happy_plus_card_container div.happy_plus_card:not(.template)');
                    $.each(hpcContainer, function (key, value){
                        sHappyPlusData.push({'id' : $(value).attr('hpc_id'), 'number' : $(value).attr('hpc_number'), 'date' : $(value).attr('hpc_date'), 'remarks' : $(value).attr('hpc_remarks')})
                    });
                }

                var iOrderType = $(this).attr('data-order-rta');
                var oOrderInfo = {
                    'address_id'               : iAddressID,
                    'address_text'             : sAddressText,
                    'address_landmark'         : sAddressLandmark,
                    'address_label'            : sAddressLabel,
                    'address_type_image'       : sImage,
                    'customer_full_name'       : sCustomerName,
                    'customer_mobile'          : sCustomerMobile.replace('Contact Num:', ''),
                    'customer_mobile_alternate': sCustomerMobileAlternate,
                    'customer_id'              : iCustomerID,
                    'store_code'               : iStoreCode,
                    'store_id'                 : iStoreID,
                    'store_time'               : iStoreTime,
                    'store_name'               : sStoreName,
                    'house_number'             : /*sAddressData[1]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[1] : ''),
                    'building'                 : /*sAddressData[2]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[2] : ''),
                    'floor'                    : /*sAddressData[3]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[3] : ''),
                    'street'                   : /*sAddressData[4]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[4] : ''),
                    'second_street'            : /*sAddressData[5]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[5] : ''),
                    'barangay'                 : /*sAddressData[6]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[6] : ''),
                    'subdivision'              : /*sAddressData[7]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[7] : ''),
                    'city'                     : /*sAddressData[8]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[8] : ''),
                    'province'                 : /*sAddressData[9]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[9] : ''),
                    'order_type'               : iOrderType,
                    'hpc_data'                 : sHappyPlusData,
                    'is_pwd'                   : sIsPWD,
                    'is_senior'                : sIsSenior
                }

                //////console.log(oOrderInfo)
                callcenter.order_process.populate_order_information(oOrderInfo);
                $('section.content #order_process_content').find('button.show_last_order').prop('disabled', false);
                callcenter.call_orders.get_order_history($('section.content #order_process_content div.meal-record-container'), iCustomerID);

                callcenter.order_process.identify_order_rta($(this).attr('data-order-rta'));
                callcenter.order_process.add_to_cart(null, uiLastMeal);
            })

            callcenter.order_process.get_remarks();

            $('div[modal-id="manual-order-complete"]').on('click', 'button.confirm_behavior_modal', function () {
                var iBehaviorID = $(this).attr('behavior_id');
                var iCustomerID = $(this).attr('customer_id');
                if(typeof(iBehaviorID) !== 'undefined' && typeof(iCustomerID) !== 'undefined')
                {
                    var oAjaxConfig = {
                        "type"   : "POST",
                        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                        "data"   : {
                            "params": {
                                "data": [{
                                    "field": "last_behavior_id",
                                    "value": iBehaviorID
                                }
                                ],

                                "qualifiers": [{
                                    "field"   : "id",
                                    "value"   : iCustomerID,
                                    "operator": "="
                                }]
                            }
                        },
                        "url"    : callcenter.config('url.api.jfc.customers') + "/customers/update",
                        "success": function (oData) {
                            // alert('update success');
                        }
                    }

                    callcenter.call_orders.get(oAjaxConfig)
                }

            })

            $('div[modal-id="manual-order-complete"]').on('click', 'div.behavior-option', function () {
                $('div[modal-id="manual-order-complete"]').find('button.confirm_behavior_modal').attr('behavior_id', $(this).attr('data-value'))
            })

            $('section.content #order_process_content').on('click', 'a.hide_order_breakdown', function () {
                $(this).parents('tbody').siblings().toggle();
                $(this).parents('tr').siblings().toggle();
                $(this).toggleText('Hide Order Breakdown', 'Show Order Breakdown');
            });

            $('section.content #order_process_content').on('click', 'button.advance_order', function () {
                $('div[modal-id="manual-order-complete"]').find('h4.order_type').text('Advance Order Complete');
                $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('15');
                setTimeout(function(){
                    var hourNow = moment().format('h'),
                        minNow = moment().format('mm'),
                        amPmNow = moment().format('A'),
                        hourplus = parseInt(hourNow) + parseInt('1'),
                        minplus = parseInt(minNow) + parseInt('1');
                    minplus = (minplus < parseInt('10')) ? '0' + minplus : minplus;
                    if (hourplus > parseInt('12')) {
                        hourplus = parseInt('1');
                        if (minplus > parseInt('59')) {
                            minplus = parseInt('00');
                        }
                    }
                    else {
                        if (minplus > parseInt('59')) {
                            minplus = parseInt('00');
                        }
                    }
                    $('div[modal-id="manual-order-summary"]').find('input[name="delivery_time"]').trigger('click');
                    $('div[modal-id="manual-order-summary"]').find('input[name="delivery_time"]').trigger('focus');
                    $('div[modal-id="manual-order-summary"]').find('input[name="delivery_date"]').trigger('click');
                    $('div.bootstrap-datetimepicker-widget:visible').find('div.timepicker:visible span.timepicker-hour').text((hourplus < parseInt('10')) ? '0' + hourplus : hourplus)
                    $('div.bootstrap-datetimepicker-widget:visible').find('div.timepicker:visible span.timepicker-minute').text(minplus)
                    $('div.bootstrap-datetimepicker-widget:visible').find('div.timepicker:visible button[data-action="togglePeriod"]').text(amPmNow)
                    $('div.bootstrap-datetimepicker-widget:visible').hide();
                }, 200);

            });

            $('div[modal-id="manual-order-summary"]').on('focus', 'input[name="delivery_time"]', function () {
                var uiThis = $(this);
                var hourNow = moment().format('h'),
                    minNow = moment().format('mm'),
                    amPmNow = moment().format('A'),
                    hourplus = parseInt(hourNow) + parseInt('1'),
                    minplus = parseInt(minNow) + parseInt('1');
                minplus = (minplus < parseInt('10')) ? '0' + minplus : minplus;
                if (hourplus > parseInt('12')) {
                    hourplus = parseInt('1');
                    if (minplus > parseInt('59')) {
                        minplus = parseInt('00');
                    }
                    uiThis.val(hourplus + ':' + minplus + ' ' + amPmNow)
                }
                else {
                    if (minplus > parseInt('59')) {
                        minplus = parseInt('00');
                    }
                    uiThis.val(hourplus + ':' + minplus + ' ' + amPmNow)
                }
                setTimeout(function (uiThis) {
                    uiThis.parents().find('div.timepicker:visible span.timepicker-hour').text((hourplus < parseInt('10')) ? '0' + hourplus : hourplus)
                    uiThis.parents().find('div.timepicker:visible span.timepicker-minute').text(minplus)
                    uiThis.parents().find('div.timepicker:visible button[data-action="togglePeriod"]').text(amPmNow)
                }, 200, uiThis)
            });

            $('section.content #order_process_content').on('click', 'button.send_order', function () {
                var orderType = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_type"]').val()
                //var customerType = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="customer_type"]').val()
                var customerType = 'new';
                // var existingCustomerConfigVal = parseInt('2500');
                var newCustomerConfigVal = parseFloat('2500.00');
                var total_bill = parseFloat($('div.cart-container table.cart-table-table').find('td.total_bill').attr('data-total-bill'));

                var uiRTA = $('section.content #order_process_content').find('strong.store_name');
                var iStoreID = (typeof(uiRTA.attr('store_id')) !== 'undefined') ? uiRTA.attr('store_id') : 0;
                var bAvailability = callcenter.gis.check_store_availability(iStoreID);

                var orderStatus = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val();

                if (orderStatus != '22' && iStoreID != 0) {
                    if (bAvailability) {
                        //if online
                        if (customerType === 'existing') {
                            // if(total_bill > existingCustomerConfigVal)
                            // {
                            //     $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                            //     $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
                            //     $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                            // }
                            // else
                            // {
                            $('section.content #order_process_content').find('button.send_order').text('Send Order');
                            $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
                            $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('23');
                            // }
                        }
                        if (customerType === 'new') {
                            if (total_bill > newCustomerConfigVal) {
                                $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                                $('section.content #order_process_content').find('button.advance_order')/*.text('Verify Advance Order')*/.hide();
                                $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                            }
                            else {
                                $('section.content #order_process_content').find('button.send_order').text('Send Order');
                                $('section.content #order_process_content').find('button.advance_order').text('Advance Order').show();
                                $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('23');
                            }
                        }
                    }
                    else {
                        //if offline
                        if (customerType === 'existing') {
                            // if(total_bill > existingCustomerConfigVal)
                            // {
                            //     $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                            //     $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
                            //     $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                            // }
                            // else
                            // {
                            $('section.content #order_process_content').find('button.send_order').text('Manual Order');
                            $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
                            $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
                            // }
                        }
                        if (customerType === 'new') {
                            if (total_bill > newCustomerConfigVal) {
                                $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                                $('section.content #order_process_content').find('button.advance_order')/*.text('Verify Advance Order')*/.hide();
                                $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                            }
                            else {
                                $('section.content #order_process_content').find('button.send_order').text('Manual Order');
                                $('section.content #order_process_content').find('button.advance_order').text('Advance Order').show();
                                $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
                            }
                        }
                    }
                }
                else {
                    if (customerType === 'existing') {
                        // if(total_bill > existingCustomerConfigVal)
                        // {
                        //     $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                        //     $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
                        //     $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                        // }
                        // else
                        // {
                        $('section.content #order_process_content').find('button.send_order').text('Parked Order');
                        $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('22');
                        // }
                    }
                    if (customerType === 'new') {
                        if (total_bill > newCustomerConfigVal) {
                            $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                            $('section.content #order_process_content').find('button.advance_order')/*.text('Verify Advance Order')*/.hide();
                            $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                        }
                        else {
                            $('section.content #order_process_content').find('button.send_order').text('Parked Order');
                            $('section.content #order_process_content').find('button.advance_order').text('Advance Order').show();
                            $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('22');
                        }
                    }
                }
            });

            //toggling product unavailability
            $('section.content #order_process_content').on('click', 'a.toggle_unavailable_products', function(){
                if($(this).html()=="Show"){
                    $(this).html("Hide");
                    $('section.content #order_process_content div.unavailable_products_container').removeClass("hidden");
                }else{
                    $(this).html("Show");
                    $('section.content #order_process_content div.unavailable_products_container').addClass("hidden");
                }
            });

            //this part removes the upgrade
            $('section.content #order_process_content').on('click', 'div.meal-upgrades.no-upgrades', function() {
                var uiThis = $(this);
                var iAddons = uiThis.parents('div.meal:first').find('tbody.add_ons_container').find('tr');

                if(typeof(iAddons) != 'undefined'){
                    $.each(iAddons, function(key, element) {
                        if($(element).attr('data-add-on') == 1 && $(element).attr('data-special-item') != 1)
                        {
                            $(element).remove();
                        }
                    })
                }

                var uiAddonContainer = uiThis.parents('div.meal:first').find('div.addon-container');
                uiAddonContainer.find('div.add-ons-select-container').find('div.frm-custom-dropdown:not(:first)').remove();
                uiAddonContainer.find('div.add-on-quantity-container').find('input.extra-quantity:not(:first)').remove();
                uiAddonContainer.find('div.add-on-quantity-container').find('br:not(:first)').remove();
                uiAddonContainer.addClass('hidden');
            });
        },

        'reiterate_meal_number' : function(uiMealContainer) {
            //console.log("here");
            if(typeof(uiMealContainer) != 'undefined')
            {
                var uiMeals = uiMealContainer.find('div.meal');
                if(typeof(uiMeals) != 'undefined')
                {       //console.log("here 2");
                    $.each(uiMeals, function(key, element) {
                        var iIndex = key + 1;
                        $(element).find('strong.meal-number-collapse').text(iIndex + ".")  ;
                    })
                }
            }
        },

        'get_remarks': function () {
            var oAjaxConfig = {
                "type"   : "GET",
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "data"   : {"params": "none"},
                "url"    : callcenter.config('url.api.jfc.customers') + "/customers/behaviors",
                "success": function (oData) {
                    var uiContainer = $('div[modal-id="manual-order-complete"]').find('div.frm-custom-dropdown-option');

                    if(typeof(oData[0]) != 'undefined')
                    {
                        $('div[modal-id="manual-order-complete"]').find('input.dd-txt').val(oData[0].display_label)
                        $('div[modal-id="manual-order-complete"]').find('button.confirm_behavior_modal').attr('behavior_id', oData[0].id)
                    }

                    var uiCustomerListHeaderBehavior = $("section #customer_list_header div#customer_list_search_by_behavior")
                        .children("div.select")
                        .children("div.frm-custom-dropdown")
                        .children("div.frm-custom-dropdown-option");
                    if (oData) {

                        uiContainer.html("");
                        var sHtml = '';
                        var sCustomerListHeaderBehaviorHtml = '';
                        $.each(oData, function (key, behavior) {
                            sHtml += '<div class="option behavior-option" data-value="' + behavior.id + '">' + behavior.display_label + '</div>';
                            sCustomerListHeaderBehaviorHtml += '<div class="option" data-value="' + behavior.id + '">' + behavior.display_label + '</div>';
                        })


                        uiContainer.append(sHtml);
                        uiCustomerListHeaderBehavior.append(sCustomerListHeaderBehaviorHtml);
                    }

                }
            }

            callcenter.call_orders.get(oAjaxConfig)
        },

        'identify_order_rta': function (iOrderRTA) {
            if (typeof(iOrderRTA) !== 'undefined') {
                var uiOrderPage = $('section.content #order_process_content');
                if (iOrderRTA == 1) {
                    uiOrderPage.find('button.send_order').text('Send Order');
                }
                else if (iOrderRTA == 4) {
                    uiOrderPage.find('button.send_order').text('Manual Order');
                }
                else if (iOrderRTA == 5) {
                    uiOrderPage.find('button.send_order').text('Park Order');
                }
                else {
                    alert('iOrderRTA missing');
                }

                var uiCart = $('section.content #order_process_content').find('table.cart-table-table');
                callcenter.order_process.compute_bill_breakdown(uiCart)
            }
        },

        'populate_order_information': function (oOrderInfo) {
            if (typeof(oOrderInfo) !== 'undefined') {
                var uiContent = $('section.content #order_process_content');
                // console.log(oOrderInfo)
                var uiCustomerInformation = $('div.order_customer_information.template').clone().removeClass('template');
                uiCustomerInformation.find('td.info-customer-full-name').text(oOrderInfo.customer_full_name);
                uiCustomerInformation.find('td.info-customer-mobile').text(oOrderInfo.customer_mobile);
                uiCustomerInformation.find('td.info-customer-mobile-alternate').html(oOrderInfo.customer_mobile_alternate);

                var uiAddress = uiContent.find('div.order_primary_address.template').clone().removeClass('template');
                var addressTypeImg = $(oOrderInfo.address_type_image).clone()
                uiAddress.find('img.address_type_image').replaceWith(addressTypeImg);
                uiAddress.find('strong.address_type').text(oOrderInfo.address_type);
                uiAddress.find('p.address_text').text(oOrderInfo.address_text);
                uiAddress.find('p.address_landmark').text(oOrderInfo.address_landmark);
                uiAddress.find('strong.address_label').text(oOrderInfo.address_label);
                uiAddress.attr('address_id', oOrderInfo.address_id);

                if (oOrderInfo.store_id != 0) // if there is rta store found
                {
                    var bAvailability = callcenter.gis.check_store_availability(oOrderInfo.store_id);
                    var uiRTA = $('div.rta_store.template').clone().removeClass('template');
                    // uiRTA.find('strong.store_name').text(oOrderInfo.store_name)
                    // uiRTA.find('strong.store_name').attr('store_id', oOrderInfo.store_id)
                    // uiRTA.find('strong.store_code').text(oOrderInfo.store_code)
                    // uiRTA.find('td.store_time').text(oOrderInfo.store_time + ' Minutes');

                    (typeof(oOrderInfo.store_time) !== 'undefined') ? uiRTA.find('td.store_time').text(oOrderInfo.store_time + ' Minutes') : uiRTA.find('td.store_time').text('0');
                    (typeof(oOrderInfo.store_code) !== 'undefined') ? uiRTA.find('strong.store_code').text(oOrderInfo.store_code) : uiRTA.find('strong.store_code').text('0');

                    if (typeof(oOrderInfo.store_name) !== 'undefined') {
                        if (bAvailability) {
                            uiRTA.find('strong.store_name').text(oOrderInfo.store_name).attr('store_id', oOrderInfo.store_id)
                        }
                        else {
                            uiRTA.find('strong.store_name').text(oOrderInfo.store_name + ' (Offline)').css('color', 'red').attr('store_id', oOrderInfo.store_id);
                        }
                    }
                    else {
                        uiRTA.find('strong.store_name').text('').attr('store_id', oOrderInfo.store_id);
                    }

                }
                else {
                    var uiRTA = $('div.no_rta_store.template').clone().removeClass('template');
                }

                var uiBillBreakdown = $('div.billbreakdown.template').clone().removeClass('template');
                uiBillBreakdown.find('strong.info-customer-full-name').text(oOrderInfo.customer_full_name);
                uiBillBreakdown.find('strong.info-customer-mobile').text(oOrderInfo.customer_mobile);
                uiBillBreakdown.find('strong.info-customer-mobile-alternate').html(oOrderInfo.customer_mobile_alternate);
                uiBillBreakdown.find('strong.info-customer-mobile-alternate').find('p').addClass('font-12');
                var addressTypeImg = $(oOrderInfo.address_type_image).clone()
                uiBillBreakdown.find('img.address_type_image').replaceWith(addressTypeImg);
                uiBillBreakdown.find('strong.address_label').text(oOrderInfo.address_label);
                uiBillBreakdown.find('p.address_text').text(oOrderInfo.address_text);
                uiBillBreakdown.find('p.address_landmark').text(oOrderInfo.address_landmark);
                uiBillBreakdown.find('strong.address_type').text(oOrderInfo.address_type);

                $('div[modal-id="manual-order-complete"]').find('strong.customer-full-name').text(oOrderInfo.customer_full_name);

                //fetch store announcements

                var uiHiddenForm = uiContent.find('form#order_customer_information');
                uiHiddenForm.find('input[name="address_id"]').val(oOrderInfo.address_id);
                uiHiddenForm.find('input[name="customer_id"]').val(oOrderInfo.customer_id);
                uiHiddenForm.find('input[name="store_id"]').val(oOrderInfo.store_id);

                uiHiddenForm.find('input[name="house_number"]').val(oOrderInfo.house_number);
                uiHiddenForm.find('input[name="building"]').val(oOrderInfo.building);
                uiHiddenForm.find('input[name="floor"]').val(oOrderInfo.floor);
                uiHiddenForm.find('input[name="street"]').val(oOrderInfo.street);
                uiHiddenForm.find('input[name="second_street"]').val(oOrderInfo.second_street);
                uiHiddenForm.find('input[name="order_type"]').val(oOrderInfo.order_type);
                uiHiddenForm.find('input[name="barangay"]').val(oOrderInfo.barangay);
                uiHiddenForm.find('input[name="subdivision"]').val(oOrderInfo.subdivision);
                uiHiddenForm.find('input[name="city"]').val(oOrderInfo.city);
                uiHiddenForm.find('input[name="province"]').val(oOrderInfo.province);

                uiHiddenForm.find('input[name="order_id"]').val(oOrderInfo.order_id);
                uiHiddenForm.find('input[name="hpc_data"]').val(JSON.stringify(oOrderInfo.hpc_data));
                uiHiddenForm.find('input[name="is_pwd"]').val(oOrderInfo.is_pwd);
                uiHiddenForm.find('input[name="is_senior"]').val(oOrderInfo.is_senior);
                uiHiddenForm.find('input[name="serving_time"]').val(oOrderInfo.store_time);

                uiContent.find('div.rta-container').html('');
                $('div.modal-container[modal-id="manual-order-summary"]').find('div.customer-information-container').html('');
                uiContent.find('div.order_customer_information_container').html('');
                uiContent.find('div.order_customer_information_container').html('');
                uiContent.find('div.order_address_container').find('div.order_primary_address:not(.template)').remove();

                if(oOrderInfo.is_senior == 0 && oOrderInfo.is_pwd == 0)
                {
                    uiContent.find('div.discount_container input#shopping-cart-none').prop("checked", true);
                }
                else if(oOrderInfo.is_pwd == 1 && oOrderInfo.is_senior == 0)
                {
                    uiContent.find('div.discount_container input#shopping-cart-pwd').prop("checked", true);
                }
                else if(oOrderInfo.is_senior == 1 && oOrderInfo.is_pwd == 0)
                {
                    uiContent.find('div.discount_container input#shopping-cart-senior').prop("checked", true);
                }

                uiContent.find('div.rta-container').append(uiRTA)
                $('div.modal-container[modal-id="manual-order-summary"]').find('div.customer-information-container').append(uiBillBreakdown)
                uiContent.find('div.order_customer_information_container').append(uiCustomerInformation)
                uiContent.find('div.order_address_container').append(uiAddress);

            }

        },

        'assemble_order_update_information': function (uiCart, uiButton) {
            if (typeof(uiCart) !== 'undefined') {

                var sDeliveryDate = $('div[modal-id="manual-order-summary"]').find('input[name="delivery_date"]').val();
                sDeliveryDate += ' ' + $('div[modal-id="manual-order-summary"]').find('input[name="delivery_time"]').val();
                var order_type = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_type"]').val();
                var oOrderUpdateInfo = {};

                if (sDeliveryDate.length > 5) {
                    order_type = 2;
                }

                var oOrderUpdateInformation =
                    {
                        "data"      : [
                            {
                                "field": "customer_id",
                                "value": $('section.content #order_process_content').find('form#order_customer_information').find('input[name="customer_id"]').val()
                            },
                            {
                                "field": "order_status",
                                "value": $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val()
                            },
                            {
                                "field": "channel_code",
                                "value": "Store"
                            },
                            {
                                "field": "agent_id",
                                "value": iConstantUserId
                            },
                            {
                                "field": "for_pickup",
                                "value": $('section.content #order_process_content').find('input[name="order-mode"]:checked').val()
                            },
                            {
                                "field": "for_special_event",
                                "value": "1"
                            },
                            {
                                "field": "delivery_date",
                                "value": sDeliveryDate
                            },
                            {
                                "field": "deliver_to_customer_address",
                                "value": $('section.content #order_process_content').find('form#order_customer_information').find('input[name="address_id"]').val()
                            },
                            {
                                "field": "sbu_id",
                                "value": "1"
                            },
                            {
                                "field": "store_id",
                                "value": $('section.content #order_process_content').find('form#order_customer_information').find('input[name="store_id"]').val()
                            },
                            {
                                "field": "change_for",
                                "value": $('div[modal-id="manual-order-summary"]').find('input[name="change_for"]').val()
                            },
                            {
                                "field": "order_type",
                                "value": order_type
                            },
                            {
                                "field": "locked_by",
                                "value": '0'
                            },
                            {
                                "field": "is_edited",
                                "value": '0'
                            }

                        ],
                        "qualifiers": [
                            {
                                "field"   : "o.id",
                                "value"   : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_id"]').val(),
                                "operator": "="
                            }
                        ],

                        "items"        : [],
                        "special_items": [],
                        "payment_type": $('section.content #order_process_content').find('form#order_customer_information').find('input[name="payment_type"]').val()
                    }

                /* order configuration orders details to display */
			
                oOrderUpdateInfo.display = {
                    'customer_name'           : $('section.content #order_process_content').find('div.order_customer_information').find("td.info-customer-full-name").text(),
                    'contact_number'          : $('section.content #order_process_content').find('div.order_customer_information').find("td.info-customer-mobile").text().replace(/\n/g, '\\n'),
                    'contact_number_alternate': $('section.content #order_process_content').find('div.order_customer_information').find("td.info-customer-mobile-alternate").text(),
                    'delivery_address'        : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="address_id"]').val(),
                    'payment_type'            : $('section.content #order_process_content').find('form#order_customer_information').find("input[name='payment_type']").val(),
                    'change_for'              : $('div[modal-id="manual-order-summary"]').find('input[name="change_for"]').val(),
                    'remarks'                 : $('section.content #order_process_content').find("textarea#order_remarks").val(),
                    'total_cost'              : $("section#order_process td.total_cost").text(),
                    'added_vat'               : $("section#order_process td.total_vat").text(),
                    'total_bill'              : $("section#order_process td.total_bill").text(),
                    'delivery_charge'         : $("section#order_process td.delivery_charge").text(),
                    'store_summary'           : $('section.content #order_process_content').find('strong.store_name').text(),
                    'store_summary'           : $('section.content #order_process_content').find('strong.store_name').text(),
                    'serving_time'            : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="serving_time"]').val(),
					'discount' 				  : $('section.content #order_process_content').find("input[name='shopping-cart']:checked").val()
                };
                oOrderUpdateInformation.delivery_address = {
                    'house_number': $('section.content #order_process_content').find('form#order_customer_information').find('input[name="house_number"]').val(),
                    'building'    : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="building"]').val(),
                    'floor'       : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="floor"]').val(),
                    'street'      : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="street"]').val(),
                    'second_street': $('section.content #order_process_content').find('form#order_customer_information').find('input[name="second_street"]').val(),
                    'barangay'    : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="barangay"]').val(),
                    'subdivision' : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="subdivision"]').val(),
                    'city'        : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="city"]').val(),
                    'province'    : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="province"]').val(),
                };

                oOrderUpdateInfo.display.full_address = '' + oOrderUpdateInformation.delivery_address.house_number + ' ' + oOrderUpdateInformation.delivery_address.building + ' ' + oOrderUpdateInformation.delivery_address.floor + ' ' + oOrderUpdateInformation.delivery_address.street + ' ' + oOrderUpdateInformation.delivery_address.second_street + ' ' + oOrderUpdateInformation.delivery_address.barangay + ' ' + oOrderUpdateInformation.delivery_address.subdivision + ' ' + oOrderUpdateInformation.delivery_address.city + ' ' + oOrderUpdateInformation.delivery_address.province + ' ';

                var trItems = uiCart.find('tr.cart-item.main:not(.template)');
                var trAddonItems = uiCart.find('tr.cart-item.addon:not(.template)');
                var trSpecialItems = uiCart.find('tr.cart-item.special:not(.template)');
                var trUpgradeItems = uiCart.find('tr.cart-item.upgrade:not(.template)');

                oOrderUpdateInformation.items = [];
                oOrderUpdateInfo.display.products = [];
                if(typeof(trItems) !== 'undefined')
                {
                    $.each(trItems, function (key, element) {

                        var iCartNumber = $(element).attr('cart-item-num');
                        oOrderUpdateInformation.items[iCartNumber] = {};
                        oOrderUpdateInformation.items[iCartNumber].addons = [];
                        oOrderUpdateInformation.items[iCartNumber].special = [];
                        oOrderUpdateInformation.items[iCartNumber].upgrades = [];
                        oOrderUpdateInformation.items[iCartNumber].item_id = $(element).find('td[data-product-id]').attr('data-product-id'),
                            oOrderUpdateInformation.items[iCartNumber].quantity = $(element).find('td[data-product-quantity]').attr('data-product-quantity'),
                            oOrderUpdateInformation.items[iCartNumber].price = $(element).find('td[data-product-price]').attr('data-product-price')

                        oOrderUpdateInfo.display.products[iCartNumber] = {};
                        oOrderUpdateInfo.display.products[iCartNumber].addons = [];
                        oOrderUpdateInfo.display.products[iCartNumber].special = [];
                        oOrderUpdateInfo.display.products[iCartNumber].upgrades = [];
                        oOrderUpdateInfo.display.products[iCartNumber].item_id = $(element).find('td[data-product-id]').attr('data-product-id');
                        oOrderUpdateInfo.display.products[iCartNumber].quantity = $(element).find('td[data-product-quantity]').attr('data-product-quantity');
                        oOrderUpdateInfo.display.products[iCartNumber].price = $(element).find('td[data-product-price]').attr('data-product-price')
                        oOrderUpdateInfo.display.products[iCartNumber].sub_total = $(element).find('td[data-product-row-total]').attr('data-product-row-total')
                        oOrderUpdateInfo.display.products[iCartNumber].product_name = $(element).find('td[data-product-name]').attr("data-product-name")
                        oOrderUpdateInfo.display.products[iCartNumber].main_product_id = 0;
                    });
                }

                if(typeof(trSpecialItems) !== 'undefined')
                {
                    $.each(trSpecialItems, function(key,element) {
                        var iCartNumber = $(element).attr('cart-item-num');
                        oOrderUpdateInformation.items[iCartNumber].special.push({
                            "item_id" : $(element).find('td[data-product-id]').attr('data-special-product-id'),
                            "quantity": $(element).find('td[data-product-quantity]').attr('data-product-quantity'),
                            "price"   : $(element).find('td[data-product-price]').attr('data-product-price')
                        })

                        oOrderUpdateInfo.display.products[iCartNumber].special.push({
                            "item_id"     : $(element).find('td[data-product-id]').attr('data-special-product-id'),
                            "quantity"    : $(element).find('td[data-product-quantity]').attr('data-product-quantity'),
                            "price"       : $(element).find('td[data-product-price]').attr('data-product-price'),
                            "sub_total"   : $(element).find('td[data-product-row-total]').attr('data-product-row-total'),
                            "product_name": $(element).find('td[data-product-name]').attr("data-product-name"),
                            "main_product_id" : oOrderUpdateInformation.items[iCartNumber].item_id
                        });
                    })
                }

                if(typeof(trUpgradeItems) !== 'undefined')
                {
                    $.each(trUpgradeItems, function(key,element) {
                        var iCartNumber = $(element).attr('cart-item-num');
                        oOrderUpdateInformation.items[iCartNumber].upgrades.push({
                            "item_id" : $(element).find('td[data-product-id]').attr('data-upgrade-product-id'),
                            "quantity": $(element).find('td[data-product-quantity]').attr('data-product-quantity'),
                            "price"   : $(element).find('td[data-product-price]').attr('data-product-price')
                        })

                        oOrderUpdateInfo.display.products[iCartNumber].upgrades.push({
                            "item_id"     : $(element).find('td[data-product-id]').attr('data-upgrade-product-id'),
                            "quantity"    : $(element).find('td[data-product-quantity]').attr('data-product-quantity'),
                            "price"       : $(element).find('td[data-product-price]').attr('data-product-price'),
                            "sub_total"   : $(element).find('td[data-product-row-total]').attr('data-product-row-total'),
                            "product_name": $(element).find('td[data-product-name]').attr("data-product-name"),
                            "main_product_id" : oOrderUpdateInformation.items[iCartNumber].item_id
                        });
                    })
                }

                if(typeof(trAddonItems) !== 'undefined')
                {
                    $.each(trAddonItems, function(key,element) {
                        var iCartNumber = $(element).attr('cart-item-num');
                        oOrderUpdateInformation.items[iCartNumber].addons.push({
                            "item_id" : $(element).find('td[data-product-id]').attr('data-product-id'),
                            "quantity": $(element).find('td[data-product-quantity]').attr('data-product-quantity'),
                            "price"   : $(element).find('td[data-product-price]').attr('data-product-price')
                        })

                        oOrderUpdateInfo.display.products[iCartNumber].addons.push({
                            "item_id"     : $(element).find('td[data-product-id]').attr('data-product-id'),
                            "quantity"    : $(element).find('td[data-product-quantity]').attr('data-product-quantity'),
                            "price"       : $(element).find('td[data-product-price]').attr('data-product-price'),
                            "sub_total"   : $(element).find('td[data-product-row-total]').attr('data-product-row-total'),
                            "product_name": $(element).find('td[data-product-name]').attr("data-product-name"),
                            "main_product_id" : oOrderUpdateInformation.items[iCartNumber].item_id
                        });
                    })
                }

                oOrderUpdateInfo.display.total_bill = uiCart.find('td.total_bill').attr('data-total-bill');
                oOrderUpdateInfo.display.total_cost = uiCart.find('td.total_cost').attr('data-total-cost');
                oOrderUpdateInfo.display.delivery_charge = uiCart.find('td.delivery_charge').attr('data-delivery-charge');
                oOrderUpdateInfo.display.added_vat = uiCart.find('td.total_vat').attr('data-total-vat');
                oOrderUpdateInfo.display.happy_plus = (($('section.content #order_process_content').find('form#order_customer_information').find('input[name="hpc_data"]').val() !== '') ? $.parseJSON($('section.content #order_process_content').find('form#order_customer_information').find('input[name="hpc_data"]').val()) : [] )
                // oOrderUpdateInfo.display.order_id = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_id"]').val();


                oOrderUpdateInformation.data.push({
                    "field": "display_data",
                    "value": JSON.stringify(oOrderUpdateInfo.display)
                })

                callcenter.coordinator_management.show_spinner(uiButton, true);
                callcenter.order_process.update_order(oOrderUpdateInformation, uiButton);
            }
        },
        'update_order' : function (oOrderUpdateInformation, uiButton) {

            if (typeof(oOrderUpdateInformation) !== 'undefined') {
                // console.log(oOrderUpdateInformation);
                var oUpdateOrderConfig = {
                    "type"   : "POST",
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "data"   : oOrderUpdateInformation,
                    "url"    : callcenter.config('url.api.jfc.orders') + "/orders/update",
                    "success": function (oData) {
                        if (typeof(oData) !== 'undefined' && oData.status === true) {

                            var iOrderID = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_id"]').val();
                            $('div[modal-id="manual-order-summary"]').find('input[name="change_for"]').parents('div.change_for:first').removeClass('has-error');
                            $('div[modal-id="manual-order-summary"]').find('div.manual-order-error-msg').hide();
                            $('div[modal-id="manual-order-summary"]').removeClass('showed')
                            $('div[modal-id="manual-order-complete"]').find('button.confirm_behavior_modal').attr('customer_id', (oOrderUpdateInformation.data.field == 'customer_id') ? oOrderUpdateInformation.data.value : '');
                            $('div[modal-id="manual-order-complete"]').addClass('showed')
                            $("body").css({overflow: 'auto'});

                            $('section.content #order_process_content').find('form#order_customer_information').find('input[type="hidden"]').val('');
                            $('section.content #order_process_content').find('form#order_customer_information').find('input[name="payment_type"]').val('1');
                            $('section.content #order_process_content').find('div.order_customer_information').addClass('hidden');
                            $('section.content #order_process_content').find('div.content-container div.order_address_container').addClass('hidden');
                            $('section.content #order_process_content').find('div.rta_store').addClass('hidden');

                            $('section.content #order_process_content').find('div.meal-record-container div.no-order-cart').removeClass('hidden').show();
                            $('section.content #order_process_content').find('button.show_last_order').prop('disabled', true);
                            $('section.content #order_process_content').find('button.show_bill_breakdown').prop('disabled', true);
                            $('section.content #order_process_content').find('textarea#order_remarks').val('');
                            $('section.content #order_process_content').find('div.order_history_container div.order_history').remove();

                            $('section.content #order_process_content').find('tr.cart-item:not(.template)').remove();
                            $('section.content #order_process_content').find('div.meal-record-container').find('table.cart-table-table').addClass('hidden');
                            $('div[modal-id="manual-order-summary"]').find('input[name="change_for"]').val('');
                            $('div[modal-id="manual-order-summary"]').find();
                            $('section.content #order_process_content').find('input[name="change_for"]').val('');
                            $('div[modal-id="manual-order-complete"]').find('strong.customer-order-id').text(iOrderID);
                        }
                        else {
                            // alert(JSON.stringify(oData.message));
                            if (typeof(oData.message.error) !== 'undefined') {
                                $('div[modal-id="manual-order-summary"]').find('div.manual-order-error-msg').empty().html('<i class="fa fa-exclamation-triangle"></i> ' + oData.message.error).show();
                                $('div[modal-id="manual-order-summary"]').find('input[name="change_for"]').parents('div.change_for:first').addClass('has-error');
                            }
                        }
                        callcenter.coordinator_management.show_spinner(uiButton, false);

                    }
                }

                callcenter.call_orders.get(oUpdateOrderConfig);
            }
        },

        'fetch_rta_grid': function () {
            var oRtaData = cr8v.get_from_storage('rta_data', ''),
                sRtaDataGrid = '';
            if (typeof(oRtaData) !== 'undefined') {
                /* sample data "9999:0:20:14.47873,121.12444" */
                oRtaData = oRtaData[0];
                var aRtaData = oRtaData.split(':');
                sRtaDataGrid = aRtaData[1];
            }

            return sRtaDataGrid;
        },

        'assemble_order_information': function (uiCart, uiButton) {
            if (typeof(uiCart) !== 'undefined') {

                var sDeliveryDate = $('div[modal-id="manual-order-summary"]').find('input[name="delivery_date"]').val();
                sDeliveryDate += ' ' + $('div[modal-id="manual-order-summary"]').find('input[name="delivery_time"]').val();

                var arrPaymentTypes = [];
                var sPaymentType = "";
                var uiPaymentType = $('section.content #order_process_content').find('input[name="payment_type[]"]:visible');
                if(typeof(uiPaymentType)!== 'undefined')
                {
                    $.each(uiPaymentType, function () {
                        var iPaymentTypeVal = 0;
                        if($(this).val()=="Cash"){
                            iPaymentTypeVal = 1;
                        }else if($(this).val()=="Credit Card"){
                            iPaymentTypeVal = 2;
                        }else if($(this).val()=="Cheque"){
                            iPaymentTypeVal = 3;
                        }else if($(this).val()=="Happy Plus"){
                            iPaymentTypeVal = 4;
                        }else{
                            iPaymentTypeVal = 5;
                        }
                        var sValue = iPaymentTypeVal +',';
                        sPaymentType += sValue;
                    });
                    arrPaymentTypes = sPaymentType.split(",");
                    arrPaymentTypes = jQuery.grep(arrPaymentTypes, function(n, i){
                        return (n !== "" && n != null);
                    });
                    //console.log(arrPaymentTypes);
                }
                //console.log(arrPaymentTypes);

                var trItems = uiCart.find('tr.cart-item.main:not(.template)');
                var trAddonItems = uiCart.find('tr.cart-item.addon:not(.template)');
                var trSpecialItems = uiCart.find('tr.cart-item.special:not(.template)');
                var trUpgradeItems = uiCart.find('tr.cart-item.upgrade:not(.template)');
                var oOrderInformation = {
                    "type"                       : "1",
                    "channel_code"               : "Store Channel",
                    "order_status"               : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val(),
                    "customer_id"                : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="customer_id"]').val(),
                    "agent_id"                   : iConstantUserId,
                    "for_special_event"          : "1",
                    "for_pickup"                 : $('section.content #order_process_content').find('input[name="order-mode"]:checked').val(),
                    "delivery_date"              : sDeliveryDate,
                    "deliver_to_customer_address": $('section.content #order_process_content').find('form#order_customer_information').find('input[name="address_id"]').val(),
                    "store_id"                   : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="store_id"]').val(),
                    "sbu_id"                     : "1",
                    "items"                      : [],
                    "special_items"              : [],
                    "payment_types"               : arrPaymentTypes,
                    "payment_type"               : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="payment_type"]').val(),
                    "change_for"                 : $('div[modal-id="manual-order-summary"]').find('input[name="change_for"]').val(),
                    "order_type"                 : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_type"]').val(),
                    "remarks"                    : $('section.content #order_process_content').find('textarea#order_remarks').val(),
                    "pricing"                    : $('section.content #order_process_content').find('input[name="pricing"]').attr('data-value')
                }

                if (sDeliveryDate.length > 5) {
                    oOrderInformation.order_type = 2;
                }


                /* order configuration orders details to display */
                oOrderInformation.display = {
                    'customer_name'           : $('section.content #order_process_content').find('div.order_customer_information').find("td.info-customer-full-name").text(),
                    'contact_number'          : $("div.order_customer_information:not(.template) td.info-customer-mobile").text(),
                    'contact_number_alternate': $("div.order_customer_information:not(.template) td.info-customer-mobile-alternate").text(),
                    'delivery_address'        : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="address_id"]').val(),
                    'payment_type'            : $("section#order-process input[name='payment_type[]']").val(),
                    'change_for'              : $('div[modal-id="manual-order-summary"]').find('input[name="change_for"]').val(),
                    'remarks'                 : $("section#order-process textarea#order_remarks").val(),
                    'total_cost'              : $("section#order-process td.total_cost").text(),
                    'added_vat'               : $("section#order-process td.total_vat").text(),
                    'total_bill'              : $("section#order-process td.total_bill").text(),
                    'delivery_charge'         : $("section#order-process td.delivery_charge").text(),
                    'store_summary'           : $("section#order-process").find('strong.store_name').text(),
                    'remarks'                 : $('section.content #order_process_content').find('textarea#order_remarks').val(),
                    'rta_grid'                : callcenter.order_process.fetch_rta_grid(),
                    'serving_time'            : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="serving_time"]').val(),
					'discount' 				  : $('section.content #order_process_content').find("input[name='shopping-cart']:checked").val()
				};
                oOrderInformation.display.products = [];
                oOrderInformation.delivery_address = {
                    'house_number': $('section.content #order_process_content').find('form#order_customer_information').find('input[name="house_number"]').val(),
                    'building'    : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="building"]').val(),
                    'floor'       : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="floor"]').val(),
                    'street'      : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="street"]').val(),
                    'second_street': $('section.content #order_process_content').find('form#order_customer_information').find('input[name="second_street"]').val(),
                    'barangay'    : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="barangay"]').val(),
                    'subdivision' : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="subdivision"]').val(),
                    'city'        : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="city"]').val(),
                    'province'    : $('section.content #order_process_content').find('form#order_customer_information').find('input[name="province"]').val(),
                };


                oOrderInformation.display.full_address = '' + oOrderInformation.delivery_address.house_number + ' ' + oOrderInformation.delivery_address.building + ' ' + oOrderInformation.delivery_address.floor + ' ' + oOrderInformation.delivery_address.street + ' ' + oOrderInformation.delivery_address.second_street + ' ' + oOrderInformation.delivery_address.barangay + ' ' + oOrderInformation.delivery_address.subdivision + ' ' + oOrderInformation.delivery_address.city + ' ' + oOrderInformation.delivery_address.province + ' ';
                oOrderInformation.items = [];
                oOrderInformation.display.products = []
                if(typeof(trItems) !== 'undefined')
                {
                    $.each(trItems, function (key, element) {

                        var iCartNumber = $(element).attr('cart-item-num');
                        oOrderInformation.items[iCartNumber] = {};
                        oOrderInformation.items[iCartNumber].addons = [];
                        oOrderInformation.items[iCartNumber].special = [];
                        oOrderInformation.items[iCartNumber].upgrades = [];
                        oOrderInformation.items[iCartNumber].item_id = $(element).find('td[data-product-id]').attr('data-product-id'),
                            oOrderInformation.items[iCartNumber].quantity = $(element).find('td[data-product-quantity]').attr('data-product-quantity'),
                            oOrderInformation.items[iCartNumber].price = $(element).find('td[data-product-price]').attr('data-product-price')

                        oOrderInformation.display.products[iCartNumber] = {};
                        oOrderInformation.display.products[iCartNumber].addons = [];
                        oOrderInformation.display.products[iCartNumber].special = [];
                        oOrderInformation.display.products[iCartNumber].upgrades = [];
                        oOrderInformation.display.products[iCartNumber].item_id = $(element).find('td[data-product-id]').attr('data-product-id');
                        oOrderInformation.display.products[iCartNumber].quantity = $(element).find('td[data-product-quantity]').attr('data-product-quantity');
                        oOrderInformation.display.products[iCartNumber].price = $(element).find('td[data-product-price]').attr('data-product-price')
                        oOrderInformation.display.products[iCartNumber].sub_total = $(element).find('td[data-product-row-total]').attr('data-product-row-total')
                        oOrderInformation.display.products[iCartNumber].product_name = $(element).find('td[data-product-name]').attr("data-product-name")
                        oOrderInformation.display.products[iCartNumber].main_product_id = 0;

                        //oOrderInformation.display.products.push({
                        //     "item_id"     : $(element).find('td[data-product-id]').attr('data-product-id'),
                        //     "quantity"    : $(element).find('td[data-product-quantity]').attr('data-product-quantity'),
                        //     "price"       : $(element).find('td[data-product-price]').attr('data-product-price'),
                        //     "sub_total"   : $(element).find('td[data-product-row-total]').attr('data-product-row-total'),
                        //     "product_name": $(element).find('td[data-product-name]').attr("data-product-name")
                        //});

                    });
                }

                if(typeof(trSpecialItems) !== 'undefined')
                {
                    $.each(trSpecialItems, function(key,element) {
                        var iCartNumber = $(element).attr('cart-item-num');
                        oOrderInformation.items[iCartNumber].special.push({
                            "item_id" : $(element).find('td[data-product-id]').attr('data-special-product-id'),
                            "quantity": $(element).find('td[data-product-quantity]').attr('data-product-quantity'),
                            "price"   : $(element).find('td[data-product-price]').attr('data-product-price')
                        })

                        oOrderInformation.display.products[iCartNumber].special.push({
                            "item_id"     : $(element).find('td[data-product-id]').attr('data-special-product-id'),
                            "quantity"    : $(element).find('td[data-product-quantity]').attr('data-product-quantity'),
                            "price"       : $(element).find('td[data-product-price]').attr('data-product-price'),
                            "sub_total"   : $(element).find('td[data-product-row-total]').attr('data-product-row-total'),
                            "product_name": $(element).find('td[data-product-name]').attr("data-product-name"),
                            "main_product_id" : oOrderInformation.items[iCartNumber].item_id
                        });

                        //oOrderInformation.display.products.push({
                        //    "item_id"     : $(element).find('td[data-product-id]').attr('data-product-id'),
                        //    "quantity"    : $(element).find('td[data-product-quantity]').attr('data-product-quantity'),
                        //    "price"       : $(element).find('td[data-product-price]').attr('data-product-price'),
                        //    "sub_total"   : $(element).find('td[data-product-row-total]').attr('data-product-row-total'),
                        //    "product_name": $(element).find('td[data-product-name]').attr("data-product-name")
                        //});
                    })
                }

                if(typeof(trUpgradeItems) !== 'undefined')
                {
                    $.each(trUpgradeItems, function(key,element) {
                        var iCartNumber = $(element).attr('cart-item-num');
                        oOrderInformation.items[iCartNumber].upgrades.push({
                            "item_id" : $(element).find('td[data-product-id]').attr('data-upgrade-product-id'),
                            "quantity": $(element).find('td[data-product-quantity]').attr('data-product-quantity'),
                            "price"   : $(element).find('td[data-product-price]').attr('data-product-price')
                        })
				
                        oOrderInformation.display.products[iCartNumber].upgrades.push({
                            "item_id"     : $(element).find('td[data-product-id]').attr('data-upgrade-product-id'),
                            "quantity"    : $(element).find('td[data-product-quantity]').attr('data-product-quantity'),
                            "price"       : $(element).find('td[data-product-price]').attr('data-product-price'),
                            "sub_total"   : $(element).find('td[data-product-row-total]').attr('data-product-row-total'),
                            "product_name": $(element).find('td[data-product-name]').attr("data-product-name"),
                            "main_product_id" : oOrderInformation.items[iCartNumber].item_id
                        });

                        //oOrderInformation.display.products.push({
                        //    "item_id"     : $(element).find('td[data-product-id]').attr('data-product-id'),
                        //    "quantity"    : $(element).find('td[data-product-quantity]').attr('data-product-quantity'),
                        //    "price"       : $(element).find('td[data-product-price]').attr('data-product-price'),
                        //    "sub_total"   : $(element).find('td[data-product-row-total]').attr('data-product-row-total'),
                        //    "product_name": $(element).find('td[data-product-name]').attr("data-product-name")
                        //});
                    })
                }

                if(typeof(trAddonItems) !== 'undefined')
                {
                    $.each(trAddonItems, function(key,element) {
                        var iCartNumber = $(element).attr('cart-item-num');
                        oOrderInformation.items[iCartNumber].addons.push({
                            "item_id" : $(element).find('td[data-product-id]').attr('data-product-id'),
                            "quantity": $(element).find('td[data-product-quantity]').attr('data-product-quantity'),
                            "price"   : $(element).find('td[data-product-price]').attr('data-product-price')
                        })

                        oOrderInformation.display.products[iCartNumber].addons.push({
                            "item_id"     : $(element).find('td[data-product-id]').attr('data-product-id'),
                            "quantity"    : $(element).find('td[data-product-quantity]').attr('data-product-quantity'),
                            "price"       : $(element).find('td[data-product-price]').attr('data-product-price'),
                            "sub_total"   : $(element).find('td[data-product-row-total]').attr('data-product-row-total'),
                            "product_name": $(element).find('td[data-product-name]').attr("data-product-name"),
                            "main_product_id" : oOrderInformation.items[iCartNumber].item_id
                        });

                        //oOrderInformation.display.products.push({
                        //    "item_id"     : $(element).find('td[data-product-id]').attr('data-product-id'),
                        //    "quantity"    : $(element).find('td[data-product-quantity]').attr('data-product-quantity'),
                        //    "price"       : $(element).find('td[data-product-price]').attr('data-product-price'),
                        //    "sub_total"   : $(element).find('td[data-product-row-total]').attr('data-product-row-total'),
                        //    "product_name": $(element).find('td[data-product-name]').attr("data-product-name")
                        //});
                    })
                }
                // console.log(oOrderInformation);

                oOrderInformation.display.happy_plus = (($('section.content #order_process_content').find('form#order_customer_information').find('input[name="hpc_data"]').val() !== '') ? $.parseJSON($('section.content #order_process_content').find('form#order_customer_information').find('input[name="hpc_data"]').val()) : [] )
                // uiCart.find('td.total_cost').attr('data-total-cost');
                // uiCart.find('td.total_vat').attr('data-total-vat');
                // uiCart.find('td.total_bill').attr('data-total-bill');
                oOrderInformation.display.total_bill = uiCart.find('td.total_bill').attr('data-total-bill');

                oOrderInformation.display.total_cost = uiCart.find('td.total_cost').attr('data-total-cost');
                oOrderInformation.display.delivery_charge = uiCart.find('td.delivery_charge').attr('data-delivery-charge');
                oOrderInformation.display.added_vat = uiCart.find('td.total_vat').attr('data-total-vat');
                callcenter.coordinator_management.show_spinner(uiButton, true);
                callcenter.order_process.send_order(oOrderInformation, uiButton);
            }


        },

        'send_order': function (oOrderInformation, uiButton) {
            if (typeof(oOrderInformation) != 'undefined') {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "data"   : {
                        "params": oOrderInformation
                    },

                    "url"    : callcenter.config('url.api.jfc.orders') + "/orders/add",
                    "success": function (oData) {
                        // console.log(oData.data)
                        if (typeof(oData) !== 'undefined' && oData.status === true) {
                            $('div[modal-id="manual-order-summary"]').find('input[name="change_for"]').parents('div.change_for:first').removeClass('has-error');
                            $('div[modal-id="manual-order-summary"]').find('div.manual-order-error-msg').hide();
                            $('div[modal-id="manual-order-summary"]').removeClass('showed')
                            $('div[modal-id="manual-order-complete"]').find('button.confirm_behavior_modal').attr('customer_id', oOrderInformation.customer_id);
                            $('div[modal-id="manual-order-complete"]').addClass('showed')
                            $("body").css({overflow: 'auto'});

                            $('section.content #order_process_content').find('form#order_customer_information').find('input[type="hidden"]').val('');
                            $('section.content #order_process_content').find('form#order_customer_information').find('input[name="payment_type"]').val('1');
                            $('section.content #order_process_content').find('div.order_customer_information').addClass('hidden');
                            $('section.content #order_process_content').find('div.content-container div.order_address_container').addClass('hidden');
                            $('section.content #order_process_content').find('div.rta_store').addClass('hidden');

                            $('section.content #order_process_content').find('div.meal-record-container div.no-order-cart').removeClass('hidden').show();
                            $('section.content #order_process_content').find('button.show_last_order').prop('disabled', true);
                            $('section.content #order_process_content').find('button.show_bill_breakdown').prop('disabled', true).show();
                            $('section.content #order_process_content').find('textarea#order_remarks').val('');
                            $('section.content #order_process_content').find('div.order_history_container div.order_history').remove();

                            $('section.content #order_process_content').find('tr.cart-item:not(.template)').remove();
                            $('section.content #order_process_content').find('div.meal-record-container').find('table.cart-table-table').addClass('hidden');
                            $('div[modal-id="manual-order-summary"]').find('input[name="change_for"]').val('');
                            $('section.content #order_process_content').find('input[name="change_for"]').val('');
                            $('div[modal-id="manual-order-complete"]').find('strong.customer-order-id').text(oData.data.order_id);
                        }
                        else {
                            //alert(JSON.stringify(oData.message));
                            if(typeof(oData.message) != 'undefined')
                            {
                                if (typeof(oData.message.error) !== 'undefined') {
                                    $('div[modal-id="manual-order-summary"]').find('div.manual-order-error-msg').empty().html('<i class="fa fa-exclamation-triangle"></i> ' + oData.message.error).show();
                                    $('div[modal-id="manual-order-summary"]').find('input[name="change_for"]').parents('div.change_for:first').addClass('has-error');
                                }
                            }

                        }

                        callcenter.coordinator_management.show_spinner(uiButton, false);

                    }
                }

                callcenter.call_orders.get(oAjaxConfig)

            }
        },

        'get_mode_of_payment': function () {
            var oAjaxConfig = {
                "type"   : "GET",
                "data"   : {"params": "none"},
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : callcenter.config('url.api.jfc.orders') + "/orders/mode_of_payment",
                "success": function (oData) {
                    // //////console.log(oData)
                    if (typeof(oData) !== 'undefined') {
                        var uiDropdownOptions = $('section.content #order_process_content').find('div.select.payment_type').find('div.frm-custom-dropdown-option');
                        uiDropdownOptions.html('');
                        // //////console.log(uiDropdownOptions)
                        $.each(oData, function (key, value) {
                            uiDropdownOptions.append('<div class="option payment_type" data-value="' + key + '">' + value + '</div>')
                        })
                    }
                    else {

                    }

                }
            }

            callcenter.call_orders.get(oAjaxConfig)


        },

        'add_to_cart': function (uiThis, uiProductContainer) {
            if (typeof(uiThis) != 'undefined' && typeof(uiProductContainer) != 'undefined') {
                var iItems = uiProductContainer.find('tr.product-breakdown-item:not(.template):not(.removed-upgrade)');
                var uiCartItem = $('section.content #order_process_content').find('tr.cart-item.template').clone().removeClass('template');

                var sHtml = '';

                if(typeof(uiProductContainer).attr('data-special-option-quantity') != 'undefined') //special option
                {
                      console.log('here');
                      var uiSpecialItems = uiProductContainer.find('tr.product-breakdown-item[data-special-item="1"]');      
                     if(typeof(uiSpecialItems) != 'undefined')
                     {
                                 var iSpecialItemsTotal = 0;
                                 $.each(uiSpecialItems, function(key, element) {
                                     iSpecialItemsTotal += parseInt($(element).attr('data-product-quantity'));
                                 })
                        if(iSpecialItemsTotal != uiProductContainer.attr('data-special-option-quantity'))
                        {
                                 uiProductContainer.find('input.extra-special-quantity').css('border', '1px solid red');  
                                 return false;
                        }
                        else
                        {
                                     var iCartCount = $('div.cart-container table.cart-table-table').find('tr.main.cart-item').length;
                // //////console.log(iItems)
                $.each(iItems, function (key, element) {
                    var iProductPrice = $(element).attr('data-product-price');
                    var iProductQuantity = $(element).attr('data-product-quantity');
                    var iProductTotal = $(element).attr('data-product-row-total');
                    var sProductName = $(element).attr('data-product-name');
                    var iProductID = $(element).attr('data-product-id');
                    var iCoreID = $(element).parents('div.product-details:first').attr('core-id');

                    if(typeof(iCoreID) == 'undefined')
                    {
                        if($(element).attr('data-upgrade') == 1)
                        {
                            var sCoreId = cr8v.get_from_storage('products', '')[0].filter(function (el){
                                return el['name'] == sProductName;
                            });
                            iCoreID = (typeof(sCoreId[0]) != 'undefined' && typeof(sCoreId[0].core_ids) != 'undefined') ? sCoreId[0].core_ids : '';
                        }
                        else
                        {
                            var sCoreId = cr8v.get_from_storage('products', '')[0].filter(function (el){
                                return el['id'] == iProductID;
                            });
                            iCoreID = (typeof(sCoreId[0]) != 'undefined' && typeof(sCoreId[0].core_ids) != 'undefined') ? sCoreId[0].core_ids : '';
                        }
                    }

                    var uiCartItem = $('section.content #order_process_content').find('tr.cart-item.template').clone().removeClass('template');

                    uiCartItem.find('td.product-quantity').text(iProductQuantity).attr('data-product-quantity', iProductQuantity);
                    uiCartItem.find('td.product-price').text(iProductPrice).attr('data-product-price', iProductPrice);
                    uiCartItem.find('td.product-total').text(iProductTotal).attr('data-product-row-total', iProductTotal);

                    if ($(element).attr('data-add-on') == 1 || $(element).attr('data-main-product-id') > 0) {
                        uiCartItem.find('td.product-name').text(sProductName).attr('data-product-name', sProductName).attr('data-product-id', iProductID).attr('data-core-id', iCoreID);
                        if($(element).attr('data-special-item') == 1)
                        {
                            uiCartItem.addClass('special');
                            uiCartItem.find('td.product-name').attr('data-special-product-id', $(element).attr('data-special-product-id'))
                        }
                        else if($(element).attr('data-upgrade') == 1)
                        {
                            uiCartItem.addClass('upgrade')
                            uiCartItem.find('td.product-name').attr('data-upgrade-product-id', $(element).attr('data-upgrade-product-id'))
                        }
                        else
                        {
                            uiCartItem.addClass('addon');
                        }

                        uiCartItem.attr('cart-item-num', iCartCount + 1);
                        uiCartItem.find('a.red-color').remove();
                    }
                    else {
                        uiCartItem.find('td.product-name').html('<div class="arrow-down"></div>' + sProductName).attr('data-product-name', sProductName).attr('data-product-id', iProductID).attr('data-core-id', iCoreID);
                        uiCartItem.addClass('main');
                        iCartCount++;
                        uiCartItem.attr('cart-item-num', iCartCount + 1);
                    }
                    callcenter.order_process.ready_cart(uiCartItem);
                    return true;

                })      
                        }
                     }
                }
                else
                {
                      var iCartCount = $('div.cart-container table.cart-table-table').find('tr.main.cart-item').length;
                // //////console.log(iItems)
                $.each(iItems, function (key, element) {
                    var iProductPrice = $(element).attr('data-product-price');
                    var iProductQuantity = $(element).attr('data-product-quantity');
                    var iProductTotal = $(element).attr('data-product-row-total');
                    var sProductName = $(element).attr('data-product-name');
                    var iProductID = $(element).attr('data-product-id');
                    var iCoreID = $(element).parents('div.product-details:first').attr('core-id');

                    if(typeof(iCoreID) == 'undefined')
                    {
                        if($(element).attr('data-upgrade') == 1)
                        {
                            var sCoreId = cr8v.get_from_storage('products', '')[0].filter(function (el){
                                return el['name'] == sProductName;
                            });
                            iCoreID = (typeof(sCoreId[0]) != 'undefined' && typeof(sCoreId[0].core_ids) != 'undefined') ? sCoreId[0].core_ids : '';
                        }
                        else
                        {
                            var sCoreId = cr8v.get_from_storage('products', '')[0].filter(function (el){
                                return el['id'] == iProductID;
                            });
                            iCoreID = (typeof(sCoreId[0]) != 'undefined' && typeof(sCoreId[0].core_ids) != 'undefined') ? sCoreId[0].core_ids : '';
                        }
                    }

                    var uiCartItem = $('section.content #order_process_content').find('tr.cart-item.template').clone().removeClass('template');

                    uiCartItem.find('td.product-quantity').text(iProductQuantity).attr('data-product-quantity', iProductQuantity);
                    uiCartItem.find('td.product-price').text(iProductPrice).attr('data-product-price', iProductPrice);
                    uiCartItem.find('td.product-total').text(iProductTotal).attr('data-product-row-total', iProductTotal);

                    if ($(element).attr('data-add-on') == 1 || $(element).attr('data-main-product-id') > 0) {
                        uiCartItem.find('td.product-name').text(sProductName).attr('data-product-name', sProductName).attr('data-product-id', iProductID).attr('data-core-id', iCoreID);
                        if($(element).attr('data-special-item') == 1)
                        {
                            uiCartItem.addClass('special');
                            uiCartItem.find('td.product-name').attr('data-special-product-id', $(element).attr('data-special-product-id'))
                        }
                        else if($(element).attr('data-upgrade') == 1)
                        {
                            uiCartItem.addClass('upgrade')
                            uiCartItem.find('td.product-name').attr('data-upgrade-product-id', $(element).attr('data-upgrade-product-id'))
                        }
                        else
                        {
                            uiCartItem.addClass('addon');
                        }

                        uiCartItem.attr('cart-item-num', iCartCount + 1);
                        uiCartItem.find('a.red-color').remove();
                    }
                    else {
                        uiCartItem.find('td.product-name').html('<div class="arrow-down"></div>' + sProductName).attr('data-product-name', sProductName).attr('data-product-id', iProductID).attr('data-core-id', iCoreID);
                        uiCartItem.addClass('main');
                        iCartCount++;
                        uiCartItem.attr('cart-item-num', iCartCount + 1);
                    }
                    callcenter.order_process.ready_cart(uiCartItem);
                    return true;

                })      
                }

                
            }
        },

        'ready_cart': function (uiCartItem) {
            var uiCart = $('section.content #order_process_content').find('table.cart-table-table');

            uiCart.find('tbody.cart-item-container').append(uiCartItem);

            if (uiCart.find('tbody.cart-item-container > tr').length > 0) {
                $('section.content #order_process_content').find('div.no-order-cart').addClass('hidden').hide();
                uiCart.removeClass('hidden').show();
                callcenter.order_process.compute_bill_breakdown(uiCart);
                callcenter.order_process.check_products_unavailability(uiCart);
            }


            //var uiCartItem =  uiCart.find('tr.cart-item').removeClass('template');
            //$('section.content #order_process_content').find('div.no-order-cart').replaceWith(uiCart);
        },

        'compute_bill_breakdown': function (uiCart, iPricing) {
            var uiItemPrices = uiCart.find('td[data-product-row-total]');
            var iTotal = 0;

            if (uiCart.find('tr.cart-item:visible').length > 0) {
                $('section.content #order_process_content').find('div.no-order-cart').addClass('hidden').hide();
                uiCart.removeClass('hidden').show();
                if ($('section.content #order_process_content').find('form#order_customer_information input[name="customer_id"]').val() > 0) {
                    $('div.order_process button.show_bill_breakdown').prop('disabled', false);
                    $('div.order_process button.advance_order').prop('disabled', false);
                }
            }
            else {
                $('section.content #order_process_content').find('div.no-order-cart').removeClass('hidden').show();
                uiCart.addClass('hidden').hide();
                $('div.order_process button.show_bill_breakdown').prop('disabled', true);
                $('div.order_process button.advance_order').prop('disabled', true);
            }

            if(typeof(uiItemPrices) !== 'undefined')
            {
                $.each(uiItemPrices, function (key, element) {
                    var iPrice = parseFloat($(element).attr('data-product-row-total'));
                    iTotal += iPrice;
                })
            }

            if(typeof(iPricing) != 'undefined')
            {
                if(iPricing == 1) //10% Delivery Charge | No Vat
                {
                       // iTotal = (iTotal / 1.1) * 0.1;
            var iVatTotal = 0;
                        var iDeliveryTotal = (iTotal / 1.1) * 0.1;;
            var iBillTotal = iTotal + iVatTotal;
                        //iBillTotal += iDeliveryTotal;
                }
                else if(iPricing == 2) // No Delivery Charge | Vat
                {
                        var iVatTotal = (iTotal * 0.12);
                        var iDeliveryTotal = 0;
                        var iBillTotal = iTotal + iVatTotal;
                        //iBillTotal += iDeliveryTotal;
                }
                else if(iPricing == 3) // No Delivery Charge | No Vat
                {
                        var iVatTotal = 0;
                        var iDeliveryTotal = 0;
                        var iBillTotal = iTotal + iVatTotal;
                        //iBillTotal += iDeliveryTotal;
                }
                else
                {
                        var iVatTotal = (iTotal * 0.12);
                        var iDeliveryTotal = (iTotal / 1.1) * 0.1;;
                        var iBillTotal = iTotal + iVatTotal;
                        //iBillTotal += iDeliveryTotal;
                }
            }
            else // 10% Delivery Charge | Vat
            {
                        var iVatTotal = (iTotal * 0.12);
                        var iDeliveryTotal = (iTotal / 1.1) * 0.1;;
                        var iBillTotal = iTotal + iVatTotal;
                        //iBillTotal += iDeliveryTotal;
            }


            uiCart.find('td.total_cost').attr('data-total-cost', iTotal);
            uiCart.find('td.delivery_charge').attr('data-delivery-charge', iDeliveryTotal.toFixed(2));
            uiCart.find('td.total_vat').attr('data-total-vat', iVatTotal);
			
		
			var fOriginalPrice = iBillTotal;
			var oSrcComputation = {};
			if ( $("#shopping-cart-senior").is(":checked"))
			{
				oSrcComputation = compute_src(iTotal);
				iBillTotal = parseFloat(oSrcComputation.total);
			}
			
            uiCart.find('td.total_bill').attr({
													'data-total-bill': iBillTotal,
													'data-original-price' : fOriginalPrice
											})
            uiCart.find('td.delivery_charge').text(iDeliveryTotal.toFixed(2) + ' PHP');
            uiCart.find('td.total_cost').text(iTotal.toFixed(2) + ' PHP');
            uiCart.find('td.total_vat').text(iVatTotal.toFixed(2) + ' PHP');
            uiCart.find('td.total_bill').text(iBillTotal.toFixed(2) + ' PHP');

            var customerType = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="customer_type"]').val()
            var customerType = 'new';
            // var existingCustomerConfigVal = parseInt('2500');
            var newCustomerConfigVal = parseFloat('2500.00');
            var uiRTA = $('section.content #order_process_content').find('strong.store_name');
            var iStoreID = (typeof(uiRTA.attr('store_id')) !== 'undefined') ? uiRTA.attr('store_id') : 0;
            var bAvailability = callcenter.gis.check_store_availability(iStoreID);
            var orderStatus = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val();

            if (orderStatus != '22' && iStoreID != 0) {
                if (bAvailability) {
                    //if online
                    if (customerType === 'existing') {
                        // if(iBillTotal > existingCustomerConfigVal)
                        // {
                        //     $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                        //     $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
                        //     $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                        // }
                        // else
                        // {
                        $('section.content #order_process_content').find('button.send_order').text('Send Order');
                        $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('23');
                        // }
                    }
                    if (customerType === 'new') {
                        if (parseFloat(iBillTotal) > newCustomerConfigVal) {
                            $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                            $('section.content #order_process_content').find('button.advance_order')/*.text('Verify Advance Order')*/.hide();
                            $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                        }
                        else {
                            $('section.content #order_process_content').find('button.send_order').text('Send Order');
                            $('section.content #order_process_content').find('button.advance_order').text('Advance Order').show();
                            $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('23');
                        }
                    }
                }
                else {
                    //if offline
                    if (customerType === 'existing') {
                        // if(iBillTotal > existingCustomerConfigVal)
                        // {
                        //     $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                        //     $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
                        //     $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                        // }
                        // else
                        // {
                        $('section.content #order_process_content').find('button.send_order').text('Manual Order');
                        $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
                        // }
                    }
                    if (customerType === 'new') {
                        if (parseFloat(iBillTotal) > newCustomerConfigVal) {
                            $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                            $('section.content #order_process_content').find('button.advance_order')/*.text('Verify Advance Order')*/.hide();
                            $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                        }
                        else {
                            $('section.content #order_process_content').find('button.send_order').text('Manual Order');
                            $('section.content #order_process_content').find('button.advance_order').text('Advance Order').show();
                            $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
                        }
                    }
                }
            }
            else {
                if (customerType === 'existing') {
                    // if(iBillTotal > existingCustomerConfigVal)
                    // {
                    //     $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                    //     $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
                    //     $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                    // }
                    // else
                    // {
                    $('section.content #order_process_content').find('button.send_order').text('Parked Order');
                    $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
                    $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('22');
                    // }
                }
                if (customerType === 'new') {
                    if (parseFloat(iBillTotal) > newCustomerConfigVal) {
                        $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                        $('section.content #order_process_content').find('button.advance_order')/*.text('Verify Advance Order')*/.hide();
                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                    }
                    else {
                        $('section.content #order_process_content').find('button.send_order').text('Parked Order');
                        $('section.content #order_process_content').find('button.advance_order').text('Advance Order').show();
                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('22');
                    }
                }
            }
        },

        'attach_item_breakdown': function (uiMeal, oItemBreakDown, isAddon) {
            var uiTrContainer;

            if(typeof(oItemBreakDown) !== 'undefined')
            {
                var uiBreakdownItemTemplate = uiMeal.find('table.product-breakdown tr.product-breakdown-item.template').clone().removeClass('template');
                uiBreakdownItemTemplate.find('p.product-name').html(oItemBreakDown.sProductName);
                uiBreakdownItemTemplate.find('p.product-quantity').text(oItemBreakDown.iQuantity);
                uiBreakdownItemTemplate.find('p.product-price').text(parseFloat(oItemBreakDown.iProductPrice * oItemBreakDown.iQuantity).toFixed(2) + ' PHP');
                uiBreakdownItemTemplate.attr('data-product-quantity', oItemBreakDown.iQuantity);
                uiBreakdownItemTemplate.attr('data-product-name', oItemBreakDown.sProductName);
                uiBreakdownItemTemplate.attr('data-product-id', oItemBreakDown.iProductID);
                if (oItemBreakDown.sSpecialOption !== 'undefined') {
                    uiBreakdownItemTemplate.attr('data-input', oItemBreakDown.sSpecialOption);
                }

                if(oItemBreakDown.upgradeInput != 'undefined')
                {
                    uiBreakdownItemTemplate.attr('data-input', oItemBreakDown.upgradeInput);
                }

                if (oItemBreakDown.bUpgrade != 'undefined') {
                    if(oItemBreakDown.bUpgrade == true)
                    {
                        uiBreakdownItemTemplate.attr('data-upgrade', 1);
                        uiBreakdownItemTemplate.attr('data-upgrade-product-id', oItemBreakDown.iUpgradeID);
                    }
                }

                if (oItemBreakDown.iGroupID !== 'undefined') {
                    uiBreakdownItemTemplate.attr('data-addon-group-id', oItemBreakDown.iGroupID);
                }
                if (oItemBreakDown.bSpecial !== 'undefined' && oItemBreakDown.bSpecial === true ) {
                    uiBreakdownItemTemplate.attr('data-special-item', 1);
                    uiBreakdownItemTemplate.attr('data-special-product-id', oItemBreakDown.iSpecialProductID);
                    uiTrContainer = uiMeal.find('table.product-breakdown tbody.add_ons_container');
                    uiBreakdownItemTemplate.attr('data-add-on', 1);
                }
                else if (isAddon == true) {
                    uiTrContainer = uiMeal.find('table.product-breakdown tbody.add_ons_container');
                    uiBreakdownItemTemplate.attr('data-add-on', 1);
                }
                else {
                    uiTrContainer = uiMeal.find('table.product-breakdown tbody.main_product_container');
                    uiBreakdownItemTemplate.attr('data-add-on', 0);
                }

                uiBreakdownItemTemplate.attr('data-product-price', parseFloat(oItemBreakDown.iProductPrice).toFixed(2));

                if(oItemBreakDown.iProductID != 3361 && oItemBreakDown.iProductID != 3398 && oItemBreakDown.iProductID != 3817) //if No Upgrades is selected
                {
                    uiTrContainer.append(uiBreakdownItemTemplate);
                }

                return uiMeal;
            }
        },

        'reset_template_value': function (uiMeal) {
            uiMeal.find('input.meal-number-inner-input').val(1);

            uiMeal.find('input[name="meal-upgrades"]').val('No Upgrades');
            //uiMeal.find('div.upgrades-select-container').addClass('hidden');

            //uiMeal.find('div.addon-container').addClass('hidden');
            //uiMeal.find('div.addon-container').find('div.frm-custom-dropdown:not(:first)').remove();
            //uiMeal.find('div.addon-container').find('div.add-on-quantity-container').find('input.extra-quantity:not(:first)').remove();
            //uiMeal.find('div.addon-container').find('div.add-on-quantity-container').find('input.extra-quantity:first').val("");
            //uiMeal.find('div.addon-container').find('div.add-on-quantity-container').find('br:not(:first)').remove();
            uiMeal.find('input[name="meal-add-ons"]').val('No Addons');
            uiMeal.find('tr.product-breakdown-item').attr('data-product-quantity', 1).find('p.product-quantity').text(1);
            //uiMeal.find('div.special-option-container').addClass('hidden');

            uiMeal.find('div.special-option-container').find('div.frm-custom-dropdown:not(:first)').remove();
            uiMeal.find('div.special-option-container').find('special-option-quantity-container').find('input.extra-quantity:not(:first)').remove();
            uiMeal.find('div.special-option-container').find('special-option-quantity-container').find('br:not(:first)').remove();
            uiMeal.find('div.special-option-container').find('div.frm-custom-dropdown:not(:first)').remove();
            uiMeal.find('div.special-option-container').find('div.special-option-quantity-container').find('input.extra-special-quantity:not(:first)').remove();
            uiMeal.find('div.special-option-container').find('div.special-option-quantity-container').find('input.extra-special-quantity:first').prop('disabled', true).val("");
            uiMeal.find('div.special-option-container').find('div.special-option-quantity-container').find('br:not(:first)').remove();
            uiMeal.find('input[name="meal-special-option"]').val('No Special Options');
            uiMeal.find('input.extra-quantity').val(1);
            uiMeal.find('tr.product-breakdown-item:not(.template)').remove();
            return uiMeal;
        },

        'check_all_inner_meal_total': function () {

            var uiInputInnerMeal = $('div.meal-container').find('input.meal-number-inner-input:visible');
            var iInnerMealTotal = 0;
            $.each(uiInputInnerMeal, function () {
                iInnerMealTotal += parseInt($(this).val());
            });

            return iInnerMealTotal;
        },

        'counter_inner_meal': function (uiArrow, uiInput, uiVal, fCallback) {
            if (typeof(uiArrow) !== 'undefined' && typeof(uiInput) !== 'undefined' && typeof(uiVal) !== 'undefined' && typeof(fCallback) == 'function') {
                if (uiArrow.hasClass('minus')) {
                    if (uiVal != '1') {
                        uiVal = parseInt(uiVal);
                        uiVal--;

                        uiInput.val(uiVal);
                        fCallback(uiVal);
                        var iInnerMealTotal = callcenter.order_process.check_all_inner_meal_total();


                        $('input.number_of_meals:visible').val(iInnerMealTotal);
                    }
                }
                else {
                    if (uiArrow.hasClass('plus')) {
                        if (uiVal == '') {
                            uiInput.val("1");
                        }
                        else {
                            var uiMealCount = parseInt($('input.number_of_meals:visible').val());
                            ////////console.log(uiVal, parseInt($('input.number_of_meals:visible').val()))
                            ////////console.log('condition ' + uiVal + ' < ' + uiMealCount)
                            var uiInnerMealCount = callcenter.order_process.check_all_inner_meal_total();
                            uiVal++;


                            uiInput.val(uiVal);
                            fCallback(uiVal);

                            var iInnerMealTotal = callcenter.order_process.check_all_inner_meal_total();


                            $('input.number_of_meals:visible').val(iInnerMealTotal);

                            if (uiInnerMealCount < uiMealCount) {


                            }

                        }
                    }
                }

                callcenter.order_process.follow_addon_quantity(uiInput, uiVal);
            }
        },

        'follow_addon_quantity' : function(uiInput, uiVal) {
            if(typeof(uiInput) != 'undefined')
            {
                var uiInputMealContainer = uiInput.parents('div.meal:first');
                uiInputMealContainer.find('input[class*=addon-count]').val(uiVal).trigger('change');
            }
        },

        'counter': function (uiArrow, uiInput, uiVal, fCallback, eventOrig) {
            if (typeof(uiArrow) !== 'undefined' && typeof(uiInput) !== 'undefined' && typeof(uiVal) !== 'undefined' && typeof(fCallback) == 'function') {
                if (uiArrow.hasClass('minus')) {
                    if (uiVal == '1') {
                        //
                    }
                    else {
                        uiVal = parseInt(uiVal);
                        // if (uiVal > callcenter.order_process.check_all_inner_meal_total()) {
                        if (eventOrig) {
                            uiVal--;
                        }
                        uiInput.val(uiVal);

                        fCallback();
                        // }

                    }
                }
                else {
                    if (uiArrow.hasClass('plus')) {
                        if (uiVal == '') {
                            uiInput.val("1")
                        }
                        else {
                            uiVal = parseInt(uiVal);
                            if (eventOrig) {
                                uiVal++;
                            }

                            uiInput.val(uiVal);
                            fCallback();
                        }
                    }
                }
            }
        },

        'get_all_products': function (iOffset) {
            var oAjaxConfig = {
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "data"   : {
                    "params": {
                        "sbu_id": 1,
                        "limit" : 200,
                        "offset" : iOffset
                    }
                },
                "url"    : callcenter.config('url.api.jfc.products') + "/products/get",
                "success": function (oData) {
                    if (typeof(oData.status) !== 'undefined') {
                        if (oData.status == true) {

                            var oProducts = cr8v.get_from_storage('products', '');
                            if(typeof(oProducts) !== 'undefined')
                            {
                                oProducts = oProducts[0];

                                $.each(oData.data, function(key, value) {
                                    oProducts.push(value);
                                })

                                cr8v.pop('products');
                                cr8v.add_to_storage('products', oProducts);

                                if(oProducts.length < iProductCount)
                                {
                                    iCycles++;
                                    callcenter.order_process.get_all_products(200 * iCycles)
                                }


                            }
                            else
                            {
                                cr8v.add_to_storage('products', oData.data);
                                iCycles++;
                                callcenter.order_process.get_all_products(200 * iCycles)
                            }
                        }
                    }
                }
            }

            callcenter.call_orders.get(oAjaxConfig)
        },

        'get_all_special_options': function (iOffset) {
            var oAjaxConfig = {
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "data"   : {
                    "params": {
                        "limit" : 999999,
                    }
                },
                "url"    : callcenter.config('url.api.jfc.products') + "/products/special_options_get",
                "success": function (oData) {
                    if (typeof(oData.status) !== 'undefined') {
                        if (oData.status == true) {

                            var oSpecialOptions = cr8v.get_from_storage('special_options', '');
                            oSpecialOptions = oSpecialOptions[0];

                            $.each(oData.data, function (key, value) {
                                oSpecialOptions.push(value);
                            })

                            cr8v.pop('special_options');
                            cr8v.add_to_storage('special_options', oSpecialOptions);

                            //console.log( oSpecialOptions.length + ' < ' + iProductCount)

                        }
                    }
                }
            }

            callcenter.call_orders.get(oAjaxConfig)
        },

        'get_categories': function () {
            var uiContainer = $('section.content #order_process_content').find('div.category-box-container');
            var uiUnavailableContainer = $('div.rta_store div.unavailable_products').find('div.unavailable_products_container');
            var oAjaxConfig = {
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "data"   : {
                    "params": {
                        "sbu_id"  : 1,
                        "group_by": 'pc.id'
                    }
                },
                "url"    : callcenter.config('url.api.jfc.products') + "/products/categories",
                "success": function (oData) {
                    ////////console.log(oData.data);
                    if (typeof(oData) !== 'undefined') {
                        if (count(oData.data) > 0) {
                            var sHtml = '';
                            var sUnavailableHtml ='';
                            $.each(oData.data, function (key, category) {
                                sHtml += '<div class="category-box" data-category-id="' + category.category_id + '">' + category.category_name + '</div>';
                                sUnavailableHtml += '<div class="margin-top-10 notify-msg product_category" data-category-id="' + category.category_id + '"><h5>' + category.category_name + '</h5><div class="unavailable_products_holder"></div></div>';
                            });

                            uiContainer.append(sHtml)
                            uiUnavailableContainer.append(sUnavailableHtml);
                        }
                    }

                }
            }

            callcenter.call_orders.get(oAjaxConfig)
        },

        'get_product_categories': function (iCategoryID) {
            if (iCategoryID > 0 && typeof(iCategoryID) == 'string') {

                var sSearchEval = " el['id'] != '0' ";
                sSearchEval += " && $.inArray('" + iCategoryID + "', el['categories']) > -1  ";

                var oProducts = cr8v.get_from_storage('products', '');
                if (typeof(oProducts) !== 'undefined') {
                    oProducts = oProducts[0];
                    if (typeof(sSearchEval) !== 'undefined') {
                        oProducts = oProducts.filter(function (el) {
                            return eval(sSearchEval);
                        })
                        $('section.content #order_process_content div.product_search_result').html("");
                        var uiProductContainer = $('div.product_search_category_result');
                        if (typeof(oProducts) !== 'undefined' && count(oProducts) > 0) {
                            callcenter.order_process.manipulate_product_template(oProducts, uiProductContainer);
                        }

                        callcenter.order_process.check_products_unavailability(uiProductContainer);
                    }
                }


            }
        },



        'select_price': function (oPrices) {
            var iCustomerProvinceID = 0;
            var iCustomerStoreID = 0;
            var iCustomerCityID = 0;
            var oPrice = {};
            /*selection of price according to province, city, store id, will not be used for now as all price of products has no province,city,store ids yet*/
            if(typeof(oPrices) !== 'undefined')
            {
                $.each(oPrices, function (k, v) {
                    if (v.city_id == iCustomerCityID && v.province_id == iCustomerProvinceID && v.store_id == iCustomerStoreID) {
                        oPrice = v;
                    }
                })
            }

            return oPrice.price;

        },

        'close_product_info': function (iProductID, uiProduct) {
            var uiTemplate = $('section.content #order_process_content').find('div.general-info.template').clone().removeClass('template');
            var sSearchEval = " el['id'] != '0' ";
            sSearchEval += " && el['id'] == '" + iProductID + "' ";

            var oProducts = cr8v.get_from_storage('products', '');
            if (typeof(oProducts) !== 'undefined') {
                oProducts = oProducts[0];

                if (typeof(sSearchEval) !== 'undefined') {
                    oProducts = oProducts.filter(function (el) {
                        return eval(sSearchEval);
                    })

                    var oAllProducts = cr8v.get_from_storage('products', '');
                    if(typeof(oAllProducts) !== 'undefined')
                    {
                        oAllProducts = oAllProducts[0];
                    }
                    if(typeof(oProducts.add_ons) !== 'undefined')
                    {
                        if(oProducts.add_ons != null)
                        {
                            $.each(oProducts.add_ons, function(addon_id, addon_array) {

                                var filtered = oAllProducts.filter(function (el) {
                                    return $.inArray(el['id'], addon_array) > -1
                                })

                                oProducts.add_ons[addon_id] = filtered;
                            })
                        }
                    }

                    if (count(oProducts) > 0) {
                        // //////console.log(oData.data[0]);
                        var oProductResult = oProducts[0];
						// console.log(">>>");
						// console.log(oProductResult);
                        uiTemplate.attr('product-id', oProductResult.id);
                        uiTemplate.attr('id', oProductResult.id);
                        uiTemplate.attr('core-id', oProductResult.core_ids);
                        if(typeof(oProductResult.price[0]) !== 'undefined')
                        {
                            uiTemplate.attr('product-price', parseFloat(oProductResult.price[0].price));
                            uiTemplate.find('p.product-price').text(oProductResult.price[0].price);
                        }
                        uiTemplate.find('p.product-name').html(oProductResult.name);
                        uiTemplate.find('p.product-code').text(oProductResult.menu_board_code);
                        uiTemplate.find('p.product-description').text(oProductResult.description);
                        uiProduct.replaceWith(uiTemplate);
                    }

                }
            }


        },

        'expand_product_info': function (iProductID, uiProduct) {
            var uiTemplate = $('section.content #order_process_content').find('div.product-details.template').clone().removeClass('template');

            var sSearchEval = " el['id'] != '0' ";
            sSearchEval += " && el['id'] == '" + iProductID + "' ";

            var oProducts = cr8v.get_from_storage('products', '');
            var oProductsTo = cr8v.get_from_storage('products', '');
            if (typeof(oProducts) !== 'undefined') {
                oProducts = oProducts[0];
                oProductsTo = oProductsTo[0]
            }

            if (typeof(sSearchEval) !== 'undefined') {
                oProducts = oProducts.filter(function (el) {
                    return eval(sSearchEval);
                })

                var oAllProducts = cr8v.get_from_storage('products', '');
                if(typeof(oAllProducts) != 'undefined')
                {
                    oAllProducts = oAllProducts[0];
                }

                var oAllSpecialOptions = cr8v.get_from_storage('special_options', '');
                if(typeof(oAllSpecialOptions) != 'undefined')
                {
                    oAllSpecialOptions = oAllSpecialOptions[0];
                }

                if(typeof(oProducts[0].add_ons) !== 'undefined' && oProducts[0].add_ons != null)
                {
                    if(oProducts[0].add_ons.group != 'undefined')
                    {
                        oProducts[0].add_ons_detail = {};
                        $.each(oProducts[0].add_ons.group, function(key, addon) {
                            if(typeof(addon[Object.keys(addon)[0]]) != 'undefined')
                            {
                                var filtered = oAllProducts.filter(function (el) {
                                    return $.inArray(el['id'], addon[Object.keys(addon)[0]]) > -1
                                });
                                var addongroupid = Object.getOwnPropertyNames(addon)[0];
                                oProducts[0].add_ons_detail[addongroupid] = filtered
                            }
                        })

                    }
                }

                if(typeof(oProducts[0].special_options) !== 'undefined')
                {
                    if(oProducts[0].special_options != null && oProducts[0].special_options != "")
                    {
                        oProducts[0].special_options_detail = {};
                        oProducts[0].special_options_detail = oAllSpecialOptions.filter(function (el) {
                            return ($.inArray(el['id'], oProducts[0].special_options) > -1 && el['product_id'] == iProductID)
                        });

                    }
                }

                var sUpgrades = '';
                if(typeof(oProducts[0].upgrades) != 'undefined')
                {
                    oProducts[0].upgrades_detail = {};
                    $.each(oProducts[0].upgrades, function (key, upgrade_info) {
                        console.log(upgrade_info)
                        if(typeof(upgrade_info.upgrades) != 'undefined')
                        {
                            oProducts[0].upgrades_detail = oAllProducts.filter(function (el) {
                                return $.inArray(el['id'], upgrade_info.upgrades) > -1
                            });

                            if(typeof(oProducts[0].upgrades_detail) != 'undefined' && oProducts[0].upgrades_detail.length > 0)
                            {
                                $.each(oProducts[0].upgrades_detail, function(key , filtered_upgrades) {
                                    sUpgrades += '<div class="option meal-upgrades" data-product-id="' + filtered_upgrades.id + '" data-price="'+filtered_upgrades.price[0].price+'" data-upgrade-product-id="'+upgrade_info.id+'" data-input="upgrades-count-' + key + '" data-upgrades-id="' + filtered_upgrades.id + '" data-value="' + filtered_upgrades.name + '">' + filtered_upgrades.name + '</div>';
                                })
                            }

                            sUpgrades += '<div class="option meal-upgrades no-upgrades" data-addon-group-id="84" data-product-id="3398" data-value="No Upgrades" data-price="0.00">No Upgrades</div>';
                        }

                    })

                    if(sUpgrades.length > 0)
                    {
                        uiTemplate.find('div.upgrades-container').find('div.frm-custom-dropdown-option').html("").append(sUpgrades);
                    }
                    else
                    {
                        uiTemplate.find('div.upgrades-container').addClass('hidden');
                    }


                }


            }

            if (count(oProducts) > 0) {
                ////////console.log(oData.data[0])
                var oProductResult = oProducts[0];
                uiTemplate.find('strong.product_name').html(oProductResult.name);
                if(typeof(oProductResult.price[0]) !== 'undefined')
                {
                    uiTemplate.find('p.product_price').text(oProductResult.price[0].price + 'PHP | ' + oProductResult.menu_board_code);
                    uiTemplate.attr('product-price', parseFloat(oProductResult.price[0].price).toFixed(2));
                }
                uiTemplate.attr('product-id', oProductResult.id);
                uiTemplate.attr('id', oProductResult.id);
                uiTemplate.attr('core-id', oProductResult.core_ids);

                uiTemplate.find('div.meal-type').css('pointer-events' , 'none');

                /*if alacarte*/
                // {
                if(oProductResult.type == 2)
                {
                    uiTemplate.find('div.addon-container').addClass('hidden');
                    uiTemplate.find('diuv.special-option-container').addClass('hidden');
                    uiTemplate.find('inpt[name="meal_type"]').text('Ala Carte');
                    uiTemplate.find('input[name="drinks_type"]').prop('disabled', true);
                    uiTemplate.find('input[name="fries_type"]').prop('disabled', true);
                    uiTemplate.find('div.item-sizes').css('pointer-events', 'none').find('input[type="radio"]').prop('checked', false);
                }
                // }

                //get product addons

                uiTemplate = callcenter.order_process.regenerate_ids(uiTemplate);
                var uiAddonTemplate = callcenter.order_process.populate_add_ons(uiTemplate, oProductResult);
                if (typeof(uiAddonTemplate.uiInterface) !== 'undefined') {
                    var oItemBreakDown = {
                        sProductName : uiTemplate.find('strong.product_name').text(),
                        iProductPrice: parseFloat(uiTemplate.attr('product-price')),
                        iQuantity    : parseFloat(uiTemplate.find('input.meal-number-inner-input').val()),
                        iProductID   : uiTemplate.attr('product-id')
                    }


                    uiTemplate = callcenter.order_process.attach_item_breakdown(uiTemplate, oItemBreakDown);
                    callcenter.order_process.compute_breakdown_price(uiTemplate);
                    uiProduct.replaceWith(uiTemplate);
                    var uiAddonButton = uiTemplate.find('a.add_add_on');
                    var uiAddSpecialOptions = uiTemplate.find('a.special-option');
                    //console.log(uiAddonTemplate.addon_count)
                    if (typeof(uiAddonTemplate.addon_count) != 'undefined') {

                        while (uiAddonTemplate.addon_count > 1) {
                            uiAddonButton.trigger('click');
                            uiAddonTemplate.addon_count--;
                        }
                        var uiSelect = uiTemplate.find('select[name="meal-add-ons"]');
                        //console.log(uiAddonTemplate.add_ons)
                        $.each(uiSelect, function (key, select) {
                            // console.log(uiAddonTemplate.add_ons[key + 1]);
                            if (typeof(uiAddonTemplate.add_ons[key + 1]) != 'undefined') {
                                $(select).prev('div.frm-custom-dropdown').find('div.frm-custom-dropdown-option').html(uiAddonTemplate.add_ons[key + 1]);
                            }
                        })



                    }
                        
                    if(typeof(uiAddonTemplate.special_options) != 'undefined')
                    {
                        uiTemplate.find('div.special-option-select-container').find('div.frm-custom-dropdown-option').html("").append(uiAddonTemplate.special_options);
                    }

                    if(uiAddonTemplate.special_options_count !='undefined'){
                        while (uiAddonTemplate.special_options_count > 1) {
                        uiAddSpecialOptions.trigger('click');
                        uiAddonTemplate.special_options_count--;
                        }
                        uiAddSpecialOptions.hide();
                        
                        //uiTemplate.find('div.special-option-select-container').find('div.frm-custom-dropdown-option').find('div[data-special-option-id="2"]').trigger('click');
                        //console.log(uiAddonTemplate.special_options_array.length);
                         $.each(uiAddonTemplate.special_options_array, function (key, special_option) {
                            if(special_option != 1 ){//exclude the first option
                               uiTemplate.find('div.special-option-select-container').find('div.frm-custom-dropdown-option').find('div[data-special-option-id="'+special_option+'"]').trigger('click');
                            }
                        })
                    }


                }


            }

        },

        'populate_add_ons': function (uiTemplate, oProductResult) {
            if (typeof(uiTemplate) !== 'undefined' && typeof(oProductResult) !== 'undefined') {
                var sHtml = '';
                /*$.each(oProductResult.add_ons, function (key, value) {
                 sHtml += '<div class="option meal-add-ons-option" data-input="addon-count-1" data-product-id="' + value[0].id + '" data-value="' + value[0].name + '" data-price="' + value[0].price[0].price + '">' + value[0].name + '</div>'
                 })*/
                console.log(oProductResult);
                var oAddons = [];
                var iAddon = 0;
                var oSpecialOptions = [];
                var iSpecialOptions = 0;
                var iSpecialOptionsCount = 0;

                if (typeof(oProductResult.add_ons_detail) != 'undefined' && oProductResult.add_ons_detail != null) {
                    $.each(oProductResult.add_ons_detail, function (key, addon_group_id) {
                        iAddon++;
                        oAddons[key] = '<div class="option meal-add-ons-option" data-addon-group-id="' + key + '" data-input="addon-count-'+iAddon+'" data-product-id="3398" data-value="No Addons" data-price="0.00">No Addons</div>';
                        $.each(addon_group_id, function (addon_key, addon_details) {
                            oAddons[key] += '<div class="option meal-add-ons-option" data-addon-group-id="' + key + '" data-input="addon-count-'+iAddon+'" data-product-id="' + addon_details.id + '" data-value="' + addon_details.name + '" data-price="' + addon_details.price[0].price + '">' + addon_details.name + '</div>';
                        })
            
                    })
                    
                }
                else
                {
                    uiTemplate.find('div.addon-container').addClass('hidden');
                    uiTemplate.find('input[name="meal_type"]').text('Ala Carte');
                    uiTemplate.find('input[name="drinks_type"]').prop('disabled', true);
                    uiTemplate.find('input[name="fries_type"]').prop('disabled', true);
                    uiTemplate.find('div.item-sizes').css('pointer-events', 'none').find('input[type="radio"]').prop('checked', false);
                }

                if(typeof(oProductResult.special_options_detail) != 'undefiend')
                {
                    if(oProductResult.special_options_detail != null)
                    {
                        $.each(oProductResult.special_options_detail, function (key, special_option) {
                            oSpecialOptions += '<div class="option meal-special-option" data-special-option-group-id="' + special_option.group_id + '" data-input="special-option-count-' + special_option.group_id + '" data-special-option-id="' + special_option.id + '" data-value="' + special_option.name + '" data-price="0">' + special_option.name + '</div>';
                            uiTemplate.attr('data-special-option-quantity', special_option.required_quantity);
                            uiTemplate.attr('data-special-option-quantity-original-value', special_option.required_quantity);
                        })
                        iSpecialOptionsCount = oProductResult.special_options_detail.length;
                    }
                    else
                    {
                        uiTemplate.find('div.special-option-container').addClass('hidden');
                    }

                    //oSpecialOptions += '<div class="option meal-special-option no-special" data-value="" data-price="0">No Special Options</div>';
                }
            }
            var oNewAddons = [];
            var iNewAddons = 0;
            if (typeof(oAddons) !== 'undefined') {
                $.each(oAddons, function (key, value) {
                    if (typeof(value) !== 'undefined') {
                        iNewAddons++;
                        oNewAddons[iNewAddons] = value;
                    }

                })
            }

            var oReturn = {
                'uiInterface': uiTemplate,
                'addon_count': iAddon,
                'add_ons'    : oNewAddons,
                'special_options' : oSpecialOptions,
                'special_options_count' : iSpecialOptionsCount,
                'special_options_array' : oProductResult.special_options
            }

            //console.log(oReturn);
            //uiTemplate.find('input[name="meal-add-ons"]').parents('div.frm-custom-dropdown:first').find('div.frm-custom-dropdown-option').append(sHtml);
            return oReturn;
        },

        'compute_breakdown_price': function (uiMealItem) {
            if (typeof(uiMealItem) !== 'undefined') {
                var iTotal = 0;
                var uiBreakDownItems = uiMealItem.find('tr[class!="template"][class="product-breakdown-item"]');
                if(typeof(uiBreakDownItems) !== 'undefined')
                {
                    $.each(uiBreakDownItems, function (key, element) {
                        var iPrice = $(element).attr('data-product-price');
                        var iQuantity = $(element).attr('data-product-quantity');
                        var iRowTotal = (iPrice * iQuantity).toFixed(2);
                        $(element).find('p.product-price').text(iRowTotal + ' PHP');
                        $(element).attr('data-product-row-total', iRowTotal);
                        iTotal += iPrice;
                    })
                }

                //uiMealItem.find('strong.breakdown-total').text(parseInt(iTotal).toFixed(2) + ' PHP').attr('data-breakdown-total-price', iTotal);

                callcenter.order_process.compute_meal_price_total(uiMealItem);

            }
        },

        'compute_meal_price_total': function (uiMealItem) {
            if (typeof(uiMealItem) != 'undefined') {
                var iMealTotal = 0;
                var uiBreakDownItems = uiMealItem.find('tr[class!="template"][class="product-breakdown-item"]');
                if(typeof(uiBreakDownItems) !== 'undefined')
                {
                    $.each(uiBreakDownItems, function (key, element) {
                        var iRowTotal = $(element).attr('data-product-row-total');
                        iMealTotal += parseFloat(iRowTotal);
                    })
                }

                uiMealItem.find('table.product-breakdown:first').attr('data-meal-total-price', iMealTotal);
                ////////console.log(uiMealItem)
                uiMealItem.find('strong.breakdown-total').text(iMealTotal.toFixed(2) + ' PHP');
                callcenter.order_process.compute_product_total(uiMealItem);
            }
        },

        'compute_product_total': function (uiMealItem) {
            if (typeof(uiMealItem) != 'undefined') {
                ////////console.log(uiMealItem);
                var iProductPriceTotal = 0;
                var uiBreakDownItems = uiMealItem.find('table.product-breakdown[class!="template"]');
                // //////console.log(uiBreakDownItems)
                if(typeof(uiBreakDownItems) !== 'undefined')
                {
                    $.each(uiBreakDownItems, function (key, element) {
                        var iItemTotalPrice = $(element).attr('data-meal-total-price');
                        iProductPriceTotal += parseFloat(iItemTotalPrice);
                    })
                }
                ////////console.log(iProductPriceTotal);

                uiMealItem.find('strong.meal_item_total_price').text(parseFloat(iProductPriceTotal).toFixed(2) + ' PHP')
            }
        },

        'regenerate_ids': function (uiTemplate) {
            if (typeof(uiTemplate) !== 'undefined') {
                /*because of the schmuck plugin of radio buttons that is based on ids LOLLLL*/
                var uiMealCount = $('div.meal-container').find('div.meal').length;
                var iIDnumber = Math.floor((Math.random() * 10000000000000000000) + 1);
                var uiInputDrink = uiTemplate.find('input[name="drink-size-1"]');
                var uiInputFries = uiTemplate.find('input[name="fries-size-1"]');

                $(uiInputDrink[0]).attr('id', 'ds' + iIDnumber).attr('name', 'drink-size-' + uiMealCount)
                uiTemplate.find('label.drinks.small').attr('for', 'ds' + iIDnumber);
                $(uiInputDrink[1]).attr('id', 'dm' + iIDnumber).attr('name', 'drink-size-' + uiMealCount)
                uiTemplate.find('label.drinks.medium').attr('for', 'dm' + iIDnumber);
                $(uiInputDrink[2]).attr('id', 'dl' + iIDnumber).attr('name', 'drink-size-' + uiMealCount)
                uiTemplate.find('label.drinks.large').attr('for', 'dl' + iIDnumber);

                $(uiInputFries[0]).attr('id', 'fs' + iIDnumber).attr('name', 'fries-size-' + uiMealCount)
                uiTemplate.find('label.fries.small').attr('for', 'fs' + iIDnumber);
                $(uiInputFries[1]).attr('id', 'fm' + iIDnumber).attr('name', 'fries-size-' + uiMealCount)
                uiTemplate.find('label.fries.medium').attr('for', 'fm' + iIDnumber);
                $(uiInputFries[2]).attr('id', 'fl' + iIDnumber).attr('name', 'fries-size-' + uiMealCount)
                uiTemplate.find('label.fries.large').attr('for', 'fl' + iIDnumber);
            }

            return uiTemplate
        },

        'manipulate_product_template': function (oProductData, uiProductContainer) {
            uiProductContainer.html('');
            if (count(oProductData) > 0 && typeof(oProductData) !== 'undefined') {
                $.each(oProductData, function (key, product) {
                    var uiProductTemplate = $('div.product.template').clone().removeClass('template');

                    uiProductTemplate.find('p.product-name').html(product.name);

					uiProductTemplate.find('p.product-code').text((product.menu_board_code != '' && product.menu_board_code != null) ? product.menu_board_code : '');

                    uiProductTemplate.attr('product-id', product.id);
                    uiProductTemplate.attr('id', product.id);
                    uiProductTemplate.attr('core-id', product.core_ids);
                    if(product.image.length > 0 && product.image != null)
                    {
                        uiProductTemplate.find('img.product-image').attr('src', callcenter.config('url.server.base') + '/assets/images/products/' + product.image);
                    }
                    uiProductTemplate.find('p.product-description').text(product.description);
                    //uiProductTemplate.find('p.product-price').text(this.select_price(product.price));
                    if (typeof(product.price[0].price) != 'undefined') {
                        uiProductTemplate.find('p.product-price').text(product.price[0].price);
                    }
                    uiProductContainer.append(uiProductTemplate);
                });


            }
        },

        'check_products_unavailability': function (uiProductContainer) {
            if (typeof(uiProductContainer) != 'undefined') {
                var aUnavailable = cr8v.get_from_storage('unavailable_core_id', '');
                var uiProducts = uiProductContainer.find('div.product.general-info');
                var uiProductCollapsed = uiProductContainer.find('div.product-details');
                var uiProductInCart = $('body').find('tr.cart-item');
                // console.log(uiProducts)

                if (typeof(aUnavailable) !== 'undefined') {
                    aUnavailable = aUnavailable[0];
                }
                
                /*for core product*/
                var aCoreProducts = cr8v.get_from_storage('core_products', '');
                if(typeof(aCoreProducts) != 'undefined')
                {
                    if (typeof(aCoreProducts) !== 'undefined') {
                        aCoreProducts = aCoreProducts[0];
                    }

                    var filteredProducts = aCoreProducts.filter(function(el) {
                        return ($.inArray(el['id'], aUnavailable) > -1) ;
                    })
                    //console.log(filteredProducts);
                    var uiContainer = $('div.rta_store div.unavailable_products').find('div.unavailable_products_container');
                    uiContainer.html("");
                    if(filteredProducts.length!==0)
                    {
                        uiContainer.parent("div.unavailable_products").removeClass("hidden");
                        var sHtml = "";

                        $.each(filteredProducts, function(key,value) {
                            sHtml +="<div class='notify-msg margin-top-10' data-core-id='"+value.id+"'>"+ value.name +" (Unavailable)</div>";
                        });
                        uiContainer.append(sHtml);
                    }else{//all core products are available remove all contents
                        uiContainer.parent("div.unavailable_products").addClass("hidden");
                    }
                }


                /*end for core product*/

                /*for category and product*/
                //loop each categories (category id)
                // var uiCategoriesContainer = $('div.rta_store div.unavailable_products div.unavailable_products_container div.product_category:visible');
                // var aProducts = cr8v.get_from_storage('products', '');
                // $.each(uiCategoriesContainer, function () {
                //     var iCategoryID = $(this).attr('data-category-id');
                //     var catProducts = cr8v.get_from_storage('products', '');
                //     if (typeof(catProducts) !== 'undefined') {
                //         catProducts = catProducts[0];
                //         var filteredProducts = catProducts.filter(function(el) {
                //             return $.inArray(iCategoryID, el['categories']) > -1;
                //         })
                //         if(typeof(filteredProducts) != 'undefined' && filteredProducts.length > 0)
                //         {
                //             filteredProducts = filteredProducts.filter(function(el) {
                //                 return ($.inArray(el['core_id'], aUnavailable) > -1) ;
                //             })
                //             //console.log($(this));
                //             var uiContainer = $(this).find('div.unavailable_products_holder');
                //             uiContainer.html("");
                //             var sHtml = "";
                //             $.each(filteredProducts, function(key,value) {
                //                 //console.log(value);
                //                 //sHtml +="<div class='notify-msg margin-top-10'>"+ value.name +"</div>";
                //                 sHtml +="<div class=''> - "+ value.name +"</div>";
                //             });
                //             uiContainer.append(sHtml);


                //             //console.log(filteredProducts);
                //             /*$.each(filteredProducts, function(key, products){
                //              if(typeof(products) != 'undefined'){
                //              products.core_arr = [];
                //              if(products.core_ids != null)
                //              {
                //              products.core_arr.push(products.core_ids.split(','));
                //              }
                //              console.log(products.core_arr);
                //              }
                //              })    */
                //         }


                //     }
                // })
                /*end category and product*/


                /*for individual product*/
                //for the sidebar display of unavailable products
                /*var aProducts = cr8v.get_from_storage('products', '');
                 if (typeof(aProducts) !== 'undefined') {
                 aProducts = aProducts[0];
                 aProducts = aProducts.filter(function(el) {
                 return ($.inArray(el['core_id'], aUnavailable) > -1) ;
                 })

                 //console.log(aProducts.length);
                 var uiContainer = $('div.rta_store div.unavailable_products').find('div.unavailable_products_container');
                 //uiContainer.html("");
                 var sHtml = "";

                 $.each(aProducts, function(key,value) {
                 //console.log(value);
                 //sHtml +="<div class='notify-msg margin-top-10'>"+ value.name +"</div>";
                 sHtml +="<div class=''> - "+ value.name +"</div>";
                 });
                 uiContainer.append(sHtml);
                 }*/
                 /*end for invidual product*/

                if(typeof(uiProducts) != 'undefined')
                {
                    $.each(uiProducts, function () {
                        if ($(this).attr('core-id') != 'undefined' && $(this).attr('core-id') != null) {
                            var sName = $(this).find('p.product-name:visible:first').text().replace('(Unavailable)', '');
                            $(this).find('p.product-name:visible:first').text(sName.replace('(Unavailable)', '')).css('color', 'black');
                            $(this).css('pointer-events', 'auto');
                            var aCoreIds = $(this).attr('core-id').split(',');
                            var uiThis = $(this);
                            if(aCoreIds.length > 0)
                            {
                                $.each(aCoreIds, function(key, iCoreID) {
                                    if ($.inArray(iCoreID, aUnavailable) > -1) {
                                        var sName = uiThis.find('p.product-name:visible:first').text().replace('(Unavailable)', '');
                                        uiThis.find('p.product-name:visible:first').text(sName + '(Unavailable)').css('color', 'red');
                                        uiThis.css('pointer-events', 'none');
                                    }
                                })
                            }

                        }
                    })
                }


                if(typeof(uiProductInCart) != 'undefined')
                {
                    var iUnavailableCount = 0;
                    $.each(uiProductInCart, function () {
                        if ($(this).find('td.product-name').attr('data-core-id') != 'undefined' && $(this).find('td.product-name').attr('data-core-id') != null) {
                            $(this).removeClass('unavailable');
                            var uiThis = $(this);
                            var aCoreIds = $(this).find('td.product-name').attr('data-core-id').split(',');
                            if(aCoreIds.length > 0)
                            {
                                $.each(aCoreIds, function(key, iCoreID) {
                                    if ($.inArray(iCoreID, aUnavailable) > -1) {
                                        uiThis.addClass('unavailable');
                                        iUnavailableCount++;
                                    }
                                })
                            }

                        }
                    })
                }

                if(typeof(uiProductCollapsed) != 'undefined')
                {
                    $.each(uiProductCollapsed, function () {
                        if ($(this).attr('core-id') != 'undefined' && $(this).attr('core-id') != null) {
                            var aCoreIds = $(this).attr('core-id').split(',');
                            var sName = $(this).find('strong.product_name:visible').text().replace('(Unavailable)', '');
                            var uiThis = $(this);
                            $(this).find('strong.product_name:visible').text(sName.replace('(Unavailable)', '')).css('color', 'black');
                            $(this).css('pointer-events', 'auto');
                            $(this).find('button.add_to_cart').prop('disabled', false);
                            if(aCoreIds.length > 0) {
                                $.each(aCoreIds, function(key, iCoreID) {
                                    if ($.inArray(iCoreID, aUnavailable) > -1) {
                                        var sName = uiThis.find('strong.product_name:visible').text().replace('(Unavailable)', '');
                                        uiThis.find('strong.product_name:visible').text(sName + '(Unavailable)').css('color', 'red');
                                        uiThis.css('pointer-events', 'none');
                                        uiThis.find('button.add_to_cart').prop('disabled', true);

                                    }
                                })
                            }

                        }
                    })
                }
            }
        },

        'clone_append': function (uiTemplate, uiContainer) {
            if (typeof(uiTemplate) !== undefined && typeof(uiContainer) !== undefined) {
                uiContainer.append(uiTemplate);
            }
        },

        'focus_item': function () {

        },

        'focus_container': function () {

        }
    }

}());

$(window).load(function () {
    callcenter.order_process.initialize();
});


