/**
 *  Coordinator management Class
 *
 *
 *
 */

(function () {
    "use strict";

    CPlatform.prototype.store_management = {

        'initialize': function () {

        },

        'fetch_stores' : function() {
            var oCallcenterParams = {
                "params" : {
                    "limit" : 99999999
                }
            }

            var oAjaxCallcenterConfig = {
                "type"   : "GET",
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "data"   : oCallcenterParams,
                "url"    : admin.config('url.api.jfc.stores') + "/stores/get",
                "success": function (oData) {
                    if(typeof( oData.data) != 'undefined')
                    {
                        cr8v.add_to_storage('stores', oData.data);
                        var oStores = cr8v.get_from_storage('stores')
                        admin.agents.render_stores_list(oStores);
                    }
                }
            }

            admin.agents.ajax(oAjaxCallcenterConfig);
        },

        'render_stores_list' : function(oStores, bAdd) {
            if(typeof(oStores) != 'undefined')
            {
                var uiStores = [];
                var uiStoresContainer = $('div.store_management_list');


                $.each(oStores, function(key, agent) {
                    var uiStoreTemplate = $('section.store-list.template').clone().removeClass('template');
                    uiStoreTemplate = admin.agents.manipulate_store_template(uiStoreTemplate, agent);
                    uiStores.push(uiStoreTemplate);
                })

                if(typeof(bAdd) == 'undefined')
                {
                    uiStoresContainer.find('section.agent-list').remove();
                    uiStoresContainer.append(uiStores);
                }
                else
                {
                    uiStoresContainer.prepend(uiAgents);
                }




            }
        }

    }

}());

$(window).load(function () {
    admin.store_management.initialize();
});


