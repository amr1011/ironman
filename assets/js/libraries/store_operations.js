(function () {
    "use strict";
    var uiBusinessRuleSettingsContainer,
    uiBulkOrderDiscount,
    uiOperationSettings,
    uiRBUList,
    uiRBUDistrictsList
    ;

    CPlatform.prototype.store_operations = {

        'initialize' : function () {
            
            $("select").transformDD();

            admin.store_operations.fetch_sbu_settings(iConstantSbuId);

            admin.store_operations.fetch_bulk_order_discount(iConstantSbuId);

            admin.store_operations.fetch_operation_settings(iConstantSbuId);

            admin.store_operations.fetch_rbu_list();

            admin.store_operations.fetch_rbu_districts_list();

            //prevent from alpha input
            $("input.input_numeric").off("keyup").on("keyup",function (event) {
                $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            });

            //time input mask
            $("input.input_time").off("keyup").on("keyup",function (event) {
                $(this).val($(this).val().replace(/^([01]?\d|2[0-3])(:[0-5]\d){1,2}$/,''));
            });

            uiBusinessRuleSettingsContainer = $("div.store_operations").find('div.business_rule_settings');
            uiBulkOrderDiscount = $("div.store_operations").find('div.bulk-order-discount');
            uiOperationSettings = $("div.store_operations").find('div.operation-settings');
            uiRBUList = $("div.store_operations").find('div.rbu-list');
            uiRBUDistrictsList = $("div.store_operations").find('div.rbu-districts-list');
            
            //for the business rules
            uiBusinessRuleSettingsContainer.on('click','a.edit-values', function(){
                //console.log($(this));
                $(this).addClass('hidden');
                uiBusinessRuleSettingsContainer.find("button").removeClass('hidden');
                uiBusinessRuleSettingsContainer.find("button").removeClass('hidden');
                uiBusinessRuleSettingsContainer.find("table.business-rules-table-editable").removeClass('hidden');
                uiBusinessRuleSettingsContainer.find("table.business-rules-table-display").addClass('hidden');

            });

            uiBusinessRuleSettingsContainer.on('click','button.save-values', function(){
                var uiEditable  = uiBusinessRuleSettingsContainer.find("table.business-rules-table-editable tbody.sbu_settings_editable_container tr:not(.template)");
                var uiDisplaytable  = uiBusinessRuleSettingsContainer.find("table.business-rules-table-display tbody.sbu_settings_container");
                
                //this is for the validation of fields
                var iError = 0;
                $.each(uiEditable.find('input.required'), function(key ,value) {
                   if($(this).val() =='')
                   {
                      $(this).css({'border-color':'red'});
                      iError++;
                   }else{
                      $(this).css({'border-color':'#ddd'});    
                   }    
                });
                
                if(iError==0)
                {       
                        admin.store_operations.show_spinner($(this),true);         
                        var arrRules = [];
                        $.each(uiEditable, function(key ,value) {
                            if(typeof($(this).attr('data-rule-id')!="undefined"))
                            {
                                var id = $(this).attr('data-rule-id');
                                var value = $(this).find('input[type="text"]').val();
                                var name = $(this).find('td[data-label="rule-text"]').text();
                                arrRules.push( { 'id': id , 'value' : value , 'name' : name } );

                                uiDisplaytable.find('tr[data-rule-id="'+id+'"]').find('td[data-label="value"]').text(value);
                            }
                        });

                        //console.log(arrRules);     
                        var oParams = {
                                "params": {
                                    "data" : arrRules,
                                    "sbu_id" : iConstantSbuId
                                },
                                "user_id" : iConstantUserId
                            }
                        //console.log(oParams);
                        admin.store_operations.update_sbu_settings(oParams,$(this));
                }
            });

            uiBusinessRuleSettingsContainer.on('click','button.cancel-edit', function(){
                uiBusinessRuleSettingsContainer.find("a.edit-values").removeClass('hidden');
                uiBusinessRuleSettingsContainer.find("button").addClass('hidden');
                uiBusinessRuleSettingsContainer.find("table.business-rules-table-editable").addClass('hidden');
                uiBusinessRuleSettingsContainer.find("table.business-rules-table-display").removeClass('hidden');
            });
            
            //for the bulk order discount
            uiBulkOrderDiscount.on('click','a.edit-values', function(){
                //console.log($(this));
                $(this).addClass('hidden');
                uiBulkOrderDiscount.find("button.save-values").removeClass('hidden');
                uiBulkOrderDiscount.find("button.cancel-edit").removeClass('hidden');
                uiBulkOrderDiscount.find("table.bulk-order-table-editable").removeClass('hidden');
                uiBulkOrderDiscount.find("table.bulk-order-table-display").addClass('hidden');
                uiBulkOrderDiscount.find("a.add-bulk-order-discount").addClass('hidden');
                uiBulkOrderDiscount.find("table.bulk-order-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                uiBulkOrderDiscount.find("button.add-value").addClass('hidden');
                uiBulkOrderDiscount.find("button.cancel-add").addClass('hidden');
            });

            uiBulkOrderDiscount.on('click','button.save-values', function(){
                var uiEditable  = uiBulkOrderDiscount.find("table.bulk-order-table-editable tbody.sbu_settings_editable_container tr:not(.template)");
                var uiDisplaytable  = uiBulkOrderDiscount.find("table.bulk-order-table-display tbody.sbu_settings_container");
                
                //this is for the validation of fields
                var iError = 0;
                $.each(uiEditable.find('input.required'), function(key ,value) {
                   if($(this).val() =='')
                   {
                      $(this).css({'border-color':'red'});
                      iError++;
                   }else{
                      $(this).css({'border-color':'#ddd'});    
                   }    
                });
                
                if(iError==0)
                {  
                        admin.store_operations.show_spinner($(this),true);
                        var arrRules = [];
                        $.each(uiEditable, function(key ,value) {
                            if(typeof($(this).attr('data-rule-id')!="undefined"))
                            {
                                var id = $(this).attr('data-rule-id');
                                var discount_value = $(this).find('input[type="text"]').val();
                                var name = $(this).find('td[data-label="rule-text"]').text();
                                arrRules.push( { 'id': id , 'discount_value' : discount_value , 'name' : name} );

                                uiDisplaytable.find('tr[data-rule-id="'+id+'"]').find('td[data-label="discount_value"]').text(discount_value);
                            }
                        });

                        //console.log(arrRules);     
                        var oParams = {
                                "params": {
                                    "data" : arrRules,
                                    "sbu_id" : iConstantSbuId
                                },
                                "user_id" : iConstantUserId
                            }
                        //console.log(oParams);

                        admin.store_operations.update_bulk_order_discount_settings(oParams,$(this));
                }        
            });

            uiBulkOrderDiscount.on('click','button.cancel-edit', function(){
                uiBulkOrderDiscount.find("a.edit-values").removeClass('hidden');
                uiBulkOrderDiscount.find("button").addClass('hidden');
                uiBulkOrderDiscount.find("table.bulk-order-table-editable").addClass('hidden');
                uiBulkOrderDiscount.find("table.bulk-order-table-display").removeClass('hidden');
                uiBulkOrderDiscount.find("a.add-bulk-order-discount").removeClass('hidden');
                uiBulkOrderDiscount.find("table.bulk-order-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                uiBulkOrderDiscount.find("button.add-value").addClass('hidden');
                uiBulkOrderDiscount.find("button.cancel-add").addClass('hidden');
            });

            uiBulkOrderDiscount.on('click','a.add-bulk-order-discount', function(){
                //console.log($(this));
                $(this).addClass('hidden');
                uiBulkOrderDiscount.find("table.bulk-order-table-display tbody.sbu_settings_container_add_row").removeClass('hidden');
                uiBulkOrderDiscount.find("button.add-value").removeClass('hidden');
                uiBulkOrderDiscount.find("button.cancel-add").removeClass('hidden');
    
            });

            uiBulkOrderDiscount.on('click','button.add-value', function(){
                var uiAddField  = uiBulkOrderDiscount.find("table.bulk-order-table-display tbody.sbu_settings_container_add_row");
               
                //this is for the validation of fields
                var iError = 0;
                $.each(uiAddField.find('input.required'), function(key ,value) {
                   if($(this).val() =='')
                   {
                      $(this).css({'border-color':'red'});
                      iError++;
                   }else{
                      $(this).css({'border-color':'#ddd'});    
                   }    
                });
                
                if(iError==0)
                { 
                        admin.store_operations.show_spinner($(this),true);
                        var sRuleText = uiAddField.find('input[name="minimum_bulk_order_cost"]').val();
                        var sRuleValue = uiAddField.find('input[name="discount_value"]').val();

                        var oParams = {
                                "params": {
                                    "data" : { 'minimum_bulk_order_cost': sRuleText , 'discount_value' : sRuleValue ,"sbu_id" : iConstantSbuId },
                                    "sbu_id" : iConstantSbuId
                                },
                                "user_id" : iConstantUserId
                            }
                        //console.log(oParams);
                        admin.store_operations.add_bulk_order_discount_settings(oParams,$(this));
                }

                
            });

            uiBulkOrderDiscount.on('click','button.cancel-add', function(){
                uiBulkOrderDiscount.find("a.add-bulk-order-discount").removeClass('hidden');
                uiBulkOrderDiscount.find("table.bulk-order-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                uiBulkOrderDiscount.find("button.add-value").addClass('hidden');
                uiBulkOrderDiscount.find("button.cancel-add").addClass('hidden');
            });

            //for the operation settings
            uiOperationSettings.on('click','a.edit-values', function(){
                //console.log($(this));
                $(this).addClass('hidden');
                uiOperationSettings.find("button").removeClass('hidden');
                uiOperationSettings.find("button").removeClass('hidden');
                uiOperationSettings.find("table.operation-settings-table-editable").removeClass('hidden');
                uiOperationSettings.find("table.operation-settings-table-display").addClass('hidden');
    
            });

            uiOperationSettings.on('click','button.save-values', function(){
                var uiEditable  = uiOperationSettings.find("table.operation-settings-table-editable tbody.sbu_settings_editable_container tr:not(.template)");
                var uiDisplaytable  = uiOperationSettings.find("table.operation-settings-table-display tbody.sbu_settings_container");

                //this is for the validation of fields
                var iError = 0;
                $.each(uiEditable.find('input.required'), function(key ,value) {
                   if($(this).val() =='')
                   {
                      $(this).css({'border-color':'red'});
                      iError++;
                   }else{
                      $(this).css({'border-color':'#ddd'});    
                   }    
                });
                
                if(iError==0)
                { 
                        admin.store_operations.show_spinner($(this),true);
                        var arrRules = [];
                        $.each(uiEditable, function(key ,value) {
                            if(typeof($(this).attr('data-rule-id')!="undefined"))
                            {
                                var id = $(this).attr('data-rule-id');
                                var timer = $(this).find('input[data-input="timer"]').val();
                                var name = $(this).find('td[data-label="rule-text"]').text();
                                var time_interval = $(this).find('input[data-input="time_interval"]').val();
                                var is_alarm_on = 0;
                                var is_alarm_on_text = 'OFF';
                                if($(this).find('input[type="checkbox"]').prop('checked') == true)
                                {
                                    is_alarm_on = 1;
                                    is_alarm_on_text = 'ON';
                                }
                                arrRules.push( { 'id': id , 'timer' : timer, 'time_interval' : time_interval, 'is_alarm_on':is_alarm_on, 'name' : name } );

                                uiDisplaytable.find('tr[data-rule-id="'+id+'"]').find('td[data-label="timer"]').text(timer);
                                uiDisplaytable.find('tr[data-rule-id="'+id+'"]').find('td[data-label="time_interval"]').text(time_interval);
                                uiDisplaytable.find('tr[data-rule-id="'+id+'"]').find('td[data-label="is_alarm_on"]').text(is_alarm_on_text);
                            }
                        });

                        //console.log(arrRules);     
                        var oParams = {
                                "params": {
                                    "data" : arrRules,
                                    "sbu_id" : iConstantSbuId
                                },
                                "user_id" : iConstantUserId
                            }
                        //console.log(oParams);

                        admin.store_operations.update_operation_settings(oParams,$(this));
                }
                
            });

            uiOperationSettings.on('click','button.cancel-edit', function(){
                uiOperationSettings.find("a.edit-values").removeClass('hidden');
                uiOperationSettings.find("button").addClass('hidden');
                uiOperationSettings.find("table.operation-settings-table-editable").addClass('hidden');
                uiOperationSettings.find("table.operation-settings-table-display").removeClass('hidden');
            });
            
            //for the RBU List
            uiRBUList.on('click','a.edit-values', function(){
                //console.log($(this));
                $(this).addClass('hidden');
                uiRBUList.find("button").removeClass('hidden');
                uiRBUList.find("button").removeClass('hidden');
                uiRBUList.find("table.rbu-list-table-editable").removeClass('hidden');
                uiRBUList.find("table.rbu-list-table-display").addClass('hidden');

                uiRBUList.find("a.add-rbu-to-list").addClass('hidden');
                uiRBUList.find("table.rbu-list-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                uiRBUList.find("button.add-value").addClass('hidden');
                uiRBUList.find("button.cancel-add").addClass('hidden');
    
            });

            uiRBUList.on('click','button.save-values', function(){
                var uiEditable  = uiRBUList.find("table.rbu-list-table-editable tbody.sbu_settings_editable_container tr:not(.template)");
                var uiDisplaytable  = uiRBUList.find("table.rbu-list-table-display tbody.sbu_settings_container");
                
                //this is for the validation of fields
                var iError = 0;
                $.each(uiEditable.find('input.required'), function(key ,value) {
                   if($(this).val() =='')
                   {
                      $(this).css({'border-color':'red'});
                      iError++;
                   }else{
                      $(this).css({'border-color':'#ddd'});    
                   }    
                });
                
                if(iError==0)
                { 
                        admin.store_operations.show_spinner($(this),true);
                        var arrRules = [];
                        $.each(uiEditable, function(key ,value) {
                            if(typeof($(this).attr('data-rule-id')!="undefined"))
                            {
                                var id = $(this).attr('data-rule-id');
                                var name = $(this).find('input[type="text"]').val();
                                arrRules.push( { 'id': id , 'name' : name, 'is_deleted' : 0 } );

                                uiDisplaytable.find('tr[data-rule-id="'+id+'"]').find('td[data-label="name"]').text(name);
                            }
                        });

                        //console.log(arrRules);     
                        var oParams = {
                                "params": {
                                    "data" : arrRules
                                },
                                "user_id" : iConstantUserId
                            }
                        //console.log(oParams);
                        admin.store_operations.update_rbu_list(oParams,$(this));
                 }
                
            });

            uiRBUList.on('click','button.cancel-edit', function(){
                uiRBUList.find("a.edit-values").removeClass('hidden');
                uiRBUList.find("button").addClass('hidden');
                uiRBUList.find("table.rbu-list-table-editable").addClass('hidden');
                uiRBUList.find("table.rbu-list-table-display").removeClass('hidden');

                uiRBUList.find("a.add-rbu-to-list").removeClass('hidden');
                uiRBUList.find("table.rbu-list-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                uiRBUList.find("button.add-value").addClass('hidden');
                uiRBUList.find("button.cancel-add").addClass('hidden');
            });

            uiRBUList.on('click','a.add-rbu-to-list', function(){
                //console.log($(this));
                $(this).addClass('hidden');
                uiRBUList.find("table.rbu-list-table-display tbody.sbu_settings_container_add_row").removeClass('hidden');
                uiRBUList.find("button.add-value").removeClass('hidden');
                uiRBUList.find("button.cancel-add").removeClass('hidden');
    
            });

            uiRBUList.on('click','button.add-value', function(){
                var uiAddField  = uiRBUList.find("table.rbu-list-table-display tbody.sbu_settings_container_add_row");

                //this is for the validation of fields
                var iError = 0;
                $.each(uiAddField.find('input.required'), function(key ,value) {
                   if($(this).val() =='')
                   {
                      $(this).css({'border-color':'red'});
                      iError++;
                   }else{
                      $(this).css({'border-color':'#ddd'});    
                   }    
                });
                
                if(iError==0)
                { 
                        admin.store_operations.show_spinner($(this),true);
                        var sRuleText = uiAddField.find('input[name="name"]').val().capitalize();

                        var oParams = {
                                "params": {
                                    "data" : { 'name': sRuleText , 'is_deleted' : 0 },
                                },
                                "user_id" : iConstantUserId
                            }
                        console.log(oParams);
                        admin.store_operations.add_rbu_list(oParams,$(this));
                }
            });

            uiRBUList.on('click','button.cancel-add', function(){
                uiRBUList.find("a.add-rbu-to-list").removeClass('hidden');
                uiRBUList.find("table.rbu-list-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                uiRBUList.find("button.add-value").addClass('hidden');
                uiRBUList.find("button.cancel-add").addClass('hidden');
            });

            //for the RBU Districts List
            uiRBUDistrictsList.on('click','a.edit-values', function(){
                //console.log($(this));
                $(this).addClass('hidden');
                uiRBUDistrictsList.find("button").removeClass('hidden');
                uiRBUDistrictsList.find("button").removeClass('hidden');
                uiRBUDistrictsList.find("table.rbu-districts-list-table-editable").removeClass('hidden');
                uiRBUDistrictsList.find("table.rbu-districts-list-table-display").addClass('hidden');

                uiRBUDistrictsList.find("a.add-rbu-districts-to-list").addClass('hidden');
                uiRBUDistrictsList.find("table.rbu-districts-list-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                uiRBUDistrictsList.find("button.add-value").addClass('hidden');
                uiRBUDistrictsList.find("button.cancel-add").addClass('hidden');
    
            });

            uiRBUDistrictsList.on('click','button.save-values', function(){
                var uiEditable  = uiRBUDistrictsList.find("table.rbu-districts-list-table-editable tbody.sbu_settings_editable_container tr:not(.template)");
                var uiDisplaytable  = uiRBUDistrictsList.find("table.rbu-districts-list-table-display tbody.sbu_settings_container");
                
                //this is for the validation of fields
                var iError = 0;
                $.each(uiEditable.find('input.required'), function(key ,value) {
                   if($(this).val() =='')
                   {
                      $(this).css({'border-color':'red'});
                      iError++;
                   }else{
                      $(this).css({'border-color':'#ddd'});    
                   }    
                });
                
                if(iError==0)
                { 
                        admin.store_operations.show_spinner($(this),true);
                        var arrRules = [];
                        $.each(uiEditable, function(key ,value) {
                            if(typeof($(this).attr('data-rule-id')!="undefined"))
                            {
                                var id = $(this).attr('data-rule-id');
                                var name = $(this).find('input[data-input="name"]').val();
                                //var rbu_id = $(this).find('input[data-input="rbu_id"]').val();
                                var rbu_id = $(this).find('input[data-input-rbu="rbu_id"]').attr('data-value');
                                var rbu_name = $(this).find('input[data-input-rbu="rbu_id"]').val();
                                arrRules.push( { 'id': id , 'name' : name, 'rbu_id' : rbu_id, 'is_deleted' : 0 } );

                                uiDisplaytable.find('tr[data-rule-id="'+id+'"]').find('td[data-label="name"]').text(name);
                                uiDisplaytable.find('tr[data-rule-id="'+id+'"]').find('td[data-label="rbu_name"]').text(rbu_name);
                                
                            }
                        });

                        //console.log(arrRules);     
                        var oParams = {
                                "params": {
                                    "data" : arrRules
                                },
                                "user_id" : iConstantUserId
                            }
                        //console.log(oParams);
                        admin.store_operations.update_rbu_districts_list(oParams,$(this));
                 }
                
            });

            uiRBUDistrictsList.on('click','button.cancel-edit', function(){
                uiRBUDistrictsList.find("a.edit-values").removeClass('hidden');
                uiRBUDistrictsList.find("button").addClass('hidden');
                uiRBUDistrictsList.find("table.rbu-districts-list-table-editable").addClass('hidden');
                uiRBUDistrictsList.find("table.rbu-districts-list-table-display").removeClass('hidden');

                uiRBUDistrictsList.find("a.add-rbu-districts-to-list").removeClass('hidden');
                uiRBUDistrictsList.find("table.rbu-districts-list-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                uiRBUDistrictsList.find("button.add-value").addClass('hidden');
                uiRBUDistrictsList.find("button.cancel-add").addClass('hidden');
            });

            uiRBUDistrictsList.on('click','a.add-rbu-districts-to-list', function(){
                //console.log($(this));
                $(this).addClass('hidden');
                uiRBUDistrictsList.find("table.rbu-districts-list-table-display tbody.sbu_settings_container_add_row").removeClass('hidden');
                uiRBUDistrictsList.find("table.rbu-districts-list-table-display tbody.sbu_settings_container_add_row div.rbu-list-select div.rbu_select:first").trigger('click');
                uiRBUDistrictsList.find("button.add-value").removeClass('hidden');
                uiRBUDistrictsList.find("button.cancel-add").removeClass('hidden');
    
            });

            uiRBUDistrictsList.on('click','button.add-value', function(){
                var uiAddField  = uiRBUDistrictsList.find("table.rbu-districts-list-table-display tbody.sbu_settings_container_add_row");

                //this is for the validation of fields
                var iError = 0;
                $.each(uiAddField.find('input.required'), function(key ,value) {
                   if($(this).val() =='')
                   {
                      $(this).css({'border-color':'red'});
                      iError++;
                   }else{
                      $(this).css({'border-color':'#ddd'});    
                   }    
                });
                
                if(iError==0)
                { 
                        admin.store_operations.show_spinner($(this),true);
                        var iRBUid = uiAddField.find('input[data-input-rbu="rbu_id"]').attr('data-value');
                        var iRBUName = uiAddField.find('input[data-input-rbu="rbu_id"]').val();
                        var sRuleText = uiAddField.find('input[data-input="name"]').val().capitalize();

                        var oParams = {
                                "params": {
                                    "data" : { 'rbu_id': iRBUid , 'name': sRuleText , 'is_deleted' : 0 },
                                    "rbu_name": iRBUName,
                                },
                                "user_id" : iConstantUserId
                            }
                        //console.log(oParams);
                        admin.store_operations.add_rbu_districts_list(oParams,$(this));
                }
            });

            uiRBUDistrictsList.on('click','button.cancel-add', function(){
                uiRBUDistrictsList.find("a.add-rbu-districts-to-list").removeClass('hidden');
                uiRBUDistrictsList.find("table.rbu-districts-list-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                uiRBUDistrictsList.find("button.rbu-districts-value").addClass('hidden');
                uiRBUDistrictsList.find("button.add-value").addClass('hidden');
                uiRBUDistrictsList.find("button.cancel-add").addClass('hidden');
            });

             uiRBUDistrictsList.on('click','div.rbu_select', function(){
                 var iRBUid = $(this).attr('int-value');    
                 $(this).parents('div.select:first').find('input').attr('data-value', iRBUid)
            });

            
        },//end initialize,

        'show_spinner': function (uiElem, bShow) {
            if (bShow == true) {
                uiElem.find('i.fa-spinner').removeClass('hidden');
                uiElem.prop('disabled', true);
            }
            else {
                uiElem.find('i.fa-spinner').addClass('hidden');
                uiElem.prop('disabled', false);
            }
        },

        'fetch_sbu_settings' : function(iConstantSbuId) {
            var oAjaxConfig = {
                "type"   : "Get",
                "data"   : {
                    "params" : {
                        "sbu_id" : iConstantSbuId
                    }
                },

                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : admin.config('url.api.jfc.store') + "/stores/sbu_settings",
                "success": function (oData) {
                    if( oData.data.length > 0)
                    {
                        var oRules = oData.data;
                        admin.store_operations.render_sbu_settings(oRules);
                        //prevent from alpha input
                        $("input.input_numeric").off("keyup").on("keyup",function (event) {
                            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
                        });

                        //tooltip
                        $('table.business-rules-table-display').find('tr.rule_tr td.business_rules').tooltipster({
                            theme     :  'tooltipster-shadow',
                            offsetX   :  -70
                        });
                    }
                }
            }

            admin.agents.ajax(oAjaxConfig);
        },

        'render_sbu_settings' : function(oRules) {
            if(typeof(oRules) != 'undefined') {
                var uiRuleTemplate = $('table.business-rules-table-display').find('tr.rule_tr.template');
                var uiRuleEditableTemplate = $('table.business-rules-table-editable').find('tr.rule_editable_tr.template');
                var uiContainer = $('table.business-rules-table-display').find('tbody.sbu_settings_container')
                var uiEditableContainer = $('table.business-rules-table-editable').find('tbody.sbu_settings_editable_container')
                $.each(oRules, function(index ,rule) {
                    var uiTemplate = uiRuleTemplate.clone().removeClass('template');
                    for (var key in rule) {
                        if (rule.hasOwnProperty(key)) {
                            uiTemplate.attr('data-rule-id',rule.id);
                            uiTemplate.find('[data-label="' + key + '"]').text(rule[key]);
                            uiTemplate.find('[data-label="' + key + '"]').prop('title',rule.description);
                        }
                    }

                    var uiEditableTemplate = uiRuleEditableTemplate.clone().removeClass('template');
                    for (var key in rule) {
                        if (rule.hasOwnProperty(key)) {
                            uiEditableTemplate.attr('data-rule-id',rule.id);
                            uiEditableTemplate.find('[data-label="' + key + '"]').text(rule[key])
                            uiEditableTemplate.find('[data-input="' + key + '"]').val(rule[key])
                            uiEditableTemplate.find('[data-input-name="' + key + '"]').attr('name', rule[key] )
                        }
                    }

                    uiContainer.append(uiTemplate)
                    uiEditableContainer.append(uiEditableTemplate)
                })
            }
        },

        'update_sbu_settings': function (oParams,uiButton) {
            if (typeof(oParams) != 'undefined') {
                var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                        "url"    : admin.config('url.api.jfc.store') + "/stores/sbu_settings_update",
                        "success": function (data) {
                            //console.log(data);
                            if(typeof(data) !== 'undefined')
                            {                      
                                //console.log(data);
                                admin.store_operations.show_spinner(uiButton,false);
                                uiBusinessRuleSettingsContainer.find("a.edit-values").removeClass('hidden');
                                uiBusinessRuleSettingsContainer.find("button").addClass('hidden');
                                uiBusinessRuleSettingsContainer.find("table.business-rules-table-editable").addClass('hidden');
                                uiBusinessRuleSettingsContainer.find("table.business-rules-table-display").removeClass('hidden');
                            }

                        }
                    }

               admin.agents.ajax(oAjaxConfig);
            }
        },

        'fetch_bulk_order_discount' : function(iConstantSbuId) {
            var oAjaxConfig = {
                "type"   : "Get",
                "data"   : {
                    "params" : {
                        "sbu_id" : iConstantSbuId
                    }
                },

                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : admin.config('url.api.jfc.store') + "/stores/bulk_order_discount",
                "success": function (oData) {
                    if( oData.data.length > 0)
                    {
                        var oRules = oData.data;
                        //console.log(oDiscounts);
                        admin.store_operations.render_bulk_order_settings(oRules);
                        //prevent from alpha input
                        $("input.input_numeric").off("keyup").on("keyup",function (event) {
                            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
                        });
                    }
                }
            }

            admin.agents.ajax(oAjaxConfig);
        },

        'render_bulk_order_settings' : function(oRules) {
            if(typeof(oRules) != 'undefined') {
                var uiRuleTemplate = $('table.bulk-order-table-display').find('tr.rule_tr.template');
                var uiRuleEditableTemplate = $('table.bulk-order-table-editable').find('tr.rule_editable_tr.template');
                var uiContainer = $('table.bulk-order-table-display').find('tbody.sbu_settings_container')
                var uiEditableContainer = $('table.bulk-order-table-editable').find('tbody.sbu_settings_editable_container')
                $.each(oRules, function(index ,rule) {
                    var uiTemplate = uiRuleTemplate.clone().removeClass('template');
                    for (var key in rule) {
                        if (rule.hasOwnProperty(key)) {
                            uiTemplate.attr('data-rule-id',rule.id);
                            uiTemplate.find('[data-label="' + key + '"]').text(rule[key])
                        }
                    }

                    var uiEditableTemplate = uiRuleEditableTemplate.clone().removeClass('template');
                    for (var key in rule) {
                        if (rule.hasOwnProperty(key)) {
                            uiEditableTemplate.attr('data-rule-id',rule.id);
                            uiEditableTemplate.find('[data-label="' + key + '"]').text(rule[key])
                            uiEditableTemplate.find('[data-input="' + key + '"]').val(rule[key])
                            uiEditableTemplate.find('[data-input-name="' + key + '"]').attr('name', rule[key] )
                        }
                    }

                    uiContainer.append(uiTemplate);
                    uiEditableContainer.append(uiEditableTemplate);
                })
            }
        },
        
        'update_bulk_order_discount_settings': function (oParams,uiButton) {
            if (typeof(oParams) != 'undefined') {
                var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                        "url"    : admin.config('url.api.jfc.store') + "/stores/bulk_order_discount_setting_update",
                        "success": function (data) {
                            //console.log(data);
                            if(typeof(data) !== 'undefined')
                            {                      
                                //console.log(data);
                                admin.store_operations.show_spinner(uiButton,false);
                                uiBulkOrderDiscount.find("a.edit-values").removeClass('hidden');
                                uiBulkOrderDiscount.find("button").addClass('hidden');
                                uiBulkOrderDiscount.find("a.add-bulk-order-discount").removeClass('hidden');
                                uiBulkOrderDiscount.find("table.bulk-order-table-editable").addClass('hidden');
                                uiBulkOrderDiscount.find("table.bulk-order-table-display").removeClass('hidden');
                            }

                        }
                    }

               admin.agents.ajax(oAjaxConfig);
            }
        },
        
        'add_bulk_order_discount_settings': function (oParams,uiButton) {
            if (typeof(oParams) != 'undefined') {
                var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                        "url"    : admin.config('url.api.jfc.store') + "/stores/bulk_order_discount_setting_add",
                        "success": function (data) {
                            //console.log(data);
                            if(typeof(data) !== 'undefined')
                            {                      
                                //console.log(data);
                                var oRules = data.data;
                                var uiRuleTemplate = $('table.bulk-order-table-display').find('tr.rule_tr.template');
                                var uiRuleEditableTemplate = $('table.bulk-order-table-editable').find('tr.rule_editable_tr.template');
                                var uiContainer = $('table.bulk-order-table-display').find('tbody.sbu_settings_container')
                                var uiEditableContainer = $('table.bulk-order-table-editable').find('tbody.sbu_settings_editable_container')

                                var uiTemplate = uiRuleTemplate.clone().removeClass('template');

                                uiTemplate.attr('data-rule-id',oRules.id);
                                uiTemplate.find('td[data-label="rule-text"]').text(oRules.minimum_bulk_order_cost);
                                uiTemplate.find('td[data-label="discount_value"]').text(oRules.discount_value);


                                var uiEditableTemplate = uiRuleEditableTemplate.clone().removeClass('template');

                                uiEditableTemplate.attr('data-rule-id',oRules.id);
                                uiEditableTemplate.find('[data-label="rule-text"]').text(oRules.minimum_bulk_order_cost)
                                uiEditableTemplate.find('[data-input="discount_value"]').val(oRules.discount_value)
        
                                uiContainer.append(uiTemplate);
                                uiEditableContainer.append(uiEditableTemplate);

                                admin.store_operations.show_spinner(uiButton,false);

                                uiBulkOrderDiscount.find("a.add-bulk-order-discount").removeClass('hidden');
                                uiBulkOrderDiscount.find("table.bulk-order-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                                uiBulkOrderDiscount.find("table.bulk-order-table-display tbody.sbu_settings_container_add_row").find('input[type="text"]').val('');
                                uiBulkOrderDiscount.find("button.add-value").addClass('hidden');
                                uiBulkOrderDiscount.find("button.cancel-add").addClass('hidden');
                                
                            }

                        }
                    }

               admin.agents.ajax(oAjaxConfig);
            }
        },
        
        'fetch_operation_settings' : function(iConstantSbuId) {
            var oAjaxConfig = {
                "type"   : "Get",
                "data"   : {
                    "params" : {
                        "sbu_id" : iConstantSbuId
                    }
                },

                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : admin.config('url.api.jfc.store') + "/stores/operation_settings",
                "success": function (oData) {
                    if( oData.data.length > 0)
                    {
                        var oRules = oData.data;
                        //console.log(oDiscounts);
                        admin.store_operations.render_operation_settings(oRules);
                        $('input.input_time').mask('00:00:00');
                        
                        $("input.input_numeric").off("keyup").on("keyup",function (event) {
                            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
                        });
                    }
                }
            }

            admin.agents.ajax(oAjaxConfig);
        },       

        'render_operation_settings' : function(oRules) {
            if(typeof(oRules) != 'undefined') {
                var uiRuleTemplate = $('table.operation-settings-table-display').find('tr.rule_tr.template');
                var uiRuleEditableTemplate = $('table.operation-settings-table-editable').find('tr.rule_editable_tr.template');
                var uiContainer = $('table.operation-settings-table-display').find('tbody.sbu_settings_container')
                var uiEditableContainer = $('table.operation-settings-table-editable').find('tbody.sbu_settings_editable_container')
                $.each(oRules, function(index ,rule) {
                    var uiTemplate = uiRuleTemplate.clone().removeClass('template');
                    for (var key in rule) {
                        if (rule.hasOwnProperty(key)) {
                            uiTemplate.attr('data-rule-id',rule.id);
                            uiTemplate.find('[data-label="' + key + '"]').text(rule[key]);
                            if(key == 'is_alarm_on'){
                               if(rule[key] == 1){
                                  uiTemplate.find('[data-label="' + key + '"]').text('ON');     
                               }else{
                                  uiTemplate.find('[data-label="' + key + '"]').text('OFF');     
                               }
                                    
                            }
                        }
                    }

                    var uiEditableTemplate = uiRuleEditableTemplate.clone().removeClass('template');
                    for (var key in rule) {
                        if (rule.hasOwnProperty(key)) {
                            uiEditableTemplate.attr('data-rule-id',rule.id);
                            uiEditableTemplate.find('[data-label="' + key + '"]').text(rule[key]);
                            uiEditableTemplate.find('[data-input="' + key + '"]').val(rule[key]);
                            uiEditableTemplate.find('[data-input-name="' + key + '"]').attr('name', rule[key] );
                            if(key == 'is_alarm_on'){
                               if(rule[key] == 1){
                                  uiEditableTemplate.find('input[type="checkbox"]').attr('checked',true);     
                               }else{
                                  uiEditableTemplate.find('input[type="checkbox"]').attr('checked',false);       
                               }
                                    
                            }
                        }
                    }

                    uiContainer.append(uiTemplate)
                    uiEditableContainer.append(uiEditableTemplate)
                })
            }
        },     

        'update_operation_settings': function (oParams,uiButton) {
            if (typeof(oParams) != 'undefined') {
                var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                        "url"    : admin.config('url.api.jfc.store') + "/stores/operation_setting_update",
                        "success": function (data) {
                            //console.log(data);
                            if(typeof(data) !== 'undefined')
                            {                      
                                //console.log(data);
                                admin.store_operations.show_spinner(uiButton,false);
                                uiOperationSettings.find("a.edit-values").removeClass('hidden');
                                uiOperationSettings.find("button").addClass('hidden');
                                uiOperationSettings.find("table.operation-settings-table-editable").addClass('hidden');
                                uiOperationSettings.find("table.operation-settings-table-display").removeClass('hidden');
                            }

                        }
                    }

               admin.agents.ajax(oAjaxConfig);
            }
        },

        'fetch_rbu_list' : function() {
            var oAjaxConfig = {
                "type"   : "Get",
                "data"   : {
                    "params" : {
                        "is_deleted" : 0
                    }
                },

                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : admin.config('url.api.jfc.store') + "/stores/rbu_list",
                "success": function (oData) {
                    if( oData.data.length > 0)
                    {
                        var oRules = oData.data;
                        //console.log(oRules);
                        admin.store_operations.render_rbu_list(oRules);

                        //render rbu list select
                        admin.store_operations.render_rbu_list_select(oRules);

                    }
                }
            }

            admin.agents.ajax(oAjaxConfig);
        }, 

        'render_rbu_list' : function(oRules) {
            if(typeof(oRules) != 'undefined') {
                var uiRuleTemplate = $('table.rbu-list-table-display').find('tr.rule_tr.template');
                var uiRuleEditableTemplate = $('table.rbu-list-table-editable').find('tr.rule_editable_tr.template');
                var uiContainer = $('table.rbu-list-table-display').find('tbody.sbu_settings_container')
                var uiEditableContainer = $('table.rbu-list-table-editable').find('tbody.sbu_settings_editable_container')
                $.each(oRules, function(index ,rule) {
                    var uiTemplate = uiRuleTemplate.clone().removeClass('template');
                    for (var key in rule) {
                        if (rule.hasOwnProperty(key)) {
                            uiTemplate.attr('data-rule-id',rule.id);
                            uiTemplate.find('[data-label="' + key + '"]').text(rule[key]);
                        }
                    }

                    var uiEditableTemplate = uiRuleEditableTemplate.clone().removeClass('template');
                    for (var key in rule) {
                        if (rule.hasOwnProperty(key)) {
                            uiEditableTemplate.attr('data-rule-id',rule.id);
                            uiEditableTemplate.find('[data-label="' + key + '"]').text(rule[key]);
                            uiEditableTemplate.find('[data-input="' + key + '"]').val(rule[key]);
                            uiEditableTemplate.find('[data-input-name="' + key + '"]').attr('name', rule[key] );
                        }
                    }

                    uiContainer.append(uiTemplate)
                    uiEditableContainer.append(uiEditableTemplate)
                })
            }
        },  

        'render_rbu_list_select' : function(oRules) {
            if(typeof(oRules) != 'undefined') {
                var uiContainer = $('div.rbu-districts-list div.rbu-list-select').find('div.frm-custom-dropdown-option');
                var sRBUHtml = '';
                $.each(oRules, function(index,rule) {
                    sRBUHtml += '<div class="option rbu_select" int-value="'+rule.id+'">'+rule.name+'</div>';
                })
                uiContainer.append(sRBUHtml);
            }
        },  
         
        'update_rbu_list': function (oParams,uiButton) {
            if (typeof(oParams) != 'undefined') {
                var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                        "url"    : admin.config('url.api.jfc.store') + "/stores/rbu_list_update",
                        "success": function (data) {
                            //console.log(data);
                            if(typeof(data) !== 'undefined')
                            {
                                //console.log(data);
                                admin.store_operations.show_spinner(uiButton,false);
                                uiRBUList.find("a.edit-values").removeClass('hidden');
                                uiRBUList.find("button").addClass('hidden');
                                uiRBUList.find("table.rbu-list-table-editable").addClass('hidden');
                                uiRBUList.find("table.rbu-list-table-display").removeClass('hidden');

                                uiRBUList.find("a.add-rbu-to-list").removeClass('hidden');
                                uiRBUList.find("table.rbu-list-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                                uiRBUList.find("button.add-value").addClass('hidden');
                                uiRBUList.find("button.cancel-add").addClass('hidden');
                            }

                        }
                    }

               admin.agents.ajax(oAjaxConfig);
            }
        },

        'add_rbu_list': function (oParams,uiButton) {
            if (typeof(oParams) != 'undefined') {
                var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                        "url"    : admin.config('url.api.jfc.store') + "/stores/rbu_list_add",
                        "success": function (data) {
                            //console.log(data);
                            if(typeof(data) !== 'undefined')
                            {                      
                                console.log(data);
                                //admin.store_operations.show_spinner(uiButton,false);
                                var oRules = data.data;
                                var uiRuleTemplate = $('table.rbu-list-table-display').find('tr.rule_tr.template');
                                var uiRuleEditableTemplate = $('table.rbu-list-table-editable').find('tr.rule_editable_tr.template');
                                var uiContainer = $('table.rbu-list-table-display').find('tbody.sbu_settings_container')
                                var uiEditableContainer = $('table.rbu-list-table-editable').find('tbody.sbu_settings_editable_container')

                                var uiTemplate = uiRuleTemplate.clone().removeClass('template');

                                uiTemplate.attr('data-rule-id',oRules.id);
                                uiTemplate.find('td[data-label="id"]').text(oRules.id);
                                uiTemplate.find('td[data-label="name"]').text(oRules.name);


                                var uiEditableTemplate = uiRuleEditableTemplate.clone().removeClass('template');

                                uiEditableTemplate.attr('data-rule-id',oRules.id);
                                uiEditableTemplate.find('td[data-label="id"]').text(oRules.id);
                                uiEditableTemplate.find('[data-input="name"]').val(oRules.name);
        
                                uiContainer.append(uiTemplate);
                                uiEditableContainer.append(uiEditableTemplate);

                                admin.store_operations.show_spinner(uiButton,false);

                                uiRBUList.find("a.add-rbu-to-list").removeClass('hidden');
                                uiRBUList.find("table.rbu-list-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                                uiRBUList.find("table.rbu-list-table-display tbody.sbu_settings_container_add_row").find('input[type="text"]').val('');
                                uiRBUList.find("button.add-value").addClass('hidden');
                                uiRBUList.find("button.cancel-add").addClass('hidden');

                            }

                        }
                    }

               admin.agents.ajax(oAjaxConfig);
            }
        },

        'fetch_rbu_districts_list' : function() {
            var oAjaxConfig = {
                "type"   : "Get",
                "data"   : {
                    "params" : {
                        "is_deleted" : 0
                    }
                },

                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : admin.config('url.api.jfc.store') + "/stores/rbu_districts_list",
                "success": function (oData) {
                    if( oData.data.length > 0)
                    {
                        var oRules = oData.data;
                        //console.log(oRules);
                        admin.store_operations.render_rbu_districts_list(oRules);
                    }
                }
            }

            admin.agents.ajax(oAjaxConfig);
        }, 

        'render_rbu_districts_list' : function(oRules) {
            if(typeof(oRules) != 'undefined') {
                var uiRuleTemplate = $('table.rbu-districts-list-table-display').find('tr.rule_tr.template');
                var uiRuleEditableTemplate = $('table.rbu-districts-list-table-editable').find('tr.rule_editable_tr.template');
                var uiContainer = $('table.rbu-districts-list-table-display').find('tbody.sbu_settings_container')
                var uiEditableContainer = $('table.rbu-districts-list-table-editable').find('tbody.sbu_settings_editable_container')
                $.each(oRules, function(index ,rule) {
                    var uiTemplate = uiRuleTemplate.clone().removeClass('template');
                    for (var key in rule) {
                        if (rule.hasOwnProperty(key)) {
                            uiTemplate.attr('data-rule-id',rule.id);
                            uiTemplate.find('[data-label="' + key + '"]').text(rule[key]);
                        }
                    }

                    var uiEditableTemplate = uiRuleEditableTemplate.clone().removeClass('template');
                    for (var key in rule) {
                        if (rule.hasOwnProperty(key)) {
                            uiEditableTemplate.attr('data-rule-id',rule.id);
                            uiEditableTemplate.find('[data-label="' + key + '"]').text(rule[key]);
                            uiEditableTemplate.find('[data-input="' + key + '"]').val(rule[key]);
                            uiEditableTemplate.find('[data-input-rbu="' + key + '"]').val(rule.rbu_name);
                            uiEditableTemplate.find('[data-input-rbu="' + key + '"]').attr('data-value',rule[key]);
                            uiEditableTemplate.find('[data-input-name="' + key + '"]').attr('name', rule[key] );
                        }
                    }

                    uiContainer.append(uiTemplate)
                    uiEditableContainer.append(uiEditableTemplate)
                })
            }
        },  
         
        'update_rbu_districts_list': function (oParams,uiButton) {
            if (typeof(oParams) != 'undefined') {
                var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                        "url"    : admin.config('url.api.jfc.store') + "/stores/rbu_districts_list_update",
                        "success": function (data) {
                            //console.log(data);
                            if(typeof(data) !== 'undefined')
                            {                      
                                //console.log(data);
                                admin.store_operations.show_spinner(uiButton,false);
                                uiRBUDistrictsList.find("a.edit-values").removeClass('hidden');
                                uiRBUDistrictsList.find("button").addClass('hidden');
                                uiRBUDistrictsList.find("table.rbu-districts-list-table-editable").addClass('hidden');
                                uiRBUDistrictsList.find("table.rbu-districts-list-table-display").removeClass('hidden');

                                uiRBUDistrictsList.find("a.add-rbu-districts-to-list").removeClass('hidden');
                                uiRBUDistrictsList.find("table.rbu-districts-list-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                                uiRBUDistrictsList.find("button.add-value").addClass('hidden');
                                uiRBUDistrictsList.find("button.cancel-add").addClass('hidden');
                            }

                        }
                    }

               admin.agents.ajax(oAjaxConfig);
            }
        },

        'add_rbu_districts_list': function (oParams,uiButton) {
            if (typeof(oParams) != 'undefined') {
                var oAjaxConfig = {
                        "type"   : "POST",
                        "data"   : oParams,
                        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                        "url"    : admin.config('url.api.jfc.store') + "/stores/rbu_districts_list_add",
                        "success": function (data) {
                            //console.log(data);
                            if(typeof(data) !== 'undefined')
                            {                      
                                //console.log(data);
                                //admin.store_operations.show_spinner(uiButton,false);
                                var oRules = data.data;
                                var uiRuleTemplate = $('table.rbu-districts-list-table-display').find('tr.rule_tr.template');
                                var uiRuleEditableTemplate = $('table.rbu-districts-list-table-editable').find('tr.rule_editable_tr.template');
                                var uiContainer = $('table.rbu-districts-list-table-display').find('tbody.sbu_settings_container')
                                var uiEditableContainer = $('table.rbu-districts-list-table-editable').find('tbody.sbu_settings_editable_container')

                                var uiTemplate = uiRuleTemplate.clone().removeClass('template');

                                uiTemplate.attr('data-rule-id',oRules.id);
                                uiTemplate.find('td[data-label="id"]').text(oRules.id);
                                uiTemplate.find('td[data-label="name"]').text(oRules.name);
                                uiTemplate.find('td[data-label="rbu_name"]').text(oRules.rbu_name);


                                var uiEditableTemplate = uiRuleEditableTemplate.clone().removeClass('template');

                                uiEditableTemplate.attr('data-rule-id',oRules.id);
                                uiEditableTemplate.find('td[data-label="id"]').text(oRules.id);
                                uiEditableTemplate.find('input[data-input-rbu="rbu_id"]').val(oRules.rbu_name);
                                uiEditableTemplate.find('[data-input="name"]').val(oRules.name);
        
                                uiContainer.append(uiTemplate);
                                uiEditableContainer.append(uiEditableTemplate);

                                admin.store_operations.show_spinner(uiButton,false);

                                uiRBUDistrictsList.find("a.add-rbu-districts-to-list").removeClass('hidden');
                                uiRBUDistrictsList.find("table.rbu-districts-list-table-display tbody.sbu_settings_container_add_row").addClass('hidden');
                                uiRBUDistrictsList.find("table.rbu-districts-list-table-display tbody.sbu_settings_container_add_row").find('input[type="text"]').val('');
                                uiRBUDistrictsList.find("button.add-value").addClass('hidden');
                                uiRBUDistrictsList.find("button.cancel-add").addClass('hidden');

                            }

                        }
                    }

               admin.agents.ajax(oAjaxConfig);
            }
        }




    }//end platform prototype

    String.prototype.capitalize = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }

}());

$(window).load(function () {
    admin.store_operations.initialize();
});