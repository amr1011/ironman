/**
 *  DST Report Class
 *
 *
 *
 */
(function () {
    "use strict";

    CPlatform.prototype.order_reports = {

        'initialize': function () {

            admin.order_reports.fetch_provinces($('div.select.select_provinces'));

            if ($().datetimepicker !== undefined) {
                var dateTimePickerOptions = {
                    format  : 'MMMM DD, YYYY',
                    pickTime: false
                };

                var uiDstDateFrom = $('#order_reports_date_from'),
                    uiDstDateTo = $('#order_reports_date_to');

                uiDstDateFrom.datetimepicker(dateTimePickerOptions);
                uiDstDateTo.datetimepicker(dateTimePickerOptions);

                uiDstDateFrom.on("dp.change", function (e) {
                    uiDstDateTo.data('DateTimePicker').setMinDate(e.date);
                });

                uiDstDateTo.on("dp.change", function (e) {
                    uiDstDateFrom.data('DateTimePicker').setMaxDate(e.date);
                });
            }

            if ($().datetimepicker !== undefined) {
                var dateTimePickerOptions = {
                    format  : 'MMMM DD, YYYY',
                    pickTime: false
                };

                var uiRepeatDateFrom = $('#repeat-date-from'),
                    uiRepeatDateTo = $('#repeat-date-to');

                uiRepeatDateFrom.datetimepicker(dateTimePickerOptions);
                uiRepeatDateTo.datetimepicker(dateTimePickerOptions);

                uiRepeatDateFrom.on("dp.change", function (e) {
                    uiRepeatDateTo.data('DateTimePicker').setMinDate(e.date);
                });

                uiRepeatDateTo.on("dp.change", function (e) {
                    uiRepeatDateFrom.data('DateTimePicker').setMaxDate(e.date);
                });
            }

            $('div.select.provinces-repeating-customer-report').on('click','div.option.province-select', function() {
                $(this).parents('div.select.provinces-repeating-customer-report:first').find('div.frm-custom-dropdown-txt').find('input[name="repeat_date_province"]').attr('int-value', $(this).attr('data-value'));
            })

            $('section#repeat').on('click', 'button.repeat_generate_report', function() {
                var uiThis = $(this);
                var sDateFrom = $('#repeat-date-from').find('input:first').val();
                var sDateTo =$('#repeat-date-to').find('input:first').val();
                var sProvinceID = $('section#repeat').find('input[name="repeat_date_province"]').attr('int-value');
                admin.order_reports.show_spinner(uiThis, true);
                if(typeof(sDateFrom) != 'undefined' && typeof(sDateTo) != 'undefined')
                {
                    var oParams = {
                        "province_id" : sProvinceID,
                        "date_start" : sDateFrom,
                        "date_end" : sDateTo,
                        "sbu_id" : 1
                    }

                    var oDstReportAjaxConfig = {
                        "type"   : "GET",
                        "data"   : oParams,
                        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                        "url"    : admin.config('url.api.jfc.reports') + "/reports/repeat_customer",
                        "success": function (oData) {
                            //console.log(oData);
                            if (typeof(oData) != 'undefined') {
                                admin.order_reports.repeat_report_manipulate(oData);
                            }

                            admin.order_reports.show_spinner(uiThis, false);
                        }
                    }

                    admin.order_reports.ajax(oDstReportAjaxConfig);
                }
            });

            $('section#reports').on('click', 'button.generate_by_date', function() {
                var uiThis = $(this);
                var sDateFrom = $('#order_reports_date_from').find('input:first').val();
                var sDateTo = $('#order_reports_date_to').find('input:first').val();
                var sOrderType = $('section#reports').find('input[name="order_type"]').attr('data-value');
                var sProvinceID = $('section#reports').find('input[name="province_id"]').attr('data-value');
                var sTransactionType = $('section#reports').find('input[name="transaction_select"]').attr('data-value');
                var sCallcenterID = $('section#reports').find('input[name="callcenter_select"]').attr('int-value');
                var sStoreID = $('section#reports').find('input[name="store_select"]').attr('int-value');
                admin.order_reports.show_spinner(uiThis, true);
                if(typeof(sDateFrom) != 'undefined' && typeof(sDateTo) != 'undefined')
                {
                    var oParams = {
                        "params": {
                            "limit"     : 9999999999999999,
                            "range_from": sDateFrom,
                            "range_to"  : sDateTo+" 23:59:59",
                            "order_status"  : sOrderType,
                            "province_id" : sProvinceID,
                            "channel_code" : sTransactionType,
                            "callcenter_id" : sCallcenterID,
                            "store_id" : sStoreID
                        },
                        "user_id" : iConstantUserId
                    }

                    var sUrl = admin.config('url.api.jfc.reports') + "/reports/order_reports";
                    if(sOrderType == '4')
                    {
                        sUrl = admin.config('url.api.jfc.reports') + "/reports/followup_reports";
                    }

                    var oDstReportAjaxConfig = {
                        "type"   : "GET",
                        "data"   : oParams,
                        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                        "url"    : sUrl,
                        "success": function (oData) {
                            if (typeof(oData.status) != 'undefined' && typeof(oData.data) != 'undefined') {
                                admin.order_reports.order_report_manipulate(oData.data, sOrderType);
                            }
                            admin.order_reports.show_spinner(uiThis, false);
                        }
                    }

                    admin.order_reports.ajax(oDstReportAjaxConfig);
                }
            });

            $('div.select.order_reports').on('click', 'div.option', function() {
                var uiThis = $(this);
                uiThis.parents('div.select.order_reports:first').find('input:first').attr('data-value', uiThis.attr('data-value'))
            });

            //filter by transaction type selected
            $('section#reports').on('click','div.transaction-selection-order-report div.option', function(){
                var uiThis = $(this);
                uiThis.parents('div.select.transaction-selection-order-report:first').find('input:first').attr('data-value', uiThis.attr('data-value'))

            });

            //filter by callcenter type selected
            $('section#reports').on('click','div.callcenter-selection-product-sales-report div.option', function(){
                var uiThis = $(this);
                uiThis.parents('div.select.callcenter-selection-product-sales-report:first').find('input:first').attr('int-value', uiThis.attr('int-value'))

            });

            //filter by store type selected
            $('section#reports').on('click','div.store-selection-product-sales-report div.option', function(){
                var uiThis = $(this);
                uiThis.parents('div.select.store-selection-product-sales-report:first').find('input:first').attr('int-value', uiThis.attr('int-value'))

            });

            $('section#reports').on('click', 'button.download_excel', function () {
                var sReportTitle = $('ul.report-tabs').find('li.active').text().trim();
                $('div#reports').find('table:visible').table2excel({
                    exclude: ".notthis",
                    //filename   : "order_reports_" + moment()
                    filename   : sReportTitle
                });
            });

            $('section#repeat').on('click', 'button.repeat_xls_btn', function() {
                $('div#repeat').find('table:visible').table2excel({
                    exclude: ".notthis",
                    filename   : "repeating_customers_" + moment()
                });
            })

            $('section#dst_for_store_view_data').on('click', 'button.dst-for-store-download', function () {
                $("#dst_reports_specific_content_table").table2excel({
                    exclude: ".notthis",
                    filename   : "dst_detailed_" + moment()
                });
            });


        },

        'repeat_report_manipulate' : function(oData) {
            if(typeof(oData) != 'undefined') {
                $('div.no_generate_report').addClass('hidden');
                $('div#repeat').find('table').addClass('hidden');

                var uiTable,
                    uiTemplate,
                    uiContainer,
                    aManipulated = [];


                uiTable = $('div#repeat').find('table#repeat_table')
                uiTemplate = uiTable.find('tr.repeat_tr.template');
                uiContainer = uiTable.find('tbody.repeat_tr_container');

                $.each(oData, function (key, data) {
                    var uiManipulate = uiTemplate.clone().removeClass('template').removeClass('notthis');

                    uiManipulate.find('[data-label="repeat_month"]').text(data.month);
                    uiManipulate.find('[data-label="repeat_new_customers"]').text(data.new_customers);
                    var new_per = 0;
                    if (data.new_customers > 0 && data.total_customer > 0) {
                        new_per = parseInt(data.new_customers) / parseInt(data.total_customer) * 100;
                    }

                    var prev_per = 0;
                    if (data.total_prev_customer > 0 && data.total_customer > 0) {
                        prev_per = parseInt(data.total_prev_customer) / parseInt(data.total_customer) * 100;
                    }

                    uiManipulate.find('[data-label="repeat_new_customers_per"]').text(new_per + ' %');
                    uiManipulate.find('[data-label="repeat_previous_customers"]').text(data.total_prev_customer);
                    uiManipulate.find('[data-label="repeat_previous_customers_per"]').text(prev_per + ' %');
                    uiManipulate.find('[data-label="repeat_total_customers"]').text(data.total_customer);
                    uiManipulate.find('[data-label="repeat_percentage"]').text(data.percentage + ' %');

                    aManipulated.push(uiManipulate)
                });

                uiContainer.find('tr.repeat_tr:not(.template)').remove();
                uiContainer.append(aManipulated);
                uiTable.removeClass('hidden');
            }
        },

        'order_report_manipulate' : function(oOrders, sOrderType) {
            if(typeof(oOrders) != 'undefined' && sOrderType != 'undefined')
            {
                $('div.no_generate_report').addClass('hidden');
                $('div#reports').find('table').addClass('hidden');

                var uiTable,
                    uiTemplate,
                    uiContainer,
                    aManipulated = [];

                if(sOrderType == "1")
                {
                    uiTable =  $('div#reports').find('table#reroute_orders_table')
                    uiTemplate = uiTable.find('tr.reroute_orders_tr.template');
                    uiContainer = uiTable.find('tbody.reroute_orders_container');

                    $.each(oOrders, function(key, order) {
                        var uiManipulate = uiTemplate.clone().removeClass('template').removeClass('notthis');

                        uiManipulate.find('[data-label="reroute_date"]').text(moment(order.process_date).format('MMMM DD, YYYY | HH:mm:ss a'));
                        uiManipulate.find('[data-label="reroute_order_id"]').text(order.order_id);
                        uiManipulate.find('[data-label="reroute_store_origin"]').text(order.original_store);
                        uiManipulate.find('[data-label="reroute_store_destination"]').text(order.new_store);

                        aManipulated.push(uiManipulate)
                    })

                    uiContainer.find('tr.reroute_orders_tr:not(.template)').remove();
                    uiContainer.append(aManipulated);
                    uiTable.removeClass('hidden');

                }
                else if(sOrderType == "2")
                {
                    uiTable =  $('div#reports').find('table#manually_relayed_table')
                    uiTemplate = uiTable.find('tr.manually_relayed_tr.template');
                    uiContainer = uiTable.find('tbody.manually_relayed_container');
                    
                    $.each(oOrders, function(key, order) {
                        var uiManipulate = uiTemplate.clone().removeClass('template').removeClass('notthis');
                        var oDisplayData = $.parseJSON(order.display_data)
                        
                        uiManipulate.find('[data-label="manually_relayed_order_id"]').text(order.order_id);
                        uiManipulate.find('[data-label="manually_relayed_store_name"]').text(oDisplayData.store_summary);
                        uiManipulate.find('[data-label="manually_relayed_customer_name"]').text(oDisplayData.customer_name);
                        uiManipulate.find('[data-label="manually_relayed_customer_address"]').text(oDisplayData.full_address);
                        uiManipulate.find('[data-label="manually_relayed_customer_contact"]').text(oDisplayData.contact_number);
                        uiManipulate.find('[data-label="manually_relayed_sales"]').text(parseFloat(oDisplayData.total_bill).toFixed(2));
                        uiManipulate.find('[data-label="manually_relayed_transaction_time"]').text(moment(order.date_added).format('MMMM DD, YYYY | h:mm:ss a'));


                        aManipulated.push(uiManipulate)
                    })

                    uiContainer.find('tr.manually_relayed_tr:not(.template)').remove();
                    uiContainer.append(aManipulated);
                    uiTable.removeClass('hidden');
                }
                else if(sOrderType == "3")
                {
                    uiTable =  $('div#reports').find('table#rejected_orders_table')
                    uiTemplate = uiTable.find('tr.rejected_orders_tr.template');
                    uiContainer = uiTable.find('tbody.rejected_orders_container');
                    
                    $.each(oOrders, function(key, order) {
                        var uiManipulate = uiTemplate.clone().removeClass('template').removeClass('notthis');

                        var oDisplayData = $.parseJSON(order.display_data)

                        uiManipulate.find('[data-label="rejected_orders_store_name"]').text(oDisplayData.store_summary);
                        uiManipulate.find('[data-label="rejected_orders_customer_name"]').text(oDisplayData.customer_name);
                        uiManipulate.find('[data-label="rejected_orders_order_id"]').text(order.order_id);
                        uiManipulate.find('[data-label="rejected_orders_date"]').text(moment(order.date_added).format('MMMM DD, YYYY'));
                        uiManipulate.find('[data-label="rejected_orders_time"]').text(moment(order.date_added).format('h:mm:ss a'));
                        uiManipulate.find('[data-label="rejected_orders_reason"]').text(order.reason);

                        aManipulated.push(uiManipulate)
                    })

                    uiContainer.find('tr.rejected_orders_tr:not(.template)').remove();
                    uiContainer.append(aManipulated);
                    uiTable.removeClass('hidden');
                }

                else if(sOrderType == "4")
                {
                    uiTable =  $('div#reports').find('table#follow_up_order_table')
                    uiTemplate = uiTable.find('tr.follow_up_order_tr.template');
                    uiContainer = uiTable.find('tbody.follow_up_order_container');

                    $.each(oOrders, function(key, order) {
                        var uiManipulate = uiTemplate.clone().removeClass('template').removeClass('notthis');
                        var oDisplayData = $.parseJSON(order.display_data)
                        
                        uiManipulate.find('[data-label="follow_up_date"]').text(moment(order.followup_date).format('MMMM DD, YYYY h:mm:ss a'));
                        uiManipulate.find('[data-label="follow_up_agent"]').text(order.agent);
                        uiManipulate.find('[data-label="follow_up_count"]').text(1);
                        uiManipulate.find('[data-label="follow_up_fu_date"]').text(moment(order.followup_date).format('MMMM DD, YYYY'));
                        uiManipulate.find('[data-label="follow_up_fu_time"]').text(moment(order.followup_date).format('h:mm:ss a'));
                        // uiManipulate.find('[data-label="follow_up_customer_name"]').text(order.customer_name);
                        // uiManipulate.find('[data-label="follow_up_store_code"]').text(order.store_code);
                        // uiManipulate.find('[data-label="follow_up_store_name"]').text(order.store_name);
                        // uiManipulate.find('[data-label="follow_up_province"]').text($('div.select_provinces.select').find('div.option[data-value="'+order.province_id+'"]').text());
                        // uiManipulate.find('[data-label="follow_up_delivery_guarantee"]').text(order.serving_time);
                        uiManipulate.find('[data-label="follow_up_customer_name"]').text(oDisplayData.customer_name);
                        uiManipulate.find('[data-label="follow_up_store_code"]').text((oDisplayData.store_summary != 'undefined') ? oDisplayData.store_summary.split('-')[0] : '');
                        uiManipulate.find('[data-label="follow_up_store_name"]').text(oDisplayData.store_summary);
                        uiManipulate.find('[data-label="follow_up_province"]').text($('div.select-filter-provinces.select').find('div.option[data-value="'+order.province_id+'"]').text());
                        uiManipulate.find('[data-label="follow_up_delivery_guarantee"]').text(oDisplayData.serving_time);
                        
                        uiManipulate.find('[data-label="follow_up_order_id"]').text(order.order_id);
                        uiManipulate.find('[data-label="follow_up_date_recieved"]').text(moment(order.date_added).format('MMMM DD, YYYY'));
                        uiManipulate.find('[data-label="follow_up_time_recieved"]').text(moment(order.date_added).format('h:mm:ss a'));
                        uiManipulate.find('[data-label="follow_up_date_completed"]').text(moment(order.delivery_date).format('MMMM DD, YYYY'));
                        uiManipulate.find('[data-label="follow_up_time_completed"]').text(moment(order.delivery_date).format('h:mm:ss a'));

                        aManipulated.push(uiManipulate)
                    })

                    uiContainer.find('tr.follow_up_order_tr:not(.template)').remove();
                    uiContainer.append(aManipulated);
                    uiTable.removeClass('hidden');
                }
                else if(sOrderType == "5")//NKAG
                {
                    uiTable =  $('div#reports').find('table#nkag_report_table')
                    uiTemplate = uiTable.find('tr.nkag_report_tr.template');
                    uiContainer = uiTable.find('tbody.nkag_report_container');
                    
                    $.each(oOrders, function(key, order) {
                        var uiManipulate = uiTemplate.clone().removeClass('template').removeClass('notthis');
                        var oDisplayData = $.parseJSON(order.display_data)
                        
                        uiManipulate.find('[data-label="nkag_report_order_id"]').text(order.id);
                        uiManipulate.find('[data-label="nkag_report_store_name"]').text(((oDisplayData.store_summary != '') ? oDisplayData.store_summary : 'N/A'));
                        uiManipulate.find('[data-label="nkag_report_nkag_account"]').text(oDisplayData.nkag_account);
                        //uiManipulate.find('[data-label="nkag_report_customer_name"]').text(oDisplayData.customer_name);
                        //uiManipulate.find('[data-label="nkag_report_customer_address"]').text(oDisplayData.full_address);
                        //uiManipulate.find('[data-label="nkag_report_customer_contact"]').text(oDisplayData.contact_number);
                        uiManipulate.find('[data-label="nkag_report_customer_information"]').text(oDisplayData.customer_name+', '+oDisplayData.full_address+', '+oDisplayData.contact_number);
                        
                        var sItems = '';
                        $.each(oDisplayData.products, function(k, product) {
                           if(typeof(product) !=="undefined" && product != '' && product != null)
                           {
                             //console.log(product)

                             sItems += product.quantity +" "+ product.product_name +" @ "+ product.price +" = "+product.sub_total +", ";

                               if(typeof(product.addons) != 'undefined' && product.addons != null)
                               {
                                   $.each(product.addons, function(k_a, addon) {
                                       sItems += addon.quantity +" "+ addon.product_name +" @ "+ addon.price +" = "+addon.sub_total +", ";
                                   })
                               }

                               if(typeof(product.special) != 'undefined' && product.special != null)
                               {
                                   $.each(product.special, function(k_s, special) {
                                       sItems += special.quantity +" "+ special.product_name +" @ "+ special.price +" = "+special.sub_total +", ";
                                   })
                               }

                               if(typeof(product.upgrades) != 'undefined' && product.upgrades != null)
                               {
                                   $.each(product.upgrades, function(k_u, upgrade) {
                                       sItems += upgrade.quantity +" "+ upgrade.product_name +" @ "+ upgrade.price +" = "+upgrade.sub_total +", ";
                                   })
                               }
                           }
                               
                        });

                        uiManipulate.find('[data-label="nkag_report_items"]').text(sItems);
                                                
                        uiManipulate.find('[data-label="nkag_report_sales"]').text(parseFloat(oDisplayData.total_bill).toFixed(2));
                        uiManipulate.find('[data-label="nkag_report_transaction_time"]').text(moment(order.date_added).format('MMMM DD, YYYY | h:mm:ss a'));


                        aManipulated.push(uiManipulate)
                    })

                    uiContainer.find('tr.nkag_report_tr:not(.template)').remove();
                    uiContainer.append(aManipulated);
                    uiTable.removeClass('hidden');
                }

                if($('div#reports').find('table:visible').length > 0)
                {
                    $('section#reports').find('button.download_excel').prop('disabled', false)
                }
                else
                {
                    $('section#reports').find('button.download_excel').prop('disabled', true)
                }

            }
        },

        'fetch_provinces' : function(uiOptionContainer) {

            var oAddressAjaxConfig = {
                "type"   : "Get",
                "data"   : [],
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : admin.config('url.api.jfc.gis') + "/gis/province",
                "success": function (data) {
                    if(typeof(data) != 'undefined')
                    {
                        admin.order_reports.render_provinces(data.data, uiOptionContainer);
                    }
                }
            }

            admin.order_reports.ajax(oAddressAjaxConfig);

        },

        'render_provinces' : function(oProvinces, uiOptionContainer) {
            if(typeof(oProvinces) != 'undefined' && typeof(uiOptionContainer) != 'undefined')
            {
                var sHtml = '';
                $.each(oProvinces, function(key, province) {
                    sHtml += '<div class="option province-select" data-value="'+province.id+'">'+province.name+'</div>'    
                })
                uiOptionContainer.find('div.frm-custom-dropdown-option').html('');
                uiOptionContainer.find('div.frm-custom-dropdown-option').append(sHtml);
            }
        },

        'ajax': function (oAjaxConfig) {
            if (typeof(oAjaxConfig) != 'undefined') {
                admin.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        'show_spinner': function (uiElem, bShow) {
            if (bShow == true) {
                if (uiElem.find('i.fa-spinner').length > 0) {
                    uiElem.find('i.fa-spinner').removeClass('hidden');
                }
                else {
                    uiElem.removeClass('hidden');
                }

                uiElem.prop('disabled', true);
            }
            else {
                if (uiElem.find('i.fa-spinner').length > 0) {
                    uiElem.find('i.fa-spinner').addClass('hidden');
                }
                uiElem.prop('disabled', false);
            }
        }

    }

}());

$(window).load(function () {
    admin.order_reports.initialize();
});


