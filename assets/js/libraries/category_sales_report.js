/**
 *  Product Sales Report Class
 *
 *
 *
 */
(function () {
    "use strict";

    var generateByDateBtn,
        generateByPeriodBtn,
        generateByDateDiv,
        generateByPeriodDiv,
        noGenerateReport,
        reportContent,
        uiDownloadXLSButton,
        momentServerTime,
        serverTime,
        uiFilterReportContainer
        ;

    function getServerTime(fnCallback) {
        var oAjaxConfig = {
            "type"   : "GET",
            "data"   : [],
            "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
            "url"    : admin.config('url.api.jfc.users') + "/users/time",
            "success": function (sTime) {
                serverTime = sTime;
                momentServerTime =  moment(serverTime);
                //console.log(momentServerTime);
            }
        };
        admin.category_sales_report.ajax(oAjaxConfig);
    }

    function commatifyNumber(x) {
        return x.toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ",");
    }

    function getProductCategories() {
        var oParams = {
                        "params": {
                            "sbu_id"  : 1,
                            "group_by": 'pc.id'
                        },
                        "user_id" : iConstantUserId
                      };

        var oAjaxConfig = {
            "type"   : "GET",
            "data"   : oParams,
            "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
            "url"    : admin.config('url.api.jfc.products') + "/products/categories",
            "success": function (data) {
                //console.log(data);
                if(typeof(data.data)!=="undefined")
                {
                    //console.log(data);
                    admin.category_sales_report.populate_product_categories(data);
                    cr8v.add_to_storage('categories',data.data);
                }
            }
        };
        admin.category_sales_report.ajax(oAjaxConfig);
    }

    CPlatform.prototype.category_sales_report = {

    'initialize' : function () {
        //initialize server time
        getServerTime();
        //initialize product categories
        getProductCategories();

        //buttons
        generateByDateBtn = $('section#sales_reports_by_category_header').find('button.generate_by_date');
        generateByPeriodBtn = $('section#sales_reports_by_category_header').find('button.generate_by_period');
        generateByDateDiv = $('section#sales_reports_by_category_header').find('div.generate_by_date');
        generateByPeriodDiv = $('section#sales_reports_by_category_header').find('div.generate_by_period');
        //default content
        noGenerateReport = $('section#sales_reports_by_category_header').find('div.no_report');
        //main table content
        reportContent = $('section.content').find('div#sales_reports_by_category_content');
        //download xls button
        uiDownloadXLSButton   = $('section#sales_reports_by_category_header').find('button.download_excel_file');
        //filter report container
        uiFilterReportContainer = $('section#sales_reports_by_category_header').find('div.filter_table_container');

        noGenerateReport.show();
        reportContent.hide();
        uiDownloadXLSButton.attr("disabled",true);

        //remove all data when sakes reports by product tab was clicked
        $('nav ul li[data-header="sales_reports_by_category_header"]').on('click',function(){
            noGenerateReport.show();
            reportContent.hide();
            uiDownloadXLSButton.attr("disabled",true);
            generateByDateDiv.find('input[name="psr_date_from"]').val("");
            generateByDateDiv.find('input[name="psr_date_to"]').val("");
            generateByPeriodDiv.find("input#psr_period_date_to").val("");
            uiFilterReportContainer.find('input[name="search_filter"]').val("");
        });

        if ($().datetimepicker !== undefined) {
                var dateTimePickerOptions = {
                    format  : 'MMMM DD, YYYY',
                    pickTime: false
                };

                var uiPsrDateFrom = $('#csr_date_from'),
                    uiPsrDateTo = $('#csr_date_to');

                uiPsrDateFrom.datetimepicker(dateTimePickerOptions);
                uiPsrDateTo.datetimepicker(dateTimePickerOptions);


                uiPsrDateFrom.on("dp.change", function (e) {
                    uiPsrDateTo.data('DateTimePicker').setMinDate(e.date);
                });

                uiPsrDateTo.on("dp.change", function (e) {
                    uiPsrDateFrom.data('DateTimePicker').setMaxDate(e.date);
                });
            }

        //$('body').on('click', 'div.option.store-selection-product-sales-report', function() {
        //    $(this).parents('div.select:first').find('input').attr('int-value', $(this).attr('int-value'))
        //})

        //generate by date button
        generateByDateBtn.on('click', function(){
           //console.log('generate by date');
           var uiStoreID = generateByDateDiv.find('input[name="generate_store"]');
           var dateFromInput = generateByDateDiv.find('input[name="psr_date_from"]').val();
           var dateToInput = generateByDateDiv.find('input[name="psr_date_to"]').val();

           var dateFromValue = '' , dateToValue = '';
           
            if(dateFromInput != '' && dateToInput != '' /*&& uiStoreID.val() !="Select Store"*/)
              {//assemble search by dates
                admin.category_sales_report.show_spinner($(this), true);
                var dateFrom = moment(dateFromInput);
                var dateTo = moment(dateToInput);

                var dateFromValue = moment(dateFrom).format("YYYY-MM-DD 00:00:00");
                var dateToValue = moment(dateTo).format("YYYY-MM-DD 23:59:59");
                dateToValue = moment(dateToValue).format("YYYY-MM-DD HH:mm:ss");

                var oParams = {
                        "params": {
                            "where": [
                                {
                                    "field"   : 'p.sbu_id',
                                    "operator": "=",
                                    "value"   : 1
                                },
                                {
                                    "field"   : 'p.status',
                                    "operator": "=",
                                    "value"   : 1
                                },
                            ],
                            "where_to_cart" : [],
                            "date_from": dateFromValue,
                            "date_to": dateToValue,
                            //"store_id": uiStoreID.attr('int-value')
                        },
                        "user_id" : iConstantUserId
                   }

                var iProvinceID = uiFilterReportContainer.find('input[name="province_select"]').attr('int-value');
                if(iProvinceID > 0)
                {
                    var oProvinceFilter = {
                                    "field"   : 'o.province_id',
                                    "operator": "=",
                                    "value"   : iProvinceID
                                };
                    oParams.params.where_to_cart.push(oProvinceFilter);
                }

                var sTransactionType = uiFilterReportContainer.find('input[name="transaction_select"]').attr('data-value');
                if(sTransactionType != "All Transaction")
                {
                     if(sTransactionType == "Store Channel")
                    {
                        var oTransactionFilter = {
                                    "field"   : 'o.channel_type',
                                    "operator": "=",
                                    "value"   : 'Voice'
                                };
                    }
                    else if (sTransactionType == "Web")
                    {
                        var oTransactionFilter = {
                                    "field"   : 'o.channel_type',
                                    "operator": "=",
                                    "value"   : sTransactionType
                                };
                    }

                    else if (sTransactionType == "SMS")
                    {
                        var oTransactionFilter = {
                                    "field"   : 'o.channel_type',
                                    "operator": "=",
                                    "value"   : sTransactionType
                                };
                    }
                    else if (sTransactionType == "nkag")
                    {
                        var oTransactionFilter = {
                                    "field"   : 'o.channel_code',
                                    "operator": "=",
                                    "value"   : sTransactionType
                                };
                    }
                    else if (sTransactionType == "Food Panda")
                    {
                        var oTransactionFilter = {
                                    "field"   : 'o.channel_type',
                                    "operator": "=",
                                    "value"   : sTransactionType
                                };
                    }
                    else
                    {
                        var oTransactionFilter = {
                                    "field"   : 'o.channel_code',
                                    "operator": "=",
                                    "value"   : sTransactionType
                                };
                    }
                    oParams.params.where_to_cart.push(oTransactionFilter);
                }

                var iStoreID = uiFilterReportContainer.find('input[name="store_select"]').attr('int-value');
                if(iStoreID > 0)
                {
                    var oStoreFilter = {
                                    "field"   : 'o.store_id',
                                    "operator": "=",
                                    "value"   : iStoreID
                                };
                    oParams.params.where_to_cart.push(oStoreFilter);
                }

                var iCallcenterID = uiFilterReportContainer.find('input[name="callcenter_select"]').attr('int-value');
                if(iCallcenterID > 0)
                {
                    var oCallcenterFilter = {
                                    "field"   : 'cu.callcenter_id',
                                    "operator": "=",
                                    "value"   : iCallcenterID
                                };
                    oParams.params.where_to_cart.push(oCallcenterFilter);
                }
                admin.category_sales_report.generate_category_sales_report(oParams);
                   
            }else{//show default page
                noGenerateReport.show();
                reportContent.hide();
                uiDownloadXLSButton.attr('disabled',true);
            }


        });

        //generate when filter by province selected
        uiFilterReportContainer.on('click','div.province-selection-category-sales-report div.option', function(){
            var id = $(this).attr('int-value');
            uiFilterReportContainer.find('input[name="province_select"]').attr('int-value',id);
            
            //filter table
            //admin.category_sales_report.filter_table_from_dropdown();
        });    

        //generate when filter by transaction type selected
        uiFilterReportContainer.on('click','div.transaction-selection-category-sales-report div.option', function(){
            var id = $(this).attr('data-value');
            uiFilterReportContainer.find('input[name="transaction_select"]').attr('data-value',id);
            
            //filter table
            //admin.product_sales_report.filter_table_from_dropdown();
        });

        //generate when filter by store selected
        uiFilterReportContainer.on('click','div.store-selection-category-sales-report div.option', function(){
            var id = $(this).attr('int-value');
            uiFilterReportContainer.find('input[name="store_select"]').attr('int-value',id);
            
            //filter table
            //admin.category_sales_report.filter_table_from_dropdown();
        });    

        //generate when filter by callcenter_id selected
        uiFilterReportContainer.on('click','div.callcenter-selection-category-sales-report div.option', function(){
            var id = $(this).attr('int-value');
            uiFilterReportContainer.find('input[name="callcenter_select"]').attr('int-value',id);
            
            //filter table
            //admin.category_sales_report.filter_table_from_dropdown();
        });

        //generate by period button
        generateByPeriodBtn.on('click', function(){
            //console.log('generate by period');
            var uiStoreID = generateByDateDiv.find('input[name="generate_store"]');
            var periodDate = generateByPeriodDiv.find("input#psr_period_date_to").val();
            var dateFromValue = "";
            var dateToValue = "";
            var oParams = {
                    "params": {
                        "where": [
                            {
                                "field"   : 'p.sbu_id',
                                "operator": "=",
                                "value"   : 1
                            },
                            {
                                "field"   : 'p.status',
                                "operator": "=",
                                "value"   : 1
                            },
                        ],
                        "date_from": dateFromValue,
                        "date_to": dateToValue,
                        //"store_id": uiStoreID.attr('int-value')
                    },
                    "user_id" : iConstantUserId
               }

            if(periodDate=="Hourly")
            {
                dateFromValue = moment(momentServerTime).subtract(1,'hour').format("YYYY-MM-DD HH:mm:ss");
                dateToValue = moment(momentServerTime).format("YYYY-MM-DD HH:mm:ss");
                //console.log(dateFromValue);
                oParams.params.date_from = dateFromValue;
                oParams.params.date_to = dateToValue;
            }
            else if(periodDate=="Daily")
            {
                dateFromValue = moment(momentServerTime).format("YYYY-MM-DD 00:00:00");
                dateToValue = moment(momentServerTime).format("YYYY-MM-DD 23:59:59");
                oParams.params.date_from = dateFromValue;
                oParams.params.date_to = dateToValue;
            }
            else if (periodDate == "Weekly")
            {
                dateFromValue = moment(momentServerTime).startOf('week');
                dateFromValue = moment(dateFromValue).format("YYYY-MM-DD 00:00:00");

                dateToValue = moment(momentServerTime).endOf('week');
                dateToValue = moment(dateToValue).format("YYYY-MM-DD 23:59:59");

                oParams.params.date_from = dateFromValue;
                oParams.params.date_to = dateToValue;
            }
            else if(periodDate == "Monthly")
            {
                dateFromValue = moment(momentServerTime).format("YYYY-MM-01 00:00:00");
                dateToValue = moment(momentServerTime).endOf('month');
                dateToValue = moment(dateToValue).format("YYYY-MM-DD 23:59:59");
                oParams.params.date_from = dateFromValue;
                oParams.params.date_to = dateToValue;
            }
            //console.log(oParams);
            if(periodDate !="" /*&& uiStoreID.val() !="Select Store"*/){
               admin.category_sales_report.show_spinner($(this), true);
               admin.category_sales_report.generate_category_sales_report(oParams);
            }else{//show default page
                noGenerateReport.show();
                reportContent.hide();
                uiDownloadXLSButton.attr('disabled',true);
            }
            

        });

        //download excel button
        uiDownloadXLSButton.on('click', function() {
            //console.log($(this));
            admin.category_sales_report.show_spinner($(this), true);
            var uiContainer = reportContent.find("div#category_sales_report_table_excel_container");
            uiContainer.html("");
            var uiTableTemplate = reportContent.find("table#category_sales_report_table").clone().removeAttr("id").attr("id","category_sales_report_table_excel").addClass("hidden");
            //var sDateRange = uiTableTemplate.find('td[data-label-for="search-date-range"]').html();
            //sDateRange = admin.category_sales_report.strip_html(sDateRange);

            //uiTableTemplate.find('td[data-label-for="search-date-range"]').html(sDateRange);
            uiTableTemplate.find('tbody tr[data-visibility="false"]').remove();
            uiContainer.append(uiTableTemplate);
            $('section.content').find('div#category_sales_report_table_excel_container table#category_sales_report_table_excel').table2excel(
                {
                    filename: moment(momentServerTime).format("YYYY-MM-DD") +"-"+ moment()
                }
             );  
                 
             setTimeout(function(){
                admin.category_sales_report.show_spinner(uiDownloadXLSButton, false);
             }, 3000)       
            
        });

        //filter report table
        uiFilterReportContainer.on('click','button.filter_table', function() {
            //console.log($(this));
            var uiSearchVal = uiFilterReportContainer.find('input[name="search_filter"]');
            var sSearchByFilter = uiFilterReportContainer.find('input[name="search_by_filter"]').val();
            var uiSearchFilterColumnEQ = 0;
            var val = $.trim(uiSearchVal.val()).replace(/ +/g, ' ').toLowerCase();
            var rows = reportContent.find("table#category_sales_report_table tbody.category-sales-report-tbody tr");
            rows.show().attr("data-visibility","true").filter(function() {
                var text = $(this).children('td').eq(uiSearchFilterColumnEQ).text().replace(/\s+/g, ' ').toLowerCase();
                return !~text.indexOf(val);
            }).hide().attr("data-visibility","false");
            
        });

        //filter by category
        uiFilterReportContainer.on('click','div.filter_by_category div.frm-custom-dropdown-option div.option',function(){
            var iCategoryID = $(this).attr("data-value");
            if(iCategoryID !=="")
            {
              //console.log(iCategoryID);
                var rows = reportContent.find("table#category_sales_report_table tbody.product-sales-report-tbody tr");
                    rows.show().attr("data-visibility","true").filter(function() {
                    if(typeof($(this).attr('data-categories'))=="undefined")
                    {
                        $(this).hide().attr("data-visibility","false");
                    }
                    else
                    {
                        var text = $(this).attr('data-categories');
                        return !~text.indexOf(iCategoryID);
                    }
                    
                }).hide().attr("data-visibility","false");
            }else{
                uiFilterReportContainer.find('input[name="search_filter"]').val("");
                uiFilterReportContainer.find('button.filter_table').trigger("click");
            }
        });

    },


    'ajax': function (oAjaxConfig) {
        if (typeof(oAjaxConfig) != 'undefined') {
            admin.CconnectionDetector.ajax(oAjaxConfig);
        }
    },
    'generate_category_sales_report': function (oParams) {
        if (typeof(oParams) != 'undefined') {
            var oAjaxConfig = {
                    "type"   : "Get",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : admin.config('url.api.jfc.reports') + "/reports/product_sales_report",
                    "success": function (data) {
                        //console.log(data);
                        if(typeof(data) !== 'undefined')
                        {   
                            uiFilterReportContainer.removeClass('hidden');                   
                            admin.category_sales_report.show_spinner(generateByDateBtn, false);
                            admin.category_sales_report.show_spinner(generateByPeriodBtn, false);
                            uiDownloadXLSButton.attr("disabled",false);
                            noGenerateReport.hide();
                            reportContent.show();

                            //console.log(data);
                            var aCategories = cr8v.get_from_storage('categories');

                            var oProducts = [];
                            $.each(data.data, function(key_p, product) {
                                if(product.categories != null)
                                {
                                    data.data[key_p].category = [];

                                    //console.log(product.categories)
                                    data.data[key_p]['category'] = product.categories.split(',');
                                    oProducts.push(product);
                                    //console.log(key_p);
                                }
                            })

                            //console.log(oProducts);

                            $.each(aCategories, function(key, category) {
                                aCategories[key].cart_item_food_sales_count = 0;
                                aCategories[key].cart_item_quantity_count = 0;
                                aCategories[key].cart_item_sales_count = 0;
                                aCategories[key].cart_items_count = 0;
                                
                                var oFilteredProducts =   oProducts;
                                oFilteredProducts = oFilteredProducts.filter(function(el){
                                    return $.inArray(category.category_id, el['category']) > -1; 
                                });

                                $.each(oFilteredProducts, function(key_p, product) {
                                    aCategories[key].cart_item_food_sales_count += product.cart_item_food_sales_count;  
                                    aCategories[key].cart_item_quantity_count += product.cart_item_quantity_count; 
                                    aCategories[key].cart_item_sales_count += product.cart_item_sales_count; 
                                    aCategories[key].cart_items_count += product.cart_items_count; 
                                    aCategories[key].search_date_from = product.search_date_from;
                                    aCategories[key].search_date_to = product.search_date_to;
                                })

                                
                            
                            });

                            var oManipulateData = {
                                'data' : aCategories,
                                'message' : [],
                                'product_quantity_count' : data.product_quantity_total,
                                'product_sales_total' : data.product_sales_total
                            }


                            var uiContainer = reportContent.find("table#category_sales_report_table tbody.category-sales-report-tbody");
                            uiContainer.html("");
                            $.each(aCategories, function(key, value) {
                                 var uiTemplate = reportContent.find("table#category_sales_report_table_template tr.template").clone().removeClass("template");
                                 var uiManipulatedTemplate = admin.category_sales_report.manipulate_template(uiTemplate,value,data.product_sales_total,data.product_quantity_total);
                                 admin.category_sales_report.clone_append(uiManipulatedTemplate, uiContainer);
                            });
                            
                            //quantity
                            var all = 0;
                            $('td[data-label-for="quantity-count"]:not(.template):visible').each(function(){
                                all += parseFloat($(this).text().replace('%', ''));
                            })

                            $('td[data-label-for="quantity-percentage"]:not(.template):visible').each(function(){
                                var quantity = $(this).siblings('td[data-label-for="quantity-count"]').text();
                                var percentage = (quantity/ all ) * 100;
                                if(isNaN(percentage)==true)
                                {
                                    percentage = 0;
                                }
                                $(this).text( percentage.toFixed(2) +'%')
                            })
                            
                            //total sales
                            var t_all = 0;
                            $('td[data-label-for="sales-count"]:not(.template):visible').each(function(){
                                t_all += parseFloat($(this).text().replace(',', ''));
                            })

                            $('td[data-label-for="sales-percentage"]:not(.template):visible').each(function(){
                                var quantity = $(this).siblings('td[data-label-for="sales-count"]').text();
                                quantity = parseFloat(quantity.replace(',',''));
                                var percentage = (quantity/ t_all ) * 100;
                                if(isNaN(percentage)==true)
                                {
                                    percentage = 0;
                                }
                                $(this).text( percentage.toFixed(2) +'%')
                            })


                            
                        }

                    }
                }

           admin.category_sales_report.ajax(oAjaxConfig);
        }
    },
    'manipulate_template': function (uiTemplate, oSearchResult, iProductSalesTotal,iProductQuantityTotal) {
        if (typeof(uiTemplate) !== 'undefined' && typeof(oSearchResult) !== 'undefined') {
            var i = oSearchResult;
            var uiManipulatedTemplate = uiTemplate;
            uiManipulatedTemplate.attr('data-id',i.id);
            //for the categories
            if(i.categories !=="null")
            {
               uiManipulatedTemplate.attr('data-categories',i.categories); 
            }
            //for the basic table column
            uiManipulatedTemplate.find('td[data-label-for="product-name"]').html(i.category_name);
            uiManipulatedTemplate.find('td[data-label-for="food-sales-count"]').text(commatifyNumber(i.cart_item_food_sales_count.toFixed(2)) +'');
            uiManipulatedTemplate.find('td[data-label-for="sales-count"]').text(commatifyNumber(i.cart_item_sales_count.toFixed(2)) +'');
            uiManipulatedTemplate.find('td[data-label-for="quantity-count"]').text(commatifyNumber(i.cart_item_quantity_count));
            
            //computation for sales percentage
            var iSalesPercentage = 0;
            iSalesPercentage = parseInt(i.cart_item_sales_count) / iProductSalesTotal;
            iSalesPercentage = iSalesPercentage * 100;
            iSalesPercentage = iSalesPercentage.toFixed(2);
            if(iSalesPercentage =="NaN"){
                iSalesPercentage = 0;
            }
            uiManipulatedTemplate.find('td[data-label-for="sales-percentage"]').text(iSalesPercentage +'%');

            //computation for quantity percentage
            var iQuantityPercentage = 0;
            iQuantityPercentage = parseInt(i.cart_item_quantity_count) / iProductQuantityTotal;
            iQuantityPercentage = iQuantityPercentage * 100;
            iQuantityPercentage = iQuantityPercentage.toFixed(2);
            if(iQuantityPercentage =="NaN"){
                iQuantityPercentage = 0;
            }
            uiManipulatedTemplate.find('td[data-label-for="quantity-percentage"]').text(iQuantityPercentage +'%');

            //for the search dates column
            uiManipulatedTemplate.find('span[data-label-for="search-date-from-month"]').text(i.search_date_from.month);
            uiManipulatedTemplate.find('span[data-label-for="search-date-from-year"]').text(i.search_date_from.year);
            uiManipulatedTemplate.find('span[data-label-for="search-date-from-day"]').text(i.search_date_from.day);
            uiManipulatedTemplate.find('span[data-label-for="search-date-from-day-now"]').text(i.search_date_from.day_now);

            uiManipulatedTemplate.find('span[data-label-for="search-date-to-month"]').text(i.search_date_to.month);
            uiManipulatedTemplate.find('span[data-label-for="search-date-to-year"]').text(i.search_date_to.year);
            uiManipulatedTemplate.find('span[data-label-for="search-date-to-day"]').text(i.search_date_to.day);
            uiManipulatedTemplate.find('span[data-label-for="search-date-to-day-now"]').text(i.search_date_to.day_now);

            //remove date range from if same date
            if(JSON.stringify(i.search_date_from)===JSON.stringify(i.search_date_to)){
                uiManipulatedTemplate.find('div.search-date-range-from').addClass("hidden");
                uiManipulatedTemplate.find('div.search-date-range-dash').addClass("hidden");
            }else{
                uiManipulatedTemplate.find('div.search-date-range-from').removeClass("hidden");
                uiManipulatedTemplate.find('div.search-date-range-dash').removeClass("hidden");
            }
            
            //console.log(iProductSalesTotal);
            return uiManipulatedTemplate;
        }       
    },
    'clone_append': function (uiTemplate, uiContainer) {
        if (typeof(uiTemplate) !== 'undefined' && typeof(uiContainer) !== 'undefined') {
            uiContainer.append(uiTemplate);
        }
    },
    'strip_html': function (text) {
        var regex = /(<([^>]+)>)/ig;
        return text.replace(regex, "");
    }, 
    'show_spinner': function (uiElem, bShow) {
        if (bShow == true) {
            uiElem.find('i.fa-spinner').removeClass('hidden');
            uiElem.prop('disabled', true);
        }
        else {
            uiElem.find('i.fa-spinner').addClass('hidden');
            uiElem.prop('disabled', false);
        }
    },
    'populate_product_categories': function (oData) {
        if (typeof(oData) !== 'undefined') {
            var uiContainer = uiFilterReportContainer.find("div.filter_by_category div.frm-custom-dropdown-option");
            $.each(oData.data, function(key, value) {
                //console.log(value); 
                    var sHtml = '<div class="option" data-value="'+value.category_id+'">'+value.category_name+'</div>';
                    admin.category_sales_report.clone_append(sHtml, uiContainer);
            });
            
        }       
    },
    'filter_table_from_dropdown' :function() {
        var dateFromInput = generateByDateDiv.find('input[name="psr_date_from"]').val();
        var dateToInput = generateByDateDiv.find('input[name="psr_date_to"]').val();
        
        var dateFromValue = moment(dateFromInput).format("YYYY-MM-DD 00:00:00");
        var dateToValue = moment(dateToInput).format("YYYY-MM-DD 23:59:59");
        dateToValue = moment(dateToValue).format("YYYY-MM-DD HH:mm:ss");

        var oFilters = {
                "params": {
                    "where": [
                        {
                            "field"   : 'p.sbu_id',
                            "operator": "=",
                            "value"   : 1
                        },
                        {
                            "field"   : 'p.status',
                            "operator": "=",
                            "value"   : 1
                        },
                    ],
                    "where_to_cart" : [],
                    "date_from": dateFromValue,
                    "date_to": dateToValue,
                },
                "user_id" : iConstantUserId   
            };

        var iProvinceID = uiFilterReportContainer.find('input[name="province_select"]').attr('int-value');
        if(iProvinceID > 0)
        {
            var oProvinceFilter = {
                            "field"   : 'o.province_id',
                            "operator": "=",
                            "value"   : iProvinceID
                        };
            oFilters.params.where_to_cart.push(oProvinceFilter);
        }

        var iStoreID = uiFilterReportContainer.find('input[name="store_select"]').attr('int-value');
        if(iStoreID > 0)
        {
            var oStoreFilter = {
                            "field"   : 'o.store_id',
                            "operator": "=",
                            "value"   : iStoreID
                        };
            oFilters.params.where_to_cart.push(oStoreFilter);
        }

        var iCallcenterID = uiFilterReportContainer.find('input[name="callcenter_select"]').attr('int-value');
        if(iCallcenterID > 0)
        {
            var oCallcenterFilter = {
                            "field"   : 'cu.callcenter_id',
                            "operator": "=",
                            "value"   : iCallcenterID
                        };
            oFilters.params.where_to_cart.push(oCallcenterFilter);
        }

        //console.log(oFilters);

        //render ajax
        admin.category_sales_report.generate_category_sales_report(oFilters);
    },

    }

}());

$(window).load(function () {
    admin.category_sales_report.initialize();
});


