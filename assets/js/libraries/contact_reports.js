(function () {
    "use strict";


    var generateByDateBtn,
        generateByDateDiv,
        showGenerateReport,
        noGenerateReport,
        reportContent,
        datepickerDateFrom,
        datepickerDateTo,
        cnReportTable,
        cnReportXlsBtn,
        cnReportLink,
        getLimit = 9999999;

    CPlatform.prototype.contact_reports = {

        'initialize' : function () {

            generateByDateBtn       = $('section[header-content="contact"]').find('button.cn_generate_by_date');
            generateByDateDiv       = $('section[header-content="contact"]').find('div.cn_generate_by_date');
            showGenerateReport      = $('section[header-content="contact"]').find('div.show_generate_report');
            noGenerateReport        = $('section[header-content="contact"]').find('div.no_generate_report');
            reportContent           = $('div[content="contact"]');
            datepickerDateFrom      = $('section[header-content="contact"]').find('div#cn-report-date-from');
            datepickerDateTo        = $('section[header-content="contact"]').find('div#cn-report-date-to');
            cnReportTable           = $('div[content="contact"]').find('table#cn_report_table');
            cnReportXlsBtn          = $('section[header-content="contact"]').find('button.cn_report_xls_btn');
            cnReportLink            = $('div[content="reports"]').find('li[content="contact"]');

            //hide/show on initialize
            noGenerateReport.show();
            showGenerateReport.hide();
            reportContent.hide();

            //contact number report li click
            cnReportLink.on('click', function(){
                //show no generate report, clear input values
                noGenerateReport.show();
                showGenerateReport.hide();
                reportContent.hide();
                cnReportXlsBtn.prop('disabled', true);
                datepickerDateFrom.find('input[name="cn_report_date_from"]').val('');
                datepickerDateTo.find('input[name="cn_report_date_to"]').val('');

                //trigger click first province
                $('div.select.provinces-contact-num-report').find('div.option.province-select:first').trigger('click');

                var dateTimePickerOptions = {
                    format  : 'MMMM DD, YYYY',
                    pickTime: false
                };

                //set date picker options
                datepickerDateFrom.datetimepicker(dateTimePickerOptions);
                datepickerDateTo.datetimepicker(dateTimePickerOptions);

                //change min date of date to not allow to pass 
                datepickerDateFrom.on("dp.change", function (e) {
                    datepickerDateTo.data('DateTimePicker').setMinDate(e.date);
                });

                datepickerDateTo.on("dp.change", function (e) {
                    datepickerDateFrom.data('DateTimePicker').setMaxDate(e.date);
                });
            });

            if ($().datetimepicker !== undefined) {
                var dateTimePickerOptions = {
                    format  : 'MMMM DD, YYYY',
                    pickTime: false
                };

                //set date picker options
                datepickerDateFrom.datetimepicker(dateTimePickerOptions);
                datepickerDateTo.datetimepicker(dateTimePickerOptions);

                //change min date of date to not allow to pass 
                datepickerDateFrom.on("dp.change", function (e) {
                    datepickerDateTo.data('DateTimePicker').setMinDate(e.date);
                });

                datepickerDateTo.on("dp.change", function (e) {
                    datepickerDateFrom.data('DateTimePicker').setMaxDate(e.date);
                });
            }

            $('div.select.provinces-contact-num-report').on('click','div.option.province-select', function() {
                $(this).parents('div.select.provinces-contact-num-report:first').find('div.frm-custom-dropdown-txt').find('input[name="cn_reports_province"]').attr('int-value', $(this).attr('data-value'));
            })

            $('div.select.type-contact-num-report').on('click','div.option.type-select', function() {
                $(this).parents('div.select.type-contact-num-report:first').find('div.frm-custom-dropdown-txt').find('input[name="cn_reports_type"]').attr('data-value', $(this).attr('data-value'));
            })

            generateByDateBtn.on('click', function(){
                var uiThis = $(this);

                admin.contact_reports.show_spinner(uiThis, true);

                var type = $('section[header-content="contact"]').find('input[name="cn_reports_type"]').attr('data-value');
                var province_id = $('section[header-content="contact"]').find('input[name="cn_reports_province"]').attr('int-value');
                var date_from = $('section[header-content="contact"]').find('input[name="cn_report_date_from"]').val();
                var date_to = $('section[header-content="contact"]').find('input[name="cn_report_date_to"]').val();


                if((date_from != '' && date_to == '') || (date_from == '' && date_to != '')) //do not allow to pass ajax if only one date picker is filled
                {
                    //show no generate report
                    noGenerateReport.show();
                    showGenerateReport.hide();
                    reportContent.hide();
                    cnReportXlsBtn.prop('disabled', true);
                    admin.contact_reports.show_spinner(uiThis, false);
                    noGenerateReport.find('h4.error_message').html('Please select valid<br>date range.');
                }
                else
                {
                    if(moment(date_to).diff(moment(date_from), 'days') < 0)
                    {
                        //show no generate report
                        noGenerateReport.show();
                        showGenerateReport.hide();
                        reportContent.hide();
                        cnReportXlsBtn.prop('disabled', true);
                        admin.contact_reports.show_spinner(uiThis, false);
                        noGenerateReport.find('h4.error_message').html('Please select valid<br>date range.');
                    }
                    else
                    {
                        if(date_from !== null && date_from != '' && date_to !== null && date_to != '')
                        {
                            date_from = moment(date_from).format('YYYY-MM-DD HH:mm:ss');
                            date_to = moment(date_to).format('YYYY-MM-DD');
                            date_to = moment(date_to+" 23:59:59").format('YYYY-MM-DD HH:mm:ss');
                        }
                        else
                        {
                            date_from = '';
                            date_to = '';
                        }

                        //ajax call
                        var oParams = {
                            "params": {
                                "type"          : type,
                                "date_from"     : date_from,
                                "date_to"       : date_to,
                                "limit"         : getLimit,
                                "province_id"   : (province_id != 0) ? province_id : '' 
                            }
                        }
                        admin.contact_reports.get_contact_number_report(oParams, uiThis);
                    }
                }
            })

            $('section[header-content="contact"]').on('click', 'button.cn_report_xls_btn', function() {
                var cnReportTable = $('div[content="contact"]').find('table#cn_report_table'),
                    cnReportXls = $('div[content="contact"]').find('table#cn_report_table_hidden'),
                    type = $('section[header-content="contact"]').find('input[name="cn_reports_type"]').attr('data-value'),
                    uiThis = $(this);
                
                admin.contact_reports.show_spinner(uiThis, true);
                if(cnReportTable.is(':visible'))
                {
                    cnReportTable.table2excel({
                        exclude: ".notthis",
                        name: "Contact Number Report",
                        filename: "Customer Contact_"+uiThis.attr('data-type')+" Number"/*+moment().unix('X')*/
                    });
                }
                setTimeout(function(){
                    admin.contact_reports.show_spinner(uiThis, false);
                }, 3000) 
            });
        },
 
        'get_contact_number_report' :function(oParams, uiThis)
        {
            if(typeof(oParams) != 'undefined' && typeof(uiThis) != 'undefined')
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : admin.config('url.api.jfc.reports') + "/reports/contact_number_report",
                    "success": function (oContactData) {
                        admin.contact_reports.show_spinner(uiThis, false);
                        if(count(oContactData.data) > 0)
                        {
                            admin.contact_reports.manipulate_template(oContactData);
                            admin.contact_reports.show_spinner(cnReportXlsBtn, false);
                            noGenerateReport.hide();
                            showGenerateReport.show();
                            reportContent.show();
                            cnReportXlsBtn.attr('data-type', oParams.params.type)
                        }
                        else
                        {
                            cnReportXlsBtn.prop('disabled', true);
                            noGenerateReport.show();
                            showGenerateReport.hide();
                            reportContent.hide();
                            noGenerateReport.find('h4.error_message').html('No results found.');
                            cnReportXlsBtn.attr('data-type', '')
                        }
                    }
                }   
                admin.contact_reports.ajax(oAjaxConfig);
            }
        },

        'manipulate_template' : function(oContactData)
        {
            if(typeof(oContactData) != 'undefined')
            {
                var uiContentContainer = $('tbody.cn_report_tr_container'),
                    uiHiddenContentContainer = $('tbody.cn_report_tr_container_hidden'),
                    uiClonedTrs = [],
                    uiClonedHiddenTrs = [];

                    uiContentContainer.find('tr.cn_report_tr:not(.template)').remove();
                    uiHiddenContentContainer.find('tr.cn_report_tr_hidden:not(.template)').remove();

                    $.each(oContactData.data, function (key, value){
                        var trTemplate = $('tr.cn_report_tr.template').clone().removeClass('template notthis'),
                            trTemplateHidden = $('tr.cn_report_tr_hidden.template').clone().removeClass('template notthis'),
                            sCustomerName = value.fname+' '+value.mname+' '+value.lname,
                            sCustomerContact = value.contact_num;

                        
                        trTemplate.find('td[data-label="cn_report_name"]').text(sCustomerName);
                        trTemplate.find('td[data-label="cn_report_number"]').text(sCustomerContact);
                        
                        uiClonedTrs.push(trTemplate);

                        // trTemplateHidden.find('td[data-label="cn_report_name"]').text(sCustomerName);
                        // trTemplateHidden.find('td[data-label="cn_report_number"]').text(sCustomerContact);
                        
                        // uiClonedHiddenTrs.push(trTemplateHidden);

                        // uiContentContainer.append(trTemplate);
                        // uiHiddenContentContainer.append(trTemplateHidden);
                    });

                uiContentContainer.append(uiClonedTrs);
                // uiHiddenContentContainer.append(uiClonedHiddenTrs);
            }
        },

        'ajax': function (oAjaxConfig) {
            if (typeof(oAjaxConfig) != 'undefined') {
                admin.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        'show_spinner': function (uiElem, bShow) {
            if (bShow == true) {
                if(uiElem.find('i.fa-spinner').length > 0)
                {
                    uiElem.find('i.fa-spinner').removeClass('hidden');
                }
                else
                {
                    uiElem.append('<i class="fa fa-spinner fa-pulse hidden"></i>').removeClass('hidden');
                }

                uiElem.prop('disabled', true);
            }
            else {
                if(uiElem.find('i.fa-spinner').length > 0)
                {
                    uiElem.find('i.fa-spinner').addClass('hidden');
                }
                uiElem.prop('disabled', false);
            }
        }

    }

}());

$(window).load(function () {
    admin.contact_reports.initialize();
});