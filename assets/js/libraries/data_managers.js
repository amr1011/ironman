(function () {
    "use strict";

    CPlatform.prototype.data_managers = {

        'initialize' : function () {
            $("select").transformDD();
        }
    }

}());

$(window).load(function () {
    admin.data_managers.initialize();
});