(function () {
    "use strict";

    function OpenInNewTab(url) {
      var win = window.open(url, '_blank');
      win.focus();
    }

    CPlatform.prototype.navigation = {

        'initialize' : function () {
            $("select").transformDD();

            $('ul.main_nav#main_nav_headers').on('click', 'li', function() {
                  $('ul.main_nav#main_nav_headers li').removeClass('active');
                  $(this).addClass('active');
                  var uiContent = $(this).attr('content')
                  $('[header-content]').addClass('hidden');
                  $('[header-content="'+uiContent+'"]').removeClass('hidden');
                  $('div[content]').addClass('hidden');
                  $('div[content="'+uiContent+'"]').removeClass('hidden');

                  //for the reports header hide //custom jech
                  $('section#reports').addClass('hidden');

                   //trigger click last active tab
                   if($(this).text()=='Reports')
                   {
                       $('ul.report-tabs').find('li.active').trigger('click');
                   }

                   //for the redirection to callcenter module
                   var sHeaderText = $(this).text().trim();
                   if(sHeaderText =='Order Management' || sHeaderText == 'Coordinator Management')
                   {
                       $(this).removeClass('active');
                       $('ul.main_nav#main_nav_headers li[content="agent_list"]').trigger('click');
                       var sCallcenterModuleUrl = admin.config('url.auth.callcenter') + "auth/login/api_landing_page/" + oUserInfo;
                       OpenInNewTab(sCallcenterModuleUrl);
                   }

                   
            })

            $('ul.sub_nav').on('click', 'li:not([contents])', function () {
                //$('div.sub-nav').addClass('hidden')
                $('div.sub-nav li').removeClass('active');
                $(this).addClass('active');
                var uiContent = $(this).attr('content');
                //$('div.sub-nav[content]').addClass('hidden')
                //$('div.sub-nav[contents]').addClass('hidden')
                //$('div.sub-nav[contentss]').addClass('hidden')
                //$('div.sub-nav[contentsss]').addClass('hidden')
                //$('div.sub-nav[content-1]').addClass('hidden')
                //$('div.sub-nav[content-2]').addClass('hidden')
                //$('div.sub-nav[content-3]').addClass('hidden')
                //$('div.sub-nav[content-4]').addClass('hidden')
                $('div.sub-nav[content="' + uiContent + '"]').removeClass('hidden');
                $('div.sub-nav[contents="' + uiContent + '"]').removeClass('hidden');
                $('div.sub-nav[contentss="' + uiContent + '"]').removeClass('hidden');
                $('div.sub-nav[contentsss="' + uiContent + '"]').removeClass('hidden');
                $('div.sub-nav[content-1="' + uiContent + '"]').removeClass('hidden');
                $('div.sub-nav[content-2="' + uiContent + '"]').removeClass('hidden');
                $('div.sub-nav[content-3="' + uiContent + '"]').removeClass('hidden');
                $('div.sub-nav[content-4="' + uiContent + '"]').removeClass('hidden');

                $('[header-content]').addClass('hidden');
                $('[header-content="' + uiContent + '"]').removeClass('hidden');
                $('section.content div[content]').addClass('hidden');
                $('section.content div[content="' + uiContent + '"]').removeClass('hidden');

               
                $('table#repeat_table').addClass('hidden');
                
                //for the reports //custom jech
                var sHeaderText = $(this).text().trim();
                var uiReports = $('section#reports');
                var uiReportsContent = $('div#reports')

                uiReportsContent.find('table#reroute_orders_table').addClass('hidden');
                uiReportsContent.find('table#manually_relayed_table').addClass('hidden');
                uiReportsContent.find('table#nkag_report_table').addClass('hidden');
                uiReportsContent.find('table#follow_up_order_table').addClass('hidden');
                uiReportsContent.find('table#rejected_orders_table').addClass('hidden');

                uiReports.find('div.transaction-type-dropdown').removeClass('hidden');

                //Rename text
                uiReports.find('h1[data-label="report_header"]').text(sHeaderText);
                //trigger order type
                if(sHeaderText =='Reroute Report')
                {
                   uiReports.find('input[name="order_type"]').attr('data-value',1); 
                }
                else if(sHeaderText =='Manually Relayed Report')
                {
                   uiReports.find('input[name="order_type"]').attr('data-value',2);
                }
                else if(sHeaderText =='Rejected Report')
                {
                   uiReports.find('input[name="order_type"]').attr('data-value',3);
                }
                else if(sHeaderText =='Follow Up Orders')
                {
                   uiReports.find('input[name="order_type"]').attr('data-value',4);                   
                }
                
                else if(sHeaderText =='NKAG Report')
                {
                   uiReports.find('input[name="order_type"]').attr('data-value',5);
                   uiReports.find('div.transaction-type-dropdown').addClass('hidden');                       
                }
                
                //console.log(sHeaderText);

            })
        }
    }

}());

$(window).load(function () {
    admin.navigation.initialize();
});


