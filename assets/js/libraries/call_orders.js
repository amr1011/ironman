/**
 *  Call Orders Class
 *
 *
 *
 */
(function () {
    "use strict";
    var oAutoComplete = {
        oProvinces  : [],
        oCities     : [],
        oPOI        : [],
        oBarangay   : [],
        oStreet     : [],
        oSubdivision: [],

        oPOIFiltered        : [],
        oBarangayFiltered   : [],
        oStreetFiltered     : [],
        oSubdivisionFiltered: []
    };

    var oOrders = {};

    var oAutocompleteParams = {
        "params": {
            "where_like": [
                {
                    "field"   : function () {
                        var uiSearchByValue = $('section #add_new_customer_header input[name="search_by"]').val();
                        if (uiSearchByValue == "Contact Number") {
                            uiSearchByValue = 'ccn.number';
                        }
                        else if (uiSearchByValue == "Order ID") {
                            uiSearchByValue = 'o.id';
                        }
                        else {
                            uiSearchByValue = 'c.first_name';
                        }

                        return uiSearchByValue;
                    },
                    "operator": "LIKE",
                    "value"   : function () {
                        return $('section #add_new_customer_header input#search_customer').val();
                    }
                },
            ]
        }
    }

    var loadingLocalData = 0;
    var loadingLocalDataSeconds = 23;

    CPlatform.prototype.call_orders = {

        /**
         * @method_id: Js_cust01
         * @params : { none }
         * @description : If there is existing admin and the callcenter adds new contact number and/or address, this will just add map to the admin’s contact number and/or address.
         * @return : void
         * @developer : lorenz
         */

        'add': function (oCustomerData) {
            var oSettings = {
                stype  : 'customer',
                url    : callcenter.config('url.api.jfc.customers') + '/customers/add',
                data   : oCustomerData,
                type   : 'POST',
                success: function (oData) {
                    var uiContainer = $('div.add-customer-error-msg');

                    if (typeof(oData) !== 'undefined') {
                       $("html, body").animate({scrollTop: 0}, "slow");
                       // console.log(sData)
                       //var oData = $.parseJSON(sData)
                       // console.log(oData);
                       if (oData.status == 0 && count(oData.message) > 0) //failed
                       {
                           var sError = '';
                           $.each(oData.message, function (k, v) {
                               sError += '<p class="error_messages"><i class="fa fa-exclamation-triangle"></i> ' + v + '</p>';
                           });
                           callcenter.call_orders.add_error_message(sError, uiContainer);
                       }
                       else {
                           oData.data.isCustomerSaved = [];
                           oData.data.isCustomerSaved.push({"isCustomerSaved": 'true'}); 
                           callcenter.add_to_storage("isCustomerSaved", oData.data.isCustomerSaved);
    
                           $('section.content #add_customer_content').find('div.success-msg').show().delay(3000).fadeOut();
                           $('section.content #add_customer_content').find('button.find_retail').prop('disabled', false);
                           $('section.content #add_customer_content').find('button.rta_proceed').attr('customer_id', oData.data.id).attr('address_id', oData.data.address_id);
                           var address_id = oData.data.address_id;
                           
                    
                           oData.data.address_id = [];
                           oData.data.address_id.push({"address_id": address_id});
                           
                           /*happy plus part*/
                            var happy_plus_cards_storage = [];
                            if(oCustomerData.happy_plus_cards.length > 0)
                            {
                                $.each(oCustomerData.happy_plus_cards, function (k ,v) {
                                    var oHappyPlusConfig = {
                                        "type"   : "POST",
                                        "data"   : v,
                                        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                                        "url"    : callcenter.config('url.api.jfc.happy_plus') + "happy_plus/add",
                                        "success": function (oHappyData) {
                                            if(oHappyData.status == true)
                                            {
                                                callcenter.call_orders.insert_happy_plus_map(oData.data.id, oHappyData.data.happy_plus_id);
                                            }
                                            if(oHappyData.status == false)
                                            {
                                                callcenter.call_orders.insert_happy_plus_map(oData.data.id, oHappyData.data.happy_plus_id);
                                            }
                                            var happy_plus_cards = oHappyData;
                                            happy_plus_cards_storage.push({"happy_plus_cards": happy_plus_cards});
                                    
                                        }
                                    }
                                    callcenter.customer_list.get(oHappyPlusConfig);
                                });
                            }
                            
                            callcenter.add_to_storage("happy_plus_cards", happy_plus_cards_storage);
                            //callcenter.add_to_storage("isCustomerSaved", oData.data.isCustomerSaved);
                            callcenter.add_to_storage("current_address_id", oData.data.address_id);
                    
                       }

                       callcenter.call_orders.show_spinner($('button.save_customer'), false);


                    }
                },
                error  : function () {

                }
            }

            callcenter.CconnectionDetector.ajax(oSettings);

        },

        /**
         * @method_id: Js_cust03
         * @params : { none }
         * @description : The function that will harvest all the data of the admin and will return and object containing the admin’s information based on the
         * datastructure required in add admin API.
         * @return : void
         * @developer : lorenz
         */

        'assemble_customer_information': function () {
            var aContact = [];

            //primary mobile
            var sPrimaryContactMobile = "";
            var uiPrimaryContactMobile = $.find('section.content #add_customer_content div.primary_contact_number input.small[name="contact_number[]"]:visible');
            if(typeof(uiPrimaryContactMobile)!== 'undefined')
            {
                $.each(uiPrimaryContactMobile, function () {
                    var sValue = $(this).val().replace(/-/g, '');
                    sPrimaryContactMobile += sValue;
                });
                aContact.push(sPrimaryContactMobile);
            }

            //primary landline
            var sPrimaryContactLandline = "";
            var uiPrimaryContactLandline = $.find('section.content #add_customer_content div.primary_contact_number input.xsmall[name="contact_number[]"]:visible');
            if(typeof(uiPrimaryContactLandline)!== 'undefined')
            {
                var iCounter = 0;
                $.each(uiPrimaryContactLandline, function () {
                    iCounter++;
                    var sValue = $(this).val().replace(/-/g, '')+'-';
                    sPrimaryContactLandline += sValue;
                    if (iCounter == 3) {

                        if(sPrimaryContactLandline.match(/^\d+-\d+--$/))
                        {
                            sPrimaryContactLandline = sPrimaryContactLandline.slice(0,-2);
                        }
                        else
                        {
                            sPrimaryContactLandline = sPrimaryContactLandline.slice(0,-1);
                        }
                        sPrimaryContactLandline += ",";
                        iCounter = 0;
                    }
                });
            }
            if(typeof(sPrimaryContactLandline) !== 'undefined')
            {
                var arsPrimaryContactLandline = sPrimaryContactLandline.split(",");
                aContact = aContact.concat(arsPrimaryContactLandline);
            }

            //alternate mobile
            var uiAlternateContactMobile = $.find('section.content #add_customer_content div.new_contact_number input.small[name="contact_number[]"]:visible');
            if(typeof(uiAlternateContactMobile) !== 'undefined')
            {
                $.each(uiAlternateContactMobile, function () {
                    var sValue = $(this).val().replace(/-/g, '')
                    aContact.push(sValue);
                });
            }

            //alternate landline
            var sAlternateContactLandLine = "";
            var uiAlternateContactLandLine = $.find('section.content #add_customer_content div.new_contact_number input.xsmall[name="contact_number[]"]:visible');
            if(typeof(uiAlternateContactLandLine) !== 'undefined')
            {
                var iCounter = 0;
                $.each(uiAlternateContactLandLine, function () {
                    iCounter++;
                    var sValue = $(this).val().replace(/-/g, '')+'-';
                    sAlternateContactLandLine += sValue;
                    if (iCounter == 3) {

                        sAlternateContactLandLine = sAlternateContactLandLine.replace(/--/g, '-')
                        if(sAlternateContactLandLine.lastIndexOf("-") != -1)
                        {
                            sAlternateContactLandLine = sAlternateContactLandLine.slice(0,-1);
                        }

                        sAlternateContactLandLine += ",";
                        iCounter = 0;
                    }
                });
            }
            if(typeof(sAlternateContactLandLine) !== 'undefined')
            {
                var arsAlternateContactLandLine = sAlternateContactLandLine.split(",");
                aContact = aContact.concat(arsAlternateContactLandLine);
            }

            aContact = jQuery.grep(aContact, function(n, i){
              return (n !== "" && n != null);
            });

            var uiAddCustomerForm = $('section.content #add_customer_content').find('form#add_customer');
            
            //get store id
            var iAddressStoreID = cr8v.get_from_storage('add_customer_rta_store_id', '');
            var iAddressStoreTime = cr8v.get_from_storage('add_customer_rta_store_id', '');
            if(typeof(iAddressStoreID) != "undefined" && typeof(iAddressStoreTime) != "undefined")
            {
                    iAddressStoreID = iAddressStoreID[0]; 
                    iAddressStoreTime = iAddressStoreTime[0];           
            }
            else
            {
                    iAddressStoreID = 0;
                    iAddressStoreTime = 0;
            }    
                
            var oCustomerData = {
                'first_name'            : uiAddCustomerForm.find('input[name="first_name"]').val(),
                'last_name'             : uiAddCustomerForm.find('input[name="last_name"]').val(),
                'middle_name'           : uiAddCustomerForm.find('input[name="middle_name"]').val(),
                'birthdate'             : uiAddCustomerForm.find('input[name="birthdate"]').val(),
                'email_address'         : uiAddCustomerForm.find('input[name="email_address[]"]').val(),
                'is_senior'             : uiAddCustomerForm.find('input[name="senior"]:checked').val(),
                'is_pwd'                : uiAddCustomerForm.find('input[name="pwd"]:checked').val(),
                'is_loyalty_card_holder': uiAddCustomerForm.find('input[name="loyalty"]:checked').val(),
                'osca_number'           : uiAddCustomerForm.find('input[name="osca_number"]').val(),
                'contact_number'        : aContact,
                'address'               : {
                    'house_number' : uiAddCustomerForm.find('input[name="house_number"]').val(),
                    'street'       : uiAddCustomerForm.find('input[name="street"]').attr("value"),
                    'second_street': uiAddCustomerForm.find('input[name="second_street"]').attr("value"),
                    'barangay'     : uiAddCustomerForm.find('input[name="barangay"]').attr("value"),
                    'subdivision'  : uiAddCustomerForm.find('input[name="subdivision"]').attr("value"),
                    'city'         : uiAddCustomerForm.find('input[name="city"]').attr("value"),
                    'province'     : uiAddCustomerForm.find('input[name="province"]').attr("value"),
                    'building'     : uiAddCustomerForm.find('input[name="building"]').attr("value"),
                    'floor'        : uiAddCustomerForm.find('input[name="floor"]').val(),
                    'address_type' : uiAddCustomerForm.find('input[name="address_type"]').val(),
                    'address_label': uiAddCustomerForm.find('input[name="address_label"]').val(),
                    'landmark'     : uiAddCustomerForm.find('input[name="landmark"]').val(),
                    'store_id'     : iAddressStoreID,
                    'delivery_time': iAddressStoreTime
                }


            }

            var oHappyPlusCards = [];
            var uiHappyPlusCards = $('div.happy_plus_card:visible');
            if(typeof(uiHappyPlusCards) !== 'undefined')
            {
                $.each(uiHappyPlusCards, function () {
                    var oHappyPlusCard = {
                        'number'              : $(this).find('input[name="number"]:first').val(),
                        'expiration_date_date': $(this).find('input[name="expiry_date"]:first').val(),
                        'is_smudged'          : $(this).find('input[name="is_smudged"]:checked').val(),
                        'remarks'             : $(this).find('input[name="remarks"]:first').val()
                    }
                    oHappyPlusCards.push(oHappyPlusCard);
                })
            }
            oCustomerData.happy_plus_cards = oHappyPlusCards;
            //console.log(oCustomerData)
            callcenter.call_orders.add(oCustomerData);
        },

        /*assemble_customer_information function end*/

        'extra_enable': function (uiContainerClass, bEnable) {
            if(typeof(uiContainerClass) !== 'undefined' && typeof(bEnable) !== 'undefined')
            {
                var oExtras = $('section.first-child-section').find('div.' + uiContainerClass + '');
                $.each(oExtras, function () {
                    var oInputs = $(this).find('input,select');
                    //console.log(oInputs);
                    $.each(oInputs, function () {
                        $(this).val('');
                        $(this).prop('disabled', bEnable);
                    })
                })
            }

        },

        'clone_append': function (uiTemplate, uiContainer) {
            if (typeof(uiTemplate) !== 'undefined' && typeof(uiContainer) !== 'undefined') {
                uiContainer.append(uiTemplate);
            }
        },

        'remove_append': function (uiElem) {
            if (typeof(uiElem) !== 'undefined') {
                uiElem.remove();
            }
        },

        'insert_happy_plus_map': function (customer_id, happy_plus_id) {
            if(typeof(happy_plus_id) !== '')
            {
                var oMapConfig = {
                    "data" : []
                }

                oMapConfig.data.push({
                    "customer_id": customer_id,
                    "happy_plus_id": happy_plus_id
                });

                var oHappyPlusMapConfig = {
                    "type"   : "POST",
                    "data"   : oMapConfig,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.happy_plus') + "happy_plus/map",
                    "success": function (oHappyMapData) {

                    }
                }
                callcenter.customer_list.get(oHappyPlusMapConfig);
            }
        },

        'add_error_message': function (sMessages, uiContainer) {
            $('p.error_messages').remove();
            if (sMessages.length > 0) {
                if (typeof(uiContainer) !== 'undefined') {
                    uiContainer.empty();
                    uiContainer.show();
                    uiContainer.append(sMessages);
                    uiContainer.find("p.error_messages:contains('is required')").hide();

                    if (uiContainer.find('p.error_messages').is(":contains('is required')")) {
                        uiContainer.append("<p class='error_messages'><i class='fa fa-exclamation-triangle'></i> Please fill up all required fields.</p>");
                    }

                    /*add new customer form*/
                    if($("section.content #add_customer_content").find('div#city_select div.select input.dd-txt[name="city"]').hasClass('error'))
                    {   
                        $("section.content #add_customer_content").find('div#city_select div.select input.dd-txt[name="city"]').parents('div.select').addClass('has-error');
                    }
                    else
                    {   
                        $("section.content #add_customer_content").find('div#city_select div.select input.dd-txt[name="city"]').parents('div.select').removeClass('has-error');
                    }
                    
                    if($("section.content #add_customer_content").find('div#province_select div.select input.dd-txt[name="province"]').hasClass('error'))
                    {   
                        $("section.content #add_customer_content").find('div#province_select div.select input.dd-txt[name="province"]').parents('div.select').addClass('has-error');
                    }
                    else
                    {   
                        $("section.content #add_customer_content").find('div#province_select div.select input.dd-txt[name="province"]').parents('div.select').removeClass('has-error');
                    }
                    
                    // if($("section.content #add_customer_content").find('div.select input.dd-txt[name="address_type"]').hasClass('error'))
                    // {   
                    //     $("section.content #add_customer_content").find('div.select input.dd-txt[name="address_type"]').parents('div.select').addClass('has-error');
                    // }
                    // else
                    // {   
                    //     $("section.content #add_customer_content").find('div#city_select div.select input.dd-txt[name="address_type"]').parents('div.select').removeClass('has-error');
                    // }

                    /*add address form*/
                    if($("form#add_address").find('div.select input.dd-txt[name="province"]').hasClass('error'))
                    {   
                        $("form#add_address").find('div.select input.dd-txt[name="province"]').parents('div.select').addClass('has-error');
                    }
                    else
                    {   
                        $("form#add_address").find('div.select input.dd-txt[name="province"]').parents('div.select').removeClass('has-error');
                    }

                    if($("form#add_address").find('div.select input.dd-txt[name="city"]').hasClass('error'))
                    {   
                        $("form#add_address").find('div.select input.dd-txt[name="city"]').parents('div.select').addClass('has-error');
                    }
                    else
                    {   
                        $("form#add_address").find('div.select input.dd-txt[name="city"]').parents('div.select').removeClass('has-error');
                    }

                    // if($("form#add_address").find('div.select input.dd-txt[name="address_type"]').hasClass('error'))
                    // {   
                    //     $("form#add_address").find('div.select input.dd-txt[name="address_type"]').parents('div.select').addClass('has-error');
                    // }
                    // else
                    // {   
                    //     $("form#add_address").find('div.select input.dd-txt[name="address_type"]').parents('div.select').removeClass('has-error');
                    // }

                    /*coordinator update address form*/
                    if($("form#coordinator_update_address").find('div.select input.dd-txt[name="province"]').hasClass('error'))
                    {   
                        $("form#coordinator_update_address").find('div.select input.dd-txt[name="province"]').parents('div.select').addClass('has-error');
                    }
                    else
                    {   
                        $("form#coordinator_update_address").find('div.select input.dd-txt[name="province"]').parents('div.select').removeClass('has-error');
                    }

                    if($("form#coordinator_update_address").find('div.select input.dd-txt[name="city"]').hasClass('error'))
                    {   
                        $("form#coordinator_update_address").find('div.select input.dd-txt[name="city"]').parents('div.select').addClass('has-error');
                    }
                    else
                    {   
                        $("form#coordinator_update_address").find('div.select input.dd-txt[name="city"]').parents('div.select').removeClass('has-error');
                    }

                    // if($("form#coordinator_update_address").find('div.select input.dd-txt[name="address_type"]').hasClass('error'))
                    // {   
                    //     $("form#coordinator_update_address").find('div.select input.dd-txt[name="address_type"]').parents('div.select').addClass('has-error');
                    // }
                    // else
                    // {   
                    //     $("form#coordinator_update_address").find('div.select input.dd-txt[name="address_type"]').parents('div.select').removeClass('has-error');
                    // }
                }

            }
        },

        'building_autocomplete': function (oData) {

            var oParams = oData;
            //Get Building
             var oAddressAjaxConfig = {
                "type"   : "Get",
                "data"   : oParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : callcenter.config('url.api.jfc.gis') + "/gis/other_address",
                "success": function (data) {
                    if(typeof(data) !== 'undefined')
                    {
                        for (var i in data.data.buildings) {
                            var item = data.data.buildings[i];
                            oAutoComplete.oPOI.push({"name": item.name, "id": item.id, "city_id": item.city_id});
                        }

                        if (count(data.data.buildings) > 0) {
                            var iOffset = data.offset;
                            var oParams = {
                                'offset'             : iOffset,
                                "limit"              : 20000,
                                'location_type_id'   : 1,
                                'call_center_user_id': 3305
                            };
                            callcenter.call_orders.building_autocomplete(oParams);
                            loadingLocalData +=0;
                            loadingLocalDataSeconds -=2;
                            $("div.loader div.prog-bar").css("width",loadingLocalData+"%");
                            $("section.splash-container h3 span.loaded").text(loadingLocalData);
                            $("section.splash-container h4 span.seconds").text(loadingLocalDataSeconds);
                        } else {
                            //Get Street

                            var oParams = {
                                'offset'             : 0,
                                "limit"              : 20000,
                                'location_type_id'   : 2,
                                'call_center_user_id': 3305
                            };
                            callcenter.call_orders.street_autocomplete(oParams);
                            loadingLocalData +=10;
                            loadingLocalDataSeconds -=2;
                            $("section.splash-container div.loader div.prog-bar").css("width",loadingLocalData+"%");
                            $("section.splash-container h3 span.loaded").text(loadingLocalData);
                            $("section.splash-container h4 span.seconds").text(loadingLocalDataSeconds);
                        }
                    }

                }
            }
            callcenter.call_orders.get(oAddressAjaxConfig);
            
        },

        'street_autocomplete'     : function (oData) {

            var oParams = oData;
            //Get Street
            var oAddressAjaxConfig = {
                "type"   : "Get",
                "data"   : oParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : callcenter.config('url.api.jfc.gis') + "/gis/other_address",
                "success": function (data) {
                    if(typeof(data) !== 'undefined')
                    {                    
                        for (var i in data.data.streets) {
                            var item = data.data.streets[i];
                            oAutoComplete.oStreet.push({"name": item.name, "id": item.id, "city_id": item.city_id});
                        }

                        /* for (var i in data.data.subdivisions) {
                         var item = data.data.subdivisions[i];
                         oAutoComplete.oSubdivision.push({"name": item.name, "id": item.id, "city_id": item.city_id});
                         }

                         for (var i in data.data.barangays) {
                         var item = data.data.barangays[i];
                         oAutoComplete.oBarangay.push({"name": item.name, "id": item.id, "city_id": item.city_id});
                         } */

                        if (count(data.data.streets) > 0) {
                            var iOffset = data.offset;

                            var oParams = {
                                'offset'             : iOffset,
                                "limit"              : 20000,
                                'location_type_id'   : 2,
                                'call_center_user_id': 3305
                            };
                            callcenter.call_orders.street_autocomplete(oParams);
                            loadingLocalData +=10;
                            loadingLocalDataSeconds -=2;
                            $("section.splash-container div.loader div.prog-bar").css("width",loadingLocalData+"%");
                            $("section.splash-container h3 span.loaded").text(loadingLocalData);
                            $("section.splash-container h4 span.seconds").text(loadingLocalDataSeconds);
                        } else {
                            //Get Subdivision
                            var oParams = {
                                'offset'             : iOffset,
                                "limit"              : 20000,
                                'location_type_id'   : 3,
                                'call_center_user_id': 3305
                            };
                            callcenter.call_orders.subdivision_autocomplete(oParams);
                            loadingLocalData +=10;
                            loadingLocalDataSeconds -=2;
                            $("section.splash-container div.loader div.prog-bar").css("width",loadingLocalData+"%");
                            $("section.splash-container h3 span.loaded").text(loadingLocalData);
                            $("section.splash-container h4 span.seconds").text(loadingLocalDataSeconds);
                        }
                    }
                }
            }
            callcenter.call_orders.get(oAddressAjaxConfig);

        },
        'subdivision_autocomplete': function (oData) {

            var oParams = oData;
            //Get Street
            var oAddressAjaxConfig = {
                "type"   : "Get",
                "data"   : oParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : callcenter.config('url.api.jfc.gis') + "/gis/other_address",
                "success": function (data) {

                    if(typeof(data) !== 'undefined')
                    {
                        for (var i in data.data.subdivisions) {
                            var item = data.data.subdivisions[i];
                            oAutoComplete.oSubdivision.push({"name": item.name, "id": item.id, "city_id": item.city_id});
                        }
                        /*
                         for (var i in data.data.barangays) {
                         var item = data.data.barangays[i];
                         oAutoComplete.oBarangay.push({"name": item.name, "id": item.id, "city_id": item.city_id});
                         } */

                        if (count(data.data.streets) > 0) {
                            var iOffset = data.offset;
                            var oParams = {
                                'offset'             : iOffset,
                                "limit"              : 20000,
                                'location_type_id'   : 3,
                                'call_center_user_id': 3305
                            };
                            callcenter.call_orders.subdivision_autocomplete(oParams);
                            loadingLocalData +=10;
                            loadingLocalDataSeconds -=2;
                            $("section.splash-container div.loader div.prog-bar").css("width",loadingLocalData+"%");
                            $("section.splash-container h3 span.loaded").text(loadingLocalData);
                            $("section.splash-container h4 span.seconds").text(loadingLocalDataSeconds);
                        } else {
                            //Get Barangay
                            var oParams = {
                                'offset'             : iOffset,
                                "limit"              : 100000,
                                'location_type_id'   : 4,
                                'call_center_user_id': 3305
                            };
                            callcenter.call_orders.barangay_autocomplete(oParams);
                            loadingLocalData +=10;
                            loadingLocalDataSeconds -=2;
                            $("section.splash-container div.loader div.prog-bar").css("width",loadingLocalData+"%");
                            $("section.splash-container h3 span.loaded").text(loadingLocalData);
                            $("section.splash-container h4 span.seconds").text(loadingLocalDataSeconds);
                        }
                    }
                }
            }
            callcenter.call_orders.get(oAddressAjaxConfig);

        },

        'barangay_autocomplete': function (oData) {

            var oParams = oData;
            //Get Street
            var oAddressAjaxConfig = {
                "type"   : "Get",
                "data"   : oParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : callcenter.config('url.api.jfc.gis') + "/gis/other_address",
                "success": function (data) {
                    if(typeof(data) !== 'undefined')
                    {
                        for (var i in data.data.barangays) {
                            var item = data.data.barangays[i];
                            oAutoComplete.oBarangay.push({"name": item.name, "id": item.id, "city_id": item.city_id});
                        }

                        if (count(data.data.streets) > 0) {
                            var iOffset = data.offset;
                            var oParams = {
                                'offset'             : iOffset,
                                "limit"              : 20000,
                                'location_type_id'   : 4,
                                'call_center_user_id': 3305
                            };
                            callcenter.call_orders.barangay_autocomplete(oParams);
                            loadingLocalData +=10;
                            loadingLocalDataSeconds -=2;
                            $("section.splash-container div.loader div.prog-bar").css("width",loadingLocalData+"%");
                            $("section.splash-container h3 span.loaded").text(loadingLocalData);
                            $("section.splash-container h4 span.seconds").text(loadingLocalDataSeconds);
                        }
                        if(loadingLocalData>90){
                            $("section.splash-container").addClass("hidden");
                            $("header").removeClass("hidden");
                            $("section.first-child-section").removeClass("hidden");
                        }
                    }
                }
            }
            callcenter.call_orders.get(oAddressAjaxConfig);

        },

        'get_province_identifier': function () {
            var oAddressAjaxConfig = {
                "type"   : "Get",
                "data"   : [],
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : callcenter.config('url.api.jfc.gis') + "/gis/province",
                "success": function (data) {
                    if(typeof(data) !== 'undefined')
                    {
                        $.each(data.data, function (k , v) {
                            if(v.id == iProvinceId)
                            {
                                $('body').find("input.xsmall.contact-0").val(v.identifier);
                            }
                        })
                    }
                }
            }
            callcenter.call_orders.get(oAddressAjaxConfig);

        },
        'initialize'           : function () {
            //
            $('section #add_new_customer_header input#search_customer').on("keyup",function (event) {
                var sSearchVal = $(this).val();
                var numericExpression = /^[0-9]+$/;
                if(sSearchVal !=""){
	            	if(sSearchVal.match(numericExpression))//input is numeric
                    {
                       $('section #add_new_customer_header input[name="search_by"]').val("Contact Number");
	            	}else{
	            	   $('section #add_new_customer_header input[name="search_by"]').val("Customer Name");
	            	}
                }
                
            });

            //prevent from alpha input
            $(".input_numeric").on("keyup",function (event) {
                $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            });

            //prevent from alpha input, for newly appended elements
            $("body").on("keyup", ".input_numeric", function (event) {
                $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            });

            //alpha spaces input only
            $(".input_alpha").on("keyup",function (event) {
                $(this).val($(this).val().replace(/[^a-zA-Z ]/g,''));
            });

            //alpha spaces input only, for newly appended elements
            $("body").on("keyup", ".input_alpha", function (event) {
                $(this).val($(this).val().replace(/[^a-zA-Z ]/g,''));
            });

            //alpha numeric input only
            $(".input_alpha_numeric").on("keyup",function (event) {
                $(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''));
            });

            //alpha numeric input only, for newly appended elements
            $("body").on("keyup", ".input_alpha_numeric", function (event) {
                $(this).val($(this).val().replace(/[^a-zA-Z0-9]/g,''));
            });

            //alpha numeric spaces input only
            $(".input_alpha_num_space").on("keyup",function (event) {
                $(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''));
            });

            //alpha numeric spaces input only, for newly appended elements
            $("body").on("keyup", ".input_alpha_num_space", function (event) {
                $(this).val($(this).val().replace(/[^a-zA-Z0-9 ]/g,''));
            });

            //alpha numeric spaces input only
            $(".input_num_space_dash").on("keyup",function (event) {
                $(this).val($(this).val().replace(/[^0-9\- ]/g,''));
            });

            //alpha numeric spaces input only, for newly appended elements
            $("body").on("keyup", ".input_num_space_dash", function (event) {
                $(this).val($(this).val().replace(/[^0-9\- ]/g,''));
            });

            $("section.content div#add_customer_content form#add_customer").on("keyup", "input[name='first_name'], input[name='middle_name'], input[name='last_name']", function (event) {
                var uiThis = $(this)
                uiThis.css('text-transform', 'capitalize')
            });

            $('section#add_new_customer_header button.add_new_customer').on('click', function (e) {
                $('p.count_search_result strong').text('');
            })


            $('section.content div#add_customer_content form#add_customer button.cancel_add_customer').on('click', function (e) {
                $('section.content div#add_customer_content form#add_customer').find('div.add-customer-error-msg').hide();
                $('section.content div#add_customer_content form#add_customer').find('div.success-msg').hide();
                $('section.content div#add_customer_content form#add_customer').find('div.found-retail').hide();
                $('section.content div#add_customer_content form#add_customer').find('button.find_retail').attr("disabled",true);
                $('section.content div#add_customer_content form#add_customer').find('button.rta_proceed').attr("disabled",true);
                callcenter.call_orders.reset_all_form_inputs($('section.content div#add_customer_content form#add_customer'));
                //hide alternate contact in add new customer
                $('section.content #add_customer_content div.primary_contact_number').find('a.alternate-contact-number').removeClass('hidden');
                $('section.content #add_customer_content div.alternate_contact_container').addClass("hidden");
                //set mobile container as default
                $('section.content #add_customer_content div.primary_contact_number').find("div.frm-custom-dropdown input[name='contact_number_type[]']").attr('value', 'Mobile');
                $('section.content #add_customer_content div.primary_contact_number').find("div.frm-custom-dropdown input[name='contact_number_type[]']").val('Mobile');
                $('section.content #add_customer_content div.primary_contact_number').find("input.small").removeClass("hidden");
                $('section.content #add_customer_content div.primary_contact_number').find("input.small").attr("datavalid", "required");
                $('section.content #add_customer_content div.primary_contact_number').find("input.xsmall").addClass("hidden");
                $('section.content #add_customer_content div.primary_contact_number').find("input.xsmall").removeAttr("datavalid");
                //set mobile container as default
                $('section.content #add_customer_content div.alternate_contact_container').find("div.frm-custom-dropdown input[name='contact_number_type[]']").attr('value', 'Mobile');
                $('section.content #add_customer_content div.alternate_contact_container').find("div.frm-custom-dropdown input[name='contact_number_type[]']").val('Mobile');
                $('section.content #add_customer_content div.alternate_contact_container').find("input.small").removeClass("hidden");
                $('section.content #add_customer_content div.alternate_contact_container').find("input.small").attr("datavalid", "required");
                $('section.content #add_customer_content div.alternate_contact_container').find("input.xsmall").addClass("hidden");
                $('section.content #add_customer_content div.alternate_contact_container').find("input.xsmall").removeAttr("datavalid");
                callcenter.call_orders.get_province_identifier();

                $('section.content #add_customer_content div.pwd_container').find('input#pwd2').prop('checked', true);
                $('section.content #add_customer_content div.senior_container').find('input#senior2').prop('checked', true);
                $('section.content #add_customer_content div.parent_happy_plus').find('input#happy2').prop('checked', true);
                $('section.content #add_customer_content div.loyalty_container').find('input#loyalty2').prop('checked', true);

                var isCustomerSaved = callcenter.get_from_storage("isCustomerSaved", '');
                if(typeof(isCustomerSaved) != 'undefined')
                {
                    callcenter.pop("isCustomerSaved");
                }
                var isHappyPlusAdded = callcenter.get_from_storage("happy_plus_cards", '');
                if(typeof(isHappyPlusAdded) != 'undefined')
                {
                    callcenter.pop("happy_plus_cards");
                }
            });

            //clear on add new customer
            $('body').on('click','button.add_new_customer', function (e) {
                $('section.content div#add_customer_content form#add_customer').find('div.add-customer-error-msg').hide();
                $('section.content div#add_customer_content form#add_customer').find('div.success-msg').hide();
                $('section.content div#add_customer_content form#add_customer').find('div.found-retail').hide();
                $('section.content div#add_customer_content form#add_customer').find('button.rta_proceed').attr("disabled",true);
                $('section.content div#add_customer_content form#add_customer').find('button.find_retail').attr("disabled",true);
                $('section.content div#add_customer_content form#add_customer').find('button.save_customer').attr("disabled",false);
                callcenter.call_orders.show_spinner($('button.save_customer'), false);
                callcenter.call_orders.reset_all_form_inputs($('section.content div#add_customer_content form#add_customer'));
                //hide alternate contact in add new customer
                $('section.content #add_customer_content div.primary_contact_number').find('a.alternate-contact-number').removeClass('hidden');
                $('section.content #add_customer_content div.alternate_contact_container').addClass("hidden");
                //set mobile container as default
                $('section.content #add_customer_content div.primary_contact_number').find("div.frm-custom-dropdown input[name='contact_number_type[]']").attr('value', 'Mobile');
                $('section.content #add_customer_content div.primary_contact_number').find("div.frm-custom-dropdown input[name='contact_number_type[]']").val('Mobile');
                $('section.content #add_customer_content div.primary_contact_number').find("input.small").removeClass("hidden");
                $('section.content #add_customer_content div.primary_contact_number').find("input.small").attr("datavalid", "required");
                $('section.content #add_customer_content div.primary_contact_number').find("input.xsmall").addClass("hidden");
                $('section.content #add_customer_content div.primary_contact_number').find("input.xsmall").removeAttr("datavalid");
                //set mobile container as default
                $('section.content #add_customer_content div.alternate_contact_container').find("div.frm-custom-dropdown input[name='contact_number_type[]']").attr('value', 'Mobile');
                $('section.content #add_customer_content div.alternate_contact_container').find("div.frm-custom-dropdown input[name='contact_number_type[]']").val('Mobile');
                $('section.content #add_customer_content div.alternate_contact_container').find("input.small").removeClass("hidden");
                $('section.content #add_customer_content div.alternate_contact_container').find("input.small").attr("datavalid", "required");
                $('section.content #add_customer_content div.alternate_contact_container').find("input.xsmall").addClass("hidden");
                $('section.content #add_customer_content div.alternate_contact_container').find("input.xsmall").removeAttr("datavalid");
                callcenter.call_orders.get_province_identifier();

                $('section.content #add_customer_content div.pwd_container').find('input#pwd2').prop('checked', true).trigger('click');
                $('section.content #add_customer_content div.senior_container').find('input#senior2').prop('checked', true).trigger('click');
                $('section.content #add_customer_content div.parent_happy_plus').find('input#happy2').prop('checked', true).trigger('click');
                $('section.content #add_customer_content div.loyalty_container').find('input#loyalty2').prop('checked', true).trigger('click');
                
                $('section.content #add_customer_content').find('input[name="contact_number_type[]"]').attr('readonly', '')

                var isCustomerSaved = callcenter.get_from_storage("isCustomerSaved", '');
                if(typeof(isCustomerSaved) != 'undefined')
                {
                    callcenter.pop("isCustomerSaved");
                }
                var isHappyPlusAdded = callcenter.get_from_storage("happy_plus_cards", '');
                if(typeof(isHappyPlusAdded) != 'undefined')
                {
                    callcenter.pop("happy_plus_cards");
                }
                var date = new Date();
                date.setDate(date.getDate());
                $('section.content #add_customer_content').find("#birthdate-date-picker").datetimepicker({pickTime: false, maxDate: date });
            });

            // $("#search_customer").on('keyup', function(){
            //     if($('input[name="search_by"]:visible').val() == 'Contact Number')
            //     {
            //         $(this).removeClass('input_alpha_num_space').addClass('input_num_space_dash');
            //     }
            //     else if($('input[name="search_by"]:visible').val() == 'Customer Name')
            //     {
            //         $(this).removeClass('input_num_space_dash')/*.addClass('input_alpha_num_space');*/
            //     }
            // });

             $("section.content div#order_process_content div.meal-record-container").on('dblclick','table.cart-table-table td[data-product-quantity]', function () {
                newInput(this);
            })

            function newInput(elm) {
                $(elm).unbind('dblclick');

                var value = $(elm).text();
                $(elm).empty();
                
                $("<input>")
                    .attr('type', 'text')
                    .val(value)
                    .css({"width":"50px"})
                    .addClass('input_numeric text-right')
                    .blur(function() {
                         var value = $(elm).find('input').val();
                         if(value==0){
                            value = 1;
                         }
                            $(elm).empty().text(value);
                            $(elm).bind("dblclick", function () {
                                newInput(elm);
                            });
                         //for the computation
                         var uiTr = $(elm).parents('tr:first');
                         $(elm).attr('data-product-quantity', value)
                         uiTr.attr('data-product-quantity', value);       
                         var iPrice = uiTr.find('td[data-product-price]').attr('data-product-price');
                         var newPrice = parseFloat(iPrice) * value;
                         uiTr.find('td[data-product-row-total]').attr('data-product-row-total', newPrice).text(newPrice.toFixed(2));  
                         var uiCartTable =   uiTr.parents('table.cart-table-table');
                         callcenter.order_process.compute_bill_breakdown(uiCartTable);  
                    })
                    .prependTo($(elm).text(value))
                    .on("keyup",function (event) {
                        var sValue = $(this).val();
                        var sNewValue = sValue.replace(/[^0-9]/g,'');
                        $(this).val(sNewValue);
                        if(event.keyCode==13){
                            $(this).trigger("blur");
                        }
                    })
                    .focus();
            }


            $('header[custom-style="header"] div.profile-container button.btn-logout').on('click', function () {
                window.location.href = callcenter.config('url.server.base') + "auth/login/logout";
            });

            //$('div.error-msg').hide();
            $('div.success-msg').hide();
            $('div.no-search-result').hide();
            $('section.content #add_customer_content div.senior_citizen').hide();
            $('section.content #add_customer_content div.happy_plus_card').hide();


            $('section.content #add_customer_content a.another-contact-number').on('click', function (e) {
                e.preventDefault();
                var uiTemplate = $('div.template.new_contact_number.add_customer').clone().removeClass('template');
                var uiContainer = $(this).parents('div.alternate_contact_container:first');
                callcenter.call_orders.clone_append(uiTemplate, uiContainer);
                callcenter.call_orders.get_province_identifier();
            })

            $('section.content #add_customer_content a.another_email_address').on('click', function (e) {
                e.preventDefault();
                var uiTemplate = $('div.template.new_email_address.add_customer').clone().removeClass('template');
                var uiContainer = $(this).parents('div.email_address_container:first');
                callcenter.call_orders.clone_append(uiTemplate, uiContainer);
            });

            $('section.content #add_customer_content').on('click', 'a.another_happy_plus', function (e) {
                e.preventDefault();

                if ($('section.content #add_customer_content').find('div.happy_plus_card:visible').length == 1) {
                    $('section.content #add_customer_content div.happy_plus_card').first().find('a.another_happy_plus').remove();
                    $('section.content #add_customer_content div.happy_plus_card').first().find("div.clear").remove();
                    $('section.content #add_customer_content div.happy_plus_card:not(.template)').last().show();
                    $('section.content #add_customer_content div.happy_plus_card:not(.template)').last().find("input[type='text'][name='number']").attr({
                        'labelinput': 'Card Number',
                        'datavalid' : 'required'
                    });
                    $('section.content #add_customer_content div.happy_plus_card:not(.template)').last().find("input[type='text'][name='expiry_date']").attr({
                        'labelinput': 'Expiry Date',
                        'datavalid' : 'required'
                    });
                }

                var uiTemplate = $('section.content #add_customer_content div.template.new_happy_plus_card').clone().removeClass('template').show();
                var happy_plus_quantity = $('section.content #add_customer_content').find('div.happy_plus_card:visible').length + 1;
                uiTemplate.find('input[name="is_smudged"]').attr('id', 'card-number-' + happy_plus_quantity);
                uiTemplate.find('label.chck-lbl').attr('for', 'card-number-' + happy_plus_quantity);
                uiTemplate.find("input[type='text'][name='number']").attr({
                    'labelinput': 'Card Number',
                    'datavalid' : 'required'
                });
                uiTemplate.find("input[type='text'][name='expiry_date']").attr({
                    'labelinput': 'Expiry Date',
                    'datavalid' : 'required'
                });
                var uiContainer = $(this).parents('div.parent_happy_plus:first');

                callcenter.call_orders.clone_append(uiTemplate, uiContainer);

                $(".date-picker").datetimepicker({pickTime: false});

                if ($('section.content #add_customer_content').find('div.happy_plus_card:visible').length > 2) {
                    $('section.content #add_customer_content div.happy_plus_card:not(.template)').last().find('hr').removeClass('margin-top-15');
                }
            });

            $('section.content #add_customer_content #add_customer').on('submit', function (e) {

                $('div.error-msg').hide();
                $('div.success-msg').hide();
                $('p.error_messages').remove();
                e.preventDefault();

            });

            $('form#add_address').on('submit', function (e) {
                e.preventDefault();
            });

            var oValidationConfig = {
                'data_validation'    : 'datavalid',
                'input_label'        : 'labelinput',
                'min_length'         : 0,
                'max_length'         : 75,
                'class'              : 'error',
                'add_remove_class'   : true,
                'Contact Number': 
                {
                    'min_length': 13
                },
                'Landline Number': 
                {
                    'min_length': 9
                },
                'City': 
                {
                    'max_length': 0
                },
                'Province': 
                {
                    'max_length': 0
                },
                'Address Type':
                {
                    'max_length': 0
                },
                'Address Label':
                {
                    'max_length': 0
                },
                'Landmark':
                {
                    'max_length': 0
                },
                'House Number':
                {
                    'max_length': 0
                },
                'Building':
                {
                    'max_length': 0
                },
                'Street':
                {
                    'max_length': 0
                },
                'Second Street':
                {
                    'max_length': 0
                },
                'Subdivision':
                {
                    'max_length': 0
                },
                'Barangay':
                {
                    'max_length': 0
                },
                'onValidationError'  : function (arrMessages) {
                    var sError = '';
                    var uiContainer = $('div.add-customer-error-msg');
                    if(typeof(arrMessages) !== 'undefined')
                    {
                        $.each(arrMessages, function (k, v) {
                            sError += '<p class="error_messages"><i class="fa fa-exclamation-triangle"></i> ' + v.error_message + '</p>';
                        });
                    }

                    callcenter.call_orders.add_error_message(sError, uiContainer);
                    callcenter.call_orders.show_spinner($('button.save_customer'), false);
                    $("html, body").animate({scrollTop: 0}, "slow");
                    ////console.log(arrMessages); //shows the errors
                },
                'onValidationSuccess': function () {
                    callcenter.call_orders.assemble_customer_information();
                }
            }

            callcenter.validate_form($('#add_customer'), oValidationConfig);

            var oAddAddressModalValidationConfig = {
                'data_validation'    : 'datavalid',
                'input_label'        : 'labelinput',
                'min_length'         : 0,
                // 'max_length'         : 20,
                'class'              : 'error',
                'add_remove_class'   : true,
                'onValidationError'  : function (arrMessages) {
                    var sError = '';
                    var uiContainer = $('div.address-error-msg');
                    //console.log(arrMessages)
                    if(typeof(arrMessages) !== 'undefined')
                    {
                        $.each(arrMessages, function (k, v) {
                            sError += '<p class="error_messages"><i class="fa fa-exclamation-triangle"></i> ' + v.error_message + '</p>';
                        });
                    }

                    callcenter.call_orders.add_error_message(sError, uiContainer);
                    callcenter.call_orders.show_spinner($('button.add_address_button'), false);
                    callcenter.call_orders.show_spinner($('button.update_address_button'), false);



                },
                'onValidationSuccess': function () {
                    callcenter.call_orders.show_spinner($('button.add_address_button'), true);
                    callcenter.call_orders.show_spinner($('button.update_address_button'), true);

                    if($('button.update_address_button').is(":visible"))
                    {
                        // console.log("update")
                        callcenter.call_orders.assemble_update_address_information();
                        $('div.find-rta-container ').show();
                        $('div.found-retail').hide();
                    }
                    if($('button.add_address_button').is(":visible"))
                    {
                        // console.log("add")
                        callcenter.call_orders.assemble_address_information();
                        $('div.find-rta-container ').show();
                        $('div.found-retail').hide();
                    }

                }
            }

            $('section.content #add_customer_content').on('click', '.alternate-contact-number', function (e) {
                $(this).addClass("hidden");
                $('section.content #add_customer_content .alternate_contact_container').removeClass("hidden");
                $('section.content #add_customer_content .alternate_contact_container').find('input[name="contact_number[]"].mobile').attr({'datavalid': 'required', 'labelinput' : 'Contact Number'});
                $('section.content #add_customer_content .alternate_contact_container').find('input[name="contact_number[]"].landline.contact-0').attr({'datavalid': 'required', 'labelinput' : 'Landline Area Code'});
                $('section.content #add_customer_content .alternate_contact_container').find('input[name="contact_number[]"].landline.contact-1').attr({'datavalid': 'required', 'labelinput' : 'Landline Number'});
                // $('section.content #add_customer_content .alternate_contact_container').find('input[name="contact_number[]"].landline.contact-2').attr({'datavalid': /*numeric*/' required', 'labelinput' : 'Landline Number Trunkline'});
                $('section.content #add_customer_content .alternate_contact_container').find("input.small").removeClass("hidden");
            });

            $('section.content #add_customer_content').on('click', 'div.option', function (e) {//selection_of_mobile_and_landline
                $(this).closest("section.content #add_customer_content div.frm-custom-dropdown").find("input.dd-txt").attr("value", $(this).html());
                if ($(this).closest("section.content #add_customer_content div.frm-custom-dropdown").find("input.dd-txt").attr("value") == "Mobile") {//mobile selected
                    $(this).closest("section.content #add_customer_content div.contact-number-container").find("input.small").removeClass("hidden");
                    $(this).closest("section.content #add_customer_content div.contact-number-container").find("input.small").attr("datavalid", "required");
                    $(this).closest("section.content #add_customer_content div.contact-number-container").find("input.xsmall").addClass("hidden");
                    $(this).closest("section.content #add_customer_content div.contact-number-container").find("input.xsmall").removeAttr("datavalid");
                } else {//landline selected
                    $(this).closest("section.content #add_customer_content div.contact-number-container").find("input.small").addClass("hidden");
                    $(this).closest("section.content #add_customer_content div.contact-number-container").find("input.small").removeAttr("datavalid");
                    $(this).closest("section.content #add_customer_content div.contact-number-container").find("input.xsmall").removeClass("hidden");
                    $(this).closest("section.content #add_customer_content div.contact-number-container").find("input.xsmall:not('.contact-2')").attr("datavalid", /*"numeric*/"required");
                }
            });

            // $('section.content #add_customer_content div.alternate_contact_container').on('click', 'div.option', function (e) { //mobile and landline selection in alternate, fields not required
            //     $(this).closest("section.content #add_customer_content div.frm-custom-dropdown").find("input.dd-txt").attr("value", $(this).html());
            //     if ($(this).closest("section.content #add_customer_content div.frm-custom-dropdown").find("input.dd-txt").attr("value") == "Mobile")
            //     {//mobile selected
            //         $(this).closest("section.content #add_customer_content div.contact-number-container").find("input.small").removeClass("hidden");
            //         $(this).closest("section.content #add_customer_content div.contact-number-container").find("input.xsmall").addClass("hidden");
            //     }
            //     else
            //     {//landline selected
            //         $(this).closest("section.content #add_customer_content div.contact-number-container").find("input.small").addClass("hidden");
            //         $(this).closest("section.content #add_customer_content div.contact-number-container").find("input.xsmall").removeClass("hidden");
            //     }
            // });

            //api_get_address
            var oParams = {'call_center_user_id': 3305};
            //Get Province
            //var oAddressAjaxConfig = {
            //    "type"   : "Get",
            //    "data"   : oParams,
            //    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
            //    "url"    : callcenter.config('url.api.jfc.gis') + "/gis/province_address",
            //    "success": function (data) {
            //        ////console.log(data);
            //        if(typeof(data) !== 'undefined')
            //        {
            //            var uiSearchCustomerHeaderProvince = $("section #add_new_customer_header div#search_customer_select_province")
            //                .children("div.select")
            //                .children("div.frm-custom-dropdown")
            //                .children("div.frm-custom-dropdown-option");
            //
            //            var uiProvinceAC = $("section.content #add_customer_content #province_select")
            //                .children("div.select")
            //                .children("div.frm-custom-dropdown")
            //                .children("div.frm-custom-dropdown-option");
            //
            //            //callcenter deliver to different adderess modal
            //            var uiProvinceDTDmodal = $("div[modal-id='deliver-to-different'] form#add_address #modal_select_province")
            //                .children("div.select")
            //                .children("div.frm-custom-dropdown")
            //                .children("div.frm-custom-dropdown-option");
            //
            //            //coordinator update-address modal
            //            var uiProvinceCoordinatorDTDmodal = $("div[modal-id='update-address'] form#coordinator_update_address #coordinator_modal_select_province")
            //                .children("div.select")
            //                .children("div.frm-custom-dropdown")
            //                .children("div.frm-custom-dropdown-option");
            //
            //            /*coordinator management*/
            //            var uiProvinceCoordinatorManagement = $("section#search_order_form_container form#search_order_form_params").find('div.province_select')
            //                .children("div.select")
            //                .children("div.frm-custom-dropdown")
            //                .children("div.frm-custom-dropdown-option");
            //
            //            //coordinator process-order
            //            var uiProvinceCoordinatorPOmodal = $("div[modal-id='process-order'] form#coordinator_process_order_address #coordinator_po_modal_select_province")
            //                .children("div.select")
            //                .children("div.frm-custom-dropdown")
            //                .children("div.frm-custom-dropdown-option");
            //
            //            /*uiSearchCustomerHeaderProvince.html("");
            //            uiProvinceAC.html("");
            //            uiProvinceDTDmodal.html("");
            //            uiProvinceCoordinatorDTDmodal.html("");
            //            uiProvinceCoordinatorManagement.html("");*/
            //
            //            /*throw province list for store management*/
            //            callcenter.store_management.process_province_list(data.data.provinces);
            //            callcenter.add_to_storage("autocomplete_gis_province",oAutoComplete.oProvinces);
    			//
            //            var sFetchRouteStores = "(";
            //
            //            for (var i in data.data.provinces) {
            //                var item = data.data.provinces[i];
            //                oAutoComplete.oProvinces.push({"name": item.name, "id": item.id});
            //
            //                var sHtml = "<div class='option' data-value='" + item.id + "'>" + item.name + "</div>";
            //                uiSearchCustomerHeaderProvince.append(sHtml);
            //                uiProvinceAC.append(sHtml);
            //                uiProvinceDTDmodal.append(sHtml);
            //                uiProvinceCoordinatorDTDmodal.append(sHtml);
            //                uiProvinceCoordinatorManagement.append(sHtml);
            //                uiProvinceCoordinatorPOmodal.append(sHtml);
            //
            //                sFetchRouteStores += item.id +",";
            //
            //            }
            //
            //            /*throw province list for store management*/
            //            callcenter.add_to_storage("autocomplete_gis_province",oAutoComplete.oProvinces);
            //
            //            sFetchRouteStores = sFetchRouteStores.slice(0,sFetchRouteStores.length-1);
            //            sFetchRouteStores +=")";
            //            /*add to local storage for coordinator management*/
            //            callcenter.coordinator_management.fetch_route_stores(1,sFetchRouteStores);
            //
            //            //callcenter.add_to_storage("autocomplete_gis_province",oAutoComplete.oProvinces);
            //            //var oAutoCompleteGISProvince = callcenter.get_from_storage("autocomplete_gis_province", '');
            //            callcenter.customer_list.render_province_select(oAutoComplete.oProvinces);
            //
            //            //Get City
            //            var oParams = {'call_center_user_id': 3305};
            //            var oAddressAjaxConfig1 = {
            //                "type"   : "Get",
            //                "data"   : oParams,
            //                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
            //                "url"    : callcenter.config('url.api.jfc.gis') + "/gis/city_address",
            //                "success": function (data) {
    			//				if(typeof(data) !== 'undefined')
            //                    {
            //                        for (var i in data.data.cities) {
            //                            var item = data.data.cities[i];
            //                            oAutoComplete.oCities.push({
            //                                "name"       : item.name,
            //                                "id"         : item.id,
            //                                "province_id": item.province_id
            //                            });
            //
            //                        }
            //                        //Get Building
            //                        var oParams = {
            //                            'offset'             : 0,
            //                            "limit"              : 20000,
            //                            'location_type_id'   : 1,
            //                            'call_center_user_id': 3305
            //                        };
        		//
            //                        loadingLocalData +=10;
            //                        loadingLocalDataSeconds -=3;
            //                        $("div.loader div.prog-bar").css("width",loadingLocalData+"%");
            //                        $("section.splash-container h3 span.loaded").text(loadingLocalData);
            //                        $("section.splash-container h4 span.seconds").text(loadingLocalDataSeconds);
            //                        callcenter.call_orders.building_autocomplete(oParams);
            //                    }
            //
            //                }
            //            }
    			//
            //            callcenter.call_orders.get(oAddressAjaxConfig1);
            //        }
            //
            //
            //        ////console.log(oAutoComplete.oBarangay);
            //    }
            //};
            //callcenter.call_orders.get(oAddressAjaxConfig);//removed as sir doms requested

            //add value on the select province option
            $('section #add_new_customer_header #search_customer_select_province').on('click', 'div.option', function (e) {
                var data_value = $(this).attr("data-value");
                $(this).closest("section #add_new_customer_header #search_customer_select_province").find("input.dd-txt").attr("value", data_value);

                if ($("section #add_new_customer_header #search_customer_select_province").find("input.dd-txt").attr("value") != "") {
                    oAutocompleteParams.params.where = [
                        {
                            "field"   : 'ca.province',
                            "operator": "=",
                            "value"   : function () {
                                return $("section #add_new_customer_header #search_customer_select_province").find("input.dd-txt").attr("value");
                            }
                        }
                    ]
                }
                ////console.log(oAutocompleteParams);

            });

            //get_city_list
            $('section.content #add_customer_content').on('click', '#province_select div.option', function (e) {
                var data_value = $(this).attr("data-value");

//                 if(data_value !="Select Province"){
//                         $('section.content div#add_customer_content form#add_customer').find('button.find_retail').attr("disabled",false);
//                 }else{
//                         $('section.content div#add_customer_content form#add_customer').find('button.find_retail').attr("disabled",true);
//                 }

                $(this).closest("section.content #add_customer_content div.frm-custom-dropdown").find("input.dd-txt").attr("value", data_value);
                $("section.content #add_customer_content #city_select div.frm-custom-dropdown").find("input.dd-txt").val("");

                var arResults = [];
                for (var i in oAutoComplete.oCities) {
                    var item = oAutoComplete.oCities[i];
                    //var bCheck = item.province_id.indexOf(data_value);
                    if (item.province_id == data_value) {

                        //if (bCheck != -1) {
                        arResults.push({"name": item.name, "id": item.id});
                        // }
                    }
                }

                ////console.log(arResults);
                var uiCity = $("section.content #add_customer_content #city_select")
                    .children("div.select")
                    .children("div.frm-custom-dropdown")
                    .children("div.frm-custom-dropdown-option");

                uiCity.html("");

                for (var i in arResults) {
                    var item = arResults[i];
                    var sHtml = "<div class='option' data-value='" + item.id + "'>" + item.name + "</div>";
                    uiCity.append(sHtml);
                }

                //Requested By Felix
                if (data_value == 1) {
                    $('section.content #add_customer_content #city_select').find('div.option[data-value="169"]').trigger('click');
                } else {
                    //add default selected city
                    $('section.content #add_customer_content #city_select').find('div.option[data-value]:first').trigger('click');
                }
                //clear values of autocomplete
                $('section.content #add_customer_content input.input_acautocomplete').val("");
                $('section.content #add_customer_content input.input_acautocomplete').attr("value", "");

            });

            //DTD_modal get_city_list
            $("div[modal-id='deliver-to-different'] form#add_address").on('click', '#modal_select_province div.option', function (e) {
                var data_value = $(this).attr("data-value");

                $(this).closest("div[modal-id='deliver-to-different'] form#add_address div.frm-custom-dropdown").find("input.dd-txt").attr("value", data_value);
                $("div[modal-id='deliver-to-different'] form#add_address div.frm-custom-dropdown").find("input.dd-txt[name!='address_type']").val("");

                var arResults = [];
                for (var i in oAutoComplete.oCities) {
                    var item = oAutoComplete.oCities[i];
                    var bCheck = item.province_id.indexOf(data_value);
                    if (bCheck != -1) {
                        arResults.push({"name": item.name, "id": item.id});
                    }
                }

                ////console.log(arResults);
                var uiCity = $("div[modal-id='deliver-to-different'] form#add_address #modal_select_city")
                    .children("div.select")
                    .children("div.frm-custom-dropdown")
                    .children("div.frm-custom-dropdown-option");

                uiCity.html("");

                for (var i in arResults) {
                    var item = arResults[i];
                    var sHtml = "<div class='option' data-value='" + item.id + "'>" + item.name + "</div>";
                    uiCity.append(sHtml);
                }


                //Requested By Felix
                if (data_value == 1) {
                    $("div[modal-id='deliver-to-different'] form#add_address #modal_select_city").find('div.option[data-value="169"]').trigger('click');
                } else {
                    //add default selected city
                    $("div[modal-id='deliver-to-different'] form#add_address #modal_select_city").find('div.option[data-value]:first').trigger('click');
                }

                //clear values of autocomplete
                $("div[modal-id='deliver-to-different'] form#add_address input.input_modal_autocomplete").val("");
                $("div[modal-id='deliver-to-different'] form#add_address input.input_modal_autocomplete").attr("value", "");

            });

            //Coordinator DTD_modal get_city_list
            $("div[modal-id='update-address'] form#coordinator_update_address").on('click', '#coordinator_modal_select_province div.option', function (e) {
                var data_value = $(this).attr("data-value");
                /*hidden field for find rta*/
                $(this).closest("div[modal-id='update-address'] form#coordinator_update_address").find('div.hidden_fields_for_rta input[name="province"][type="hidden"]').val(data_value);
                $("div[modal-id='update-address'] form#coordinator_update_address").find('div.found-retail').hide();
                $("div[modal-id='update-address'] form#coordinator_update_address").find('div.find-rta-container').show()

                $(this).closest("div[modal-id='update-address'] form#coordinator_update_address div.frm-custom-dropdown").find("input.dd-txt").attr("value", data_value);
                $("div[modal-id='update-address'] form#coordinator_update_address div.frm-custom-dropdown").find("input.dd-txt[name!='address_type']").val("");

                var arResults = [];
                for (var i in oAutoComplete.oCities) {
                    var item = oAutoComplete.oCities[i];
                    var bCheck = item.province_id.indexOf(data_value);
                    if (bCheck != -1) {
                        arResults.push({"name": item.name, "id": item.id});
                    }
                }

                ////console.log(arResults);
                var uiCity = $("div[modal-id='update-address'] form#coordinator_update_address #coordinator_modal_select_city")
                    .children("div.select")
                    .children("div.frm-custom-dropdown")
                    .children("div.frm-custom-dropdown-option");

                uiCity.html("");

                for (var i in arResults) {
                    var item = arResults[i];
                    var sHtml = "<div class='option' data-value='" + item.id + "'>" + item.name + "</div>";
                    uiCity.append(sHtml);
                }


                //Requested By Felix
                if (data_value == 1) {
                    $(this).closest("div[modal-id='update-address'] form#coordinator_update_address").find('div#city_select input[name="city"][type="hidden"]').val('169');
                    $("div[modal-id='update-address'] form#coordinator_update_address #coordinator_modal_select_city").find('div.option[data-value="169"]').trigger('click');
                } else {
                    //add default selected city
                    $("div[modal-id='update-address'] form#coordinator_update_address #coordinator_modal_select_city").find('div.option[data-value]:first').trigger('click');

                    /*hidden field for find rta*/
                    var cityId = $("div[modal-id='update-address'] form#coordinator_update_address #coordinator_modal_select_city").find('div.option[data-value]:first').attr('data-value');
                    $(this).closest("div[modal-id='update-address'] form#coordinator_update_address").find('div#city_select input[name="city"][type="hidden"]').val(cityId);

                }

                //clear values of autocomplete
                $("div[modal-id='update-address'] form#coordinator_update_address input.input_modal_autocomplete").val("");
                $("div[modal-id='update-address'] form#coordinator_update_address input.input_modal_autocomplete").attr("value", "");

            });

            //Coordinator PO_modal get_city_list
            $("div[modal-id='process-order'] form#coordinator_process_order_address").on('click', '#coordinator_po_modal_select_province div.option', function (e) {
                var data_value = $(this).attr("data-value");
                /*hidden field for find rta*/
                $(this).closest("div[modal-id='process-order'] form#coordinator_process_order_address").find('div.hidden_fields_for_rta input[name="province"][type="hidden"]').val(data_value);
                $("div[modal-id='process-order'] form#coordinator_process_order_address").find('div.found-retail').hide();
                $("div[modal-id='process-order'] form#coordinator_process_order_address").find('div.find-rta-container').show()

                $(this).closest("div[modal-id='process-order'] form#coordinator_process_order_address div.frm-custom-dropdown").find("input.dd-txt").attr("value", data_value);
                $("div[modal-id='process-order'] form#coordinator_process_order_address div.frm-custom-dropdown").find("input.dd-txt[name!='address_type']").val("");

                var arResults = [];
                for (var i in oAutoComplete.oCities) {
                    var item = oAutoComplete.oCities[i];
                    var bCheck = item.province_id.indexOf(data_value);
                    if (bCheck != -1) {
                        arResults.push({"name": item.name, "id": item.id});
                    }
                }

                ////console.log(arResults);
                var uiCity = $("div[modal-id='process-order'] form#coordinator_process_order_address #coordinator_po_modal_select_city")
                    .children("div.select")
                    .children("div.frm-custom-dropdown")
                    .children("div.frm-custom-dropdown-option");

                uiCity.html("");

                for (var i in arResults) {
                    var item = arResults[i];
                    var sHtml = "<div class='option' data-value='" + item.id + "'>" + item.name + "</div>";
                    uiCity.append(sHtml);
                }


                //Requested By Felix
                if (data_value == 1) {
                    $(this).closest("div[modal-id='process-order'] form#coordinator_process_order_address").find('div#city_select input[name="city"][type="hidden"]').val('169');
                    $("div[modal-id='process-order'] form#coordinator_process_order_address #coordinator_po_modal_select_city").find('div.option[data-value="169"]').trigger('click');
                } else {
                    //add default selected city
                    $("div[modal-id='process-order'] form#coordinator_process_order_address #coordinator_po_modal_select_city").find('div.option[data-value]:first').trigger('click');

                    /*hidden field for find rta*/
                    var cityId = $("div[modal-id='process-order'] form#coordinator_process_order_address #coordinator_po_modal_select_city").find('div.option[data-value]:first').attr('data-value');
                    $(this).closest("div[modal-id='process-order'] form#coordinator_process_order_address").find('div#city_select input[name="city"][type="hidden"]').val(cityId);

                }

                //clear values of autocomplete
                $("div[modal-id='process-order'] form#coordinator_process_order_address input.input_modal_autocomplete").val("");
                $("div[modal-id='process-order'] form#coordinator_process_order_address input.input_modal_autocomplete").attr("value", "");

            });

            //get_location_list
            $('section.content #add_customer_content').on('click', '#city_select div.option', function (e) {

                var data_value = $(this).attr("data-value");
                $(this).closest("section.content #add_customer_content div.frm-custom-dropdown").find("input.dd-txt").attr("value", data_value);
                oAutoComplete.oPOIFiltered = [];
                oAutoComplete.oBarangayFiltered = [];
                oAutoComplete.oStreetFiltered = [];
                oAutoComplete.oSubdivisionFiltered = [];

                callcenter.call_orders.filter_autocomplete_results_object(data_value);

                //add_customer_autocompletes
                $('section.content #add_customer_content input#search_building').on('keyup', function () {
                    if ($(this).val().length > 2) {
                        callcenter.call_orders.render_autocomplete($(this).val(), oAutoComplete.oPOIFiltered, $(this).siblings("div.result-list"));
                    } else {
                        $(this).siblings("div.result-list").html("");
                    }
                })
                $('section.content #add_customer_content input#search_street').on('keyup', function () {
                    if ($(this).val().length > 2) {
                        callcenter.call_orders.render_autocomplete($(this).val(), oAutoComplete.oStreetFiltered, $(this).siblings("div.result-list"));
                    } else {
                        $(this).siblings("div.result-list").html("");
                    }
                })
                $('section.content #add_customer_content input#search_second_street').on('keyup', function () {
                    if ($(this).val().length > 2) {
                        callcenter.call_orders.render_autocomplete($(this).val(), oAutoComplete.oStreetFiltered, $(this).siblings("div.result-list"));
                    } else {
                        $(this).siblings("div.result-list").html("");
                    }
                })
                $('section.content #add_customer_content input#search_subdivision').on('keyup', function () {
                    if ($(this).val().length > 2) {
                        callcenter.call_orders.render_autocomplete($(this).val(), oAutoComplete.oSubdivisionFiltered, $(this).siblings("div.result-list"));
                    } else {
                        $(this).siblings("div.result-list").html("");
                    }
                })
                $('section.content #add_customer_content input#search_barangay').on('keyup', function () {
                    if ($(this).val().length > 2) {
                        callcenter.call_orders.render_autocomplete($(this).val(), oAutoComplete.oBarangayFiltered, $(this).siblings("div.result-list"));
                    } else {
                        $(this).siblings("div.result-list").html("");
                    }
                })


                //clear values of autocomplete
                $('section.content #add_customer_content input.input_acautocomplete').val("");
                $('section.content #add_customer_content input.input_acautocomplete').attr("value", "");

            });

            //DTD_modal get_location_list
            $("div[modal-id='deliver-to-different'] form#add_address").on('click', '#modal_select_city div.option', function (e) {
                var data_value = $(this).attr("data-value");
                $(this).closest("div[modal-id='deliver-to-different'] form#add_address div.frm-custom-dropdown").find("input.dd-txt").attr("value", data_value);

                oAutoComplete.oPOIFiltered = [];
                oAutoComplete.oBarangayFiltered = [];
                oAutoComplete.oStreetFiltered = [];
                oAutoComplete.oSubdivisionFiltered = [];

                callcenter.call_orders.filter_autocomplete_results_object(data_value);

                //DTD_address_autocompletes
                $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_building").on('keyup', function () {
                    if ($(this).val().length > 2) {
                        callcenter.call_orders.render_autocomplete($(this).val(), oAutoComplete.oPOIFiltered, $(this).siblings("div.result-list"));
                    } else {
                        $(this).siblings("div.result-list").html("");
                    }
                })
                $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_street").on('keyup', function () {
                    if ($(this).val().length > 2) {
                        callcenter.call_orders.render_autocomplete($(this).val(), oAutoComplete.oStreetFiltered, $(this).siblings("div.result-list"));
                    } else {
                        $(this).siblings("div.result-list").html("");
                    }
                })
                $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_second_street").on('keyup', function () {
                    if ($(this).val().length > 2) {
                        callcenter.call_orders.render_autocomplete($(this).val(), oAutoComplete.oStreetFiltered, $(this).siblings("div.result-list"));
                    } else {
                        $(this).siblings("div.result-list").html("");
                    }
                })
                $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_subdivision").on('keyup', function () {
                    if ($(this).val().length > 2) {
                        callcenter.call_orders.render_autocomplete($(this).val(), oAutoComplete.oSubdivisionFiltered, $(this).siblings("div.result-list"));
                    } else {
                        $(this).siblings("div.result-list").html("");
                    }
                })
                $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_barangay").on('keyup', function () {
                    if ($(this).val().length > 2) {
                        callcenter.call_orders.render_autocomplete($(this).val(), oAutoComplete.oBarangayFiltered, $(this).siblings("div.result-list"));
                    } else {
                        $(this).siblings("div.result-list").html("");
                    }
                })


                //clear values of autocomplete
                $("div[modal-id='deliver-to-different'] form#add_address input.input_modal_autocomplete").val("");
                $("div[modal-id='deliver-to-different'] form#add_address input.input_modal_autocomplete").attr("value", "");

            });

            //Coordinator_DTD_modal get_location_list
            $("div[modal-id='update-address'] form#coordinator_update_address").on('click', '#coordinator_modal_select_city div.option', function (e) {
                var data_value = $(this).attr("data-value");
                $(this).closest("div[modal-id='update-address'] form#coordinator_update_address div.frm-custom-dropdown").find("input.dd-txt").attr("value", data_value);

                oAutoComplete.oPOIFiltered = [];
                oAutoComplete.oBarangayFiltered = [];
                oAutoComplete.oStreetFiltered = [];
                oAutoComplete.oSubdivisionFiltered = [];

                callcenter.call_orders.filter_autocomplete_results_object(data_value);

                //Coordinator_DTD_address_autocompletes
                $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_building").on('keyup', function () {
                    if ($(this).val().length > 2) {
                        callcenter.call_orders.render_autocomplete($(this).val(), oAutoComplete.oPOIFiltered, $(this).siblings("div.result-list"));
                    } else {
                        $(this).siblings("div.result-list").html("");
                    }
                })
                $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_street").on('keyup', function () {
                    if ($(this).val().length > 2) {
                        callcenter.call_orders.render_autocomplete($(this).val(), oAutoComplete.oStreetFiltered, $(this).siblings("div.result-list"));
                    } else {
                        $(this).siblings("div.result-list").html("");
                    }
                })
                $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_second_street").on('keyup', function () {
                    if ($(this).val().length > 2) {
                        callcenter.call_orders.render_autocomplete($(this).val(), oAutoComplete.oStreetFiltered, $(this).siblings("div.result-list"));
                    } else {
                        $(this).siblings("div.result-list").html("");
                    }
                })
                $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_subdivision").on('keyup', function () {
                    if ($(this).val().length > 2) {
                        callcenter.call_orders.render_autocomplete($(this).val(), oAutoComplete.oSubdivisionFiltered, $(this).siblings("div.result-list"));
                    } else {
                        $(this).siblings("div.result-list").html("");
                    }
                })
                $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_barangay").on('keyup', function () {
                    if ($(this).val().length > 2) {
                        callcenter.call_orders.render_autocomplete($(this).val(), oAutoComplete.oBarangayFiltered, $(this).siblings("div.result-list"));
                    } else {
                        $(this).siblings("div.result-list").html("");
                    }
                })


                //clear values of autocomplete
                $("div[modal-id='update-address'] form#coordinator_update_address input.input_modal_autocomplete").val("");
                $("div[modal-id='update-address'] form#coordinator_update_address input.input_modal_autocomplete").attr("value", "");

            });

            //Coordinator_PO_modal get_location_list
            $("div[modal-id='process-order'] form#coordinator_process_order_address").on('click', '#coordinator_po_modal_select_city div.option', function (e) {
                var data_value = $(this).attr("data-value");
                //console.log(data_value);
                $("div[modal-id='process-order'] form#coordinator_process_order_address div#city_select_process_order_address").find("input").attr("value", data_value);
                //$(this).closest("div[modal-id='process-order'] form#coordinator_process_order_address div.frm-custom-dropdown").find("input.dd-txt").attr("value", data_value);

                oAutoComplete.oPOIFiltered = [];
                oAutoComplete.oBarangayFiltered = [];
                oAutoComplete.oStreetFiltered = [];
                oAutoComplete.oSubdivisionFiltered = [];

                callcenter.call_orders.filter_autocomplete_results_object(data_value);

                //Coordinator_PO_address_autocompletes
                $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_building").on('keyup', function () {
                    if ($(this).val().length > 2) {
                        callcenter.call_orders.render_autocomplete($(this).val(), oAutoComplete.oPOIFiltered, $(this).siblings("div.result-list"));
                    } else {
                        $(this).siblings("div.result-list").html("");
                    }
                })
                $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_street").on('keyup', function () {
                    if ($(this).val().length > 2) {
                        callcenter.call_orders.render_autocomplete($(this).val(), oAutoComplete.oStreetFiltered, $(this).siblings("div.result-list"));
                    } else {
                        $(this).siblings("div.result-list").html("");
                    }
                })
                $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_second_street").on('keyup', function () {
                    if ($(this).val().length > 2) {
                        callcenter.call_orders.render_autocomplete($(this).val(), oAutoComplete.oStreetFiltered, $(this).siblings("div.result-list"));
                    } else {
                        $(this).siblings("div.result-list").html("");
                    }
                })
                $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_subdivision").on('keyup', function () {
                    if ($(this).val().length > 2) {
                        callcenter.call_orders.render_autocomplete($(this).val(), oAutoComplete.oSubdivisionFiltered, $(this).siblings("div.result-list"));
                    } else {
                        $(this).siblings("div.result-list").html("");
                    }
                })
                $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_barangay").on('keyup', function () {
                    if ($(this).val().length > 2) {
                        callcenter.call_orders.render_autocomplete($(this).val(), oAutoComplete.oBarangayFiltered, $(this).siblings("div.result-list"));
                    } else {
                        $(this).siblings("div.result-list").html("");
                    }
                })


                //clear values of autocomplete
                $("div[modal-id='process-order'] form#coordinator_process_order_address input.input_modal_autocomplete").val("");
                $("div[modal-id='process-order'] form#coordinator_process_order_address input.input_modal_autocomplete").attr("value", "");

            });


            callcenter.validate_form($('form#add_address'), oAddAddressModalValidationConfig);

            $('section.content #add_customer_content button.save_customer').on('click', function (e) {
                callcenter.call_orders.show_spinner($('button.save_customer'), true);
                $('section.content #add_customer_content form#add_customer').submit();
            });

            $('button.add_address_button').on('click', function (e) {
                callcenter.call_orders.show_spinner($('button.add_address_button'), true);
                $('form#add_address').submit();
            });

            $('button.update_address_button').on('click', function (e) {
                callcenter.call_orders.show_spinner($('button.update_address_button'), true);
                $('form#add_address').submit();
            });

            $('section.first-child-section').on('click', function (e) {

                if ($(e.target).hasClass('another_remove')) {
                    e.preventDefault();
                    var uiButton = $(e.target);
                    var uiElemtoRemove = $(e.target).parents('div.remove_parent:first');
                    callcenter.call_orders.remove_append(uiElemtoRemove);
                }
            });

            //hide header / show content
            $('section.content #search_customer_content').on('click', 'div.header-search', function () {
                
                $(this).parents('div.search_result_container:first').find('div.collapsible').show();
                $(this).hide();

               
                $(this).parents('div.search_result_container').siblings().find('div.collapsible').hide();
                $(this).parents('div.search_result_container').siblings().find('div.header-search').show();

                //hide update success msg
                $(this).parents('div.search_result_container:first').find('div.collapsible div.success-msg').hide();

                var bVisible = $(this).next('div.collapsible').is(":visible");
                var iCustomerID = $(this).parents('div.search_result_container:first').attr('data-customer-id');
                if (bVisible) {
                    callcenter.call_orders.get_order_history($(this).parents('div.search_result_container:first'), iCustomerID);
                }

                
            });

            //hide content / show header
            $('section.content #search_customer_content').on('click', 'div.second_header', function () {
                var iCustomerId = $(this).attr('data-customer-id');
                $('section.content #search_customer_content').find('div.search_result_container[data-customer-id="' + iCustomerId + '"]').find('div.header-search').show();
                $(this).parents('div.collapsible').find('div.success-msg').hide();
//                 $(this).parents('div.collapsible').find('div.find-rta-container').show();
//                 $(this).parents('div.collapsible').find('div.found-retail').hide();
                $(this).parent().hide();
            });


            $('section.content #add_customer_content').on('click', 'input[type="radio"][name="happyplus"]', function (e) { //hide/show of happy plus card
                var uiContainerClass = 'happy_plus_card';
                if ($(this).val() == "1") { //if yes, show and add a require validation

                    if ($('section.content #add_customer_content div.happy_plus_card').first().find('a.another_happy_plus').length == 0) {
                        var oLink = $('section.content #add_customer_content div.happy_plus_card').find('a.another_happy_plus').clone();
                        $('section.content #add_customer_content div.happy_plus_card').first().append(oLink);
                        $('section.content #add_customer_content div.happy_plus_card').first().append("<div class='clear'></div>");
                    }

                    // callcenter.call_orders.extra_enable(uiContainerClass, false);
                    $('section.content #add_customer_content div.happy_plus_card:not(.template)').first().show();
                    $('section.content #add_customer_content div.happy_plus_card:not(.template)').find("input[type='text'][name='number']").attr({
                        'labelinput': 'Card Number',
                        'datavalid' : 'required'
                    });
                    $('section.content #add_customer_content div.happy_plus_card:not(.template)').find("input[type='text'][name='expiry_date']").attr({
                        'labelinput': 'Expiry Date',
                        'datavalid' : 'required'
                    });
                }
                else if ($(this).val() == "0") { //if no, it will hide and remove the validation
                    // callcenter.call_orders.extra_enable(uiContainerClass, true);
                    $('section.content #add_customer_content div.happy_plus_card').hide();
                    $('section.content #add_customer_content div.happy_plus_card:not(.template)').find("input[type='text'][name='number']").removeAttr('labelinput datavalid');
                    $('section.content #add_customer_content div.happy_plus_card:not(.template)').find("input[type='text'][name='expiry_date']").removeAttr('labelinput datavalid');
                }
            })


            $('section.content #add_customer_content').on('click', "input[type='checkbox'][name='is_smudged']", function (e) { //disable card number and expiry date if is smudged is checked
                if ($(this).is(':checked')) {
                    $(this).parents('section.content #add_customer_content div.happy_plus_card').find("input:not([type='checkbox']):not([name='remarks'])").prop("disabled", true);
                    $(this).parents('section.content #add_customer_content div.happy_plus_card').find("div.date-picker").css('pointer-events', 'none');
                }
                else {
                    $(this).parents('section.content #add_customer_content div.happy_plus_card').find("input").prop("disabled", false);
                    $(this).parents('section.content #add_customer_content div.happy_plus_card').find("div.date-picker").css('pointer-events', 'auto');
                }
            });

            $('section.content #add_customer_content').on('click', 'input[type="radio"][name="senior"]', function (e) { //hide/show of senior citizen osca number
                if ($(this).val() == "1") {
                    $('section.content #add_customer_content div.senior_citizen').show();
                    $('section.content #add_customer_content div.senior_citizen').find("input[type='text'][name='osca_number']").attr({
                        'labelinput': 'Osca Number',
                        'datavalid' : 'required'
                    });
                    $('section.content #add_customer_content div.pwd_container').find('input#pwd2').prop('checked', true); //uncheck pwd because their can only be one discount
                }
                else if ($(this).val() == "0") {
                    $('section.content #add_customer_content div.senior_citizen').hide();
                    $('section.content #add_customer_content div.senior_citizen').find("input[type='text'][name='osca_number']").removeAttr('datavalid labelinput');
                }
            });

            $('section.content #add_customer_content').on('click', 'input[type="radio"][name="pwd"]', function (e) { //uncheck senior citizen because their can only be one discount
                if ($(this).val() == "1") {
                    $('section.content #add_customer_content div.senior_citizen').hide();
                    $('section.content #add_customer_content div.senior_citizen').find("input[type='text'][name='osca_number']").removeAttr('datavalid labelinput');
                    $('section.content #add_customer_content div.senior_container').find('input#senior2').prop('checked', true);
                }
                else if ($(this).val() == "0") {

                }
            });

            $('section.content #no_search_customer_content').on('click', 'button.add_new_customer', function (e) { //if no customer is searched with the contact number input, it will add the number to the input if add new customer is clicked
                var isCustomerSaved = callcenter.get_from_storage("isCustomerSaved", '');
                if(typeof(isCustomerSaved) != 'undefined')
                {
                    callcenter.pop("isCustomerSaved");
                }
                var isHappyPlusAdded = callcenter.get_from_storage("happy_plus_cards", '');
                if(typeof(isHappyPlusAdded) != 'undefined')
                {
                    callcenter.pop("happy_plus_cards");
                }
                callcenter.call_orders.content_panel_toggle('add_customer');
                $('section.container-fluid[section-style="top-panel"]').hide();
                $('section.container-fluid.add_new_customer[section-style="top-panel"]').removeClass('hidden').show();
                $("section.content #add_customer_content").find('div#city_select div.select input.dd-txt[name="city"]').attr({'datavalid':'required', 'labelinput':'City'});
                $("section.content #add_customer_content").find('div#province_select div.select input.dd-txt[name="province"]').attr({'datavalid':'required', 'labelinput':'Province'});
                // $("section.content #add_customer_content").find('div.select input.dd-txt[name="address_type"]').attr({'datavalid':'required', 'labelinput':'Address Type'});

                $('section.content #add_customer_content div.pwd_container').find('input#pwd2').prop('checked', true);
                $('section.content #add_customer_content div.senior_container').find('input#senior2').prop('checked', true);
                $('section.content #add_customer_content div.parent_happy_plus').find('input#happy2').prop('checked', true);
                $('section.content #add_customer_content div.loyalty_container').find('input#loyalty2').prop('checked', true);
                
                var oValue = $('section.add_new_customer').find('input.search').val();
                if (oValue.length == 9 && !isNaN(oValue)) //if 9 digits is search, it will put it on the landline input
                {
                    $('section.content #add_customer_content div.primary_contact_number').find("input[name='contact_number[]']:first").val('');
                    $('section.content #add_customer_content div.primary_contact_number').find("div.frm-custom-dropdown input[name='contact_number_type[]']").attr('value', 'Landline');
                    $('section.content #add_customer_content div.primary_contact_number').find("div.frm-custom-dropdown input[name='contact_number_type[]']").val('Landline');

                    if ($('section.content #add_customer_content div.primary_contact_number').find("div.frm-custom-dropdown input[name='contact_number_type[]']").attr("value") == "Landline") {
                        $("section.content #add_customer_content div.primary_contact_number").find("input.small").addClass("hidden");
                        $("section.content #add_customer_content div.primary_contact_number").find("input.small").removeAttr("datavalid");
                        $("section.content #add_customer_content div.primary_contact_number").find("input.xsmall").removeClass("hidden");
                        $("section.content #add_customer_content div.primary_contact_number").find("input.xsmall").attr("datavalid", /*"numeric*/" required");
                        $("section.content #add_customer_content div.primary_contact_number").find("input.xsmall:first").val(oValue);
                    }
                }
                else if ((oValue.length == 11 && !isNaN(oValue)) || (oValue.length == 12 && !isNaN(oValue))) //if 11 or 12 digits is search, it will put it on the mobile input
                {
                    $("section.content #add_customer_content div.primary_contact_number").find("input.xsmall:first").val('');
                    $('section.content #add_customer_content div.primary_contact_number').find("div.frm-custom-dropdown input[name='contact_number_type[]']").attr('value', 'Mobile');
                    $('section.content #add_customer_content div.primary_contact_number').find("div.frm-custom-dropdown input[name='contact_number_type[]']").val('Mobile');

                    if ($('section.content #add_customer_content div.primary_contact_number').find("div.frm-custom-dropdown input[name='contact_number_type[]']").attr("value") == "Mobile") {
                        $("section.content #add_customer_content div.primary_contact_number").find("input.small").removeClass("hidden");
                        $("section.content #add_customer_content div.primary_contact_number").find("input.small").attr("datavalid", /*"numeric*/" required");
                        $("section.content #add_customer_content div.primary_contact_number").find("input.xsmall").addClass("hidden");
                        $("section.content #add_customer_content div.primary_contact_number").find("input.xsmall").removeAttr("datavalid");
                        $('section.content #add_customer_content div.primary_contact_number').find("input[name='contact_number[]']:first").val(oValue);
                    }
                }
                else {
                    $("section.content #add_customer_content div.primary_contact_number").find("input.xsmall:first").val('');
                    $('section.content #add_customer_content div.primary_contact_number').find("input[name='contact_number[]']:first").val('');
                    $('section.content #add_customer_content div.primary_contact_number').find("div.frm-custom-dropdown input[name='contact_number_type[]']").attr('value', 'Mobile');
                    $('section.content #add_customer_content div.primary_contact_number').find("div.frm-custom-dropdown input[name='contact_number_type[]']").val('Mobile');

                    if ($('section.content #add_customer_content div.primary_contact_number').find("div.frm-custom-dropdown input[name='contact_number_type[]']").attr("value") == "Mobile") {
                        $("section.content #add_customer_content div.primary_contact_number").find("input.small").removeClass("hidden");
                        $("section.content #add_customer_content div.primary_contact_number").find("input.small").attr("datavalid", /*numeric*/" required");
                        $("section.content #add_customer_content div.primary_contact_number").find("input.xsmall").addClass("hidden");
                        $("section.content #add_customer_content div.primary_contact_number").find("input.xsmall").removeAttr("datavalid");
                    }

                }
            });

            Offline.on('up', function () {
                callcenter.CconnectionDetector.initialize.on_online();
            });

            Offline.on('down', function (e) {
                callcenter.CconnectionDetector.initialize.on_offline();
            });

            Offline.options = {
                checks  : {
                    xhr: {
                        url: '/callcenter/admin'
                    }
                },
                requests: false

            };

            //var connection_state_on = true;

            var check_connection_state = function () {
                Offline.check()
            }

            setInterval(check_connection_state, 5000);

            $('section#add_new_customer_header').on('click', 'div.list', function () {
                $('div.list').remove();
                var sFirstName = $(this).attr('data-first_name'),
                    sMiddleName = $(this).attr('data-middle_name'),
                    sLastName = $(this).attr('data-last_name');

                var oParams = {
                    "params": {
                        "where": [
                            {
                                "field"   : 'c.first_name',
                                "operator": "=",
                                "value"   : '"'+sFirstName+'"'
                            },
                            {
                                "field"   : 'c.middle_name',
                                "operator": "=",
                                "value"   : '"'+sMiddleName+'"'
                            },
                            {
                                "field"   : 'c.last_name',
                                "operator": "=",
                                "value"   : '"'+sLastName+'"'
                            }
                        ]
                    }
                }

                var oAjaxConfig = {
                    "type"   : "Get",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.customers') + "/customers/get",
                    "success": function (data) {
                        /*//console.log(data);
                         //console.log(count(data.data))*/
                         //clear search box
                        $('section#add_new_customer_header #search_customer').val("");
                        if(typeof(data) !== 'undefined')
                        {
                            if (count(data.data) > 0) {
                                $('p.count_search_result strong').text('Search Result(s) | ' + count(data.data) + ' Customers');
                                callcenter.call_orders.content_panel_toggle('search_customer');
                                var uiContainer = $('div.search_customer');
                                uiContainer.html('');
                                $.each(data.data, function (key, value) {
                                    var uiTemplate = $('div.search_result_container.template').clone().removeClass('template');
                                    var uiManipulatedTemplate = callcenter.call_orders.manipulate_template(uiTemplate, value);
                                    uiManipulatedTemplate.find('div.customer_secondary_address').hide();
                                    callcenter.call_orders.clone_append(uiManipulatedTemplate, uiContainer);
                                })
                            }
                            else {
                                $('p.count_search_result').find('strong').text('No Search Result');
                                callcenter.call_orders.content_panel_toggle('no_search_customer');
                            }
                        }
                        //add class active on coordinator side
                        $('header ul#main_nav_headers li.order_management').addClass('active');
                    }
                };

                callcenter.call_orders.get(oAjaxConfig)

            })

            $('section#add_new_customer_header').on('click', 'button.search_button', function () {
                if($('section#add_new_customer_header input#search_customer').val().length > 2 ){
                    callcenter.call_orders.show_spinner($('button.search_button'), true);
                    callcenter.call_orders.assemble_search_param();
                }
            })

            $('section#add_new_customer_header').on('click', 'button.add_new_customer', function () {
                callcenter.call_orders.content_panel_toggle('add_customer');
                $('section.container-fluid[section-style="top-panel"]').addClass('hidden');
                $('section.container-fluid.add_new_customer[section-style="top-panel"]').removeClass('hidden').show();
                $("section.content #add_customer_content div#city_select").find('div.select input.dd-txt').attr({
                    'labelinput': 'City',
                    'datavalid' : 'required'
                });
                $('header ul#main_nav_headers li.order_management').addClass('active');
                var isCustomerSaved = callcenter.get_from_storage("isCustomerSaved", '');
                if(typeof(isCustomerSaved) != 'undefined')
                {
                    callcenter.pop("isCustomerSaved");
                }
                var isHappyPlusAdded = callcenter.get_from_storage("happy_plus_cards", '');
                if(typeof(isHappyPlusAdded) != 'undefined')
                {
                    callcenter.pop("happy_plus_cards");
                }
                $('section.content #add_customer_content div.pwd_container').find('input#pwd2').prop('checked', true);
                $('section.content #add_customer_content div.senior_container').find('input#senior2').prop('checked', true);
                $('section.content #add_customer_content div.parent_happy_plus').find('input#happy2').prop('checked', true);
                $('section.content #add_customer_content div.loyalty_container').find('input#loyalty2').prop('checked', true);
            })

            $('section#add_new_customer_header').on('click', 'button.product_list', function () {
                callcenter.call_orders.content_panel_toggle('product_list');
                $('section.content #order_process_content').removeClass("hidden");
                $('header ul#main_nav_headers li.order_management').addClass('active');
            })

            $('section.content #search_customer_content').on('click', 'button.order_process', function () {
                callcenter.call_orders.content_panel_toggle('order_process');
            })


            $('section#customer_list_header').on('click', 'button.add_new_customer', function () {
                callcenter.call_orders.content_panel_toggle('add_customer');
                $('section.container-fluid[section-style="top-panel"]').addClass('hidden');
                $('section.container-fluid.add_new_customer[section-style="top-panel"]').removeClass('hidden').show();
                $("section.content #add_customer_content div#city_select").find('div.select input.dd-txt').attr({
                    'labelinput': 'City',
                    'datavalid' : 'required'
                });
                $('header ul#main_nav_headers li.order_management').addClass('active');
                var isCustomerSaved = callcenter.get_from_storage("isCustomerSaved", '');
                if(typeof(isCustomerSaved) != 'undefined')
                {
                    callcenter.pop("isCustomerSaved");
                }
                var isHappyPlusAdded = callcenter.get_from_storage("happy_plus_cards", '');
                if(typeof(isHappyPlusAdded) != 'undefined')
                {
                    callcenter.pop("happy_plus_cards");
                }
                $('section.content #add_customer_content div.pwd_container').find('input#pwd2').prop('checked', true);
                $('section.content #add_customer_content div.senior_container').find('input#senior2').prop('checked', true);
                $('section.content #add_customer_content div.parent_happy_plus').find('input#happy2').prop('checked', true);
                $('section.content #add_customer_content div.loyalty_container').find('input#loyalty2').prop('checked', true);
            })

            $('ul.main_nav').on('click', 'li.customer_list', function () {
                callcenter.call_orders.content_panel_toggle('customer_list');
                $('section.container-fluid[section-style="top-panel"]').hide();
                $('section.container-fluid.customer_list[section-style="top-panel"]').removeClass('hidden').show();
                $('section#search_order_list_container').addClass('hidden');

                //clearing form values
                $('section#add_new_customer_header input#search_customer').val("");
                $('section#search_order_form_container form#search_order_form_params input[name="search_string"]').val("");
                $('section#customer_list_header input#search_customer_list').val("");

                //clearing results
                $('section.content div#customer_list_content').addClass('hidden');
            })

            $('section.content #search_customer_content').on('click', 'p[modal-target="deliver-to-different"]', function () {
                $(this).parents().find('div.modal-error-msg p.error_messages').remove();
                $(this).parents().find('div.modal-error-msg').hide();
                $('form#add_address').find('input').val('');
                $('form#add_address').find('input[name="is_default"]').prop('checked', false);
                $('form#add_address').find('input').removeClass('error');
                $('form#add_address').find('div.select').removeClass('has-error');

                $('div[modal-id="deliver-to-different"]').find('button.update_address_button').removeClass("update_address_button").addClass("add_address_button");

                var iCustomerID = $(this).parents('div.search_result_container:first').attr('data-customer-id');
                $('form#add_address').find('input[name="customer_id"]').val(iCustomerID);

                // $('form#add_address').find('input.dd-txt[name="address_type"]').attr({'datavalid': 'required', 'labelinput':'Address Type'});
                $('form#add_address').find('input.dd-txt[name="province"]').attr({'datavalid': 'required', 'labelinput':'Province'});
                $('form#add_address').find('input.dd-txt[name="city"]').attr({'datavalid': 'required', 'labelinput':'City'});
            })

            $('section.content #search_customer_content').on('click', 'a.edit_address', function (e) {
                e.preventDefault();
                $(this).toggleHtml('Cancel<div class="arrow-down"></div>', 'Change<br>Address<br><div class="arrow-down"></div>');
                $(this).parents('div.customer_address_container:first').next('div.customer_secondary_address').toggle();

            })

            $('section.content #search_customer_content').on('click', 'div.customer_secondary_address div.customer_address', function (e) {
                var uiThis = $(this);
                var oParams = {
                    "params": {
                        "address_id" : $(this).attr('address_id'),
                        "customer_id": $(this).attr('customer_id')
                    }
                }

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.customers') + "/customers/set_default_address",
                    "success": function (data) {
                        if(typeof(data) !== 'undefined')
                        {  
                            var uiParent = uiThis.parents('div.customer_secondary_address:first');
                            var oOldPrimaryAddress = uiThis.parents('div.customer_secondary_address').prev('div.customer_address_container').find('div.customer_address:visible').clone();
                            var changeaddress = oOldPrimaryAddress.find('div.edit_container').clone();
                            var uiParentContainer = uiThis.parents('div.search_result_container:first');
                            uiThis.parents('div.customer_secondary_address').prev('div.customer_address_container').find('div.customer_address:visible').replaceWith(uiThis.removeClass('white').addClass('bggray-light').append(changeaddress));
                            uiParent.prepend(oOldPrimaryAddress.removeClass('bggray-light').addClass('white')).find('div.edit_container').remove();
                            uiParent.toggle();
                            uiThis.parents('div.search_result_container').find('a.edit_address').html('Change<br>Address<br><div class="arrow-down"></div>');
                            uiParentContainer.find('div.header-search div.remarks_and_address p.address').empty();
                            uiParentContainer.find('input[name="house_number"]').val(uiThis.attr('house_number'));
                            uiParentContainer.find('input[name="building"]').val(uiThis.attr('building'));
                            uiParentContainer.find('input[name="floor"]').val(uiThis.attr('floor'));
                            uiParentContainer.find('input[name="street"]').val(uiThis.attr('street'));
                            uiParentContainer.find('input[name="second_street"]').val(uiThis.attr('second_street'));
                            uiParentContainer.find('input[name="barangay"]').val(uiThis.attr('barangay'));
                            uiParentContainer.find('input[name="subdivision"]').val(uiThis.attr('subdivision'));
                            uiParentContainer.find('input[name="city"]').val(uiThis.attr('city'));
                            uiParentContainer.find('input[name="province"]').val(uiThis.attr('province'));
                            uiParentContainer.find('div.header-search div.remarks_and_address p.address').append("<span class='red-color info_address'><strong class=''>Address: </strong></span>" + uiThis.attr('all_address'));

                            uiParentContainer.find('div.find-rta-container').show();
                            uiParentContainer.find('div.found-retail').hide();
                            if(uiThis.attr('store_id') > 0)
                            {
                                var oStoreInformation = callcenter.call_orders.fetch_store_information(uiThis.attr('store_id'), uiParentContainer, uiThis.attr('delivery_time'));
                            }

                            uiParentContainer.find('button.add_last_meal_to_order').prop('disabled', true)
                            uiParentContainer.find('button.proceed_order_from_search').prop('disabled', true)
                        }
                    }
                };

                callcenter.call_orders.get(oAjaxConfig)


            })

            //add_customer_autocompletes
            $("section.content #add_customer_content input#search_building").siblings("div.result-list").on('mousedown', 'div.acautocomplete', function () {//put the data on textbox
                $("section.content #add_customer_content input#search_building").siblings("div.result-list").html("");
                $("section.content #add_customer_content input#search_building").val($(this).attr("data-value"));
                $("section.content #add_customer_content input#search_building").attr("value", $(this).attr("data-id"));
                //enable find RTA
                $('section.content div#add_customer_content form#add_customer').find('button.find_retail').attr("disabled",false);
            })
            $("section.content #add_customer_content input#search_street").siblings("div.result-list").on('mousedown', 'div.acautocomplete', function () {//put the data on textbox
                $("section.content #add_customer_content input#search_street").siblings("div.result-list").html("");
                $("section.content #add_customer_content input#search_street").val($(this).attr("data-value"));
                $("section.content #add_customer_content input#search_street").attr("value", $(this).attr("data-id"));
                //enable find RTA
                $('section.content div#add_customer_content form#add_customer').find('button.find_retail').attr("disabled",false);
            })
            $("section.content #add_customer_content input#search_second_street").siblings("div.result-list").on('mousedown', 'div.acautocomplete', function () {//put the data on textbox
                $("section.content #add_customer_content input#search_second_street").siblings("div.result-list").html("");
                $("section.content #add_customer_content input#search_second_street").val($(this).attr("data-value"));
                $("section.content #add_customer_content input#search_second_street").attr("value", $(this).attr("data-id"));
                //enable find RTA
                $('section.content div#add_customer_content form#add_customer').find('button.find_retail').attr("disabled",false);
            })
            $("section.content #add_customer_content input#search_subdivision").siblings("div.result-list").on('mousedown', 'div.acautocomplete', function () {//put the data on textbox
                $("section.content #add_customer_content input#search_subdivision").siblings("div.result-list").html("");
                $("section.content #add_customer_content input#search_subdivision").val($(this).attr("data-value"));
                $("section.content #add_customer_content input#search_subdivision").attr("value", $(this).attr("data-id"));
                //enable find RTA
                $('section.content div#add_customer_content form#add_customer').find('button.find_retail').attr("disabled",false);
            })
            $("section.content #add_customer_content input#search_barangay").siblings("div.result-list").on('mousedown', 'div.acautocomplete', function () {//put the data on textbox
                $("section.content #add_customer_content input#search_barangay").siblings("div.result-list").html("");
                $("section.content #add_customer_content input#search_barangay").val($(this).attr("data-value"));
                $("section.content #add_customer_content input#search_barangay").attr("value", $(this).attr("data-id"));
                //enable find RTA
                $('section.content div#add_customer_content form#add_customer').find('button.find_retail').attr("disabled",false);
            })

            //DTD_address_autocompletes modal
            $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_building").siblings("div.result-list").on('mousedown', 'div.acautocomplete', function () {//put the data on textbox
                $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_building").siblings("div.result-list").html("");
                $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_building").val($(this).attr("data-value"));
                $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_building").attr("value", $(this).attr("data-id"));
            })
            $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_street").siblings("div.result-list").on('mousedown', 'div.acautocomplete', function () {//put the data on textbox
                $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_street").siblings("div.result-list").html("");
                $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_street").val($(this).attr("data-value"));
                $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_street").attr("value", $(this).attr("data-id"));
            })
            $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_second_street").siblings("div.result-list").on('mousedown', 'div.acautocomplete', function () {//put the data on textbox
                $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_second_street").siblings("div.result-list").html("");
                $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_second_street").val($(this).attr("data-value"));
                $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_second_street").attr("value", $(this).attr("data-id"));
            })
            $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_subdivision").siblings("div.result-list").on('mousedown', 'div.acautocomplete', function () {//put the data on textbox
                $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_subdivision").siblings("div.result-list").html("");
                $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_subdivision").val($(this).attr("data-value"));
                $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_subdivision").attr("value", $(this).attr("data-id"));
            })
            $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_barangay").siblings("div.result-list").on('mousedown', 'div.acautocomplete', function () {//put the data on textbox
                $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_barangay").siblings("div.result-list").html("");
                $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_barangay").val($(this).attr("data-value"));
                $("div[modal-id='deliver-to-different'] form#add_address input#modal_search_barangay").attr("value", $(this).attr("data-id"));
            })

            //Coordinator_DTD_address_autocompletes modal
            $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_building").siblings("div.result-list").on('mousedown', 'div.acautocomplete', function () {//put the data on textbox
                $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_building").siblings("div.result-list").html("");
                $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_building").val($(this).attr("data-value"));
                $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_building").attr("value", $(this).attr("data-id"));
                /*hidden field for find rta*/
                $("div[modal-id='update-address'] form#coordinator_update_address div.hidden_fields_for_rta").find('input[name="building"][type="hidden"]').val($(this).attr("data-value"));
                $("div[modal-id='update-address'] form#coordinator_update_address").find('div.found-retail').hide();
                $("div[modal-id='update-address'] form#coordinator_update_address").find('div.find-rta-container').show()

            })
            $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_street").siblings("div.result-list").on('mousedown', 'div.acautocomplete', function () {//put the data on textbox
                $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_street").siblings("div.result-list").html("");
                $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_street").val($(this).attr("data-value"));
                $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_street").attr("value", $(this).attr("data-id"));
                /*hidden field for find rta*/
                $("div[modal-id='update-address'] form#coordinator_update_address div.hidden_fields_for_rta").find('input[name="street"][type="hidden"]').val($(this).attr("data-id"));
                $("div[modal-id='update-address'] form#coordinator_update_address").find('div.found-retail').hide();
                $("div[modal-id='update-address'] form#coordinator_update_address").find('div.find-rta-container').show()
            })
            $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_second_street").siblings("div.result-list").on('mousedown', 'div.acautocomplete', function () {//put the data on textbox
                $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_second_street").siblings("div.result-list").html("");
                $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_second_street").val($(this).attr("data-value"));
                $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_second_street").attr("value", $(this).attr("data-id"));
                /*hidden field for find rta*/
                $("div[modal-id='update-address'] form#coordinator_update_address div.hidden_fields_for_rta").find('input[name="street"][type="hidden"]').val($(this).attr("data-id"));
                $("div[modal-id='update-address'] form#coordinator_update_address").find('div.found-retail').hide();
                $("div[modal-id='update-address'] form#coordinator_update_address").find('div.find-rta-container').show()
            })
            $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_subdivision").siblings("div.result-list").on('mousedown', 'div.acautocomplete', function () {//put the data on textbox
                $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_subdivision").siblings("div.result-list").html("");
                $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_subdivision").val($(this).attr("data-value"));
                $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_subdivision").attr("value", $(this).attr("data-id"));
                /*hidden field for find rta*/
                $("div[modal-id='update-address'] form#coordinator_update_address div.hidden_fields_for_rta").find('input[name="subdivision"][type="hidden"]').val($(this).attr("data-id"));
                $("div[modal-id='update-address'] form#coordinator_update_address").find('div.found-retail').hide();
                $("div[modal-id='update-address'] form#coordinator_update_address").find('div.find-rta-container').show()
            })
            $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_barangay").siblings("div.result-list").on('mousedown', 'div.acautocomplete', function () {//put the data on textbox
                $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_barangay").siblings("div.result-list").html("");
                $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_barangay").val($(this).attr("data-value"));
                $("div[modal-id='update-address'] form#coordinator_update_address input#coordinator_modal_search_barangay").attr("value", $(this).attr("data-id"));
                /*hidden field for find rta*/
                $("div[modal-id='update-address'] form#coordinator_update_address div.hidden_fields_for_rta").find('input[name="barangay"][type="hidden"]').val($(this).attr("data-id"));
                $("div[modal-id='update-address'] form#coordinator_update_address").find('div.found-retail').hide();
                $("div[modal-id='update-address'] form#coordinator_update_address").find('div.find-rta-container').show()
            })

            //Coordinator_PO_address_autocompletes modal
            $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_building").siblings("div.result-list").on('mousedown', 'div.acautocomplete', function () {//put the data on textbox
                $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_building").siblings("div.result-list").html("");
                $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_building").val($(this).attr("data-value"));
                $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_building").attr("value", $(this).attr("data-id"));
                /*hidden field for find rta*/
                $("div[modal-id='process-order'] form#coordinator_process_order_address div.hidden_fields_for_rta").find('input[name="building"][type="hidden"]').val($(this).attr("data-value"));
                $("div[modal-id='process-order'] form#coordinator_process_order_address").find('div.found-retail').hide();
                $("div[modal-id='process-order'] form#coordinator_process_order_address").find('div.find-rta-container').show()

            })
            $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_street").siblings("div.result-list").on('mousedown', 'div.acautocomplete', function () {//put the data on textbox
                $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_street").siblings("div.result-list").html("");
                $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_street").val($(this).attr("data-value"));
                $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_street").attr("value", $(this).attr("data-id"));
                /*hidden field for find rta*/
                $("div[modal-id='process-order'] form#coordinator_process_order_address div.hidden_fields_for_rta").find('input[name="street"][type="hidden"]').val($(this).attr("data-value"));
                $("div[modal-id='process-order'] form#coordinator_process_order_address").find('div.found-retail').hide();
                $("div[modal-id='process-order'] form#coordinator_process_order_address").find('div.find-rta-container').show()
            })
            $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_second_street").siblings("div.result-list").on('mousedown', 'div.acautocomplete', function () {//put the data on textbox
                $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_second_street").siblings("div.result-list").html("");
                $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_second_street").val($(this).attr("data-value"));
                $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_second_street").attr("value", $(this).attr("data-id"));
                /*hidden field for find rta*/
                $("div[modal-id='process-order'] form#coordinator_process_order_address div.hidden_fields_for_rta").find('input[name="street"][type="hidden"]').val($(this).attr("data-value"));
                $("div[modal-id='process-order'] form#coordinator_process_order_address").find('div.found-retail').hide();
                $("div[modal-id='process-order'] form#coordinator_process_order_address").find('div.find-rta-container').show()
            })
            $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_subdivision").siblings("div.result-list").on('mousedown', 'div.acautocomplete', function () {//put the data on textbox
                $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_subdivision").siblings("div.result-list").html("");
                $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_subdivision").val($(this).attr("data-value"));
                $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_subdivision").attr("value", $(this).attr("data-id"));
                /*hidden field for find rta*/
                $("div[modal-id='process-order'] form#coordinator_process_order_address div.hidden_fields_for_rta").find('input[name="subdivision"][type="hidden"]').val($(this).attr("data-value"));
                $("div[modal-id='process-order'] form#coordinator_process_order_address").find('div.found-retail').hide();
                $("div[modal-id='process-order'] form#coordinator_process_order_address").find('div.find-rta-container').show()
            })
            $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_barangay").siblings("div.result-list").on('mousedown', 'div.acautocomplete', function () {//put the data on textbox
                $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_barangay").siblings("div.result-list").html("");
                $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_barangay").val($(this).attr("data-value"));
                $("div[modal-id='process-order'] form#coordinator_process_order_address input#coordinator_po_modal_search_barangay").attr("value", $(this).attr("data-id"));
                /*hidden field for find rta*/
                $("div[modal-id='process-order'] form#coordinator_process_order_address div.hidden_fields_for_rta").find('input[name="barangay"][type="hidden"]').val($(this).attr("data-value"));
                $("div[modal-id='process-order'] form#coordinator_process_order_address").find('div.found-retail').hide();
                $("div[modal-id='process-order'] form#coordinator_process_order_address").find('div.find-rta-container').show()
            })

            $('body').on('click', function () {
                $("section.content #add_customer_content div.result-list").html("");
                $("div[modal-id='deliver-to-different'] form#add_address div.result-list").html("");
                $("div[modal-id='update-address'] form#coordinator_update_address div.result-list").html("");
                $("div[modal-id='process-order'] form#coordinator_process_order_address div.result-list").html("");
            });

            $("select").transformDD();

            $("#search_customer").search_autocomplete({
                "ui"      : $("#search_customer"),
                "data"    : oAutocompleteParams,
                "url"     : callcenter.config('url.api.jfc.customers') + "/customers/search",
                "callback": function (oData) {
                    if (typeof(oData) !== 'undefined') {
                        var uiContainer = this.ui.next('div.result-list:first');
                        uiContainer.find('div.list').remove();
                        if (count(oData.data) > 0) {
                            $.each(oData.data, function (key, value) {
                                uiContainer.append(
                                    '<div class="list" data-first_name="' + value.first_name + '" data-last_name="' + value.last_name + '" data-middle_name="' + value.middle_name + '">' + value.first_name + ' ' + value.middle_name + ' ' + value.last_name + '</div>'
                                );
                            });
                        }
                    }
                }


            });

            $('#main_nav_headers').on('click', 'li.add_customer', function () {
                callcenter.call_orders.content_panel_toggle('add_customer');
                $('section.headers').hide();
                $('section#search_order_form_container').hide();
                $('section#add_new_customer_header').removeClass('hidden').show();
                $("section.content #add_customer_content").find('div#city_select div.select input.dd-txt[name="city"]').attr({'datavalid':'required', 'labelinput':'City'});
                $("section.content #add_customer_content").find('div#province_select div.select input.dd-txt[name="province"]').attr({'datavalid':'required', 'labelinput':'Province'});
                // $("section.content #add_customer_content").find('div.select input.dd-txt[name="address_type"]').attr({'datavalid':'required', 'labelinput':'Address Type'});
                $('section#search_order_list_container').addClass('hidden');

                //clearing form values
                $('section#add_new_customer_header input#search_customer').val("");
                $('section#search_order_form_container form#search_order_form_params input[name="search_string"]').val("");
                $('section#customer_list_header input#search_customer_list').val("");

                //clearing results
                $('section.content div#customer_list_content').addClass('hidden');
            })

            $('#main_nav_headers').on('click', 'li.search_order', function () {
                callcenter.call_orders.content_panel_toggle('search_order');
                $('section.headers').hide();
                $('section#search_order_form_container').removeClass("hidden").show();
                $('section#search_order_list_container').removeClass("hidden").show();
                $('section#search_order_list_container div.search_result_container').removeClass("hidden").show();
                $('section#search_order_form_container').find("button.archive").addClass("hidden");
                //clearing form values
                $('section#add_new_customer_header input#search_customer').val("");
                $('section#search_order_form_container form#search_order_form_params input[name="search_string"]').val("");
                $('section#customer_list_header input#search_customer_list').val("");

                //clearing results
                $('section.content div#customer_list_content').addClass('hidden');
            })

            $('#main_nav_headers').on('click', 'li.product_list', function () {
                callcenter.call_orders.content_panel_toggle('product_list');
                $('section.headers').hide();
                $('section#add_new_customer_header').removeClass('hidden').show();
                $('section.content #order_process_content').removeClass("hidden").show();
                $('section#search_order_list_container').addClass('hidden');
                $('section#search_order_form_container').addClass('hidden');
                //clearing form values
                $('section#add_new_customer_header input#search_customer').val("");
                $('section#search_order_form_container form#search_order_form_params input[name="search_string"]').val("");
                $('section#customer_list_header input#search_customer_list').val("");

                //clearing results
                $('section.content div#customer_list_content').addClass('hidden');
            })

            $('section.content #add_customer_content').on('click', 'button.rta_proceed', function () {
               //$('section.content #add_customer_content form#add_customer').submit();
//                 var isCustomerSaved = callcenter.get_from_storage("isCustomerSaved", '');
//                 alert(JSON.stringify(callcenter.get_from_storage("isCustomerSaved", '')))
                if(typeof(callcenter.get_from_storage("isCustomerSaved", '')) == 'undefined' || callcenter.get_from_storage("isCustomerSaved", '').length == 0)
                {
                    $('section.content #add_customer_content button.save_customer').trigger('click');
                }

                var uiThis = $(this);

                setTimeout(function(uiThis){
                
                var isCustomerSaved = callcenter.get_from_storage("isCustomerSaved", '');
                //alert(typeof(isCustomerSaved))
                //alert(isCustomerSaved.length)
                if(typeof(isCustomerSaved) != 'undefined')
                {
                    //alert(isCustomerSaved[0][0].isCustomerSaved);
                    if(isCustomerSaved[0][0].isCustomerSaved == 'true')
                    {
                        var uiContainer = uiThis.parents('form#add_customer:first');
                        var uiRTA = uiContainer.find('div.found-retail').find('span.retail-store-name');

                        var iStoreCode = (typeof(uiRTA.attr('store_code')) !== 'undefined') ? uiRTA.attr('store_code') : 0;
                        var iStoreID = (typeof(uiRTA.attr('store_id')) !== 'undefined') ? uiRTA.attr('store_id') : 0;
                        var iStoreTime = (typeof(uiRTA.attr('store_time')) !== 'undefined') ? uiRTA.attr('store_time') : 0;
                        var sStoreName = (typeof(uiRTA.attr('store_name')) !== 'undefined') ? uiRTA.attr('store_name') : '';

                        var iAddressID = (typeof(uiThis.attr('address_id'))!== 'undefined' && uiThis.attr('address_id') !== 'false') ? uiThis.attr('address_id') : 0 ;

                        var house_number = (uiContainer.find('input[name="house_number"]').val() !== '') ? uiContainer.find('input[name="house_number"]').val() : '',
                            floor = (uiContainer.find('input[name="floor"]').val() !== '' && uiContainer.find('input[name="floor"]').val() != "0") ? uiContainer.find('input[name="floor"]').val() : '',
                            building = (uiContainer.find('input[name="building"]').val() != 'undefined') ? uiContainer.find('input[name="building"]').val() : '',
                            barangay = (uiContainer.find('input[name="barangay"]').val() != 'undefined') ? uiContainer.find('input[name="barangay"]').val() : '',
                            subdivision = (uiContainer.find('input[name="subdivision"]').val() != 'undefined') ? uiContainer.find('input[name="subdivision"]').val() : '',
                            city = (uiContainer.find('input[name="city"]').val() != 'undefined') ? uiContainer.find('input[name="city"]').val() : '',
                            street = (uiContainer.find('input[name="street"]').val() != 'undefined') ? uiContainer.find('input[name="street"]').val() : '',
                            second_street = (uiContainer.find('input[name="second_street"]').val() != 'undefined') ? uiContainer.find('input[name="second_street"]').val() : '',
                            province = (uiContainer.find('input[name="province"]').val() != 'undefined') ? uiContainer.find('input[name="province"]').val() : '';

                        var sAddressText = house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province;
                        var sAddressLandmark = uiContainer.find('input[name="landmark"]').val();
                        var sAddressType = uiContainer.find('input[name="address_type"]').val();
                        var sImage = '<img alt="work icon" class="info_address_type" width="35px" onError="$.fn.checkImage(this);" height="33px" src="http://localhost/callcenter/assets/images/' + sAddressType + '.png">';
                        var sAddressLabel = uiContainer.find('input[name="address_label"]').val();
                        var sFirstName = uiContainer.find('input[name="first_name"]').val(),
                            sMiddleName = uiContainer.find('input[name="middle_name"]').val(),
                            sLastName = uiContainer.find('input[name="last_name"]').val();
                        var sCustomerName = sFirstName + ' ' + sMiddleName + ' ' + sLastName;
                        var sIsPWD = (typeof(uiContainer.find('input[name="pwd"]:checked').val()) != 'undefined') ? uiContainer.find('input[name="pwd"]:checked').val() : 0 ;
                        var sIsSenior = (typeof(uiContainer.find('input[name="senior"]:checked').val()) != 'undefined') ? uiContainer.find('input[name="senior"]:checked').val() : 0 ;

                        // var sCustomerMobile = (uiContainer.find('div.primary_contact_number input[name="contact_number[]"]').is(':visible') && uiContainer.find('div.primary_contact_number input[name="contact_number[]"]').val() !== '') ? uiContainer.find('div.primary_contact_number input[name="contact_number[]"]').val().split('-').join('') : '';
                        var sCustomerMobile = "";//primary
                        if(uiContainer.find('div.primary_contact_number input.small[name="contact_number[]"]').is(':visible'))//mobile
                        {
                            sCustomerMobile = uiContainer.find('div.primary_contact_number input.small[name="contact_number[]"]:visible').val().split('-').join('');
                        }
                        if(uiContainer.find('div.primary_contact_number input.xsmall[name="contact_number[]"]').is(':visible'))//landline
                        {
                            var sPrimaryContactLandline = "";
                            var uiPrimaryContactLandline = uiContainer.find('div.primary_contact_number input.xsmall[name="contact_number[]"]:visible');
                            if(typeof(uiPrimaryContactLandline)!== 'undefined')
                            {
                                var iCounter = 0;
                                $.each(uiPrimaryContactLandline, function () {
                                    iCounter++;
                                    var sValue = $(this).val().replace(/-/g, '')+'-';
                                    sPrimaryContactLandline += sValue;
                                    if (iCounter == 3) {

                                        if(sPrimaryContactLandline.match(/^\d+-\d+--$/))
                                        {
                                            sPrimaryContactLandline = sPrimaryContactLandline.slice(0,-2);
                                        }
                                        else
                                        {
                                            sPrimaryContactLandline = sPrimaryContactLandline.slice(0,-1);
                                        }
                                        iCounter = 0;
                                    }
                                });
                            }
                            if(typeof(sPrimaryContactLandline) !== 'undefined')
                            {
                                sCustomerMobile = sPrimaryContactLandline;
                            }
                        }

                        // var sCustomerMobileAlternate = (uiContainer.find('div.alternate_contact_container input[name="contact_number[]"]').is(':visible') && uiContainer.find('div.alternate_contact_container input[name="contact_number[]"]').val() !== '') ? uiContainer.find('div.alternate_contact_container input[name="contact_number[]"]').val().split('-').join('') : '';
                        var sCustomerMobileAlternate = "";
                        if(uiContainer.find('div.alternate_contact_container input.small[name="contact_number[]"]').is(':visible'))
                        {
                            sCustomerMobileAlternate = '<p>'+uiContainer.find('div.alternate_contact_container input.small[name="contact_number[]"]:visible').val().split('-').join('')+',</p>';
                        }
                        if(uiContainer.find('div.alternate_contact_container input.xsmall[name="contact_number[]"]').is(':visible'))
                        {
                            var sAlternateContactLandLine = "";
                            var uiAlternateContactLandLine = $.find('section.content #add_customer_content div.new_contact_number input.xsmall[name="contact_number[]"]:visible');
                            if(typeof(uiAlternateContactLandLine) !== 'undefined')
                            {
                                var iCounter = 0;
                                $.each(uiAlternateContactLandLine, function () {
                                    iCounter++;
                                    var sValue = $(this).val().replace(/-/g, '')+'-';
                                    sAlternateContactLandLine += sValue;
                                    if (iCounter == 3) {

                                        sAlternateContactLandLine = sAlternateContactLandLine.replace(/--/g, '-')
                                        if(sAlternateContactLandLine.lastIndexOf("-") != -1)
                                        {
                                            sAlternateContactLandLine = sAlternateContactLandLine.slice(0,-1);
                                        }

                                        sAlternateContactLandLine = '<p>'+sAlternateContactLandLine+',</p>';
                                        iCounter = 0;
                                    }
                                });
                            }
                            if(typeof(sAlternateContactLandLine) !== 'undefined')
                            {
                                sCustomerMobileAlternate += sAlternateContactLandLine;
                            }
                        }


                        var isHappyPlusAdded = callcenter.get_from_storage("happy_plus_cards", '');
                        var sHappyPlusData = [];
                        if(typeof(isHappyPlusAdded) != 'undefined')
                        {
                            if(isHappyPlusAdded[0].length > 0)
                            {
                                $.each(isHappyPlusAdded[0], function (key, value){
                                    sHappyPlusData.push({'id' : value.happy_plus_cards.data.happy_plus_id, 'number' : value.happy_plus_cards.here.number, 'date' : value.happy_plus_cards.here.expiration_date_date, 'remarks' : value.happy_plus_cards.here.remarks})
                                })
                            }
                        }

                        var iCustomerID = uiThis.attr('customer_id');
                        var iOrderType = uiThis.attr('data-order-rta');
                        var oOrderInfo = {
                            'address_id'        : iAddressID,
                            'address_text'      : sAddressText,
                            'address_landmark'  : sAddressLandmark,
                            'address_label'     : sAddressLabel,
                            'address_type_image': sImage,
                            'customer_full_name': sCustomerName,
                            'customer_mobile'   : sCustomerMobile,
                            'customer_mobile_alternate': sCustomerMobileAlternate,
                            'customer_id'       : iCustomerID,
                            'store_code'        : iStoreCode,
                            'store_id'          : iStoreID,
                            'store_time'        : iStoreTime,
                            'store_name'        : sStoreName,
                            'house_number'      : ((typeof(house_number) !== 'undefined') ? house_number : ''),
                            'building'          : ((typeof(building) !== 'undefined') ? building : ''),
                            'floor'             : ((typeof(floor) !== 'undefined') ? floor : ''),
                            'street'            : ((typeof(street) !== 'undefined') ? street : ''),
                            'second_street'     : ((typeof(second_street) !== 'undefined') ? second_street : ''),
                            'barangay'          : ((typeof(barangay) !== 'undefined') ? barangay : ''),
                            'subdivision'       : ((typeof(subdivision) !== 'undefined') ? subdivision : ''),
                            'city'              : ((typeof(city) !== 'undefined') ? city : ''),
                            'province'          : ((typeof(province) !== 'undefined') ? province : ''),
                            'order_type'        : iOrderType,
                            'hpc_data'          : sHappyPlusData,
                            'is_pwd'            : sIsPWD,
                            'is_senior'         : sIsSenior
                        }

                        //console.log(oOrderInfo)
                        callcenter.order_process.populate_order_information(oOrderInfo);
                        callcenter.call_orders.content_panel_toggle('order_process');
                        callcenter.order_process.identify_order_rta(uiThis.attr('data-order-rta'));
                        callcenter.pop("isCustomerSaved");
                        if(typeof(isHappyPlusAdded) != 'undefined')
                        {
                            callcenter.pop("happy_plus_cards");
                        }
                    }
                    else
                    {
                        $('section.content #add_customer_content button.save_customer').trigger('click');
                    }
                }
                else
                {
                    $('section.content #add_customer_content button.save_customer').trigger('click');
                }
                }, 1500, uiThis)
                


            })


            $("section.content #search_customer_content").on('click', 'div.arrow-right', function () {
                if ($(this).parents('div.search_result_container:first').find("div.order_history_container div:visible").next().length != 0) {
                    $(this).parents('div.search_result_container:first').find("div.order_history_container div:visible").next().show().prev().hide();
                }
                else {
                    $(this).parents('div.search_result_container:first').find("div.order_history_container div:visible").hide();
                    $(this).parents('div.search_result_container:first').find("div.order_history_container div:first").show();
                }
                var uiShown = $("section.content #search_customer_content div.order_history_container div:visible");
                var iOrderID = uiShown.attr('data-order-id');
                var sDate = uiShown.parents('div.search_result_container:first').find('div.order_date_option.option[data-value="' + iOrderID + '"]:first').text();
                uiShown.parents('div.search_result_container:first').find('div.frm-custom-dropdown-txt').find('input[name="date-selected"]').val(sDate);
                return false;
            });

            $("section.content #search_customer_content").on('click', 'div.arrow-left', function () {
                if ($(this).parents('div.search_result_container:first').find("div.order_history_container div:visible").prev().length != 0)
                    $(this).parents('div.search_result_container:first').find("div.order_history_container div:visible").prev().show().next().hide();
                else {
                    $(this).parents('div.search_result_container:first').find("div.order_history_container div:visible").hide();
                    $(this).parents('div.search_result_container:first').find("div.order_history_container div:last").show();

                }
                var uiShown = $("section.content #search_customer_content div.order_history_container div:visible");
                var iOrderID = uiShown.attr('data-order-id');
                var sDate = uiShown.parents('div.search_result_container:first').find('div.order_date_option.option[data-value="' + iOrderID + '"]:first').text();
                uiShown.parents('div.search_result_container:first').find('div.frm-custom-dropdown-txt').find('input[name="date-selected"]').val(sDate);
                return false;
            });

            $('section.content #search_customer_content').on('click', 'div.order_date_option', function () {
                var iOrderID = $(this).attr('data-value');
                $('section.content #search_customer_content').find('div.order_history_container').children('div').hide();
                $('section.content #search_customer_content').find('div.order_history_container').find('div[data-order-id="' + iOrderID + '"]').show();
            })

            $('section.content #search_customer_content').on('click', 'button.btn-update', function (e){
                var oData = $(this).parents('div.search_result_container').attr('data-customer-id');
                var updateClone = $('div.update_customer.template').clone().removeClass('hidden').removeClass('template');
                var uiTemplate = $(this).parents('div.search_result_container div.content-container div.collapsible');
                uiTemplate.children().hide();
                uiTemplate.siblings().hide();

                if(uiTemplate.find('div.update_customer').length)
                {
                    uiTemplate.find('div.update_customer').remove();
                }
                
                uiTemplate.append(updateClone);
                callcenter.call_orders.assemble_update_form(oData, uiTemplate, updateClone);
            });

            $('section.content #search_customer_content').on('click', 'button.btn-cancel', function (e){
                e.preventDefault();
                var oData = $(this).parents('div.search_result_container').attr('data-customer-id');
                var updateClone = $('div.update_customer').clone().removeClass('hidden').removeClass('template');
                var uiTemplate = $(this).parents('div.search_result_container div.content-container div.collapsible');
                uiTemplate.find('div.second_header').show();
                uiTemplate.find('div.second_header').siblings().not(updateClone).show();
                uiTemplate.find('div.success-msg').hide();
                uiTemplate.find('div.error-msg').hide();
                uiTemplate.find('div.update_customer').hide();
                
                var oParam = {
                    "params": {
                        "where": [
                            {
                                "field"   : 'c.id',
                                "operator": "=",
                                "value"   : oData
                            }
                        ]
                    }
                }
                var oConfig = {
                    "type"   : "Get",
                    "data"   : oParam,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.customers') + "/customers/get",
                    "success": function (oData) {
                        if(typeof(oData) !== 'undefined')
                        {
                            $.each(oData.data, function (key, value) {
                                /*address*/
                                if (count(value.address) > 0 && count(value.address_text) > 0) {
                                    uiTemplate.find('div.customer_address_container').find('div.customer_address:not(.template)').remove();
                                    uiTemplate.find('div.customer_secondary_address').find('div.deliver-to-different').siblings().remove();
                                    if($('div.search_result_container[data-customer-id="' + value.id + '"]').find('div.customer_secondary_address').is(':visible'))
                                    {
                                        $('div.search_result_container[data-customer-id="' + value.id + '"]').find('div.customer_secondary_address').toggle();
                                    }
                                    
                                    $.each(value.address, function (k, v) {

                                        var house_number = (v.house_number !== '' && v.house_number != "0") ? v.house_number : '',
                                            floor = (v.floor !== '' && v.floor != "0") ? v.floor : '',
                                            building = (value.address_text[k].building !== null) ? value.address_text[k].building.name : '',
                                            barangay = (value.address_text[k].barangay !== null) ? value.address_text[k].barangay.name : '',
                                            subdivision = (value.address_text[k].subdivision !== null) ? value.address_text[k].subdivision.name : '',
                                            city = (value.address_text[k].city !== null) ? value.address_text[k].city.name : '',
                                            street = (value.address_text[k].street !== null) ? value.address_text[k].street.name : '',
                                            second_street = (value.address_text[k].second_street !== null && typeof(value.address_text[k].second_street) != 'undefined') ? value.address_text[k].second_street.name : '',
                                            province = (value.address_text[k].province !== null) ? value.address_text[k].province.name : '';

                                        var uiAddressTemplate = uiTemplate.find('div.customer_address.template').clone().removeClass('template');

                                        uiAddressTemplate.find('strong.info_address_label').text(v.address_label);
                                        uiAddressTemplate.find('p.info_address_complete').text('' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' '+ second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);
                                        uiAddressTemplate.find('p.info_address_complete').attr('data-address', '/##/' + house_number + '/##/' + building + '/##/' + floor + '/##/' + street + '/##/' + second_street + '/##/' + barangay + '/##/' + subdivision + '/##/' + city + '/##/' + province);
                                        uiAddressTemplate.find('strong.info_address_type').text(v.address_type.toUpperCase());
                                        uiAddressTemplate.find('p.info_address_landmark').text('- ' + v.landmark.toUpperCase());

                                        uiAddressTemplate.attr('customer_id', value.id);
                                        uiAddressTemplate.attr('address_id', v.id);
                                        uiAddressTemplate.attr('house_number', v.house_number);
                                        uiAddressTemplate.attr('building', building);
                                        uiAddressTemplate.attr('floor', v.floor);
                                        uiAddressTemplate.attr('street', v.street);
                                        uiAddressTemplate.attr('second_street', v.second_street);
                                        uiAddressTemplate.attr('barangay', v.barangay);
                                        uiAddressTemplate.attr('subdivision', v.subdivision);
                                        uiAddressTemplate.attr('city', v.city);
                                        uiAddressTemplate.attr('province', v.province);
                                        uiAddressTemplate.attr('address_label', v.address_label);
                                        uiAddressTemplate.attr('address_type', v.address_type);
                                        uiAddressTemplate.attr('all_address', '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);

                                        if (v.is_default == 1) {
                                            // uiAddressTemplate.attr('address_id', v.id);
                                            // uiTemplate.siblings('div.header-search').find('div.remarks_and_address p.address').empty();
                                            // uiTemplate.siblings('div.header-search').find('div.remarks_and_address p.address').append("<span class='red-color info_address'><strong class=''>Address: </strong></span>" + '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);

                                            // /* hidden fields for find rta */
                                            // uiTemplate.siblings('div.header-search').find('input[name="building"]').val(building);
                                            // uiTemplate.siblings('div.header-search').find('input[name="street"]').val(v.street);
                                            // uiTemplate.siblings('div.header-search').find('input[name="barangay"]').val(v.barangay);
                                            // uiTemplate.siblings('div.header-search').find('input[name="subdivision"]').val(v.subdivision);
                                            // uiTemplate.siblings('div.header-search').find('input[name="city"]').val(v.city);
                                            // uiTemplate.siblings('div.header-search').find('input[name="province"]').val(v.province);

                                            // callcenter.call_orders.clone_append(uiAddressTemplate, uiTemplate.find('div.customer_address_container'));

                                            if(v.is_deleted_date != '0000-00-00 00:00:00' && v.is_deleted == 1)
                                            {
                                                var dateNow = moment().format("YYYY-MM-DD"),
                                                    timeNow = moment().format("HH:mm:ss"),
                                                    is_deleted_fulldate = v.is_deleted_date.split(' '),
                                                    is_deleted_date = is_deleted_fulldate[0].split('-'),
                                                    is_deleted_time = is_deleted_fulldate[1].split(':'),
                                                    time_now = timeNow.split(':');

                                                /*deleted time*/
                                                var is_deleted_hour = is_deleted_time[0],
                                                    is_deleted_min = is_deleted_time[1],
                                                    is_deleted_sec = is_deleted_time[2];

                                                /*time now*/
                                                var hour_now = time_now[0],
                                                    min_now = time_now[1],
                                                    sec_now = time_now[2];

                                                if(dateNow == is_deleted_fulldate[0])
                                                {
                                                    if( ( parseInt(hour_now) - parseInt(is_deleted_hour) ) > parseInt('1') || ( parseInt(hour_now) - parseInt(is_deleted_hour) ) == parseInt('1') && parseInt(is_deleted_min) <= parseInt(min_now) )
                                                    {
                                                        // console.log("remove/hide address")
                                                    }
                                                    else
                                                    {
                                                        // console.log("undo");
                                                        uiAddressTemplate.attr('address_id', v.id);
                                                        uiTemplate.siblings('div.header-search').find('div.remarks_and_address p.address').empty();
                                                        uiTemplate.siblings('div.header-search').find('div.remarks_and_address p.address').append("<span class='red-color info_address'><strong class=''>Address: </strong></span>" + '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);

                                                        /* hidden fields for find rta */
                                                        uiTemplate.siblings('div.header-search').find('input[name="building"]').val(building);
                                                        uiTemplate.siblings('div.header-search').find('input[name="street"]').val(v.street);
                                                        uiTemplate.siblings('div.header-search').find('input[name="barangay"]').val(v.barangay);
                                                        uiTemplate.siblings('div.header-search').find('input[name="subdivision"]').val(v.subdivision);
                                                        uiTemplate.siblings('div.header-search').find('input[name="city"]').val(v.city);
                                                        uiTemplate.siblings('div.header-search').find('input[name="province"]').val(v.province);

                                                        callcenter.call_orders.clone_append(uiAddressTemplate, uiTemplate.find('div.customer_address_container'));
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                uiAddressTemplate.attr('address_id', v.id);
                                                uiTemplate.siblings('div.header-search').find('div.remarks_and_address p.address').empty();
                                                uiTemplate.siblings('div.header-search').find('div.remarks_and_address p.address').append("<span class='red-color info_address'><strong class=''>Address: </strong></span>" + '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);

                                                /* hidden fields for find rta */
                                                uiTemplate.siblings('div.header-search').find('input[name="building"]').val(building);
                                                uiTemplate.siblings('div.header-search').find('input[name="street"]').val(v.street);
                                                uiTemplate.siblings('div.header-search').find('input[name="barangay"]').val(v.barangay);
                                                uiTemplate.siblings('div.header-search').find('input[name="subdivision"]').val(v.subdivision);
                                                uiTemplate.siblings('div.header-search').find('input[name="city"]').val(v.city);
                                                uiTemplate.siblings('div.header-search').find('input[name="province"]').val(v.province);

                                                callcenter.call_orders.clone_append(uiAddressTemplate, uiTemplate.find('div.customer_address_container'));
                                            }
                                        }
                                        else {
                                            // uiAddressTemplate.removeClass('bggray-light').addClass('white');
                                            // uiAddressTemplate.find('a.edit_address').remove();
                                            // uiTemplate.find('div.customer_secondary_address').prepend(uiAddressTemplate);

                                            if(v.is_deleted_date != '0000-00-00 00:00:00' && v.is_deleted == 1)
                                            {
                                                var dateNow = moment().format("YYYY-MM-DD"),
                                                    timeNow = moment().format("HH:mm:ss"),
                                                    is_deleted_fulldate = v.is_deleted_date.split(' '),
                                                    is_deleted_date = is_deleted_fulldate[0].split('-'),
                                                    is_deleted_time = is_deleted_fulldate[1].split(':'),
                                                    time_now = timeNow.split(':');

                                                /*deleted time*/
                                                var is_deleted_hour = is_deleted_time[0],
                                                    is_deleted_min = is_deleted_time[1],
                                                    is_deleted_sec = is_deleted_time[2];

                                                /*time now*/
                                                var hour_now = time_now[0],
                                                    min_now = time_now[1],
                                                    sec_now = time_now[2];

                                                if(dateNow == is_deleted_fulldate[0])
                                                {
                                                    if( ( parseInt(hour_now) - parseInt(is_deleted_hour) ) > parseInt('1') || ( parseInt(hour_now) - parseInt(is_deleted_hour) ) == parseInt('1') && parseInt(is_deleted_min) <= parseInt(min_now) )
                                                    {
                                                        // console.log("remove/hide address")
                                                    }
                                                    else
                                                    {
                                                        // console.log("undo");
                                                        uiAddressTemplate.removeClass('bggray-light').addClass('white');
                                                        uiAddressTemplate.find('a.edit_address').remove();
                                                        uiTemplate.find('div.customer_secondary_address').prepend(uiAddressTemplate);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                uiAddressTemplate.removeClass('bggray-light').addClass('white');
                                                uiAddressTemplate.find('a.edit_address').remove();
                                                uiTemplate.find('div.customer_secondary_address').prepend(uiAddressTemplate);
                                            }
                                        }


                                        if (v.address_type.length > 0) {
                                            uiAddressTemplate.find('img.info_address_type').attr('src', callcenter.config('url.server.base') + 'assets/images/' + v.address_type.toLocaleLowerCase() + '.png');
                                            uiAddressTemplate.find('img.info_address_type').attr('onError', '$.fn.checkImage(this);');
                                        }
                                    })
                                }
                            });
                        }
                    }
                }
                callcenter.customer_list.get(oConfig);
            });

            $('section.content #search_customer_content').on('click', 'a.another_email_address',function (e) {
                var uiTemplate = $(this).parents('div.email_address_container').find('div.template.new_email_address').clone().removeClass('template');
                var uiContainer = $(this).parents('div.email_address_container:first');
                callcenter.call_orders.clone_append(uiTemplate, uiContainer);
            });

            $('section.content #search_customer_content').on('click', 'input[type="radio"][name="senior"]',function (e) {
                if ($(this).val() == "1") {
                    $(this).parents('div.senior_container').find('div.senior_citizen').show();
                    $(this).parents('div.senior_container').find('div.senior_citizen').find("input[type='text'][name='osca_number']").attr({
                        'labelinput': 'Osca Number',
                        'datavalid' : 'required'
                    });
                    $(this).parents('div.senior_container').siblings('div.pwd_container').find('input[id^="pwd2"]').prop('checked', true); //uncheck pwd because their can only be one discount
                }
                else if ($(this).val() == "0") {
                    $(this).parents('div.senior_container').find('div.senior_citizen').hide();
                    $(this).parents('div.senior_container').find('div.senior_citizen').find("input[type='text'][name='osca_number']").removeAttr('datavalid labelinput');
                }
            });

            $('section.content #search_customer_content').on('click', 'input[type="radio"][name="pwd"]', function (e) {  //uncheck senior citizen because their can only be one discount
                if ($(this).val() == "1") {
                    $(this).parents('div.pwd_container').siblings('div.senior_container').find('div.senior_citizen').hide();
                    $(this).parents('div.pwd_container').siblings('div.senior_container').find('div.senior_citizen').find("input[type='text'][name='osca_number']").removeAttr('datavalid labelinput');
                    $(this).parents('div.pwd_container').siblings('div.senior_container').find('input[id^="senior2"]').prop('checked', true);
                }
                else if ($(this).val() == "0") {

                }
            });

            $('section.content #search_customer_content').on('click', '.alternate-contact-number', function (e) {
                $(this).addClass("hidden");
                $(this).parents('div.contact-number-container').siblings('div.alternate_contact_container').removeClass("hidden");
                $(this).parents('div.contact-number-container').siblings('div.alternate_contact_container').find('input[name="contact_number[]"].mobile').attr({'datavalid': 'required', 'labelinput' : 'Contact Number'});
                $(this).parents('div.contact-number-container').siblings('div.alternate_contact_container').find('input[name="contact_number[]"].landline.contact-0').attr({'datavalid': 'required', 'labelinput' : 'Landline Area Code'});
                $(this).parents('div.contact-number-container').siblings('div.alternate_contact_container').find('input[name="contact_number[]"].landline.contact-1').attr({'datavalid': 'required', 'labelinput' : 'Landline Number'});
                // $(this).parents('div.contact-number-container').siblings('div.alternate_contact_container').find('input[name="contact_number[]"].landline.contact-2').attr({'datavalid': /*numeric*/' required', 'labelinput' : 'Landline Number Trunkline'});
                $(this).parents('div.contact-number-container').siblings('div.alternate_contact_container').find("input.small").removeClass("hidden");
            });

            $('section.content #search_customer_content').on('click', 'div.option', function (e) {//selection_of_mobile_and_landline
                $(this).closest("section.content #search_customer_content div.frm-custom-dropdown").find("input.dd-txt").attr("value", $(this).html());
                if ($(this).closest("section.content #search_customer_content div.frm-custom-dropdown").find("input.dd-txt").attr("value") == "Mobile") {//mobile selected
                    $(this).closest("section.content #search_customer_content div.contact-number-container").find("input.small").removeClass("hidden");
                    $(this).closest("section.content #search_customer_content div.contact-number-container").find("input.small").attr("datavalid", "required");
                    $(this).closest("section.content #search_customer_content div.contact-number-container").find("input.xsmall").addClass("hidden");
                    $(this).closest("section.content #search_customer_content div.contact-number-container").find("input.xsmall").removeAttr("datavalid");
                } else {//landline selected
                    $(this).closest("section.content #search_customer_content div.contact-number-container").find("input.small").addClass("hidden");
                    $(this).closest("section.content #search_customer_content div.contact-number-container").find("input.small").removeAttr("datavalid");
                    $(this).closest("section.content #search_customer_content div.contact-number-container").find("input.xsmall").removeClass("hidden");
                    $(this).closest("section.content #search_customer_content div.contact-number-container").find("input.xsmall:not('.contact-2')").attr("datavalid", /*"numeric*/ "required");
                }
            });

            $('section.content #search_customer_content').on('click', ' a.another-contact-number', function (e) {
                var uiTemplate = $(this).parents('div.alternate_contact_container').find('div.template.new_contact_number').clone().removeClass('template');
                var uiContainer = $(this).parents('div.alternate_contact_container:first');
                uiTemplate.find('input[name="contact_number[]"].mobile').val('').removeAttr('disabled').attr({'datavalid': /*numeric*/' required', 'labelinput' : 'Contact Number'});
                uiTemplate.find('input[name="contact_number[]"].landline').val('').removeAttr('disabled');
                uiTemplate.find('input[name="contact_number[]"].landline.contact-0').val('').removeAttr('disabled').attr({'datavalid': 'required', 'labelinput' : 'Landline Area Code'});
                uiTemplate.find('input[name="contact_number[]"].landline.contact-1').val('').removeAttr('disabled').attr({'datavalid': 'required', 'labelinput' : 'Landline Number'});
                // uiTemplate.find('input[name="contact_number[]"].landline.contact-2').val('').removeAttr('disabled').attr({'datavalid': 'required', 'labelinput' : 'Landline Number Trunkline'});
                uiTemplate.find('input[name="contact_number_type[]"]').val('Mobile').removeAttr('disabled').attr('value', 'Mobile');
                callcenter.call_orders.clone_append(uiTemplate, uiContainer);
                callcenter.call_orders.get_province_identifier();
            });

            $('section.content #search_customer_content').on('click', 'div.secondary_address_container_update button.set_default_address', function (e) {
                var uiThis = $(this);

                var oParams = {
                    "params": {
                        "address_id" : uiThis.parents('div.customer_list_default_address_update').attr('address_id'),
                        "customer_id": uiThis.parents('div.customer_list_default_address_update').attr('customer_id')
                    }
                }

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.customers') + "/customers/set_default_address",
                    "success": function (data) {
                        if(typeof(data) !== 'undefined')
                        {
                            var uiParent = uiThis.parents('div.secondary_address_container_update:first');
                            var newAddress = uiThis.parents('div.customer_list_default_address_update');
                            var oOldPrimaryAddress = uiThis.parents('div.secondary_address_container_update').prev('div.default_address_container_update').find('div.customer_list_default_address_update:visible').clone();
                            newAddress.find('button.set_default_address').attr('disabled', '')
                            newAddress.find('strong.default_address_label').html(newAddress.attr('address_label')+' - <span class="red-color">Default</span>');
                            uiThis.parents('div.secondary_address_container_update').prev('div.default_address_container_update').find('div.customer_list_default_address_update:visible').replaceWith(newAddress.addClass('bggray-light').addClass('white'));
                            oOldPrimaryAddress.find('button.set_default_address').removeAttr('disabled');
                            // oOldPrimaryAddress.removeClass('bggray-light').addClass('white');
                            oOldPrimaryAddress.find('strong.default_address_label').empty().html(oOldPrimaryAddress.attr('address_label'));
                            oOldPrimaryAddress.attr('is_default', 0)
                            newAddress.attr('is_default', 1)
                            uiParent.prepend(oOldPrimaryAddress);
                        }

                    }
                };

                callcenter.call_orders.get(oAjaxConfig);
            });

            $('section.content #search_customer_content').on('click', 'a.another_happy_plus', function (e) {
                var uiTemplate = $(this).parents('div.parent_happy_plus').find('div.template.new_happy_plus_card').clone().removeClass('template').show();
                var happy_plus_quantity = $(this).parents('div.parent_happy_plus').find('div.happy_plus_card:visible').length + 1;
                uiTemplate.find('input[name="is_smudged"]').attr('id', 'card-number-' + happy_plus_quantity);
                uiTemplate.find('label.chck-lbl').attr('for', 'card-number-' + happy_plus_quantity);
                uiTemplate.find("input[type='text'][name='number']").attr({'labelinput': 'Card Number','datavalid' : 'required'});
                uiTemplate.find("input[type='text'][name='expiry_date']").attr({'labelinput': 'Expiry Date','datavalid' : 'required'});
                var uiContainer = $(this).parents('div.parent_happy_plus:first');

                callcenter.call_orders.clone_append(uiTemplate, uiContainer);

                $(".date-picker").datetimepicker({pickTime: false});
            });

            $('section.content #search_customer_content').on('click', 'input[type="radio"][name="happyplus"]', function () { //hide/show of happy plus card
                if ($(this).val() == "1")
                { //if yes, show and add a require validation
                    $(this).parents('div.parent_happy_plus').find('div.happy_plus_card:not(.template)').show();
                    $(this).parents('div.parent_happy_plus').find('div.happy_plus_card:not(.template)').first().find('a.alternate_happy_plus').removeClass("hidden");
                    $(this).parents('div.parent_happy_plus').find('div.happy_plus_card:not(.template)').find("input[type='text'][name='number']").attr({'labelinput': 'Card Number','datavalid' : 'required'});
                    $(this).parents('div.parent_happy_plus').find('div.happy_plus_card:not(.template)').find("input[type='text'][name='expiry_date']").attr({'labelinput': 'Expiry Date','datavalid' : 'required'});
                    if($(this).parents('div.parent_happy_plus').find('div.happy_plus_card:not(.template):visible').length > 1)
                    {
                        $(this).parents('div.parent_happy_plus').find('a.alternate_happy_plus').addClass("hidden");
                    }
                    if($(this).parents('div.parent_happy_plus').find('div.happy_plus_card:not(.template):visible').length > 2)
                    {
                        $(this).parents('div.parent_happy_plus').find('a.another_happy_plus').removeClass("margin-bottom-15");
                    }
                }
                else if ($(this).val() == "0")
                { //if no, it will hide and remove the validation
                    $(this).parents('div.parent_happy_plus').find('div.happy_plus_card').hide();
                    $(this).parents('div.parent_happy_plus').find('div.happy_plus_card:not(.template)').find("input[type='text'][name='number']").removeAttr('labelinput datavalid');
                    $(this).parents('div.parent_happy_plus').find('div.happy_plus_card:not(.template)').find("input[type='text'][name='expiry_date']").removeAttr('labelinput datavalid');
                }
            });

            $('section.content #search_customer_content').on('click', 'a.alternate_happy_plus', function (e) {
                $(this).addClass("hidden");
                $(this).parents('div.parent_happy_plus').find('div.another_happy_plus').removeClass("hidden").show();
            });

            $('section.content #search_customer_content').on('click', 'button.btn-save', function(){
                $(this).parents('#update-customer').submit();
            })

            $('section.content #search_customer_content').on('submit', '#update-customer',function (e) {
                // $('div.error-msg').hide();
                // $('div.success-msg').hide();
                // $('p.error_messages').remove();
                e.preventDefault();
            });

            $('section.content #search_customer_content').on('click', 'a[modal-target="deliver-to-different"]', function () {
                $(this).parents().find('div.modal-error-msg p.error_messages').remove();
                $(this).parents().find('div.modal-error-msg').hide();
                $('form#add_address').find('input').removeClass('error');
                $('form#add_address').find('input').val('');
                $('form#add_address').find('input[name="is_default"]').prop('checked', false);
                $('form#add_address').find('div.select').removeClass('has-error');

                $('div[modal-id="deliver-to-different"]').find('button.update_address_button').removeClass("update_address_button").addClass("add_address_button");

                var iCustomerID = $(this).parents('div.search_result_container:first').attr('data-customer-id');
                $('form#add_address').find('input[name="customer_id"]').val(iCustomerID);

                // $('form#add_address').find('input.dd-txt[name="address_type"]').attr({'datavalid': 'required', 'labelinput':'Address Type'});
                $('form#add_address').find('input.dd-txt[name="province"]').attr({'datavalid': 'required', 'labelinput':'Province'});
                $('form#add_address').find('input.dd-txt[name="city"]').attr({'datavalid': 'required', 'labelinput':'City'});
            });

            $('section.content #search_customer_content').on('click', 'button.update_address[modal-target="deliver-to-different"]', function () {
                $(this).parents().find('div.modal-error-msg p.error_messages').remove();
                $(this).parents().find('div.modal-error-msg').hide();
                var uiModal = $('div[modal-id="deliver-to-different"] form#add_address');
                uiModal.find('input').val('');
                uiModal.find('input[name="is_default"]').prop('checked', false);
                uiModal.find('input').removeClass('error');
                uiModal.find('div.select').removeClass('has-error');
                
                var iCustomerID = $(this).parents('div.search_result_container:first').attr('data-customer-id');
                uiModal.find('input[name="customer_id"]').val(iCustomerID);

                $('div[modal-id="deliver-to-different"]').find('button.add_address_button').removeClass("add_address_button").addClass("update_address_button");

                // $('form#add_address').find('input.dd-txt[name="address_type"]').attr({'datavalid': 'required', 'labelinput':'Address Type'});
                uiModal.find('input.dd-txt[name="province"]').attr({'datavalid': 'required', 'labelinput':'Province'});
                uiModal.find('input.dd-txt[name="city"]').attr({'datavalid': 'required', 'labelinput':'City'});

                var address_id = $(this).parents('div.customer_list_address_update:first').attr('address_id'),

                    house_number_text = $(this).parents('div.customer_list_address_update:first').attr('house_number_text'),
                    building_text = $(this).parents('div.customer_list_address_update:first').attr('building_text'),
                    floor_text = $(this).parents('div.customer_list_address_update:first').attr('floor_text'),
                    street_text = $(this).parents('div.customer_list_address_update:first').attr('street_text'),
                    second_street_text = $(this).parents('div.customer_list_address_update:first').attr('second_street_text'),
                    barangay_text = $(this).parents('div.customer_list_address_update:first').attr('barangay_text'),
                    subdivision_text = $(this).parents('div.customer_list_address_update:first').attr('subdivision_text'),
                    city_text = $(this).parents('div.customer_list_address_update:first').attr('city_text'),
                    province_text = $(this).parents('div.customer_list_address_update:first').attr('province_text'),
                    address_label_text = $(this).parents('div.customer_list_address_update:first').attr('address_label_text'),
                    address_type_text = $(this).parents('div.customer_list_address_update:first').attr('address_type_text'),
                    landmark_text = $(this).parents('div.customer_list_address_update:first').attr('landmark_text'),

                    house_number = $(this).parents('div.customer_list_address_update:first').attr('house_number'),
                    building = $(this).parents('div.customer_list_address_update:first').attr('building'),
                    floor = $(this).parents('div.customer_list_address_update:first').attr('floor'),
                    street = $(this).parents('div.customer_list_address_update:first').attr('street'),
                    second_street = $(this).parents('div.customer_list_address_update:first').attr('second_street'),
                    barangay = $(this).parents('div.customer_list_address_update:first').attr('barangay'),
                    subdivision = $(this).parents('div.customer_list_address_update:first').attr('subdivision'),
                    city = $(this).parents('div.customer_list_address_update:first').attr('city'),
                    province = $(this).parents('div.customer_list_address_update:first').attr('province'),
                    address_label = $(this).parents('div.customer_list_address_update:first').attr('address_label'),
                    address_type = $(this).parents('div.customer_list_address_update:first').attr('address_type'),
                    landmark = $(this).parents('div.customer_list_address_update:first').attr('landmark'),
                    is_default = $(this).parents('div.customer_list_address_update:first').attr('is_default');

                uiModal.find('#modal_select_province div.option[data-value="'+province+'"]').trigger('click')
                
                uiModal.find('input[name="address_id"]:hidden').val(address_id);

                uiModal.find('input[name="house_number"]').val(house_number_text);
                uiModal.find('input[name="building"]').val(building_text);
                uiModal.find('input[name="floor"]').val(floor_text);
                uiModal.find('input[name="street"]').val(street_text);
                uiModal.find('input[name="second_street"]').val(second_street_text);
                uiModal.find('input[name="barangay"]').val(barangay_text);
                uiModal.find('input[name="subdivision"]').val(subdivision_text);
                uiModal.find('input[name="city"]').val(city_text);
                uiModal.find('input[name="province"]').val(province_text);
                uiModal.find('input[name="address_label"]').val(address_label_text);
                uiModal.find('input[name="address_type"]').val(address_type_text);
                uiModal.find('input[name="landmark"]').val(landmark_text);
                uiModal.find('input[name="is_default"]').val(is_default);
                
                uiModal.find('input[name="house_number"]').attr("value", house_number);
                uiModal.find('input[name="building"]').attr("value", building);
                uiModal.find('input[name="floor"]').attr("value", floor);
                uiModal.find('input[name="street"]').attr("value", street);
                uiModal.find('input[name="second_street"]').attr("value", second_street);
                uiModal.find('input[name="barangay"]').attr("value", barangay);
                uiModal.find('input[name="subdivision"]').attr("value", subdivision);
                uiModal.find('input[name="city"]').attr("value", city);
                uiModal.find('input[name="province"]').attr("value", province);
                uiModal.find('input[name="address_label"]').attr("value", address_label);
                uiModal.find('input[name="address_type"]').attr("value", address_type);
                uiModal.find('input[name="landmark"]').attr("value", landmark);
                uiModal.find('input[name="is_default"]').attr("value", is_default);
                uiModal.find('input[name="is_default"]').prop('checked', ((uiModal.find('input[name="is_default"]').val() == 1) ? true : false));
            });

            $('section.content #search_customer_content').on('click', 'div.behavior-option', function (e) {
                var behaviorId = $(this).attr('data-value-id');
                $(this).parents('div.frm-custom-dropdown.behavior').find('input[name="behavior"]').attr('last_behavior_id', behaviorId);
            });

            $('section.content #search_customer_content').on('change', 'div.frm-custom-dropdown.behavior input[name="behavior"]', function (e) {
                if($(this).val() == '')
                {
                    $(this).attr('last_behavior_id' , '0');
                }
            });

            $('section.content #search_customer_content').on('click', 'button.archive_address_btn', function (e) {
                var uiThis = $(this);
                uiThis.attr('data-value', '1');
                // var yearNow = moment().format('YYYY'),
                //     monthNow = moment().format('MM'),
                //     dayNow = moment().format('DD'),
                //     hourNow = moment().format('HH'),
                //     minNow = moment().format('mm'),
                //     secNow = moment().format('ss');
                // var archive_address_date = yearNow+'-'+monthNow+'-'+dayNow+' '+hourNow+':'+minNow+':'+secNow;
                var archive_address_date = moment().format('YYYY-MM-DD HH:mm:ss');

                var oParams = {
                    "params": {
                        "address_id" : uiThis.parents('div.customer_list_default_address_update').attr('address_id'),
                        "customer_id": uiThis.parents('div.customer_list_default_address_update').attr('customer_id'),
                        "is_deleted": uiThis.attr('data-value'),
                        "archive_address_date": archive_address_date
                    }
                }
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.customers') + "/customers/archive_address",
                    "success": function (data) {
                        if(typeof(data) !== 'undefined')
                        {
                            if(data.status == true)
                            {
                                uiThis.text("Undo").removeClass("archive_address_btn").addClass("unarchive_address_btn").attr('data-value', '0');
                            }
                        }
                    }
                };

                callcenter.customer_list.get(oAjaxConfig)
            })

            $('section.content #search_customer_content').on('click', 'button.unarchive_address_btn', function (e) {
                var uiThis = $(this);
                uiThis.attr('data-value', '0');
                var archive_address_date = '0000-00-00 00:00:00';

                var oParams = {
                    "params": {
                        "address_id" : uiThis.parents('div.customer_list_default_address_update').attr('address_id'),
                        "customer_id": uiThis.parents('div.customer_list_default_address_update').attr('customer_id'),
                        "is_deleted": uiThis.attr('data-value'),
                        "archive_address_date": archive_address_date
                    }
                }
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.customers') + "/customers/archive_address",
                    "success": function (data) {
                        if(typeof(data) !== 'undefined')
                        {
                            if(data.status == true)
                            {
                                uiThis.text("Archive").removeClass("unarchive_address_btn").addClass("archive_address_btn").attr('data-value', '1');
                            }
                        }
                    }
                };

                callcenter.customer_list.get(oAjaxConfig)
            })

            $('section.content #add_customer_content').on('keyup', 'input[name="contact_number[]"].mobile', function (e) {
                // $(this).val($(this).val().replace(/[^0-9-\.]/g,''));
                // if($(this).val() != '')
                // {
                //     var uiThis = $(this).val().split("-").join("");
                //     uiThis = uiThis.match(new RegExp('.{1,3}$|.{1,4}', 'g')).join("-");
                //     $(this).val(uiThis);
                // }
                var uiThis = $(this);
                uiThis.val(uiThis.val().replace(/[^0-9\.]/g,''));
                if(uiThis.val() != '')
                {
                    callcenter.call_orders.auto_dash_mobile(e, uiThis);
                }
            });

            $('section.content #add_customer_content').on('keyup', 'input[name="contact_number[]"].landline.contact-1', function (e) {
                var uiThis = $(this);
                uiThis.val(uiThis.val().replace(/[^0-9\.]/g,''));
                if(uiThis.val() != '')
                {
                    callcenter.call_orders.auto_dash_landline(e, uiThis);
                }
            });

            $('section.content #search_customer_content').on('keyup', 'div#update_customer_content input[name="contact_number[]"].mobile', function (e) {
                // $(this).val($(this).val().replace(/[^0-9-\.]/g,''));
                // if($(this).val() != '')
                // {
                //     var uiThis = $(this).val().split("-").join("");
                //     uiThis = uiThis.match(new RegExp('.{1,3}$|.{1,4}', 'g')).join("-");
                //     $(this).val(uiThis);
                // }
                var uiThis = $(this);
                uiThis.val(uiThis.val().replace(/[^0-9\.]/g,''));
                if(uiThis.val() != '')
                {
                    callcenter.call_orders.auto_dash_mobile(e, uiThis);
                }
            });

            $('section.content #search_customer_content').on('keyup', 'div#update_customer_content input[name="contact_number[]"].landline.contact-1', function (e) {
                var uiThis = $(this);
                uiThis.val(uiThis.val().replace(/[^0-9\.]/g,''));
                if(uiThis.val() != '')
                {
                    callcenter.call_orders.auto_dash_landline(e, uiThis);
                }
            });


        },

        /* 'set_default_address' : function(uiAddress, iCustomerID) {

         },*/
         'reset_all_form_inputs': function (uiForm) {

            if(typeof(uiForm) !== 'undefined')
            {
                uiForm.find('input:text:not([name=address_type],[name=province],[name=city],[name="contact_number_type[]"]), input:password, input:file, select, textarea').val('');
                uiForm.find('input:text:not([name=address_type],[name=province],[name="contact_number_type[]"]), input:password, input:file, select, textarea').removeClass('error');
                uiForm.find('div.select').removeClass('has-error');
                uiForm.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
            }
        },

        'auto_dash_mobile': function(e, uiThis){
            if(typeof(e) !== 'undefined' && typeof(uiThis) !== 'undefined')
            {            
                if(uiThis.val().length >= 11)
                {
                    var newVal = uiThis.val().substr(0,11);
                    if(newVal.match(/^\d{11}$/) !== null)
                    {
                        uiThis.val(newVal.substr(0,4)+'-'+newVal.substr(4,3)+'-'+newVal.substr(7,4));
                    }
                }
                if(uiThis.val().match(/^\d{8,10}$/) !== null)
                {
                    var sliceCount = 0;
                    if(uiThis.val().length == 8)
                    {
                        sliceCount = 4;
                    }
                    else if(uiThis.val().length == 9)
                    {
                        sliceCount = 5;
                    }
                    else if(uiThis.val().length == 10)
                    {
                        sliceCount = 6;
                    }
                    var sliced = uiThis.val().slice(-sliceCount);
                    uiThis.val(uiThis.val().slice(0,-sliceCount)+'-'+sliced);
                }
                if(uiThis.val().match(/^\d{5,7}$/) !== null)
                {
                    var sliceCount = 0;
                    if(uiThis.val().length == 5)
                    {
                        sliceCount = 1;
                    }
                    else if(uiThis.val().length == 6)
                    {
                        sliceCount = 2;
                    }
                    else if(uiThis.val().length == 7)
                    {
                        sliceCount = 3;
                    }
                    var sliced = uiThis.val().slice(-sliceCount);
                    uiThis.val(uiThis.val().slice(0,-sliceCount)+'-'+sliced);
                }
                if(uiThis.val().match(/^\d{5}-[\d-]*$/) !== null)
                {
                    var splitVal = uiThis.val().split('-').join('');
                    if(splitVal.length >= 5 && splitVal.length <= 7)
                    {
                        uiThis.val(splitVal.substr(0,4)+'-'+splitVal.substr(4,3));
                    }
                    if(splitVal.length >= 8)
                    {
                        uiThis.val(splitVal.substr(0,4)+'-'+splitVal.substr(4,3)+'-'+splitVal.substr(7,4));
                    }
                }
                if(uiThis.val().match(/^\d{4}-\d{3}[\d-]*$/) !== null)
                {
                    var splitVal = uiThis.val().split('-').join('');
                    if(splitVal.length >= 5 && splitVal.length <= 7)
                    {
                        uiThis.val(splitVal.substr(0,4)+'-'+splitVal.substr(4,3));
                    }
                    if(splitVal.length >= 8)
                    {
                        uiThis.val(splitVal.substr(0,4)+'-'+splitVal.substr(4,3)+'-'+splitVal.substr(7,4));
                    }
                }
                if(uiThis.val().match(/^\d{4}-\d{4,8}$/) !== null)
                {
                    var sliceCount = 0;
                    if(uiThis.val().length == 9)
                    {
                        sliceCount = 1;
                    }
                    else if(uiThis.val().length == 10)
                    {
                        sliceCount = 2;
                    }
                    else if(uiThis.val().length == 11)
                    {
                        sliceCount = 3;
                    }
                    else if(uiThis.val().length == 12)
                    {
                        sliceCount = 4;
                    }
                    else if(uiThis.val().length == 13)
                    {
                        sliceCount = 5;
                    }
                    var sliced = uiThis.val().slice(-sliceCount);
                    uiThis.val(uiThis.val().slice(0,-sliceCount)+'-'+sliced);
                }
                if(uiThis.val().match(/^\d{4}-\d{3}$/) !== null)
                {
                    uiThis.val(uiThis.val()+"-");
                }
                if(uiThis.val().match(/^\d{4}$/) !== null)
                {
                    uiThis.val(uiThis.val()+"-");
                }
                
                if(e.keyCode == 8)
                {
                    if(uiThis.val().slice(-1) == '-')
                    {
                        uiThis.val(uiThis.val().substr(0, uiThis.val().length-1));            
                    }
                    else
                    {
                        uiThis.val(uiThis.val().substr(0, uiThis.val().length));
                    }
                }
                if(e.keyCode == 173 || e.keyCode == 109 || e.keyCode == 189)
                {
                    if(uiThis.val().slice(-1) == '-')
                    {
                        uiThis.val(uiThis.val().substr(0, uiThis.val().length-1));
                    }
                }
            }
        },

        'auto_dash_landline': function(e, uiThis){
            if(typeof(e) !== 'undefined' && typeof(uiThis) !== 'undefined')
            {
                if(uiThis.val().length >= 8)
                {
                    var newVal = uiThis.val().substr(0,7);
                    if(newVal.match(/^\d{7}$/) !== null)
                    {
                        var sliced = newVal.slice(-4);
                        var sliced2 = sliced.slice(-2);
                        uiThis.val(newVal.slice(0,-4)+'-'+sliced.substr(0, 2)+'-'+sliced2);
                    }
                }
                if(uiThis.val().match(/^\d{7}$/) !== null)
                {
                    var sliced = uiThis.val().slice(-4);
                    var sliced2 = sliced.slice(-2)
                    uiThis.val(uiThis.val().slice(0,-4)+'-'+sliced.substr(0, 2)+'-'+sliced2);
                }
                if(uiThis.val().match(/^\d{5,6}$/) !== null)
                {
                    var sliceCount = 0;
                    if(uiThis.val().length == 5)
                    {
                        sliceCount = 2;
                    }
                    else if(uiThis.val().length == 6)
                    {
                        sliceCount = 3;
                    }
                    var sliced = uiThis.val().slice(-sliceCount);
                    uiThis.val(uiThis.val().slice(0,-sliceCount)+'-'+sliced);
                }
                if(uiThis.val().match(/^\d{4}-[\d-]*$/) !== null)
                {
                    var splitVal = uiThis.val().split('-').join('');
                    if(splitVal.length >= 4 && splitVal.length <= 5)
                    {
                        uiThis.val(splitVal.substr(0,3)+'-'+splitVal.substr(3,2));
                    }
                    if(splitVal.length >= 6)
                    {
                        uiThis.val(splitVal.substr(0,3)+'-'+splitVal.substr(3,2)+'-'+splitVal.substr(5,2));
                    }
                }
                if(uiThis.val().match(/^\d{3}-\d{3}[\d-]*$/) !== null)
                {
                    var splitVal = uiThis.val().split('-').join('');
                    if(splitVal.length >= 4 && splitVal.length <= 5)
                    {
                        uiThis.val(splitVal.substr(0,3)+'-'+splitVal.substr(3,2));
                    }
                    if(splitVal.length >= 6)
                    {
                        uiThis.val(splitVal.substr(0,3)+'-'+splitVal.substr(3,2)+'-'+splitVal.substr(5,2));
                    }
                }
                if(uiThis.val().match(/^\d{3}-\d{3}$/) !== null)
                {
                    var sliced = uiThis.val().slice(-1);
                    uiThis.val(uiThis.val().slice(0,-1)+'-'+sliced);
                }
                if(uiThis.val().match(/^\d{4}$/) !== null)
                {
                    var sliced = uiThis.val().slice(-1);
                    uiThis.val(uiThis.val().slice(0,-1)+'-'+sliced);
                }
                if(uiThis.val().match(/^\d{3}-\d{2}$/) !== null)
                {
                    uiThis.val(uiThis.val()+"-");
                }
                if(uiThis.val().match(/^\d{3}$/) !== null)
                {
                    uiThis.val(uiThis.val()+"-");
                }
                if(e.keyCode == 8)
                {
                    if(uiThis.val().slice(-1) == '-')
                    {
                        uiThis.val(uiThis.val().substr(0, uiThis.val().length-1));            
                    }
                    else
                    {
                        uiThis.val(uiThis.val().substr(0, uiThis.val().length));
                    }
                }
                if(e.keyCode == 173 || e.keyCode == 109 || e.keyCode == 189)
                {
                    if(uiThis.val().slice(-1) == '-')
                    {
                        uiThis.val(uiThis.val().substr(0, uiThis.val().length-1));
                    }
                }
            }
        },

        'assemble_update_form' : function (oData, uiTemplate, updateClone){
            if(typeof(oData) !== 'undefined' && typeof(uiTemplate) !== 'undefined' && typeof(updateClone) !== 'undefined')
            {
                var oParams = {
                    "params": {
                        "where": [
                            {
                                "field"   : 'c.id',
                                "operator": "=",
                                "value"   : oData
                            }
                        ]
                    }
                }

                var oAjaxConfig = {
                    "type"   : "Get",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.customers') + "/customers/get",
                    "success": function (oData) {
                        if(typeof(oData) !== 'undefined')
                        {
                            var cRemarks = '';
                            if (count(oData.data) > 0 && typeof(oData.data) !== 'undefined') {
                                $.each(oData.data, function (key, value){
                                    updateClone = callcenter.customer_list.regenerate_ids(updateClone, value);
                                    
                                    /*name*/
                                    updateClone.find('input[name="first_name"]').val(value.first_name);
                                    updateClone.find('input[name="middle_name"]').val(value.middle_name);
                                    updateClone.find('input[name="last_name"]').val(value.last_name).attr({"datavalid":"required", "labelinput":"Last Name"});

                                    /*email*/
                                    updateClone.find('input[name="email_address[]"]:first').val(value.email_address)/*.attr({"datavalid":"required", "labelinput":"Email Address"})*/;
                                    
                                    /*birthdate*/
                                    var dateArr = (value.birthdate !== '' && typeof(value.birthdate) !== 'undefined') ? value.birthdate.split("-"): '' ;
                                    var dateStr = dateArr[1] + "/" + dateArr[2] + "/" + dateArr[0];
                                    updateClone.find('input[name="birthdate"]').val(dateStr);

                                    /*osca_number*/
                                    updateClone.find('input[name="osca_number"]').val(value.osca_number);

                                    /*contact number*/
                                    if(count(value.contact_number) > 0 && typeof(value.contact_number) !== 'undefined')
                                    {   
                                        if(count(value.contact_number) > 1)
                                        {
                                            updateClone.find('a.alternate-contact-number').addClass("hidden");
                                            updateClone.find('a.alternate-contact-number').parents('div.primary_contact_number').siblings('div.alternate_contact_container').removeClass("hidden");
                                            updateClone.find('a.alternate-contact-number').parents('div.primary_contact_number').siblings('div.alternate_contact_container').find("input.small").removeClass("hidden");
                                        }

                                        $.each(value.contact_number, function (k, v) {
                                            if(k == 0)
                                            {
                                                if(v.number.indexOf('-') !== -1/*v.number.length >= 7 && v.number.length <= 9*/)
                                                {
                                                    var landlineSplit = v.number.split('-');
                                                    updateClone.find('div.primary_contact_number div.select div.frm-custom-dropdown input[name="contact_number_type[]"]').val("Landline").attr("value", "Landline");
                                                    updateClone.find('div.primary_contact_number').find("input.small").addClass("hidden");
                                                    updateClone.find('div.primary_contact_number').find("input.small").removeAttr("datavalid");
                                                    updateClone.find('div.primary_contact_number').find("input.xsmall").removeClass("hidden").prop("disabled", true);
                                                    updateClone.find('div.primary_contact_number').find("input.xsmall:first").val(landlineSplit[0]);
                                                    updateClone.find('div.primary_contact_number').find("input.xsmall:eq(1)").val(landlineSplit[1]);
                                                    updateClone.find('div.primary_contact_number').find("input.xsmall:eq(2)").val(landlineSplit[2]);
                                                }
                                                else //if(v.number.length == 11 /*|| v.number.length == 12*/)
                                                {
                                                    updateClone.find('div.primary_contact_number input[name="contact_number[]"]:visible').val(v.number).prop("disabled", true).removeAttr("datavalid");

                                                }

                                                updateClone.find('div.primary_contact_number div.select div.frm-custom-dropdown').prop("disabled", true);
                                                updateClone.find('div.primary_contact_number div.select div.frm-custom-dropdown input[name="contact_number_type[]"]').prop("disabled", true);
                                            }
                                            else if(k == 1)
                                            {
                                                if(v.number.indexOf('-') !== -1/*v.number.length >= 7 && v.number.length <= 9*/)
                                                {
                                                    var landlineSplit = v.number.split('-');
                                                    updateClone.find('div.alternate_contact_container div.select div.frm-custom-dropdown input[name="contact_number_type[]"]').val("Landline").attr("value", "Landline");
                                                    updateClone.find('div.alternate_contact_container').find("input.small").addClass("hidden").val('');
                                                    updateClone.find('div.alternate_contact_container').find("input.small").removeAttr("datavalid");
                                                    updateClone.find('div.alternate_contact_container').find("input.xsmall").removeClass("hidden").prop("disabled", true);
                                                    updateClone.find('div.alternate_contact_container').find("input.xsmall:first").val(landlineSplit[0]);
                                                    updateClone.find('div.alternate_contact_container').find("input.xsmall:eq(1)").val(landlineSplit[1]);
                                                    updateClone.find('div.alternate_contact_container').find("input.xsmall:eq(2)").val(landlineSplit[2]);
                                                }
                                                else //if(v.number.length == 11 /*|| v.number.length == 12*/)
                                                {
                                                    updateClone.find('a.alternate-contact-number').parents('div.primary_contact_number').siblings('div.alternate_contact_container').find('input[name="contact_number[]"]').val(v.number).prop("disabled", true).removeAttr("datavalid");
                                                }

                                                updateClone.find('div.alternate_contact_container div.select div.frm-custom-dropdown').prop("disabled", true);
                                                updateClone.find('div.alternate_contact_container div.select div.frm-custom-dropdown input[name="contact_number_type[]"]').prop("disabled", true);
                                            }
                                            else
                                            {
                                                var oContactTemplate = updateClone.find('div.template.new_contact_number').clone().removeClass('template');
                                                var oContactContainer = updateClone.find('div.alternate_contact_container:first');
                                                oContactTemplate.find('input:not(input[name="contact_number_type[]"])').val('');

                                                if(v.number.indexOf('-') !== -1/*v.number.length >= 7 && v.number.length <= 9*/)
                                                {
                                                    var landlineSplit = v.number.split('-');
                                                    oContactTemplate.find('div.select div.frm-custom-dropdown input[name="contact_number_type[]"]').val("Landline").attr("value", "Landline");
                                                    oContactTemplate.find("input.small").addClass("hidden").val('');
                                                    oContactTemplate.find("input.small").removeAttr("datavalid");
                                                    oContactTemplate.find("input.xsmall").removeClass("hidden").prop("disabled", true);
                                                    oContactTemplate.find("input.xsmall:first").val(landlineSplit[0]);
                                                    oContactTemplate.find("input.xsmall:eq(1)").val(landlineSplit[1]);
                                                    oContactTemplate.find("input.xsmall:eq(2)").val(landlineSplit[2]);
                                                }
                                                else //if(v.number.length == 11 /*|| v.number.length == 12*/)
                                                {
                                                    oContactTemplate.find('input[name="contact_number[]"].small').val(v.number).prop("disabled", true).removeAttr("datavalid");
                                                }

                                                oContactTemplate.find('a.another_remove').hide();
                                                oContactTemplate.find('div.select div.frm-custom-dropdown').prop("disabled", true);
                                                oContactTemplate.find('div.select div.frm-custom-dropdown input[name="contact_number_type[]"]').prop("disabled", true);
                                                callcenter.call_orders.clone_append(oContactTemplate, oContactContainer);
                                            }
                                        });
                                    }

                                    /*happy plus*/
                                    if(count(value.happy_plus_card) > 0 && typeof(value.happy_plus_card) !== 'undefined')
                                    {   
                                        $.each(value.happy_plus_card, function (k, v) {
                                            if(k == 0)
                                            {
                                                updateClone.find('div.happy_plus_card:first').addClass("disabled");
                                                updateClone.find('input[name="number"]:first').val(v.number).prop("disabled", true);
                                                updateClone.find('input[name="expiry_date"]:first').val(v.expiration_date_date).prop("disabled", true);
                                                updateClone.find('input[name="remarks"]:first').val(v.remarks).prop("disabled", true);
                                                updateClone.find("input[name='is_smudged']:first").prop("disabled", true);
                                                updateClone.find('div.happy_plus_card:first div.date-picker').css('pointer-events', 'none');
                                                if(v.is_smudged == 1)
                                                {
                                                    updateClone.find("input[name='is_smudged']:first").prop("checked", true);
                                                }
                                            }
                                            else if(k == 1)
                                            {
                                                updateClone.find('div.parent_happy_plus div.happy_plus_card:first a.alternate_happy_plus').addClass("hidden");
                                                updateClone.find('div.another_happy_plus').addClass("disabled");
                                                updateClone.find('div.another_happy_plus').removeClass("hidden");
                                                updateClone.find('div.another_happy_plus').find('input[name="number"]').val(v.number).prop("disabled", true);
                                                updateClone.find('div.another_happy_plus').find('input[name="expiry_date"]').val(v.expiration_date_date).prop("disabled", true);
                                                updateClone.find('div.another_happy_plus').find('input[name="remarks"]').val(v.remarks).prop("disabled", true);
                                                updateClone.find('div.another_happy_plus').find("input[name='is_smudged']").prop("disabled", true);
                                                updateClone.find('div.another_happy_plus div.date-picker').css('pointer-events', 'none');
                                                if(v.is_smudged == 1)
                                                {
                                                    updateClone.find("input[name='is_smudged']").prop("checked", true);
                                                }
                                            }
                                            else
                                            {
                                                var oHappyPlusTemplate = updateClone.find('div.parent_happy_plus div.template.new_happy_plus_card').clone().removeClass('template').addClass("disabled");
                                                var oHappyPlusContainer = updateClone.find('div.parent_happy_plus');
                                                var happyPlusQuantity = updateClone.find('div.parent_happy_plus div.happy_plus_card:visible').length + 1;

                                                oHappyPlusTemplate.find('input[name="is_smudged"]').attr('id', 'card-number-' + happyPlusQuantity);
                                                oHappyPlusTemplate.find('label.chck-lbl').attr('for', 'card-number-' + happyPlusQuantity);
                                                oHappyPlusTemplate.find("input[type='text'][name='number']").val(v.number).prop("disabled", true);
                                                oHappyPlusTemplate.find("input[type='text'][name='expiry_date']").val(v.expiration_date_date).prop("disabled", true);
                                                oHappyPlusTemplate.find("input[type='text'][name='remarks']").val(v.remarks).prop("disabled", true);
                                                oHappyPlusTemplate.find("input[name='is_smudged']").prop("disabled", true);
                                                oHappyPlusTemplate.find('div.date-picker').css('pointer-events', 'none');
                                                if(v.is_smudged == 1)
                                                {
                                                    oHappyPlusTemplate.find("input[name='is_smudged']").prop("checked", true);
                                                }
                                                oHappyPlusTemplate.find("input[type='text'][name='remarks']").addClass("margin-bottom-15");
                                                oHappyPlusTemplate.find("a.another_remove").hide();
                                                if(updateClone.find('div.parent_happy_plus div.happy_plus_card:not(.template):visible').length > 2)
                                                {
                                                    updateClone.find('a.another_happy_plus').removeClass("margin-bottom-15");
                                                }
                                                callcenter.customer_list.clone_append(oHappyPlusTemplate, oHappyPlusContainer);
                                                $(".date-picker").datetimepicker({pickTime: false});
                                            }

                                        })
                                    }

                                    /*remarks*/
                                    callcenter.customer_list.get_remarks(updateClone, value.last_behavior_id, '');
                                    // if(typeof(value.remarks) !== 'undefined')
                                    // {
                                    //     if(count(value.remarks) > 0)
                                    //     {   
                                    //         $.each(value.remarks, function (k, v) {
                                    //             cRemarks += '<div class="option behavior-option" data-value-id="' + v.id + '"><span class="">' + v.display_label + '</span></div>'
                                    //             if(v.id == value.last_behavior_id)
                                    //             {
                                    //                 updateClone.find('input[name="behavior"]').val(v.display_label).attr('last_behavior_id', v.id);
                                    //             }
                                    //         })
                                    //     }
                                    //     else
                                    //     {
                                    //         cRemarks += '<div class="option behavior-option" data-value-id="0"><span class="">No remarks available.</span></div>'
                                    //         updateClone.find('input[name="behavior"]').val('No remarks available.').attr('last_behavior_id', '0');
                                    //     }

                                    //     var oDivs = updateClone.find('div.frm-custom-dropdown.behavior');
                                    //     $.each(oDivs, function (k, v) {
                                    //         $(this).find('div.frm-custom-dropdown-option').html('').append(cRemarks);
                                    //     })
                                    // }

                                    /*address*/
                                    if (count(value.address) > 0 && count(value.address_text) > 0 && typeof(value.address) !== 'undefined' && typeof(value.address_text) !== 'undefined')
                                    {
                                        $.each(value.address, function (k, v) {
                                            var house_number = (v.house_number !== '' && v.house_number != "0") ? v.house_number : '',
                                                floor = (v.floor !== '' && v.floor != "0") ? v.floor : '',
                                                building = (value.address_text[k].building !== null) ? value.address_text[k].building.name : '',
                                                barangay = (value.address_text[k].barangay !== null) ? value.address_text[k].barangay.name : '',
                                                subdivision = (value.address_text[k].subdivision !== null) ? value.address_text[k].subdivision.name : '',
                                                city = (value.address_text[k].city !== null) ? value.address_text[k].city.name : '',
                                                street = (value.address_text[k].street !== null) ? value.address_text[k].street.name : '',
                                                second_street = (value.address_text[k].second_street !== null && typeof(value.address_text[k].second_street) != 'undefined') ? value.address_text[k].second_street.name : '',
                                                province = (value.address_text[k].province !== null) ? value.address_text[k].province.name : '';

                                            var uiAddressTemplate = updateClone.find('div.customer_list_default_address_update.template').clone().removeClass('template');

                                            if(v.address_type.toUpperCase() === "HOME")
                                            {
                                                uiAddressTemplate.find('img.default_address_image').attr('onError', '$.fn.checkImage(this);');
                                                uiAddressTemplate.find('img.default_address_image').attr({'src': callcenter.config('url.server.base') + 'assets/images/Home2.svg', "alt" : "work icon", "class" : "small-thumb"});
                                            }
                                            else
                                            {
                                                uiAddressTemplate.find('img.default_address_image').attr({'src': callcenter.config('url.server.base') + 'assets/images/work-icon.png', "alt" : "work icon"});
                                                uiAddressTemplate.find('img.default_address_image').attr('onError', '$.fn.checkImage(this);');
                                            }
                                            uiAddressTemplate.find('strong.default_address_label').text(v.address_label);
                                            uiAddressTemplate.find('p.default_address_complete').text('' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);
                                            uiAddressTemplate.find('strong.default_address_type').text(v.address_type.toUpperCase());
                                            uiAddressTemplate.find('p.default_address_landmark').text('- ' + v.landmark.toUpperCase());
                                            uiAddressTemplate.attr('customer_id', value.id);
                                            uiAddressTemplate.attr('address_id', v.id);
                                            uiAddressTemplate.attr('house_number', v.house_number);
                                            uiAddressTemplate.attr('building', v.building);
                                            uiAddressTemplate.attr('floor', v.floor);
                                            uiAddressTemplate.attr('street', v.street);
                                            uiAddressTemplate.attr('second_street', v.second_street);
                                            uiAddressTemplate.attr('barangay', v.barangay);
                                            uiAddressTemplate.attr('subdivision', v.subdivision);
                                            uiAddressTemplate.attr('city', v.city);
                                            uiAddressTemplate.attr('province', v.province);
                                            uiAddressTemplate.attr('landmark', v.landmark);
                                            uiAddressTemplate.attr('address_label', v.address_label);
                                            uiAddressTemplate.attr('address_type', v.address_type);
                                            uiAddressTemplate.attr('all_address', '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);
                                            uiAddressTemplate.attr('is_default', v.is_default);

                                            if(v.is_deleted == 1)
                                            {
                                                uiAddressTemplate.find('button.archive_address_btn').text("Undo").removeClass("archive_address_btn").addClass("unarchive_address_btn").attr('data-value', '0');
                                            }
                                            else
                                            {
                                                uiAddressTemplate.find('button.archive_address_btn').text("Archive").removeClass("unarchive_address_btn").addClass("archive_address_btn").attr('data-value', '1');
                                            }

                                            /*needed attr for update of address*/
                                            uiAddressTemplate.attr('house_number_text', house_number);
                                            uiAddressTemplate.attr('building_text', building);
                                            uiAddressTemplate.attr('floor_text', floor);
                                            uiAddressTemplate.attr('street_text', street);
                                            uiAddressTemplate.attr('second_street_text', second_street);
                                            uiAddressTemplate.attr('barangay_text', barangay);
                                            uiAddressTemplate.attr('subdivision_text', subdivision);
                                            uiAddressTemplate.attr('city_text', city);
                                            uiAddressTemplate.attr('province_text', province);
                                            uiAddressTemplate.attr('landmark_text', v.landmark);
                                            uiAddressTemplate.attr('address_label_text', v.address_label);
                                            uiAddressTemplate.attr('address_type_text', v.address_type);

                                            if(v.is_default == 1)
                                            {
                                                if(v.is_deleted_date != '0000-00-00 00:00:00' && v.is_deleted == 1)
                                                {
                                                    var dateNow = moment().format("YYYY-MM-DD"),
                                                        timeNow = moment().format("HH:mm:ss"),
                                                        is_deleted_fulldate = v.is_deleted_date.split(' '),
                                                        is_deleted_date = is_deleted_fulldate[0].split('-'),
                                                        is_deleted_time = is_deleted_fulldate[1].split(':'),
                                                        time_now = timeNow.split(':');

                                                    /*deleted time*/
                                                    var is_deleted_hour = is_deleted_time[0],
                                                        is_deleted_min = is_deleted_time[1],
                                                        is_deleted_sec = is_deleted_time[2];

                                                    /*time now*/
                                                    var hour_now = time_now[0],
                                                        min_now = time_now[1],
                                                        sec_now = time_now[2];

                                                    // console.log('DELETED TIME '+is_deleted_hour+':'+is_deleted_min)
                                                    // console.log('TIME NOW '+hour_now+':'+min_now)

                                                    // console.log(dateNow == is_deleted_fulldate[0])
                                                    // console.log(dateNow > is_deleted_fulldate[0]) //do not show address if it is deleted previous date

                                                    if(dateNow == is_deleted_fulldate[0])
                                                    {
                                                        if( ( parseInt(hour_now) - parseInt(is_deleted_hour) ) > parseInt('1') || ( parseInt(hour_now) - parseInt(is_deleted_hour) ) == parseInt('1') && parseInt(is_deleted_min) <= parseInt(min_now) )
                                                        {
                                                            // console.log("remove/hide address")
                                                        }
                                                        else
                                                        {
                                                            // console.log("undo");
                                                            uiAddressTemplate.find('button.archive_address_btn').text("Undo").removeClass("archive_address_btn").addClass("unarchive_address_btn").attr('data-value', '0');
                                                            uiAddressTemplate.find('strong.default_address_label').html(v.address_label+' - <span class="red-color">Default</span>');
                                                            updateClone.find('div.default_address_container_update').append(uiAddressTemplate);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    uiAddressTemplate.find('strong.default_address_label').html(v.address_label+' - <span class="red-color">Default</span>');
                                                    updateClone.find('div.default_address_container_update').append(uiAddressTemplate); 
                                                }
                                            }
                                            else
                                            {
                                                if(v.is_deleted_date != '0000-00-00 00:00:00' && v.is_deleted == 1)
                                                {
                                                    var dateNow = moment().format("YYYY-MM-DD"),
                                                        timeNow = moment().format("HH:mm:ss"),
                                                        is_deleted_fulldate = v.is_deleted_date.split(' '),
                                                        is_deleted_date = is_deleted_fulldate[0].split('-'),
                                                        is_deleted_time = is_deleted_fulldate[1].split(':'),
                                                        time_now = timeNow.split(':');

                                                    /*deleted time*/
                                                    var is_deleted_hour = is_deleted_time[0],
                                                        is_deleted_min = is_deleted_time[1],
                                                        is_deleted_sec = is_deleted_time[2];

                                                    /*time now*/
                                                    var hour_now = time_now[0],
                                                        min_now = time_now[1],
                                                        sec_now = time_now[2];

                                                    if(dateNow == is_deleted_fulldate[0])
                                                    {
                                                        if( ( parseInt(hour_now) - parseInt(is_deleted_hour) ) > parseInt('1') || ( parseInt(hour_now) - parseInt(is_deleted_hour) ) == parseInt('1') && parseInt(is_deleted_min) <= parseInt(min_now) )
                                                        {
                                                            // console.log("remove/hide address")
                                                        }
                                                        else
                                                        {
                                                            // console.log("undo");
                                                            uiAddressTemplate.find('button.archive_address_btn').text("Undo").removeClass("archive_address_btn").addClass("unarchive_address_btn").attr('data-value', '0');
                                                            // uiAddressTemplate.removeClass('bggray-light').addClass('white');
                                                            uiAddressTemplate.find('button.set_default_address').removeAttr('disabled');
                                                            updateClone.find('div.secondary_address_container_update').append(uiAddressTemplate);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    // uiAddressTemplate.removeClass('bggray-light').addClass('white');
                                                    uiAddressTemplate.find('button.set_default_address').removeAttr('disabled');
                                                    updateClone.find('div.secondary_address_container_update').append(uiAddressTemplate);
                                                }
                                            }
                                        });
                                    }

                                });
                            }
                        }
                    }
                };

                callcenter.call_orders.get(oAjaxConfig);

                var oValidationConfig = {
                        'data_validation'    : 'datavalid',
                        'input_label'        : 'labelinput',
                        'min_length'         : 0,
                        'max_length'         : 75,
                        'class'              : 'error',
                        'add_remove_class'   : true,
                        'Contact Number': 
                        {
                            'min_length': 13
                        },
                        'Landline Number': 
                        {
                            'min_length': 9
                        },
                        'Confirm Password':
                        {
                            'compare_input': 'Password'
                        },
                        'onValidationError'  : function (arrMessages) {
                            var sError = '';
                            var uiContainer = uiTemplate.find('div.update-customer-error-msg');
                            if(typeof(arrMessages) !== 'undefined')
                            {
                                $.each(arrMessages, function (k, v) {
                                    sError += '<p class="error_messages"><i class="fa fa-exclamation-triangle"></i> ' + v.error_message + '</p>';
                                });
                            }
                            uiTemplate.find('div.success-msg').hide();
                            uiTemplate.find('div.error-msg').hide();
                            callcenter.call_orders.add_error_message(sError, uiContainer);
                            callcenter.call_orders.show_spinner($('button.btn-save'), false);
                            $("html, body").animate({scrollTop: updateClone.parents('div.collapsible').offset().top}, 'slow');
                            // //console.log(arrMessages); //shows the errors
                        },
                        'onValidationSuccess': function () {
                            callcenter.call_orders.show_spinner($('button.btn-save'), true);
                            callcenter.call_orders.assemble_update_params(updateClone, uiTemplate);
                        }
                    }
                updateClone.find('form#update-customer').attr('customer_id', oData);
                if(updateClone.find('form#update-customer').attr('customer_id') == oData)
                {
                    callcenter.validate_form(updateClone.find('form#update-customer'), oValidationConfig);
                }
                $(".date-picker").datetimepicker({pickTime: false});
                var date = new Date();
                date.setDate(date.getDate());
                updateClone.find("#birthdate-date-picker").datetimepicker({pickTime: false, maxDate: date });
            }
        },

        'assemble_update_params': function (updateClone, uiTemplate) {
            if(typeof(updateClone) !== 'undefined' && typeof(uiTemplate) !== 'undefined')
            {            
                var updateCloneForm = updateClone.find('form#update-customer');

                var aContact = [];

                //primary mobile
                var sPrimaryContactMobile = "";
                var uiPrimaryContactMobile = updateClone.find('div.primary_contact_number input.small[name="contact_number[]"]:visible:enabled');
                if(typeof(uiPrimaryContactMobile)!== 'undefined')
                {
                    $.each(uiPrimaryContactMobile, function () {
                        var sValue = $(this).val().replace(/-/g, '');
                        sPrimaryContactMobile += sValue;
                    });
                    aContact.push(sPrimaryContactMobile);
                }

                //primary landline
                var sPrimaryContactLandline = "";
                var uiPrimaryContactLandline = updateClone.find('div.primary_contact_number input.xsmall[name="contact_number[]"]:visible');
                if(typeof(uiPrimaryContactLandline)!== 'undefined')
                {
                    var iCounter = 0;
                    $.each(uiPrimaryContactLandline, function () {
                        iCounter++;
                        var sValue = $(this).val().replace(/-/g, '')+'-';
                        sPrimaryContactLandline += sValue;
                        if (iCounter == 3) {

                            if(sPrimaryContactLandline.match(/^\d+-\d+--$/))
                            {
                                sPrimaryContactLandline = sPrimaryContactLandline.slice(0,-2);
                            }
                            else
                            {
                                sPrimaryContactLandline = sPrimaryContactLandline.slice(0,-1);
                            }
                            sPrimaryContactLandline += ",";
                            iCounter = 0;
                        }
                    });
                }
                if(typeof(sPrimaryContactLandline) !== 'undefined')
                {
                    var arsPrimaryContactLandLine = sPrimaryContactLandline.split(",");
                    aContact = aContact.concat(arsPrimaryContactLandLine);
                }

                var uiAlternateContactMobile = updateClone.find('div.new_contact_number input.small[name="contact_number[]"]:visible:enabled');
                if(typeof(uiAlternateContactMobile) !== 'undefined')
                {
                    $.each(uiAlternateContactMobile, function () {
                        var sValue = $(this).val().replace(/-/g, '')
                        aContact.push(sValue);
                    });
                }

                var sAlternateContactLandLine = "";
                var uiAlternateContactLandLine = updateClone.find('div.new_contact_number input.xsmall[name="contact_number[]"]:visible:enabled');
                if(typeof(uiAlternateContactLandLine) !== 'undefined')
                {
                    var iCounter = 0;
                    $.each(uiAlternateContactLandLine, function () {
                        iCounter++;
                        var sValue = $(this).val().replace(/-/g, '')+'-';
                        sAlternateContactLandLine += sValue;
                        if (iCounter == 3) {

                            sAlternateContactLandLine = sAlternateContactLandLine.replace(/--/g, '-')
                            if(sAlternateContactLandLine.lastIndexOf("-") != -1)
                            {
                                sAlternateContactLandLine = sAlternateContactLandLine.slice(0,-1);
                            }

                            sAlternateContactLandLine += ",";
                            iCounter = 0;
                        }
                    });
                }
                if(typeof(sAlternateContactLandLine) !== 'undefined')
                {
                    var arsAlternateContactLandLine = sAlternateContactLandLine.split(",");
                    aContact = aContact.concat(arsAlternateContactLandLine);
                }

                aContact = jQuery.grep(aContact, function(n, i){
                  return (n !== "" && n != null);
                });

                var dateArr = (updateCloneForm.find('input[name="birthdate"]').val() !== '') ? updateCloneForm.find('input[name="birthdate"]').val().split("/"): '';
                if(typeof(dateArr) !== 'undefined' && dateArr !== '')
                {
                    var dateStr = dateArr[2] + "-" + dateArr[0] + "-" + dateArr[1];
                }
                else
                {
                    dateStr = '0000-00-00';
                }

                var oUpdateParams = {
                        "params": {
                            "data" : [
                               {
                                 "field" : "first_name",
                                 "value" : updateCloneForm.find('input[name="first_name"]').val()
                               },
                               {
                                 "field" : "middle_name",
                                 "value" : updateCloneForm.find('input[name="middle_name"]').val()
                               },
                               {
                                 "field" : "last_name",
                                 "value" : updateCloneForm.find('input[name="last_name"]').val()
                               },
                               {
                                 "field" : "birthdate",
                                 "value" : dateStr
                               },
                               {
                                 "field" : "email_address",
                                 "value" : updateCloneForm.find('input[name="email_address[]"]').val()
                               },
                               // {
                               //   "field" : "position",
                               //   "value" : updateCloneForm.find('input[name="position"]').val()
                               // },
                               // {
                               //   "field" : "company",
                               //   "value" : updateCloneForm.find('input[name="company"]').val()
                               // },
                               // {
                               //   "field" : "last_order_date",
                               //   "value" : updateCloneForm.find('input[name="last_order_date"]').val()
                               // },
                               {
                                 "field" : "last_behavior_id",
                                 "value" : updateCloneForm.find('input[name="behavior"]').attr('last_behavior_id')
                               },
                               {
                                 "field" : "is_senior",
                                 "value" : updateCloneForm.find('input[type="radio"][name="senior"]:checked').val()
                               },
                               {
                                 "field" : "is_pwd",
                                 "value" : updateCloneForm.find('input[name="pwd"]:checked').val()
                               },
                               {
                                 "field" : "osca_number",
                                 "value" : updateCloneForm.find('input[name="osca_number"]').val()
                               },
                               {
                                 "field" : "is_loyalty_card_holder",
                                 "value" : updateCloneForm.find('input[name="loyalty"]:checked').val()
                               }
                               ],
                            "customer_id": updateCloneForm.attr('customer_id'),
                            "contact_number": aContact,
                            "qualifiers" : [
                                {
                                 "field" : "id",
                                 "value" : updateCloneForm.attr('customer_id'),
                                 "operator" : "="
                                }
                            ]
                        }
                    }

                var oHappyPlusCards = [];
                var uiHappyPlusCards = updateCloneForm.find('div.happy_plus_card:not(.disabled):not(.template):visible');
                if(typeof(uiHappyPlusCards) !== 'undefined')
                {
                    $.each(uiHappyPlusCards, function () {
                        var oHappyPlusCard = {
                            'number'              : $(this).find('input[name="number"]:first').val(),
                            'expiration_date_date': $(this).find('input[name="expiry_date"]:first').val(),
                            'is_smudged'          : $(this).find('input[name="is_smudged"]:checked').val(),
                            'remarks'             : $(this).find('input[name="remarks"]:first').val()
                        }
                        oHappyPlusCards.push(oHappyPlusCard);
                    })
                }

                var oUpdateConfig = {
                    "type"   : "POST",
                    "data"   : oUpdateParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.customers') + "/customers/update",
                    "success": function (oUpdateData) {
                        callcenter.customer_list.show_spinner($('button.btn-save'), false);
                        if(typeof(oUpdateData) !== 'undefined')
                        {
                            if(oUpdateData.status == true)
                            {
                                if(updateCloneForm.find('input[name="behavior"]').attr('last_behavior_id') != 0)
                                {
                                    callcenter.customer_list.insert_behavior(updateCloneForm.attr('customer_id'), updateCloneForm.find('input[name="behavior"]').attr('last_behavior_id'));
                                }
                                
                                if(oHappyPlusCards.length > 0)
                                {
                                    $.each(oHappyPlusCards, function (k ,v) {
                                        var oHappyPlusConfig = {
                                            "type"   : "POST",
                                            "data"   : v,
                                            "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                                            "url"    : callcenter.config('url.api.jfc.happy_plus') + "happy_plus/add",
                                            "success": function (oHappyData) {
                                                console.log(oHappyData)
                                                if(oHappyData.status == true)
                                                {
                                                    callcenter.customer_list.insert_happy_plus_map(updateCloneForm, oHappyData.data.happy_plus_id);
                                                    callcenter.call_orders.assemble_customer_info_success(updateCloneForm, uiTemplate, oUpdateData, updateClone);
                                                }
                                                if(oHappyData.status == false)
                                                {
                                                    callcenter.customer_list.insert_happy_plus_map(updateCloneForm, oHappyData.data.happy_plus_id);
                                                    callcenter.call_orders.assemble_customer_info_success(updateCloneForm, uiTemplate, oUpdateData, updateClone);
                                                }
                                            }
                                        }
                                        callcenter.customer_list.get(oHappyPlusConfig);
                                    });
                                }

                                if(oHappyPlusCards.length == 0)
                                {
                                    callcenter.call_orders.assemble_customer_info_success(updateCloneForm, uiTemplate, oUpdateData, updateClone);
                                }
                            }
                            else
                            {
                                updateCloneForm.parents('div#update_customer_content').find('div.success-msg').hide();
                                //updateCloneForm.parents('div#update_customer_content').find('div.update-customer-error-msg').empty().html('<i class="fa fa-exclamation-triangle"></i> '+data.message.error).show();
                                var sError = '';
                                $.each(oUpdateData.message.error, function (k, v) {
                                    sError += '<p class="error_messages"><i class="fa fa-exclamation-triangle"></i> ' + v + '</p>';
                                });
                                updateCloneForm.parents('div#update_customer_content').find('div.update-customer-error-msg').empty().html(sError).show();

                                $("html, body").animate({scrollTop: updateCloneForm.parents('div.collapsible').offset().top}, 'slow');
                                
                                if(oUpdateData.message.error == 'Invalid Email address')
                                {
                                    updateCloneForm.find('input[name="email_address[]"]').addClass('error');
                                }
                                else
                                {
                                    updateCloneForm.find('input[name="email_address[]"]').removeClass('error');
                                }

                                if(oUpdateData.message.error == 'Middle name invalid or is too short')
                                {
                                    updateCloneForm.find('input[name="middle_name"]').addClass('error');
                                }
                                else
                                {
                                    updateCloneForm.find('input[name="middle_name"]').removeClass('error');
                                }

                                if(oUpdateData.message.error == 'Please specify birthdate')
                                {
                                    updateCloneForm.find('input[name="birthdate"]').addClass('error');
                                }
                                else
                                {
                                    updateCloneForm.find('input[name="birthdate"]').removeClass('error');
                                }
                            }
                        }

                    }
                };

                callcenter.customer_list.get(oUpdateConfig);
            }
        },

        'assemble_customer_info_success': function (updateCloneForm, uiTemplate, oUpdateData, updateClone){
            if(typeof(updateCloneForm) !== 'undefined' && typeof(uiTemplate) !== 'undefined' && typeof(oUpdateData) !== 'undefined' && typeof(updateClone) !== 'undefined')
            {
                updateCloneForm.find('input[name="birthdate"]').removeClass('error');
                updateCloneForm.find('input[name="middle_name"]').removeClass('error');
                updateCloneForm.find('input[name="email_address[]"]').removeClass('error');
                updateCloneForm.parents('div#update_customer_content').find('div.update-customer-error-msg').hide();
                updateCloneForm.parents('div#update_customer_content').hide();

                $("html, body").animate({scrollTop: uiTemplate.offset().top}, 'slow');
                uiTemplate.find('div.success-msg').empty().html('<i class="fa fa-exclamation-triangle"></i> '+oUpdateData.message[0]).show().delay(3000).fadeOut();;
                uiTemplate.siblings('div.header-search').hide();
                uiTemplate.show();
                uiTemplate.children().not(updateClone).show();

                var oParam = {
                    "params": {
                        "where": [
                            {
                                "field"   : 'c.id',
                                "operator": "=",
                                "value"   : updateCloneForm.attr('customer_id')
                            }
                        ]
                    }
                }

                var oConfig = {
                    "type"   : "Get",
                    "data"   : oParam,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.customers') + "/customers/get",
                    "success": function (oData) {
                        if(typeof(oData) !== 'undefined')
                        {
                            $.each(oData.data, function (key, value) {
                                
                                //header attr
                                uiTemplate.parents('div.search_result_container:first').attr({'data-customer-id': value.id, 'is_pwd': value.is_pwd, 'is_senior': value.is_senior})

                                /*name*/
                                uiTemplate.siblings('div.header-search').find('strong.info_name').text(value.first_name + ' ' + value.middle_name + ' ' + value.last_name);
                                uiTemplate.find('strong.info_name').text(value.first_name + ' ' + value.middle_name + ' ' + value.last_name);

                                /*contact numbers*/
                                if (count(value.contact_number) > 0 && typeof(value.contact_number) !== 'undefined') {
                                    var fCarrier = '';
                                    if (typeof(value.contact_number[0].name) !== 'undefined') {
                                        if(value.contact_number[0].number.indexOf('-') != -1)
                                        {
                                            fCarrier = 'Landline'; //if landline put landline in carrier prefix
                                        }
                                        else
                                        {
                                            fCarrier = ((value.contact_number[0].name !== 'None')? value.contact_number[0].name : 'Unknown'); //if mobile put unknown
                                        }
                                    }
                                    uiTemplate.find('strong.contact_number').text('Contact Num: ' + value.contact_number[0].number + ' ' + fCarrier);
                                    var sNumbers = '';
                                    var fNumber = '';
                                    $.each(value.contact_number, function (k, v) {
                                        var sCarrier = '';
                                        if (typeof(v.name) != 'undefined') {
                                            if(v.number.indexOf('-') != -1)
                                            {
                                                sCarrier = 'Landline'; //if landline put landline in carrier prefix
                                            }
                                            else
                                            {
                                                sCarrier = ((v.name !== 'None') ? v.name : 'Unknown' ); //if mobile put unknown
                                            }
                                        }
                                        if (k == 0) {
                                            uiTemplate.find('div.contact_number input.dd-txt').val(v.number + " " + sCarrier);
                                        }
                                        else if (k == 1) {
                                            uiTemplate.find('div.alternate_contact_number input.dd-txt').val(v.number + " " + sCarrier);
                                        }

                                        sNumbers += '<div class="option" data-value="' + v.number + '"><span class="">' + v.number + '</span> ' + sCarrier + ' </div>'

                                    })

                                    var oDivs = uiTemplate.find('div.data-container:first div.frm-custom-dropdown');
                                    $.each(oDivs, function (k, v) {
                                        if (k == 1 || k == 0) {
                                            $(this).find('div.frm-custom-dropdown-option').html('').append(sNumbers);
                                        }
                                    })
                                }

                                /*email address*/
                                uiTemplate.find('p.email_address').text(value.email_address);
                                
                                /*remarks*/
                                // callcenter.customer_list.get_remarks('', value.last_behavior_id, uiTemplate);
                                // if (count(value.remarks) > 0) {
                                //     $.each(value.remarks, function (k, v) {
                                //         if(v.id == value.last_behavior_id)
                                //         {
                                //             uiTemplate.find('span.info-remarks').text(v.display_label.toUpperCase());
                                //             uiTemplate.find('span.info-remarks').css('color', '#' + v.color);
                                //             uiTemplate.siblings('div.header-search').find('span.info-remarks').text(v.display_label.toUpperCase());
                                //             uiTemplate.siblings('div.header-search').find('span.info-remarks').css('color', '#' + v.color);
                                //         }
                                //     })
                                // }
                                // else {
                                //     uiTemplate.find('span.info-remarks').text('NO REMARKS AVAILABLE');
                                //     uiTemplate.find('span.info-remarks').css('color', '#000000');
                                //     uiTemplate.siblings('div.header-search').find('span.info-remarks').text('NO REMARKS AVAILABLE');
                                //     uiTemplate.siblings('div.header-search').find('span.info-remarks').css('color', '#000000');
                                // }
                                if (value.color != null && value.display_label != null) {
                                    uiTemplate.find('span.info-remarks').text(value.display_label.toUpperCase());
                                    uiTemplate.find('span.info-remarks').css('color', '#' + value.color);
                                    uiTemplate.siblings('div.header-search').find('span.info-remarks').text(value.display_label.toUpperCase());
                                    uiTemplate.siblings('div.header-search').find('span.info-remarks').css('color', '#' + value.color);
                                }
                                else {
                                    uiTemplate.find('span.info-remarks').text('NO REMARKS AVAILABLE');
                                    uiTemplate.find('span.info-remarks').css('color', '#000000');
                                    uiTemplate.siblings('div.header-search').find('span.info-remarks').text('NO REMARKS AVAILABLE');
                                    uiTemplate.siblings('div.header-search').find('span.info-remarks').css('color', '#000000');
                                }

                                /*happy plus cards*/
                                if (count(value.happy_plus_card) > 0) {
                                    uiTemplate.find('div.happy_plus_card_container').find('div.happy_plus_card:not(.template)').remove();
                                    $.each(value.happy_plus_card, function (k, v) {
                                        var uiHappyPlusTemplate = uiTemplate.find('div.happy_plus_card.template:not(.new_happy_plus_card)').clone().removeClass('template');
                                        uiHappyPlusTemplate.find('p.info-happy-plus-number').text(v.number);
                                        uiHappyPlusTemplate.find('p.expiration_date').text(v.expiration_date_date);
                                        uiHappyPlusTemplate.attr({'hpc_id': v.id, 'hpc_number' : v.number,'hpc_date' : v.expiration_date_date, 'hpc_remarks': v.remarks})
                                        callcenter.call_orders.clone_append(uiHappyPlusTemplate, uiTemplate.find('div.happy_plus_card_container'));
                                    })
                                }

                                /*address*/
                                if (count(value.address) > 0 && count(value.address_text) > 0 && typeof(value.address) !== 'undefined') {
                                    uiTemplate.find('div.customer_address_container').find('div.customer_address:not(.template)').remove();
                                    uiTemplate.find('div.customer_secondary_address').find('div.deliver-to-different').siblings().remove();
                                    if($('div.search_result_container[data-customer-id="' + updateCloneForm.attr('customer_id') + '"]').find('div.customer_secondary_address').is(':visible'))
                                    {
                                        $('div.search_result_container[data-customer-id="' + updateCloneForm.attr('customer_id') + '"]').find('div.customer_secondary_address').toggle();
                                    }

                                    $.each(value.address, function (k, v) {

                                        var house_number = (v.house_number !== '' && v.house_number != "0") ? v.house_number : '',
                                            floor = (v.floor !== '' && v.floor != "0") ? v.floor : '',
                                            building = (value.address_text[k].building !== null) ? value.address_text[k].building.name : '',
                                            barangay = (value.address_text[k].barangay !== null) ? value.address_text[k].barangay.name : '',
                                            subdivision = (value.address_text[k].subdivision !== null) ? value.address_text[k].subdivision.name : '',
                                            city = (value.address_text[k].city !== null) ? value.address_text[k].city.name : '',
                                            street = (value.address_text[k].street !== null) ? value.address_text[k].street.name : '',
                                            second_street = (value.address_text[k].second_street !== null  && typeof(value.address_text[k].second_street) != 'undefined') ? value.address_text[k].second_street.name : '',
                                            province = (value.address_text[k].province !== null) ? value.address_text[k].province.name : '';

                                        var uiAddressTemplate = uiTemplate.find('div.customer_address.template').clone().removeClass('template');

                                        uiAddressTemplate.find('strong.info_address_label').text(v.address_label);
                                        uiAddressTemplate.find('p.info_address_complete').text('' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);
                                        uiAddressTemplate.find('p.info_address_complete').attr('data-address', '/##/' + house_number + '/##/' + building + '/##/' + floor + '/##/' + street + '/##/' + second_street + '/##/' + barangay + '/##/' + subdivision + '/##/' + city + '/##/' + province);
                                        uiAddressTemplate.find('strong.info_address_type').text(v.address_type.toUpperCase());
                                        uiAddressTemplate.find('p.info_address_landmark').text('- ' + v.landmark.toUpperCase());

                                        uiAddressTemplate.attr('customer_id', value.id);
                                        uiAddressTemplate.attr('address_id', v.id);
                                        uiAddressTemplate.attr('house_number', v.house_number);
                                        uiAddressTemplate.attr('building', building);
                                        uiAddressTemplate.attr('floor', v.floor);
                                        uiAddressTemplate.attr('street', v.street);
                                        uiAddressTemplate.attr('second_street', v.second_street);
                                        uiAddressTemplate.attr('barangay', v.barangay);
                                        uiAddressTemplate.attr('subdivision', v.subdivision);
                                        uiAddressTemplate.attr('city', v.city);
                                        uiAddressTemplate.attr('province', v.province);
                                        uiAddressTemplate.attr('all_address', '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);

                                        if (v.is_default == 1) {
                                            // uiAddressTemplate.attr('address_id', v.id);
                                            // uiTemplate.siblings('div.header-search').find('div.remarks_and_address p.address').empty();
                                            // uiTemplate.siblings('div.header-search').find('div.remarks_and_address p.address').append("<span class='red-color info_address'><strong class=''>Address: </strong></span>" + '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);

                                            // /* hidden fields for find rta */
                                            // uiTemplate.siblings('div.header-search').find('input[name="building"]').val(building);
                                            // uiTemplate.siblings('div.header-search').find('input[name="street"]').val(v.street);
                                            // uiTemplate.siblings('div.header-search').find('input[name="barangay"]').val(v.barangay);
                                            // uiTemplate.siblings('div.header-search').find('input[name="subdivision"]').val(v.subdivision);
                                            // uiTemplate.siblings('div.header-search').find('input[name="city"]').val(v.city);
                                            // uiTemplate.siblings('div.header-search').find('input[name="province"]').val(v.province);

                                            // callcenter.call_orders.clone_append(uiAddressTemplate, uiTemplate.find('div.customer_address_container'));

                                            if(v.is_deleted_date != '0000-00-00 00:00:00' && v.is_deleted == 1)
                                            {
                                                var dateNow = moment().format("YYYY-MM-DD"),
                                                    timeNow = moment().format("HH:mm:ss"),
                                                    is_deleted_fulldate = v.is_deleted_date.split(' '),
                                                    is_deleted_date = is_deleted_fulldate[0].split('-'),
                                                    is_deleted_time = is_deleted_fulldate[1].split(':'),
                                                    time_now = timeNow.split(':');

                                                /*deleted time*/
                                                var is_deleted_hour = is_deleted_time[0],
                                                    is_deleted_min = is_deleted_time[1],
                                                    is_deleted_sec = is_deleted_time[2];

                                                /*time now*/
                                                var hour_now = time_now[0],
                                                    min_now = time_now[1],
                                                    sec_now = time_now[2];

                                                if(dateNow == is_deleted_fulldate[0])
                                                {
                                                    if( ( parseInt(hour_now) - parseInt(is_deleted_hour) ) > parseInt('1') || ( parseInt(hour_now) - parseInt(is_deleted_hour) ) == parseInt('1') && parseInt(is_deleted_min) <= parseInt(min_now) )
                                                    {
                                                        // console.log("remove/hide address")
                                                    }
                                                    else
                                                    {
                                                        // console.log("undo");
                                                        uiAddressTemplate.attr('address_id', v.id);
                                                        uiTemplate.siblings('div.header-search').find('div.remarks_and_address p.address').empty();
                                                        uiTemplate.siblings('div.header-search').find('div.remarks_and_address p.address').append("<span class='red-color info_address'><strong class=''>Address: </strong></span>" + '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);

                                                        /* hidden fields for find rta */
                                                        uiTemplate.siblings('div.header-search').find('input[name="building"]').val(building);
                                                        uiTemplate.siblings('div.header-search').find('input[name="street"]').val(v.street);
                                                        uiTemplate.siblings('div.header-search').find('input[name="barangay"]').val(v.barangay);
                                                        uiTemplate.siblings('div.header-search').find('input[name="subdivision"]').val(v.subdivision);
                                                        uiTemplate.siblings('div.header-search').find('input[name="city"]').val(v.city);
                                                        uiTemplate.siblings('div.header-search').find('input[name="province"]').val(v.province);

                                                        callcenter.call_orders.clone_append(uiAddressTemplate, uiTemplate.find('div.customer_address_container'));
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                uiAddressTemplate.attr('address_id', v.id);
                                                uiTemplate.siblings('div.header-search').find('div.remarks_and_address p.address').empty();
                                                uiTemplate.siblings('div.header-search').find('div.remarks_and_address p.address').append("<span class='red-color info_address'><strong class=''>Address: </strong></span>" + '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);

                                                /* hidden fields for find rta */
                                                uiTemplate.siblings('div.header-search').find('input[name="building"]').val(building);
                                                uiTemplate.siblings('div.header-search').find('input[name="street"]').val(v.street);
                                                uiTemplate.siblings('div.header-search').find('input[name="barangay"]').val(v.barangay);
                                                uiTemplate.siblings('div.header-search').find('input[name="subdivision"]').val(v.subdivision);
                                                uiTemplate.siblings('div.header-search').find('input[name="city"]').val(v.city);
                                                uiTemplate.siblings('div.header-search').find('input[name="province"]').val(v.province);

                                                callcenter.call_orders.clone_append(uiAddressTemplate, uiTemplate.find('div.customer_address_container'));
                                            }
                                        }
                                        else {
                                            // // uiAddressTemplate.removeClass('bggray-light').addClass('white');
                                            // uiAddressTemplate.find('a.edit_address').remove();
                                            // uiTemplate.find('div.customer_secondary_address').prepend(uiAddressTemplate);

                                            if(v.is_deleted_date != '0000-00-00 00:00:00' && v.is_deleted == 1)
                                            {
                                                var dateNow = moment().format("YYYY-MM-DD"),
                                                    timeNow = moment().format("HH:mm:ss"),
                                                    is_deleted_fulldate = v.is_deleted_date.split(' '),
                                                    is_deleted_date = is_deleted_fulldate[0].split('-'),
                                                    is_deleted_time = is_deleted_fulldate[1].split(':'),
                                                    time_now = timeNow.split(':');

                                                /*deleted time*/
                                                var is_deleted_hour = is_deleted_time[0],
                                                    is_deleted_min = is_deleted_time[1],
                                                    is_deleted_sec = is_deleted_time[2];

                                                /*time now*/
                                                var hour_now = time_now[0],
                                                    min_now = time_now[1],
                                                    sec_now = time_now[2];

                                                if(dateNow == is_deleted_fulldate[0])
                                                {
                                                    if( ( parseInt(hour_now) - parseInt(is_deleted_hour) ) > parseInt('1') || ( parseInt(hour_now) - parseInt(is_deleted_hour) ) == parseInt('1') && parseInt(is_deleted_min) <= parseInt(min_now) )
                                                    {
                                                        // console.log("remove/hide address")
                                                    }
                                                    else
                                                    {
                                                        // console.log("undo");
                                                        uiAddressTemplate.removeClass('bggray-light').addClass('white');
                                                        uiAddressTemplate.find('a.edit_address').remove();
                                                        uiTemplate.find('div.customer_secondary_address').prepend(uiAddressTemplate);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                uiAddressTemplate.removeClass('bggray-light').addClass('white');
                                                uiAddressTemplate.find('a.edit_address').remove();
                                                uiTemplate.find('div.customer_secondary_address').prepend(uiAddressTemplate); 
                                            }
                                        }

                                        if (v.address_type.length > 0) {
                                            uiAddressTemplate.find('img.info_address_type').attr('src', callcenter.config('url.server.base') + 'assets/images/' + v.address_type.toLocaleLowerCase() + '.png');
                                            uiAddressTemplate.find('img.info_address_type').attr('onError', '$.fn.checkImage(this);');
                                        }
                                    })
                                }
                            })
                        }
                    }
                }
                callcenter.customer_list.get(oConfig);
            }
        },

        'filter_autocomplete_results_object': function (iCityId) {
            //console.log(oAutoComplete);

            for (var i = 0; (oAutoComplete.oPOI.length - 1) >= i; i++) {
                var item = oAutoComplete.oPOI[i];
                var bCheck = item.city_id.indexOf(iCityId);
                if (bCheck != -1) {
                    oAutoComplete.oPOIFiltered.push({"name": item.name, "id": item.id, "city_id": item.city_id});
                }
            }

            for (var i = 0; (oAutoComplete.oStreet.length - 1) >= i; i++) {
                var item = oAutoComplete.oStreet[i];
                var bCheck = item.city_id.indexOf(iCityId);
                if (bCheck != -1) {
                    oAutoComplete.oStreetFiltered.push({"name": item.name, "id": item.id, "city_id": item.city_id});
                }
            }

            for (var i = 0; (oAutoComplete.oSubdivision.length - 1) >= i; i++) {
                var item = oAutoComplete.oSubdivision[i];
                var bCheck = item.city_id.indexOf(iCityId);
                if (bCheck != -1) {
                    oAutoComplete.oSubdivisionFiltered.push({
                        "name"   : item.name,
                        "id"     : item.id,
                        "city_id": item.city_id
                    });
                }
            }

            for (var i = 0; (oAutoComplete.oBarangay.length - 1) >= i; i++) {
                var item = oAutoComplete.oBarangay[i];
                var bCheck = item.city_id.indexOf(iCityId);
                if (bCheck != -1) {
                    oAutoComplete.oBarangayFiltered.push({"name": item.name, "id": item.id, "city_id": item.city_id});
                }
            }

        },

        'render_autocomplete': function (sSearchVal, oAutoCompleteData, uiSearchResults) {
            ////console.log(sSearchFrom);
            var arResults = [];
            for (var i = 0; (oAutoCompleteData.length - 1) >= i; i++) {
                var item = oAutoCompleteData[i];
                var sName = item.name.toLowerCase();
                sSearchVal = sSearchVal.toLowerCase();
                var bCheck = sName.indexOf(sSearchVal);
                if (bCheck != -1) {
                    arResults.push({"name": item.name, "id": item.id});
                }
            }
            uiSearchResults.html("");
            for (var i = 0; (arResults.length - 1) >= i; i++) {
                var item = arResults[i];
                var sHtml = "<div class='list acautocomplete' data-id='" + item.id + "' data-value='" + item.name + "' >" + item.name + "</div>";
                uiSearchResults.append(sHtml);
            }

            if (sSearchVal == "") {
                uiSearchResults.html("");
            }
        },

        'assemble_address_information': function () {
            var uiAddressAddForm = $("div[modal-id='deliver-to-different']").find('form#add_address');
            var oParams = {
                "params": {
                    "id"     : $('#add_address input[name="customer_id"]').val(),
                    "address": {
                        /*"address_id" : $('#add_address input[name="address_id"]').val(),*/
                        "address_type" : uiAddressAddForm.find('input[name="address_type"]').val(),
                        "address_label": uiAddressAddForm.find('input[name="address_label"]').val(),
                        "house_number" : uiAddressAddForm.find('input[name="house_number"]').val(),
                        "street"       : uiAddressAddForm.find('input[name="street"]').attr("value"),
                        "second_street": uiAddressAddForm.find('input[name="second_street"]').attr("value"),
                        "subdivision"  : uiAddressAddForm.find('input[name="subdivision"]').attr("value"),
                        "barangay"     : uiAddressAddForm.find('input[name="barangay"]').attr("value"),
                        "city"         : uiAddressAddForm.find('input[name="city"]').attr("value"),
                        "province"     : uiAddressAddForm.find('input[name="province"]').attr("value"),
                        "building"     : uiAddressAddForm.find('input[name="building"]').attr("value"),
                        "floor"        : uiAddressAddForm.find('input[name="floor"]').val(),
                        "unit"         : uiAddressAddForm.find('input[name="unit"]').val(),
                        "landmark"     : uiAddressAddForm.find('input[name="landmark"]').val(),
                        "is_default"   : uiAddressAddForm.find('input[name="is_default"]:checked').length
                    }
                }
            };

            var oAjaxConfig = {
                "type"   : "Post",
                "data"   : oParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : callcenter.config('url.api.jfc.customers') + "/customers/add_address",
                "success": function (oData) {
                    ////console.log(oData);
                    if (typeof(oData) !== 'undefined') {
                        if (oData.status == false && count(oData.message) > 0) //failed
                        {
                            var uiContainer = $('div.address-error-msg');
                            var sError = '';
                            $.each(oData.message, function (k, v) {
                                sError += '<p class="error_messages"><i class="fa fa-exclamation-triangle"></i> ' + v + '</p>';
                            });
                            callcenter.call_orders.add_error_message(sError, uiContainer);
                        }
                        else {
                            $('div[modal-id="deliver-to-different"]').removeClass('showed');
                            $('body').css('overflow', 'auto');
                            callcenter.call_orders.populate_address(oData.data.address, oData.data.address_text, oParams.params.id);
                            $('section#order-process #customer_list_content').find('button.btn-cancel').trigger('click');
                            $('section.content #search_customer_content').find('button.btn-cancel').trigger('click');
                        }
                        callcenter.call_orders.show_spinner($('button.add_address_button'), false);
                    }
                }
            };

            callcenter.call_orders.get(oAjaxConfig)
        },

        'assemble_update_address_information': function () {
            var uiAddressAddForm = $("div[modal-id='deliver-to-different']").find('form#add_address');
            var oParams = {
                "params": {
                    "id"         : $('#add_address input[name="customer_id"]').val(),
                    "address_id" : $('#add_address input[name="address_id"]').val(),
                    "address": {
                        // "address_id" : $('#add_address input[name="address_id"]').val(),
                        "address_type" : uiAddressAddForm.find('input[name="address_type"]').val(),
                        "address_label": uiAddressAddForm.find('input[name="address_label"]').val(),
                        "house_number" : uiAddressAddForm.find('input[name="house_number"]').val(),
                        "street"       : uiAddressAddForm.find('input[name="street"]').attr("value"),
                        "second_street": uiAddressAddForm.find('input[name="second_street"]').attr("value"),
                        "subdivision"  : uiAddressAddForm.find('input[name="subdivision"]').attr("value"),
                        "barangay"     : uiAddressAddForm.find('input[name="barangay"]').attr("value"),
                        "city"         : uiAddressAddForm.find('input[name="city"]').attr("value"),
                        "province"     : uiAddressAddForm.find('input[name="province"]').attr("value"),
                        "building"     : uiAddressAddForm.find('input[name="building"]').attr("value"),
                        "floor"        : uiAddressAddForm.find('input[name="floor"]').val(),
                        "unit"         : uiAddressAddForm.find('input[name="unit"]').val(),
                        "landmark"     : uiAddressAddForm.find('input[name="landmark"]').val(),
                        "is_default"   : uiAddressAddForm.find('input[name="is_default"]:checked').length
                    }
                }
            };

            var oAjaxConfig = {
                "type"   : "Post",
                "data"   : oParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : callcenter.config('url.api.jfc.customers') + "/customers/add_address",
                "success": function (oData) {
                    // console.log(oData);
                    if (typeof(oData) !== 'undefined') {
                        if (oData.status == false && count(oData.message) > 0) //failed
                        {
                            var uiContainer = $('div.address-error-msg');
                            var sError = '';
                            $.each(oData.message, function (k, v) {
                                sError += '<p class="error_messages"><i class="fa fa-exclamation-triangle"></i> ' + v + '</p>';
                            });
                            callcenter.call_orders.add_error_message(sError, uiContainer);
                        }
                        else {
                            $('div[modal-id="deliver-to-different"]').removeClass('showed');
                            $('body').css('overflow', 'auto');
                            // callcenter.call_orders.populate_address(oData.data.address, oData.data.address_text, oParams.params.id);
                            $('section#order-process #customer_list_content').find('button.btn-cancel').trigger('click');
                            $('section.content #search_customer_content').find('button.btn-cancel').trigger('click');
                        }
                        callcenter.call_orders.show_spinner($('button.update_address_button'), false);
                    }
                }
            };

            callcenter.call_orders.get(oAjaxConfig)
        },

        'assemble_search_param': function () {
            $('div.list').remove();
            var oParams = {
                "params": {
                    "where_like": [
                        {
                            "field"   : function () {
                                var uiSearchByValue = $('section#add_new_customer_header input[name="search_by"]').val();
                                if (uiSearchByValue == "Contact Number") {
                                    uiSearchByValue = 'ccn.number';
                                }
                                else if (uiSearchByValue == "Order ID") {
                                    uiSearchByValue = 'o.id';
                                }
                                else {
                                    uiSearchByValue = 'c.first_name';
                                }

                                return uiSearchByValue;
                            },
                            "operator": "LIKE",
                            "value"   : function () {
                                return $('section#add_new_customer_header #search_customer').val()
                            }
                        }
                    ]

                }
            };

            if ($("section #add_new_customer_header #search_customer_select_province").find("input.dd-txt").attr("value") != "") {
                oParams.params.where = [
                    {
                        "field"   : 'ca.province',
                        "operator": "=",
                        "value"   : function () {
                            return $("section #add_new_customer_header #search_customer_select_province").find("input.dd-txt").attr("value");
                        }
                    }
                ]
            }

            var oAjaxConfig = {
                "type"   : "Get",
                "data"   : oParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : callcenter.config('url.api.jfc.customers') + "/customers/search",
                "success": function (data) {
                    callcenter.call_orders.show_spinner($('button.search_button'), false);
                    //clear search box
                    $('section#add_new_customer_header #search_customer').val("");

                    ////console.log(data);
                    if(typeof(data) !== 'undefined')
                    {
                        if (count(data.data) > 0) {
                            $('p.count_search_result strong').text('Search Result(s) | ' + count(data.data) + ' Customers');
                            callcenter.call_orders.content_panel_toggle('search_customer');
                            var uiContainer = $('div.search_customer');
                            uiContainer.html('');
                            $.each(data.data, function (key, value) {
                                var uiTemplate = $('div.search_result_container.template').clone().removeClass('template');
                                var uiManipulatedTemplate = callcenter.call_orders.manipulate_template(uiTemplate, value);
                                uiManipulatedTemplate.find('div.customer_secondary_address').hide();
                                callcenter.call_orders.clone_append(uiManipulatedTemplate, uiContainer);
                            })
                        }
                        else {
                            $('p.count_search_result strong').text('No Search Result');
                            callcenter.call_orders.content_panel_toggle('no_search_customer');
                        }
                    }
                    //add class active on coordinator side
                    $('header ul#main_nav_headers li.order_management').addClass('active');
                }
            };

            callcenter.call_orders.get(oAjaxConfig);
        },

        'show_retail': function () {

        },

        'populate_address': function (v, address_text, customer_id) {
            if(typeof(v) !== 'undefined' && typeof(address_text) !== 'undefined' && typeof(customer_id) !== 'undefined')
            {            
                var uiTemplate = $('div.search_result_container.template').clone().removeClass('template');

                var house_number = (v.house_number !== '' && v.house_number != "0") ? v.house_number : '',
                    floor = (v.floor !== '' && v.floor != "0") ? v.floor : '',
                    building = (address_text.building !== null) ? address_text.building.name : '',
                    barangay = (address_text.barangay !== null) ? address_text.barangay.name : '',
                    subdivision = (address_text.subdivision !== null) ? address_text.subdivision.name : '',
                    city = (address_text.city !== null) ? address_text.city.name : '',
                    street = (address_text.street !== null) ? address_text.street.name : '',
                    second_street = (address_text.second_street !== null && typeof(address_text.second_street) != 'undefined') ? address_text.second_street.name : '',
                    province = (address_text.province !== null) ? address_text.province.name : '';

                var uiAddressTemplate = uiTemplate.find('div.customer_address.template').clone().removeClass('template');
                uiAddressTemplate.attr('address_id', v.id);
                uiAddressTemplate.find('strong.info_address_label').text(v.address_label);
                uiAddressTemplate.find('p.info_address_complete').text('' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);
                uiAddressTemplate.find('p.info_address_complete').attr('data-address', '/##/' + house_number + '/##/' + building + '/##/' + floor + '/##/' + street + '/##/' + second_street + '/##/' + barangay + '/##/' + subdivision + '/##/' + city + '/##/' + province);
                uiAddressTemplate.find('strong.info_address_type').text(v.address_type.toUpperCase());
                uiAddressTemplate.find('p.info_address_landmark').text('- ' + v.landmark.toUpperCase());


                if (v.address_type.length > 0) {
                    uiAddressTemplate.find('img.info_address_type').attr('src', callcenter.config('url.server.base') + 'assets/images/' + v.address_type.toLocaleLowerCase() + '.png');
                    uiAddressTemplate.find('img.info_address_type').attr('onError', '$.fn.checkImage(this);');
                }

                if (v.is_default == 1) {
                    uiAddressTemplate.removeClass('white').addClass('bggray-light')
                    $('div.search_result_container[data-customer-id="' + customer_id + '"]').find('div.customer_secondary_address').toggle();
                    var oOldPrimary = $('div.search_result_container[data-customer-id="' + customer_id + '"]').find('div.customer_address_container').find('div.customer_address:visible').clone();
                    oOldPrimary.find('div.edit_container').remove();
                    oOldPrimary.removeClass('bggray-light').addClass('white');
                    $('div.search_result_container[data-customer-id="' + customer_id + '"]').find('div.customer_address_container').find('div.customer_address:visible').replaceWith(uiAddressTemplate);
                    $('div.search_result_container[data-customer-id="' + customer_id + '"]').find('div.customer_secondary_address').prepend(oOldPrimary)
                }
                else {
                    uiAddressTemplate.find('div.edit_container').remove();

                    $('div.search_result_container[data-customer-id="' + customer_id + '"]').find('div.customer_secondary_address').prepend(uiAddressTemplate)
                }
            }
        },

        'manipulate_template': function (uiTemplate, oSearchResult) {
            if (typeof(uiTemplate) !== 'undefined' && typeof(oSearchResult) !== 'undefined') {
                var i = oSearchResult;
                var uiManipulatedTemplate = uiTemplate;
                //console.log(i)
                uiManipulatedTemplate.attr({'data-customer-id': i.id, 'is_pwd': i.is_pwd, 'is_senior': i.is_senior});
                uiManipulatedTemplate.find("div.second_header").attr('data-customer-id', i.id);
                uiManipulatedTemplate.find('strong.last_order_date').text(moment(i.last_order_date).format('MMMM DD, YYYY | hh:mm a'));
                /*name*/
                uiManipulatedTemplate.find('strong.info_name').text(i.first_name + ' ' + i.middle_name + ' ' + i.last_name);

                /*remarks*/
                // callcenter.customer_list.get_remarks('', i.last_behavior_id, uiManipulatedTemplate);
                // if (count(i.remarks) > 0) {
                //     $.each(i.remarks, function (k, v) {
                //         if(v.id == i.last_behavior_id)
                //         {
                //             uiManipulatedTemplate.find('span.info-remarks').text(v.display_label.toUpperCase());
                //             uiManipulatedTemplate.find('span.info-remarks').css('color', '#' + v.color);
                //             uiManipulatedTemplate.siblings('div.header-search').find('span.info-remarks').text(v.display_label.toUpperCase());
                //             uiManipulatedTemplate.siblings('div.header-search').find('span.info-remarks').css('color', '#' + v.color);
                //         }
                //     })
                // }
                // else {
                //     uiManipulatedTemplate.find('span.info-remarks').text('NO REMARKS AVAILABLE');
                //     uiManipulatedTemplate.find('span.info-remarks').css('color', '#000000');
                //     uiManipulatedTemplate.siblings('div.header-search').find('span.info-remarks').text('NO REMARKS AVAILABLE');
                //     uiManipulatedTemplate.siblings('div.header-search').find('span.info-remarks').css('color', '#000000');
                // }
                if (i.color != null && i.display_label != null) {
                    uiManipulatedTemplate.find('span.info-remarks').text(i.display_label.toUpperCase());
                    uiManipulatedTemplate.find('span.info-remarks').css('color', '#' + i.color);
                    uiManipulatedTemplate.siblings('div.header-search').find('span.info-remarks').text(i.display_label.toUpperCase());
                    uiManipulatedTemplate.siblings('div.header-search').find('span.info-remarks').css('color', '#' + i.color);
                }
                else {
                    uiManipulatedTemplate.find('span.info-remarks').text('NO REMARKS AVAILABLE');
                    uiManipulatedTemplate.find('span.info-remarks').css('color', '#000000');
                    uiManipulatedTemplate.siblings('div.header-search').find('span.info-remarks').text('NO REMARKS AVAILABLE');
                    uiManipulatedTemplate.siblings('div.header-search').find('span.info-remarks').css('color', '#000000');
                }
                //last_payment_method_used 
                uiManipulatedTemplate.find('strong.last_payment_method_used').text("");

                /*address*/
                if (count(i.address) > 0 && count(i.address_text) > 0 && typeof(i.address) !== 'undefined') {
                    /*default address*/
                    ////console.log(i.address_text);
                    var house_number = (i.address[0].house_number !== '' && i.address[0].house_number != "0") ? i.address[0].house_number : '',
                        floor = (i.address[0].floor !== '' && i.address[0].floor != "0") ? i.address[0].floor : '',
                        building = (i.address_text[0].building !== null) ? i.address_text[0].building.name : '',
                        barangay = (i.address_text[0].barangay !== null) ? i.address_text[0].barangay.name : '',
                        subdivision = (i.address_text[0].subdivision !== null) ? i.address_text[0].subdivision.name : '',
                        city = (i.address_text[0].city !== null) ? i.address_text[0].city.name : '',
                        street = (i.address_text[0].street !== null) ? i.address_text[0].street.name : '',
                        second_street = (i.address_text[0].second_street !== null && typeof(i.address_text[0].second_street) != 'undefined') ? i.address_text[0].second_street.name : '',
                        province = (i.address_text[0].province !== null) ? i.address_text[0].province.name : '';

                    /*other address*/
                    $.each(i.address, function (k, v) {

                        var house_number = (v.house_number !== '' && v.house_number != "0") ? v.house_number : '',
                            floor = (v.floor !== '' && v.floor != "0") ? v.floor : '',
                            building = (i.address_text[k].building !== null) ? i.address_text[k].building.name : '',
                            barangay = (i.address_text[k].barangay !== null) ? i.address_text[k].barangay.name : '',
                            subdivision = (i.address_text[k].subdivision !== null) ? i.address_text[k].subdivision.name : '',
                            city = (i.address_text[k].city !== null) ? i.address_text[k].city.name : '',
                            street = (i.address_text[k].street !== null) ? i.address_text[k].street.name : '',
                            second_street = (i.address_text[k].second_street !== null && typeof(i.address_text[k].second_street) != 'undefined') ? i.address_text[k].second_street.name : '',
                            province = (i.address_text[k].province !== null) ? i.address_text[k].province.name : '';

                        var uiAddressTemplate = uiTemplate.find('div.customer_address.template').clone().removeClass('template');
                        ////console.log(uiAddressTemplate)
                        uiAddressTemplate.find('strong.info_address_label').text(v.address_label);
                        uiAddressTemplate.find('p.info_address_complete').text('' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);
                        uiAddressTemplate.find('p.info_address_complete').attr('data-address', '/##/' + house_number + '/##/' + building + '/##/' + floor + '/##/' + street + '/##/' + second_street + '/##/' + barangay + '/##/' + subdivision + '/##/' + city + '/##/' + province);
                        uiAddressTemplate.find('strong.info_address_type').text(v.address_type.toUpperCase());
                        uiAddressTemplate.find('p.info_address_landmark').text('- ' + v.landmark.toUpperCase());

                        uiAddressTemplate.attr('customer_id', i.id);
                        uiAddressTemplate.attr('address_id', v.id);
                        uiAddressTemplate.attr('house_number', v.house_number);
                        uiAddressTemplate.attr('building', building);
                        uiAddressTemplate.attr('floor', v.floor);
                        uiAddressTemplate.attr('street', v.street);
                        uiAddressTemplate.attr('second_street', v.second_street);
                        uiAddressTemplate.attr('barangay', v.barangay);
                        uiAddressTemplate.attr('store_id', v.store_id);
                        uiAddressTemplate.attr('subdivision', v.subdivision);
                        uiAddressTemplate.attr('city', v.city);
                        uiAddressTemplate.attr('province', v.province);
                        uiAddressTemplate.attr('delivery_time', v.delivery_time);
                        uiAddressTemplate.attr('all_address', '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);

                        if (v.is_default == 1) {
                            // uiAddressTemplate.attr('address_id', v.id);
                            // uiManipulatedTemplate.find('span.info_address').after('' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);

                            // /* hidden fields for find rta */
                            // uiManipulatedTemplate.find('input[name="house_number"]').val(v.house_number);
                            // uiManipulatedTemplate.find('input[name="building"]').val(building);
                            // uiManipulatedTemplate.find('input[name="floor"]').val(v.floor);
                            // uiManipulatedTemplate.find('input[name="street"]').val(v.street);
                            // uiManipulatedTemplate.find('input[name="barangay"]').val(v.barangay);
                            // uiManipulatedTemplate.find('input[name="subdivision"]').val(v.subdivision);
                            // uiManipulatedTemplate.find('input[name="city"]').val(v.city);
                            // uiManipulatedTemplate.find('input[name="province"]').val(v.province);

                            // callcenter.call_orders.clone_append(uiAddressTemplate, uiTemplate.find('div.customer_address_container'));

                            if(v.is_deleted_date != '0000-00-00 00:00:00' && v.is_deleted == 1)
                            {
                                var dateNow = moment().format("YYYY-MM-DD"),
                                    timeNow = moment().format("HH:mm:ss"),
                                    is_deleted_fulldate = v.is_deleted_date.split(' '),
                                    is_deleted_date = is_deleted_fulldate[0].split('-'),
                                    is_deleted_time = is_deleted_fulldate[1].split(':'),
                                    time_now = timeNow.split(':');

                                /*deleted time*/
                                var is_deleted_hour = is_deleted_time[0],
                                    is_deleted_min = is_deleted_time[1],
                                    is_deleted_sec = is_deleted_time[2];

                                /*time now*/
                                var hour_now = time_now[0],
                                    min_now = time_now[1],
                                    sec_now = time_now[2];

                                if(dateNow == is_deleted_fulldate[0])
                                {
                                    if( ( parseInt(hour_now) - parseInt(is_deleted_hour) ) > parseInt('1') || ( parseInt(hour_now) - parseInt(is_deleted_hour) ) == parseInt('1') && parseInt(is_deleted_min) <= parseInt(min_now) )
                                    {
                                        // console.log("remove/hide address")
                                    }
                                    else
                                    {
                                        // console.log("undo");
                                        uiAddressTemplate.attr('address_id', v.id);
                                        uiAddressTemplate.attr('store_id', v.store_id);
                                        uiManipulatedTemplate.find('span.info_address').after('' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);

                                        /* hidden fields for find rta */
                                        uiManipulatedTemplate.find('input[name="house_number"]').val(v.house_number);
                                        uiManipulatedTemplate.find('input[name="building"]').val(building);
                                        uiManipulatedTemplate.find('input[name="floor"]').val(v.floor);
                                        uiManipulatedTemplate.find('input[name="street"]').val(v.street);
                                        uiManipulatedTemplate.find('input[name="barangay"]').val(v.barangay);
                                        uiManipulatedTemplate.find('input[name="subdivision"]').val(v.subdivision);
                                        uiManipulatedTemplate.find('input[name="city"]').val(v.city);
                                        uiManipulatedTemplate.find('input[name="province"]').val(v.province);

                                        callcenter.call_orders.clone_append(uiAddressTemplate, uiTemplate.find('div.customer_address_container'));
                                    }
                                }
                            }
                            else
                            {
                                uiAddressTemplate.attr('address_id', v.id);
                                uiAddressTemplate.attr('store_id', v.store_id);
                                uiManipulatedTemplate.find('span.info_address').after('' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);

                                /* hidden fields for find rta */
                                uiManipulatedTemplate.find('input[name="house_number"]').val(v.house_number);
                                uiManipulatedTemplate.find('input[name="building"]').val(building);
                                uiManipulatedTemplate.find('input[name="floor"]').val(v.floor);
                                uiManipulatedTemplate.find('input[name="street"]').val(v.street);
                                uiManipulatedTemplate.find('input[name="barangay"]').val(v.barangay);
                                uiManipulatedTemplate.find('input[name="subdivision"]').val(v.subdivision);
                                uiManipulatedTemplate.find('input[name="city"]').val(v.city);
                                uiManipulatedTemplate.find('input[name="province"]').val(v.province);

                                if(v.store_id > 0)
                                {
                                   var oStoreInformation = callcenter.call_orders.fetch_store_information(v.store_id, uiTemplate, v.delivery_time);    
                                }
                                
                                

                                callcenter.call_orders.clone_append(uiAddressTemplate, uiTemplate.find('div.customer_address_container'));
                            }
                        }
                        else {

                            // uiAddressTemplate.removeClass('bggray-light').addClass('white');
                            // uiAddressTemplate.find('a.edit_address').remove();
                            // uiTemplate.find('div.customer_secondary_address').prepend(uiAddressTemplate);

                            //callcenter.call_orders.clone_append(uiAddressTemplate, uiTemplate.find('div.customer_secondary_address'));

                            if(v.is_deleted_date != '0000-00-00 00:00:00' && v.is_deleted == 1)
                            {
                                var dateNow = moment().format("YYYY-MM-DD"),
                                    timeNow = moment().format("HH:mm:ss"),
                                    is_deleted_fulldate = v.is_deleted_date.split(' '),
                                    is_deleted_date = is_deleted_fulldate[0].split('-'),
                                    is_deleted_time = is_deleted_fulldate[1].split(':'),
                                    time_now = timeNow.split(':');

                                /*deleted time*/
                                var is_deleted_hour = is_deleted_time[0],
                                    is_deleted_min = is_deleted_time[1],
                                    is_deleted_sec = is_deleted_time[2];

                                /*time now*/
                                var hour_now = time_now[0],
                                    min_now = time_now[1],
                                    sec_now = time_now[2];

                                if(dateNow == is_deleted_fulldate[0])
                                {
                                    if( ( parseInt(hour_now) - parseInt(is_deleted_hour) ) > parseInt('1') || ( parseInt(hour_now) - parseInt(is_deleted_hour) ) == parseInt('1') && parseInt(is_deleted_min) <= parseInt(min_now) )
                                    {
                                        // console.log("remove/hide address")
                                    }
                                    else
                                    {
                                        // console.log("undo");
                                        uiAddressTemplate.removeClass('bggray-light').addClass('white');
                                        uiAddressTemplate.find('a.edit_address').remove();
                                        uiTemplate.find('div.customer_secondary_address').prepend(uiAddressTemplate);
                                    }
                                }
                            }
                            else
                            {
                                uiAddressTemplate.removeClass('bggray-light').addClass('white');
                                uiAddressTemplate.find('a.edit_address').remove();
                                uiTemplate.find('div.customer_secondary_address').prepend(uiAddressTemplate);
                            }
                        }


                        if (v.address_type.length > 0) {
                            uiAddressTemplate.find('img.info_address_type').attr('src', callcenter.config('url.server.base') + 'assets/images/' + v.address_type.toLocaleLowerCase() + '.png');
                            uiAddressTemplate.find('img.info_address_type').attr('onError', '$.fn.checkImage(this);');
                        }
                    })

                }


                /*contact numbers*/
                if (count(i.contact_number) > 0 && typeof(i.contact_number) !== 'undefined') {
                    var fCarrier = '';
                    if (typeof(i.contact_number[0].name) != 'undefined')
                    {
                        if(i.contact_number[0].number.indexOf('-') != -1)
                        {
                            fCarrier = ''; //if landline put none in carrier prefix
                        }
                        else
                        {
                            fCarrier = ((i.contact_number[0].name !== 'None')? i.contact_number[0].name : 'Unknown'); //if mobile put unknown
                        }
                    }
                    uiManipulatedTemplate.find('strong.contact_number').text('Contact Num: ' + i.contact_number[0].number + ' ' + fCarrier);

                    var sNumbers = '';
                    var fNumber = '';
                    $.each(oSearchResult.contact_number, function (k, v) {
                        var sCarrier = '';
                        if (typeof(v.name) != 'undefined') {
                            if(v.number.indexOf('-') != -1)
                            {
                                sCarrier = 'Landline'; //if landline put none in carrier prefix
                            }
                            else
                            {
                                sCarrier = ((v.name !== 'None') ? v.name : 'Unknown' );  //if mobile put unknown
                            }
                        }
                        if (k == 0) {
                            uiManipulatedTemplate.find('div.contact_number input.dd-txt').val(v.number + " " + sCarrier);
                        }
                        else if (k == 1) {
                            uiManipulatedTemplate.find('div.alternate_contact_number input.dd-txt').val(v.number + " " + sCarrier);
                        }

                        sNumbers += '<div class="option" data-value="' + v.number + '"><span class="">' + v.number + '</span> ' + sCarrier + ' </div>'

                    })

                    var oDivs = uiManipulatedTemplate.find('div.data-container:first div.frm-custom-dropdown');
                    $.each(oDivs, function (k, v) {
                        if (k == 1 || k == 0) {
                            $(this).find('div.frm-custom-dropdown-option').html('').append(sNumbers);
                        }
                    })


                }

                /*email address*/
                uiManipulatedTemplate.find('p.email_address').text(i.email_address);

                /*happy plus cards*/
                if (count(i.happy_plus_card) > 0 && typeof(i.happy_plus_card) !== 'undefined') {
                    $.each(i.happy_plus_card, function (k, v) {
                        var uiHappyPlusTemplate = uiTemplate.find('div.happy_plus_card.template').clone().removeClass('template');
                        uiHappyPlusTemplate.find('p.info-happy-plus-number').text(v.number);
                        uiHappyPlusTemplate.find('p.expiration_date').text(v.expiration_date_date);
                        uiHappyPlusTemplate.attr({'hpc_id': v.id, 'hpc_number' : v.number,'hpc_date' : v.expiration_date_date, 'hpc_remarks': v.remarks})
                        callcenter.call_orders.clone_append(uiHappyPlusTemplate, uiTemplate.find('div.happy_plus_card_container'));
                    })
                }


                //uiManipulatedTemplate.find('strong.info_address').text('Address: ' + i.first_name + ' ' + i.middle_name + ' ' + i.last_name);

                return uiManipulatedTemplate;
            }
        },

        'show_seach_result_collapse': function (uiElem) {
            $(this).find('div.collapsible').slideToggle("slow");
        },

        'fetch_store_information' : function(iStoreID, uiTemplate, iMinutes){
            var oStoreInformation = {};
            if(typeof(iStoreID) != 'undefined')
            {
                var oParams = {
                    "params" : {
                        "where" : [{
                            "field" : "s.id",
                            "operator" : "=",
                            "value": iStoreID
                        }],
                        "sbu_id": 1,
                        "limit" : 1
                    }
                }
                

                
                var oAjaxConfig = {
                    "type"   : "Get",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.store') + "/stores/get",
                    "success": function (oData) {
                        var sMinutes = '';
                        if(typeof(oData) != 'undefined')
                        {
                            if(typeof(oData.data) != 'undefined')
                            {
                                var bAvailability = callcenter.gis.check_store_availability(iStoreID);
                                if (bAvailability) {
                                    uiTemplate.find(".retail-store-name").html(oData.data[0].name).attr({
                                        'store_code': oData.data[0].code,
                                        'store_id'  : oData.data[0].id,
                                        'store_name': oData.data[0].name,
                                        'store_time': iMinutes
                                    }).css('color', 'black');
                                    uiTemplate.find('span.retail-store-time-label').text('Delivery Time:')
                                    uiTemplate.find('span.retail-store-time').text(iMinutes + ' Minutes')
                                    callcenter.gis.rta_return(1, iStoreID)
                                }
                                else {
                                    uiTemplate.find(".retail-store-name").html(oData.data[0].name + " (Offline)").attr({
                                        'store_code': oData.data[0].code,
                                        'store_id'  : oData.data[0].id,
                                        'store_name': oData.data[0].name,
                                        'store_time': iMinutes
                                    }).css('color', 'red');
                                    uiTemplate.find('span.retail-store-time-label').text('Delivery Time:')
                                    uiTemplate.find('span.retail-store-time').text(iMinutes + ' Minutes')
                                    callcenter.gis.rta_return(4, iStoreID)
                                }
                            }

                            uiTemplate.find('div.found-retail').show();
                            uiTemplate.find('div.find-rta-container').hide();
                        }
                    }
                };

                callcenter.call_orders.get(oAjaxConfig);

            }

        },

        'show_spinner': function (uiElem, bShow) {
            if (bShow == true) {
                if(uiElem.find('i.fa-spinner').length > 0)
                {
                    uiElem.find('i.fa-spinner').removeClass('hidden');
                }
                else
                {
                    uiElem.append('<i class="fa fa-spinner fa-pulse hidden"></i>').removeClass('hidden');
                }

                uiElem.prop('disabled', true);
            }
            else {
                if(uiElem.find('i.fa-spinner').length > 0)
                {
                    uiElem.find('i.fa-spinner').addClass('hidden');
                }
                uiElem.prop('disabled', false);
            }
        },

        'get': function (oAjaxConfig) {
            if (typeof(oAjaxConfig) != 'undefined') {
                callcenter.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        'content_panel_toggle': function (uiString) {
            var uiContents = $('section#order-process[section-style="content-panel"]').children('div.row');
            var uiNavs = $('ul.main_nav li');
            var uiSubNav = $('div.sub-nav.' + uiString);
            ////console.log(uiContents)
            $.each(uiNavs, function () {
                $(this).removeClass('active')

                if ($(this).hasClass(uiString)) {
                    $(this).addClass('active');
                }
            })

            $.each(uiContents, function () {
                $(this).addClass('hidden');
                $('#' + uiString + '_content').removeClass('hidden');
            });

            if (uiSubNav.length > 0) {
                uiSubNav.show();
            }

        },

        'populate_order_history': function (oOrders, uiContainer) {
            if (typeof(oOrders) !== 'undefined' && typeof(uiContainer) !== 'undefined') {
                var sHtml = '';
                uiContainer.find('div.order_history_container').children('div.order_history:not(.template)').remove()
                
                $.each(oOrders, function (key, value) {
                    var oDisplayData = $.parseJSON(value.display_data);

                    var dateAdded = (value.date_added.indexOf('|') !== -1) ? value.date_added.split('|').join('') : value.date_added;
                    dateAdded = moment(dateAdded).format('MMMM DD, YYYY | hh:mm a');
                    if(key == 0)
                    {
                        $('input[name="date-selected"]:visible').val(dateAdded)
                    }

                    sHtml += '<div data-value="' + value.id + '" class="option order_date_option">' + dateAdded + '</div>';
                    var uiHistoryTemplate = $('div.order_history.template').clone().removeClass('template');
                    var iTotalCost = 0;
                    var iVATtotal = 0;
                    var iTotalBill = 0;

                    var last_payment_method_used = "None";
                    if(value.payment_mode){
                        last_payment_method_used = value.payment_mode;
                    }

                    uiContainer.find('div.order_history').find("strong.last_payment_method_used").text(last_payment_method_used);
                    
                    if(typeof(oDisplayData.products) !== 'undefined')
                    {
                        $.each(oDisplayData.products, function (key, oItemBreakDown) {
                        if(oItemBreakDown != "" && oItemBreakDown !== null)
                        {
                                var uiBreakdownItemTemplate = uiHistoryTemplate.find('tr.product-breakdown-item.template').clone().removeClass('template');
                                uiBreakdownItemTemplate.find('td.product-name').text(oItemBreakDown.product_name);
                                uiBreakdownItemTemplate.find('td.product-quantity').text(oItemBreakDown.quantity);

                                uiBreakdownItemTemplate.find('td.product-price').text(oItemBreakDown.price + ' PHP');
                                uiBreakdownItemTemplate.find('td.product-total').text(oItemBreakDown.sub_total + ' PHP');

                                uiBreakdownItemTemplate.attr('data-product-quantity', oItemBreakDown.quantity);
                                uiBreakdownItemTemplate.attr('data-product-name', oItemBreakDown.product_name);
                                uiBreakdownItemTemplate.attr('data-main-product-id', oItemBreakDown.main_product_id);
                                uiBreakdownItemTemplate.attr('data-product-id', oItemBreakDown.item_id);
                                uiBreakdownItemTemplate.attr('data-product-price', oItemBreakDown.price);
                                uiBreakdownItemTemplate.attr('data-product-row-total', (oItemBreakDown.price * oItemBreakDown.quantity).toFixed(2));
                                uiHistoryTemplate.find('tbody.cart-container').append(uiBreakdownItemTemplate);

                                if(typeof(oItemBreakDown.addons) != 'undefined')
                                {
                                     $.each(oItemBreakDown.addons, function(key, oAddonBreakDown) {
                                                var uiBreakdownItemTemplate = uiHistoryTemplate.find('tr.product-breakdown-item.template').clone().removeClass('template');
                                                uiBreakdownItemTemplate.find('td.product-name').text(oAddonBreakDown.product_name);
                                                uiBreakdownItemTemplate.find('td.product-quantity').text(oAddonBreakDown.quantity);

                                                uiBreakdownItemTemplate.find('td.product-price').text(oAddonBreakDown.price + ' PHP');
                                                uiBreakdownItemTemplate.find('td.product-total').text(oAddonBreakDown.sub_total + ' PHP');

                                                uiBreakdownItemTemplate.attr('data-product-quantity', oAddonBreakDown.quantity);
                                                uiBreakdownItemTemplate.attr('data-product-name', oAddonBreakDown.product_name);
                                                uiBreakdownItemTemplate.attr('data-main-product-id', oAddonBreakDown.main_product_id);
                                                uiBreakdownItemTemplate.attr('data-product-id', oAddonBreakDown.item_id);
                                                uiBreakdownItemTemplate.attr('data-product-price', oAddonBreakDown.price);
                                                uiBreakdownItemTemplate.attr('data-product-row-total', (oAddonBreakDown.price * oAddonBreakDown.quantity).toFixed(2));
                                                uiHistoryTemplate.find('tbody.cart-container').append(uiBreakdownItemTemplate);  
                                        })   
                                }

                                if(typeof(oItemBreakDown.special) != 'undefined')
                                {
                                     $.each(oItemBreakDown.special, function(key, oSpecialItemBreakDown) {
                                                var uiBreakdownItemTemplate = uiHistoryTemplate.find('tr.product-breakdown-item.template').clone().removeClass('template');
                                                uiBreakdownItemTemplate.find('td.product-name').text(oSpecialItemBreakDown.product_name);
                                                uiBreakdownItemTemplate.find('td.product-quantity').text(oSpecialItemBreakDown.quantity);

                                                uiBreakdownItemTemplate.find('td.product-price').text(oSpecialItemBreakDown.price + ' PHP');
                                                uiBreakdownItemTemplate.find('td.product-total').text(oSpecialItemBreakDown.sub_total + ' PHP');

                                                uiBreakdownItemTemplate.attr('data-product-quantity', oSpecialItemBreakDown.quantity);
                                                uiBreakdownItemTemplate.attr('data-product-name', oSpecialItemBreakDown.product_name);
                                                uiBreakdownItemTemplate.attr('data-main-product-id', oSpecialItemBreakDown.main_product_id);
                                                uiBreakdownItemTemplate.attr('data-product-id', oSpecialItemBreakDown.item_id);
                                                uiBreakdownItemTemplate.attr('data-special-product-id', oSpecialItemBreakDown.item_id);
                                                uiBreakdownItemTemplate.attr('data-product-price', oSpecialItemBreakDown.price);
                                                uiBreakdownItemTemplate.attr('data-product-row-total', (oSpecialItemBreakDown.price * oSpecialItemBreakDown.quantity).toFixed(2));
                                                uiBreakdownItemTemplate.attr('data-special-item', 1);
                                                uiHistoryTemplate.find('tbody.cart-container').append(uiBreakdownItemTemplate);  
                                        })   
                                }

                                if(typeof(oItemBreakDown.upgrades) != 'undefined')
                                {
                                     $.each(oItemBreakDown.upgrades, function(key, oUpgradesItemBreakDown) {
                                                var uiBreakdownItemTemplate = uiHistoryTemplate.find('tr.product-breakdown-item.template').clone().removeClass('template');
                                                uiBreakdownItemTemplate.find('td.product-name').text(oUpgradesItemBreakDown.product_name);
                                                uiBreakdownItemTemplate.find('td.product-quantity').text(oUpgradesItemBreakDown.quantity);

                                                uiBreakdownItemTemplate.find('td.product-price').text(oUpgradesItemBreakDown.price + ' PHP');
                                                uiBreakdownItemTemplate.find('td.product-total').text(oUpgradesItemBreakDown.sub_total + ' PHP');

                                                uiBreakdownItemTemplate.attr('data-product-quantity', oUpgradesItemBreakDown.quantity);
                                                uiBreakdownItemTemplate.attr('data-product-name', oUpgradesItemBreakDown.product_name);
                                                uiBreakdownItemTemplate.attr('data-main-product-id', oUpgradesItemBreakDown.main_product_id);
                                                uiBreakdownItemTemplate.attr('data-product-id', oUpgradesItemBreakDown.item_id);
                                                uiBreakdownItemTemplate.attr('data-upgrade-product-id', oUpgradesItemBreakDown.item_id);
                                                uiBreakdownItemTemplate.attr('data-product-price', oUpgradesItemBreakDown.price);
                                                uiBreakdownItemTemplate.attr('data-product-row-total', (oUpgradesItemBreakDown.price * oUpgradesItemBreakDown.quantity).toFixed(2));
                                                uiBreakdownItemTemplate.attr('data-upgrade', 1);
                                                uiHistoryTemplate.find('tbody.cart-container').append(uiBreakdownItemTemplate);  
                                        })   
                                }
                                
                        }
                        

                        })     
                    }
                   
                    uiHistoryTemplate.find('td.total-vat').text(parseFloat(oDisplayData.added_vat).toFixed(2) + ' PHP');
                    uiHistoryTemplate.find('td.delivery-charge').text(parseFloat(oDisplayData.delivery_charge).toFixed(2) + ' PHP');
                    uiHistoryTemplate.find('strong.total-bill').text(parseFloat(oDisplayData.total_bill).toFixed(2) + ' PHP');
                    uiHistoryTemplate.find('td.total-cost').text(parseFloat(oDisplayData.total_cost).toFixed(2) + ' PHP');
                    if (key != 0) {
                        uiHistoryTemplate.hide();
                    }

                    uiHistoryTemplate.attr('data-order-id', value.id)
                    uiContainer.find('div.order_history_container').append(uiHistoryTemplate);


                })

                uiContainer.find('div.order_history div.frm-custom-dropdown-option').html('').append(sHtml);
 

            }
        },

        'get_order_history': function (uiContainer, iCustomerID) {
            if (typeof(uiContainer) !== 'undefined') {

                // var oParams = {
                //     "params": {
                //         "where": [
                //             {
                //                 "field"   : "o.customer_id",
                //                 "operator": "=",
                //                 "value"   : iCustomerID
                //             }
                //         ],
                //         "order_by" : 'o.id',
                //         "sorting" : 'DESC',
                //         "limit" : 9999999999,
                //     }
                // };
                // //console.log(oParams)
                // var oAjaxConfig = {
                //     "type"   : "Get",
                //     "data"   : oParams,
                //     "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                //     "url"    : callcenter.config('url.api.jfc.orders') + "/orders/get",
                //     "success": function (oData) {
                //         if (oData.status == true) {
                //             //alert();
                //             if (count(oData.data) > 0) {
                //                 oOrders = oData.data;
                                
                //                 callcenter.call_orders.populate_order_history(oOrders, uiContainer);

                //             }
                //             else
                //             {
                //                 $('input[name="date-selected"]:visible').val('No Available Order History')
                //             }

                //         }
                //         else {
                //             //console.log('api error')
                //         }
                //     }
                // };

                // callcenter.call_orders.get(oAjaxConfig);
                var oOrders = cr8v.get_from_storage('orders', '');
                if(typeof(oOrders) != 'undefined')
                {
                    oOrders = oOrders[0];
                    oOrders = oOrders.filter(function (el) {
                        return el['customer_id'] == iCustomerID;
                    })

                    if(count(oOrders) > 0)
                    {
                        callcenter.call_orders.populate_order_history(oOrders, uiContainer);
                    }
                    else
                    {
                        $('input[name="date-selected"]:visible').val('No Available Order History');
                    }
                }
            }
        }

    }

}());

$(window).load(function () {
    callcenter.call_orders.initialize();
});


