/**
 *  orders Class
 *
 *
 *
 */
(function () {
    "use strict";


    CPlatform.prototype.orders = {

       
		'initialize': function () {
			
			var iOrderType = $('input#order_type').val();
			var oParams = {'limit'  : 100,
							'offset' : 0,
							'order_type': iOrderType
							};
			
			callcenter.orders.get_xavier_orders_data(oParams);

        },
		
		'get_xavier_orders_data' : function(oParams){
			
			
			
            var oAjaxConfig = {
                "type"   : "Post",
                "data"   : oParams,
                "headers" : {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                //"url"    : "http://localhost/api.jfc.orders/migration/get_orders_data",
				"url"     : callcenter.config('url.api.jfc.orders') + "/migration/get_orders_data",
                "success": function (oData) {
					console.log(oData);
                    if(typeof(oData) != 'undefined')
                    {
                        if (oData.status == false) //failed
                        {
                            /* var uiContainer = $('div.address-error-msg');
                            var sError = '';
                            $.each(oData.message, function (k, v) {
                                sError += '<p class="error_messages"> ' + v + '</p>';
                            });
                            callcenter.orders.add_error_message(sError, uiContainer); */
                        }
                        else {
                            /* if (count(oData.data) > 0) {
								var iOffset = oData.offset;
								var oParams = {'offset': iOffset, "limit": 100, "order_type":$('input#order_type').val()};
								callcenter.orders.get_xavier_orders_data(oParams);
							} */
                        }
                       
                    }
                }
            };

            callcenter.orders.get(oAjaxConfig)
		},
		
        'get': function (oAjaxConfig) {
            if (typeof(oAjaxConfig) != 'undefined') {
                callcenter.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

       

    }

}());

$(window).load(function () {
    callcenter.orders.initialize();
});


