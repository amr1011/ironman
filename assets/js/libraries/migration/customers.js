/**
 *  Customer Class
 *
 *
 *
 */
(function () {
    "use strict";


    CPlatform.prototype.customers = {

       
		'initialize': function () {
			
			var oParams = {'limit'  : 10,
							'offset' : 0,};
			
			callcenter.customers.get_xavier_customer_data(oParams);

        },
		
		'get_xavier_customer_data' : function(oParams){
			
			
			
            var oAjaxConfig = {
                "type"   : "Post",
                "data"   : oParams,
                "headers" : {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                //"url"    : "http://localhost/api.jfc.customers/migration/get_customer_data",
				"url"     : callcenter.config('url.api.jfc.customers') + "/migration/get_customer_data",
                "success": function (oData) {
					console.log(oData);
                    if(typeof(oData) != 'undefined')
                    {
                        if (oData.status == false) //failed
                        {
                            /* var uiContainer = $('div.address-error-msg');
                            var sError = '';
                            $.each(oData.message, function (k, v) {
                                sError += '<p class="error_messages"> ' + v + '</p>';
                            });
                            callcenter.customers.add_error_message(sError, uiContainer); */
                        }
                        else {
                            /* if (count(oData.data) > 0) {
								var iOffset = oData.offset;
								var oParams = {'offset': iOffset, "limit": 10};
								callcenter.customers.get_xavier_customer_data(oParams);
							} */
                        }
                       
                    }
                }
            };

            callcenter.customers.get(oAjaxConfig)
		},
		
        'get': function (oAjaxConfig) {
            if (typeof(oAjaxConfig) != 'undefined') {
                callcenter.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

       

    }

}());

$(window).load(function () {
    callcenter.customers.initialize();
});


