function CBatchDownload () {
	var oInstance = this;
	/* 
		function that will batch download data based on limit
		@param : (object) oSettings
		@prototype :  
		
		{
			'offset' : (int), // required
			'limit' : (int), // required
			'periodic_callback' : (function), // optional
			'complete' : (function), // optional
			'url' : (string) // required
		}
	*/
	this.batch = function(oSettings) {
		// -- validation
		if (typeof (oSettings) != 'undefined' 
		&& typeof (oSettings.offset) != 'undefined' 
		&& typeof (oSettings.limit) != 'undefined' 
		&& typeof (oSettings.url) != 'undefined') {
			var regExp = new RegExp ("[0-9]");
			if (regExp.test(oSettings.offset) && regExp.test(oSettings.limit)) {
				var iTempOffset = oSettings.offset;
				var oParam = {'offset' : oSettings.offset, 'limit' : oSettings.limit};
				if (typeof (oSettings.special) != 'undefined') {
					oParam = $.extend(oParam, oSettings.special);
				}
				$.ajax({
					'type': "POST",
					'url' : oSettings.url,
					'data' : oParam,
					'success' : function(sData) {
						var bRecurse = true;
						if (typeof (oSettings.return_count) != 'undefined' && oSettings.return_count) {
							var pattern = new RegExp(/^\d+$/);
							
							bRecurse = (pattern.test(sData) && parseInt(sData) > 0);
							
						} else {
							var oData = $.parseJSON(sData);	
							bRecurse = oInstance.is_empty(oData)
						}
						if (bRecurse) {
							oSettings.offset = parseInt(oSettings.offset) + parseInt(oSettings.limit);
							if (typeof (oSettings.periodic_callback) == 'function') {
								oSettings.periodic_callback(sData);
							}
							setTimeout(function() {
								oInstance.batch(oSettings);
							},1);
						} else {
							if (typeof (oSettings.complete) == 'function') {
								oSettings.complete(sData);
							}
						}
					}
				});
				
			}
		}
	}
	
	this.is_empty = function(oData) {
		var x = 0;
		if(oData){
			for(var y in oData){
				return false;
			}
		}
		return true;
	}

}

if (typeof (ironman) == 'undefined') {
	var ironman = {};
}

ironman.download = new CBatchDownload;