/**
 *  user Class
 *
 *
 *
 */
(function () {
    "use strict";


    CPlatform.prototype.nx_users = {

       
		'initialize': function () {
			
			/* var oParams = {'limit'  : 100,
							'offset' : 0,}; */
							
			var oParams = {
                "params" : {
                    "type"  :2,
                    "limit"  : 100,
					"offset" : 0,
                    "where" : [
                        {
							key : "u.type",
							operator : "=",
							value : "2"
						},
                        {
                            key : "u.sbu_id",
                            operator : "=",
                            value : "1"
                        },
                        
                    ]
                }

            };
			
			callcenter.nx_users.get_xavier_user_data(oParams);

        },
		
		'get_xavier_user_data' : function(oParams){
			
			
			
            var oAjaxConfig = {
                "type"   : "Post",
                "data"   : oParams,
                "headers" : {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                //"url"    : "http://localhost/api.jfc.users/migration/get_user_data",
				"url"     : callcenter.config('url.api.jfc.users') + "/migration/get_ironman_users",
                "success": function (oData) {
					console.log(oData);
                    if(typeof(oData) != 'undefined')
                    {
                        if (oData.status == false) //failed
                        {
                            /* var uiContainer = $('div.address-error-msg');
                            var sError = '';
                            $.each(oData.message, function (k, v) {
                                sError += '<p class="error_messages"> ' + v + '</p>';
                            });
                            callcenter.nx_users.add_error_message(sError, uiContainer); */
                        }
                        else {
                            if (count(oData.data) > 0) {
								var iOffset = oData.offset;
								// var oParams = {'offset': iOffset, "limit": 100};
								
								var oParams = {
									"params" : {
										"type"  :2,
										"limit"  : 100,
										"offset" : iOffset,
										"where" : [
											{
												key : "u.type",
												operator : "=",
												value : "2"
											},
											{
												key : "u.sbu_id",
												operator : "=",
												value : "1"
											},
											
										]
									}

								};
								
								
								
								callcenter.nx_users.get_xavier_user_data(oParams);
							}
                        }
                       
                    }
                }
            };

            callcenter.nx_users.get(oAjaxConfig)
		},
		
        'get': function (oAjaxConfig) {
            if (typeof(oAjaxConfig) != 'undefined') {
                callcenter.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

       

    }

}());

$(window).load(function () {
    callcenter.nx_users.initialize();
});


