/**
 *  user Class
 *
 *
 *
 */
(function () {
    "use strict";


    CPlatform.prototype.products = {

       
		'initialize': function () {
			
			var oParams = {'limit'  : 100,
							'offset' : 0,};
			
			callcenter.products.get_xavier_products_data(oParams);

        },
		
		'get_xavier_products_data' : function(oParams){
			
			
			
            var oAjaxConfig = {
                "type"   : "Post",
                "data"   : oParams,
                "headers" : {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                //"url"    : "http://localhost/api.jfc.products/migration/get_user_data",
				"url"     : callcenter.config('url.api.jfc.products') + "/migration/get_products_data",
                "success": function (oData) {
					console.log(oData);
                    if(typeof(oData) != 'undefined')
                    {
                        if (oData.status == false) //failed
                        {
                            /* var uiContainer = $('div.address-error-msg');
                            var sError = '';
                            $.each(oData.message, function (k, v) {
                                sError += '<p class="error_messages"> ' + v + '</p>';
                            });
                            callcenter.products.add_error_message(sError, uiContainer); */
                        }
                        else {
                            if (count(oData.data) > 0) {
								var iOffset = oData.offset;
								var oParams = {'offset': iOffset, "limit": 100};
								callcenter.products.get_xavier_products_data(oParams);
							}
                        }
                       
                    }
                }
            };

            callcenter.products.get(oAjaxConfig)
		},
		
        'get': function (oAjaxConfig) {
            if (typeof(oAjaxConfig) != 'undefined') {
                callcenter.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

       

    }

}());

$(window).load(function () {
    callcenter.products.initialize();
});


