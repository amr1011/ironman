(function () {
    "use strict";

    var promoContent,
        upsellContent,
        uploadNewUpsellContainer,
        createNewPromoContainer,
        editPromoContainer,
        defaultImage;


    CPlatform.prototype.upsell_promos = {

        'initialize' : function () {

            promoContent = $('div[content="promos"]');
            upsellContent = $('div[content="upsell"]');
            uploadNewUpsellContainer = $('div.upload_new_upsell_container');
            createNewPromoContainer = $('div.create_new_promo_container');
            editPromoContainer = $('div.edit_promo_container');
            defaultImage = admin.config('url.server.base')+'/assets/images/icon-camera.png';

            promoContent.find('div.add_promo_error_msg').hide();
            promoContent.find('div.add_promo_success_msg').hide();
            promoContent.find('div.add_promo_error_msg').find('p.error_messages').remove();

            //show upload new upsell
            promoContent.on('click', 'a.upload_new_upsell', function(){
                var uiThis = $(this);
                uiThis.addClass('hidden');
                uploadNewUpsellContainer.removeClass('hidden');
                promoContent.find('div.upload_new_upsell_container').find('button.cancel_upload_new_upsell_btn').show();
            });

            //hide upload new upsell
            promoContent.on('click', 'button.cancel_upload_new_upsell_btn', function(){
                promoContent.find('a.upload_new_upsell').removeClass('hidden');
                uploadNewUpsellContainer.addClass('hidden');
                promoContent.find('div.upload_new_upsell_container').find('button.choose_upload_new_upsell_btn').show();
                promoContent.find('div.upload_new_upsell_container').find('button.upload_new_upsell_save_btn').hide();
                promoContent.find('div.upload_new_upsell_container').find('img.upsell_camera_image').hide();
                promoContent.find('div.upload_new_upsell_container').find('img.upsell_camera_image').attr('src', '');
                promoContent.find('div.upload_new_upsell_container').find('p.new_upsell_name').text('No File Chosen');
                promoContent.find('div.upload_new_upsell_container').find('div.add_upsell_success_msg').hide();
            });

            //show create new promo container
            promoContent.on('click', 'a.create_new_promo', function(){
                var uiThis = $(this);
                uiThis.addClass('hidden');
                
                if(createNewPromoContainer.find('p.camera').find('span').length == 0)
                {
                    createNewPromoContainer.find('p.camera').append("<span class='text-center font-12'>Upload Image</span>");
                }
                createNewPromoContainer.find('p.camera').find('img.promo_image').attr('src', defaultImage);
                createNewPromoContainer.find('div.add_promo_camera').find('input[name="new_promo_image"]').removeAttr('image_name');
                createNewPromoContainer.find('input, textarea').val('');
                createNewPromoContainer.find('div.mechanics_container').find('div.promo_mechanics:not(:first):not(.template)').remove();
                createNewPromoContainer.find('div.add_promo_error_msg').hide();
                createNewPromoContainer.removeClass('hidden');
            });

            //hide create new promo container
            promoContent.on('click', 'button.cancel_promo_btn', function(){
                promoContent.find('a.create_new_promo').removeClass('hidden');
                createNewPromoContainer.addClass('hidden');
            });

            //add another mechanic in add or edit promo container
            promoContent.on('click', 'a.add_another_mechanics', function(){
                var uiThis = $(this);
                var newMechanic =  uiThis.parents('div.mechanics_container').find('div.promo_mechanics.template').clone().removeClass('template');
                var newMechanicCount = uiThis.parents('div.mechanics_container').find('div.promo_mechanics:not(.template):visible').length;
                
                newMechanic.find('p.promo_mechanics_num').text(newMechanicCount + 1 + '.');
                newMechanic.find('input[labelinput="Promo Mechanics"]').val('');
                uiThis.parents('div.mechanics_container').append(newMechanic);
            });

            //remove another mechanic in add or edit promo container
            promoContent.on('click', 'a.remove_another_mechanics', function(){
                var uiThis = $(this);
                var newMechanicCount = uiThis.parents('div.mechanics_container').find('div.promo_mechanics:not(.template):visible').length;
                
                uiThis.parents('div.promo_mechanics:first:not(.template)').hide();

                var uiMechanic = uiThis.parents('div.mechanics_container').find('div.promo_mechanics:not(.template):visible');
                var count = 0;
                
                $.each(uiMechanic, function (k, v){
                    count++
                    $(v).find('p.promo_mechanics_num').text(count+'.')
                });
                uiThis.parents('div.promo_mechanics:first:not(.template)').remove();
            });
        
            //save new promo
            promoContent.on('click', 'button.save_promo_btn', function(){
                $(this).parents('form#add_promo').submit();
            });

            promoContent.find('form#add_promo').on('submit', function (e) {
                promoContent.find('div.add_promo_error_msg').hide();
                promoContent.find('div.add_promo_success_msg').hide();
                promoContent.find('div.add_promo_error_msg').find('p.error_messages').remove();
                e.preventDefault();
            });

            //save edit promo
            promoContent.on('click', 'button.edit_promo_save_btn:visible', function(){
                $(this).parents('form#edit_promo').submit();
            });

            promoContent.on('submit', 'form#edit_promo', function (e) {
                promoContent.find('div.edit_promo_error_msg').hide();
                promoContent.find('div.edit_promo_success_msg').hide();
                promoContent.find('div.editd_promo_error_msg').find('p.error_messages').remove();
                e.preventDefault();
            });

            //show collapsible
            promoContent.on('click', 'div.promo_uncollapsed', function (e) {
                var uiThis = $(this);
                uiThis.hide();
                uiThis.parents('div.promo:first').find('div.promo_collapsed').show();
                uiThis.parents('div.promo:first').siblings('div.promo').find('div.promo_collapsed').hide();
                uiThis.parents('div.promo:first').siblings('div.promo').find('div.edit_promo_container:not(.template)').remove();
                uiThis.parents('div.promo:first').siblings('div.promo').find('div.promo_uncollapsed').show();
            })

            //hide collapsible
            promoContent.on('click', 'div.promo_collapsed div.img-container', function (e) {
                var uiThis = $(this);
                uiThis.parents('div.promo:first').find('div.promo_collapsed').hide();
                uiThis.parents('div.promo:first').find('div.promo_uncollapsed').show();
            });

            //add and remove class on hover because there is no on hover from html 
            promoContent.on('mouseenter', 'div.promo_uncollapsed', function (e) {
                var uiThis = $(this);
                uiThis.removeClass('opaque');
            }).on('mouseleave', 'div.promo_uncollapsed', function (e) {
                var uiThis = $(this);
                uiThis.addClass('opaque');
            });

            //show edit promo container
            promoContent.on('click', 'button.edit_promo_btn:visible', function (e) {
                var uiThis = $(this);
                var uiContainer = uiThis.parents('div.promo:first');
                uiContainer.siblings('div.promo').find('div.promo_collapsed').hide();
                uiContainer.siblings('div.promo').find('div.edit_promo_container:not(.template)').remove();
                uiContainer.siblings('div.promo').find('div.promo_uncollapsed').show();
                admin.upsell_promos.assemble_edit_container(uiContainer);
            });

            //hide edit promo container and show the collapsible container
            promoContent.on('click', 'button.cancel_edit_promo_btn:visible', function (e) {
                var uiThis = $(this);
                uiThis.parents('div.promo:first').find('div.promo_collapsed').show();
                uiThis.parents('div.promo:first').find('div.edit_promo_container:not(.template)').remove();
            });

            //add promo image button
            promoContent.on('click', 'p.camera:visible',  function (e) {
                e.preventDefault();
                promoContent.find('div.create_new_promo_container:visible').find('input[name="new_promo_image"]').trigger('click');
            });

            //add image
            var last_image_path = admin.config('url.server.base') + 'assets/images/w-logo.png';
            promoContent.on('change', 'input[name="new_promo_image"]', function(){
                var uiThis = $(this);
                var image = $('input[type=file][name="new_promo_image"]')[0];
                var image_path = '';
                var image_name = '';
                var formdata = false;
                var uiLogoContainer = promoContent.find('p.camera:visible');

                if(window.FormData)
                {
                    formdata = new FormData();
                    formdata.append('client_img', image.files[0]);
                    formdata.append('csrf_ironman_token', $.cookie('csrf_ironman_cookie'));
                    var oSettings = {
                        type: 'POST',
                        url: admin.config('url.server.base') + 'admin/ajax_upload_promo',
                        data: formdata,
                        processData: false,
                        contentType: false,
                        beforeSend: function(){

                        },
                        success: function(sData){
                            var oData = $.parseJSON(sData);
                            image_path = (typeof(oData.data.image_path) != 'undefined') ? oData.data.image_path : last_image_path;
                            image_name = oData.data.image_name;
                            last_image_path = image_path;
                            if(typeof(oData.status) != 'undefined')
                            {
                                uiLogoContainer.attr('data-original-title', oData.message);
                                uiLogoContainer.attr('title', oData.message).tooltip({trigger:'click'});
                                uiLogoContainer.click();
                                setTimeout(function() {
                                    uiLogoContainer.tooltip ('destroy').removeAttr('title data-original-title');
                                }, 3000);
                            }
                        },
                        complete: function(){
                            uiLogoContainer.find('.preloader').remove();
                            if(typeof(image_path) != 'undefined')
                            {
                                var sStyle = '';
                                uiLogoContainer.html('<img src="'+image_path+'" class="img-responsive center-block promo_image" >');
                                uiThis.attr('image_name', image_name);
                            }

                        }
                    };
                    $.ajax(oSettings);
                }
            });

            //edit promo image button
            promoContent.on('click', 'p.edit_camera:visible',  function (e) {
                e.preventDefault();
                promoContent.find('div.edit_promo_container:visible').find('input[name="edit_promo_image"]').trigger('click');
            });

            //edit image
            var last_image_path = admin.config('url.server.base') + 'assets/images/w-logo.png';
            promoContent.on('change', 'input[name="edit_promo_image"]', function(){
                var uiThis = $(this);
                var image = promoContent.find('div.edit_promo_container:visible').find('input[type=file][name="edit_promo_image"]')[0];
                var image_path = '';
                var image_name = '';
                var formdata = false;
                var uiLogoContainer = promoContent.find('p.edit_camera:visible');

                if(window.FormData)
                {
                    formdata = new FormData();
                    formdata.append('client_img', image.files[0]);
                    formdata.append('csrf_ironman_token', $.cookie('csrf_ironman_cookie'));
                    var oSettings = {
                        type: 'POST',
                        url: admin.config('url.server.base') + 'admin/ajax_upload_promo',
                        data: formdata,
                        processData: false,
                        contentType: false,
                        beforeSend: function(){

                        },
                        success: function(sData){
                            var oData = $.parseJSON(sData);
                            image_path = (typeof(oData.data.image_path) != 'undefined') ? oData.data.image_path : last_image_path;
                            image_name = oData.data.image_name;
                            last_image_path = image_path;
                            if(typeof(oData.status) != 'undefined')
                            {
                                uiLogoContainer.attr('data-original-title', oData.message);
                                uiLogoContainer.attr('title', oData.message).tooltip({trigger:'click'});
                                uiLogoContainer.click();
                                setTimeout(function() {
                                    uiLogoContainer.tooltip ('destroy').removeAttr('title data-original-title');
                                }, 3000);
                            }
                        },
                        complete: function(){
                            uiLogoContainer.find('.preloader').remove();
                            if(typeof(image_path) != 'undefined')
                            {
                                var sStyle = '';
                                uiLogoContainer.html('<img src="'+image_path+'" class="img-responsive center-block promo_image" >');
                                uiThis.attr('image_name', image_name);
                            }

                        }
                    };
                    $.ajax(oSettings);
                }
            });

            //upload upsell image button
            promoContent.on('click', 'button.choose_upload_new_upsell_btn:visible',  function (e) {
                e.preventDefault();
                promoContent.find('div.upload_new_upsell_container:visible').find('input[name="upsell_image_input"]').trigger('click');
            });

            //upload upsell image
            var last_image_path = admin.config('url.server.base') + 'assets/images/w-logo.png';
            promoContent.on('change', 'input[name="upsell_image_input"]', function(){
                var uiThis = $(this);
                var image = promoContent.find('div.upload_new_upsell_container:visible').find('input[name="upsell_image_input"]')[0];
                var image_path = '';
                var image_name = '';
                var formdata = false;
                var uiLogoContainer = promoContent.find('p.upsell_camera:visible');

                if(window.FormData)
                {
                    formdata = new FormData();
                    formdata.append('client_img', image.files[0]);
                    formdata.append('csrf_ironman_token', $.cookie('csrf_ironman_cookie'));
                    var oSettings = {
                        type: 'POST',
                        url: admin.config('url.server.base') + 'admin/ajax_upload_promo',
                        data: formdata,
                        processData: false,
                        contentType: false,
                        beforeSend: function(){

                        },
                        success: function(sData){
                            var oData = $.parseJSON(sData);
                            image_path = (typeof(oData.data.image_path) != 'undefined') ? oData.data.image_path : last_image_path;
                            image_name = oData.data.image_name;
                            last_image_path = image_path;
                            if(typeof(oData.status) != 'undefined')
                            {
                                uiLogoContainer.attr('data-original-title', oData.message);
                                uiLogoContainer.attr('title', oData.message).tooltip({trigger:'click'});
                                uiLogoContainer.click();
                                setTimeout(function() {
                                    uiLogoContainer.tooltip ('destroy').removeAttr('title data-original-title');
                                }, 3000);

                                if(oData.status == true)
                                {
                                    if(typeof(image_path) != 'undefined')
                                    {
                                        uiLogoContainer.html('<img src="'+image_path+'" class="img-responsive center-block upsell_camera_image" >');
                                        uiThis.attr('image_name', image_name);
                                        promoContent.find('div.upload_new_upsell_container:visible').find('p.new_upsell_name').text(image_name);
                                        promoContent.find('div.upload_new_upsell_container:visible').find('button.choose_upload_new_upsell_btn').hide();
                                        promoContent.find('div.upload_new_upsell_container:visible').find('button.upload_new_upsell_save_btn').show();
                                    }
                                }
                            }
                        },
                        complete: function(){
                            uiLogoContainer.find('.preloader').remove();
                            if(typeof(image_path) != 'undefined')
                            {
                                var sStyle = '';
                            }

                        }
                    };
                    $.ajax(oSettings);
                }
            });

            //save new upsell button
            promoContent.on('click', 'button.upload_new_upsell_save_btn', function(){
                var image_name = promoContent.find('input[name="upsell_image_input"]').attr('image_name');
                admin.upsell_promos.show_spinner(promoContent.find('button.upload_new_upsell_save_btn:visible'), true);
                admin.upsell_promos.assemble_upsell_information(image_name);
            });

            //archive upsell modal confirm button
            promoContent.on("click", "button.archive_upsell_btn", function(){
                var uiThis = $(this);
                $("body").css({overflow:'hidden'});
                var tm = $(this).attr("modal-target");

                $("div[modal-id~='"+tm+"']").addClass("showed");

                $("div[modal-id~='"+tm+"'] .close-me").on("click",function(){
                    $("body").css({'overflow-y':'initial'});
                    $("div[modal-id~='"+tm+"']").removeClass("showed");
                });

                var upsellID = uiThis.parents('div.upsell:first').attr('data-upsell-id');
                $("div[modal-id~='"+tm+"']").find("button.archive_upsell_btn_confirm").attr('data-upsell-id', upsellID);
            });

            //archive promo modal confirm button
            promoContent.on("click", "button.archive_promo_btn", function(){
                var uiThis = $(this);
                $("body").css({overflow:'hidden'});
                var tm = $(this).attr("modal-target");

                $("div[modal-id~='"+tm+"']").addClass("showed");

                $("div[modal-id~='"+tm+"'] .close-me").on("click",function(){
                    $("body").css({'overflow-y':'initial'});
                    $("div[modal-id~='"+tm+"']").removeClass("showed");
                });

                var promoID = uiThis.parents('div.promo:first').attr('data-promo-id');
                $("div[modal-id~='"+tm+"']").find("button.archive_promo_btn_confirm").attr('data-promo-id', promoID);
            });

            //confirm archive upsell
            $('div[modal-id="confirm-archive-upsell"]').on('click', 'button.archive_upsell_btn_confirm', function(){
                var uiThis = $(this);
                var upsellID = uiThis.attr('data-upsell-id');
                admin.upsell_promos.archive_upsell(upsellID)
            });

            //confirm archive promo
            $('div[modal-id="confirm-archive-promo"]').on('click', 'button.archive_promo_btn_confirm', function(){
                var uiThis = $(this);
                var promoID = uiThis.attr('data-promo-id');
                admin.upsell_promos.archive_promo(promoID)
            });

            //add new promo validation
            var oValidationConfig = {
                'data_validation'    : 'datavalid',
                'input_label'        : 'labelinput',
                'min_length'         : 0,
                // 'max_length'         : 75,
                'class'              : 'error',
                'add_remove_class'   : true,
                'onValidationError'  : function (arrMessages) {
                    admin.upsell_promos.show_spinner(promoContent.find('button.save_promo_btn'), false);
                    promoContent.find('div.add_promo_error_msg').html('<i class="fa fa-exclamation-triangle"></i>Please select  up all required fields.');
                    promoContent.find('div.add_promo_error_msg').show();
                    // console.log(arrMessages); //shows the errors
                },
                'onValidationSuccess': function () {
                    var promoImageInput = promoContent.find('input[name="new_promo_image"]');
                    if(typeof(promoImageInput.attr('image_name')) != 'undefined' && promoImageInput.attr('image_name') !== '' && promoImageInput.attr('image_name') !== null)
                    {
                        admin.upsell_promos.show_spinner(promoContent.find('button.save_promo_btn'), true);
                        promoContent.find('div.add_promo_error_msg').hide();
                        admin.upsell_promos.assemble_add_promo_information();
                    }
                    else
                    {
                        promoContent.find('div.add_promo_error_msg').html('<i class="fa fa-exclamation-triangle"></i>Please select image.');
                        promoContent.find('div.add_promo_error_msg').show();
                    }
                }
            }
            admin.validate_form($('#add_promo'), oValidationConfig);

            //get all promos
            var oPromoParams = {
                'params' : {
                    "sbu_id" : "1",
                    "limit"  : 99999,
                    "where"  : [{
                        "field"     : "is_active",
                        "operator"  : "=",
                        "value"     : 1
                    }]
                }
            }
            admin.upsell_promos.get_promos(oPromoParams);

            //get all upsells
            var oUpsellParams = {
                'params' : {
                    "sbu_id" : "1",
                    "limit"  : 99999,
                    "where"  : [{
                        "field"     : "is_active",
                        "operator"  : "=",
                        "value"     : 1
                    }]
                }
            }
            admin.upsell_promos.get_upsells(oUpsellParams);

        },

        //assemble the promo to be added
        'assemble_add_promo_information':function(){
            var promoName = promoContent.find('input[name="new_promo_name"]').val();
            var promoDesc = promoContent.find('textarea[name="new_promo_description"]').val();
            var uiPromoMech = promoContent.find('div.new_promo_mechanics:not(.template) input[name="new_promo_mechanics[]"]:visible');
            var arrPromoMech = [];
            var promoImg = promoContent.find('input[name="new_promo_image"]').attr('image_name');

            if(typeof(uiPromoMech) !== 'undefined')
            {
                $.each(uiPromoMech, function () {
                    var sValue = $(this).val();
                    arrPromoMech.push(sValue);
                });
            }

            arrPromoMech = jQuery.grep(arrPromoMech, function(n, i){
                return (n !== "" && n != null);
            });

            var oNewPromoParams = {
                'params' : {
                   "display_name"    : promoName, //required
                   "description"     : promoDesc, //required
                   "sbu_id"          : "1", //required
                   "promo_mechanics" : arrPromoMech, //required
                   "image"           : promoImg
               },
               "user_id" : iConstantUserId
            }

            var oAjaxConfig = {
                "type"   : "POST",
                "data"   : oNewPromoParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : admin.config('url.api.jfc.promos') + "/promos/add",
                "success": function (oPromoData) {
                    if(oPromoData.status == true)
                    {
                        promoContent.find('div.add_promo_success_msg').show().delay(3000).fadeOut();

                        setTimeout( function() {
                            promoContent.find('button.cancel_promo_btn').trigger('click');
                        }, 3000)

                        var uiContainer = promoContent.find('div.promo_container');

                        var uiTemplate = promoContent.find('div.promo_container div.promo.template').clone().removeClass('template');

                        var uiManipulatedTemplate = admin.upsell_promos.manipulate_promos(uiTemplate, oPromoData.data);

                        uiManipulatedTemplate.find('div.promo_collapsed').hide();
                        uiManipulatedTemplate.find('div.promo_uncollapsed').show();

                        // admin.upsell_promos.clone_append(uiManipulatedTemplate, uiContainer);
                        admin.upsell_promos.clone_prepend(uiManipulatedTemplate, uiContainer);
                    }
                    else
                    {

                    }
                    admin.upsell_promos.show_spinner(promoContent.find('button.save_promo_btn'), false);
                }
            }   
            admin.upsell_promos.ajax(oAjaxConfig);
        },

        //assemble the promo to be updated
        'assemble_edit_promo_information': function(uiContainer){
            if(typeof(uiContainer) != 'undefined')
            {
                var promoID = uiContainer.attr('data-promo-id');
                var promoName = uiContainer.find('input[name="edit_promo_name"]').val();
                var promoDesc = uiContainer.find('textarea[name="edit_promo_description"]').val();
                var uiPromoMech = uiContainer.find('div.edit_promo_mechanics:not(.template) input[name="edit_promo_mechanics[]"]:visible');
                var promoImg = uiContainer.find('input[name="edit_promo_image"]').attr('image_name');
                var arrPromoMech = [];
                
                if(typeof(uiPromoMech) !== 'undefined')
                {
                    $.each(uiPromoMech, function () {
                        var sValue = $(this).val();
                        arrPromoMech.push(sValue);
                    });
                }

                arrPromoMech = jQuery.grep(arrPromoMech, function(n, i){
                    return (n !== "" && n != null);
                });

                var oEditPromoParams = {
                    "params":{
                        'qualifiers':{
                            "id" : promoID
                        },
                        'data' : {
                            "display_name"    : promoName, //required
                            "description"     : promoDesc, //required
                            "sbu_id"          : "1", //required
                            "promo_mechanics" : arrPromoMech, //required
                            "image"           : promoImg
                       }
                    },
                    'user_id' : iConstantUserId
                }

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oEditPromoParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : admin.config('url.api.jfc.promos') + "/promos/update",
                    "success": function (oPromoData) {
                        if(oPromoData.status == true)
                        {
                            promoContent.find('div.edit_promo_success_msg').show().delay(3000).fadeOut();

                            setTimeout(function(){
                                var v = oPromoData.data;
                                var uiMech = '';

                                uiContainer.attr('data-promo-id', v.id);
                                uiContainer.attr('data-promo-image-name', v.image);
                                uiContainer.find('p.promo_name').text(v.display_name);
                                uiContainer.find('p.promo_name').find('strong').text(v.display_name);
                                uiContainer.find('p.promo_description').html(v.description);
                                uiContainer.find('img.promo_image').attr('src', admin.config('url.server.base') + 'assets/images/promos/'+v.image);
                                uiContainer.find('img.promo_image').attr('onError', '$.fn.checkImage(this);');
                                
                                if(count(v.promo_mechanics) > 0)
                                {
                                    $.each(v.promo_mechanics, function (k, mech){
                                        uiMech += '<li class="font-12">'+mech.mechanic_name+'</li>';
                                    });
                                }

                                uiContainer.find('ul.promo_mechanics').html('').append(uiMech);
                                uiContainer.find('div.promo_collapsed').show();
                                uiContainer.find('div.promo_uncollapsed').hide();
                                uiContainer.find('div.edit_promo_container:not(.template)').remove();
                            }, 1500);
                        }
                        else
                        {

                        }
                        admin.upsell_promos.show_spinner(promoContent.find('button.edit_promo_save_btn'), false);
                    }
                }   
                admin.upsell_promos.ajax(oAjaxConfig);
            }
        },

        //assemble the upsell to be added
        'assemble_upsell_information':function(upsellImg){
            if(typeof(upsellImg) != 'undefined')
            {
                var oNewUpsellParams = {
                    'params' : {
                       "sbu_id"          : "1", //required
                       "image"           : upsellImg
                    },
                    "user_id" : iConstantUserId 
                }

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oNewUpsellParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : admin.config('url.api.jfc.promos') + "/promos/add_upsell",
                    "success": function (oUpsellData) {
                        if(count(oUpsellData.data) > 0 && typeof(oUpsellData) != 'undefined')
                        {
                            
                            promoContent.find('div.upload_new_upsell_container').find('div.add_upsell_success_msg').show().delay(3000).fadeOut();
                            promoContent.find('div.upload_new_upsell_container').find('button.upload_new_upsell_save_btn').hide();
                            promoContent.find('div.upload_new_upsell_container').find('button.cancel_upload_new_upsell_btn').hide();
                            setTimeout(function(){
                                promoContent.find('div.upload_new_upsell_container').find('button.choose_upload_new_upsell_btn').show();
                                promoContent.find('div.upload_new_upsell_container').find('img.upsell_camera_image').hide();
                                promoContent.find('div.upload_new_upsell_container').find('img.upsell_camera_image').attr('src', '');
                                promoContent.find('div.upload_new_upsell_container').find('p.new_upsell_name').text('No File Chosen');
                                promoContent.find('div.upload_new_upsell_container').find('div.add_upsell_success_msg').hide();
                                promoContent.find('div.upload_new_upsell_container').find('button.cancel_upload_new_upsell_btn').trigger('click');
                            }, 3000);

                            var uiContainer = promoContent.find('div.upsell_container');
                            var uiTemplate = promoContent.find('div.upsell_container div.upsell.template').clone().removeClass('template');
                            var uiManipulatedTemplate = admin.upsell_promos.manipulate_upsells(uiTemplate, oUpsellData.data);
                            // admin.upsell_promos.clone_append(uiManipulatedTemplate, uiContainer);
                            admin.upsell_promos.clone_prepend(uiManipulatedTemplate, uiContainer);

                        }
                        admin.upsell_promos.show_spinner(promoContent.find('button.upload_new_upsell_save_btn'), false);
                    }
                }   
                admin.upsell_promos.ajax(oAjaxConfig);
            }
        },

        //put the values in the edit container
        'assemble_edit_container' : function(uiContainer){
            if(typeof(uiContainer) != 'undefined')
            {
                if(uiContainer.find('div.edit_promo_container:not(template)').length > 0)
                {
                    uiContainer.find('div.edit_promo_container:not(template)').remove();
                }

                uiContainer.find('div.promo_collapsed').hide();

                var editTemplate = promoContent.find('div.edit_promo_container.template').clone().removeClass('template');
                var promoName = uiContainer.find('p.promo_name:first').text();
                var promoDesc = uiContainer.find('p.promo_description:first').text();
                var promoMech = uiContainer.find('ul.promo_mechanics:first li');
                var sPromoMech = [];
                var promoImg = uiContainer.attr('data-promo-image-name');
                

                if(promoMech.length > 0)
                {
                    $.each(promoMech, function(){
                        sPromoMech.push($(this).text());
                    })
                }

                editTemplate.find('input[name="edit_promo_name"]').val(promoName);
                editTemplate.find('textarea[name="edit_promo_description"]').val(promoDesc);
                editTemplate.find('input[name="edit_promo_image"]').attr('image_name', promoImg);
                editTemplate.find('img.promo_image').attr('src', admin.config('url.server.base') + 'assets/images/promos/'+promoImg);
                // editTemplate.find('img.promo_image').attr('onError', '$.fn.checkImage(this);');
                editTemplate.find('p.edit_camera').find('span').remove();
                editTemplate.find('div.edit_promo_error_msg ').hide();
                editTemplate.find('div.edit_promo_success_msg').hide();
                
                if(count(sPromoMech) > 0)
                {
                    $.each(sPromoMech, function (k, v){
                        var editMechanic = editTemplate.find('div.edit_promo_mechanics.template').clone().removeClass('template');
                        var editMechanicCount = editTemplate.find('div.edit_promo_mechanics:not(.template)').length;
                        if(k == 0)
                        {
                            editTemplate.find('div.edit_promo_mechanics:not(.template) input[name="edit_promo_mechanics[]"]').val(v)
                        }
                        else
                        {
                            editMechanic.find('p.edit_promo_mechanics_num').text(editMechanicCount + 1 + '. ');
                            editMechanic.find('input[name="edit_promo_mechanics[]"]').val(v);
                            editTemplate.find('div.edit_promo_mechanics_container').append(editMechanic);
                        }
                    });
                }
                uiContainer.append(editTemplate);

                var oValidationConfig = {
                    'data_validation'    : 'datavalid',
                    'input_label'        : 'labelinput',
                    'min_length'         : 0,
                    // 'max_length'         : 75,
                    'class'              : 'error',
                    'add_remove_class'   : true,
                    'onValidationError'  : function (arrMessages) {
                        admin.upsell_promos.show_spinner(promoContent.find('button.edit_promo_save_btn'), false);
                        promoContent.find('div.edit_promo_error_msg').show();
                        // console.log(arrMessages); //shows the errors
                    },
                    'onValidationSuccess': function () {
                        admin.upsell_promos.show_spinner(promoContent.find('button.edit_promo_save_btn'), true);
                        promoContent.find('div.edit_promo_error_msg').hide();
                        admin.upsell_promos.assemble_edit_promo_information(uiContainer);
                    }
                }
                admin.validate_form(uiContainer.find('form#edit_promo'), oValidationConfig);
            }
        },

        //archive upsell
        'archive_upsell' :function(upsellID)
        {
            if(typeof(upsellID) != 'undefined')
            {
                var oUpsellParams = {
                    'params' : {
                        "upsell_id" : upsellID,
                        "is_active": 0
                    },
                    "user_id" : iConstantUserId
                }

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oUpsellParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : admin.config('url.api.jfc.promos') + "/promos/archive_upsell",
                    "success": function (oUpsellData) {
                        $('body').css('overflow', 'auto');
                        $('div[modal-id="confirm-archive-upsell"]').removeClass('showed');
                        promoContent.find('div.upsell_container').find('div.upsell[data-upsell-id="'+upsellID+'"]').remove();
                    }
                }   
                admin.upsell_promos.ajax(oAjaxConfig);
            }
        },

        //archive promo
        'archive_promo' :function(promoID)
        {
            if(typeof(promoID) != 'undefined')
            {
                var oPromoParams = {
                    'params' : {
                        "promo_id" : promoID,
                        "is_active": 0
                    },
                    "user_id" : iConstantUserId
                }

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oPromoParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : admin.config('url.api.jfc.promos') + "/promos/archive_promo",
                    "success": function (oPromoData) {
                        $('body').css('overflow', 'auto');
                        $('div[modal-id="confirm-archive-promo"]').removeClass('showed');
                        promoContent.find('div.promo_container').find('div.promo[data-promo-id="'+promoID+'"]').remove();
                    }
                }   
                admin.upsell_promos.ajax(oAjaxConfig);
            }
        },

        //get all promos
        'get_promos' :function(oPromoParams)
        {
            if(typeof(oPromoParams) != 'undefined')
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oPromoParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : admin.config('url.api.jfc.promos') + "/promos/get",
                    "success": function (oPromoData) {
                        if(count(oPromoData.data) > 0 && typeof(oPromoData) != 'undefined')
                        {
                            var uiContainer = promoContent.find('div.promo_container');
                            $.each(oPromoData.data, function (key, value){
                                var uiTemplate = promoContent.find('div.promo_container div.promo.template').clone().removeClass('template');

                                var uiManipulatedTemplate = admin.upsell_promos.manipulate_promos(uiTemplate, value);

                                uiManipulatedTemplate.find('div.promo_collapsed').hide();
                                uiManipulatedTemplate.find('div.promo_uncollapsed').show();

                                admin.upsell_promos.clone_append(uiManipulatedTemplate, uiContainer);
                            });
                        }
                    }
                }   
                admin.upsell_promos.ajax(oAjaxConfig);
            }
        },

        //get all upsells
        'get_upsells' :function(oUpsellParams)
        {
            if(typeof(oUpsellParams) != 'undefined')
            {
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oUpsellParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : admin.config('url.api.jfc.promos') + "/promos/get_upsells",
                    "success": function (oUpsellData) {
                        if(count(oUpsellData.data) > 0 && typeof(oUpsellData) != 'undefined')
                        {
                            var uiContainer = promoContent.find('div.upsell_container');
                            $.each(oUpsellData.data, function (key, value){
                                var uiTemplate = promoContent.find('div.upsell_container div.upsell.template').clone().removeClass('template');

                                var uiManipulatedTemplate = admin.upsell_promos.manipulate_upsells(uiTemplate, value);

                                admin.upsell_promos.clone_append(uiManipulatedTemplate, uiContainer);
                            });
                        }
                    }
                }   
                admin.upsell_promos.ajax(oAjaxConfig);
            }
        },

        //append and manipulate the promos
        'manipulate_promos' : function(uiTemplate, oPromoData){
            if(typeof(oPromoData) != 'undefined')
            {
                var v = oPromoData;
                var uiMech = '';

                uiTemplate.attr('data-promo-id', v.id);
                uiTemplate.attr('data-promo-image-name', v.image);
                uiTemplate.find('p.promo_name').text(v.display_name);
                uiTemplate.find('p.promo_name').find('strong').text(v.display_name);
                uiTemplate.find('p.promo_description').html(v.description);

                uiTemplate.find('img.promo_image').attr('src', admin.config('url.server.base') + 'assets/images/promos/'+v.image);
                uiTemplate.find('img.promo_image').attr('onError', '$.fn.checkImage(this);');
                
                if(count(v.promo_mechanics) > 0)
                {
                    $.each(v.promo_mechanics, function (k, mech){
                        uiMech += '<li class="font-12">'+mech.mechanic_name+'</li>';
                    });
                }

                uiTemplate.find('ul.promo_mechanics').append(uiMech);

                return uiTemplate;
            }
        },

        //append and manipulate the promos
        'manipulate_upsells' : function(uiTemplate, oUpsellData){
            if(typeof(oUpsellData) != 'undefined')
            {
                var v = oUpsellData;

                uiTemplate.attr('data-upsell-id', v.id);
                uiTemplate.find('p.upsell_date_uploaded').text(moment(v.date_added).format('MMMM DD, YYYY | hh:mm:ss A'));
                uiTemplate.find('img.upsell_image').attr('src', admin.config('url.server.base') + 'assets/images/promos/'+v.image);
                uiTemplate.find('img.upsell_image').attr('onError', '$.fn.checkImage(this);');

                return uiTemplate;
            }
        },

        'clone_append': function (uiTemplate, uiContainer) {
            if (typeof(uiTemplate) !== 'undefined' && typeof(uiContainer) !== 'undefined') {
                uiContainer.append(uiTemplate);
            }
        },

        'clone_prepend': function (uiTemplate, uiContainer) {
            if (typeof(uiTemplate) !== 'undefined' && typeof(uiContainer) !== 'undefined') {
                uiContainer.prepend(uiTemplate);
            }
        },

        'add_error_message': function (sMessages, uiContainer) {
            $('p.error_messages').remove();
            if (sMessages.length > 0) {
                if (typeof(uiContainer) !== 'undefined') {
                    uiContainer.empty();
                    uiContainer.show();
                    uiContainer.append(sMessages);
                    uiContainer.find("p.error_messages:contains('is required')").hide();

                    if (uiContainer.find('p.error_messages').is(":contains('is required')")) {
                        uiContainer.append("<p class='error_messages'><i class='fa fa-exclamation-triangle'></i> Please fill up all required fields.</p>");
                    }
                }

            }
        },

        'ajax': function (oAjaxConfig) {
            if (typeof(oAjaxConfig) != 'undefined') {
                admin.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        'show_spinner': function (uiElem, bShow) {
            if (bShow == true) {
                if(uiElem.find('i.fa-spinner').length > 0)
                {
                    uiElem.find('i.fa-spinner').removeClass('hidden');
                }
                else
                {
                    uiElem.append('<i class="fa fa-spinner fa-pulse hidden"></i>').removeClass('hidden');
                }

                uiElem.prop('disabled', true);
            }
            else {
                if(uiElem.find('i.fa-spinner').length > 0)
                {
                    uiElem.find('i.fa-spinner').addClass('hidden');
                }
                uiElem.prop('disabled', false);
            }
        }

    }

}());

$(window).load(function () {
    admin.upsell_promos.initialize();
});