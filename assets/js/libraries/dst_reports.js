/**
 *  DST Report Class
 *
 *
 *
 */
(function () {

    CPlatform.prototype.dst_reports = {
        'get_provinces' : function(){
            var oProvincesAjaxConfig = {
               "type"   : "Get",
               "data"   : {
                   "all" : 1,
                   "call_center_user_id" : 3305//iConstantCallcenterUserId
               },
               "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
               "url"    : admin.config('url.api.jfc.gis') + "/gis/province_address",
               "success": function (data) {
                    if(typeof(data) != 'undefined')
                    {
                        if(typeof(data.data.provinces) != 'undefined')
                        {
                            admin.dst_reports.provinces = data.data.provinces;
                        }
                    }
               }
           };
           admin.dst_reports.ajax(oProvincesAjaxConfig);
        },
        'provinces' : {},


        'initialize': function () {
            //make select dropdown readonly

            $('div#dst_reports_specific_content').on('click', 'button.transaction-logs', function (e) {
                var iOrderID = $(this).parents('section.section-order').attr('data-order-id')
                console.log(iOrderID);
                var oParams = {
                    "params": {
                        "where": [
                            {
                                'field'   : 'opl.order_id',
                                'operator': '=',
                                'value'   : iOrderID,
                            }
                        ],
                        "limit" : 9999999999999
                    }
                }

                var oAjaxConfig = {
                    'type'   : "GET",
                    'headers': {"X-API-KEY": "IEYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    'url'    : admin.config('url.api.jfc.orders') + "/orders/logs",
                    'data'   : oParams,
                    'success': function (oData) {
                        //console.log(oData);
                        var sAgentLogsHtml = '';
                        var sStoreLogsHtml = '';
                        var uiAgentLogsContainer = $('div[modal-id="order-logs"]').find('tbody.logs_container');
                        
                        uiAgentLogsContainer.find('tr:not(.has_spinner)').remove();
                        uiAgentLogsContainer.find('tr.has_spinner i.fa-spinner').addClass('hidden');
                        uiAgentLogsContainer.find('tr.has_spinner').addClass('hidden');

                        if (typeof(oData) !== 'undefined') {
                            $.each(oData.data, function (key, log) {
                                var sAgentLogsHtml = '';
                                var date_processed_diff = '';
                                // if(key > 0)
                                // {
                                //     var date_processed_prev = oData.data[key - 1].date_processed;
                                //     var d1 = moment(date_processed_prev).format('MM-DD-YYYY H:mm');
                                //     var d2 = moment(log.date_processed).format('MM-DD-YYYY H:mm');
                                //     var iMinDiff = moment(d2, 'MM-DD-YYYY H:mm').diff(moment(d1, 'MM-DD-YYYY H:mm'), 'minutes');
                                
                                //     date_processed_diff = iMinDiff;
                                
                                //     if(date_processed_diff > 1)
                                //     {
                                //         date_processed_diff = '| '+ date_processed_diff +' mins';
                                //     }
                                //     else
                                //     {
                                //         date_processed_diff = '| '+ date_processed_diff +' min';
                                //     }
                                // }

                                var uiPreviousTr = uiAgentLogsContainer.find('tr:not(.has_spinner):not([is_followup="1"]):last');
                                if(uiPreviousTr.length > 0 && log.action !== 'Followed up Order')
                                {
                                    var date_processed_prev = uiPreviousTr.attr('date_processed');
                                    var d1 = moment(date_processed_prev).format('MM-DD-YYYY H:mm');
                                    var d2 = moment(log.date_processed).format('MM-DD-YYYY H:mm');
                                    var iMinDiff = moment(d2, 'MM-DD-YYYY H:mm').diff(moment(d1, 'MM-DD-YYYY H:mm'), 'minutes');
                                    
                                    date_processed_diff = iMinDiff

                                    if(date_processed_diff > 1)
                                    {
                                        date_processed_diff = '| '+ date_processed_diff +' mins';
                                    }
                                    else
                                    {
                                        date_processed_diff = '| '+ date_processed_diff +' min';
                                    }
                                }

                                var isFollowUp = 0;
                                if(log.action == 'Followed up Order')
                                {
                                    isFollowUp = 1;
                                }

                                if (log.type == 1 || log.type == 2) {
                                    sAgentLogsHtml = '<tr is_followup="'+isFollowUp+'" date_processed="'+log.date_processed+'"><td>'
                                     + log.action + '</td><td class="text-center">' + moment(log.date_processed).format('MM-DD-YYYY | hh:mm a ') + date_processed_diff + '</td><td class="text-center">' 
                                     + log.full_name + '</td>';
                                }
                                else if (log.type == 3) {
                                    sAgentLogsHtml = '<tr is_followup="'+isFollowUp+'" date_processed="'+log.date_processed+'"><td>'
                                     + log.action + '</td><td class="text-center">' + moment(log.date_processed).format('MM-DD-YYYY | hh:mm a ') + date_processed_diff + '</td><td class="text-center">' 
                                     + log.username.toUpperCase() + '</td></tr>'
                                }
                                uiAgentLogsContainer.append(sAgentLogsHtml);
                            })

                            $('div[modal-id="order-logs"]').addClass('showed');
                            //uiStoreLogsContainer.html("").append(sStoreLogsHtml);
                        }
                    }
                }
                admin.CconnectionDetector.ajax(oAjaxConfig);
            })

            if ($().datetimepicker !== undefined) {
                var dateTimePickerOptions = {
                    format  : 'MMMM DD, YYYY',
                    pickTime: false
                };

                var uiDstDateFrom = $('#dst_for_day_date_from_dp'),
                    uiDstDateTo = $('#dst_for_day_date_to_dp');

                uiDstDateFrom.datetimepicker(dateTimePickerOptions);
                uiDstDateTo.datetimepicker(dateTimePickerOptions);


                uiDstDateFrom.on("dp.change", function (e) {
                    uiDstDateTo.data('DateTimePicker').setMinDate(e.date);
                });

                uiDstDateTo.on("dp.change", function (e) {
                    uiDstDateFrom.data('DateTimePicker').setMaxDate(e.date);
                });
            }

            $('body').on('click' , 'div.option.custom-filter-option' , function() {
                 $(this).parents('div.select:first').find('input[type="text"]').attr('data-value' , $(this).attr('data-value'))       
            })

            // GENERATE REPORT CLICK EVENT FOR 'DST Reports per Day'
            //
            $('section#dst_per_day_reports_content').on('click', 'button.generate-dst', function () {
                var uiDateFrom = $('#dst_date_from').find('div.date-picker > input'),
                    uiDateTo = $('#dst_date_to').find('div.date-picker > input'),
                    sDateFrom = uiDateFrom.val(),
                    sDateTo = uiDateTo.val();

                if(sDateFrom.length > 0 && sDateTo.length > 0){

                    var sDateFromFormatted = moment(sDateFrom).format('YYYY-MM-DD'),
                        ssDateToFormatted = moment(sDateTo).format('YYYY-MM-DD');
                                    
                    
                    var oParams = {
                        "params": {
                            "limit"     : 9999,
                            "range_from": sDateFromFormatted,
                            "range_to"  : ssDateToFormatted,
                            "store_id"  : $('#dst_per_day_store').attr('data-value'),
                            "province_id" : $('#dst_per_day_province').attr('data-value'),
                            "order_type" : $('#dst_per_day_type').attr('data-value'),
                            "callcenter_id" : $('#dst_per_day_callcenter').attr('data-value'),
                        },
                        "user_id" : iConstantUserId
                    };
                    
                    var uiThis = $(this)
                    admin.agents.show_spinner(uiThis, true);

                    var oDstReportAjaxConfig = {
                        "type"   : "GET",
                        "data"   : oParams,
                        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                        "url"    : admin.config('url.api.jfc.reports') + "/reports/dst_report",
                        "success": function (oData) {
                            if (typeof(oData.status) != 'undefined' && typeof(oData.data) != 'undefined') {
                                admin.dst_reports.generate_dst_report_per_day(oData.data);
                                admin.agents.show_spinner(uiThis, false);
                            }
                        }

                    }

                    admin.dst_reports.ajax(oDstReportAjaxConfig);

                }

                /*var iStoreID = $('div.select.store-selection').find('input').attr('int-value')
                if($('div#dst_date_from').find('input:first').val() != "" && $('div#dst_date_to').find('input:first').val() != "")
                {
                    var uiThis = $(this);
                    admin.dst_reports.show_spinner(uiThis, true)

                    if($('section#dst_reports_header').find('input[name="generate_period"]').val() == 'Monthly')
                    {
                        var sDstFromDate = moment($('div#dst_date_from').find('input:first').val()).startOf('month').format('YYYY-MM-DD'),
                            sDstToDate = moment($('div#dst_date_to').find('input:first').val()).endOf('month').format('YYYY-MM-DD');
                    }
                    else
                    {
                        var sDstFromDate = $('div#dst_date_from').find('input:first').val(),
                            sDstToDate = $('div#dst_date_to').find('input:first').val();
                    }



                    var oParams = {
                        "params": {
                            "limit"     : 9999999999999999,
                            "range_from": moment(sDstFromDate).format('YYYY-MM-DD'),
                            "range_to"  : moment(sDstToDate).format('YYYY-MM-DD'),
                            "store_id"  : iStoreID
                        }
                    }

                    var oDstReportAjaxConfig = {
                        "type"   : "GET",
                        "data"   : oParams,
                        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                        "url"    : admin.config('url.api.jfc.reports') + "/reports/dst_report",
                        "success": function (oData) {
                            if (typeof(oData.status) != 'undefined' && typeof(oData.data) != 'undefined') {
                                admin.dst_reports.manipulate_dst_template(oData.data);
                            }
                            admin.dst_reports.show_spinner(uiThis, false)
                        }

                    }

                    if(sDstFromDate != "" && sDstToDate != "")
                    {
                        admin.dst_reports.ajax(oDstReportAjaxConfig);
                    }
                }*/


            });

            // GENERATE REPORT CLICK EVENT FOR 'DST Reports per Day' WITH WEEKLY MONTHLY DAILY FILTER
            //
            $('section#dst_per_day_reports_content').on('click', 'button.generate-dst-period', function(){
                var uiDateFrom = $('#dst_date_from').find('div.date-picker > input'),
                    uiDateTo = $('#dst_date_to').find('div.date-picker > input'),
                    uiPeriod = $('#dst_per_day_reports_content').find('.dd-txt'),
                    sPeriod = uiPeriod.val(),
                    sDateFrom = uiDateFrom.val(),
                    sDateTo = uiDateTo.val();

                if(sDateFrom.length > 0 && sDateTo.length > 0){

                    var sDateFromFormatted = moment(sDateFrom).format('YYYY-MM-DD'),
                        ssDateToFormatted = moment(sDateTo).format('YYYY-MM-DD');


                    var oParams = {
                        "params": {
                            "limit"     : 9999999999999999,
                            "range_from": sDateFromFormatted,
                            "range_to"  : ssDateToFormatted,
                            "store_id"  : 0
                        },
                        "user_id" : iConstantUserId
                    }


                    var oDstReportAjaxConfig = {
                        "type"   : "GET",
                        "data"   : oParams,
                        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                        "url"    : admin.config('url.api.jfc.reports') + "/reports/dst_report",
                        "success": function (oData) {
                            if (typeof(oData.status) != 'undefined' && typeof(oData.data) != 'undefined') {
                                admin.dst_reports.generate_dst_report_per_period(oData.data, sPeriod);
                            }
                        }

                    }

                    admin.dst_reports.ajax(oDstReportAjaxConfig);
                }
            });

            $('body').on('click', 'div.option.store-selection', function() {
                $(this).parents('div.select:first').find('input').attr('int-value', $(this).attr('int-value'))
            })

            $('body').on('click', 'a.link-view-data', function (e) {
                e.preventDefault();
                var uiThis = $(this);
                var iStoreID = $('div.store-selection.select').find('input').attr('int-value');
                var sDate = uiThis.attr('data-date-view');
                var oParams = {
                    "params": {
                        "limit"     : 9999999999999999,
                        "range_from": sDate,
                        "range_to"  : sDate,
                        "store_id"  : iStoreID
                    },
                    "user_id" : iConstantUserId
                }

                var oDstReportAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : admin.config('url.api.jfc.reports') + "/reports/dst_report",
                    "success": function (oData) {
                        if (typeof(oData.status) != 'undefined' && typeof(oData.data) != 'undefined') {
                            admin.dst_reports.manipulate_dst_template_specific(oData.data);
                        }
                    }

                }
                admin.dst_reports.ajax(oDstReportAjaxConfig);
            });

            $('div.row#dst_reports_specific_content').on('click', 'section.section-order', function () {
                $(this).find('div.order-overview').toggleClass('hidden')
                $(this).find('div.order-detailed').toggleClass('hidden')
            });

            $('section#dst_reports_specific_header').on('click', 'button.back_to_dst_report', function() {
                $('li[content="dst"]').trigger('click');
            })

            $('div.sort-container').on('click', 'strong.dst-sort', function() {
                var uiOrderContainers = $('div#dst_reports_specific_content:visible');
                var sSortKey = $(this).attr('sort');
                var iSortDirectionAsc;
                var iSortDirectionDesc;

                $(this).toggleClass('asc desc');

                if($(this).hasClass('asc') == true)
                {
                    iSortDirectionAsc = -1;
                    iSortDirectionDesc = 1;
                    $(this).siblings('img.asc').removeClass('hidden');
                    $(this).siblings('img.desc').addClass('hidden');
                }
                else
                {
                    iSortDirectionAsc = 1;
                    iSortDirectionDesc = -1;
                    $(this).siblings('img.asc').addClass('hidden');
                    $(this).siblings('img.desc').removeClass('hidden');
                }

                if(typeof(uiOrderContainers) != 'undefined')
                {
                    $.each(uiOrderContainers, function(key, container) {
                        $(container).children('section.section-order:not(.template)').sortDomElements(function(a,b){
                            var akey = $(a).attr(sSortKey);
                            var bkey = $(b).attr(sSortKey);
                            if (akey == bkey) return 0;
                            if (akey < bkey) return iSortDirectionAsc;
                            if (akey > bkey) return iSortDirectionDesc;
                        })
                    })
                }

            });

            $('section#dst_reports_specific_header').on('click', 'div.option.dst-filter', function () {
                var uiThis = $(this);
                uiThis.parents('div.select:first').find('input[name="filter-by"]').attr('data-filter', uiThis.attr('data-value') )

            })

            $('section#dst_reports_specific_header').on('click', 'button.filter-dst', function() {

                var uiThis = $(this);
                var sSearchVal = $('section#dst_reports_specific_header').find('input[name="dst-filter"]').val();
                var sFilter = $('section#dst_reports_specific_header').find('input[name="filter-by"]').attr('data-filter');

                admin.dst_reports.show_spinner(uiThis, true);

                setTimeout(function() {
                    admin.dst_reports.show_spinner(uiThis, false);
                }, 1000)

                $('div#dst_reports_specific_content').find('section.section-order:not(.template)').addClass('hidden');



                $('div#dst_reports_specific_content > section.section-order:not(.template)').filter(function( key, element ) {
                    return $(element).attr(sFilter).indexOf(sSearchVal) > -1
                }).removeClass('hidden');


            });

            $('section[section-style="content-panel"]').find('input.dd-txt').prop('readonly', true);

            $('#dst_per_day').on('click', 'a.viewdata', function(e){
                var uiThis = $(this);
                uiThis.html('<i class="fa fa-pulse fa-spinner"></i>');

                var uiTarget = $(e.target),
                    sDate = uiTarget.attr('date-value');
                admin.dst_reports.generate_view_data_report(sDate);
            });

            admin.dst_reports.get_provinces();

            $('div#dst_per_day_view_data').on('click', 'div.option', function() {
                var uiThis = $(this);
                uiThis.parents('div.select:first').find('input').attr('data-value' , uiThis.attr('data-value') );

                var iProvince = (typeof($('#dst_per_day_view_data div.select-filter-provinces').find('input:first').attr('data-value')) != 'undefined') ? $('#dst_per_day_view_data div.select-filter-provinces').find('input:first').attr('data-value') : 0;
                var iCallcenterId = (typeof($('#dst_per_day_view_data div.select-filter-call-centers').find('input:first').attr('data-value')) != 'undefined') ? $('#dst_per_day_view_data div.select-filter-call-centers').find('input:first').attr('data-value') : 0;
                //var iOrderType = (typeof($('#dst_per_day_view_data div.select-filter-transactions').find('input:first').attr('data-value')) != 'undefined') ? $('#dst_per_day_view_data div.select-filter-transactions').find('input:first').attr('data-value') : '';
                var sDate = $('#big_date').text();
                var sFormatedDated = moment(sDate).format('YYYY-MM-DD');

                admin.dst_reports.generate_view_data_report(sFormatedDated, iCallcenterId, undefined , iProvince);
            })
        },

        'table_to_excel' : function(table){
            table.table2excel({
                exclude: ".notthis",
                filename   : "dst_" + moment()
            });
        },

        'generate_dst_report_per_day' : function(oData){
            var uiExcelBtn = $('#dst_reports_header').find('button.excel_dst');

            if(Object.keys(oData).length > 0){
                var uiPaginationTableWrapper = $('div.report-pagination-table-wrapper'),
                    uiGenerateLogo = $('div.dst-not-generated'),
                    uiPaginationContainer = $('div[pagination-container="true"]'),
                    uiTableDiv =  uiPaginationTableWrapper.find('.tbl'),
                    uiTableTemplate = uiPaginationTableWrapper.find('table.viewable.hidden').clone();
                
                uiPaginationTableWrapper.removeClass('hidden');
                
                //Find existing table then remove
                if(uiTableDiv.find('table.report-data').length > 0){
                    uiTableDiv.find('table.report-data').remove(); 
                }

                //Hide table and logo
                uiPaginationTableWrapper.find('table.viewable').addClass('hidden');
                uiGenerateLogo.addClass('hidden');
                
                function pageAction(oData){
                    var uiTableExcel = $('#dst_per_day_reports_content').find('table.to-excel');
                    uiTableExcel.find('tbody').html('');

                    uiTableTemplate.addClass('report-data').find('tbody').html('');  
                    for(var i in oData){
                        var uiRow = uiTableTemplate.find('tbody > tr').clone();
                        var row = uiRow;
                        var oDate = {
                           'year' : moment(oData[i]['date']).format('YYYY'),
                           'day' : moment(oData[i]['date']).format('DD'),
                           'month' : moment(oData[i]['date']).format('MMM'),
                           'day_name' : moment(oData[i]['date']).format('dddd'),
                        };


                        var totalOf_20_30_45_yes = (parseInt(oData[i]['yes_20'])) + (parseInt(oData[i]['yes_30'])) + (parseInt(oData[i]['yes_45']));
                        var totalOf_20_30_45_no = (parseInt(oData[i]['no_20'])) + (parseInt(oData[i]['no_30'])) + (parseInt(oData[i]['no_45']));
                        var totalPercentHit = 0;
                        if(totalOf_20_30_45_yes > 0 && totalOf_20_30_45_no > 0){
                             totalPercentHit = (totalOf_20_30_45_yes / (totalOf_20_30_45_yes + totalOf_20_30_45_no)) * 100;
                        }

                        var sHtml = ''+
                        '<tr>'+
                        '    <td>'+
                        '       <div class="f-left">'+
                        '           <p class="f-left font-10">'+oDate.month+'<br>'+oDate.year+'</p>'+
                        '           <p class="f-left font-20">'+'<strong>'+oDate.day+'</strong>'+'</p>'+
                        '           <div class="clear"></div>'+
                        '           <p class="font-10 margin-left-5 margin-right-10">'+oDate.day_name+'</p>'+                           
                        '       </div>'+
                        '   </td>'+

                        '   <td>'+oData[i]['yes_20']+'</td>'+
                        '   <td>'+oData[i]['no_20']+'</td>'+
                        '   <td>'+(parseInt(oData[i]['yes_20']) + parseInt(oData[i]['no_20']))+'</td>'+
                        '   <td><strong>'+oData[i]['percent_20'].toFixed(1)+'</strong></td>'+

                        '   <td>'+oData[i]['yes_30']+'</td>'+
                        '   <td>'+oData[i]['no_30']+'</td>'+
                        '   <td>'+(parseInt(oData[i]['yes_30']) + parseInt(oData[i]['no_30']))+'</td>'+
                        '   <td><strong>'+oData[i]['percent_30'].toFixed(1)+'</strong></td>'+

                        '   <td>'+oData[i]['yes_45']+'</td>'+
                        '   <td>'+oData[i]['no_45']+'</td>'+
                        '   <td>'+(parseInt(oData[i]['yes_45']) + parseInt(oData[i]['no_45']))+'</td>'+
                        '   <td><strong>'+oData[i]['percent_45'].toFixed(1)+'</strong></td>'+

                        '   <td>'+oData[i]['advance_order']+'</td>'+

                        '   <td>0</td>'+
                        '   <td>0</td>'+

                        '   <td>'+totalOf_20_30_45_yes+'</td>'+
                        '   <td>'+totalOf_20_30_45_no+'</td>'+
                        '   <td>'+ (totalOf_20_30_45_yes + totalOf_20_30_45_no) +'</td>'+
                        '   <td><strong>'+totalPercentHit.toFixed(1)+'</strong></td>'+

                        '   <td><span class="font-20">'+(oData[i]['percent_all']).toFixed(1)+'%</span><br><a href="javascript:void(0)" class="viewdata" date-value="'+oData[i]['date']+'">View Data</a></td>'+
                        '</tr>';

                        uiTableTemplate.find('tbody').append(sHtml);


                        var sHtmlExcel = ''+
                        '<tr>'+
                        '    <td> '+oDate.month+'. '+oDate.day+', '+oDate.year+' </td> '+

                        '   <td>'+oData[i]['yes_20']+'</td>'+
                        '   <td>'+oData[i]['no_20']+'</td>'+
                        '   <td>'+(parseInt(oData[i]['yes_20']) + parseInt(oData[i]['no_20']))+'</td>'+
                        '   <td> '+oData[i]['percent_20'].toFixed(1)+' </td>'+

                        '   <td>'+oData[i]['yes_30']+'</td>'+
                        '   <td>'+oData[i]['no_30']+'</td>'+
                        '   <td>'+(parseInt(oData[i]['yes_30']) + parseInt(oData[i]['no_30']))+'</td>'+
                        '   <td> '+oData[i]['percent_30'].toFixed(1)+' </td>'+

                        '   <td>'+oData[i]['yes_45']+'</td>'+
                        '   <td>'+oData[i]['no_45']+'</td>'+
                        '   <td>'+(parseInt(oData[i]['yes_45']) + parseInt(oData[i]['no_45']))+'</td>'+
                        '   <td> '+oData[i]['percent_45'].toFixed(1)+' </td>'+

                        '   <td>'+oData[i]['advance_order']+'</td>'+

                        '   <td>0</td>'+
                        '   <td>0</td>'+

                        '   <td>'+totalOf_20_30_45_yes+'</td>'+
                        '   <td>'+totalOf_20_30_45_no+'</td>'+
                        '   <td>'+ (totalOf_20_30_45_yes + totalOf_20_30_45_no) +'</td>'+
                        '   <td> '+totalPercentHit.toFixed(1)+' </td>'+

                        '   <td> '+(oData[i]['percent_all']).toFixed(1)+'% </td>'+
                        '</tr>';
                        uiTableExcel.find('tbody').append(sHtmlExcel);
                    }


                    uiTableDiv.append(uiTableTemplate);
                    uiTableTemplate.removeClass('hidden');

                    setTimeout(function(){
                        uiExcelBtn.off('click').on('click', function(){
                            admin.dst_reports.table_to_excel($('#dst_per_day_reports_content').find('table.to-excel'));
                        });
                    },100);
                }

                reportPagination.paginate(uiPaginationContainer, 10, oData, pageAction, 'daily');

                uiExcelBtn.removeClass('disabled');

            }else{
                uiExcelBtn.addClass('disabled');
            }
        },

        'generate_dst_report_per_period' : function(oData, sPeriod){
            var uiExcelBtn = $('#dst_reports_header').find('button.excel_dst');

            if(Object.keys(oData).length > 0){
                var uiPaginationTableWrapper = $('div.report-pagination-table-wrapper'),
                    uiGenerateLogo = $('div.dst-not-generated'),
                    uiPaginationContainer = $('div[pagination-container="true"]'),
                    uiTableDiv =  uiPaginationTableWrapper.find('.tbl'),
                    uiTableTemplate = uiPaginationTableWrapper.find('table.viewable.hidden').clone();
                
                uiPaginationTableWrapper.removeClass('hidden');
                
                //Find existing table then remove
                if(uiTableDiv.find('table.report-data').length > 0){
                    uiTableDiv.find('table.report-data').remove(); 
                }

                //Hide table and logo
                uiPaginationTableWrapper.find('table.viewable').addClass('hidden');
                uiGenerateLogo.addClass('hidden');
                
                function pageAction(oData){
                    var uiTableExcel = $('#dst_per_day_reports_content').find('table.to-excel');
                    uiTableExcel.find('tbody').html('');

                    uiTableTemplate.addClass('report-data').find('tbody').html('');  
                    for(var i in oData){
                        var oDate = {
                           'year' : moment(oData[i]['date']).format('YYYY'),
                           'day' : moment(oData[i]['date']).format('DD'),
                           'month' : moment(oData[i]['date']).format('MMM'),
                           'day_name' : moment(oData[i]['date']).format('dddd'),
                        };


                        var totalOf_20_30_45_yes = (parseInt(oData[i]['yes_20'])) + (parseInt(oData[i]['yes_30'])) + (parseInt(oData[i]['yes_45']));
                        var totalOf_20_30_45_no = (parseInt(oData[i]['no_20'])) + (parseInt(oData[i]['no_30'])) + (parseInt(oData[i]['no_45']));
                        var totalPercentHit = 0;
                        if(totalOf_20_30_45_yes > 0 && totalOf_20_30_45_no > 0){
                             totalPercentHit = (totalOf_20_30_45_yes / (totalOf_20_30_45_yes + totalOf_20_30_45_no)) * 100;
                        }

                        var sHtml = ''+
                        '<tr>'+
                        '    <td>'+
                        '       <div class="f-left">'+
                        '           <p class="f-left font-10">'+oDate.month+'<br>'+oDate.year+'</p>'+
                        '           <p class="f-left font-20">'+'<strong>'+oDate.day+'</strong>'+'</p>'+
                        '           <div class="clear"></div>'+
                        '           <p class="font-10 margin-left-5 margin-right-10">'+oDate.day_name+'</p>'+                           
                        '       </div>'+
                        '   </td>'+

                        '   <td>'+oData[i]['yes_20']+'</td>'+
                        '   <td>'+oData[i]['no_20']+'</td>'+
                        '   <td>'+(parseInt(oData[i]['yes_20']) + parseInt(oData[i]['no_20']))+'</td>'+
                        '   <td><strong>'+oData[i]['percent_20'].toFixed(1)+'</strong></td>'+

                        '   <td>'+oData[i]['yes_30']+'</td>'+
                        '   <td>'+oData[i]['no_30']+'</td>'+
                        '   <td>'+(parseInt(oData[i]['yes_30']) + parseInt(oData[i]['no_30']))+'</td>'+
                        '   <td><strong>'+oData[i]['percent_30'].toFixed(1)+'</strong></td>'+

                        '   <td>'+oData[i]['yes_45']+'</td>'+
                        '   <td>'+oData[i]['no_45']+'</td>'+
                        '   <td>'+(parseInt(oData[i]['yes_45']) + parseInt(oData[i]['no_45']))+'</td>'+
                        '   <td><strong>'+oData[i]['percent_45'].toFixed(1)+'</strong></td>'+

                        '   <td>'+oData[i]['advance_order']+'</td>'+

                        '   <td>0</td>'+
                        '   <td>0</td>'+

                        '   <td>'+totalOf_20_30_45_yes+'</td>'+
                        '   <td>'+totalOf_20_30_45_no+'</td>'+
                        '   <td>'+ (totalOf_20_30_45_yes + totalOf_20_30_45_no) +'</td>'+
                        '   <td><strong>'+totalPercentHit.toFixed(1)+'</strong></td>'+

                        '   <td><span class="font-20">'+(oData[i]['percent_all']).toFixed(1)+'%</span><br><a href="javascript:void(0)" class="viewdata" date-value="'+oData[i]['date']+'">View Data</a></td>'+
                        '</tr>';

                        uiTableTemplate.find('tbody').append(sHtml);

                        var sHtmlExcel = ''+
                        '<tr>'+
                        '    <td> '+oDate.month+'. '+oDate.day+', '+oDate.year+' </td> '+

                        '   <td>'+oData[i]['yes_20']+'</td>'+
                        '   <td>'+oData[i]['no_20']+'</td>'+
                        '   <td>'+(parseInt(oData[i]['yes_20']) + parseInt(oData[i]['no_20']))+'</td>'+
                        '   <td> '+oData[i]['percent_20'].toFixed(1)+' </td>'+

                        '   <td>'+oData[i]['yes_30']+'</td>'+
                        '   <td>'+oData[i]['no_30']+'</td>'+
                        '   <td>'+(parseInt(oData[i]['yes_30']) + parseInt(oData[i]['no_30']))+'</td>'+
                        '   <td> '+oData[i]['percent_30'].toFixed(1)+' </td>'+

                        '   <td>'+oData[i]['yes_45']+'</td>'+
                        '   <td>'+oData[i]['no_45']+'</td>'+
                        '   <td>'+(parseInt(oData[i]['yes_45']) + parseInt(oData[i]['no_45']))+'</td>'+
                        '   <td> '+oData[i]['percent_45'].toFixed(1)+' </td>'+

                        '   <td>'+oData[i]['advance_order']+'</td>'+

                        '   <td>0</td>'+
                        '   <td>0</td>'+

                        '   <td>'+totalOf_20_30_45_yes+'</td>'+
                        '   <td>'+totalOf_20_30_45_no+'</td>'+
                        '   <td>'+ (totalOf_20_30_45_yes + totalOf_20_30_45_no) +'</td>'+
                        '   <td> '+totalPercentHit.toFixed(1)+' </td>'+

                        '   <td> '+(oData[i]['percent_all']).toFixed(1)+'% </td>'+
                        '</tr>';
                        uiTableExcel.find('tbody').append(sHtmlExcel);
                    }
                    uiTableDiv.append(uiTableTemplate);
                    uiTableTemplate.removeClass('hidden');

                    setTimeout(function(){
                        uiExcelBtn.off('click').on('click', function(){
                            admin.dst_reports.table_to_excel($('#dst_per_day_reports_content').find('table.to-excel'));
                        });
                    },100);
                }

                var perpage = 10;
                var sPeriodType = 'daily';
                if(sPeriod.toLowerCase() == 'monthly'){
                    perpage = 31;
                    sPeriodType = sPeriod.toLowerCase();
                }
                if(sPeriod.toLowerCase() == 'weekly'){
                    perpage = 7;
                    sPeriodType = sPeriod.toLowerCase();
                }

                reportPagination.paginate(uiPaginationContainer, perpage, oData, pageAction, sPeriodType);
            }
        },

        'manipulate_dst_template_specific': function (oDstData) {
            $('li[content="dst_spec"]').trigger('click');
			
            $.each(oDstData, function (key, dst) {
                var uiDstSpecific = $('section#dst_reports_specific_header');

                uiDstSpecific.find('.info_date').html(moment(dst.date).format('MMMM DD, YYYY'));
                uiDstSpecific.find('.info_month_year').html(moment(dst.date).format('MMMM') + '<br>' + moment(dst.date).format('YYYY'))
                //uiDstSpecific.find('.info_date').text(moment(dst.date).format('DD'));
                uiDstSpecific.find('.info_day').text(moment(dst.date).format('dddd'));
                uiDstSpecific.find('.yes_20').text(dst.yes_20);
                uiDstSpecific.find('.no_20').text(dst.no_20);
                uiDstSpecific.find('.order_20').text(dst.order_20);
                uiDstSpecific.find('.percent_20').text(dst.percent_20.toFixed(2) + '%');
                uiDstSpecific.find('.yes_30').text(dst.yes_30);
                uiDstSpecific.find('.no_30').text(dst.no_30);
                uiDstSpecific.find('.order_30').text(dst.order_30);
                uiDstSpecific.find('.percent_30').text(dst.percent_30.toFixed(2) + '%');
                uiDstSpecific.find('.yes_45').text(dst.yes_45);
                uiDstSpecific.find('.no_45').text(dst.no_45);
                uiDstSpecific.find('.order_45').text(dst.order_45);
                uiDstSpecific.find('.percent_45').text(dst.percent_45.toFixed(2) + '%');
                uiDstSpecific.find('.yes_all').text(dst.yes_all);
                uiDstSpecific.find('.no_all').text(dst.no_all);
                uiDstSpecific.find('.total_all').text(dst.total_all);
                uiDstSpecific.find('.advance_order').text(dst.advance_order);
                uiDstSpecific.find('.bulk_order').text(dst.bulk_order);
                uiDstSpecific.find('.exception').text(dst.exempted_order);
                uiDstSpecific.find('.percent_all').text(dst.percent_all.toFixed(2) + '%');
                $('div#dst_reports_specific_content').find('section.section-order:not(.template)').remove();
                if (typeof(dst.orders) != 'undefined') {
                    $.each(dst.orders, function (key, order) {
                        var oOrderTemplate = $('section.section-order.template').clone().removeClass('template');

                        oOrderTemplate.find('.data-order-id').text(order.id)

                        if (typeof(order.display_data) != 'undefined') {
                            var oDisplayData = $.parseJSON(order.display_data);
                            oOrderTemplate.find('.data-full-name').text(oDisplayData.customer_name)
							// console.log(" HIT RATE UNDEFINED: " + order.id + " " + order.hitrate);
							var hitrate = 0;
							if (typeof (order.hitrate) != 'undefined')
							{
								hitrate = order.hitrate;
							}
                            oOrderTemplate.attr('data-customer-name', oDisplayData.customer_name)
                            oOrderTemplate.attr('data-order-id', order.id)
                            oOrderTemplate.attr('data-hit-rate', order.hitrate)
                            oOrderTemplate.find('.data-hit-rate').text( hitrate + ' %');
                            oOrderTemplate.find('.data-order-date').text(order.date_added);
                            oOrderTemplate.find('.data-contact-number').text(oDisplayData.contact_number);
                            oOrderTemplate.find('.data-full-address').text(oDisplayData.full_address)
                            oOrderTemplate.find('.data-full-address').text(oDisplayData.full_address)
                            //oOrderTemplate.find('.data-full-address').text(oDisplayData.full_address)
                            oOrderTemplate.find('info.change_for').text(order.change_for)
                            oOrderTemplate.find('[data-label="total_cost"]').text(oDisplayData.total_cost)
                            oOrderTemplate.find('[data-label="added_vat"]').text(oDisplayData.added_vat)
                            oOrderTemplate.find('[data-label="total_bill"]').text(oDisplayData.total_bill)
                            oOrderTemplate.find('[data-label="delivery_charge"]').text(oDisplayData.delivery_charge)

                            if (typeof(oDisplayData.happy_plus) != 'undefined') {
                                $.each(oDisplayData.happy_plus, function (k, v) {
                                    var uiHappyPlusTemplate = oOrderTemplate.find('div.happy_plus_cards.template').clone().removeClass('template');
                                    uiHappyPlusTemplate.find('p[data-label="card_number"]').text(v.number);
                                    uiHappyPlusTemplate.find('p[data-label="card_expiration"]').text(v.date);
                                    oOrderTemplate.find('div.happy_plus_cards_container').append(uiHappyPlusTemplate);
                                });
                            }

                            if(typeof(oDisplayData.products) != 'undefined')
                            {
                                $.each(oDisplayData.products, function(key, product) {
                                    if(typeof(product) != 'undefined' && product != "" && product != null)
                                    {
                                        var uiProductContainer = oOrderTemplate.find('tbody[data-transaction-orders="detailed_orders"]');
                                        var uiProduct = oOrderTemplate.find('tr[data-template-transaction-for="order"].template').clone().removeClass('template');
                                        uiProduct.find('td[data-label="quantity"]').text(product.quantity);
                                        uiProduct.find('td[data-label="product"]').text(product.product_name)
                                        uiProduct.find('td[data-label="price"]').text(product.price)
                                        uiProduct.find('td[data-label="sub_total"]').text(product.sub_total)
                                        uiProductContainer.append(uiProduct)

                                        if(typeof(product.addons) != 'undefined')
                                        {
                                            $.each(product.addons, function(key, addon) {
                                                var uiAddon = oOrderTemplate.find('tr[data-template-transaction-for="order"].template').clone().removeClass('template');
                                                uiAddon.find('td[data-label="quantity"]').text(addon.quantity);
                                                uiAddon.find('td[data-label="product"]').text(addon.product_name)
                                                uiAddon.find('td[data-label="price"]').text(addon.price)
                                                uiAddon.find('td[data-label="sub_total"]').text(addon.sub_total)
                                                uiProductContainer.append(uiAddon)
                                            })


                                        }

                                        if(typeof(product.special) != 'undefined')
                                        {
                                            $.each(product.special, function(key, spcl) {
                                                var uiSpecial = oOrderTemplate.find('tr[data-template-transaction-for="order"]').clone().removeClass('template');
                                                uiSpecial.find('td[data-label="quantity"]').text(spcl.quantity);
                                                uiSpecial.find('td[data-label="product"]').text(spcl.product_name)
                                                uiSpecial.find('td[data-label="price"]').text(spcl.price)
                                                uiSpecial.find('td[data-label="sub_total"]').text(spcl.sub_total)
                                                uiProductContainer.append(uiSpecial)
                                            })
                                        }

                                        if(typeof(product.upgrades) != 'undefined')
                                        {
                                            $.each(product.upgrades, function(key, upgrade) {
                                                var uiUpgrades = oOrderTemplate.find('tr[data-template-transaction-for="order"]').clone().removeClass('template');
                                                uiUpgrades.find('td[data-label="quantity"]').text(upgrade.quantity);
                                                uiUpgrades.find('td[data-label="product"]').text(upgrade.product_name)
                                                uiUpgrades.find('td[data-label="price"]').text(upgrade.price)
                                                uiUpgrades.find('td[data-label="sub_total"]').text(upgrade.sub_total)
                                                uiProductContainer.append(uiUpgrades)
                                            })
                                        }
                                    }
                                })


                            }
                        }

                        $('div#dst_reports_specific_content').append(oOrderTemplate);
                    })
                }

            });
        },

        'manipulate_dst_template': function (oDstData) {
            var aClonedTrs = [];
            var aDownloadaClonedTrs = [];
            var uiDstContainer = $('div.dst-generated').find('tbody.dst-container');
            var uiDownloadDstContainer = $('div.dst-generated').find('tbody.download-dst-container');

            $.each(oDstData, function (key, dst) {

                var uiDstTemplate = $('tr.dst-row.template').clone().removeClass('template');
                var uiDownloadDstTemplate = $('tr.dst-row-download.template').clone().removeClass('template').removeClass('notthis');

                uiDstTemplate.find('.info_date').html(moment(dst.date).format('MMMM DD YYYY'));
                uiDstTemplate.find('.info_month_year').html(moment(dst.date).format('MMMM') + '<br>' + moment(dst.date).format('YYYY'))
                uiDstTemplate.find('.info_date').text(moment(dst.date).format('DD'));
                uiDstTemplate.find('.info_day').text(moment(dst.date).format('dddd'));
                uiDstTemplate.find('.yes_20').text(dst.yes_20);
                uiDstTemplate.find('.no_20').text(dst.no_20);
                uiDstTemplate.find('.order_20').text(dst.order_20);
                uiDstTemplate.find('.percent_20').text(dst.percent_20.toFixed(2) + '%');
                uiDstTemplate.find('.yes_30').text(dst.yes_30);
                uiDstTemplate.find('.no_30').text(dst.no_30);
                uiDstTemplate.find('.order_30').text(dst.order_30);
                uiDstTemplate.find('.percent_30').text(dst.percent_30.toFixed(2) + '%');
                uiDstTemplate.find('.yes_45').text(dst.yes_45);
                uiDstTemplate.find('.no_45').text(dst.no_45);
                uiDstTemplate.find('.order_45').text(dst.order_45);
                uiDstTemplate.find('.percent_45').text(dst.percent_45.toFixed(2) + '%');
                uiDstTemplate.find('.yes_all').text(dst.yes_all);
                uiDstTemplate.find('.no_all').text(dst.no_all);
                uiDstTemplate.find('.total_all').text(dst.total_all);
                uiDstTemplate.find('td.advance_order').text(dst.advance_order);
                uiDstTemplate.find('.percent_all').text(dst.percent_all.toFixed(2) + '%');

                uiDstTemplate.find('a.link-view-data').attr('data-date-view', dst.date)

                aClonedTrs.push(uiDstTemplate);

                uiDownloadDstTemplate.find('.full_date').html(moment(dst.date).format('MMMM DD YYYY'));
                uiDownloadDstTemplate.find('.yes_20').text(dst.yes_20);
                uiDownloadDstTemplate.find('.no_20').text(dst.no_20);
                uiDownloadDstTemplate.find('.order_20').text(dst.order_20);
                uiDownloadDstTemplate.find('.percent_20').text(dst.percent_20.toFixed(2) + '%');
                uiDownloadDstTemplate.find('.yes_30').text(dst.yes_30);
                uiDownloadDstTemplate.find('.no_30').text(dst.no_30);
                uiDownloadDstTemplate.find('.order_30').text(dst.order_30);
                uiDownloadDstTemplate.find('.percent_30').text(dst.percent_30.toFixed(2) + '%');
                uiDownloadDstTemplate.find('.yes_45').text(dst.yes_45);
                uiDownloadDstTemplate.find('.no_45').text(dst.no_45);
                uiDownloadDstTemplate.find('.order_45').text(dst.order_45);
                uiDownloadDstTemplate.find('.percent_45').text(dst.percent_45.toFixed(2) + '%');
                uiDownloadDstTemplate.find('.yes_all').text(dst.yes_all);
                uiDownloadDstTemplate.find('.no_all').text(dst.no_all);
                uiDownloadDstTemplate.find('.total_all').text(dst.total_all);
                uiDownloadDstTemplate.find('td.advance_order').text(dst.advance_order);
                uiDownloadDstTemplate.find('.percent_all').text(dst.percent_all.toFixed(2) + '%');
                uiDownloadDstTemplate.find('a.link-view-data').remove();

                aDownloadaClonedTrs.push(uiDownloadDstTemplate)
            });

            $('div.dst-generated').removeClass('hidden');
            $('div.dst-not-generated ').addClass('hidden');
            $('section#dst_reports_header button.excel_dst').removeClass('disabled');
            uiDstContainer.find('tr.dst-row:not(.template)').remove();
            uiDownloadDstContainer.find('tr.dst-row-download:not(.template)').remove();
            uiDstContainer.append(aClonedTrs);
            uiDownloadDstContainer.append(aDownloadaClonedTrs);
        },


        'ajax': function (oAjaxConfig) {
            if (typeof(oAjaxConfig) != 'undefined') {
                admin.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        'show_spinner': function (uiElem, bShow) {
            if (bShow == true) {
                if (uiElem.find('i.fa-spinner').length > 0) {
                    uiElem.find('i.fa-spinner').removeClass('hidden');
                }
                else {
                    uiElem.removeClass('hidden');
                }

                uiElem.prop('disabled', true);
            }
            else {
                if (uiElem.find('i.fa-spinner').length > 0) {
                    uiElem.find('i.fa-spinner').addClass('hidden');
                }
                uiElem.prop('disabled', false);
            }
        },

        'generate_view_data_report' : function(date, iCallcenterId, iOrderType, iProvince){
//             $('#dst_per_day').addClass('hidden');
//             $('#dst_reports_header').addClass('hidden');
//             $('#dst_per_day_view_data_header').removeClass('hidden');
//             $('#dst_per_day_view_data').removeClass('hidden');
            
            var sDate = moment(date).format('MMMM DD, YYYY');
            
            $('#smal_date').html(sDate+' (View Data)');
            $('#big_date').html(sDate);

            $('#backto_dst').off('click').on('click', function(e){
               e.preventDefault();
               $('#dst_per_day').removeClass('hidden');
               $('#dst_reports_header').removeClass('hidden');
               $('#dst_per_day_view_data_header').addClass('hidden');
               $('#dst_per_day_view_data').addClass('hidden');
            });

            var oParams = {
                "params": {
                        "limit"     : 9999,
                        "date"      : date,
                        //"callcenter_id" : iCallcenterId,
                        /*"order_type" : iOrderType,*/
                        //"province" : iProvince
                        "store_id"  : $('#dst_per_day_store').attr('data-value'),
                        "province_id" : $('#dst_per_day_province').attr('data-value'),
                        //"order_type" : $('#dst_per_day_type').attr('data-value'),
                        "callcenter_id" : $('#dst_per_day_callcenter').attr('data-value'),
                }
            }


            var oDstReportAjaxConfig = {
                "type"   : "GET",
                "data"   : oParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : admin.config('url.api.jfc.reports') + "/reports/dst_report_view_data",
                "success": function (oData) {
                    if (typeof(oData.status) != 'undefined' && typeof(oData.data) != 'undefined') {
                        admin.dst_reports.manipulate_view_data_report(oData.data);
                        $('#dst_per_day').addClass('hidden');
                        $('#dst_reports_header').addClass('hidden');
                        $('#dst_per_day_view_data_header').removeClass('hidden');
                        $('#dst_per_day_view_data').removeClass('hidden');
                        $('#dst_per_day').find('a.viewdata').html('View Data');
                    }
                }

            }

            admin.dst_reports.ajax(oDstReportAjaxConfig);
        },

        'manipulate_view_data_report' : function(oData){
            var uiExcelBtn = $('#dst_per_day_view_data_header').find('button.excel_dst');
            var uiSearchProvince = $('#dst_per_day_view_data_content').find('.select-provinces'),
                uiFilterProvince = $('#dst_per_day_view_data_content').find('.select-filter-provinces'),
                uiFilterTrancsaction = $('#dst_per_day_view_data_content').find('.select-filter-transactions'),
                uiFilterCallCenter = $('#dst_per_day_view_data_content').find('.select-filter-call-centers'),
                oProvinces = admin.dst_reports.provinces;

            if(!uiSearchProvince.attr('is-dirty')){
                for(var i in oProvinces){
                    var sOption = '<div class="option" data-value="'+oProvinces[i]['id']+'">'+oProvinces[i]['name']+'</div>';
                    uiSearchProvince.find('.frm-custom-dropdown-option').append(sOption);
                }
                uiSearchProvince.attr('is-dirty', 'true');
            }
            if(!uiFilterProvince.attr('is-dirty')){
               for(var i in oProvinces){
                   var sOption = '<div class="option" data-value="'+oProvinces[i]['id']+'">'+oProvinces[i]['name']+'</div>';
                    uiFilterProvince.find('.frm-custom-dropdown-option').append(sOption);
                }
                uiFilterProvince.attr('is-dirty', 'true');
            }
            
            

            if(Object.keys(oData).length > 0){
                var uiTableTemplate = $('#dst_per_day_view_data_content').find('table.template').clone(),
                    uiTableDiv = $('#dst_per_day_view_data').find('.tbl'),
                    uiPaginationContainer = $('#dst_per_day_view_data_content').find('div[pagination-container="true"]');
                
                uiTableTemplate.removeClass('template');

                //Find existing table then remove
                if(uiTableDiv.find('table.report-data').length > 0){
                    uiTableDiv.find('table.report-data').remove(); 
                }

                function pageAction(oData){
                    var uiTableExcel = $('#dst_per_day_view_data_content').find('table.to-excel');
                    uiTableExcel.find('tbody').html('');

                    uiTableTemplate.addClass('report-data').find('tbody').html('');
                    for(var i in oData){

                        var totalOf_20_30_45_yes = (parseInt(oData[i]['yes_20'])) + (parseInt(oData[i]['yes_30'])) + (parseInt(oData[i]['yes_45']));
                        var totalOf_20_30_45_no = (parseInt(oData[i]['no_20'])) + (parseInt(oData[i]['no_30'])) + (parseInt(oData[i]['no_45']));
                        var totalPercentHit = 0;
                        if(totalOf_20_30_45_yes > 0 && totalOf_20_30_45_no > 0){
                             totalPercentHit = (totalOf_20_30_45_yes / (totalOf_20_30_45_yes + totalOf_20_30_45_no)) * 100;
                        }

                        var sHtml = ''+
                        '<tr>'+
                        '   <td colspan="20" class="no-padding-all">'+
                        '       <table class="no-bg">'+
                        '           <tbody class="font-14 padding-bottom-15">'+
                        '               <tr>'+
                        '                   <td colspan="20"> <p class="text-left"> <span>'+oData[i]['store_name']+'</span> </p> </td>'+
                        '               </tr>'+
                        '               <tr>'+
                        '                   <td class="width-25px">'+oData[i]['yes_20']+'</td>'+
                        '                   <td class="width-25px">'+oData[i]['no_20']+'</td>'+
                        '                   <td class="width-45px">'+(parseInt(oData[i]['yes_20']) + parseInt(oData[i]['no_20']))+'</td>'+
                        '                   <td class="width-20px"> <strong>'+(oData[i]['percent_20']).toFixed(1)+'</strong> </td>'+
                        '                   <td class="width-30px">'+oData[i]['yes_30']+'</td>'+
                        '                   <td class="width-25px">'+oData[i]['no_30']+'</td>'+
                        '                   <td class="width-45px">'+(parseInt(oData[i]['yes_30']) + parseInt(oData[i]['no_30']))+'</td>'+
                        '                   <td class="width-30px"> <strong>'+(oData[i]['percent_30']).toFixed(1)+'</strong> </td>'+
                        '                   <td class="width-30px">'+oData[i]['yes_45']+'</td>'+
                        '                   <td class="width-25px">'+oData[i]['no_45']+'</td>'+
                        '                   <td class="width-45px">'+(parseInt(oData[i]['yes_45']) + parseInt(oData[i]['no_45']))+'</td>'+
                        '                   <td class="width-30px"> <strong>'+(oData[i]['percent_45']).toFixed(1)+'</strong> </td>'+
                        '                   <td class="width-90px">'+oData[i]['advance_order']+'</td>'+
                        '                   <td class="width-75px">0</td>'+
                        '                   <td class="width-70px">0</td>'+
                        '                   <td class="width-25px">'+totalOf_20_30_45_yes+'</td>'+
                        '                   <td class="width-25px">'+totalOf_20_30_45_no+'</td>'+
                        '                   <td class="width-40px">'+(totalOf_20_30_45_yes + totalOf_20_30_45_no)+'</td>'+
                        '                   <td class="width-30px"> <strong>'+(totalPercentHit).toFixed(1)+'</strong> </td>'+
                        '                   <td class="width-120px"> <strong>'+(oData[i]['percent_all']).toFixed(1)+'%</strong> </td>'+
                        '               </tr>'+
                        '           </tbody>'+
                        '       </table>'+
                        '   </td>'+
                        '</tr>';

                        uiTableTemplate.find('tbody').eq(0).append(sHtml);

                        var sExcelHtml = ''+
                        '<tr>'+
                        '   <td colspan="20" class="no-padding-all">'+
                        '               <tr>'+
                        '                   <td colspan="20"> '+oData[i]['store_name']+' </td>'+
                        '               </tr>'+
                        '               <tr>'+
                        '                   <td class="width-25px">'+oData[i]['yes_20']+'</td>'+
                        '                   <td class="width-25px">'+oData[i]['no_20']+'</td>'+
                        '                   <td class="width-45px">'+(parseInt(oData[i]['yes_20']) + parseInt(oData[i]['no_20']))+'</td>'+
                        '                   <td class="width-20px"> '+(oData[i]['percent_20']).toFixed(1)+' </td>'+
                        '                   <td class="width-30px">'+oData[i]['yes_30']+'</td>'+
                        '                   <td class="width-25px">'+oData[i]['no_30']+'</td>'+
                        '                   <td class="width-45px">'+(parseInt(oData[i]['yes_30']) + parseInt(oData[i]['no_30']))+'</td>'+
                        '                   <td class="width-30px"> '+(oData[i]['percent_30']).toFixed(1)+' </td>'+
                        '                   <td class="width-30px">'+oData[i]['yes_45']+'</td>'+
                        '                   <td class="width-25px">'+oData[i]['no_45']+'</td>'+
                        '                   <td class="width-45px">'+(parseInt(oData[i]['yes_45']) + parseInt(oData[i]['no_45']))+'</td>'+
                        '                   <td class="width-30px"> '+(oData[i]['percent_45']).toFixed(1)+' </td>'+
                        '                   <td class="width-90px">'+oData[i]['advance_order']+'</td>'+
                        '                   <td class="width-75px">0</td>'+
                        '                   <td class="width-70px">0</td>'+
                        '                   <td class="width-25px">'+totalOf_20_30_45_yes+'</td>'+
                        '                   <td class="width-25px">'+totalOf_20_30_45_no+'</td>'+
                        '                   <td class="width-40px">'+(totalOf_20_30_45_yes + totalOf_20_30_45_no)+'</td>'+
                        '                   <td class="width-30px"> '+(totalPercentHit).toFixed(1)+' </td>'+
                        '                   <td class="width-120px"> '+(oData[i]['percent_all']).toFixed(1)+'% </td>'+
                        '               </tr>'+
                        '   </td>'+
                        '</tr>';

                        uiTableExcel.find('tbody[report-body]').eq(0).append(sExcelHtml);

                    }
                    uiTableDiv.append(uiTableTemplate);
                    uiTableTemplate.removeClass('hidden');

                    setTimeout(function(){
                        uiExcelBtn.off('click').on('click', function(){
                            admin.dst_reports.table_to_excel($('#dst_per_day_view_data_content').find('table.to-excel'));
                        });
                    },100);
                }

                reportPagination.paginate(uiPaginationContainer, 10, oData, pageAction, 'daily');

                $('#dst_per_day_view_data_content').find('.contents').show();
            }else{
                $('#dst_per_day_view_data_content').find('.contents').hide();
            }
        }

    }

}());

$(window).load(function () {
    admin.dst_reports.initialize();
});


