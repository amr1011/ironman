/**
 *  Search Order Class
 *
 *
 *
 */
(function () {
    "use strict";
    var uiPage = 'auth';

    CPlatform.prototype.auth = {

        'initialize' : function () {
			$('div.login-error-msg').hide();
            
            $("section.login-parent div.login-form input.normal").keypress( function (e) {
                if( e.keyCode == 13 ){
                    $("section.login-parent div.login-form button.btn-login").click();
                }
            });

			$('section.login-parent div.login-form button.btn-login').on('click', function (e) {
                var uiContainer = $('section.login-parent div.login-form div.login-error-msg');
                admin.auth.show_spinner($('section.login-parent div.login-form button.btn-login'),true);
                var oType = $('section.login-parent div.login-form').find('input[name="type"]').val('2'); //type value for admin is '2'
                var oParams = {
                    "params": {
                        "username" : $('section.login-parent div.login-form').find('input[name="username"]').val(),
                        "password" : $('section.login-parent div.login-form').find('input[name="password"]').val(),
                        "type" : oType.val(),
                        "user_level" : true
                    }
                }

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : admin.config('url.api.jfc.users') + "/users/authenticate",
                    "success": function (oData) {
                        if(oData.status == 1)
                        {
                        	admin.auth.show_spinner($('section.login-parent div.login-form button.btn-login'),false);
                            var oAjaxWhitelistConfig = {
                                "type"   : "POST",
                                "data"   : {"data": "data"},
                                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                                "url"    : admin.config('url.server.base') + "auth/login/server_checklist",
                                "success": function (sChecklistData) {
                                    var oChecklistData = sChecklistData;
									
                                    if(typeof(oChecklistData.passed) != 'undefined')
                                    {
                                        if(oData.data.is_blacklisted == 1)
                                        {
                                            alert('Sorry your account/username is blacklisted, Please contact your Administrator')
                                        }
                                        else
                                        {
                                            if(oChecklistData.passed || oData.data.is_ip_whitelist_excluded > 0) {
                                                var oUserInfo = encodeURIComponent( JSON.stringify(oData.data) );
                                                var oUserInformation = {
                                                    'params' : oData.data
                                                };
                                                // window.location.href = admin.config('url.server.base') + "auth/login/landing_page/"+oUserInfo;
                                                admin.auth.save_session(oUserInformation);
                                            }
                                            else
                                            {
                                                alert(oChecklistData.message);
                                            }
                                        }

                                    }
                                }
                            }

                            admin.auth.get(oAjaxWhitelistConfig);

                        }
                        else
                        {
                        	admin.auth.show_spinner($('section.login-parent div.login-form button.btn-login'),false);
                            var sError = '<p class="error_messages"><i class="fa fa-exclamation-triangle"></i> ' + oData.message + '</p>';
                            if(oData.message == 'Username is required.' || oData.message == 'Password is required.')
                            {
                                $('section.login-parent div.login-form').find('input[name="username"]').addClass('error');
                                $('section.login-parent div.login-form').find('input[name="password"]').addClass('error');
                            }
                            if($('section.login-parent div.login-form').find('input[name="username"]').val() != '')
                            {
                                $('section.login-parent div.login-form').find('input[name="username"]').removeClass('error');
                            }
                            if($('section.login-parent div.login-form').find('input[name="password"]').val() != '')
                            {
                                $('section.login-parent div.login-form').find('input[name="password"]').removeClass('error');
                            }
                            if(oData.message == 'Incorrect user password.')
                            {
                                $('section.login-parent div.login-form').find('input[name="password"]').addClass('error');
                            }
                            admin.auth.add_error_message(sError, uiContainer);
                        }
                    },
                    "error" : function () {

                    }
                };
                admin.auth.get(oAjaxConfig);
            });

            $('header[custom-style="header"] div.profile-container button.btn-logout').on('click', function(){
                window.location.href = admin.config('url.server.base') + "auth/login/logout";
            });
			
			if($('section.login-parent').is(':visible'))
            {
                $('header[custom-style="header"]').hide();
            }
			
			

        },
		'get': function (oAjaxConfig) {
            if (typeof(oAjaxConfig) != 'undefined') {
                admin.CconnectionDetector.ajax(oAjaxConfig);
            }
        },
		'add_error_message': function (sMessages, uiContainer) {
            $('p.error_messages').remove();
            if (sMessages.length > 0) {
                if (typeof(uiContainer) != 'undefined') {
                    uiContainer.empty();
                    uiContainer.show();
                    uiContainer.append(sMessages);
                    uiContainer.find("p.error_messages:contains('is required')").hide();

                    if (uiContainer.find('p.error_messages').is(":contains('is required')")) {
                        uiContainer.append("<p class='error_messages'><i class='fa fa-exclamation-triangle'></i> Please fill up all required fields.</p>");
                    }
                    // if($("section.content #add_customer_content div#city_select").find('div.select input.dd-txt').hasClass('error'))
                    // {
                    //     //console.log($("section.content #add_customer_content div#city_select").find('div.select input.dd-txt').closest('div.select').css("border-color", "red"));
                    // }
                }

            }
        },
        'show_spinner' : function (uiElem, bShow) {
            if (bShow == true) {
                if(uiElem.find('i.fa-spinner').length > 0)
                {
                    uiElem.find('i.fa-spinner').removeClass('hidden');
                }
                else
                {
                    uiElem.append('<i class="fa fa-spinner fa-pulse"></i>');
                }

                uiElem.prop('disabled', true);
            }
            else {
                if(uiElem.find('i.fa-spinner').length > 0)
                {
                    uiElem.find('i.fa-spinner').addClass('hidden');
                    uiElem.find('i.fa-spinner').remove();
                }
                uiElem.prop('disabled', false);
            }
        },

        save_session : function(oUserInformation){
            if(typeof(oUserInformation) != 'undefined')
            {
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oUserInformation,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : admin.config('url.server.base') + "auth/login/landing_page",
                    "success": function (data) {
                        if(typeof(data) != 'undefined')
                        {
                            // console.log(data)
                            window.location.href = admin.config('url.server.base') + "admin";
                        }
                    }
                };

                admin.auth.get(oAjaxConfig);
            }
        },
    }

}());

$(window).load(function () {
    admin.auth.initialize();
});


