(function () {
    "use strict";
    var arrAgents = [],
        arrIpWhitelistExcludedAgents = [],
        iPagination = 25;
    CPlatform.prototype.agents = {

        'initialize' : function () {

            $("select").transformDD();
            admin.agents.provinces();
            admin.agents.fetch_callcenters();
               
            var oParams = {
                    "params" : {
                        "offset": 0,        
                        "limit" : 1000,
                        "type" : 2,
                        "where" : [
                            /*{
                                "key": "cu.user_level",
                                "operator" : "=",
                                "value": "1"
                            },*/
                            {
                                "key": "u.sbu_id",
                                "operator" : "=",
                                "value": "1"
                            },
                             {
                                "key": "cu.callcenter_id",
                                "operator" : ">",
                                "value": "0"
                            }
                        ]
                    }

                };
             admin.agents.fetch_agents(oParams);

            $('div[content="agent_list"]').on('click', 'section.agent-list div.agent-overview', function(e) {
                $(this).addClass('hidden');
                $(this).parents('section.agent-list:first').find('div.agent-detailed').removeClass('hidden');

                /*var iTop = $(this).find('div.agent-detailed').top,
                    iFinalTopValue = iTop - 10;

                $('html,body').animate({
                    scrollTop: iFinalTopValue
                }, 100);*/

                var uiMainContainer = $(this).parents('section.agent-list:first');
                var iAgentID = uiMainContainer.attr('data-agent-id');
                var oParams = {
                    "params": {
                            "limit": 99999,
                            "where": [
                                {
                                    "field"   : 'agent_id',
                                    "operator": "=",
                                    "value"   : iAgentID
                                },
                            ]
                    }
                }
                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : admin.config('url.api.jfc.orders') + "/orders/fetch_agent_orders",
                    "success": function (oData) {
                        //console.log(oData);
                        if(oData.status == true)
                        {
                           //console.log(oData.data.length);
                           uiMainContainer.find('strong.info_order_processed_count').text(oData.data[0].order_processed_count);
                        }
                    }
                };

                admin.agents.ajax(oAjaxConfig);

            })

            $('div[content="agent_list"]').on('click', 'section.agent-list div.agent-detailed', function(e) {
                if(e.target.nodeName == "BUTTON" || e.target.nodeName == "A")
                {
                    //
                }
                else
                {
                    $(this).addClass('hidden');
                    $(this).parents('section.agent-list:first').find('div.agent-overview').removeClass('hidden');
                }
                
            })

            $('div.agent-list-container').on('click', 'div.agent-detailed button.archive_user', function() {
                    var uiParentContainer = $(this).parents('section.agent-list');
                    $('div[modal-id="archive"]').addClass('showed');
                    $('div[modal-id="archive"]').attr('data-agent-id', uiParentContainer.attr('data-agent-id'))    
            })

            $('div.agent-list-container').on('click', 'div.agent-detailed button.edit_user', function() {
                var iAgentID = $(this).parents('section.agent-list:first').attr('data-agent-id');
                
                $('div.add-agent-container').addClass('hidden')

                $('div[content="agent_list"]').find('div.edit-agent-container').removeClass('hidden');

                var oAgents = cr8v.get_from_storage('agents');
                var uiEditContainer = $('div[content="agent_list"]').find('div.edit-agent-container');
                
                var iTop = uiEditContainer.offset().top,
                iFinalTopValue = iTop;

                $('html,body').animate({
                    scrollTop: iFinalTopValue
                }, 100);

                if(typeof(oAgents) != 'undefined' && typeof(iAgentID) != 'undefined')
                {
                     var oSelectedAgent = $.grep(oAgents, function(e){ return e.user_id == iAgentID; });       
                     //console.log(oSelectedAgent)
                     if(typeof(oSelectedAgent[0]) != 'undefined')
                     {
                          var oAgent = oSelectedAgent[0];
                          uiEditContainer.find('input[name="username"]').val(oAgent.username)
                          uiEditContainer.find('input[name="first_name"]').val(oAgent.first_name)
                          uiEditContainer.find('input[name="middle_name"]').val(oAgent.middle_name)
                          uiEditContainer.find('input[name="last_name"]').val(oAgent.last_name)
                          //console.log(oAgent.is_ip_whitelist_excluded)
                          if(oAgent.is_ip_whitelist_excluded > 0)
                          {
                              uiEditContainer.find('input#ip-whitelist-exclude-edit').prop('checked', true)
                          }
                          if(oAgent.is_blacklisted > 0)
                          {
                              uiEditContainer.find('input#is-blacklisted').prop('checked', true)
                          }
                          uiEditContainer.find('input[name="user_id"]').val(oAgent.user_id)
                          uiEditContainer.find('input[name="email_address"]').val(oAgent.email_address)
                          var sCallcenter = uiEditContainer.find('div.callcenters-choices .frm-custom-dropdown-option').find('div.option.callcenter-select[data-value="'+oAgent.callcenter_id+'"]').text();
                          var iCallcenter = uiEditContainer.find('div.callcenters-choices .frm-custom-dropdown-option').find('div.option.callcenter-select[data-value="'+oAgent.callcenter_id+'"]').attr('data-value');
                          var sUserRole = uiEditContainer.find('div.callcenters-role .frm-custom-dropdown-option').find('div.option.agent-type[data-value="'+oAgent.user_level+'"]').text();
                          var iUserRole = uiEditContainer.find('div.callcenters-role .frm-custom-dropdown-option').find('div.option.agent-type[data-value="'+oAgent.user_level+'"]').attr('data-value');
                          uiEditContainer.find('input[name="callcenter_id"]').val(sCallcenter).attr('int-value', iCallcenter)
                          uiEditContainer.find('input[name="callcenter_role"]').val(sUserRole).attr('int-value', iUserRole)
                          
                          if(typeof(oAgent.provinces_ids) != 'undefined')
                          {
                              $.each(oAgent.provinces_ids, function(key, province_id) {
                                  uiEditContainer.find('input.chck-box.assign-check[value="'+province_id+'"]').prop('checked' , true)        
                              })        
                          }

                          if(typeof(oAgent.image_file) != 'undefined')
                          {
                              if(oAgent.image_file.length > 0)
                              {
                                     $('div.edit-agent-container').find('div.sphere-camera').replaceWith('<div class="sphere-camera  margin-left-50"><img src="'+ admin.config('url.server.assets_base') +'images/agents/'+oAgent.image_file+'" class="img-circle" width="94px" height="94px"></div>')
                              }        
                          }
                                    

                          //uiEditContainer.find('input[name="callcenter_id"]').attr('int-value')
                          //uiEditContainer.find('input[name="client_img"]').attr('image_name')
                                
                     }
                }
            })

            $('div[modal-id="archive"]').on('click', 'button.confirm-archive', function() {
                var iAgentID = $(this).parents('div[modal-id="archive"]:first').attr('data-agent-id');
                var uiThis = $(this);
                cr8v.agents.show_spinner(uiThis, true);
                var oParams = {
                    "params" : {
                        "type" : 2,
                        "data" : [{
                               "field" : "u.is_archived",
                               "value" : 1  
                        }],
                        "qualifiers" : [{
                                "field" : "u.id",
                                "value" : iAgentID
                         
                        }]
                             
                        
                    }

                }

                var oAjaxFetchAgentsConfig = {
                    "type"   : "POST",
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "data"   : oParams,
                    "url"    : admin.config('url.api.jfc.users') + "/users/update",
                    "success": function (oData) {
                        if(typeof(oData) != 'undefined' && typeof(oData.data[0]) != 'undefined')
                        {
                              var oUpdatedUser = oData.data[0]; 
                              var oAgents = cr8v.get_from_storage('agents');
                                var iFoundIndex;
                                if(typeof(oAgents) != 'undefined')
                                {
                                        $.each(oAgents, function(i, agent) {
                                              if(agent.user_id == oUpdatedUser.user_id)
                                              {
                                                   iFoundIndex = i;
                                                   return false;     
                                              }  
                                        });

                                        cr8v.get_from_storage('agents').splice(iFoundIndex, 1);
                                        cr8v.get_from_storage('agents').unshift(oData.data[0]);
                                        $('section.agent-list[data-agent-id="'+oUpdatedUser.user_id+'"]').remove();
                                        $('div[modal-id="archive"]').removeClass('showed');
                                        cr8v.agents.show_spinner(uiThis, false);
                                }
                        } 
                        
      
                    }
                }

                admin.agents.ajax(oAjaxFetchAgentsConfig);
                cr8v.agents.show_spinner(uiThis, false);
            })

            $('section[header-content="agent_list"]').on('click','div.option.province-select', function() {
                $(this).parents('div.select.provinces-agent-list:first').find('div.frm-custom-dropdown-txt').find('input').attr('int-value', $(this).attr('data-value'));
            })

            $('section[header-content="agent_list"]').on('click','div.option.callcenter-select', function() {
                $(this).parents('div.select:first').find('div.frm-custom-dropdown-txt').find('input').attr('int-value', $(this).attr('data-value'));
            })

            $('div.add-agent-container').on('click','div.option.callcenter-select', function() {
                $(this).parents('div.select:first').find('div.frm-custom-dropdown-txt').find('input').attr('int-value', $(this).attr('data-value'));
            })

            $('section[header-content="agent_list"]').on('keypress', 'input[name="agent_search"]', function (e) {
              if(e.keyCode == 13)
              {
                $('section[header-content="agent_list"]').find('button.search_agent').trigger('click');
              }
            });

            $('section[header-content="agent_list"]').on('click', 'button.search_agent', function() {
                var uiThis = $(this);  
                admin.agents.show_spinner(uiThis, true)
                var sSearchString = ' el["callcenter_id"] != 0';
                var sString = $('section[header-content="agent_list"]').find('input[name="agent_search"]').val().toLowerCase();
                var sStringSearchBy = $('section[header-content="agent_list"]').find('input[name="agent_search_by"]').val();
                var iStringProvince = $('section[header-content="agent_list"]').find('input[name="agent_province"]').attr('int-value')
                var iStringCallcenter = $('section[header-content="agent_list"]').find('input[name="agent_callcenter"]').attr('int-value')

                var oParams = {
                    "params" : {
                        "offset": 0,
                        "limit" : 1000,
                        "type" : 2,
                        "where" : [
                            {
                                "key": "u.sbu_id",
                                "operator" : "=",
                                "value": "1"
                            },
                            {
                                "key": "cu.callcenter_id",
                                "operator" : ">",
                                "value": "0"
                            }
                        ],
                        "custom" : ""
                    }

                }



                if(iConstantUserLevel == 4)
                {
                    oParams.params.where.push({
                        'key' : "cu.callcenter_id",
                        "operator" : "=",
                        "value" : iConstantCallcenterId
                    })
                }
                
                if($('div[content="agent_list"]').find('li[contents="Archived Agent List"]').hasClass('active'))
                {
                    oParams.params.where.push({
                        'key' : "u.is_archived",
                        "operator" : "=",
                        "value" : 1
                    })
                }
                else
                {
                    oParams.params.where.push({
                        'key' : "u.is_archived",
                        "operator" : "=",
                        "value" : 0
                    })

                    //sSearchString += " && el['is_archived'] == 0 ";
                }

                if(sString.length > 0)
                {
                    oParams.params.custom = " AND ( cu.first_name LIKE '%"+sString+"%' || cu.last_name LIKE '%"+sString+"%' || u.username LIKE '%"+sString+"%')";
                }

                if(iStringCallcenter > 0)
                {
                    oParams.params.where.push({
                        'key' : "cu.callcenter_id",
                        "operator" : "=",
                        "value" : iStringCallcenter
                    })
                }

                var oAjaxFetchAgentsConfig = {
                    "type"   : "GET",
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "data"   : oParams,
                    "url"    : admin.config('url.api.jfc.users') + "/users/get",
                    "success": function (oData) {
                        //var oData = JSON.parse(oData);
                        if (typeof( oData.data) != 'undefined') {
                            //if (count(oData.data) > 0) {
                                var oAgents = oData.data;
                                var oFilteredAgents = oAgents.filter(function(el) {
                                    return eval(sSearchString);
                                })
                                //populate search result count
                                var iSearchCount = parseInt(oFilteredAgents.length);
                                $('section.content span.agent_search_result').text(iSearchCount);
                                if(iSearchCount > 0)
                                {//show pagination
                                    $('section.content div.agent_list ul#paginationss').removeClass('hidden');
                                }
                                else
                                {//hide pagination
                                    $('section.content div.agent_list ul#paginationss').addClass('hidden');
                                }
                                try {
                                    $('#paginationss').twbsPagination('destroy');
                                }
                                catch (e) {
                                    // statements to handle any exceptions
                                    //logMyErrors(e); // pass exception object to error handler
                                }
                                admin.agents.render_pagination(oFilteredAgents);
                                oFilteredAgents = oFilteredAgents.filter(function (el, i) {
                                    return i >= 0 && i <= iPagination;
                                });
                                admin.agents.render_agents_list(oFilteredAgents);

                            //}

                        }
                    }
                }

                admin.agents.ajax(oAjaxFetchAgentsConfig);

                setTimeout(function() {
                     admin.agents.show_spinner(uiThis, false)
                }, 1000)
            
            })

            $('section[header-content="agent_list"]').on('click', 'button.add_agents', function() {
                $('div.edit-agent-container').addClass('hidden')
                $('div[content="agent_list"]').find('div.add-agent-container').removeClass('hidden');

                var iTop = $('div[content="agent_list"]').find('div.add-agent-container').offset().top,
                    iFinalTopValue = iTop - 10;

                $('html,body').animate({
                    scrollTop: iFinalTopValue
                }, 20);
            })

            var oValidationConfig = {
                        'data_validation'    : 'datavalid',
                        'input_label'        : 'labelinput',
                        'min_length'         : 0,
                        'max_length'         : 75,
                        'class'              : 'error',
                        'add_remove_class'   : true,
                        'Username': {
                            'min_length': 4,
                            'max_length': 32
                        },
                        'Password': {
                            'min_length': 4,
                            'max_length': 32
                        },
                        'Confirm Password': {
                            'min_length': 4,
                            'max_length': 32
                        },
                        'Confirm Password':
                        {
                            'compare_input': 'Password'
                        },
                        'onValidationError'  : function (arrMessages) {
                            //alert(JSON.stringify(arrMessages))
                            $('section.notification').find('div.error-msg').removeClass('hidden')
                        },
                        'onValidationSuccess': function () {
                                  var iError = 0;
                                  var sMessage = '';

                                  if($('div.add-agent-container').find('table.provinces-choices').find('input:checked').length == 0)
                                  {     
                                        sMessage = "You must assign province(s) to the user."; 
                                        iError++;
                                  }



                                  if(iError > 0)
                                  {
                                      $('div.add-agent-container').find('span.error-message-text').text(sMessage)
                                      $('div.add-agent-container').find('section.notification').find('div.error-msg').removeClass('hidden')
                                  }
                                  else
                                  {
                                      var userName = $('form#add_agent_form').find('input[name="username"]').val();
                                      var bCheckDuplicateUsername = admin.agents.check_duplicate_username(userName);

                                      if(bCheckDuplicateUsername)
                                      {
                                        $('div.add-agent-container').find('section.notification').find('div.error-msg').removeClass('hidden');
                                        $('div.add-agent-container').find('span.error-message-text').text('Username already existing.');
                                        $('form#add_agent_form').find('input[name="username"]').addClass('error');
                                      }
                                      else
                                      {
                                        $('form#add_agent_form').find('input[name="username"]').removeClass('error');
                                        $('div.add-agent-container').find('section.notification').find('div.error-msg').addClass('hidden')
                                        $('div.add-agent-container').find('span.error-message-text').text('Please fill up all required fields.')
                                        var uiButton = $('div.add-agent-container button.add_agent_button');
                                        admin.stores.show_spinner(uiButton, true);
                                        admin.agents.assemble_agent_information();    
                                      }
                                  }


                             
                       }
            }


            
            admin.validate_form($('form#add_agent_form'), oValidationConfig);

            var oEditValidationConfig = {
                'data_validation'    : 'datavalid',
                'input_label'        : 'labelinput',
                'min_length'         : 0,
                'max_length'         : 75,
                'class'              : 'error',
                'add_remove_class'   : true,
                'Username': {
                    'min_length': 4,
                    'max_length': 32
                },
                'Password': {
                    'min_length': 4,
                    'max_length': 32
                },
                'Confirm Password': {
                    'min_length': 4,
                    'max_length': 32
                },
                'Confirm Password':
                {
                    'compare_input': 'Password'
                },
                'onValidationError'  : function (arrMessages) {
                    //alert(JSON.stringify(arrMessages))

                    $('section.notification').find('div.error-msg').removeClass('hidden')
                },
                'onValidationSuccess': function () {
                    var iError = 0;
                    var sMessage = '';
                    if($('div.edit-agent-container').find('input[name="callcenter_id"]').val().length == 0)
                    {
                        sMessage = "You must assign a callcenter to the user.";
                        iError++;
                    }

                    if($('div.edit-agent-container').find('table.provinces-choices-edit').find('input:checked').length == 0)
                    {
                        sMessage = "You must assign province(s) to the user.";
                        iError++;
                    }

                    if(iError > 0)
                    {
                        $('div.edit-agent-container').find('span.error-message-text').text(sMessage)
                        $('div.edit-agent-container').find('section.notification').find('div.error-msg').removeClass('hidden')
                    }
                    else
                    {
                        var userName = $('form#edit_agent_form').find('input[name="username"]').val();
                        var userID = $('form#edit_agent_form').find('input[name="user_id"]').val();
                        var bCheckDuplicateUsername = admin.agents.check_duplicate_username(userName, true, userID);

                        if(bCheckDuplicateUsername)
                        {
                          $('div.edit-agent-container').find('section.notification').find('div.error-msg').removeClass('hidden');
                          $('div.edit-agent-container').find('span.error-message-text').text('Username already existing.');
                          $('form#edit_agent_form').find('input[name="username"]').addClass('error');
                        }
                        else
                        {
                          $('form#edit_agent_form').find('input[name="username"]').removeClass('error');
                          $('div.edit-agent-container').find('section.notification').find('div.error-msg').addClass('hidden')
                          $('div.edit-agent-container').find('span.error-message-text').text('Please fill up all required fields.')
                          
                          var uiButton = $('div.edit-agent-container button.edit_agent_button');
                          admin.stores.show_spinner(uiButton, true);
                          admin.agents.assemble_edit_agent_information();
                        }
                    }

                }
            }

            admin.validate_form($('form#edit_agent_form'), oEditValidationConfig);
            
            $('div.add-agent-container').on('click', 'button.add_agent_button', function(e) {
                  e.preventDefault();
                  $('div.edit-agent-container').addClass('hidden')
                  $('div.add-agent-container').find('section.notification').find('div.error-msg').addClass('hidden')
                  $('div.add-agent-container').find('span.error-message-text').text('Please fill up all required fields.')

                  $('form#add_agent_form').submit();

                    var iTop = $('div.add-agent-container').offset().top,
                        iFinalTopValue = iTop - 10;

                    $('html,body').animate({
                        scrollTop: iFinalTopValue
                    }, 100);
            })

            $('form#add_agent_form').on('submit', function(e) {
                  e.preventDefault();



            })

            $('div.edit-agent-container').on('click', 'button.edit_agent_button', function(e) {
                e.preventDefault();

                $('div.edit-agent-container').find('section.notification').find('div.error-msg').addClass('hidden')
                $('div.edit-agent-container').find('span.error-message-text').text('Please fill up all required fields.')

                var iTop = $('div.edit-agent-container').offset().top,
                    iFinalTopValue = iTop - 10;

                $('html,body').animate({
                    scrollTop: iFinalTopValue
                }, 100);

                $('form#edit_agent_form').submit();
            })

            $('form#edit_agent_form').on('submit', function(e) {
                e.preventDefault();
            })

            $('div.add-agent-container').on('click', 'div.sphere-camera',  function(e) {
                e.preventDefault();
                 $('div.add-agent-container').find('input[name="client_img"]').trigger('click');
            });

             $('div.edit-agent-container').on('click', 'div.sphere-camera',  function(e) {
                e.preventDefault();
                $('div.edit-agent-container').find('input[name="client_img"]').trigger('click');
            });

            $('div.add-agent-container').on('click', 'button.close_add_agent',  function(e) {
                $('div.add-agent-container').addClass('hidden');
            });

            $('div.edit-agent-container').on('click', 'button.close_add_agent',  function(e) {
                $('div.edit-agent-container').addClass('hidden');
            });
            
            $('div.callcenters-choices').on('click', 'div.option.callcenter-select', function() {
                $(this).parents('div.select:first').find('input').attr('int-value', $(this).attr('data-value'))
            })            
            
            $('div.callcenters-role').on('click', 'div.option.agent-type', function() {
                $(this).parents('div.select:first').find('input').attr('int-value', $(this).attr('data-value'))
            })

            $('section[header-content="agent_list"] div.sort-container').on('click', 'strong.sort', function() {
                var uiAgentContainers = $('div.agent-list-container:visible');
                var sSortKey = $(this).attr('sort');
                var iSortDirectionAsc;
                var iSortDirectionDesc;

                $(this).toggleClass('asc desc');

                if($(this).hasClass('asc') == true)
                {
                    iSortDirectionAsc = -1;
                    iSortDirectionDesc = 1;
                    $(this).siblings('img.asc').removeClass('hidden');
                    $(this).siblings('img.desc').addClass('hidden');
                }
                else
                {
                    iSortDirectionAsc = 1;
                    iSortDirectionDesc = -1;
                    $(this).siblings('img.asc').addClass('hidden');
                    $(this).siblings('img.desc').removeClass('hidden');
                }

                if(typeof(uiAgentContainers) != 'undefined')
                {
                    $.each(uiAgentContainers, function(key, container) {
                        $(container).children('section.agent-list:not(.template)').sortDomElements(function(a,b){
                            var akey = $(a).attr(sSortKey);
                            var bkey = $(b).attr(sSortKey);
                            if (akey == bkey) return 0;
                            if (akey < bkey) return iSortDirectionAsc;
                            if (akey > bkey) return iSortDirectionDesc;
                        })
                    })
                }

            })

            var last_image_path = admin.config('url.server.base') + 'assets/images/w-logo.png';
            $('body').on('change', 'input[name="client_img"]', function(){
                var uiThis = $(this);
                var image = $(this)[0];
                var image_path = '';
                var image_name = '';
                var formdata = false;
                var uiLogoContainer = $(this).parents('div.content-container:first').find('.sphere-camera');

                if(window.FormData)
                {
                    formdata = new FormData();
                    formdata.append('client_img', image.files[0]);
                    formdata.append('csrf_ironman_token', $.cookie('csrf_ironman_cookie'));
                    var oSettings = {
                        type: 'POST',
                        url: admin.config('url.server.base') + 'admin/ajax_upload_logo',
                        data: formdata,
                        processData: false,
                        contentType: false,
                        beforeSend: function(){

                        },
                        success: function(sData){
                            var oData = $.parseJSON(sData);
                            image_path = (typeof(oData.data.image_path) != 'undefined') ? oData.data.image_path : last_image_path;
                            image_name = oData.data.image_name;
                            last_image_path = image_path;
                            if(typeof(oData.status) != 'undefined')
                            {
                                uiLogoContainer.attr('data-original-title', oData.message);
                                uiLogoContainer.attr('title', oData.message).tooltip({trigger:'click'});
                                uiLogoContainer.click();
                                setTimeout(function() {
                                    uiLogoContainer.tooltip ('destroy').removeAttr('title data-original-title');
                                }, 3000);
                            }
                        },
                        complete: function(){
                            uiLogoContainer.find('.preloader').remove();
                            if(typeof(image_path) != 'undefined')
                            {
                                var sStyle = '';
                                uiLogoContainer.html('<img src="'+image_path+'" class="img-circle" width="94px" height="94px" >');
                                uiThis.attr('image_name', image_name);
                            }

                        }
                    };

                    $.ajax(oSettings);
                }

            });

//             $('table.provinces-choices').on('click', 'label.chck-lbl', function(e) {
//                     var uiAreaCount = $('table.provinces-choices').parents('div.add-agent-container').find('info.area-count')
//                     var uiSelectedCount = $('table.provinces-choices').find('input:checked').length;
//                     uiAreaCount.text(uiSelectedCount)
//             });

            $('div.sub-nav[content="agent_list"]').on('click', 'li.sub-nav-coordinator-order', function() {
                console.log('awa')
                var uiContent = $(this).attr('contents');
                $('section[header-content="agent_list"]').find('h1.content').text(uiContent);
                
                        var sSearchString = ' el["callcenter_id"] != 0';   
                        var oAgents = cr8v.get_from_storage('agents'); 
                        $('div[content="agent_list"]').find('li.sub-nav-coordinator-order').removeClass('active');
                        $(this).addClass('active')
                        if(typeof(oAgents) != 'undefined')
                        {
                            if( uiContent == 'Archived Agent List')  
                            {
                                
                                sSearchString += ' && el["is_archived"] == 1 ' ;

                                $('div.agent-list-container').find('div.custom-pagination').removeClass('hidden')    
                                $('section#add_new_agent_header').find('button.add_agents').addClass('hidden')
                                $('section#add_new_agent_header').find('button.notify_agents').addClass('hidden')
                            }
                            else
                            {
                                $('div.agent-list-container').find('div.custom-pagination').removeClass('hidden')   
                                $('section#add_new_agent_header').find('button.add_agents').removeClass('hidden')
                                $('section#add_new_agent_header').find('button.notify_agents').removeClass('hidden')
                                sSearchString += ' && el["is_archived"] == 0 ' ;
                            }

                            var oFilteredAgents = oAgents.filter(function(el) {
                                return eval(sSearchString);
                            })

                            admin.agents.render_pagination(oFilteredAgents);

                            oFilteredAgents = oFilteredAgents.filter(function(el, i) {
                                    return i >= 0 && i <= iPagination;
                            }) 

                            admin.agents.render_agents_list(oFilteredAgents);
                            $('div.agent-list-container').find('li.page[page-start="0"][page-end="'+iPagination+'"]').addClass('page-active');
                        }   
               
            });

            $('div.agent-list-container').on('click', 'li.page-prev', function() {
                    
                    var uiActive = $(this).siblings('li.page.page-active');
                     if(typeof(uiActive.prev('li.page')) != 'undefined')
                     {
                         uiActive.prev('li.page').trigger('click')
                     }
            })

             $('div.agent-list-container').on('click', 'li.page-next', function() {
                    var uiActive = $(this).siblings('li.page.page-active');
                     if(typeof(uiActive.next('li.page')) != 'undefined')
                     {
                         uiActive.next('li.page').trigger('click')
                     }
            })

            $('div.agent-list-container').on('click', 'li.page', function () {
                console.log('lipage')
                var uiThis = $(this);

                admin.agents.show_spinner(uiThis , true)

                var sString = $('section[header-content="agent_list"]').find('input[name="agent_search"]').val().toLowerCase();
                var sStringSearchBy = $('section[header-content="agent_list"]').find('input[name="agent_search_by"]').val();
                var iStringProvince = $('section[header-content="agent_list"]').find('input[name="agent_province"]').attr('int-value')
                var iStringCallcenter = $('section[header-content="agent_list"]').find('input[name="agent_callcenter"]').attr('int-value')

                if (uiThis.hasClass('active') == false) {

                    uiThis.siblings('li.page').removeClass('page-active');

                    var iStart = uiThis.attr('page-start');
                    var iEnd = uiThis.attr('page-end');
                    $('div.agent-list-container').find('li.page[page-start="' + iStart + '"][page-end="' + iEnd + '"]').addClass('page-active');

                    var oParams = {
                        "params": {
                            "offset": iStart,
                            "limit" : 25,
                            "type"  : 2,
                            "where" : [
                                {
                                    "key"     : "u.sbu_id",
                                    "operator": "=",
                                    "value"   : "1"
                                },
                                {
                                    "key"     : "cu.callcenter_id",
                                    "operator": ">",
                                    "value"   : "0"
                                }
                            ],
                            "custom": ""
                        }

                    }

                    if (iConstantUserLevel == 4) {
                        oParams.params.where.push({
                            'key'     : "cu.callcenter_id",
                            "operator": "=",
                            "value"   : iConstantCallcenterId
                        })
                    }

                    if ($('div[content="agent_list"]').find('li[contents="Archived Agent List"]').hasClass('active')) {
                        oParams.params.where.push({
                            'key'     : "u.is_archived",
                            "operator": "=",
                            "value"   : 1
                        })
                    }
                    else {
                        oParams.params.where.push({
                            'key'     : "u.is_archived",
                            "operator": "=",
                            "value"   : 0
                        })
                    }

                    if (sString.length > 0) {
                        oParams.params.custom = " AND ( cu.first_name LIKE '%" + sString + "%' || cu.last_name LIKE '%" + sString + "%' || u.username LIKE '%" + sString + "%')";

                    }

                    if (iStringCallcenter > 0) {
                        oParams.params.where.push({
                            'key'     : "cu.callcenter_id",
                            "operator": "=",
                            "value"   : iStringCallcenter
                        })
                    }

                    var oAjaxFetchAgentsConfig = {
                        "type"   : "GET",
                        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                        "data"   : oParams,
                        "url"    : admin.config('url.api.jfc.users') + "/users/get",
                        "success": function (oData) {
                            //var oData = JSON.parse(oData);
                            if (typeof( oData.data) != 'undefined') {
                                if (count(oData.data) > 0) {
                                    var oAgents = oData.data;
                                    admin.agents.render_agents_list(oAgents);
                                    admin.agents.show_spinner(uiThis , false)
                                }

                            }
                        }
                    }

                    admin.agents.ajax(oAjaxFetchAgentsConfig);

                }

            });

            $('div[modal-id]').on('click', '.close-me', function() {
                $(this).parents('div[modal-id]').removeClass('showed');    
            })    
            
        },

        'assemble_edit_agent_information' : function() {
            var uiForm =  $('form#edit_agent_form');
            var aAssignedProvinces = [];
            var iAgentID = uiForm.find('input[name="user_id"]').val();            
            $('div.edit-agent-container').find('table.provinces-choices-edit').find('input:checked').each(function(key, element) {
                aAssignedProvinces.push($(element).val());
            }) 

            var oData = {
                "user_id" : uiForm.find('input[name="user_id"]').val(),
                "provinces" : aAssignedProvinces,
                "updated_by" : iConstantUserId,
                "data" : [
                {
                            field : "u.username",
                            value : uiForm.find('input[name="username"]').val()
                },
                
                {
                            field : "cu.first_name",
                            value : uiForm.find('input[name="first_name"]').val()
                },
                {
                            field : "cu.last_name",
                            value : uiForm.find('input[name="last_name"]').val()
                },
                {
                            field : "cu.email_address",
                            value : uiForm.find('input[name="email_address"]').val()
                },
                {
                            field : "cu.callcenter_id",
                            value : uiForm.find('input[name="callcenter_id"]').attr('int-value')
                },
                {
                            field : "cu.user_level",
                            value : uiForm.find('input[name="callcenter_role"]').attr('int-value')
                },
                {
                            field : "u.is_ip_whitelist_excluded",
                            value : $('div.edit-agent-container').find('input#ip-whitelist-exclude-edit:checked').length
                },
                {
                            field : "u.is_blacklisted",
                            value : $('div.edit-agent-container').find('input#is-blacklisted:checked').length
                }
               ],
                
                "qualifiers" : [{
                            field : 'u.id',
                            value : uiForm.find('input[name="user_id"]').val()
                }]
                           
                            
                
               
            } 

            if(uiForm.find('input[name="password"]').val().length > 0){
                
                oData.data.push({
                        field : "u.password",
                        value : uiForm.find('input[name="password"]').val()
                })
            }  

            if(typeof($('div.edit-agent-container').find('input[name="client_img"]').attr('image_name')) != 'undefined')
            {
               oData.data.push({
                        field : "u.image_file",
                        value : $('div.edit-agent-container').find('input[name="client_img"]').attr('image_name')
                })         
            }

             var oAddAgentAjaxConfig = {
                "type"   : "POST",
                "data"   : oData,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : admin.config('url.api.jfc.users') + "/users/add_user",
                "success": function (oData) {

                    if(typeof(oData.status) != 'undefined')
                    {
                        var iFoundIndex;
                        if(oData.status)
                        {
                            $('div.edit-agent-container').find('section.notification').find('div.success-msg').removeClass('hidden')
                            $('div.edit-agent-container').find('section.notification').find('div.error-msg').addClass('hidden')
                            $('section.agent-list[data-agent-id="'+iAgentID+'"]').remove();
                            var oAgents = cr8v.get_from_storage('agents');

                            $.each(oAgents, function(i, agent) {
                                if(agent.user_id == oData.data[0].user_id)
                                {
                                    iFoundIndex = i;
                                    return false;
                                }
                            });

                            cr8v.get_from_storage('agents').splice(iFoundIndex, 1);

                            if(typeof(oData.data[0]) != 'undefined')
                            {
                                cr8v.get_from_storage('agents').unshift(oData.data[0]);    
                            }

                            admin.agents.render_agents_list(oData.data, true);

                            $('form#edit_agent_form')[0].reset();

                            $('div.edit-agent-container').find('div.sphere-camera').replaceWith('<div class="sphere-camera  margin-left-50"><i class="fa fa-camera fa-2x  gray-color  margin-top-20"></i><p class="gray-color font-12"><strong>Upload Image</strong></p></div>')
                            $('div.edit-agent-container').find('input[name="client_img"]').removeAttr('image_name');
                            $('div.edit-agent-container').find('input.chck-box').prop('checked', false);
                        }
                        else
                        {
                            $('div.edit-agent-container').find('section.notification').find('div.error-msg').removeClass('hidden')
                            $('div.edit-agent-container').find('section.notification').find('div.success-msg').addClass('hidden')
                        }

                        setTimeout(function() {
                            $('div.edit-agent-container').find('section.notification').find('div.error-msg').addClass('hidden')
                            $('div.edit-agent-container').find('section.notification').find('div.success-msg').addClass('hidden')
                            $('div.edit-agent-container').addClass('hidden');

                            var uiButton = $('div.edit-agent-container button.edit_agent_button');
                            admin.stores.show_spinner(uiButton, false);
                        }, 2000)

                       
                    }
                }
            }  

            admin.agents.ajax(oAddAgentAjaxConfig)         
        },

        'assemble_agent_information' : function() {
            var uiForm =  $('form#add_agent_form');
            var aAssignedProvinces = [];

            $('div.add-agent-container').find('table.provinces-choices').find('input:checked').each(function(key, element) {
                aAssignedProvinces.push($(element).val());
            })    

            var oData = {
                "params" : {
                            "username" : uiForm.find('input[name="username"]').val(),
                            "password" : uiForm.find('input[name="password"]').val(),
                            "password_confirmation" : uiForm.find('input[name="confirm_password"]').val(),
                            "first_name" : uiForm.find('input[name="first_name"]').val(),
                            "middle_name" : uiForm.find('input[name="middle_name"]').val(),
                            "last_name" : uiForm.find('input[name="last_name"]').val(),
                            "email_address" : uiForm.find('input[name="email_address"]').val(),
                            "callcenter_id" : uiForm.find('input[name="callcenter_id"]').attr('int-value'),
                            "image_file" : $('input[name="client_img"]').attr('image_name'),
                            "user_level" : uiForm.find('input[name="callcenter_role"]').attr('int-value'),
                            "type" : 2,
                            "sbu_id" : 1, //JOllibee
                            "provinces" : aAssignedProvinces,
                            "is_ip_whitelist_excluded" : $('div.add-agent-container').find('input#ip-whitelist-exclude:checked').length
                },
                "user_id" : iConstantUserId
            }   

             var oAddAgentAjaxConfig = {
                "type"   : "POST",
                "data"   : oData,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : admin.config('url.api.jfc.users') + "/users/add",
                "success": function (oData) {

                    admin.stores.show_spinner($('button.add_agent_button'), false);
                    if(typeof(oData.status) != 'undefined')
                    {
                        if(oData.status)
                        {
                            $('div.add-agent-container').find('section.notification').find('div.success-msg').removeClass('hidden')
                            $('div.add-agent-container').find('section.notification').find('div.error-msg').addClass('hidden')
                            admin.agents.render_agents_list(oData.data, true);
                            if(typeof(oData.data[0]) != 'undefined')
                            {
                                cr8v.get_from_storage('agents').unshift(oData.data[0]);    
                            }

                            $('form#add_agent_form')[0].reset();

                            $('div.add-agent-container').find('div.sphere-camera').replaceWith('<div class="sphere-camera  margin-left-50"><i class="fa fa-camera fa-2x  gray-color  margin-top-20"></i><p class="gray-color font-12"><strong>Upload Image</strong></p></div>')
                            $('div.add-agent-container').find('input[name="client_img"]').removeAttr('image_name');
                            $('div.add-agent-container').find('input.chck-box').prop('checked', false);
                        }
                        else
                        {
                            $('div.add-agent-container').find('section.notification').find('div.error-msg').removeClass('hidden')
                            $('div.add-agent-container').find('section.notification').find('div.success-msg').addClass('hidden')
                        }

                        setTimeout(function() {
                            $('div.add-agent-container').find('section.notification').find('div.error-msg').addClass('hidden')
                            $('div.add-agent-container').find('section.notification').find('div.success-msg').addClass('hidden')
                            $('div.add-agent-container').addClass('hidden');
                        }, 2000)

                    }
                }
            }  

            admin.agents.ajax(oAddAgentAjaxConfig)
        },

        'provinces' : function() {
            var oProvincesAjaxConfig = {
                "type"   : "Get",
                "data"   : {
                    "all" : 1,
                    "call_center_user_id" : 3305//iConstantCallcenterUserId
                },
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : admin.config('url.api.jfc.gis') + "/gis/province_address",
                "success": function (data) {
                      if(typeof(data) != 'undefined')
                      {
                          if(typeof(data.data.provinces) != 'undefined')
                          {
                              cr8v.add_to_storage('provinces', data.data.provinces);
                              admin.agents.render_provinces();
                          }
                      }
                }
            }  

            admin.agents.ajax(oProvincesAjaxConfig)
        },



        'render_provinces' : function() {
            var oProvinces = cr8v.get_from_storage('provinces');
            if(typeof(oProvinces) != 'undefined')
            {
                var uiProvinceSelect = $('div.select.provinces-agent-list').find('div.frm-custom-dropdown-option');
                var uiProvinceStoreSelect = $('div.select.provinces-store-list').find('div.frm-custom-dropdown-option');
                var uiProvinceDstViewStoreSelect = $('div.select-filter-provinces').find('div.frm-custom-dropdown-option');
                var uiTableProvinceSelect = $('table.provinces-choices').find('tbody').html("");;
                var uiEditTableProvinceSelect = $('table.provinces-choices-edit').find('tbody').html("");
                var uiProvinceContactReportSelect = $('div.select.provinces-contact-num-report').find('div.frm-custom-dropdown-option');
                var uiProvinceRepeatingCustomerReportSelect = $('div.select.provinces-repeating-customer-report').find('div.frm-custom-dropdown-option');
                var uiProvincMessagingSelect = $('div.select.messaging-province-select').find('div.frm-custom-dropdown-option');
                var uiProvinceHourlySelect = $('div.select.hourly_province_selection').find('div.frm-custom-dropdown-option');
                var uiProvinceSalesSummarySelect = $('div.select.sales_summary_province_selection').find('div.frm-custom-dropdown-option');
                var uiProvinceProdAvailSelect = $('div.select.pu_province_selection').find('div.frm-custom-dropdown-option');
                var uiProvinceItemSalesReportSelect = $('div.select.province-selection-product-sales-report').find('div.frm-custom-dropdown-option');
                var uiProvinceCategorySalesReportSelect = $('div.select.province-selection-category-sales-report').find('div.frm-custom-dropdown-option');
                var uiOrderReportsProvince = $('div.province-select-order-reports').find('div.frm-custom-dropdown-option');
                var uiDstReportProvince = $('div.custom-province-filter').find('div.frm-custom-dropdown-option');
                var sProvincesHtml = '';
                var sTableProvinces = '';
                var sEditTableProvinces = '';
                var sDstProvinces = '';
                var sHourlyProvinces = '';
                var sItemSalesReport = '';
                var sCategorySalesReport = '';
                var sDstReportsProvince = '';
                var sOrderReports = '<div class="option province-select" data-value="0">Show All Provinces</div>';
                $.each(oProvinces, function(key, province) {
                    sProvincesHtml += '<div class="option province-select" data-value="'+province.id+'">'+province.name+'</div>';
                    sEditTableProvinces += '<tr><td class="padding-left-5  padding-right-5  first-td"><div class="form-group  f-right  no-margin-all padding-top-5"><input class="chck-box assign-check" id="check'+key+''+key+''+key+''+key+'" type="checkbox" value="'+province.id+'"><label class="chck-lbl" for="check'+key+''+key+''+key+''+key+'"></label> </div> <p class="f-left font-12"><strong>'+province.name+'</strong></p><div class="clear"></div></td></tr>'
                    sTableProvinces += '<tr><td class="padding-left-5  padding-right-5  first-td"><div class="form-group  f-right  no-margin-all padding-top-5"><input class="chck-box assign-check" id="check'+key+'" type="checkbox" value="'+province.id+'"><label class="chck-lbl" for="check'+key+'"></label> </div> <p class="f-left font-12"><strong>'+province.name+'</strong></p><div class="clear"></div></td></tr>'
                    sDstProvinces += '<div class="option custom-filter" data-value="'+province.id+'">'+province.name+'</div>';
                    sHourlyProvinces += '<div class="option" province_select int-value="'+province.id+'">'+province.name+'</div>';
                    sItemSalesReport += '<div class="option" int-value="'+province.id+'">'+province.name+'</div>';
                    sCategorySalesReport += '<div class="option" int-value="'+province.id+'">'+province.name+'</div>';
                    sOrderReports += '<div class="option province-select" data-value="'+province.id+'">'+province.name+'</div>';
                    sDstReportsProvince += '<div class="option custom-filter-option" data-value="'+province.id+'">'+province.name+'</div>';
                });

                uiDstReportProvince.append(sDstReportsProvince);
                uiProvinceDstViewStoreSelect.append(sDstProvinces);
                uiEditTableProvinceSelect.append(sEditTableProvinces)
                uiProvinceSelect.append(sProvincesHtml);
                uiProvinceStoreSelect.append(sProvincesHtml);
                uiTableProvinceSelect.append(sTableProvinces)
                uiProvinceContactReportSelect.append(sProvincesHtml);
                uiProvinceRepeatingCustomerReportSelect.append(sProvincesHtml);
                uiProvincMessagingSelect.append(sProvincesHtml);
                uiProvinceHourlySelect.append(sHourlyProvinces);
                uiProvinceSalesSummarySelect.append(sHourlyProvinces);
                uiProvinceItemSalesReportSelect.append(sItemSalesReport);
                uiProvinceCategorySalesReportSelect.append(sCategorySalesReport);
                uiProvinceProdAvailSelect.append(sHourlyProvinces);
                uiOrderReportsProvince.append(sOrderReports);
                $('table.provinces-choices').filterTable({
                     inputSelector: '#add_agent_province'
                });

                $('table.provinces-choices-edit').filterTable({
                     inputSelector: '#edit_agent_province'
                });
            }
        },

        'render_callcenters' : function() {
            var oCallcenters = cr8v.get_from_storage('callcenters');
            if(typeof(oCallcenters) != 'undefined')
            {
                var uiCallcenterSelect = $('div.select.callcenters').find('div.frm-custom-dropdown-option');
                var uiCallcenterSelectAdd = $('div.select.callcenters-choices').find('div.frm-custom-dropdown-option');
                var uiCallcenterFilter = $('#dst_per_day_view_data_content').find('.select-filter-call-centers .frm-custom-dropdown-option');
                var uiCallcenterSelectViewPerDay = $('#dst_for_store').find('div.select.select-filter-call-centers').find('div.frm-custom-dropdown-option');
                var uiCallcenterSelectPerStore = $('#dst_for_store').find('div.select.select-filter-call-centers').find('div.frm-custom-dropdown-option');
                var uiDstPerdayCallcenter = $('#dst_per_day_view_data_content div.select.callcenters-choices').find('div.frm-custom-dropdown-option')
                var uiCallcenterHourlySelect = $('div.select.hourly_callcenter_selection').find('div.frm-custom-dropdown-option');
                var uiCallcentersalesSummarySelect = $('div.select.sales_summary_callcenter_selection').find('div.frm-custom-dropdown-option');
                var uiCallcentersItemSalesReportSelect = $('div.select.callcenter-selection-product-sales-report').find('div.frm-custom-dropdown-option');
                var uiCallcentersCategorySalesReportSelect = $('div.select.callcenter-selection-category-sales-report').find('div.frm-custom-dropdown-option');
                var uiCallcentersDstReportSelect = $('div.select.custom-callcenter-filter').find('div.frm-custom-dropdown-option');
                var sCallcentersHtml = '';
                var sCallcentersAddHtml = '';
                var sCallCenterFilter = '';
                var sHourlyCallCenter = '';
                var sItemSalesReport = '';
                var sCategorySalesReport = '';
                var sDstReports = '';

                $.each(oCallcenters, function(key, callcenter) {
                    sCallcentersHtml += '<div class="option callcenter-select" data-value="'+callcenter.id+'">'+callcenter.name+'</div>';
                    sCallcentersAddHtml += '<div class="option callcenter-select" data-value="'+callcenter.id+'">'+callcenter.name+'</div>';
                    sCallCenterFilter += '<div class="option callcenter-filter custom-filter" data-value="'+callcenter.id+'">'+callcenter.name+'</div>';
                    sHourlyCallCenter += '<div class="option callcenter_select" int-value="'+callcenter.id+'">'+callcenter.name+'</div>';
                    sItemSalesReport += '<div class="option callcenter_select" int-value="'+callcenter.id+'">'+callcenter.name+'</div>';
                    sCategorySalesReport += '<div class="option callcenter_select" int-value="'+callcenter.id+'">'+callcenter.name+'</div>';
                    sDstReports += '<div class="option custom-filter-option" data-value="'+callcenter.id+'">'+callcenter.name+'</div>';
                })

                uiCallcentersDstReportSelect.append(sDstReports);
                uiCallcenterSelectAdd.append(sCallcentersAddHtml)
                uiCallcenterSelect.append(sCallcentersHtml);
                uiCallcenterFilter.append(sCallCenterFilter);
                uiCallcenterSelectPerStore.append(sCallCenterFilter);
                uiDstPerdayCallcenter.append(sCallCenterFilter)
                uiCallcenterHourlySelect.append(sHourlyCallCenter)
                uiCallcentersalesSummarySelect.append(sHourlyCallCenter)
                //uiCallcenterSelectViewPerDay.append(sCallCenterFilter)
                uiCallcentersItemSalesReportSelect.append(sItemSalesReport)
                uiCallcentersCategorySalesReportSelect.append(sCategorySalesReport)
            }
        },
 
        'ajax': function (oAjaxConfig) {
            if (typeof(oAjaxConfig) != 'undefined') {
                admin.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        'render_pagination' : function(oAgents) {

            var uiAgentsContainer = $('div.agent-list-container');
            var iPageCount = oAgents.length / iPagination;
            var uiPaginationContainer = uiAgentsContainer.find('ul.paganationss.content-container');
            var uiPaginationContainerHidden = uiAgentsContainer.find('ul.paganations-hidden.content-container');
            var sPaginationHtmlhidden = '<li class="page-prev"><i class="fa fa-angle-left"></i></li>';
            var sPaginationHtml = '<li class="page-prev"><i class="fa fa-angle-left"></i></li>';
            iPageCount += 1;

            if (iPageCount > 0) {
                for (i = 0; i < parseInt(iPageCount); i++) {
                    var iPage = i + 1;
                    var iStart, iEnd;
                    if (i == 0) {
                        iStart = 0;
                        iEnd = iPagination;
                    }
                    else {
                        iStart = i * iPagination;
                        iEnd = ((i + 1) * iPagination);
                    }


                    sPaginationHtmlhidden += '<li class="page" page-start="' + iStart + '" page-end="' + iEnd + '" page-end="'+iPagination+'" data-no="' + iPage + '">' + iPage + '</li>';
                    sPaginationHtml += '<li class="pages" page-start="' + iStart + '" page-end="' + iEnd + '">' + iPage + '</li>';
                }
            }
            else {
                sPaginationHtml += '<li class="page page-active" page-start="0" page-end="'+iPagination+'" data-no="1">1</li>';
            }

            uiPaginationContainer.html('');
            uiPaginationContainer.append(sPaginationHtml);
            uiPaginationContainerHidden.html('');
            uiPaginationContainerHidden.append(sPaginationHtmlhidden)

            $('#paginationss').twbsPagination({
                totalPages  : iPageCount,
                visiblePages: 15,
                onPageClick : function (event, page) {
                    $('li.page[data-no="' + page + '"').trigger('click');
                }
            });
        },

        'fetch_agents' : function(oData) {
          
                var oParams = oData;

                var oAjaxFetchAgentsConfig = {
                    "type"   : "GET",
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "data"   : oParams,
                    "url"    : admin.config('url.api.jfc.users') + "/users/get",
                    "success": function (oData) {
                        //var oData = JSON.parse(oData);
                        if(typeof( oData.data) != 'undefined')
                        { 
                           if (count(oData.data) > 0) 
                           {
                           var iOffset = oData.offset;
                           
                           var oParams = {
                                "params" : {
                                    "offset": iOffset,        
                                    "limit" : 1000,
                                    "type" : 2,
                                    "where" : [
                                        /*{
                                            "key": "cu.user_level",
                                            "operator" : "=",
                                            "value": "1"
                                        },*/
                                        {
                                            "key": "u.sbu_id",
                                            "operator" : "=",
                                            "value": "1"
                                        },
                                         {
                                            "key": "cu.callcenter_id",
                                            "operator" : ">",
                                            "value": "0"
                                        }
                                    ]
                                }

                            };
                            
                            for (var i in oData.data) {
                                 var item = oData.data[i];
                                 arrAgents.push({
                                             "callcenter_id": item.callcenter_id,
                                             "callcenter_name": item.callcenter_name,
                                             "date_created": item.date_created,
                                             "email_address": item.email_address,
                                             "first_name": item.first_name,
                                             "image_file": item.image_file,
                                             "is_archived": item.is_archived,
                                             "last_name": item.last_name,
                                             "province_id": item.province_id,
                                             "provinces": item.provinces,
                                             "provinces_ids": item.provinces_ids,
                                             "sbu_id": item.sbu_id,
                                             "status": item.status,
                                             "type": item.type,
                                             "user_id": item.user_id,
                                             "user_level": item.user_level,
                                             "username": item.username,
                                             "is_ip_whitelist_excluded" : item.is_ip_whitelist_excluded,
                                             "is_blacklisted" : item.is_blacklisted
                                              });
                                if(item.is_ip_whitelist_excluded == 1)
                                {
                                    arrIpWhitelistExcludedAgents.push({
                                        "username" : item.username,
                                        "user_id" : item.user_id
                                    })
                                }




                            }
                            admin.agents.fetch_agents(oParams);
                            //cr8v.add_to_storage('agents', oData.data);
                           }
                           else {
                               cr8v.add_to_storage('agents', arrAgents);

                               cr8v.add_to_storage('ip_whitelist_excluded_agents', arrIpWhitelistExcludedAgents);
                               //console.log('finished get agent');
                               var oAgents = cr8v.get_from_storage('agents');

                               oAgents = oAgents.filter(function(el) {
                                   return el['callcenter_id'] != 0 && el['is_archived'] == 0;
                               })

                               if(iConstantUserLevel == 4)
                               {
                                   oAgents = oAgents.filter(function(el) {
                                        return el['callcenter_id'] == iConstantCallcenterId
                                   })
                               }

                               //admin.agents.render_pagination(oAgents);

                               oAgents = oAgents.filter(function (el, i) {
                                   return i >= 0 && i <= iPagination && el['is_archived'] == 0;
                               });

                               //admin.agents.render_agents_list(oAgents);

                               //$('div.agent-list-container').find('li.page:first').addClass('page-active');
                           }

                        }

                    }
                }

            admin.agents.ajax(oAjaxFetchAgentsConfig);
                
               

        },

        'fetch_callcenters' : function() {
             var oCallcenterParams = {
                    "params" : {
                        "limit" : 99999999
                    }
                }
                
                var oAjaxCallcenterConfig = {
                    "type"   : "GET",
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "data"   : oCallcenterParams,
                    "url"    : admin.config('url.api.jfc.users') + "/users/call",
                    "success": function (oData) {
                        
                         cr8v.add_to_storage('callcenters', oData.data);
                         admin.agents.render_callcenters();

                        //for global announcement dropdown of callcenters
                        var uiCCenterMessagingSelect = $('div.select.messaging-callcenter-select').find('div.frm-custom-dropdown-option');
                        var sCallcenterHtml = '';
                        $.each(oData.data, function(key, value) {
                            sCallcenterHtml += '<div class="option callcenter-select" data-value="'+value.id+'">'+value.name+'</div>';
                        })

                        uiCCenterMessagingSelect.append(sCallcenterHtml)
                    }
                }

                admin.agents.ajax(oAjaxCallcenterConfig);
        },

        'show_spinner': function (uiElem, bShow) {
            if (bShow == true) {
                if(uiElem.find('i.fa-spinner').length > 0)
                {
                    uiElem.find('i.fa-spinner').removeClass('hidden');
                }
                else
                {
                    uiElem.append('<i class="fa fa-spinner fa-pulse hidden"></i>').removeClass('hidden');
                }

                uiElem.prop('disabled', true);
            }
            else {
                if(uiElem.find('i.fa-spinner').length > 0)
                {
                    uiElem.find('i.fa-spinner').addClass('hidden');
                }
                uiElem.prop('disabled', false);
            }
        },

        'render_agents_list' : function(oAgents, bAdd) {
            if(typeof(oAgents) != 'undefined')
            {
                  
                var uiAgents = [];
                var uiAgentsContainer = $('div.agent-list-container');

                var oStoredAgents = cr8v.get_from_storage('agents');

                $.each(oAgents, function(key, agent) {
                    var uiAgentTemplate = $('section.agent-list.template').clone().removeClass('template');
                    uiAgentTemplate = admin.agents.manipulate_agent_template(uiAgentTemplate, agent);
                    uiAgents.push(uiAgentTemplate);
                })

                if(typeof(bAdd) == 'undefined')
                {
                    uiAgentsContainer.find('section.agent-list').remove();
                    uiAgentsContainer.append(uiAgents);
                }
                else
                {
                    uiAgentsContainer.find('div.edit-agent-container').after(uiAgents);
                }

                //offline online
                if($('input[name="agent_status"]').val() == 'Show Offline Agents')
                {
                    uiAgentsContainer.find('.red-color.status').each(function(){
                        $(this).parents('section.agent-list').removeClass('hidden')
                    })

                    uiAgentsContainer.find('.online.status').each(function(){
                        $(this).parents('section.agent-list').addClass('hidden')
                    })
                }
                else if($('input[name="agent_status"]').val() == 'Show Online Agents')
                {
                    uiAgentsContainer.find('.red-color.status').each(function(){
                        $(this).parents('section.agent-list').addClass('hidden')
                    })

                    uiAgentsContainer.find('.online.status').each(function(){
                        $(this).parents('section.agent-list').removeClass('hidden')
                    })
                }
                else
                {
                    uiAgentsContainer.find('.red-color.status').each(function(){
                        $(this).parents('section.agent-list').removeClass('hidden')
                    })

                    uiAgentsContainer.find('.online.status').each(function(){
                        $(this).parents('section.agent-list').removeClass('hidden')
                    })
                }

                var iSearchCount = parseInt($('section.agent-list:visible').length);
                $('section.content span.agent_search_result').text(iSearchCount);
                if(iSearchCount > 0)
                {//show pagination
                    $('section.content div.agent_list ul#paginationss').removeClass('hidden');
                }
                else
                {//hide pagination
                    $('section.content div.agent_list ul#paginationss').addClass('hidden');
                }
                
            }
        },

        'manipulate_agent_template' : function(uiAgentTemplate, oAgent)
        {
            if(typeof(uiAgentTemplate) != 'undefined' && typeof(oAgent) != 'undefined')
            {

                var uiAgentOverview = uiAgentTemplate.find('div.agent-overview');
                var uiAgentDetailed = uiAgentTemplate.find('div.agent-detailed');

                uiAgentTemplate.attr('data-agent-name', oAgent.first_name + ' ' + oAgent.last_name);
                uiAgentTemplate.attr('data-agent-id', oAgent.user_id );
                uiAgentTemplate.attr('data-callcenter', oAgent.callcenter_name);
                uiAgentTemplate.find('.info_full_name').text(oAgent.first_name + ' ' + oAgent.last_name);
                uiAgentTemplate.find('.info_username').text(oAgent.username);
                uiAgentTemplate.attr('data-user-id', oAgent.user_id);
                uiAgentTemplate.find('.info_user_level').text($('div.option.agent-type[data-value="'+oAgent.user_level+'"]:first').text())
                if (typeof(oPusher) != 'undefined') {
                    $.each(oPusher.channels.channels, function (channel_name, channel) {
                        if (channel_name == 'presence-ironman') {

                            if(typeof(channel.members.members[oAgent.user_id]) != 'undefined')
                            {
                                uiAgentTemplate.find('h2.status').text('ONLINE').removeClass('red-color').css('color', 'green');
                                uiAgentTemplate.find('span.status').text('ONLINE').removeClass('red-color').css('color', 'green');
                                uiAgentTemplate.find('span.status').addClass('online')
                            }

                            
                        }

                        
                    })


                }
                
          
                if(oAgent.is_archived == 1)
                {
                     uiAgentTemplate.find('button.archive_user').addClass('hidden');
                     uiAgentTemplate.find('button.edit_user').addClass('hidden');
                }
                if(oAgent.email_address.length > 0)
                {
                    uiAgentTemplate.find('.info_email_address').text(oAgent.email_address);
                }
                else
                {
                    uiAgentTemplate.find('.info_email_address').text('None');
                }
                
                if(oAgent.callcenter_name != null)
                {
                   uiAgentTemplate.find('.info_callcenter_name').text(oAgent.callcenter_name)
                }
                else
                {
                   uiAgentTemplate.find('.info_callcenter_name').text('Unknown')
                }

                if(typeof(oAgent.image_file) != 'undefined')
                {
                    if(oAgent.image_file.length > 0)
                    {
                        uiAgentTemplate.find('.info_initials').replaceWith('<div class="profile-pic" prof-name="HP"><img style="width:45px; height: 45px" class="img-circle img-responsive" src="'+ admin.config('url.server.assets_base') +'images/agents/'+oAgent.image_file+'"></div>')
                        uiAgentTemplate.find('.info_initials_inside').replaceWith('<div class="profile-pic" prof-name="HP"><img style="width:80px; height: 80px" class="img-circle img-responsive" src="'+ admin.config('url.server.assets_base') +'images/agents/'+oAgent.image_file+'"></div>')
                    }
                    else
                    {
                        var i=0;
                        var character='';
                        var sCharacters = '';
                        var iChars = "~`!#$%^&*+=-[]\\\';,/{}|\":<>?._";
                        while (i <= oAgent.first_name.length){
                            character = oAgent.first_name.charAt(i);
                            if (!isNaN(character * 1)){

                            }else{
                                if (character == character.toUpperCase() && iChars.indexOf(character) == -1) {
                                    sCharacters += character
                                }
                            }
                            i++;
                        }   
                        
                        if(sCharacters.length > 0)
                        {
                            uiAgentTemplate.find('.info_initials').text(sCharacters.substring(0, 2));    
                        }
                        else
                        {
                            uiAgentTemplate.find('.info_initials').text(oAgent.username.toUpperCase().substring(0, 2));     
                        }
                    }
                }

               /* if(iConstantUserLevel != 4)
                {
                    uiAgentTemplate.find('button.archive_user').addClass('hidden');
                    uiAgentTemplate.find('button.edit_user').addClass('hidden');
                }*/
                
                
                
                if(typeof(oAgent.provinces) != 'undefined')
                {
                    var aAgentProvincesId = oAgent.provinces.map(function(el) {
                        return el['province_id']
                    })
                    
                    var oProvinces = cr8v.get_from_storage('provinces');
                    if(typeof(oProvinces))
                    {
                        var oAgentProvinces = oProvinces.filter(function(el) {
                            return $.inArray(el['id'], aAgentProvincesId) > -1
                        })
                        
                        if(oAgentProvinces.length > 0)
                        {
                            var sProvinceButtonHtml = '';
                            $.each(oAgentProvinces, function(key, province) {
                               sProvinceButtonHtml += '<button type="button" class="btn btn-dark  margin-right-10" province_id="'+province.id+'">'+province.name+'</button>'
                            })

                            uiAgentTemplate.find('div.province-container').append(sProvinceButtonHtml)
                            uiAgentTemplate.find('.info_provinces').text(oAgentProvinces.length)
                        }
                    }
                }


                return uiAgentTemplate;
            }
        },

        'check_duplicate_username': function(userName, bEdit, userID){
          if(typeof(userName) != 'undefined')
          {
            var oAgents = cr8v.get_from_storage('agents');
            if(typeof(oAgents) != 'undefined')
            {
              if(!bEdit)
              {
                var oFilteredAgents = oAgents.filter(function(el) {
                  return el['username'].toLowerCase() == userName.toLowerCase() ;
                })
              }
              else
              {
                if(typeof(userID) != 'undefined')
                {
                  var oFilteredAgents = oAgents.filter(function(el) {
                    return el['user_id'] != userID && el['username'].toLowerCase() == userName.toLowerCase() ;
                  })
                }
              }

              if(count(oFilteredAgents) > 0)
              {
                return true;
              }
              
              return false;
            }
          }
        }

    }

}());

$(window).load(function () {
    admin.agents.initialize();
});