/**
 *  DST Report Class
 *
 *
 *
 */


(function () {



    "use strict";

    CPlatform.prototype.audit_trails = {

        'initialize': function () {
            //
            var $auditTrailsContainer = $('div.row[content="audit_trails"]'),
                $auditTrailsTable = $auditTrailsContainer.find('table#audit_trail_table'),
                $autidTrailsTrTemplate = $auditTrailsTable.find('tr.audit_trail_tr.template'),
                $generateButton = $auditTrailsContainer.find('button.generate_audit_trails');

            //populate input on change of select
            $auditTrailsContainer.on('click', 'div.option.audit_trail_type', function(e) {
               $(this).parents('div.select:first') .find('input:first').val($(this).attr('data-value'));
            })

            $auditTrailsContainer.on('click', 'button.generate_audit_trails', function() {
                var uiThis = $(this);
                uiThis.find('i.fa-spinner').removeClass('hidden');
                admin.audit_trails.fetch_audit_trails($auditTrailsContainer);
                setTimeout(function() {
                    uiThis.find('i.fa-spinner').addClass('hidden');
                }, 1000)
            })

        },

        'ajax': function (oAjaxConfig) {
            if (typeof(oAjaxConfig) != 'undefined') {
                admin.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        'fetch_audit_trails' : function($auditTrailsContainer) {
            if(typeof($auditTrailsContainer) != 'undefined')
            {
                var sDateFrom = $auditTrailsContainer.find('div#audit_trails_date_from input').val(),
                    sDateTo = $auditTrailsContainer.find('div#audit_trails_date_to input').val(),
                    sAuditTrailsType = $auditTrailsContainer.find('input[name="audit_trail_type"]').val(),
                    oParams = {
                        'params' : {
                            'date_from' : sDateFrom,
                            'date_to' : sDateTo
                        }
                        
                    },
                    oConsolidatedData = [],
                    
                    oLogUrls = [
                        admin.config('url.api.jfc.users') + "/users/fetch_logs",
                        admin.config('url.api.jfc.orders') + "/orders/fetch_logs",
                        admin.config('url.api.jfc.store') + "/stores/fetch_logs",
                        admin.config('url.api.jfc.promos') + "/promos/fetch_logs"
                    ];


                    if(sAuditTrailsType != 'All Audit Trails')
                    {
                        oParams.params.type = sAuditTrailsType;
                    }

                    $.each(oLogUrls, function(url_key, url) {
                        var oAjaxConfig = {
                            "type"   : "POST",
                            "data"   : oParams,
                            "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                            "url"    : url,
                            "success": function (data) {
                                if(typeof(data) != 'undefined')
                                {
                                    if(typeof(data.data) != 'undefined')
                                    {
                                        $.each(data.data, function(key, data) {
                                            data.username = admin.audit_trails.fetch_username(data)
                                            oConsolidatedData.push(data);
                                        })
                                    }
                                    
                                }
                                
                                if(url_key == (oLogUrls.length -1) )
                                {
                                    //console.log('last')
                                    oConsolidatedData = oConsolidatedData.sort(function(a, b){
                                        var keyA = new Date(a.date_created),
                                            keyB = new Date(b.date_created);
                                        // Compare the 2 dates
                                        if(keyA > keyB) return -1;
                                        if(keyA < keyB) return 1;
                                        return 0;
                                    });
                                    admin.audit_trails.render_audit_trails(oConsolidatedData)

                                }
                                
                            }
                        }

                        admin.audit_trails.ajax(oAjaxConfig);
                    }) 
            }
        },
        'fetch_username' : function(oData) {
            if(typeof(oData) != 'undefined')
            {
                var oAgents = cr8v.get_from_storage('agents'),
                    oStoreUsers = cr8v.get_from_storage('store_users'),
                    sUsername = 'Unknown',
                    oFilteredInformation;

                if(typeof(oAgents) != 'undefined')
                {
                    oFilteredInformation = oAgents.filter(function(agent) {
                        return agent['user_id'] == oData.user_id
                    })

                    if(count(oFilteredInformation) == 0)
                    {
                        oFilteredInformation = oStoreUsers.filter(function(store_user) {
                            return store_user['user_id'] == oData.user_id
                        })
                    }
                    
                        
                    if(count(oFilteredInformation) > 0)
                    {
                        if(typeof(oFilteredInformation[0].username) != 'undefined')
                            {
                                sUsername = oFilteredInformation[0].username;
                            }    
                    }

                    
                }

                return sUsername;
            }
        },

        'render_audit_trails' : function(oConsolidatedData) {
            if(typeof(oConsolidatedData) != 'undefined' && oConsolidatedData.length > 0)
            {
                var $auditTrailsContainer = $('div.row[content="audit_trails"]'),
                    $auditTrailsTable = $auditTrailsContainer.find('table#audit_trail_table')
                  
                    $auditTrailsContainer.find('div.audit-trail-content').removeClass('hidden');
                    $auditTrailsTable.removeClass('hidden')
                    $auditTrailsTable.find('tbody').find('tr.audit_trail_tr:not(.template)').remove();
                    $auditTrailsContainer.find('div.dst-not-generated').addClass('hidden');
               
                    $.each(oConsolidatedData, function(key, value) {
                        var $autidTrailsTrTemplate = $('div.row[content="audit_trails"]').find('tr.audit_trail_tr.template').clone().removeClass('template');
                        var $manipulatedtTemplate = admin.audit_trails.manipulate_audit_trails($autidTrailsTrTemplate, value);
                        $auditTrailsTable.find('tbody').append($manipulatedtTemplate)
                    })
            }
            else
            {
                var $auditTrailsContainer = $('div.row[content="audit_trails"]'),
                    $auditTrailsTable = $auditTrailsContainer.find('table#audit_trail_table')

                $auditTrailsContainer.find('div.dst-not-generated').removeClass('hidden');
                $auditTrailsContainer.find('div.audit-trail-content').addClass('hidden');
                $auditTrailsTable.addClass('hidden');
                $auditTrailsTable.find('tbody').find('tr.audit_trail_tr:not(.template)').remove();
            }
        },
        
        'manipulate_audit_trails' : function($autidTrailsTrTemplate, oData) {
            
            for (var key in oData) {
                  if (oData.hasOwnProperty(key)) {
                        
                        var oReconstructedData = admin.audit_trails.reconstruct_audit_trails_data(oData.data);
                        if(typeof($autidTrailsTrTemplate.find('[data-label="'+key+'"]')) != 'undefined')
                        {
                            if(moment(oData[key]).isValid() && key == 'date_created')
                            {
                                oData[key] = moment(oData[key]).format('MMMM DD,YYYY | h:mm a ');
                            }

                            if(key == 'value')
                            {
                                oData[key] = admin.audit_trails.reconstruct_audit_trails_data(oData[key], oData.type)
                            }
                            
                            $autidTrailsTrTemplate.find('[data-label="'+key+'"]').html(oData[key])
                        }
                  }
            }

            return $autidTrailsTrTemplate
        },
        
        'reconstruct_audit_trails_data' :  function(oData, sType) {
            if(typeof(oData) != 'undefined')
            {
                var oData = $.parseJSON(oData);
                var aUnincludedKeys = [
                    'is_ip_whitelist_excluded',
                    'is_deleted',
                    'sbu_id',
                    'callcenter_id',
                    'password',
                    'password_confirmation'
                ];

                $.each(aUnincludedKeys, function(key, value) {
                    if(typeof(oData[0]) != 'undefined')
                    {
                        if(typeof(oData[0][value]) != 'undefined')
                        {
                            delete oData[0][value];
                        }
                    }
                })



                    var oReconstructedData = '';
                    if(sType == 'Reports')
                    {
                       
                       oReconstructedData += 'Date From : ' + moment(oData.date_from).format('MMMM DD, YYYY') + ' <br>';
                       oReconstructedData += 'Date To : ' + moment(oData.date_to).format('MMMM DD, YYYY') + ' <br>';
                    }
                    else
                    {
                        if(typeof(oData[0]) != 'undefined')
                        {
                            //var oData = $.parseJSON(oData);
                            if(typeof(oData[0]) != 'undefined')
                            {

                                
                                for (var key in oData[0]) {
                                    if (oData[0].hasOwnProperty(key)) {


                                        var sKey = ucwords(key.replace('_', ' '));

                                        oReconstructedData += sKey + ' : ' + oData[0][key] + ' <br>';

                                    }


                                }
                            }

                        }
                    }

                    

                    return oReconstructedData;    
            }    
            
        },
       
    }

}());

$(window).load(function () {
    admin.audit_trails.initialize();
});


