(function () {
    "use strict";
    
    var loadMoreIndex = 3;

    CPlatform.prototype.messaging = {

        'initialize' : function () {
            $("select").transformDD();

            $('div[content="messaging"]').find('div.add_announcement_error_msg').hide();
            $('div[content="messaging"]').find('div.add_announcement_success_msg').hide();
            $('div[content="messaging"]').find('div.add_announcement_error_msg').find('p.error_messages').remove();

            $('div[content="messaging"]').on('click', 'a.create_new_global', function(){
                var uiThis = $(this);
                uiThis.hide();
            	$('div[content="messaging"]').find('div.new_announcement_container').show();
            	$('div[content="messaging"]').find('div.messaging_radio_select').find('label[for="store1"]').trigger('click');
            	$('div[content="messaging"]').find('div.new_announcement_container').find('input:visible, textarea:visible').val('').removeClass('error');
            	$('div[content="messaging"]').find('div.new_announcement_container').find('.select').removeClass('has-error');
            });

            $('div[content="messaging"]').find('div.new_announcement_container').on('click', 'button.cancel_new_announcement_btn', function(){
                var uiThis = $(this);
            	uiThis.parents('div.new_announcement_container:first').hide();
            	$('div[content="messaging"]').find('a.create_new_global').show();
            });

            //all stores
            $('div[content="messaging"]').find('div.messaging_radio_select').on('click', 'label[for="store1"]', function(){
                var uiThis = $(this);
                $('div[content="messaging"]').find('div.messaging-province-select').hide().find('input[name="messaging_province"]').val('').removeAttr('data-value datavalid labelinput');
                $('div[content="messaging"]').find('div.messaging-trading-time-select').hide().find('input[name="messaging_trading_time"]').val('').removeAttr('start-value close-value datavalid labelinput');
                $('div[content="messaging"]').find('div.messaging-callcenter-select').hide().find('input[name="messaging_callcenter_area"]').val('').removeAttr('datavalid labelinput');
            });

            //store area
            $('div[content="messaging"]').find('div.messaging_radio_select').on('click', 'label[for="store2"]', function(){
                var uiThis = $(this);
                $('div[content="messaging"]').find('div.messaging-province-select').show().find('input[name="messaging_province"]').val('').removeAttr('data-value datavalid labelinput').attr({'datavalid': 'required', 'labelinput' : 'Store Area'});
                $('div[content="messaging"]').find('div.messaging-trading-time-select').hide().find('input[name="messaging_trading_time"]').val('').removeAttr('start-value close-value datavalid labelinput');
                $('div[content="messaging"]').find('div.messaging-callcenter-select').hide().find('input[name="messaging_callcenter_area"]').val('').removeAttr('datavalid labelinput');
            });

            //store with specific trading time
            $('div[content="messaging"]').find('div.messaging_radio_select').on('click', 'label[for="store3"]', function(){
                var uiThis = $(this);
                $('div[content="messaging"]').find('div.messaging-province-select').hide().find('input[name="messaging_province"]').val('').removeAttr('data-value datavalid labelinput');
                $('div[content="messaging"]').find('div.messaging-trading-time-select').show().find('input[name="messaging_trading_time"]').val('').removeAttr('start-value close-value datavalid labelinput').attr({'datavalid': 'required', 'labelinput' : 'Trading Store Time'});
                $('div[content="messaging"]').find('div.messaging-callcenter-select').hide().find('input[name="messaging_callcenter_area"]').val('').removeAttr('datavalid labelinput');
            });

            //all callcenters
            $('div[content="messaging"]').find('div.messaging_radio_select').on('click', 'label[for="store4"]', function(){
                var uiThis = $(this);
                $('div[content="messaging"]').find('div.messaging-province-select').hide().find('input[name="messaging_province"]').val('').removeAttr('data-value datavalid labelinput');
                $('div[content="messaging"]').find('div.messaging-trading-time-select').hide().find('input[name="messaging_trading_time"]').val('').removeAttr('start-value close-value datavalid labelinput');
                $('div[content="messaging"]').find('div.messaging-callcenter-select').hide().find('input[name="messaging_callcenter_area"]').val('').removeAttr('datavalid labelinput');
            });

            $('div[content="messaging"]').find('div.messaging_radio_select').on('click', 'label[for="store5"]', function(){
                var uiThis = $(this);
                $('div[content="messaging"]').find('div.messaging-province-select').hide().find('input[name="messaging_province"]').val('').removeAttr('data-value datavalid labelinput');
                $('div[content="messaging"]').find('div.messaging-trading-time-select').hide().find('input[name="messaging_trading_time"]').val('').removeAttr('start-value close-value datavalid labelinput');
                $('div[content="messaging"]').find('div.messaging-callcenter-select').show().find('input[name="messaging_callcenter_area"]').val('').attr({'datavalid': 'required', 'labelinput' : 'Callcenter Area'});
            });

            //store area select
            $('div[content="messaging"]').find('div.messaging-province-select').on('click', 'div.option.province-select', function(){
                var uiThis = $(this);
                $('div[content="messaging"]').find('div.messaging-province-select').find('input[name="messaging_province"]').attr('data-value', uiThis.attr('data-value'));
            });

            //specific trading time select
            $('div[content="messaging"]').find('div.messaging-trading-time-select').on('click', 'div.option.store-time-selection', function(){
                var uiThis = $(this);
                $('div[content="messaging"]').find('div.messaging-trading-time-select').find('input[name="messaging_trading_time"]').attr({'start-value': uiThis.attr('start-value'), 'close-value': uiThis.attr('close-value')});
            });

            //callcenter area select
            $('div[content="messaging"]').find('div.messaging-callcenter-select').on('click', 'div.option.callcenter-select', function(){
                var uiThis = $(this);
                $('div[content="messaging"]').find('div.messaging-callcenter-select').find('input[name="messaging_callcenter_area"]').attr('data-value', uiThis.attr('data-value'));
            });

            //add new announcement validation
            var oValidationConfig = {
                'data_validation'    : 'datavalid',
                'input_label'        : 'labelinput',
                'min_length'         : 0,
                // 'max_length'         : 75,
                'class'              : 'error',
                'add_remove_class'   : true,
                'onValidationError'  : function (arrMessages) {
                    console.log(arrMessages); //shows the errors
                    var recipient = $('div[content="messaging"]').find('div.messaging-trading-time-select').find('input[name="messaging_trading_time"]');
                    var province = $('div[content="messaging"]').find('div.messaging-province-select').find('input[name="messaging_province"]');
                    var callcenter = $('div[content="messaging"]').find('div.messaging-callcenter-select').find('input[name="messaging_callcenter_area"]');

                    if(recipient.hasClass('error'))
                    {   
                        recipient.parents('div.select').addClass('has-error');
                    }
                    else
                    {   
                        recipient.parents('div.select').removeClass('has-error');
                    }
                    if(province.hasClass('error'))
                    {   
                        province.parents('div.select').addClass('has-error');
                    }
                    else
                    {   
                        province.parents('div.select').removeClass('has-error');
                    }
                    if(callcenter.hasClass('error'))
                    {   
                        callcenter.parents('div.select').addClass('has-error');
                    }
                    else
                    {   
                        callcenter.parents('div.select').removeClass('has-error');
                    }
                },
                'onValidationSuccess': function () {
                    var recipient = $('div[content="messaging"]').find('div.messaging-trading-time-select').find('input[name="messaging_trading_time"]');
                    var province = $('div[content="messaging"]').find('div.messaging-province-select').find('input[name="messaging_province"]');
                    var callcenter = $('div[content="messaging"]').find('div.messaging-callcenter-select').find('input[name="messaging_callcenter_area"]');

                    if(recipient.hasClass('error'))
                    {   
                        recipient.parents('div.select').addClass('has-error');
                    }
                    else
                    {   
                        recipient.parents('div.select').removeClass('has-error');
                    }
                    if(province.hasClass('error'))
                    {   
                        province.parents('div.select').addClass('has-error');
                    }
                    else
                    {   
                        province.parents('div.select').removeClass('has-error');
                    }
                    if(callcenter.hasClass('error'))
                    {   
                        callcenter.parents('div.select').addClass('has-error');
                    }
                    else
                    {   
                        callcenter.parents('div.select').removeClass('has-error');
                    }
                    //assemble info for adding of announcement
        			admin.messaging.assemble_add_announcement_information();
                }
            }
            //initialize form validation
            admin.validate_form($('form#add_announcement'), oValidationConfig);

            //save new announcement
            $('div[content="messaging"]').on('click', 'button.send_new_announcement_btn', function(){
                $(this).parents('form#add_announcement').submit();
            });

            $('div[content="messaging"]').find('form#add_announcement').on('submit', function (e) {
                $('div[content="messaging"]').find('div.add_announcement_error_msg').hide();
                $('div[content="messaging"]').find('div.add_announcement_success_msg').hide();
                $('div[content="messaging"]').find('div.add_announcement_error_msg').find('p.error_messages').remove();
                e.preventDefault();
            });


		    $('div[content="messaging"]').find('button.load_more').on('click', function () {
		    	var uiContainer = $('div.announcement_container')
			    var announcementSize = uiContainer.find('div.announcement:not(.template)').size();
	        	loadMoreIndex = (loadMoreIndex + 3 <= announcementSize) ? loadMoreIndex + 3 : announcementSize;
		        uiContainer.find('div.announcement:not(.template):lt('+ loadMoreIndex +')').show();
		        (loadMoreIndex == announcementSize) ? $(this).addClass('hidden') : '' ;
		    });

		    // $('#showLess').click(function () {
		    //     var x=(x-5<0) ? 3 : x-5;
		    //     $('#myList li').not(':lt('+x+')').hide();
		    // });

			admin.messaging.fetch_stores_operation_hours();
			admin.messaging.get_announcements();

            //archive upsell modal confirm button
            $('div[content="messaging"]').on("click", "button.archive_announcement_btn", function(){
                var uiThis = $(this);
                $("body").css({overflow:'hidden'});
                var tm = $(this).attr("modal-target");

                $("div[modal-id~='"+tm+"']").addClass("showed");

                $("div[modal-id~='"+tm+"'] .close-me").on("click",function(){
                    $("body").css({'overflow-y':'initial'});
                    $("div[modal-id~='"+tm+"']").removeClass("showed");
                });

                var announcementID = uiThis.attr('announcement-id');
                $("div[modal-id~='"+tm+"']").find("button.archive_announcement_btn_confirm").attr('announcement-id', announcementID);
            });

            //confirm archive announcement
            $('div[modal-id="confirm-archive-announcement"]').on('click', 'button.archive_announcement_btn_confirm', function(){
                var uiThis = $(this);
                var announcementID = uiThis.attr('announcement-id');
                admin.messaging.archive_announcement(announcementID)
            });
            

        },

        //assemble the announcement to be added
        'assemble_add_announcement_information': function(){
            var announcementSubject = $('div[content="messaging"]').find('form#add_announcement').find('input[name="announcement_subject"]').val();
            var announcementMessage = $('div[content="messaging"]').find('form#add_announcement').find('textarea[name="announcement_message"]').val();
            var announcementRecipient = $('div[content="messaging"]').find('form#add_announcement').find('input[type="radio"]:checked').val();
            var provinceID = $('div[content="messaging"]').find('form#add_announcement').find('input[name="messaging_province"]').attr('data-value');
            var provinceName = $('div[content="messaging"]').find('form#add_announcement').find('input[name="messaging_province"]').val();
            var startTime = $('div[content="messaging"]').find('form#add_announcement').find('input[name="messaging_trading_time"]').attr('start-value');
            var closeTime = $('div[content="messaging"]').find('form#add_announcement').find('input[name="messaging_trading_time"]').attr('close-value');
            var callcenterID = $('div[content="messaging"]').find('form#add_announcement').find('input[name="messaging_callcenter_area"]').attr('data-value');
            var callcenterName = $('div[content="messaging"]').find('form#add_announcement').find('input[name="messaging_callcenter_area"]').val();

            var oAnnouncementParams = {
                "params":{
					'sbu_id' 			: '1',
					'content' 			: announcementMessage,
					'subject' 			: announcementSubject,
					'for_store' 		: announcementRecipient,
					'store_area' 		: (typeof(provinceID) != 'undefined') ? provinceID : 0,
					'store_name' 		: (typeof(provinceName) != 'undefined') ? provinceName : '',
                    'callcenter_area'   : (typeof(callcenterID) != 'undefined') ? callcenterID : 0,
                    'callcenter_name'   : (typeof(callcenterName) != 'undefined') ? callcenterName : '',
					'store_time_start' 	: (typeof(startTime) != 'undefined') ? startTime : '00:00:00',
					'store_time_close' 	: (typeof(closeTime) != 'undefined') ? closeTime : '00:00:00',
					'is_deleted' 		: 0
                },
                "user_id" : iConstantUserId
            }
            
            var oAjaxConfig = {
                "type"   : "POST",
                "data"   : oAnnouncementParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : admin.config('url.api.jfc.store') + "/stores/add_announcements",
                "success": function (oAnnouncementData) {
                    if(oAnnouncementData.status == true)
                    {
       					var v = oAnnouncementData.data;
       					var uiContainer = $('div.announcement_container')
       					var uiTemplate 	= $('div.announcement_container').find('div.announcement.template').clone().removeClass('template');
       					var recipient = '';
       					if(v.for_store == 1)
       					{
       						recipient = 'All Stores';
       					}
       					else if(v.for_store == 2)
       					{
       						recipient = v.store_name+' Stores';
       					}
       					else if(v.for_store == 3)
       					{
       						recipient = 'Stores with Trading time of '+ moment(v.store_time_start, "HH:mm:ss").format('hh:mm A') +' to '+ moment(v.store_time_close, "HH:mm:ss").format('hh:mm A') +'';
       					}
                        else if(v.for_store == 4)
                        {
                            recipient = 'All Callcenters';
                        }
                        else if(v.for_store == 5)
                        {
                            recipient = v.callcenter_name+' Callcenters';
                        }

						uiTemplate.attr('announcement-id', v.id);
       					uiTemplate.find('p.subject').html(v.subject);
       					uiTemplate.find('p.message').html(v.content);
       					uiTemplate.find('td.date_added').text(moment(v.date_added).format('MMMM DD, YYYY'));
       					uiTemplate.find('td.recipient').text(recipient);
       					uiTemplate.find('button.archive_announcement_btn').attr('announcement-id', v.id);
       					uiContainer.prepend(uiTemplate)

       					$('div[content="messaging"]').find('div.add_announcement_success_msg').show().delay(3000).fadeOut();
       					
                        setTimeout( function() {
	       					$('div[content="messaging"]').find('form#add_announcement').find('button.cancel_new_announcement_btn').trigger('click');
                    		$('div[content="messaging"]').find('button.load_more').removeClass('hidden');
                        }, 3000)

                    }
                    else
                    {

                    }
                }
            }
            admin.upsell_promos.ajax(oAjaxConfig);
        },

        'fetch_stores_operation_hours' : function() {
          
                var oParams = {
                    "params" : {
                    	"sbu_id" : 1,
                        "limit" : 9999999999   
                    }
                }

                var oAjaxFetchStoresConfig = {
                    "type"   : "GET",
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "data"   : oParams,
                    "url"    : admin.config('url.api.jfc.store') + "/stores/get_stores_operation_hours",
                    "success": function (oData) {
                        if(typeof( oData.data) != 'undefined')
                        {
                            var sDropdownHtml = '';
                            $.each(oData.data, function(key, time) {
                                sDropdownHtml += '<div class="option store-time-selection" start-value="'+time.start_time+'" close-value="'+time.close_time+'">'+time.start_time +' - '+ time.close_time+'</div>'
                            })

                            $('div.select.messaging-trading-time-select').find('div.frm-custom-dropdown-option').html('').append(sDropdownHtml)
                        }
                        
                    }
                }
                admin.stores.ajax(oAjaxFetchStoresConfig);
        },

        //archive announcement
        'archive_announcement' :function(announcementID)
        {
            if(typeof(announcementID) != 'undefined')
            {
                var oParams = {
                    'params' : {
                        "announcement_id" : announcementID,
                        "is_deleted": 1
                    },
                    'user_id' : iConstantUserId
                }

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : admin.config('url.api.jfc.store') + "/stores/archive_announcement",
                    "success": function (oUpsellData) {
                        $('body').css('overflow', 'auto');
                        $('div[modal-id="confirm-archive-announcement"]').removeClass('showed');
                        $('div[content="messaging"]').find('div.announcement_container').find('div.announcement[announcement-id="'+announcementID+'"]').remove();
                    }
                }   
                admin.stores.ajax(oAjaxConfig);
            }
        },

        'get_announcements' : function() {
            var oParams = {
                "params" : {
                	"sbu_id" : 1,
                    "limit" : 9999999999,
                    "where" : [{
                    	"field" 	: 'ga.is_deleted',
                    	"operator" 	: '=',
                    	"value" 	: '0'
                    }]
                }
            }

            var oAjaxFetchStoresConfig = {
                "type"   : "GET",
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "data"   : oParams,
                "url"    : admin.config('url.api.jfc.store') + "/stores/get_announcements",
                "success": function (oData) {
                    if(typeof(oData.data) != 'undefined')
                    {
						var uiContainer = $('div.announcement_container')
                    	$.each(oData.data, function (key, value) {
							var uiTemplate 	= $('div.announcement_container').find('div.announcement.template').clone().removeClass('template');
							var uiManipulatedTemplate = admin.messaging.manipulate_announcements(uiTemplate, value);

							uiContainer.append(uiManipulatedTemplate.css('display', 'none' ))
                    	});

					    uiContainer.find('div.announcement:not(.template):lt('+loadMoreIndex+')').show();

                    }
                    
                }
            }
            admin.stores.ajax(oAjaxFetchStoresConfig);
        },

        'manipulate_announcements' :function( uiTemplate, oAnnouncementData){
        	if(typeof(uiTemplate) != 'undefined' && typeof(oAnnouncementData) != 'undefined')
        	{
				var v = oAnnouncementData;
				var recipient = '';
                var callcenters = cr8v.get_from_storage('callcenters');
                var provinces = cr8v.get_from_storage('provinces');

                if(typeof(callcenters) != 'undefined' && typeof(provinces) != 'undefined')
                {
    				if(v.for_store == 1)
    				{
    					recipient = 'All Stores';
    				}
    				else if(v.for_store == 2)
    				{
                        $.each(provinces, function (key ,value){
                            if(v.store_area == value.id)
                            {
                                recipient = value.name+' Stores';
                            }
                        })
    					// recipient = v.store_name+' Stores';
    				}
    				else if(v.for_store == 3)
    				{
    					recipient = 'Stores with Trading time of '+ moment(v.store_time_start, "HH:mm:ss").format('hh:mm A') +' to '+ moment(v.store_time_close, "HH:mm:ss").format('hh:mm A') +'';
                    }
                    else if(v.for_store == 4)
                    {
                        recipient = 'All Callcenters';
                    }
                    else if(v.for_store == 5)
                    {
                        $.each(callcenters, function (key ,value){
                            if(v.callcenter_area == value.id)
                            {
                                recipient = value.name+' Callcenters';
                            }
                        })
                        // recipient = v.callcenter_name+' Callcenters';
                    }
                }

				uiTemplate.attr('announcement-id', v.id);
				uiTemplate.find('p.subject').html(v.subject);
				uiTemplate.find('p.message').html(v.content);
				uiTemplate.find('td.date_added').text(moment(v.date_added).format('MMMM DD, YYYY'));
				uiTemplate.find('td.recipient').text(recipient);
				uiTemplate.find('button.archive_announcement_btn').attr('announcement-id', v.id);
				return uiTemplate;
        	}

        },
    }

}());

$(window).load(function () {
    admin.messaging.initialize();
});