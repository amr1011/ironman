/**
 *  Order management Class
 *
 *
 *
 */

(function () {
    "use strict";

    CPlatform.prototype.order_management = {

        'initialize': function () {

            //order managmenet tab
            $('header ul#main_nav_headers').on('click', 'li.order_management', function(){
                callcenter.store_management.content_panel_toggle('order_management');
                callcenter.store_management.sub_content_panel_toggle('call_orders');

                $('header ul#main_nav_headers li.order_management').addClass('active');
                $('section#search_order_form_container').addClass('hidden');
                $('section#search_order_list_container').addClass('hidden');
                $('section#customer_list_header').addClass('hidden')
                $('section#add_new_customer_header').addClass('hidden')
                $('section#order-process').children('div.row').addClass('hidden')
                $('body section#add_new_customer_header').removeClass('hidden').show();
                $('body section#order-process').removeClass('hidden').show();
                $('body section#order-process div#add_customer_content').removeClass('hidden');
                $("section.content #add_customer_content").find('div#city_select div.select input.dd-txt[name="city"]').attr({'datavalid':'required', 'labelinput':'City'});
                $("section.content #add_customer_content").find('div.select input.dd-txt[name="province"]').attr({'datavalid':'required', 'labelinput':'Province'});

                // $('header div.sub-nav.order_management').find("li.active").trigger("click");
                $('header div.sub-nav.order_management').find("li.call_orders").trigger("click");

                //clearing form values
                $('section#add_new_customer_header input#search_customer').val("");
                $('section#search_order_form_container form#search_order_form_params input[name="search_string"]').val("");
                $('section#customer_list_header input#search_customer_list').val("");

                //clearing results
                $('section.content div#customer_list_content').addClass('hidden');

            });

            //order managment call orders
            $('header div.sub-nav.order_management').on('click', 'li.call_orders', function(){
                $('header ul#main_nav_headers li.order_management').addClass('active');
                $('section#search_order_form_container').addClass('hidden');
                $('section#search_order_list_container').addClass('hidden');
                $('section#customer_list_header').addClass('hidden')
                $('section#add_new_customer_header').addClass('hidden')
                $('section#order-process').children('div.row').addClass('hidden')
                $('body section#add_new_customer_header').removeClass('hidden').show();
                $('body section#add_new_customer_header').find('p.count_search_result').html('<strong>No Search Result</strong>');
                // $('body section#order-process').removeClass('hidden').show();
                // $('body section#order-process div#add_customer_content').removeClass('hidden');
                $("section.content #add_customer_content").find('div#city_select div.select input.dd-txt[name="city"]').attr({'datavalid':'required', 'labelinput':'City'});
                $("section.content #add_customer_content").find('div.select input.dd-txt[name="province"]').attr({'datavalid':'required', 'labelinput':'Province'});
            })


            //order managment search order
            $('header div.sub-nav.order_management').on('click', 'li.search_orders', function(){
                $('header ul#main_nav_headers li.order_management').addClass('active');
                $('section#add_new_customer_header').addClass('hidden')
                $('section#customer_list_header').addClass('hidden')
                $('section#order-process').children('div.row').addClass('hidden')
                $('section#search_order_form_container').removeClass('hidden').show();
                $('section#search_order_list_container').removeClass('hidden').show();
            })

            //customer list tab in coordinator
            $('header div.sub-nav.order_management').on('click', 'li.customer_list', function(){
                $('header ul#main_nav_headers li.order_management').addClass('active');
                $('section#search_order_form_container').addClass('hidden');
                $('section#search_order_list_container').addClass('hidden');
                $('section#customer_list_header').addClass('hidden')
                $('section#add_new_customer_header').addClass('hidden')
                $('section#order-process').children('div.row').addClass('hidden')
                $('body section#customer_list_header').removeClass('hidden').show();
                $('body section#order-process').removeClass('hidden').show();
                $('body section#order-process div#customer_list_content').removeClass('hidden');
            });

            //product list tab in coordinator

            $('header div.sub-nav.order_management').on('click', 'li.product_list', function(){
                $('header ul#main_nav_headers li.order_management').addClass('active');
                $('section#search_order_form_container').addClass('hidden');
                $('section#search_order_list_container').addClass('hidden');
                $('section#customer_list_header').addClass('hidden')
                $('section#add_new_customer_header').addClass('hidden')
                $('section#order-process').children('div.row').addClass('hidden')
                $('body section#add_new_customer_header').removeClass('hidden').show();
                $('body section#order-process').removeClass('hidden').show();
                $('body section#order-process div#order_process_content').removeClass('hidden');
            });

            $('header div.sub-nav.order_management').on('click', 'li', function(){
                $(this).siblings('li').removeClass('active');
                $(this).addClass('active');
                //callcenter.store_management.sub_content_panel_toggle($(this).attr('page'));
                
                //clearing form values
                $('section#add_new_customer_header input#search_customer').val("");
                $('section#search_order_form_container form#search_order_form_params input[name="search_string"]').val("");
                $('section#customer_list_header input#search_customer_list').val("");

                //clearing results
                $('section.content div#customer_list_content').addClass('hidden');

                //hide alternate contact in add new customer
                $('section.content #add_customer_content div.primary_contact_number').find('a.alternate-contact-number').removeClass('hidden');
                $('section.content #add_customer_content div.alternate_contact_container').addClass("hidden");
                //set mobile container as default
                $('section.content #add_customer_content div.primary_contact_number').find("div.frm-custom-dropdown input[name='contact_number_type[]']").attr('value', 'Mobile');
                $('section.content #add_customer_content div.primary_contact_number').find("div.frm-custom-dropdown input[name='contact_number_type[]']").val('Mobile');
                $('section.content #add_customer_content div.primary_contact_number').find("input.small").removeClass("hidden");
                $('section.content #add_customer_content div.primary_contact_number').find("input.small").attr("datavalid", "required");
                $('section.content #add_customer_content div.primary_contact_number').find("input.xsmall").addClass("hidden");
                $('section.content #add_customer_content div.primary_contact_number').find("input.xsmall").removeAttr("datavalid");
                //set mobile container as default
                $('section.content #add_customer_content div.alternate_contact_container').find("div.frm-custom-dropdown input[name='contact_number_type[]']").attr('value', 'Mobile');
                $('section.content #add_customer_content div.alternate_contact_container').find("div.frm-custom-dropdown input[name='contact_number_type[]']").val('Mobile');
                $('section.content #add_customer_content div.alternate_contact_container').find("input.small").removeClass("hidden");
                $('section.content #add_customer_content div.alternate_contact_container').find("input.small").attr("datavalid", "required");
                $('section.content #add_customer_content div.alternate_contact_container').find("input.xsmall").addClass("hidden");
                $('section.content #add_customer_content div.alternate_contact_container').find("input.xsmall").removeAttr("datavalid");

            });




        }


    }

}());

$(window).load(function () {
    callcenter.order_management.initialize();
});


