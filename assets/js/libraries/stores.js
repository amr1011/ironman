(function () {
    "use strict";

    CPlatform.prototype.stores = {

        'initialize' : function () {
            //console.log('awa')
            //$("select").transformDD();
            
            admin.stores.fetch_stores_user();
            admin.stores.fetch_stores();

            oSock.bind({
                'event'   : 'pusher:member_added',
                'channel' : 'presence-ironman',
                'callback': function (oData) {
                    console.log(oData)
                    admin.stores.redraw_presence();
                    //alert(JSON.stringify(oData))
                }
            });

            oSock.bind({
                'event'   : 'pusher:member_removed',
                'channel' : 'presence-ironman',
                'callback': function (oData) {
                    console.log(oData)
                    admin.stores.redraw_presence();
                    //alert(JSON.stringify(oData))
                }
            });
         
               
            $('div.date-picker-store').datetimepicker({pickDate: false});

            $('div[content="store_management_list"].store_management_list').on('click', 'section.store-list div.store-overview', function(e) {
                $(this).addClass('hidden');
                $(this).parents('section.store-list:first').find('div.store-detailed').removeClass('hidden');

              /*  var iTop = $(this).parents('section.agent-list:first').find('div.agent-detailed').top,
                    iFinalTopValue = iTop - 10;

                $('html,body').animate({
                    scrollTop: iFinalTopValue
                }, 100);*/
            })

            $('div[content="store_management_list"].store_management_list').on('click', 'section.store-list div.store-detailed', function(e) {
                if(e.target.nodeName == "BUTTON" || e.target.nodeName == "A")
                {
                    //
                }
                else
                {
                    $(this).addClass('hidden');
                    $(this).parents('section.store-list:first').find('div.store-overview').removeClass('hidden');
                }
                
            })

            $('div.add_store_list').on('click','div.option.province-select', function() {
                $(this).parents('div.select:first').find('div.frm-custom-dropdown-txt').find('input').attr('int-value', $(this).attr('data-value'));
            });

            $('div.store_management_list').on('click', 'li.page-prev', function() {
                    
                    var uiActive = $(this).siblings('li.page.page-active');
                     if(typeof(uiActive.prev('li.page')) != 'undefined')
                     {
                         uiActive.prev('li.page').trigger('click')
                     }
            })

             $('div.store_management_list').on('click', 'li.page-next', function() {
                    var uiActive = $(this).siblings('li.page.page-active');
                     if(typeof(uiActive.next('li.page')) != 'undefined')
                     {
                         uiActive.next('li.page').trigger('click')
                     }
            })

            $('section[header-content="store_management_list"]').on('click', 'button.add_stores', function() {
                $('div[content="store_management_list"]').find('div.add_store_list').removeClass('hidden');

                var iTop = $('div[content="store_management_list"]').find('div.add_store_list').offset().top,
                    iFinalTopValue = iTop - 10;

                $('html,body').animate({
                    scrollTop: iFinalTopValue
                }, 20);
            })

            $('div.sub-nav[content="store_management_list"]').on('click', 'li.sub-nav-coordinator-order', function() {
                //console.log('awa')
                var uiContent = $(this).attr('contents');
                $('section[header-content="store_management_list"]').find('h1.content').text(uiContent);

                var sSearchString = ' el["id"] != 0';
                var oStores = cr8v.get_from_storage('stores');
                var bPagination = false;

                if(typeof(oStores) != 'undefined')
                {

                    if( uiContent == 'Archive Store List')
                    {
                        $('li.sub-nav-coordinator-order').removeClass('active');
                        $(this).addClass('active')
                        sSearchString += ' && el["is_deleted"] == 1 '
                        $('div.store_management_list').find('ul.paganationss').addClass('hidden')
                    }
                    else if(uiContent == 'Online Stores')
                    {
                        $('li.sub-nav-coordinator-order').removeClass('active');
                        $(this).addClass('active')
                        sSearchString += ' && admin.stores.check_store_availability(el["id"]) == true '
                        $('div.store_management_list').find('ul.paganationss').addClass('hidden')
                    }
                    else if(uiContent == 'Offline Stores')
                    {
                        $('li.sub-nav-coordinator-order').removeClass('active');
                        $(this).addClass('active')
                        sSearchString += ' && admin.stores.check_store_availability(el["id"]) == false '
                        $('div.store_management_list').find('ul.paganationss').addClass('hidden')
                    }
                    else
                    {
                        $('div.store_management_list').find('ul.paganationss').removeClass('hidden')
                        sSearchString += ' && el["is_deleted"] == 0 ' ;
                        bPagination = true;
                    }

                    oStores = oStores.filter(function(el) {
                        return eval(sSearchString);
                    })

                    if(bPagination)
                    {
                        oStores = oStores.filter(function(el, i) {
                            return i >= 0 && i <= 50;
                        })
                    }


                   admin.stores.create_pagination(oStores);
                   admin.stores.render_store_list(oStores, undefined ,bPagination, uiContent);
                   $('div.store_management_list').find('li.page[page-start="0"][page-end="50"]').addClass('page-active');
                }

            })

            $('section[header-content="store_management_list"]').on('click','div.option.province-select', function() {
                $(this).parents('div.select.provinces-store-list:first').find('div.frm-custom-dropdown-txt').find('input').attr('int-value', $(this).attr('data-value'));
            })

            var oValidationConfig = {
                        'data_validation'    : 'datavalid',
                        'input_label'        : 'labelinput',
                        'min_length'         : 0,
                        'max_length'         : 75,
                        'class'              : 'error',
                        'add_remove_class'   : true,
                        'Username': {
                            'min_length': 4,
                            'max_length': 12
                        },
                        'Password': {
                            'min_length': 4,
                            'max_length': 12
                        },
                        'Confirm Password': {
                            'min_length': 4,
                            'max_length': 12
                        },
                        'onValidationError'  : function (arrMessages) {
                            //alert(JSON.stringify(arrMessages))
                            $('div.add_store_list').find('.notification').find('div.error-msg').removeClass('hidden')
                        },
                        'onValidationSuccess': function () {
                                  var iError = 0;
                                  var sMessage = '';
                                  /*if($('input[name="callcenter_id"]').val().length == 0)
                                  {
                                        sMessage = "You must assign select province of the store.";  
                                        iError++;
                                  }*/


                                  var oAgents = cr8v.get_from_storage('agents'),
                                        oStoreUsers = cr8v.get_from_storage('store_users'),
                                        iError = 0,
                                        sUsername = $('form#add_store').find('input[name="username"]').val(),
                                        oFilteredInformation;

                                  if(typeof(oAgents) != 'undefined')
                                  {
                                        oFilteredInformation = oAgents.filter(function(agent) {
                                            return agent['username'].toLowerCase() == sUsername.toLowerCase()
                                        })

                                        if(count(oFilteredInformation) == 0)
                                        {
                                            oFilteredInformation = oStoreUsers.filter(function(store_user) {
                                                return store_user['username'].toLowerCase() == sUsername.toLowerCase()
                                            })
                                        }

                                        if(count(oFilteredInformation) > 0)
                                        {
                                            iError++;
                                            sMessage = 'Username is already taken';
                                        }


                                  }

                                  if(iError > 0)
                                  {
                                       $('span.error-message-text').text(sMessage)
                                       $('section.notification').find('div.error-msg').removeClass('hidden')
                                  }
                                  else
                                  {
                                       $('section.notification').find('div.error-msg').addClass('hidden')
                                       $('section.notification').find('div.error-msg').find('span.error-message-text').text('Please fill up all required fields.')
                                       admin.stores.assemble_store_information();    
                                  }
                             
                       }
            }
            
            admin.validate_form($('form#add_store'), oValidationConfig);
            
            $('div.add_store_list').on('click', 'button.add_store_submit', function(e) {
                  e.preventDefault();
                  $('form#add_store').submit();       
            })

            $('form#add_store').on('submit', function(e) {
                  e.preventDefault(); 
            })
            
            var oEditValidationConfig = {
                        'data_validation'    : 'datavalid',
                        'input_label'        : 'labelinput',
                        'min_length'         : 0,
                        'max_length'         : 75,
                        'class'              : 'error',
                        'add_remove_class'   : true,
                        'Username': {
                            'min_length': 4,
                            'max_length': 12
                        },
                        'Password': {
                            'min_length': 4,
                            'max_length': 12
                        },
                        'Confirm Password': {
                            'min_length': 4,
                            'max_length': 12
                        },
                        'onValidationError'  : function (arrMessages) {
                            console.log(JSON.stringify(arrMessages))
                            $('div.edit_store_list').find('.notification').find('div.error-msg').removeClass('hidden');
                        },
                        'onValidationSuccess': function () {
                                  var iError = 0;
                                  var sMessage = '';
                                  /*if($('input[name="callcenter_id"]').val().length == 0)
                                  {
                                        sMessage = "You must assign select province of the store.";  
                                        iError++;
                                  }*/
                            var oAgents = cr8v.get_from_storage('agents'),
                                oStoreUsers = cr8v.get_from_storage('store_users'),
                                iError = 0,
                                sUsername = $('form#edit_store').find('input[name="username"]').val(),
                                sUserId = $('form#edit_store').find('input[type="hidden"][name="user_id"]').val(),
                                oFilteredInformation;

                            if(typeof(oAgents) != 'undefined')
                            {
                                oFilteredInformation = oAgents.filter(function(agent) {
                                    return agent['username'].toLowerCase() == sUsername.toLowerCase() && agent['user_id'] != sUserId
                                })

                                if(count(oFilteredInformation) == 0)
                                {
                                    oFilteredInformation = oStoreUsers.filter(function(store_user) {
                                        return store_user['username'].toLowerCase() == sUsername.toLowerCase() && store_user['user_id'] != sUserId
                                    })
                                }

                                if(count(oFilteredInformation) > 0)
                                {
                                    iError++;
                                    sMessage = 'Username is already taken';
                                }


                            }

                            if(iError > 0)
                            {
                                $('span.error-message-text').text(sMessage)
                                $('section.notification').find('div.error-msg').removeClass('hidden')
                            }
                            else{
                                           console.log('here');
                                       $('section.notification').find('div.error-msg').addClass('hidden')
                                       $('section.notification').find('div.error-msg').find('span.error-message-text').text('Please fill up all required fields.')
                                       var uiButton = $('div.edit_store_list button.edit_store_submit');
                                       admin.stores.show_spinner(uiButton, true);
                                       admin.stores.assemble_store_update_information();    
                                  }
                             
                       }
            }
            
            admin.validate_form($('form#edit_store'), oEditValidationConfig);

            $('div.edit_store_list').on('click', 'button.edit_store_submit', function(e) {
                  e.preventDefault();
                  $('form#edit_store').submit();       
            })

            $('form#edit_store').on('submit', function(e) {
                  e.preventDefault(); 
            })

            $('div.add_store_list').on('click', 'button.add_store_cancel',  function(e) {
                $('div.add_store_list').addClass('hidden');
            });


            $('div.store_management_list').on('click', 'li.page:not(.active)', function() {
                    if($('li[content="store_management_list"]:visible').hasClass('active'))
                    {
                    var uiThis = $(this);
                   
                    
                    var iStart = uiThis.attr('page-start');
                    var iEnd = uiThis.attr('page-end');
                    
                    var oStores = cr8v.get_from_storage('stores');
                    var oFilteredStores = oStores.filter(function( el,i) {
                            return i >= iStart &&  i <= iEnd && el['is_deleted'] == 0;
                    })

                    admin.stores.render_store_list(oFilteredStores)

                    uiThis.siblings('li.page').removeClass('active');
                    
                                        //uiThis.addClass('page-active')
                   
                    $('div.store_management_list').find('li.page[page-start="'+iStart+'"][page-end="'+iEnd+'"]').addClass('page-active');        
                    }
                    
            })   


//             $('table.provinces-choices').on('click', 'label.chck-lbl', function(e) {
//                     var uiAreaCount = $('table.provinces-choices').parents('div.add-agent-container').find('info.area-count')
//                     var uiSelectedCount = $('table.provinces-choices').find('input:checked').length;
//                     uiAreaCount.text(uiSelectedCount)
//             });

            $('section[header-content="store_management_list"] div.sort-container').on('click', 'strong.sort', function() {
                var uiAgentContainers = $('div.store_management_list:visible');
                var sSortKey = $(this).attr('sort');
                var iSortDirectionAsc;
                var iSortDirectionDesc;

                $(this).toggleClass('asc desc');

                if($(this).hasClass('asc') == true)
                {
                    iSortDirectionAsc = -1;
                    iSortDirectionDesc = 1;
                    $(this).siblings('img.asc').removeClass('hidden');
                    $(this).siblings('img.desc').addClass('hidden');
                }
                else
                {
                    iSortDirectionAsc = 1;
                    iSortDirectionDesc = -1;
                    $(this).siblings('img.asc').addClass('hidden');
                    $(this).siblings('img.desc').removeClass('hidden');
                }

                if(typeof(uiAgentContainers) != 'undefined')
                {
                    $.each(uiAgentContainers, function(key, container) {
                        $(container).children('section.store-list:not(.template)').sortDomElements(function(a,b){
                            var akey = $(a).attr(sSortKey);
                            var bkey = $(b).attr(sSortKey);
                            if (akey == bkey) return 0;
                            if (akey < bkey) return iSortDirectionAsc;
                            if (akey > bkey) return iSortDirectionDesc;
                        })
                    })
                }

            })

            $('section[header-content="store_management_list"]').on('keypress', 'input[name="store_search"]', function (e) {
              if(e.keyCode == 13)
              {
                $('section[header-content="store_management_list"]').find('button.search_store').trigger('click');
              }
            });

            $('section[header-content="store_management_list"]').on('click', 'button.search_store', function() {
                 var uiThis = $(this);
                 admin.stores.show_spinner(uiThis, true)

                 setTimeout(function() {
                    admin.stores.show_spinner(uiThis, false)
                }, 1000)
                 var    sSearchString = ' el["store_id"] != 0',
                        oStores = cr8v.get_from_storage('stores'),
                        sString = $('section[header-content="store_management_list"]').find('input[name="store_search"]').val(),
                        sStringSearchBy = $('section[header-content="store_management_list"]').find('input[name="store_search_by"]').val(),
                        iStringProvince = $('section[header-content="store_management_list"]').find('input[name="store_province"]').attr('int-value'),
                        iActiveTab = $('div[content="store_management_list"]').find('li.active'),
                        aOnlineStoreId = [];

                uiThis.siblings('li.page').removeClass('active');
                uiThis.addClass('active');

                $.each(oPusher.channels.channels, function (channel_name, channel) {
                        if (channel_name == 'presence-ironman') {
                        
                            $.each(channel.members.members, function(i, member) {
                                if (member.hasOwnProperty('store_user_id'))
                                {
                                    if(typeof(member) != 'undefined')
                                    {
                                       aOnlineStoreId.push(member.store_user_id);             
                                    }
                                 }
                            })
                        }
                })

                if($('div[content="store_management_list"]').find('li[contents="Archive Store List"]').hasClass('active'))
                {
                    sSearchString += " && el['is_deleted'] == 1 ";    
                }
                else
                {
                    sSearchString += " && el['is_deleted'] == 0 ";        
                }        

                if(sString.length > 0)
                {
                    sSearchString += " && el['name'].toLowerCase().indexOf('"+sString.toLowerCase()+"') > -1";
                }

                if(iStringProvince > 0)
                {
                    sSearchString += " && el['province'] == '"+iStringProvince+"'";
                }

                if(typeof(oStores) != 'undefined')
                {
                    var oFilteredStores = oStores.filter(function(el) {
                        return eval(sSearchString);
                    })

                    if(iActiveTab.attr('contents') == 'Online Stores')
                            {
                                oFilteredStores = oFilteredStores.filter(function(el) {
                                    return aOnlineStoreId.indexOf(el['id']) > -1
                                })            
                            }
                            else if(iActiveTab.attr('contents') == 'Offline Stores')
                            {
                                 oFilteredStores = oFilteredStores.filter(function(el) {
                                    return aOnlineStoreId.indexOf(el['id']) < 0
                                })
                            }

                    //populate search result count
                    var iSearchCount = parseInt(oFilteredStores.length);
                    $('section.content span.store_search_result').text(iSearchCount);
                    if(iSearchCount > 0)
                    {//show pagination
                        $('section.content div.store_management_list ul#store-paginationss').removeClass('hidden');
                    }
                    else
                    {//hide pagination
                        $('section.content div.store_management_list ul#store-paginationss').addClass('hidden');
                    }

                    admin.stores.create_pagination(oFilteredStores);
                    admin.stores.render_store_list(oFilteredStores);
                }

               

            });

            $('div.store_management_list').on('click', 'div.store-detailed button.archive_store', function() {
                    
                    var uiParentContainer = $(this).parents('section.store-list');
                    $('div[modal-id="archive-store"]').addClass('showed');
                    $('div[modal-id="archive-store"]').attr('data-store-id', uiParentContainer.attr('data-store-id'));
                    $('div[modal-id="archive-store"]').attr('data-user-id', uiParentContainer.attr('data-user-id'));    
            })

            $('div.edit_store_list').on('click','div.option.province-select', function() {
                var iProvinceID = $(this).attr('data-value');        
                $(this).parents('div.select:first').find('div.frm-custom-dropdown-txt').find('input').attr('int-value', iProvinceID);
            });

            $('div.edit_store_list').on('click','label.edit-trading-time', function() {
                var uiOpenTime = $('div.edit_store_list').find('input[name="time_start"]');
                var uiCloseTime = $('div.edit_store_list').find('input[name="time_end"]');

                var tOpenTimeOrigVal = uiOpenTime.attr('original-value');
                var tCloseTimeOrigVal = uiCloseTime.attr('original-value');
                
                if($(this).siblings('input').is(':checked')==true)
                {
                   uiOpenTime.val(tOpenTimeOrigVal);
                   uiCloseTime.val(tCloseTimeOrigVal);
                }
                else
                {
                   uiOpenTime.val("12:00 AM");
                   uiCloseTime.val("12:00 AM");
                }
            });

            $('div.add_store_list').on('click','label.add-trading-time', function() {
                var uiOpenTime = $('div.add_store_list').find('input[name="time_start"]');
                var uiCloseTime = $('div.add_store_list').find('input[name="time_end"]');

                var tOpenTimeOrigVal = uiOpenTime.attr('original-value');
                var tCloseTimeOrigVal = uiCloseTime.attr('original-value');
                
                if($(this).siblings('input').is(':checked')==true)
                {
                   uiOpenTime.val("");
                   uiCloseTime.val("");
                }
                else
                {
                   uiOpenTime.val("12:00 AM");
                   uiCloseTime.val("12:00 AM");
                }
            });

            $('div.store_management_list').on('click', 'div.store-detailed button.edit_store', function() {  
                    $('form#edit_store')[0].reset();
                    var uiParentContainer = $(this).parents('section.store-list');
                    //initialize variables
                    var iStoreID = uiParentContainer.attr('data-store-id'),
                        iUserID = uiParentContainer.attr('data-user-id'),
                        sStoreName = uiParentContainer.attr('data-store-name'),
                        sUsername = uiParentContainer.find('p.username:last').text(),
                        sEmail = uiParentContainer.find('p.email_address:first').text(),
                        sContact = uiParentContainer.find('p.contact_number:first').text(),
                        sServingTimeStart = uiParentContainer.find('info.serving_time_start:first').text(),
                        sServingTimeEnd = uiParentContainer.find('info.serving_time_end:first').text(),
                        iArea = uiParentContainer.attr('data-store-province'),
                        sTelcoProvider = uiParentContainer.attr('data-telco-provider'),
                        sStoreOwnership = uiParentContainer.attr('data-store-ownership'),
                        sIPaddress = uiParentContainer.attr('data-ip-address'),
                        iStoreCode = uiParentContainer.find('info[data-label="code"]:first').attr('store_code'),
                        iOrderLimit = uiParentContainer.find('info.order_limit:first').text(),
                        iExcluded = uiParentContainer.attr('excluded'),
                        iBlacklisted = uiParentContainer.attr('blacklisted')
                    ;

                    //sServingTimeStart = moment('2015-12-19 '+sServingTimeStart).format('hh:mm:ss A');
                    //sServingTimeEnd = moment('2015-12-19 '+sServingTimeEnd).format('hh:mm:ss A');

                    //show container
                    var uiFormContainer = $('div.store_management_list').find('div.edit_store_list').removeClass('hidden'); 
                    
                    var iTop = uiFormContainer.offset().top,
                    iFinalTopValue = iTop;

                    $('html,body').animate({
                        scrollTop: iFinalTopValue
                    }, 100);

                    uiFormContainer.find('div.error-msg').addClass('hidden');

                    //populate form to update
                    uiFormContainer.find('input[name="store_id"]').val(iStoreID);
                    if(iExcluded > 0)
                    {
                        uiFormContainer.find('input#ip-whitelist-exclude-store-edit').prop('checked', true);
                    }
                    if(iBlacklisted > 0)
                    {
                        uiFormContainer.find('input#is_blacklisted').prop('checked', true);
                    }
                    uiFormContainer.find('input[name="user_id"]').val(iUserID);
                    uiFormContainer.find('input[name="store_name"]').val(sStoreName);
                    uiFormContainer.find('input[name="store_code"]').val(iStoreCode);
                    uiFormContainer.find('input[name="max_order"]').val(iOrderLimit);
                    uiFormContainer.find('input[name="username"]').val(sUsername);
                    uiFormContainer.find('input[name="email"]').val(sEmail);
                    uiFormContainer.find('input[name="contact_number"]').val(sContact);
                    uiFormContainer.find('input[name="store_telco_provider"]').val(sTelcoProvider);
                    uiFormContainer.find('input[name="ownership"]').val(sStoreOwnership);
                    uiFormContainer.find('input[name="service_ip"]').val(sIPaddress);
                    uiFormContainer.find('input[name="time_start"]').val(sServingTimeStart).attr('original-value',sServingTimeStart);
                    uiFormContainer.find('input[name="time_end"]').val(sServingTimeEnd).attr('original-value',sServingTimeEnd);
                    uiFormContainer.find('input[labelinput="Operation Time Start"]').val(sServingTimeStart);
                    uiFormContainer.find('input[labelinput="Operation Time End"]').val(sServingTimeEnd);
                    
                    uiFormContainer.find('div.provinces-store-list div[data-value="'+iArea+'"]').trigger('click');
                    
            })

            $('div.store_management_list').on('click', 'button.edit_store_cancel', function() {  
                    $('div.store_management_list').find('div.edit_store_list').addClass('hidden'); 
            })

            $('div[modal-id="archive-store"]').on('click', 'button.confirm-archive', function() {
                var iStoreID = $(this).parents('div[modal-id="archive-store"]:first').attr('data-store-id');
                var iUserID = $(this).parents('div[modal-id="archive-store"]:first').attr('data-user-id');
                var uiThis = $(this);
                cr8v.agents.show_spinner(uiThis, true);
                var oParams = {
                    "params" : {
                        "data" : [{
                            "field" : 'is_deleted',
                            "value" : '1'
                        }],
                        "qualifiers" : [{
                            "field" : "id",
                            "value" : iStoreID,
                            "operator" : "="
                        }]
                    },
                    "user_id" : iConstantUserId

                }

                var oAjaxFetchAgentsConfig = {
                    "type"   : "POST",
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "data"   : oParams,
                    "url"    : admin.config('url.api.jfc.store') + "/stores/update",
                    "success": function (oData) {
                        $('body').css('overflow-y', 'auto');            
                        if(typeof(oData) != 'undefined' && typeof(oData.data[0]) != 'undefined')
                        {
                              var oUpdatedUser = oData.data[0]; 
                              var oStore = cr8v.get_from_storage('stores');
                              var iFoundIndex;
                              if(typeof(oStore) != 'undefined')
                              {
                                        $.each(oStore, function(i, store) {
                                              if(store.id == oUpdatedUser.id)
                                              {
                                                   iFoundIndex = i;
                                                   return false;     
                                              }  
                                        });

                                        var oUserParams = {
                                                    "params":{
                                                            "data" : [
                                                                   {
                                                                     "field" : "u.is_archived",
                                                                     "value" : 1
                                                                   }

                                                            ],
                                                                "qualifiers" : [
                                                                    {
                                                                     "field" : "user_id",
                                                                     "value" : iUserID,
                                                                     "operator" : "="
                                                                    }
                                                                ]
                                                    },
                                                    "user_id" : iConstantUserId
                                                };
                                        
                                        var oAjaxAddUserConfig = {
                                                "type"   : "POST",
                                                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                                                "data"   : oUserParams,
                                                "url"    : admin.config('url.api.jfc.users') + "/users/update_store_user",
                                                "success": function (oUserData) {
                                                     if(typeof(oUserData.status) != 'undefined')
                                                    {
          
                                                    }
                                                }
                                            }
                                            admin.stores.ajax(oAjaxAddUserConfig);

                                        cr8v.get_from_storage('stores').splice(iFoundIndex, 1);
                                        cr8v.get_from_storage('stores').unshift(oData.data[0]);
                                        $('section.store-list[data-store-id="'+oUpdatedUser.id+'"]').remove();
                                        $('div[modal-id="archive-store"]').removeClass('showed');
                                        cr8v.agents.show_spinner(uiThis, false);
                              }
                        } 
                        
      
                    }
                }

                admin.agents.ajax(oAjaxFetchAgentsConfig);
                cr8v.agents.show_spinner(uiThis, false);
            })
            
        },

        'check_user_availability' : function(iUserID) {
            if(typeof(iUserID) != 'undefined')
            {
                var bAvailable = false;
                if (typeof(oPusher) != 'undefined') {
                    $.each(oPusher.channels.channels, function (channel_name, channel) {
                        if (channel_name == 'presence-ironman') {

                            channel.members.each(function (member) {
                                var userId = member.id;
                                var userInfo = member.info;
                                ////console.log(userInfo)
                                if (userInfo != null) {
                                    if (userInfo.hasOwnProperty('user_id')) {
                                        if(userInfo.user_id == iUserID)
                                        {
                                            bAvailable = true;
                                        }
                                    }
                                }

              
                            })
                        }

                        
                    })


                }
                        
                
            }
            console.log(bAvailable)
            return bAvailable;
        },

        'redraw_presence' : function() {
                    if (typeof(oPusher) != 'undefined') {
                        $.each(oPusher.channels.channels, function (channel_name, channel) {
                            if (channel_name == 'presence-ironman') {
                                 $('section.agent-list[data-user-id]').find('h2.status').text('OFFLINE').css('color', 'red').addClass('red-color');
                                 $('section.agent-list[data-user-id]').find('span.status').text('OFFLINE').css('color', 'red').addClass('red-color');
                                $('section.store-list[data-store-id]').find('h3.status').text('OFFLINE').css('color', 'red').addClass('red-color');
                                $('section.store-list[data-store-id]').find('strong.status').text('OFFLINE').css('color', 'red').addClass('red-color');
                                channel.members.each(function (member) {
                                    var userId = member.id;
                                    var userInfo = member.info;
                                    ////console.log(userInfo)
                                    if (userInfo != null && typeof(userInfo) != 'undefined') {
                                        if (userInfo.hasOwnProperty('user_id')) {
                                            $('section.agent-list[data-user-id="'+userInfo.user_id+'"]').find('h2.status').text('ONLINE').css('color', 'green').removeClass('red-color');
                                            $('section.agent-list[data-user-id="'+userInfo.user_id+'"]').find('span.status').text('ONLINE').css('color', 'green').removeClass('red-color');
                                        }

                                        if (userInfo.hasOwnProperty('store_user_id'))
                                        {
                                            $('section.store-list[data-store-id="'+userInfo.store_user_id+'"]').find('h3.status').text('ONLINE').css('color', 'green').removeClass('red-color');
                                            $('section.store-list[data-store-id="'+userInfo.store_user_id+'"]').find('strong.status').text('ONLINE').css('color', 'green').removeClass('red-color');
                                        }
                                    }
                                })
                            }
                        })
                    }
        },

        'assemble_store_information' : function() {
            
            var oData = {
                    
                       "name" : $('form#add_store').find('input[name="store_name"]').val(),
                       "code" : $('form#add_store').find('input[name="store_code"]').val(),
                       "operating_hours_start" : $('form#add_store').find('input[name="time_start"]').val(),
                       "operating_hours_close" :$('form#add_store').find('input[name="time_end"]').val(),
                       "phone_number" : "5555555",
                       "email_address" : $('form#add_store').find('input[name="email"]').val(),
                       "ownership": $('form#add_store').find('input[name="ownership"]').val(),
                       "address" : "address",
                       "max_order_count" :  $('form#add_store').find('input[name="max_order"]').val(),
                       "is_delivery" : $('form#add_store').find('input[name="24_hours"]:checked').length,
                       "current_status" : "1",
                       "sbu_id" : "1",
                        "telco" : $('form#add_store').find('input[name="store_telco_provider"]').val(),
                        "ip" : $('form#add_store').find('input[name="service_ip"]').val(),
                       "username" : $('form#add_store').find('input[name="username"]').val(),
                       "password" : $('form#add_store').find('input[name="password"]').val(),
                       "province" : $('form#add_store').find('input[name="province"]').attr('int-value')

            }

            var oParams = {
                "params" : oData,
                "user_id" : iConstantUserId
            }

                var oAjaxFetchStoresConfig = {
                    "type"   : "POST",
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "data"   : oParams,
                    "url"    : admin.config('url.api.jfc.store') + "/stores/add",
                    "success": function (oData) {
                        
                        if(typeof(oData) != 'undefined')
                        {
                            if(oData.status == true && oData.data[0].id > 0)
                            {
                                var oUserParams = {
                                    "params" : {
                                        'sbu_id' : 1,
                                        'first_name' : $('form#add_store').find('input[name="username"]').val(),
                                        'last_name' : $('form#add_store').find('input[name="username"]').val(),
                                        'username' : $('form#add_store').find('input[name="username"]').val(),
                                        'password' : $('form#add_store').find('input[name="password"]').val(),
                                        'email' : $('form#add_store').find('input[name="email"]').val(),
                                        'store_id' : oData.data[0].id,
                                        'is_ip_whitelist_excluded' : $('form#add_store').find('input#ip-whitelist-exclude-store-add:checked').length

                                    },
                                    "updated_by" : iConstantUserId
                                   
                                }

                                var oAjaxAddUserConfig = {
                                    "type"   : "POST",
                                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                                    "data"   : oUserParams,
                                    "url"    : admin.config('url.api.jfc.users') + "/users/add_user",
                                    "success": function (oUserData) {
                                         if(typeof(oUserData.status) != 'undefined')
                                        {
                                            if(oUserData.status)
                                            {
                                                $('section.notification').find('div.success-msg').removeClass('hidden')
                                                $('section.notification').find('div.error-msg').addClass('hidden')
                                                
                                                if(typeof(oData.data[0]) != 'undefined')
                                                {
                                                    cr8v.get_from_storage('stores').unshift(oData.data[0]);
                                                    cr8v.get_from_storage('store_users').unshift(oUserData.data);        
                                                }

                                                var oStores = cr8v.get_from_storage('stores');

                                                oStores = oStores.filter(function(el, i) {
                                                            return i >= 0 && i <= 50 && el['is_deleted'] == 0;
                                                })    

                                                admin.stores.render_store_list(oStores);
                                                $('form#add_store')[0].reset()
                                                

                                            }
                                            else
                                            {
                                                $('section.notification').find('div.error-msg').removeClass('hidden')
                                                $('section.notification').find('div.success-msg').addClass('hidden')
                                            }

                                            setTimeout(function() {
                                                $('section.notification').find('div.error-msg').addClass('hidden')
                                                $('section.notification').find('div.success-msg').addClass('hidden')
                                                $('div.add_store_list').addClass('hidden');
                                            }, 2000)

                                        }
                                    }
                                }
                                admin.stores.ajax(oAjaxAddUserConfig);
                            }
                        }
                        
                    }
                }

                admin.stores.ajax(oAjaxFetchStoresConfig);
            
        },

        'assemble_store_update_information' : function() {
               var tOperationStart = '2015-12-19 ' + $('form#edit_store').find('input[name="time_start"]').val();
               var tOperationClose = '2015-12-19 ' + $('form#edit_store').find('input[name="time_end"]').val();

               tOperationStart = moment(tOperationStart,"YYYY-MM-DD hh:mm A").format('HH:mm:ss');
               tOperationClose = moment(tOperationClose,"YYYY-MM-DD hh:mm A").format('HH:mm:ss');


               var oParams = {
                        "params": {
                            "data" : [
                                           {
                                             "field" : "name",
                                             "value" : $('form#edit_store').find('input[name="store_name"]').val()
                                           },
                                           {
                                             "field" : "code",
                                             "value" : $('form#edit_store').find('input[name="store_code"]').val()
                                           },
                                           {
                                             "field" : "operating_hours_start",
                                             "value" : tOperationStart
                                           },
                                           {
                                             "field" : "operating_hours_close",
                                             "value" : tOperationClose
                                           },
                                           {
                                             "field" : "email_address",
                                             "value" : $('form#edit_store').find('input[name="email"]').val()
                                           },
                                           {
                                             "field" : "contact_number",
                                             "value" : $('form#edit_store').find('input[name="contact_number"]').val()
                                           },
                                           {
                                             "field" : "ownership",
                                             "value" : $('form#edit_store').find('input[name="ownership"]').val()
                                           },
                                           {
                                             "field" : "max_order_count",
                                             "value" : $('form#edit_store').find('input[name="max_order"]').val()
                                           },
                                           {
                                             "field" : "telco",
                                             "value" : $('form#edit_store').find('input[name="store_telco_provider"]').val()
                                           },
                                           {
                                             "field" : "ip",
                                             "value" : $('form#edit_store').find('input[name="service_ip"]').val()
                                           },
                                           {
                                             "field" : "province",
                                             "value" : $('form#edit_store').find('input[name="province"]').attr('int-value')
                                           },

                                    ],
                            "qualifiers" : [
                                {
                                 "field" : "id",
                                 "value" : $('form#edit_store').find('input[name="store_id"]').val(),
                                 "operator" : "="
                                }
                            ]
                        },
                        "user_id" : iConstantUserId
               };

                var oAjaxFetchStoresConfig = {
                    "type"   : "POST",
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "data"   : oParams,
                    "url"    : admin.config('url.api.jfc.store') + "/stores/update",
                    "success": function (oData) {
                        
                        if(typeof(oData) != 'undefined')
                        {
                            if(oData.status == true)
                            {
                                var oUserParams = {
                                        "params":{
                                                "data" : [
                                                       {
                                                         "field" : "su.first_name",
                                                         "value" : $('form#edit_store').find('input[name="store_name"]').val()
                                                       },
                                                       {
                                                         "field" : "su.last_name",
                                                         "value" : $('form#edit_store').find('input[name="username"]').val()
                                                       },
                                                       {
                                                         "field" : "su.email_address",
                                                         "value" : $('form#edit_store').find('input[name="email"]').val()
                                                       },
                                                       {
                                                         "field" : "u.username",
                                                         "value" : $('form#edit_store').find('input[name="username"]').val()
                                                       },
                                                        {
                                                            "field" : "u.is_ip_whitelist_excluded",
                                                            "value" : $('form#edit_store').find('input#ip-whitelist-exclude-store-edit:checked').length
                                                        },
                                                         {
                                                            "field" : "u.is_blacklisted",
                                                            "value" : $('form#edit_store').find('input#is_blacklisted:checked').length
                                                        }
 
                                                ],
                                                    "qualifiers" : [
                                                        {
                                                         "field" : "user_id",
                                                         "value" : $('form#edit_store').find('input[name="user_id"]').val(),
                                                         "operator" : "="
                                                        }
                                                    ]
                                        },
                                        "user_id" : iConstantUserId
                                    };
                                
                                if($('form#edit_store').find('input[name="password"]').val()!="")
                                {
                                      var sPassword = $('form#edit_store').find('input[name="password"]').val();
                                      oUserParams.params.data.push(
                                      {
                                        "field":"u.password",
                                        "value":sPassword,
                                        "operator": "="
                                      }
                                      );      
                                }

                                var oAjaxAddUserConfig = {
                                    "type"   : "POST",
                                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                                    "data"   : oUserParams,
                                    "url"    : admin.config('url.api.jfc.users') + "/users/update_store_user",
                                    "success": function (oUserData) {
                                         if(typeof(oUserData.status) != 'undefined')
                                        {
                                            //console.log('test');
//                                             if(typeof(oData.data[0]) != 'undefined')
//                                                 {
//                                                     cr8v.get_from_storage('stores').unshift(oData.data[0]);
//                                                     cr8v.get_from_storage('store_users').unshift(oUserData.data);        
//                                                 }

//                                                 var oStores = cr8v.get_from_storage('stores');

//                                                 oStores = oStores.filter(function(el, i) {
//                                                             return i >= 0 && i <= 50 && el['is_deleted'] == 0;
//                                                 })    

//                                                 admin.stores.render_store_list(oStores);

                                                var oUpdatedStore = oData.data[0]; 
                                                var oStores = cr8v.get_from_storage('stores');
                                                var iFoundIndex;
                                                if(typeof(oStores) != 'undefined')
                                                {
                                                    $.each(oStores, function(i, store) {
                                                          //console.log(store.id);
                                                          if(store.id == oUpdatedStore.id)
                                                          {
                                                               iFoundIndex = i;
                                                               return false;     
                                                          }  
                                                    });
                                                    //console.log(iFoundIndex);
                                                    cr8v.get_from_storage('stores').splice(iFoundIndex, 1);
                                                    cr8v.get_from_storage('stores').unshift(oData.data[0]);
                                                    
                                                    oStores = cr8v.get_from_storage('stores');

                                                    //update user 
                                                            var oUpdatedStoreUser = oUserData.data[0]; 
                                                            var oStoreUsers = cr8v.get_from_storage('store_users');
                                                            var iFoundUserIndex;
                                                            if(typeof(oStoreUsers) != 'undefined')
                                                            {
                                                                $.each(oStoreUsers, function(k, store_user) {
                                                                      //console.log(store.id);
                                                                      if(store_user.user_id == oUpdatedStoreUser.user_id)
                                                                      {
                                                                           iFoundUserIndex = k;
                                                                           return false;     
                                                                      }  
                                                                });
                                                                //console.log(iFoundIndex);
                                                                cr8v.get_from_storage('store_users').splice(iFoundUserIndex, 1);
                                                                cr8v.get_from_storage('store_users').unshift(oUserData.data[0]);

                                                                oStoreUsers = cr8v.get_from_storage('store_users');
                                                            }

                                                    admin.stores.render_store_list(oStores);

                                                }
                                                
                                            

                                            $('form#edit_store')[0].reset()
                                            $('section.notification').find('div.success-msg').removeClass('hidden')
                                            $('section.notification').find('div.error-msg').addClass('hidden')
                                            
                                            setTimeout(function() {
                                                $('section.notification').find('div.error-msg').addClass('hidden')
                                                $('section.notification').find('div.success-msg').addClass('hidden')
                                                $('div.edit_store_list').addClass('hidden');

                                                var uiButton = $('div.edit_store_list button.edit_store_submit');
                                                admin.stores.show_spinner(uiButton, false);
                                            }, 2000)

                                        }
                                    }
                                }
                                admin.stores.ajax(oAjaxAddUserConfig);
                            }
                            
                        }
                        
                    }
                }

                admin.stores.ajax(oAjaxFetchStoresConfig);
            
        },



        'ajax': function (oAjaxConfig) {
            if (typeof(oAjaxConfig) != 'undefined') {
                admin.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        'fetch_stores_user' : function() {
        	  var oParams = {
                    "params" : {
                    	"type" : 3,
                    	"sbu_id" : 1,
                        "limit" : 9999999999,
                        "where" : [
                        	{
                        		'key' : 'u.sbu_id',
                        		'operator' : '=',
                        		'value' : '1'
                        	},
                        	{
                        		'key' : 'su.store_id',
                        		'operator' : '>',
                        		'value' : '0'
                        	}
                        ]
                        
                    }

                }

                var oAjaxFetchStoresUsersConfig = {
                    "type"   : "GET",
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "data"   : oParams,
                    "url"    : admin.config('url.api.jfc.users') + "/users/get",
                    "success": function (oData) {
                        //var oData = JSON.parse(oData);
                        if(typeof( oData.data) != 'undefined')
                        {
                            cr8v.add_to_storage('store_users', oData.data);
                            var oStores = cr8v.get_from_storage('store_users')
                        }
                        
                    }
                }

                admin.stores.ajax(oAjaxFetchStoresUsersConfig);	
        },

        'fetch_stores' : function() {
          
                var oParams = {
                    "params" : {
                    	"sbu_id" : 1,
                        "limit" : 9999999999
                        
                    }

                }

                var oAjaxFetchStoresConfig = {
                    "type"   : "GET",
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "data"   : oParams,
                    "url"    : admin.config('url.api.jfc.store') + "/stores/get",
                    "success": function (oData) {
                        //var oData = JSON.parse(oData);
                        if(typeof( oData.data) != 'undefined')
                        {
                            //add dropdown in dst reports
                            var sDropdownHtml = '<div class="option store-selection" int-value="0">All Stores</div>';
                            //add dropdown for sales reports
                            var sDropdownHtmlReport = '<div class="option store-selection" int-value="0">All Stores</div>';
                            //add dropdown for product sales reports
                            var sDropdownHtmlProductReport = '<div class="option store-selection" int-value="0">All Stores</div>';
                            //add dropdown for category sales reports
                            var sDropdownHtmlCategoryReport = '<div class="option store-selection" int-value="0">All Stores</div>';
                            var sHourlyStore = '';
                            var sDstStore = '';
                            $.each(oData.data, function(key, store) {
                                sDropdownHtml += '<div class="option store-selection" int-value="'+store.id+'">'+store.name+'</div>'
                                sDropdownHtmlReport += '<div class="option" int-value="'+store.id+'">'+store.name+'</div>'
                                sDropdownHtmlProductReport += '<div class="option store-selection-product-sales-report" int-value="'+store.id+'">'+store.name+'</div>'
                                sDropdownHtmlCategoryReport += '<div class="option store-selection-category-sales-report" int-value="'+store.id+'">'+store.name+'</div>'
                                sHourlyStore += '<div class="option store_select" int-value="'+store.id+'">'+store.name+'</div>'
                                sDstStore += '<div class="option custom-filter-option" data-value="'+store.id+'">'+store.name+'</div>'

                            })

                            $('div.select.custom-store-filter').find('div.frm-custom-dropdown-option').append(sDstStore)

                            $('div.select.store-selection').find('div.frm-custom-dropdown-option').html('').append(sDropdownHtml)
                            
                            //add dropdown for sales reports
                            $('div.select.sales-report-store-selection').find('div.frm-custom-dropdown-option').html('').append(sDropdownHtmlReport)

                            //add dropdown for product sales reports
                            $('div.select.store-selection-product-sales-report').find('div.frm-custom-dropdown-option').html('').append(sDropdownHtmlProductReport)
            
                            //add dropdown for category sales reports
                            $('div.select.store-selection-category-sales-report').find('div.frm-custom-dropdown-option').html('').append(sDropdownHtmlCategoryReport)

                            var uiStoreHourlySelect = $('div.select.hourly_store_selection').find('div.frm-custom-dropdown-option');
                            uiStoreHourlySelect.append(sHourlyStore);

                            var uiStoreSalesSummarySelect = $('div.select.sales_summary_store_selection').find('div.frm-custom-dropdown-option');
                            uiStoreSalesSummarySelect.append(sHourlyStore);

                            var uiStoreProdAvailSelect = $('div.select.pu_store_selection').find('div.frm-custom-dropdown-option');
                            uiStoreProdAvailSelect.append(sHourlyStore);

                            cr8v.add_to_storage('stores', oData.data);

                            var oStores = cr8v.get_from_storage('stores')
                            var uiStoreContainer = $('div[content="store_management_list"].store_management_list');
                            var iPageCount = oStores.length / 50;
                            var uiPaginationContainer = uiStoreContainer.find('ul.paganationss.content-container');
                            var uiPaginationContainerHidden = uiStoreContainer.find('ul.paganations-hidden.content-container');
                            var sPaginationHtmlhidden = '<li class="page-prev"><i class="fa fa-angle-left"></i></li>';
                            var sPaginationHtml = '<li class="page-prev"><i class="fa fa-angle-left"></i></li>';

                            iPageCount += 1;
                            if(iPageCount > 0)
                            {
                                for (i = 0; i < parseInt(iPageCount); i++) {
                                    var iPage = i + 1;
                                    var iStart, iEnd;
                                    if(i == 0)
                                    {
                                        iStart = 0;
                                        iEnd = 50;
                                    }
                                    else
                                    {
                                        iStart = i * 50;
                                        iEnd = ((i + 1) * 50);
                                    }


                                    sPaginationHtmlhidden += '<li class="page store" page-start="'+iStart+'" page-end="'+iEnd+'" page-end="50" data-no="'+ iPage +'">'+iPage+'</li>';
                                    sPaginationHtml += '<li class="pages" page-start="'+iStart+'" page-end="'+iEnd+'">'+iPage+'</li>';
                                }
                            }
                            else
                            {
                                sPaginationHtml+= '<li class="page page-active" page-start="0" page-end="50" data-no="1">1</li>';
                            }

                            uiPaginationContainer.html('');
                            uiPaginationContainer.append(sPaginationHtml);
                            uiPaginationContainerHidden.html('');
                            uiPaginationContainerHidden.append(sPaginationHtmlhidden)

                            $('#store-paginationss').twbsPagination({
                                totalPages: iPageCount,
                                visiblePages: 15,
                                onPageClick: function (event, page) {
                                    console.log($(event))
                                    //alert();
                                    $('li.page.store[data-no="'+page+'"').trigger('click');
                                }
                            });





                            admin.stores.render_store_list(oStores);
                            $('div.store_management_list').find('li.page[page-start="0"][page-end="50"]').addClass('page-active');
                        }
                        
                    }
                }

                admin.stores.ajax(oAjaxFetchStoresConfig);
                
               

        },

        'check_store_availability': function (iStoreID) {
            var bOnlineStore = false;
            if (typeof(iStoreID) && typeof(oPusher) != undefined) {
                $.each(oPusher.channels.channels, function (channel_name, channel) {
                    if (channel_name == 'presence-ironman') {

                        channel.members.each(function (member) {
                            if (member.info.hasOwnProperty('store_id')) //if this member is a store
                            {
                                if (member.info.store_id == iStoreID) {
                                    bOnlineStore = true;
                                    return false;
                                }
                            }
                        });
                    }
                })
            }

            return bOnlineStore;
        },

        'render_store_list': function (oStores, bAdd, bPagination, uiContent) {
            if (typeof(oStores) != 'undefined') {
                var uiStoreContainer = $('div[content="store_management_list"].store_management_list');
                uiStoreContainer.find('section.store-list:not(.template)').remove();

                // var oStoredStores = cr8v.get_from_storage('stores');


                // var iPageCount = oStores.length / 50;

                // var uiPaginationContainer = uiStoreContainer.find('ul.paganationss.content-container');
                // var uiPaginationContainerHidden = uiStoreContainer.find('ul.paganations-hidden.content-container');
                // var sPaginationHtmlhidden = '<li class="page-prev"><i class="fa fa-angle-left"></i></li>';
                // var sPaginationHtml = '<li class="page-prev"><i class="fa fa-angle-left"></i></li>';

                // iPageCount += 1;
                // if (iPageCount > 0) {
                //     for (i = 0; i < parseInt(iPageCount); i++) {
                //         var iPage = i + 1;
                //         var iStart, iEnd;
                //         if (i == 0) {
                //             iStart = 0;
                //             iEnd = 50;
                //         }
                //         else {
                //             iStart = i * 50;
                //             iEnd = ((i + 1) * 50);
                //         }


                //         sPaginationHtmlhidden += '<li class="page store" page-start="' + iStart + '" page-end="' + iEnd + '" page-end="50" data-no="' + iPage + '">' + iPage + '</li>';
                //         sPaginationHtml += '<li class="pages" page-no="'+iPage+'"  page-start="' + iStart + '" page-end="' + iEnd + '">' + iPage + '</li>';
                //     }
                // }
                // else {
                //     sPaginationHtml += '<li class="page page-active" page-start="0" page-end="50" data-no="1">1</li>';
                // }

                // uiPaginationContainer.html('');
                // uiPaginationContainer.append(sPaginationHtml);
                // uiPaginationContainerHidden.html('');
                // uiPaginationContainerHidden.append(sPaginationHtmlhidden);

                // try {
                //     $('#store-paginationss').twbsPagination('destroy');
                // }
                // catch (e) {
                //     // statements to handle any exceptions
                //     //logMyErrors(e); // pass exception object to error handler
                // }

                // $('#store-paginationss').twbsPagination({
                //     totalPages  : iPageCount,
                //     visiblePages: 15,
                //     onPageClick : function (event, page) {
                //         console.log($(event.currentTarget));
                //         console.log($(event));            
                //         $('li.page.store[data-no="' + page + '"').trigger('click');
                //     }
                // });

                //uiPaginationContainer.append(sPaginationHtml);

                oStores = oStores.filter(function (el, i) {
                    return i >= 0 && i <= 50;
                })

                $.each(oStores, function (key, store) {
                    var oUsers = cr8v.get_from_storage('store_users');
                    var uiTemplate = $('section.store-list.template').clone().removeClass('template');
                    uiTemplate.find('[data-label="name"]').text(store.name)
                    // uiTemplate.find('[data-label="id"]').text(store.id)
                    // uiTemplate.find('[data-label="code"]').text(store.code)

                    //aligned from zxavier
                    /*system id in zxavier*/
                    uiTemplate.find('[data-label="id"]').text(store.code);
                    /*split the name for the store code*/
                    var sStoreCode = store.name.split('-');
                    uiTemplate.find('[data-label="code"]').text(sStoreCode[0]).attr('store_code', store.code);

                    uiTemplate.find('.contact_number').text(store.contact_number);
                    uiTemplate.find('.last_online').text(store.last_online_date);

                    if (uiContent == 'Online Stores' || uiContent == 'Offline Stores') {
                        uiTemplate.find('button.edit_store').addClass('hidden');
                        $('button.add_stores').addClass('hidden')
                    }
                    else {
                        $('button.add_stores').removeClass('hidden')
                    }

                    if (typeof(oPusher) != 'undefined') {
                        $.each(oPusher.channels.channels, function (channel_name, channel) {
                            if (channel_name == 'presence-ironman') {

                                $.each(channel.members.members, function (i, member) {
                                    if (member.hasOwnProperty('store_id')) {
                                        if (typeof(member) != 'undefined') {
                                            if (member.store_id == store.id) {
                                                uiTemplate.find('h3.status').text('ONLINE').removeClass('red-color').css('color', 'green');
                                                uiTemplate.find('strong.status').text('ONLINE').removeClass('red-color').css('color', 'green');
                                            }
                                        }


                                    }
                                })


                            }


                        })


                    }

                    if (store.is_deleted == 1) {
                        uiTemplate.find('button.archive_store').addClass('hidden');
                    }
                    //console.log(store);
                    uiTemplate.attr('data-store-name', store.name)
                    uiTemplate.attr('data-store-name', store.is_ip_whitelist_excluded)
                    uiTemplate.attr('data-store-id', store.id)
                    uiTemplate.attr('data-store-province', store.province)
                    uiTemplate.attr('data-telco-provider', store.telco)
                    uiTemplate.attr('data-store-ownership', store.ownership)
                    uiTemplate.attr('data-ip-address', store.ip)
                    uiTemplate.find('.email_address').text(store.email_address)
                    uiTemplate.find('.order_limit').text(store.max_order_count)
                    uiTemplate.find('[data-label="province"]').text($('div.provinces-agent-list:first').find('div.option[data-value="' + store.province + '"]').text())

                    oUsers = oUsers.filter(function (el) {
                        return el['store_id'] == store.id;
                    });

                    var uiUserContainer = uiTemplate.find('section.store-user-container');
                    if (typeof(oUsers) != 'undefined' && oUsers.length > 0) {
                        uiUserContainer.find('section.store-user:not(.template)').remove();
                        $.each(oUsers, function (ukey, user) {
                            var uiUserTemplate = uiTemplate.find('section.store-user.template').clone().removeClass('template');
                            uiUserTemplate.find('.username').text(user.username);
                            uiUserContainer.append(uiUserTemplate)
                            uiTemplate.attr('data-user-id', user.user_id)
                            uiTemplate.attr('excluded', user.is_ip_whitelist_excluded)
                            uiTemplate.attr('blacklisted', user.is_blacklisted)
                        })
                    }
                    else {
                        uiUserContainer.append('<p>No Accounts Related to store</p>')
                    }

                    //var starttime = store.operating_hours_start.split(":");
                    //var endtime = store.operating_hours_close.split(":");
                    //uiTemplate.find('.serving_time_start').text(moment({hour: starttime[0], minute: starttime[1], seconds: starttime[2]}).format('hh:mm a'))
                    //uiTemplate.find('.serving_time_end').text(moment({hour: endtime[0], minute: endtime[1], seconds: endtime[2]}).format('hh:mm a'))

                    var sServingTimeStart = moment('2015-12-19 ' + store.operating_hours_start).format('hh:mm A');
                    var sServingTimeEnd = moment('2015-12-19 ' + store.operating_hours_close).format('hh:mm A');

                    uiTemplate.find('.serving_time_start').text(sServingTimeStart);
                    uiTemplate.find('.serving_time_end').text(sServingTimeEnd);

                    /* if(iConstantUserLevel != 4)
                     {
                     uiTemplate.find('button.archive_store').addClass('hidden');
                     }*/

                    if (typeof(bAdd) == 'undefined') {
                        uiStoreContainer.append(uiTemplate);
                    }
                    else {
                        uiPaginationContainer.after(uiTemplate);
                    }

                    //uiStoreContainer.append(uiTemplate);
                });

            }
        },

        'create_pagination' : function(oStores){
            var uiStoreContainer = $('div[content="store_management_list"].store_management_list');
            uiStoreContainer.find('section.store-list:not(.template)').remove();

            var oStoredStores = cr8v.get_from_storage('stores');


            var iPageCount = oStores.length / 50;

            var uiPaginationContainer = uiStoreContainer.find('ul.paganationss.content-container');
            var uiPaginationContainerHidden = uiStoreContainer.find('ul.paganations-hidden.content-container');
            var sPaginationHtmlhidden = '<li class="page-prev"><i class="fa fa-angle-left"></i></li>';
            var sPaginationHtml = '<li class="page-prev"><i class="fa fa-angle-left"></i></li>';

            iPageCount += 1;
            if (iPageCount > 0) {
                for (i = 0; i < parseInt(iPageCount); i++) {
                    var iPage = i + 1;
                    var iStart, iEnd;
                    if (i == 0) {
                        iStart = 0;
                        iEnd = 50;
                    }
                    else {
                        iStart = i * 50;
                        iEnd = ((i + 1) * 50);
                    }


                    sPaginationHtmlhidden += '<li class="page store" page-start="' + iStart + '" page-end="' + iEnd + '" page-end="50" data-no="' + iPage + '">' + iPage + '</li>';
                    sPaginationHtml += '<li class="pages" page-no="'+iPage+'"  page-start="' + iStart + '" page-end="' + iEnd + '">' + iPage + '</li>';
                }
            }
            else {
                sPaginationHtml += '<li class="page page-active" page-start="0" page-end="50" data-no="1">1</li>';
            }

            uiPaginationContainer.html('');
            uiPaginationContainer.append(sPaginationHtml);
            uiPaginationContainerHidden.html('');
            uiPaginationContainerHidden.append(sPaginationHtmlhidden);

            try {
                $('#store-paginationss').twbsPagination('destroy');
            }
            catch (e) {
                // statements to handle any exceptions
                //logMyErrors(e); // pass exception object to error handler
            }

            $('#store-paginationss').twbsPagination({
                totalPages  : iPageCount,
                visiblePages: 15,
                onPageClick : function (event, page) {
                    console.log($(event.currentTarget));
                    console.log($(event));            
                    $('li.page.store[data-no="' + page + '"').trigger('click');
                }
            });
        },

        'show_spinner': function (uiElem, bShow) {
            if (bShow == true) {
                if(uiElem.find('i.fa-spinner').length > 0)
                {
                    uiElem.find('i.fa-spinner').removeClass('hidden');
                }
                else
                {
                    uiElem.append('<i class="fa fa-spinner fa-pulse hidden"></i>').removeClass('hidden');
                }

                uiElem.prop('disabled', true);
            }
            else {
                if(uiElem.find('i.fa-spinner').length > 0)
                {
                    uiElem.find('i.fa-spinner').addClass('hidden');
                }
                uiElem.prop('disabled', false);
            }
        },

    }

}());

$(window).load(function () {
    admin.stores.initialize();
});