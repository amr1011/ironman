/**
 *  Search Order Class
 *
 *
 *
 */
(function () {
    "use strict";
    var uiPage = 'search_order';

    CPlatform.prototype.search_order = {

        'initialize' : function () {

            callcenter.search_order.content_panel_toggle('search_order');

        },

        'content_panel_toggle': function (uiClass) {
            var uiNavs = $('ul.main_nav li');

            $.each(uiNavs, function () {
                $(this).removeClass('active')

                if ($(this).hasClass(uiClass)) {
                    $(this).addClass('active');
                }
            })


        }

    }

}());

$(window).load(function () {
    callcenter.search_order.initialize();
});


