/**
 *  Sales Report Class
 *
 *
 *
 */
(function () {
    "use strict";

    var generateByDateBtn,
        generateByPeriodBtn,
        generateByDateDiv,
        generateByPeriodDiv,
        showGenerateReport,
        noGenerateReport,
        datepickerDateFrom,
        datepickerDateTo,
        reportContent,
        salesReportTable,
        salesReportXlsBtn,
        selectPeriodDropdownDiv,
        salesReportLink,
        selectStoreDropdownDiv,
        selectProvinceDropdownDiv,
        selectCCenterDropdownDiv,
        selectTransactionDropdownDiv,
        selectGroupDropdownDiv,
        getOrderLimit = 9999999;

    CPlatform.prototype.sales_report = {

        'initialize' : function () {

            generateByDateBtn       = $('section#sales_reports_header').find('button.generate_by_date');
            generateByPeriodBtn     = $('section#sales_reports_header').find('button.generate_by_period');
            generateByDateDiv       = $('section#sales_reports_header').find('div.generate_by_date');
            generateByPeriodDiv     = $('section#sales_reports_header').find('div.generate_by_period');
            showGenerateReport      = $('section#sales_reports_header').find('div.show_generate_report');
            noGenerateReport        = $('section#sales_reports_header').find('div.no_generate_report');
            reportContent           = $('section.content').find('div#sales_reports_content');
            datepickerDateFrom      = $('section#sales_reports_header').find('div#sales-report-date-from');
            datepickerDateTo        = $('section#sales_reports_header').find('div#sales-report-date-to');
            salesReportTable        = $('section.content').find('div#sales_reports_content table#sales_report_table');
            salesReportXlsBtn       = $('section#sales_reports_header').find('button.sales_report_xls_button');
            selectPeriodDropdownDiv = $('section#sales_reports_header').find('div.generate_by_period div.select_period');
            // selectStoreDropdownDiv  = $('section#sales_reports_header').find('div.sales-report-store-selection');
            selectStoreDropdownDiv  = $('section#sales_reports_header').find('div.sales_summary_store_selection');
            selectProvinceDropdownDiv  = $('section#sales_reports_header').find('div.sales_summary_province_selection');
            selectCCenterDropdownDiv  = $('section#sales_reports_header').find('div.sales_summary_callcenter_selection');
            selectTransactionDropdownDiv  = $('section#sales_reports_header').find('div.sales_summary_transaction_selection');
            selectGroupDropdownDiv  = $('section#sales_reports_header').find('div.sales_summary_group_selection');
            salesReportLink         = $('ul.report-tabs.sub_nav').find('li[content="sales"]');
            

            //hide/show on initialize
            noGenerateReport.show();
            showGenerateReport.hide();
            reportContent.hide();
            
            if ($().datetimepicker !== undefined) {
                var dateTimePickerOptions = {
                    format  : 'MMMM DD, YYYY',
                    pickTime: false
                };

                //set date picker options
                datepickerDateFrom.datetimepicker(dateTimePickerOptions);
                datepickerDateTo.datetimepicker(dateTimePickerOptions);

                //change min date of date to not allow to pass 
                datepickerDateFrom.on("dp.change", function (e) {
                    datepickerDateTo.data('DateTimePicker').setMinDate(e.date);
                });

                datepickerDateTo.on("dp.change", function (e) {
                    datepickerDateFrom.data('DateTimePicker').setMaxDate(e.date);
                });
            }

            //sales report li click
            salesReportLink.on('click', function(){
                //show no generate report, clear input values
                noGenerateReport.show();
                showGenerateReport.hide();
                reportContent.hide();
                admin.dst_reports.show_spinner(salesReportXlsBtn, true);
                datepickerDateFrom.find('input[name="sales_report_date_from"]').val('');
                datepickerDateTo.find('input[name="sales_report_date_to"]').val('');
                selectStoreDropdownDiv.find('input[name="sales_summary_stores"]').attr('int-value', 0).val('Show All Stores')
                selectProvinceDropdownDiv.find('input[name="sales_summary_provinces"]').attr('int-value', 0).val('Show All Provinces')
                selectCCenterDropdownDiv.find('input[name="sales_summary_callcenters"]').attr('int-value', 0).val('Show All Call Centers')
                selectGroupDropdownDiv.find('input[name="sales_summary_group"]').attr('data-value', 'date').val('Date')
                selectTransactionDropdownDiv.find('input[name="sales_summary_transactions"]').attr('data-value', 'Show All Transactions').val('Show All Transactions');
            });

            //generate by date button
            generateByDateBtn.on('click', function(){
                var dateFromInput = generateByDateDiv.find('input[name="sales_report_date_from"]')
                var dateToInput = generateByDateDiv.find('input[name="sales_report_date_to"]')

                if(dateFromInput.val() == '' || dateToInput.val() == '')
                {
                    //show no generate report
                    noGenerateReport.show();
                    showGenerateReport.hide();
                    reportContent.hide();
                    selectStoreDropdownDiv.find('input[name="sales_summary_stores"]').attr('int-value', 0).val('Show All Stores')
                    selectProvinceDropdownDiv.find('input[name="sales_summary_provinces"]').attr('int-value', 0).val('Show All Provinces')
                    selectCCenterDropdownDiv.find('input[name="sales_summary_callcenters"]').attr('int-value', 0).val('Show All Call Centers')
                    selectGroupDropdownDiv.find('input[name="sales_summary_group"]').attr('data-value', 'date').val('Date')
                    selectTransactionDropdownDiv.find('input[name="sales_summary_transactions"]').attr('data-value', 'Show All Transactions').val('Show All Transactions');
                    admin.dst_reports.show_spinner(salesReportXlsBtn, true);
                }
                else
                {
                    var dateFrom = moment(dateFromInput.val()),
                        dateTo = moment(dateToInput.val()),
                        dateFromValue = moment(dateFrom).format("YYYY-MM-DD HH:mm:ss"),
                        dateToValue = moment(dateTo).format("YYYY-MM-DD");
                        dateToValue = moment(dateToValue+" 23:59:59").format("YYYY-MM-DD HH:mm:ss");

                    if(dateTo.diff(dateFrom, 'days') < 0)
                    {
                        noGenerateReport.show();
                        showGenerateReport.hide();
                        reportContent.hide();
                        selectStoreDropdownDiv.find('input[name="sales_summary_stores"]').attr('int-value', 0).val('Show All Stores')
                        selectProvinceDropdownDiv.find('input[name="sales_summary_provinces"]').attr('int-value', 0).val('Show All Provinces')
                        selectCCenterDropdownDiv.find('input[name="sales_summary_callcenters"]').attr('int-value', 0).val('Show All Call Centers')
                        selectGroupDropdownDiv.find('input[name="sales_summary_group"]').attr('data-value', 'date').val('Date')
                        selectTransactionDropdownDiv.find('input[name="sales_summary_transactions"]').attr('data-value', 'Show All Transactions').val('Show All Transactions');
                        //date to should be larger than the date from
                        console.log('date to should be larger than the date from')
                        admin.dst_reports.show_spinner(salesReportXlsBtn, true);
                    }
                    else
                    {
                        var transType = selectTransactionDropdownDiv.find('input[name="sales_summary_transactions"]').attr('data-value');
                        //ajax call
                        var oParams = {
                            "params": {
                                "where": [
                                    {
                                        "field"   : "o.order_status",
                                        "operator": "=",
                                        "value"   : 19
                                    }
                                ],
                                "date_from" : dateFromValue,
                                "date_to"   : dateToValue,
                                "limit"     : getOrderLimit,
                                "period"    : "Daily",
                                "sbu_id"    : 1,
                                "transaction_type" : transType
                            },
                            "user_id" : iConstantUserId
                        }

                        var iStoreID = selectStoreDropdownDiv.find('input[name="sales_summary_stores"]').attr('int-value');
                        if (iStoreID > 0) {
                            oParams.params.where.push({
                                'field'   : 'o.store_id',
                                'operator': '=',
                                'value'   : iStoreID
                            });
                            oParams.params.store_id = iStoreID;
                        }

                        var iProvinceID = selectProvinceDropdownDiv.find('input[name="sales_summary_provinces"]').attr('int-value');
                        if (iProvinceID > 0) {
                            oParams.params.where.push({
                                'field'   : 'o.province_id',
                                'operator': '=',
                                'value'   : iProvinceID
                            });
                        }

                        var iCCenterID = selectCCenterDropdownDiv.find('input[name="sales_summary_callcenters"]').attr('int-value');
                        if (iCCenterID > 0) {
                            oParams.params.where.push({
                                'field'   : 'cu.callcenter_id',
                                'operator': '=',
                                'value'   : iCCenterID
                            });
                        }

                        var sGroupBy = selectGroupDropdownDiv.find('input[name="sales_summary_group"]').attr('data-value');
                        if (sGroupBy == 'date') {
                            oParams.params.grouped_by = 'date';
                            admin.dst_reports.show_spinner(generateByDateBtn, true);
                            admin.sales_report.get_orders_by_date(oParams);
                        }
                        else
                        {
                            oParams.params.grouped_by = 'stores';
                            admin.dst_reports.show_spinner(generateByDateBtn, true);
                            admin.sales_report.get_orders_by_store(oParams)
                        }
                    }
                }

            });

            //generate by period button
            generateByPeriodBtn.on('click', function(){
                var dateFromInput = generateByDateDiv.find('input[name="sales_report_date_from"]')
                var dateToInput = generateByDateDiv.find('input[name="sales_report_date_to"]')

                if(dateFromInput.val() == '' || dateToInput.val() == '')
                {
                    //show no generate report
                    noGenerateReport.show();
                    showGenerateReport.hide();
                    reportContent.hide();
                    selectStoreDropdownDiv.find('input[name="sales_summary_stores"]').attr('int-value', 0).val('Show All Stores')
                    selectProvinceDropdownDiv.find('input[name="sales_summary_provinces"]').attr('int-value', 0).val('Show All Provinces')
                    selectCCenterDropdownDiv.find('input[name="sales_summary_callcenters"]').attr('int-value', 0).val('Show All Call Centers')
                    selectGroupDropdownDiv.find('input[name="sales_summary_group"]').attr('data-value', 'date').val('Date')
                    selectTransactionDropdownDiv.find('input[name="sales_summary_transactions"]').attr('data-value', 'Show All Transactions').val('Show All Transactions');
                    admin.dst_reports.show_spinner(salesReportXlsBtn, true);
                }
                else
                {
                    var dateFrom = moment(dateFromInput.val()),
                        dateTo = moment(dateToInput.val()),
                        dateFromValue = moment(dateFrom).format("YYYY-MM-DD HH:mm:ss"),
                        dateToValue = moment(dateTo).format("YYYY-MM-DD");
                        dateToValue = moment(dateToValue+" 23:59:59").format("YYYY-MM-DD HH:mm:ss");

                    if(dateTo.diff(dateFrom, 'days') < 0)
                    {
                        noGenerateReport.show();
                        showGenerateReport.hide();
                        reportContent.hide();
                        selectStoreDropdownDiv.find('input[name="sales_summary_stores"]').attr('int-value', 0).val('Show All Stores')
                        selectProvinceDropdownDiv.find('input[name="sales_summary_provinces"]').attr('int-value', 0).val('Show All Provinces')
                        selectCCenterDropdownDiv.find('input[name="sales_summary_callcenters"]').attr('int-value', 0).val('Show All Call Centers')
                        selectGroupDropdownDiv.find('input[name="sales_summary_group"]').attr('data-value', 'date').val('Date')
                        selectTransactionDropdownDiv.find('input[name="sales_summary_transactions"]').attr('data-value', 'Show All Transactions').val('Show All Transactions');
                        //date to should be larger than the date from
                        console.log('date to should be larger than the date from');
                        admin.dst_reports.show_spinner(salesReportXlsBtn, true);
                    }
                    else
                    {
                        var transType = selectTransactionDropdownDiv.find('input[name="sales_summary_transactions"]').attr('data-value');
                        //ajax call
                        var oParams = {
                            "params": {
                                "where": [
                                    {
                                        "field"   : "o.order_status",
                                        "operator": "=",
                                        "value"   : 19
                                    }
                                ],
                                "date_from" : dateFromValue,
                                "date_to"   : dateToValue,
                                "limit"     : getOrderLimit,
                                "period"    : selectPeriodDropdownDiv.find('input[name="period"]').attr('value'),
                                "sbu_id"    : 1,
                                "transaction_type" : transType
                            },
                            "user_id" : iConstantUserId

                        }

                        var iStoreID = selectStoreDropdownDiv.find('input[name="sales_summary_stores"]').attr('int-value');
                        if (iStoreID > 0) {
                            oParams.params.where.push({
                                'field'   : 'o.store_id',
                                'operator': '=',
                                'value'   : iStoreID
                            });
                            oParams.params.store_id = iStoreID;
                        }

                        var iProvinceID = selectProvinceDropdownDiv.find('input[name="sales_summary_provinces"]').attr('int-value');
                        if (iProvinceID > 0) {
                            oParams.params.where.push({
                                'field'   : 'o.province_id',
                                'operator': '=',
                                'value'   : iProvinceID
                            });
                        }

                        var iCCenterID = selectCCenterDropdownDiv.find('input[name="sales_summary_callcenters"]').attr('int-value');
                        if (iCCenterID > 0) {
                            oParams.params.where.push({
                                'field'   : 'cu.callcenter_id',
                                'operator': '=',
                                'value'   : iCCenterID
                            });
                        }

                        var sGroupBy = selectGroupDropdownDiv.find('input[name="sales_summary_group"]').attr('data-value');
                        if (sGroupBy == 'date') {
                            oParams.params.grouped_by = 'date';
                            admin.dst_reports.show_spinner(generateByDateBtn, true);
                            admin.sales_report.get_orders_by_date(oParams);
                        }
                        else
                        {
                            oParams.params.grouped_by = 'stores';
                            admin.dst_reports.show_spinner(generateByDateBtn, true);
                            admin.sales_report.get_orders_by_store(oParams)
                        }
                    }
                }
            });

            //generate excel button
            salesReportXlsBtn.on('click', function() {
                var salesReportTable = $('section.content').find('div#sales_reports_content table#sales_report_table'),
                    salesReportXls = $('section.content').find('div#sales_reports_content table#sales_report_table_hidden');

                var salesReportTableStore = $('section.content').find('div#sales_reports_content table#sales_report_table_store'),
                    salesReportXlsStore = $('section.content').find('div#sales_reports_content table#sales_report_table_store_hidden');

                admin.dst_reports.show_spinner(salesReportXlsBtn, true);
                
                if(salesReportTable.is(':visible'))
                {
                    salesReportXls.table2excel({
                        exclude: ".notthis",
                        name: "Sales Report",
                        filename: "Sales_Summary_Report"/*+moment().unix('X')*/
                    });
                }
                else
                {
                    salesReportXlsStore.table2excel({
                        exclude: ".notthis",
                        name: "Sales Report",
                        filename: "Sales_Summary_Report"/*+moment().unix('X')*/
                    });
                }

                setTimeout(function(){
                    admin.dst_reports.show_spinner(salesReportXlsBtn, false);
                }, 3000) 
            });

            //period dropdown
            selectPeriodDropdownDiv.find('div.option').on('click', function(){
                var uiThis = $(this);
                selectPeriodDropdownDiv.find('input[name="period"]').attr('value', uiThis.attr('data-value'))
            });

            //store dropdown
            selectStoreDropdownDiv.on('click', 'div.option', function(){
                var uiThis = $(this);
                selectStoreDropdownDiv.find('input[name="sales_summary_stores"]').attr('int-value', uiThis.attr('int-value'))

                // admin.sales_report.trigger_ajax_on_filter_click();
            });

            //province dropdown
            selectProvinceDropdownDiv.on('click', 'div.option', function(){
                var uiThis = $(this);
                selectProvinceDropdownDiv.find('input[name="sales_summary_provinces"]').attr('int-value', uiThis.attr('int-value'))

                // admin.sales_report.trigger_ajax_on_filter_click();
            });

            //ccenter dropdown
            selectCCenterDropdownDiv.on('click', 'div.option', function(){
                var uiThis = $(this);
                selectCCenterDropdownDiv.find('input[name="sales_summary_callcenters"]').attr('int-value', uiThis.attr('int-value'))

                // admin.sales_report.trigger_ajax_on_filter_click();
            });

            //transactions dropdown
            selectTransactionDropdownDiv.on('click', 'div.option', function(){
                var uiThis = $(this);
                selectTransactionDropdownDiv.find('input[name="sales_summary_transactions"]').attr('data-value', uiThis.attr('data-value'));

                // admin.sales_report.trigger_ajax_on_filter_click();
            });

            //group dropdown
            selectGroupDropdownDiv.on('click', 'div.option', function(){
                var uiThis = $(this);
                selectGroupDropdownDiv.find('input[name="sales_summary_group"]').attr('data-value', uiThis.attr('data-value'))

                // admin.sales_report.trigger_ajax_on_filter_click();
            });
        },

        'trigger_ajax_on_filter_click': function(){
            var dateFromInput = generateByDateDiv.find('input[name="sales_report_date_from"]')
            var dateToInput = generateByDateDiv.find('input[name="sales_report_date_to"]')
            var dateFrom = moment(dateFromInput.val()),
                dateTo = moment(dateToInput.val()),
                dateFromValue = moment(dateFrom).format("YYYY-MM-DD HH:mm:ss"),
                dateToValue = moment(dateTo).format("YYYY-MM-DD");
                dateToValue = moment(dateToValue+" 23:59:59").format("YYYY-MM-DD HH:mm:ss");
            var transType = selectTransactionDropdownDiv.find('input[name="sales_summary_transactions"]').attr('data-value');

            if(dateTo.diff(dateFrom, 'days') < 0)
            {
                noGenerateReport.show();
                showGenerateReport.hide();
                reportContent.hide();
                selectStoreDropdownDiv.find('input[name="sales_summary_stores"]').attr('int-value', 0).val('Show All Stores')
                selectProvinceDropdownDiv.find('input[name="sales_summary_provinces"]').attr('int-value', 0).val('Show All Provinces')
                selectCCenterDropdownDiv.find('input[name="sales_summary_callcenters"]').attr('int-value', 0).val('Show All Call Centers')
                selectGroupDropdownDiv.find('input[name="sales_summary_group"]').attr('data-value', 'date').val('Date')
                selectTransactionDropdownDiv.find('input[name="sales_summary_transactions"]').attr('data-value', 'Show All Transactions').val('Show All Transactions');
                //date to should be larger than the date from
                console.log('date to should be larger than the date from')
                admin.dst_reports.show_spinner(salesReportXlsBtn, true);
            }
            else
            {
                //ajax call
                var oParams = {
                    "params": {
                        "where": [
                            {
                                "field"   : "o.order_status",
                                "operator": "=",
                                "value"   : 19
                            }
                        ],
                        "date_from" : dateFromValue,
                        "date_to"   : dateToValue,
                        "limit"     : getOrderLimit,
                        "period"    : "Daily",
                        "sbu_id"    : 1,
                        "transaction_type" : transType
                    }

                }

                var iStoreID = selectStoreDropdownDiv.find('input[name="sales_summary_stores"]').attr('int-value');
                if (iStoreID > 0) {
                    oParams.params.where.push({
                        'field'   : 'o.store_id',
                        'operator': '=',
                        'value'   : iStoreID
                    });
                    oParams.params.store_id = iStoreID;
                }

                var iProvinceID = selectProvinceDropdownDiv.find('input[name="sales_summary_provinces"]').attr('int-value');
                if (iProvinceID > 0) {
                    oParams.params.where.push({
                        'field'   : 'o.province_id',
                        'operator': '=',
                        'value'   : iProvinceID
                    });
                }

                var iCCenterID = selectCCenterDropdownDiv.find('input[name="sales_summary_callcenters"]').attr('int-value');
                if (iCCenterID > 0) {
                    oParams.params.where.push({
                        'field'   : 'cu.callcenter_id',
                        'operator': '=',
                        'value'   : iCCenterID
                    });
                }

                var sGroupBy = selectGroupDropdownDiv.find('input[name="sales_summary_group"]').attr('data-value');
                if (sGroupBy == 'date') {
                    oParams.params.grouped_by = 'date';
                    admin.dst_reports.show_spinner(generateByDateBtn, true);
                    admin.sales_report.get_orders_by_date(oParams);
                }
                else
                {
                    oParams.params.grouped_by = 'stores';
                    admin.dst_reports.show_spinner(generateByDateBtn, true);
                    admin.sales_report.get_orders_by_store(oParams)
                }
            }
        },

        'get_orders_by_date' :function(oParams)
        {
            var oAjaxConfig = {
                "type"   : "GET",
                "data"   : oParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : admin.config('url.api.jfc.reports') + "/reports/get_sales_report",
                "success": function (data) {
                    // console.log(data)
                    admin.dst_reports.show_spinner(generateByDateBtn, false);
                    admin.dst_reports.show_spinner(generateByPeriodBtn, false);
                    if(count(data.order_data) > 0)
                    {
                        //enable download excel button
                        admin.dst_reports.show_spinner(salesReportXlsBtn, false);
                        //manipulate template
                        admin.sales_report.manipulate_template(data, oParams);
                        //show table
                        noGenerateReport.hide();
                        showGenerateReport.show();
                        reportContent.find('table#sales_report_table').removeClass('hidden');
                        reportContent.find('table#sales_report_table_store').addClass('hidden');
                        reportContent.show();
                    }
                    else
                    {
                        //hide table
                        noGenerateReport.show();
                        showGenerateReport.hide();
                        reportContent.hide();
                        admin.dst_reports.show_spinner(salesReportXlsBtn, true);
                        selectStoreDropdownDiv.find('input[name="sales_summary_stores"]').attr('int-value', 0).val('Show All Stores')
                        selectProvinceDropdownDiv.find('input[name="sales_summary_provinces"]').attr('int-value', 0).val('Show All Provinces')
                        selectCCenterDropdownDiv.find('input[name="sales_summary_callcenters"]').attr('int-value', 0).val('Show All Call Centers')
                        selectGroupDropdownDiv.find('input[name="sales_summary_group"]').attr('data-value', 'date').val('Date')
                        selectTransactionDropdownDiv.find('input[name="sales_summary_transactions"]').attr('data-value', 'Show All Transactions').val('Show All Transactions');
                    }
                }
            }   
            admin.sales_report.ajax(oAjaxConfig);
        },

        'get_orders_by_store' :function(oParams)
        {
            var oAjaxConfig = {
                "type"   : "GET",
                "data"   : oParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : admin.config('url.api.jfc.reports') + "/reports/get_sales_report",
                "success": function (data) {
                    // console.log(data)
                    admin.dst_reports.show_spinner(generateByDateBtn, false);
                    admin.dst_reports.show_spinner(generateByPeriodBtn, false);
                    if(count(data.store_data) > 0)
                    {
                        //enable download excel button
                        admin.dst_reports.show_spinner(salesReportXlsBtn, false);
                        //manipulate template
                        admin.sales_report.manipulate_template_store(data, oParams);
                        //show table
                        noGenerateReport.hide();
                        showGenerateReport.show();
                        reportContent.find('table#sales_report_table').addClass('hidden');
                        reportContent.find('table#sales_report_table_store').removeClass('hidden');
                        reportContent.show();
                    }
                    else
                    {
                        //hide table
                        noGenerateReport.show();
                        showGenerateReport.hide();
                        reportContent.hide();
                        admin.dst_reports.show_spinner(salesReportXlsBtn, true);
                        selectStoreDropdownDiv.find('input[name="sales_summary_stores"]').attr('int-value', 0).val('Show All Stores')
                        selectProvinceDropdownDiv.find('input[name="sales_summary_provinces"]').attr('int-value', 0).val('Show All Provinces')
                        selectCCenterDropdownDiv.find('input[name="sales_summary_callcenters"]').attr('int-value', 0).val('Show All Call Centers')
                        selectGroupDropdownDiv.find('input[name="sales_summary_group"]').attr('data-value', 'date').val('Date')
                        selectTransactionDropdownDiv.find('input[name="sales_summary_transactions"]').attr('data-value', 'Show All Transactions').val('Show All Transactions');
                    }
                }
            }   
            admin.sales_report.ajax(oAjaxConfig);
        },

        'manipulate_template': function(oData, oParams){
            if(typeof(oData) != 'undefined' && typeof(oParams) != 'undefined')
            {
                var dateFrom = moment(oData.date_from, "YYYY-MM-DD").format('MMMM D, YYYY'),
                    dateTo = moment(oData.date_to, "YYYY-MM-DD").format('MMMM D, YYYY'),
                    uiContentContainer = $('tbody.sales_report_tr_container'),
                    uiHiddenBodyContainer = $('tbody.sales_report_tr_container_hidden'),
                    uiHiddenFooterContainer = $('tfoot.hidden_footer'),
                    ctr = 0;

                    var totalCost = parseFloat(oData.total_cost).toFixed(2);
                    var totalBill = parseFloat(oData.total_bill).toFixed(2);
                        totalCost = admin.sales_report.numberWithCommas(totalCost);
                        totalBill = admin.sales_report.numberWithCommas(totalBill);

                //supply data to rounded boxes
                showGenerateReport.find('p[data-label="date_from"]').text(dateFrom+' to ');
                showGenerateReport.find('p[data-label="date_to"]').text(dateTo);
                showGenerateReport.find('p[data-label="total_sales"]').text(totalCost +' PHP');
                showGenerateReport.find('p[data-label="food_sales"]').text(totalBill +' PHP');
                showGenerateReport.find('p[data-label="transaction_count"]').text(oData.total_order_count);
                
                //remove existing tr
                uiContentContainer.find('tr.sales_report_tr:not(.template)').remove();
                uiHiddenBodyContainer.find('tr.sales_report_tr_hidden:not(.template)').remove();
                uiHiddenFooterContainer.find('tr.sales_report_tr_total_hidden:not(.template)').remove();
                
                uiContentContainer.find('tr.sales_report_tr_total:not(.template)').remove();

                //loop order_data for display
                $.each(oData.order_data, function (key, value){
                    //viewable table
                    var trTemplate = $('tr.sales_report_tr.template').clone().removeClass('template notthis');
                    //hidden table template for xls download
                    var trTemplateHidden = $('tr.sales_report_tr_hidden.template').clone().removeClass('template notthis').addClass('hidden');
                    //no.
                    ctr++;
                    //date total cost
                    var dateTotalCost = parseFloat(value.date_total_cost).toFixed(2);
                    var dateTotalBill = parseFloat(value.date_total_bill).toFixed(2);
                        dateTotalCost = admin.sales_report.numberWithCommas(dateTotalCost);
                        dateTotalBill = admin.sales_report.numberWithCommas(dateTotalBill);

                    //change date text
                    if(oData.period == "Daily")
                    {
                        var sales_report_date = moment(value.date_order_date, "YYYY-MM-DD").format('MMMM D, YYYY');
                        trTemplate.find('td[data-label="sales_report_date"]').text(sales_report_date+" ");
                        trTemplateHidden.find('td[data-label="sales_report_date"]').text(moment(sales_report_date).format('MMMM DD YYYY'));
                    }
                    else if(oData.period == "Monthly")
                    {
                        var sales_report_date_start = moment(value.date_order_date, "YYYY-MM").format('MMMM D, YYYY');
                        var sales_report_date_end = moment(sales_report_date_start).endOf('month');
                        sales_report_date_end = sales_report_date_end.format('MMMM D, YYYY')

                        if(moment(sales_report_date_end).diff(moment(dateTo), 'days') > parseInt(0))
                        {
                            if(moment(sales_report_date_start).diff(moment(dateFrom), 'days') > parseInt(0))
                            {
                                trTemplate.find('td[data-label="sales_report_date"]').text(sales_report_date_start+ ' to ' +dateTo);
                                trTemplateHidden.find('td[data-label="sales_report_date"]').text(sales_report_date_start+ ' to ' +dateTo);
                            }
                            else
                            {
                                trTemplate.find('td[data-label="sales_report_date"]').text(dateFrom+ ' to ' +dateTo);
                                trTemplateHidden.find('td[data-label="sales_report_date"]').text(dateFrom+ ' to ' +dateTo);
                            }
                        }
                        else
                        {
                            if(moment(sales_report_date_start).diff(moment(dateFrom), 'days') > parseInt(0))
                            {
                                trTemplate.find('td[data-label="sales_report_date"]').text(sales_report_date_start+ ' to ' +sales_report_date_end);
                                trTemplateHidden.find('td[data-label="sales_report_date"]').text(sales_report_date_start+ ' to ' +sales_report_date_end);
                            }
                            else
                            {
                                trTemplate.find('td[data-label="sales_report_date"]').text(dateFrom+ ' to ' +sales_report_date_end);
                                trTemplateHidden.find('td[data-label="sales_report_date"]').text(dateFrom+ ' to ' +sales_report_date_end);
                            }
                        }
                    }
                    else if(oData.period == "Weekly")
                    {
                        // var sales_report_date_start = moment(value.date_order_date).startOf('week').format('MMMM D, YYYY'),
                            // sales_report_date_end = moment(sales_report_date_start).endOf('week').format('MMMM D, YYYY');

                        var sales_report_date_start = moment(value.date_order_date_start, "MM-DD-YY").format('MMMM D, YYYY'),
                            sales_report_date_end = moment(value.date_order_date_end, "MM-DD-YY").format('MMMM D, YYYY');

                        if(moment(sales_report_date_end).diff(moment(dateTo), 'days') > parseInt(0))
                        {
                            if(moment(sales_report_date_start).diff(moment(dateFrom), 'days') > parseInt(0))
                            {
                                trTemplate.find('td[data-label="sales_report_date"]').text(sales_report_date_start+ ' to ' +dateTo);
                                trTemplateHidden.find('td[data-label="sales_report_date"]').text(sales_report_date_start+ ' to ' +dateTo);
                            }
                            else
                            {
                                trTemplate.find('td[data-label="sales_report_date"]').text(dateFrom+ ' to ' +dateTo);
                                trTemplateHidden.find('td[data-label="sales_report_date"]').text(dateFrom+ ' to ' +dateTo);
                            }
                        }
                        else
                        {
                            if(moment(sales_report_date_start).diff(moment(dateFrom), 'days') > parseInt(0))
                            {
                                trTemplate.find('td[data-label="sales_report_date"]').text(sales_report_date_start+ ' to ' +sales_report_date_end);
                                trTemplateHidden.find('td[data-label="sales_report_date"]').text(sales_report_date_start+ ' to ' +sales_report_date_end);
                            }
                            else
                            {
                                trTemplate.find('td[data-label="sales_report_date"]').text(dateFrom+ ' to ' +sales_report_date_end);
                                trTemplateHidden.find('td[data-label="sales_report_date"]').text(dateFrom+ ' to ' +sales_report_date_end);
                            }
                        }

                    }

                    //number text
                    trTemplate.find('td[data-label="sales_report_number"]').text(ctr);
                    trTemplateHidden.find('td[data-label="sales_report_number"]').text(ctr);

                    //total sales text
                    trTemplate.find('td[data-label="sales_report_total_cost"]').text(dateTotalCost +' PHP');
                    trTemplateHidden.find('td[data-label="sales_report_total_cost"]').text(dateTotalCost +' PHP');

                    //food sales text
                    trTemplate.find('td[data-label="sales_report_total_bill"]').text(dateTotalBill +' PHP');
                    trTemplateHidden.find('td[data-label="sales_report_total_bill"]').text(dateTotalBill +' PHP');

                    //order count text
                    trTemplate.find('td[data-label="sales_report_count"]').text(value.date_order_count);
                    trTemplateHidden.find('td[data-label="sales_report_count"]').text(value.date_order_count);

                    //append tables
                    uiContentContainer.append(trTemplate);
                    // append to hidden tbody table
                    uiHiddenBodyContainer.append(trTemplateHidden);
                })

                //hidden total table template
                var trTemplateTotalHidden = $('tr.sales_report_tr_total_hidden.template').clone().removeClass('template notthis').addClass('hidden');

                //hidden total tr to be uploaded as xls
                trTemplateTotalHidden.find('th[data-label="date_range"]').text(dateFrom+' to '+dateTo);
                trTemplateTotalHidden.find('th[data-label="total_cost"]').text(totalCost +' PHP');
                trTemplateTotalHidden.find('th[data-label="total_bill"]').text(totalBill +' PHP');
                trTemplateTotalHidden.find('th[data-label="total_count"]').text(oData.total_order_count);
                //append to footer
                uiHiddenFooterContainer.append(trTemplateTotalHidden);

                //total net sales at the bottom
                var trTemplateTotal = $('tr.sales_report_tr_total.template').clone().removeClass('template notthis');

                trTemplateTotal.find('th[data-label="total_cost"]').text(totalCost +' PHP');
                trTemplateTotal.find('th[data-label="total_bill"]').text(totalBill +' PHP');
                trTemplateTotal.find('th[data-label="total_count"]').text(oData.total_order_count);
                //append to footer
                uiContentContainer.append(trTemplateTotal);
            }
        },

        'manipulate_template_store': function(oData, oParams){
            if(typeof(oData) != 'undefined' && typeof(oParams) != 'undefined')  
            {
                var dateFrom = moment(oData.date_from, "YYYY-MM-DD").format('MMMM D, YYYY'),
                    dateTo = moment(oData.date_to, "YYYY-MM-DD").format('MMMM D, YYYY'),
                    uiContentContainer = $('tbody.sales_report_tr_container_store'),
                    uiHiddenBodyContainer = $('tbody.sales_report_tr_container_store_hidden'),
                    // uiHiddenFooterContainer = $('tfoot.hidden_footer_store'),
                    ctr = 0;

                    var totalCost = parseFloat(oData.total_cost).toFixed(2);
                    var totalBill = parseFloat(oData.total_bill).toFixed(2);
                        totalCost = admin.sales_report.numberWithCommas(totalCost);
                        totalBill = admin.sales_report.numberWithCommas(totalBill);

                //supply data to rounded boxes
                showGenerateReport.find('p[data-label="date_from"]').text(dateFrom+' to ');
                showGenerateReport.find('p[data-label="date_to"]').text(dateTo);
                showGenerateReport.find('p[data-label="total_sales"]').text(totalCost +' PHP');
                showGenerateReport.find('p[data-label="food_sales"]').text(totalBill +' PHP');
                showGenerateReport.find('p[data-label="transaction_count"]').text(oData.total_order_count);
                
                //remove existing tr
                uiContentContainer.find('tr.sales_report_tr_store:not(.template)').remove();
                uiHiddenBodyContainer.find('tr.sales_report_tr_store_hidden:not(.template)').remove();

                uiContentContainer.find('tr.sales_report_tr_total_store:not(.template)').remove();
                uiHiddenBodyContainer.find('tr.sales_report_tr_total_store:not(.template)').remove();
                // uiHiddenFooterContainer.find('tr.sales_report_tr_total_hidden:not(.template)').remove();
                
                //loop store order_data for display\

                $.each(oData.store_data, function (key, value){
                    //viewable table
                    var trTemplate = $('tr.sales_report_tr_store.template').clone().removeClass('template notthis');
                    //hidden table template for xls download
                    var trTemplateHidden = $('tr.sales_report_tr_store_hidden.template').clone().removeClass('template notthis').addClass('hidden');
                    //no.
                    ctr++;
                    //date total cost
                    var totalSales = parseFloat(value.total_sales).toFixed(2);
                    var foodSales = parseFloat(value.food_sales).toFixed(2);
                        totalSales = admin.sales_report.numberWithCommas(totalSales);
                        foodSales = admin.sales_report.numberWithCommas(foodSales);

                    //number text
                    trTemplate.find('td[data-label="sales_report_number_store"]').text(ctr);
                    trTemplateHidden.find('td[data-label="sales_report_number_store"]').text(ctr);

                    //dates for xls
                    trTemplateHidden.find('td[data-label="sales_report_date_store"]').text(dateFrom+' to '+dateTo);

                    //store name text
                    trTemplate.find('td[data-label="sales_report_name_store"]').text(value.name);
                    trTemplateHidden.find('td[data-label="sales_report_name_store"]').text(value.name);

                    //store code text
                    trTemplate.find('td[data-label="sales_report_code_store"]').text(value.code);
                    trTemplateHidden.find('td[data-label="sales_report_code_store"]').text(value.code);

                    //total sales text
                    trTemplate.find('td[data-label="sales_report_total_sales_store"]').text(totalSales +' PHP');
                    trTemplateHidden.find('td[data-label="sales_report_total_sales_store"]').text(totalSales +' PHP');

                    //food sales text
                    trTemplate.find('td[data-label="sales_report_food_sales_store"]').text(foodSales +' PHP');
                    trTemplateHidden.find('td[data-label="sales_report_food_sales_store"]').text(foodSales +' PHP');

                    //order count text
                    trTemplate.find('td[data-label="sales_report_count_store"]').text(value.order_count);
                    trTemplateHidden.find('td[data-label="sales_report_count_store"]').text(value.order_count);

                    //append tables
                    uiContentContainer.append(trTemplate);
                    // append to hidden tbody table
                    uiHiddenBodyContainer.append(trTemplateHidden);
                })

                // //hidden total table template
                // var trTemplateTotalHidden = $('tr.sales_report_tr_total_hidden.template').clone().removeClass('template notthis').addClass('hidden');

                // //hidden total tr to be uploaded as xls
                // trTemplateTotalHidden.find('th[data-label="date_range"]').text(dateFrom+' to '+dateTo);
                // trTemplateTotalHidden.find('th[data-label="total_cost"]').text(totalCost +' PHP');
                // trTemplateTotalHidden.find('th[data-label="total_bill"]').text(totalBill +' PHP');
                // trTemplateTotalHidden.find('th[data-label="total_count"]').text(oData.total_order_count);
                // //append to footer
                // uiHiddenFooterContainer.append(trTemplateTotalHidden);

                //total net sales at the bottom
                var trTemplateTotal = $('tr.sales_report_tr_total_store.template').clone().removeClass('template notthis');

                trTemplateTotal.find('th[data-label="total_cost"]').text(totalCost +' PHP');
                trTemplateTotal.find('th[data-label="total_bill"]').text(totalBill +' PHP');
                trTemplateTotal.find('th[data-label="total_count"]').text(oData.total_order_count);
                //append to footer
                uiContentContainer.append(trTemplateTotal);

                //total net sales at the bottom of xls
                var trTemplateTotalHidden = $('tr.sales_report_tr_total_store.template').clone().removeClass('template notthis').addClass('hidden');

                trTemplateTotalHidden.find('th[data-label="total_cost"]').text(totalCost +' PHP');
                trTemplateTotalHidden.find('th[data-label="total_bill"]').text(totalBill +' PHP');
                trTemplateTotalHidden.find('th[data-label="total_count"]').text(oData.total_order_count);
                //append to footer for xls
                uiHiddenBodyContainer.append(trTemplateTotalHidden);

                if(!oParams.params.hasOwnProperty('store_id'))
                {
                    var trTemplateHidden = $('tr.sales_report_tr_store_hidden.template').clone().removeClass('template notthis').addClass('hidden');
                    trTemplateHidden.find('td[data-label="sales_report_date_store"]').text(dateFrom+' to '+dateTo);
                    trTemplateHidden.find('td[data-label="sales_report_name_store"]').text('All Stores');
                    trTemplateHidden.find('td[data-label="sales_report_code_store"]').text('All Stores');
                    trTemplateHidden.find('td[data-label="sales_report_total_sales_store"]').text(totalCost +' PHP');
                    trTemplateHidden.find('td[data-label="sales_report_food_sales_store"]').text(totalBill +' PHP');
                    trTemplateHidden.find('td[data-label="sales_report_count_store"]').text(oData.total_order_count);
    
                    uiHiddenBodyContainer.find('tr:first').after(trTemplateHidden);
                }
            }
        },

        'ajax': function (oAjaxConfig) {
            if (typeof(oAjaxConfig) != 'undefined') {
                admin.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        'numberWithCommas': function(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

    }

}());

$(window).load(function () {
    "use strict";
    admin.sales_report.initialize();
});


