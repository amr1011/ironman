/**
 *  Product Unavailabilty Report Class
 *
 *
 *
 */
(function () {
    "use strict";

    var generateByDateBtn,
        generateByDateDiv,
        showGenerateReport,
        noGenerateReport,
        reportContent,
        datepickerDateFrom,
        datepickerDateTo,
        puReportTable,
        puReportXlsBtn,
        puReportLink,
        selectStoreDropdownDiv,
        selectProvinceDropdownDiv,
        getLimit = 9999999;

    CPlatform.prototype.product_unavailability_report = {

        'initialize' : function () {

            generateByDateBtn           = $('section[header-content="prod_unavailability_reports"]').find('button.pu_generate_by_date');
            generateByDateDiv           = $('section[header-content="prod_unavailability_reports"]').find('div.pu_generate_by_date');
            showGenerateReport          = $('section[header-content="prod_unavailability_reports"]').find('div.show_generate_report');
            noGenerateReport            = $('section[header-content="prod_unavailability_reports"]').find('div.no_generate_report');
            reportContent               = $('div[content="prod_unavailability_reports"]');
            datepickerDateFrom          = $('section[header-content="prod_unavailability_reports"]').find('div#pu-report-date-from');
            datepickerDateTo            = $('section[header-content="prod_unavailability_reports"]').find('div#pu-report-date-to');
            puReportTable               = $('div[content="prod_unavailability_reports"]').find('table#pu_report_table');
            puReportXlsBtn              = $('section[header-content="prod_unavailability_reports"]').find('button.pu_report_xls_button');
            puReportLink                = $('div[content="reports"]').find('li[content="prod_unavailability_reports"]');
            selectStoreDropdownDiv      = $('section[header-content="prod_unavailability_reports"]').find('div.pu_store_selection');;
            selectProvinceDropdownDiv   = $('section[header-content="prod_unavailability_reports"]').find('div.pu_province_selection');;

            //hide/show on initialize
            noGenerateReport.show();
            showGenerateReport.hide();
            reportContent.hide();
            
            if ($().datetimepicker !== undefined) {
                var dateTimePickerOptions = {
                    format  : 'MMMM DD, YYYY',
                    pickTime: false
                };

                //set date picker options
                datepickerDateFrom.datetimepicker(dateTimePickerOptions);
                datepickerDateTo.datetimepicker(dateTimePickerOptions);

                //change min date of date to not allow to pass 
                datepickerDateFrom.on("dp.change", function (e) {
                    datepickerDateTo.data('DateTimePicker').setMinDate(e.date);
                });

                datepickerDateTo.on("dp.change", function (e) {
                    datepickerDateFrom.data('DateTimePicker').setMaxDate(e.date);
                });
            }

            //prod unavailability report li click
            puReportLink.on('click', function(){
                //show no generate report, clear input values
                noGenerateReport.show();
                showGenerateReport.hide();
                reportContent.hide();
                // admin.contact_reports.show_spinner(puReportXlsBtn, true);
                puReportXlsBtn.prop('disabled', true);
                datepickerDateFrom.find('input[name="pu_report_date_from"]').val('');
                datepickerDateTo.find('input[name="pu_report_date_to"]').val('');

                selectStoreDropdownDiv.find('input[name="pu_stores"]').attr('int-value', 0).val('Show All Stores')
                selectProvinceDropdownDiv.find('input[name="pu_provinces"]').attr('int-value', 0).val('Show All Provinces')

                var dateTimePickerOptions = {
                    format  : 'MMMM DD, YYYY',
                    pickTime: false
                };

                //set date picker options
                datepickerDateFrom.datetimepicker(dateTimePickerOptions);
                datepickerDateTo.datetimepicker(dateTimePickerOptions);

                //change min date of date to not allow to pass 
                datepickerDateFrom.on("dp.change", function (e) {
                    datepickerDateTo.data('DateTimePicker').setMinDate(e.date);
                });

                datepickerDateTo.on("dp.change", function (e) {
                    datepickerDateFrom.data('DateTimePicker').setMaxDate(e.date);
                });
            });

            //generate by date button
            generateByDateBtn.on('click', function(){
                var dateFromInput = generateByDateDiv.find('input[name="pu_report_date_from"]')
                var dateToInput = generateByDateDiv.find('input[name="pu_report_date_to"]')

                if(dateFromInput.val() == '' || dateToInput.val() == '')
                {
                    //show no generate report
                    noGenerateReport.show();
                    showGenerateReport.hide();
                    reportContent.hide();
                    selectStoreDropdownDiv.find('input[name="pu_stores"]').attr('int-value', 0).val('Show All Stores')
                    selectProvinceDropdownDiv.find('input[name="pu_provinces"]').attr('int-value', 0).val('Show All Provinces')
                    // admin.contact_reports.show_spinner(puReportXlsBtn, true);
                    puReportXlsBtn.prop('disabled', true);
                }
                else
                {
                    var dateFrom = moment(dateFromInput.val()),
                        dateTo = moment(dateToInput.val()),
                        dateFromValue = moment(dateFrom).format("YYYY-MM-DD HH:mm:ss"),
                        dateToValue = moment(dateTo).format("YYYY-MM-DD");
                        dateToValue = moment(dateToValue+" 23:59:59").format("YYYY-MM-DD HH:mm:ss");

                    if(dateTo.diff(dateFrom, 'days') < 0)
                    {
                        noGenerateReport.show();
                        showGenerateReport.hide();
                        reportContent.hide();
                        selectStoreDropdownDiv.find('input[name="pu_stores"]').attr('int-value', 0).val('Show All Stores')
                        selectProvinceDropdownDiv.find('input[name="pu_provinces"]').attr('int-value', 0).val('Show All Provinces')
                        //date to should be larger than the date from
                        console.log('date to should be larger than the date from')
                        // admin.contact_reports.show_spinner(puReportXlsBtn, true);
                        puReportXlsBtn.prop('disabled', true);
                    }
                    else
                    {
                        //ajax call
                        var oParams = {
                            "params": {
                                "date_from" : dateFromValue,
                                "date_to"   : dateToValue,
                                "limit"     : getLimit,
                                "sbu_id"    : 1,
                                "where" : []
                            },
                            "user_id" : iConstantUserId
                        }

                        var iStoreID = selectStoreDropdownDiv.find('input[name="pu_stores"]').attr('int-value');
                        if (iStoreID > 0) {
                            oParams.params.where.push({
                                "field": "psuh.store_id",
                                "operator": "=",
                                "value": iStoreID
                            });
                            oParams.params.store_id = iStoreID
                        }

                        var iProvinceID = selectProvinceDropdownDiv.find('input[name="pu_provinces"]').attr('int-value');
                        if (iProvinceID > 0) {
                            oParams.params.where.push({
                                'field'   : 's.province',
                                'operator': '=',
                                'value'   : iProvinceID
                            });
                        }

                        admin.contact_reports.show_spinner(generateByDateBtn, true);
                        admin.product_unavailability_report.get_products_by_date(oParams);
                    }
                }

            });
            
            //generate excel button
            puReportXlsBtn.on('click', function() {
                var puReportTable = $('div[content="prod_unavailability_reports"]').find('table#pu_report_table'),
                    puReportXls = $('div[content="prod_unavailability_reports"]').find('table#pu_report_table_hidden');
                
                admin.contact_reports.show_spinner(puReportXlsBtn, true);
                if(puReportTable.is(':visible'))
                {
                    puReportXls.table2excel({
                        exclude: ".notthis",
                        name: "Product Unavailabilty Report",
                        filename: "Product_Unavailabilty_"+moment().unix('X')
                    });
                }
                setTimeout(function(){
                    admin.contact_reports.show_spinner(puReportXlsBtn, false);
                }, 3000) 
            });

            //store dropdown
            selectStoreDropdownDiv.on('click', 'div.option', function(){
                var uiThis = $(this);
                selectStoreDropdownDiv.find('input[name="pu_stores"]').attr('int-value', uiThis.attr('int-value'))

                // admin.product_unavailability_report.trigger_ajax_on_filter_click();
            });


            //province dropdown
            selectProvinceDropdownDiv.on('click', 'div.option', function(){
                var uiThis = $(this);
                selectProvinceDropdownDiv.find('input[name="pu_provinces"]').attr('int-value', uiThis.attr('int-value'))

                // admin.product_unavailability_report.trigger_ajax_on_filter_click();
            });

            reportContent.on('click', 'li.page', function() {
                // console.log('orayt')
                var uiThis = $(this);
                if(uiThis.hasClass('active') == false)
                {
                    uiThis.siblings('li.page').removeClass('page-active');
                    
                    var iStart = uiThis.attr('page-start');
                    var iEnd = uiThis.attr('page-end');
                    
                    var oData = cr8v.get_from_storage('prod_avail');
                    var oFilteredData = oData.filter(function( el,i) {
                            return i >= iStart &&  i < iEnd;
                    })

                    admin.product_unavailability_report.manipulate_template(oFilteredData, '');
                    reportContent.find('li.page[page-start="'+iStart+'"][page-end="'+iEnd+'"]').addClass('page-active');        
                }                   
            });
        },

        'trigger_ajax_on_filter_click': function(){
            var dateFromInput = generateByDateDiv.find('input[name="pu_report_date_from"]')
            var dateToInput = generateByDateDiv.find('input[name="pu_report_date_to"]')
            var dateFrom = moment(dateFromInput.val()),
                dateTo = moment(dateToInput.val()),
                dateFromValue = moment(dateFrom).format("YYYY-MM-DD HH:mm:ss"),
                dateToValue = moment(dateTo).format("YYYY-MM-DD");
                dateToValue = moment(dateToValue+" 23:59:59").format("YYYY-MM-DD HH:mm:ss");

            if(dateTo.diff(dateFrom, 'days') < 0)
            {
                noGenerateReport.show();
                showGenerateReport.hide();
                reportContent.hide();
                selectStoreDropdownDiv.find('input[name="pu_stores"]').attr('int-value', 0).val('Show All Stores')
                selectProvinceDropdownDiv.find('input[name="pu_provinces"]').attr('int-value', 0).val('Show All Provinces')
                //date to should be larger than the date from
                console.log('date to should be larger than the date from')
                // admin.contact_reports.show_spinner(puReportXlsBtn, true);
                puReportXlsBtn.prop('disabled', true);
            }
            else
            {
                //ajax call
                var oParams = {
                    "params": {
                        "date_from" : dateFromValue,
                        "date_to"   : dateToValue,
                        "limit"     : getLimit,
                        "sbu_id"    : 1,
                        "where" : []
                    },
                    "user_id" : iConstantUserId
                }

                var iStoreID = selectStoreDropdownDiv.find('input[name="pu_stores"]').attr('int-value');
                if (iStoreID > 0) {
                    oParams.params.where.push({
                        "field": "psuh.store_id",
                        "operator": "=",
                        "value": iStoreID
                    });
                    oParams.params.store_id = iStoreID
                }

                var iProvinceID = selectProvinceDropdownDiv.find('input[name="pu_provinces"]').attr('int-value');
                if (iProvinceID > 0) {
                    oParams.params.where.push({
                        'field'   : 's.province',
                        'operator': '=',
                        'value'   : iProvinceID
                    });
                }

                admin.contact_reports.show_spinner(generateByDateBtn, true);
                admin.product_unavailability_report.get_products_by_date(oParams);
            }
        },

        'get_products_by_date' :function(oParams)
        {
            var oAjaxConfig = {
                "type"   : "GET",
                "data"   : oParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : admin.config('url.api.jfc.reports') + "/reports/product_unavailability_report",
                "success": function (oProductData) {
                    // console.log(data)
                    admin.contact_reports.show_spinner(generateByDateBtn, false);
                    if(count(oProductData.data) > 0)
                    {
                        //manipulate template
                        // admin.product_unavailability_report.manipulate_template(oProductData.data, oParams);
                        //show table
                        noGenerateReport.hide();
                        showGenerateReport.show();
                        reportContent.show();
                        puReportTable.show();
                        
                        if(typeof(cr8v.get_from_storage('prod_avail')) != 'undefined')
                        {
                            cr8v.pop('prod_avail');
                        }

                        cr8v.add_to_storage('prod_avail', oProductData.data);

                        var oData = cr8v.get_from_storage('prod_avail');

                        //for xls
                        admin.product_unavailability_report.manipulate_template_xls(oData, '');
                        //enable download excel button
                        admin.contact_reports.show_spinner(puReportXlsBtn, false);

                        var iPageCount = Math.ceil(oData.length / 10);
                        var uiPaginationContainer = reportContent.find('ul#prod_paginations');
                        var sPaginationHtml = '<li class="page-prev"><i class="fa fa-angle-left"></i></li>';

                        var uiPaginationContainerHidden = reportContent.find('ul.prod-paganations-hidden');
                        var sPaginationHtmlhidden = '<li class="page-prev"><i class="fa fa-angle-left"></i></li>';

                        if(iPageCount > 0)
                        {
                            for (i = 0; i < parseInt(iPageCount); i++) {
                                var iPage = i + 1;
                                var iStart, iEnd;
                                if(i == 0)
                                {
                                    iStart = 0;
                                    iEnd = 10;
                                }
                                else
                                {
                                    iStart = i * 10;
                                    iEnd = ((i + 1) * 10);
                                }

                                sPaginationHtmlhidden += '<li class="page" page-start="'+iStart+'" page-end="'+iEnd+'" data-num="'+ iPage +'"><a href="javascript:void(0)">'+iPage+'</a></li>';
                                sPaginationHtml += '<li class="pages" page-start="'+iStart+'" page-end="'+iEnd+'" data-num="'+iPage+'" >'+iPage+'</li>';
                            }
                        }
                        else
                        {
                            sPaginationHtml+= '<li class="page page-active" page-start="0" page-end="10" data-num="1"><a href="javascript:void(0)">1</a></li>';
                        }

                        uiPaginationContainer.html('');
                        uiPaginationContainer.append(sPaginationHtml);
                        uiPaginationContainerHidden.html('');
                        uiPaginationContainerHidden.append(sPaginationHtmlhidden)

                        if($('#prod_paginations').data("twbs-pagination")){
                            $('#prod_paginations').twbsPagination('destroy');
                        }

                        $('#prod_paginations').twbsPagination({
                            totalPages: iPageCount,
                            visiblePages: 10,
                            onPageClick: function (event, page) {
                                $('li.page[data-num="'+page+'"').trigger('click');
                            }
                        });

                        oData = oData.filter(function(el, i) {
                            return i >= 0 && i < 10;
                        });
                        admin.product_unavailability_report.manipulate_template(oData, '');
                    }
                    else
                    {
                        //hide table
                        noGenerateReport.show();
                        showGenerateReport.hide();
                        reportContent.hide();
                        selectStoreDropdownDiv.find('input[name="pu_stores"]').attr('int-value', 0).val('Show All Stores')
                        selectProvinceDropdownDiv.find('input[name="pu_provinces"]').attr('int-value', 0).val('Show All Provinces')
                        // admin.contact_reports.show_spinner(puReportXlsBtn, true);
                        puReportXlsBtn.prop('disabled', true);
                    }
                }
            }   
            admin.product_unavailability_report.ajax(oAjaxConfig);
        },

        'manipulate_template': function(oProductData, oParams){
            if(typeof(oProductData) != 'undefined' && typeof(oParams) != 'undefined')
            {
                var uiContentContainer = $('tbody.pu_report_tr_container'),
                    uiHiddenContentContainer = $('tbody.pu_report_tr_container_hidden'),
                    uiClonedTrs = [],
                    uiClonedHiddenTrs = [],
                    uiItems = '',
                    uiItemsHidden = '',
                    ctr = 0;

                    uiContentContainer.find('tr.pu_report_tr:not(.template)').remove();
                    // uiHiddenContentContainer.find('tr.pu_report_tr_hidden:not(.template)').remove();
                    
                    uiContentContainer.find('tr.pu_report_tr_items:not(.template)').remove();
                    // uiHiddenContentContainer.find('tr.pu_report_tr_hidden_items:not(.template)').remove();

                //     $.each(oProductData.data, function (key, value){
                //         var trTemplate = $('tr.pu_report_tr.template').clone().removeClass('template notthis'),
                //             trTemplateHidden = $('tr.pu_report_tr_hidden.template').clone().removeClass('template notthis'),
                //             sStoreName = value.store_name,
                //             sProductInfo = value.product_info,
                //             sItems = (value.is_deleted == 1) ? "On" : "Off" ,
                //             sDateTime = moment(value.date_unavailabled);
                //         // var arrProductItems = value.core_product_items;
                //         var uiClonedTrsItems = [];
                //         var uiClonedHiddenTrsItems = [];
                        
                //         ctr++;

                //         // if(arrProductItems.length > 0 && arrProductItems != '' && arrProductItems !== null)
                //         // {
                //         //     $.each(arrProductItems, function (k, v){
                //         //         var trTemplateItems = $('tr.pu_report_tr_items.template').clone().removeClass('template notthis');
                //         //         var trTemplateHiddenItems = $('tr.pu_report_tr_hidden_items.template').clone().removeClass('template notthis');

                //         //         trTemplateItems.find('td[data-label="pu_report_product_items"]').text(v.product_name)
                //         //         trTemplateHiddenItems.find('td[data-label="pu_report_product_items"]').text(v.product_name)
                                
                //         //         uiClonedTrsItems.push(trTemplateItems)
                //         //         uiClonedHiddenTrsItems.push(trTemplateHiddenItems)
                //         //     })
                //         // }
                        
                //         trTemplate.find('td[data-label="pu_report_number"]').text(ctr);
                //         trTemplate.find('td[data-label="pu_report_store"]').text(sStoreName);
                //         // trTemplate.find('td[data-label="pu_report_type"]').text('type');
                //         trTemplate.find('td[data-label="pu_report_product_info"]').text(sProductInfo);
                //         trTemplate.find('td[data-label="pu_report_items"]').text(sItems);
                //         trTemplate.find('td[data-label="pu_report_date_time"]').text(sDateTime.format('MMMM DD, YYYY | hh:mm:ss A'));
                        
                //         uiClonedTrs.push(trTemplate);

                //         trTemplateHidden.find('td[data-label="pu_report_number"]').text(ctr);
                //         trTemplateHidden.find('td[data-label="pu_report_store"]').text(sStoreName);
                //         // trTemplateHidden.find('td[data-label="pu_report_type"]').text('type');
                //         trTemplateHidden.find('td[data-label="pu_report_product_info"]').text(sProductInfo);
                //         trTemplateHidden.find('td[data-label="pu_report_items"]').text(sItems);
                //         trTemplateHidden.find('td[data-label="pu_report_date_time"]').text(sDateTime.format('YYYY-MM-DD HH:mm:ss'));
                        
                //         uiClonedHiddenTrs.push(trTemplateHidden);
                        
                //         // uiContentContainer.append(trTemplate);
                //         // uiHiddenContentContainer.append(trTemplateHidden);

                //         // uiContentContainer.append(uiClonedTrsItems);
                //         // uiHiddenContentContainer.append(uiClonedHiddenTrsItems);
                //     });
                
                // uiContentContainer.append(uiClonedTrs);
                // uiHiddenContentContainer.append(uiClonedHiddenTrs);

                //delps pagination
                // var uiPaginationContainer = $('div[content="prod_unavailability_reports"]').find('div[pagination-container="true"]');
                //// var oProductData = oProductData.data;
                // function pageAction(oProductData){

                //     uiContentContainer.find('tr.pu_report_tr:not(.template)').remove();
                //     uiHiddenContentContainer.find('tr.pu_report_tr_hidden:not(.template)').remove();
                    
                //     uiContentContainer.find('tr.pu_report_tr_items:not(.template)').remove();
                //     uiHiddenContentContainer.find('tr.pu_report_tr_hidden_items:not(.template)').remove();

                //     // var uiClonedTrsItems = [];
                //     // var uiClonedHiddenTrsItems = [];

                //     for(var i in oProductData){
                //         var trTemplate = $('tr.pu_report_tr.template').clone().removeClass('template notthis'),
                //             trTemplateHidden = $('tr.pu_report_tr_hidden.template').clone().removeClass('template notthis'),
                //             sStoreName = oProductData[i].store_name,
                //             sProductInfo = oProductData[i].product_info,
                //             sItems = (oProductData[i].is_deleted == 1) ? "On" : "Off" ,
                //             sDateTime = moment(oProductData[i].date_unavailabled),
                //             sID = oProductData[i].id;

                //         trTemplate.find('td[data-label="pu_report_number"]').text(sID);
                //         trTemplate.find('td[data-label="pu_report_store"]').text(sStoreName);
                //         // trTemplate.find('td[data-label="pu_report_type"]').text('type');
                //         trTemplate.find('td[data-label="pu_report_product_info"]').html(sProductInfo);
                //         trTemplate.find('td[data-label="pu_report_items"]').text(sItems);
                //         trTemplate.find('td[data-label="pu_report_date_time"]').text(sDateTime.format('MMMM DD, YYYY | hh:mm:ss A'));
                        
                //         // uiClonedTrs.push(trTemplate);

                //         trTemplateHidden.find('td[data-label="pu_report_number"]').text(sID);
                //         trTemplateHidden.find('td[data-label="pu_report_store"]').text(sStoreName);
                //         // trTemplateHidden.find('td[data-label="pu_report_type"]').text('type');
                //         trTemplateHidden.find('td[data-label="pu_report_product_info"]').html(sProductInfo);
                //         trTemplateHidden.find('td[data-label="pu_report_items"]').text(sItems);
                //         trTemplateHidden.find('td[data-label="pu_report_date_time"]').text(sDateTime.format('YYYY-MM-DD | HH:mm:ss'));
                        
                //         // uiClonedHiddenTrs.push(trTemplateHidden);

                //         uiContentContainer.append(trTemplate);
                //         uiHiddenContentContainer.append(trTemplateHidden);
                //     }
                // }
                // reportPagination.paginate(uiPaginationContainer, 10, oProductData, pageAction, 'daily');

                $.each(oProductData, function (key, value){
                    var trTemplate = $('tr.pu_report_tr.template').clone().removeClass('template notthis'),
                        trTemplateHidden = $('tr.pu_report_tr_hidden.template').clone().removeClass('template notthis'),
                        sStoreName = value.store_name,
                        sProductInfo = value.product_info,
                        sItems = (value.is_deleted == 1) ? "On" : "Off" ,
                        sDateTime = moment(value.date_unavailabled),
                        sID = value.id;
                    
                    trTemplate.find('td[data-label="pu_report_number"]').text(sID);
                    trTemplate.find('td[data-label="pu_report_store"]').text(sStoreName);
                    // trTemplate.find('td[data-label="pu_report_type"]').text('type');
                    trTemplate.find('td[data-label="pu_report_product_info"]').html(sProductInfo);
                    trTemplate.find('td[data-label="pu_report_items"]').text(sItems);
                    trTemplate.find('td[data-label="pu_report_date_time"]').text(sDateTime.format('MMMM DD, YYYY | hh:mm:ss A'));

                    // trTemplateHidden.find('td[data-label="pu_report_number"]').text(sID);
                    // trTemplateHidden.find('td[data-label="pu_report_store"]').text(sStoreName);
                    // // trTemplateHidden.find('td[data-label="pu_report_type"]').text('type');
                    // trTemplateHidden.find('td[data-label="pu_report_product_info"]').html(sProductInfo);
                    // trTemplateHidden.find('td[data-label="pu_report_items"]').text(sItems);
                    // trTemplateHidden.find('td[data-label="pu_report_date_time"]').text(sDateTime.format('YYYY-MM-DD | HH:mm:ss'));
                    
                    uiContentContainer.append(trTemplate);
                    // uiHiddenContentContainer.append(trTemplateHidden);
                });
            }
        },

        'manipulate_template_xls': function(oProductData, oParams){
            if(typeof(oProductData) != 'undefined' && typeof(oParams) != 'undefined')
            {
                var uiContentContainer = $('tbody.pu_report_tr_container'),
                    uiHiddenContentContainer = $('tbody.pu_report_tr_container_hidden'),
                    uiClonedTrs = [],
                    uiClonedHiddenTrs = [],
                    uiItems = '',
                    uiItemsHidden = '',
                    ctr = 0;

                    uiHiddenContentContainer.find('tr.pu_report_tr_hidden:not(.template)').remove();
                    uiHiddenContentContainer.find('tr.pu_report_tr_hidden_items:not(.template)').remove();

                $.each(oProductData, function (key, value){
                    var trTemplateHidden = $('tr.pu_report_tr_hidden.template').clone().removeClass('template notthis'),
                        sStoreName = value.store_name,
                        sProductInfo = value.product_info,
                        sItems = (value.is_deleted == 1) ? "On" : "Off" ,
                        sDateTime = moment(value.date_unavailabled),
                        sID = value.id;

                    trTemplateHidden.find('td[data-label="pu_report_number"]').text(sID);
                    trTemplateHidden.find('td[data-label="pu_report_store"]').text(sStoreName);
                    // trTemplateHidden.find('td[data-label="pu_report_type"]').text('type');
                    trTemplateHidden.find('td[data-label="pu_report_product_info"]').html(sProductInfo);
                    trTemplateHidden.find('td[data-label="pu_report_items"]').text(sItems);
                    trTemplateHidden.find('td[data-label="pu_report_date_time"]').text(sDateTime.format('YYYY-MM-DD | HH:mm:ss'));

                    uiHiddenContentContainer.append(trTemplateHidden);
                });
            }
        },

        'ajax': function (oAjaxConfig) {
            if (typeof(oAjaxConfig) != 'undefined') {
                admin.CconnectionDetector.ajax(oAjaxConfig);
            }
        }
    }

}());

$(window).load(function () {
    "use strict";
    admin.product_unavailability_report.initialize();
});


