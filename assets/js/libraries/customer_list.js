/**
 *  Search Order Class
 *
 *
 *
 */
(function () {
    "use strict";
    var oAutoComplete = {
        oProvinces  : []
    };

    var oOrders = {};
    var userArray = [];

    var oAutocompleteParams = {
        "params": {
            "offset":0,
            "where_like": [
                {
                    "field"   : function () {
                        var uiSearchByValue = $('section #customer_list_header input#customer_list_search_by').val();
                        if (uiSearchByValue == "Contact Number") {
                            uiSearchByValue = 'ccn.number';
                        }
                        else {
                            uiSearchByValue = 'c.first_name';
                        }

                        return uiSearchByValue;
                    },
                    "operator": "LIKE",
                    "value"   : function () {
                        return $('section #customer_list_header input#search_customer_list').val()
                    }
                },
                /*{
                    "field"   : "c.last_order_date",
                    "operator": "LIKE",
                    "value"   : function () {
                        return $("section #customer_list_header #customer_list_search_by_last_ordered").find("input.dd-txt").attr("value");
                    }
                },*/
            ],
            "where": [],

        }
    }

    var uiPage = 'customer_list';

    CPlatform.prototype.customer_list = {

        'initialize' : function () {
			$("section#customer_list_header #search_customer_list").search_autocomplete({
                "ui" : $("section#customer_list_header #search_customer_list"),
                "data"    : oAutocompleteParams,
                "url"     : callcenter.config('url.api.jfc.customers') + "/customers/search",
                "callback": function (oData) {
                    if(typeof(oData) != 'undefined')
                    {
                        var uiContainer = this.ui.next('div.result-list:first');
                        uiContainer.find('div.list').remove();
                        if(count(oData.data) > 0)
                        {
                            $.each(oData.data, function(key,value){
                                uiContainer.append(
                                    '<div class="list" data-first_name="' + value.first_name + '" data-last_name="' + value.last_name + '" data-middle_name="' + value.middle_name + '">' + value.first_name + ' ' + value.middle_name + ' ' + value.last_name + '</div>'
                                );
                            });
                        }
                    }
                }

            });

            $('body').on('click', function () {
                $("section#customer_list_header div.result-list").html("");
            });

            $('section#customer_list_header a.customer_list_sort').each(function( i ) {
                $(this).css({"color":"black"});
                $(this).removeClass("active red-color");
                $(this).find("i.fa").removeClass("fa-angle-up");
                $(this).find("i.fa").removeClass("fa-angle-down");
            });
			
            //add value on the select province option
            $('section #customer_list_header #customer_list_search_by_province').on('click', 'div.option', function (e) {
                var data_value = $(this).attr("data-value");
                $(this).closest("section #customer_list_header #customer_list_search_by_province").find("input.dd-txt").attr("value", data_value);
                oAutocompleteParams.params.where =[];
                if ($("section #customer_list_header #customer_list_search_by_province").find("input.dd-txt").attr("value")!="All Province") {
                    oAutocompleteParams.params.where.push(
                        {
                            "field"   : 'ca.province',
                            "operator": "=",
                            "value"   : function () {
                                return $("section #customer_list_header #customer_list_search_by_province").find("input.dd-txt").attr("value");
                            }
                        }
                    );
                }
                // oAutoComplete.oProvinces = callcenter.get_from_storage("autocomplete_gis_province", '');
                // //console.log(oAutoComplete.oProvinces);

            });

            //add value on the select last ordered option
            $('section #customer_list_header #customer_list_search_by_last_ordered').on('click', 'div.option', function (e) {
                var data_value = $(this).attr("data-value");
                $(this).closest("section #customer_list_header #customer_list_search_by_last_ordered").find("input.dd-txt").attr("value", data_value);
            });

            //add value on the select customer behavior options
            $('section #customer_list_header #customer_list_search_by_behavior').on('click', 'div.option', function (e) {
                var data_value = $(this).attr("data-value");
                $(this).closest("section #customer_list_header #customer_list_search_by_behavior").find("input.dd-txt").attr("value", data_value);
            });

            //triggering click to second list if address is clicked
            $('section #customer_list_header #customer_list_search_by_filter').on('click', 'div.option', function (e) {
                var data_value = $(this).attr("data-value");
                if(data_value =="Address")
                {   
                    $("section #customer_list_header").find('div#customer_list_search_by_province div.option[data-value]:eq(1)').trigger('click');
                }
                else
                {
                    $("section #customer_list_header").find('div#customer_list_search_by_province div.option[data-value]:first').trigger('click');
                }
            });

            $('section#customer_list_header').on('click', 'button.search_button', function () {
                if($('section#customer_list_header #search_customer_list').val().length > 2 ){
                    
                    if($('section #customer_list_header input#customer_list_search_by').val()=="Address")
                    {//search by address
                        //callcenter.customer_list.show_spinner($('button.search_button'), true);
                        if($("section #customer_list_header #customer_list_search_by_province").find("input.dd-txt").attr("value") !="All Province")
                            {
                                callcenter.customer_list.show_spinner($('button.search_button'), true);
                                callcenter.customer_list.show_spinner($('button.download_customer'), true);
                                callcenter.customer_list.assemble_search_param_by_address();
                                callcenter.customer_list.assemble_search_param_by_address_csv();
                            }
                        
                    }
                    else
                    {   
                        callcenter.customer_list.show_spinner($('button.download_customer'), true);
                        callcenter.customer_list.show_spinner($('button.search_button'), true);
                        callcenter.customer_list.assemble_search_param();
                        callcenter.customer_list.assemble_search_param_csv();
                    }
                    
                }
            })

			 $('section#customer_list_header').on('click', 'div.list', function () {
                $('div.list').remove();
				
                var sFirstName = $(this).attr('data-first_name'),
                    sMiddleName = $(this).attr('data-middle_name'),
                    sLastName = $(this).attr('data-last_name');


                var oParams = {
                    "params": {
                        "where": [
                            {
                                "field"   : 'c.first_name',
                                "operator": "=",
                                "value"   : '"'+sFirstName+'"'
                            },
                            {
                                "field"   : 'c.middle_name',
                                "operator": "=",
                                "value"   : '"'+sMiddleName+'"'
                            },
                            {
                                "field"   : 'c.last_name',
                                "operator": "=",
                                "value"   : '"'+sLastName+'"'
                            }
                        ]
                    }
                }

                var oAjaxConfig = {
                    "type"   : "Get",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.customers') + "/customers/get",
                    "success": function (data) {
                        /*//console.log(data);
                         //console.log(count(data.data))*/
                        //clear search box
                        // $('section#customer_list_header input#search_customer_list').val("");
                        if(typeof(data) !== 'undefined')
                        {
                            if (count(data.data) > 0) {
                                $('p.count_search_result strong').text('Search Result(s) | ' + count(data.data) + ' Customers');
                                callcenter.customer_list.content_panel_toggle('customer_list');
                                var uiContainer = $('div#customer_list_content div.search-container');
                                uiContainer.html('');
                                $.each(data.data, function (key, value) {
                                    var uiTemplate = $('div#customer_list_search_result.template').clone().removeClass('template');
                                    var uiManipulatedTemplate = callcenter.customer_list.manipulate_template(uiTemplate, value);
                                    uiManipulatedTemplate.find('div.customer_secondary_address').hide();
                                    callcenter.customer_list.clone_append(uiManipulatedTemplate, uiContainer);
                                })
                            }
                            else {
                                $('p.count_search_result').find('strong').text('No Search Result');
                                callcenter.customer_list.content_panel_toggle('no_search_customer');
                            }
                            callcenter.customer_list.clear_customer_list_sortby_classes();
                            //$("section.content #customer_list_content div#paginations").addClass("hidden"); 
                            //for the pagination class     
                            var page = new paginations("section.content #customer_list_content div#paginations",data.total_rows,15);
                            if(data.total_rows > 10)//hide or show pagination
                            {
                               $("section.content #customer_list_content div#paginations").removeClass("hidden"); 
                            }else{
                               $("section.content #customer_list_content div#paginations").addClass("hidden"); 
                            }  
                            //Delegation of events on the paginations li
                            $("section.content #customer_list_content div#paginations").off('click','li.page:not(.page-ellips)').on('click','li.page:not(.page-ellips)',function(){
                                var no = $(this).text()
                                page.showPage(no,callcenter.config('url.api.jfc.customers') + "/customers/search");
                                page.paginate(this);      
                            });
                            $("section.content #customer_list_content div#paginations").off('click','li.page-prev').on('click','li.page-prev',function(){
                                page.paginates("-");
                                page.showPage(page.currentpage,callcenter.config('url.api.jfc.customers') + "/customers/search");
                            });
                            $("section.content #customer_list_content div#paginations").off('click','li.page-next').on('click','li.page-next',function(){
                                page.paginates("+");
                                page.showPage(page.currentpage,callcenter.config('url.api.jfc.customers') + "/customers/search");
                            });
                            $("section.content #customer_list_content div#paginations").off('click','li.page-max').on('click','li.page-max',function(){
                                var no = $(this).attr("data-no");
                                page.showmaxPage(no);
                            });

                            //for the sorting
                            callcenter.customer_list.clear_customer_list_sortby_classes();

                            $('section#customer_list_header a.customer_list_sortby_name').addClass("red-color active");
                            $('section#customer_list_header a.customer_list_sortby_name').find("i.fa").addClass("fa-angle-up");
                            $('section#customer_list_header a.customer_list_sortby_name').attr("data-value","desc");

                            $('section#customer_list_header').off('click','a.customer_list_sortby_name').on('click', 'a.customer_list_sortby_name', function () {
                                var iPageNo = $("section.content #customer_list_content div#paginations").find("li.page-active").attr("data-no");
                                page.sortPage($(this),"c.first_name",iPageNo,callcenter.config('url.api.jfc.customers') + "/customers/search");
                            })

                            $('section#customer_list_header').off('click','a.customer_list_sortby_behavior').on('click', 'a.customer_list_sortby_behavior', function () {
                                var iPageNo = $("section.content #customer_list_content div#paginations").find("li.page-active").attr("data-no");
                                page.sortPage($(this),"c.last_behavior_id",iPageNo,callcenter.config('url.api.jfc.customers') + "/customers/search");
                            })

                            $('section#customer_list_header').off('click','a.customer_list_sortby_recent_ordered').on('click', 'a.customer_list_sortby_recent_ordered', function () {
                                var iPageNo = $("section.content #customer_list_content div#paginations").find("li.page-active").attr("data-no");
                                page.sortPage($(this),"c.last_order_date",iPageNo,callcenter.config('url.api.jfc.customers') + "/customers/search");
                            })
                            
                            //add class active on coordinator side
                            $('header ul#main_nav_headers li.order_management').addClass('active');
                        }
                        }
                };

                callcenter.customer_list.get(oAjaxConfig);
                callcenter.customer_list.show_spinner($('button.download_customer'), true);
                callcenter.customer_list.assemble_list_param_csv(sFirstName, sMiddleName, sLastName);

            });
            
            //show content / hide header
            $('section.content #customer_list_content').on('click', 'div.header-search', function () {
                $(this).parent('div.content-container').find('div.collapsible').show();
                $(this).hide();

                $(this).parents('div.search_result_customer_list').siblings().find('div.collapsible').hide();
                $(this).parents('div.search_result_customer_list').siblings().find('div.header-search').show();

                //hide update success msg
                $(this).parents('div.search_result_container:first').find('div.collapsible div.success-msg').hide();
                var bVisible = $(this).next('div.collapsible').is(":visible");
                var iCustomerID = $(this).parents('div.search_result_customer_list:first').attr('data-customer-id');
                if (bVisible) {
                    callcenter.call_orders.get_order_history($(this).parents('div.search_result_customer_list:first'), iCustomerID);
                }
            });
            
            //hide content / show header
            $('section.content #customer_list_content').on('click', 'div.second_header', function () {
				var iCustomerId = $(this).attr('data-customer-list-id');
				$('section.content #customer_list_content').find('div.header-search[data-customer-id="' + iCustomerId + '"]').show();
                $(this).parents('div.collapsible').find('div.success-msg').hide();
				$(this).parent().hide();
            });
			
			//api_get_address
            var oParams = {
                "params": {}
            };
			
            /* //Get Address Test
            var oAddressAjaxConfig = {
                "type"   : "Get",
                "data"   : oParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : callcenter.config('url.api.jfc.gis') + "/gis/test_address",
                "success": function (data) {
					
				}
			}
			callcenter.customer_list.get(oAddressAjaxConfig); */

            $('ul.main_nav').on('click', 'li.customer_list', function () {
                
            });

            $('section#order-process #customer_list_content').on('click', 'button.btn-update', function (e){
                var oData = $(this).parents('div.search_result_customer_list').attr('data-customer-id');
                var updateClone = $('div.update_customer.template').clone().removeClass('hidden').removeClass('template');
                var uiTemplate = $(this).parents('div.search_result_customer_list div.content-container div.collapsible');
                uiTemplate.find('div.second_header').hide();
                uiTemplate.find('div.second_header').siblings().not(updateClone).hide();

                if(uiTemplate.find('div.update_customer').length)
                {
                    uiTemplate.find('div.update_customer').remove();
                }

                uiTemplate.append(updateClone);
                callcenter.customer_list.assemble_update_form(oData, uiTemplate, updateClone);
            });

            $('section#order-process #customer_list_content').on('click', 'button.btn-cancel', function (e){
                e.preventDefault();
                var oData = $(this).parents('div.search_result_customer_list').attr('data-customer-id');
                var updateClone = $('div.update_customer').clone().removeClass('hidden').removeClass('template');
                var uiTemplate = $(this).parents('div.search_result_customer_list div.content-container div.collapsible');
                uiTemplate.find('div.second_header').show();
                uiTemplate.find('div.second_header').siblings().not(updateClone).show();
                uiTemplate.find('div.success-msg').hide();
                uiTemplate.find('div.error-msg').hide();
                uiTemplate.find('div.update_customer').hide();

                var oParam = {
                    "params": {
                        "where": [
                            {
                                "field"   : 'c.id',
                                "operator": "=",
                                "value"   : oData
                            }
                        ]
                    }
                }
                var oConfig = {
                    "type"   : "Get",
                    "data"   : oParam,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.customers') + "/customers/get",
                    "success": function (oData) {
                        if(typeof(oData) !== 'undefined')
                        {
                            $.each(oData.data, function (key, value) {
                                /*address*/
                                if (count(value.address) > 0 && count(value.address_text) > 0 && typeof(value.address) !== 'undefined') {
                                    uiTemplate.find('div.customer_list_default_address:not(.template)').remove();
                                    uiTemplate.find('div.other_address_container div.customer_list_address:not(.template)').remove();
                                    uiTemplate.find('div.default_address_container div.customer_list_address:not(.template)').remove();
                                    uiTemplate.find('div.default_address_container div.customer_list_default_address:not(.template)').remove();

                                    $.each(value.address, function (k, v) {
                                        var house_number = (v.house_number !== '' && v.house_number != "0") ? v.house_number : '',
                                            floor = (v.floor !== '' && v.floor != "0") ? v.floor : '',
                                            building = (value.address_text[k].building !== null) ? value.address_text[k].building.name : '',
                                            barangay = (value.address_text[k].barangay !== null) ? value.address_text[k].barangay.name : '',
                                            subdivision = (value.address_text[k].subdivision !== null) ? value.address_text[k].subdivision.name : '',
                                            city = (value.address_text[k].city !== null) ? value.address_text[k].city.name : '',
                                            street = (value.address_text[k].street !== null) ? value.address_text[k].street.name : '',
                                            second_street = (value.address_text[k].second_street !== null && typeof(value.address_text[k].second_street) != 'undefined') ? value.address_text[k].second_street.name : '',
                                            province = (value.address_text[k].province !== null) ? value.address_text[k].province.name : '';

                                        var uiAddressTemplate = uiTemplate.find('div.customer_list_address.template').clone().removeClass('template');

                                        if(v.address_type.toUpperCase() === "HOME")
                                        {
                                            uiAddressTemplate.find('img.address_image').attr({'src': callcenter.config('url.server.base') + 'assets/images/Home2.svg', "alt" : "work icon", "class" : "small-thumb"});
                                            uiAddressTemplate.find('img.address_image').attr('onError', '$.fn.checkImage(this);');
                                        }
                                        else
                                        {
                                            uiAddressTemplate.find('img.address_image').attr({'src': callcenter.config('url.server.base') + 'assets/images/work-icon.png', "alt" : "work icon"});
                                            uiAddressTemplate.find('img.address_image').attr('onError', '$.fn.checkImage(this);');
                                        }
                                        uiAddressTemplate.find('strong.address_label').text(v.address_label);
                                        uiAddressTemplate.find('p.address_complete').text('' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);
                                        uiAddressTemplate.find('strong.address_type').text(v.address_type.toUpperCase());
                                        uiAddressTemplate.find('p.address_landmark').text('- ' + v.landmark.toUpperCase());
                                        uiAddressTemplate.attr('customer_id', i.id);
                                        uiAddressTemplate.attr('address_id', v.id);
                                        uiAddressTemplate.attr('house_number', v.house_number);
                                        uiAddressTemplate.attr('building', v.building);
                                        uiAddressTemplate.attr('floor', v.floor);
                                        uiAddressTemplate.attr('street', v.street);
                                        uiAddressTemplate.attr('second_street', v.second_street);
                                        uiAddressTemplate.attr('barangay', v.barangay);
                                        uiAddressTemplate.attr('subdivision', v.subdivision);
                                        uiAddressTemplate.attr('city', v.city);
                                        uiAddressTemplate.attr('province', v.province);
                                        uiAddressTemplate.attr('address_label', v.address_label);
                                        uiAddressTemplate.attr('landmark', v.landmark);
                                        uiAddressTemplate.attr('address_type', v.address_type);
                                        uiAddressTemplate.attr('all_address', '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);

                                        if (v.is_default == 1)
                                        {
                                            // var all_address = '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province;
                                            // uiTemplate.siblings('div.header-search').find('div.remarks_and_address p.address').empty().html('<span class="red-color info_address"><strong class="">Address: </strong></span>'+all_address);
                                            // uiAddressTemplate.find('strong.address_label').html(v.address_label+' - <span class="red-color">Default</span>');
                                            // uiTemplate.find('div.default_address_container').append(uiAddressTemplate);

                                            if(v.is_deleted_date != '0000-00-00 00:00:00' && v.is_deleted == 1)
                                            {
                                                var dateNow = moment().format("YYYY-MM-DD"),
                                                    timeNow = moment().format("HH:mm:ss"),
                                                    is_deleted_fulldate = v.is_deleted_date.split(' '),
                                                    is_deleted_date = is_deleted_fulldate[0].split('-'),
                                                    is_deleted_time = is_deleted_fulldate[1].split(':'),
                                                    time_now = timeNow.split(':');

                                                /*deleted time*/
                                                var is_deleted_hour = is_deleted_time[0],
                                                    is_deleted_min = is_deleted_time[1],
                                                    is_deleted_sec = is_deleted_time[2];

                                                /*time now*/
                                                var hour_now = time_now[0],
                                                    min_now = time_now[1],
                                                    sec_now = time_now[2];

                                                if(dateNow == is_deleted_fulldate[0])
                                                {
                                                    if( ( parseInt(hour_now) - parseInt(is_deleted_hour) ) > parseInt('1') || ( parseInt(hour_now) - parseInt(is_deleted_hour) ) == parseInt('1') && parseInt(is_deleted_min) <= parseInt(min_now) )
                                                    {
                                                        // console.log("remove/hide address")
                                                    }
                                                    else
                                                    {
                                                        // console.log("undo");
                                                        var all_address = '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province;
                                                        uiTemplate.siblings('div.header-search').find('div.remarks_and_address p.address').empty().html('<span class="red-color info_address"><strong class="">Address: </strong></span>'+all_address);
                                                        uiAddressTemplate.find('strong.address_label').html(v.address_label+' - <span class="red-color">Default</span>');
                                                        uiTemplate.find('div.default_address_container').append(uiAddressTemplate);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                var all_address = '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province;
                                                uiTemplate.siblings('div.header-search').find('div.remarks_and_address p.address').empty().html('<span class="red-color info_address"><strong class="">Address: </strong></span>'+all_address);
                                                uiAddressTemplate.find('strong.address_label').html(v.address_label+' - <span class="red-color">Default</span>');
                                                uiTemplate.find('div.default_address_container').append(uiAddressTemplate);
                                            }
                                        }
                                        else
                                        {
                                            // // uiAddressTemplate.removeClass('bggray-light').addClass('white');
                                            // uiTemplate.find('div.other_address_container').append(uiAddressTemplate);

                                            if(v.is_deleted_date != '0000-00-00 00:00:00' && v.is_deleted == 1)
                                            {
                                                var dateNow = moment().format("YYYY-MM-DD"),
                                                    timeNow = moment().format("HH:mm:ss"),
                                                    is_deleted_fulldate = v.is_deleted_date.split(' '),
                                                    is_deleted_date = is_deleted_fulldate[0].split('-'),
                                                    is_deleted_time = is_deleted_fulldate[1].split(':'),
                                                    time_now = timeNow.split(':');

                                                /*deleted time*/
                                                var is_deleted_hour = is_deleted_time[0],
                                                    is_deleted_min = is_deleted_time[1],
                                                    is_deleted_sec = is_deleted_time[2];

                                                /*time now*/
                                                var hour_now = time_now[0],
                                                    min_now = time_now[1],
                                                    sec_now = time_now[2];

                                                if(dateNow == is_deleted_fulldate[0])
                                                {
                                                    if( ( parseInt(hour_now) - parseInt(is_deleted_hour) ) > parseInt('1') || ( parseInt(hour_now) - parseInt(is_deleted_hour) ) == parseInt('1') && parseInt(is_deleted_min) <= parseInt(min_now) )
                                                    {
                                                        // console.log("remove/hide address")
                                                    }
                                                    else
                                                    {
                                                        // console.log("undo");
                                                        // uiAddressTemplate.removeClass('bggray-light').addClass('white');
                                                        uiTemplate.find('div.other_address_container').append(uiAddressTemplate);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                // uiAddressTemplate.removeClass('bggray-light').addClass('white');
                                                uiTemplate.find('div.other_address_container').append(uiAddressTemplate); 
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }
                }
                callcenter.customer_list.get(oConfig);
            });

            $('section#order-process #customer_list_content').on('click', 'a.another_email_address',function (e) {
                var uiTemplate = $(this).parents('div.email_address_container').find('div.template.new_email_address').clone().removeClass('template');
                var uiContainer = $(this).parents('div.email_address_container:first');
                callcenter.customer_list.clone_append(uiTemplate, uiContainer);
            });

            $('section#order-process #customer_list_content').on('click', 'input[type="radio"][name="senior"]',function (e) {
                if ($(this).val() == "1") {
                    $(this).parents('div.senior_container').find('div.senior_citizen').show();
                    $(this).parents('div.senior_container').find('div.senior_citizen').find("input[type='text'][name='osca_number']").attr({
                        'labelinput': 'Osca Number',
                        'datavalid' : 'required'
                    });
                    $(this).parents('div.senior_container').siblings('div.pwd_container').find('input[id^="pwd2"]').prop('checked', true); //uncheck pwd because their can only be one discount
                }
                else if ($(this).val() == "0") {
                    $(this).parents('div.senior_container').find('div.senior_citizen').hide();
                    $(this).parents('div.senior_container').find('div.senior_citizen').find("input[type='text'][name='osca_number']").removeAttr('datavalid labelinput');
                }
            });

            $('section.content #customer_list_content').on('click', 'input[type="radio"][name="pwd"]', function (e) { //uncheck senior citizen because their can only be one discount
                if ($(this).val() == "1") {
                    $(this).parents('div.pwd_container').siblings('div.senior_container').find('div.senior_citizen').hide();
                    $(this).parents('div.pwd_container').siblings('div.senior_container').find('div.senior_citizen').find("input[type='text'][name='osca_number']").removeAttr('datavalid labelinput');
                    $(this).parents('div.pwd_container').siblings('div.senior_container').find('input[id^="senior2"]').prop('checked', true);
                }
                else if ($(this).val() == "0") {

                }
            });

            $('section#order-process #customer_list_content').on('click', '.alternate-contact-number', function (e) {
                $(this).addClass("hidden");
                $(this).parents('div.contact-number-container').siblings('div.alternate_contact_container').removeClass("hidden");
                $(this).parents('div.contact-number-container').siblings('div.alternate_contact_container').find('input[name="contact_number[]"].mobile').attr({'datavalid': /*numeric*/' required', 'labelinput' : 'Contact Number'});
                // $(this).parents('div.contact-number-container').siblings('div.alternate_contact_container').find('input[name="contact_number[]"].landline.contact-1').attr({'datavalid': /*numeric*/' required', 'labelinput' : 'Landline Number'});
                // $(this).parents('div.contact-number-container').siblings('div.alternate_contact_container').find('input[name="contact_number[]"].landline.contact-2').attr({'datavalid': /*numeric*/' required', 'labelinput' : 'Landline Number Trunkline'});
                $(this).parents('div.contact-number-container').siblings('div.alternate_contact_container').find("input.small").removeClass("hidden");
            });

            $('section#order-process #customer_list_content').on('click', 'div.option', function (e) {//selection_of_mobile_and_landline
                $(this).closest("section#order-process #customer_list_content div.frm-custom-dropdown").find("input.dd-txt").attr("value", $(this).html());
                if ($(this).closest("section#order-process #customer_list_content div.frm-custom-dropdown").find("input.dd-txt").attr("value") == "Mobile") {//mobile selected
                    $(this).closest("section#order-process #customer_list_content div.contact-number-container").find("input.small").removeClass("hidden");
                    $(this).closest("section#order-process #customer_list_content div.contact-number-container").find("input.small").attr("datavalid",  "required");
                    $(this).closest("section#order-process #customer_list_content div.contact-number-container").find("input.xsmall").addClass("hidden");
                    $(this).closest("section#order-process #customer_list_content div.contact-number-container").find("input.xsmall").removeAttr("datavalid");
                } else {//landline selected
                    $(this).closest("section#order-process #customer_list_content div.contact-number-container").find("input.small").addClass("hidden");
                    $(this).closest("section#order-process #customer_list_content div.contact-number-container").find("input.small").removeAttr("datavalid");
                    $(this).closest("section#order-process #customer_list_content div.contact-number-container").find("input.xsmall").removeClass("hidden");
                    $(this).closest("section#order-process #customer_list_content div.contact-number-container").find("input.xsmall").attr("datavalid", /*"numeric */"required");
                }
            });

            $('section#order-process #customer_list_content').on('click', ' a.another-contact-number', function (e) {
                var uiTemplate = $(this).parents('div.alternate_contact_container').find('div.template.new_contact_number').clone().removeClass('template');
                var uiContainer = $(this).parents('div.alternate_contact_container:first');
                uiTemplate.find('input[name="contact_number[]"].mobile').val('').removeAttr('disabled').attr({'datavalid': /*numeric*/' required', 'labelinput' : 'Contact Number'});
                uiTemplate.find('input[name="contact_number[]"].landline').val('').removeAttr('disabled');
                uiTemplate.find('input[name="contact_number[]"].landline.contact-0').val('').removeAttr('disabled').attr({'datavalid': 'required', 'labelinput' : 'Landline Area Code'});
                uiTemplate.find('input[name="contact_number[]"].landline.contact-1').val('').removeAttr('disabled').attr({'datavalid': 'required', 'labelinput' : 'Landline Number'});
                // uiTemplate.find('input[name="contact_number[]"].landline.contact-2').val('').removeAttr('disabled').attr({'datavalid': 'required', 'labelinput' : 'Landline Number Trunkline'});
                uiTemplate.find('input[name="contact_number_type[]"]').val('Mobile').removeAttr('disabled').attr('value', 'Mobile');
                callcenter.call_orders.clone_append(uiTemplate, uiContainer);
                callcenter.call_orders.get_province_identifier();
            });

            $('section#order-process #customer_list_content').on('click', 'div.secondary_address_container_update button.set_default_address', function (e) {
                var uiThis = $(this);

                var oParams = {
                    "params": {
                        "address_id" : uiThis.parents('div.customer_list_default_address_update').attr('address_id'),
                        "customer_id": uiThis.parents('div.customer_list_default_address_update').attr('customer_id')
                    }
                }

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.customers') + "/customers/set_default_address",
                    "success": function (data) {

                        var uiParent = uiThis.parents('div.secondary_address_container_update:first');
                        var newAddress = uiThis.parents('div.customer_list_default_address_update');
                        var oOldPrimaryAddress = uiThis.parents('div.secondary_address_container_update').prev('div.default_address_container_update').find('div.customer_list_default_address_update:visible').clone();
                        newAddress.find('button.set_default_address').attr('disabled', '')
                        newAddress.find('strong.default_address_label').html(newAddress.attr('address_label')+' - <span class="red-color">Default</span>');
                        uiThis.parents('div.secondary_address_container_update').prev('div.default_address_container_update').find('div.customer_list_default_address_update:visible').replaceWith(newAddress.addClass('bggray-light').addClass('white'));
                        oOldPrimaryAddress.find('button.set_default_address').removeAttr('disabled');
                        // oOldPrimaryAddress.removeClass('bggray-light').addClass('white');
                        oOldPrimaryAddress.find('strong.default_address_label').empty().html(oOldPrimaryAddress.attr('address_label'));
                        oOldPrimaryAddress.attr('is_default', 0)
                        newAddress.attr('is_default', 1)
                        uiParent.prepend(oOldPrimaryAddress);

                    }
                };

                callcenter.customer_list.get(oAjaxConfig)
            })

            $('section#order-process #customer_list_content').on('click', 'a.another_happy_plus', function (e) {
                var uiTemplate = $(this).parents('div.parent_happy_plus').find('div.template.new_happy_plus_card').clone().removeClass('template').show();
                var happy_plus_quantity = $(this).parents('div.parent_happy_plus').find('div.happy_plus_card:visible').length + 1;
                uiTemplate.find('input[name="is_smudged"]').attr('id', 'card-number-' + happy_plus_quantity);
                uiTemplate.find('label.chck-lbl').attr('for', 'card-number-' + happy_plus_quantity);
                uiTemplate.find("input[type='text'][name='number']").attr({'labelinput': 'Card Number','datavalid' : 'required'});
                uiTemplate.find("input[type='text'][name='expiry_date']").attr({'labelinput': 'Expiry Date','datavalid' : 'required'});
                var uiContainer = $(this).parents('div.parent_happy_plus:first');

                callcenter.customer_list.clone_append(uiTemplate, uiContainer);

                $(".date-picker").datetimepicker({pickTime: false});
            });

            $('section#order-process #customer_list_content').on('click', 'input[type="radio"][name="happyplus"]', function () { //hide/show of happy plus card
                if ($(this).val() == "1")
                { //if yes, show and add a require validation
                    $(this).parents('div.parent_happy_plus').find('div.happy_plus_card:not(.template)').show();
                    $(this).parents('div.parent_happy_plus').find('div.happy_plus_card:not(.template)').first().find('a.alternate_happy_plus').removeClass("hidden");
                    $(this).parents('div.parent_happy_plus').find('div.happy_plus_card:not(.template)').find("input[type='text'][name='number']").attr({'labelinput': 'Card Number','datavalid' : 'required'});
                    $(this).parents('div.parent_happy_plus').find('div.happy_plus_card:not(.template)').find("input[type='text'][name='expiry_date']").attr({'labelinput': 'Expiry Date','datavalid' : 'required'});
                    if($(this).parents('div.parent_happy_plus').find('div.happy_plus_card:not(.template):visible').length > 1)
                    {
                        $(this).parents('div.parent_happy_plus').find('a.alternate_happy_plus').addClass("hidden");
                    }
                    if($(this).parents('div.parent_happy_plus').find('div.happy_plus_card:not(.template):visible').length > 2)
                    {
                        $(this).parents('div.parent_happy_plus').find('a.another_happy_plus').removeClass("margin-bottom-15");
                    }
                }
                else if ($(this).val() == "0")
                { //if no, it will hide and remove the validation
                    $(this).parents('div.parent_happy_plus').find('div.happy_plus_card').hide();
                    $(this).parents('div.parent_happy_plus').find('div.happy_plus_card:not(.template)').find("input[type='text'][name='number']").removeAttr('labelinput datavalid');
                    $(this).parents('div.parent_happy_plus').find('div.happy_plus_card:not(.template)').find("input[type='text'][name='expiry_date']").removeAttr('labelinput datavalid');
                }
            });

            $('section#order-process #customer_list_content').on('click', 'a.alternate_happy_plus', function (e) {
                $(this).addClass("hidden");
                $(this).parents('div.parent_happy_plus').find('div.another_happy_plus').removeClass("hidden").show();
            });

            $('section#order-process #customer_list_content').on('click', 'button.btn-save', function(){
                $(this).parents('#update-customer').submit();
            })

            $('section#order-process #customer_list_content').on('submit', '#update-customer',function (e) {
                // $('div.error-msg').hide();
                // $('div.success-msg').hide();
                // $('p.error_messages').remove();
                e.preventDefault();
            });

            $("section#order-process #customer_list_content").on('click', 'div.arrow-right', function () {
                if ($(this).parents("div.search_result_customer_list").find("div.order_history_container div:visible").next().length != 0) {
                    $(this).parents("div.search_result_customer_list").find("div.order_history_container div:visible").next().show().prev().hide();
                }
                else {
                    $(this).parents("div.search_result_customer_list").find("div.order_history_container div:visible").hide();
                    $(this).parents("div.search_result_customer_list").find("div.order_history_container div:first").show();

                }
                var uiShown = $(this).parents("div.search_result_customer_list").find("div.order_history_container div:visible");
                var iOrderID = uiShown.attr('data-order-id');
                var sDate = uiShown.parents('div.search_result_customer_list:first').find('div.order_date_option.option[data-value="' + iOrderID + '"]:first').text();
                uiShown.parents('div.search_result_customer_list:first').find('div.frm-custom-dropdown-txt').find('input[name="date-selected"]').val(sDate);
                return false;
            });

            $("section#order-process #customer_list_content").on('click', 'div.arrow-left', function () {
                if ($(this).parents("div.search_result_customer_list").find("div.order_history_container div:visible").prev().length != 0)
                    $(this).parents("div.search_result_customer_list").find("div.order_history_container div:visible").prev().show().next().hide();
                else {
                    $(this).parents("div.search_result_customer_list").find("div.order_history_container div:visible").hide();
                    $(this).parents("div.search_result_customer_list").find("div.order_history_container div:last").show();

                }
                var uiShown = $(this).parents("div.search_result_customer_list").find("div.order_history_container div:visible");
                var iOrderID = uiShown.attr('data-order-id');
                var sDate = uiShown.parents('div.search_result_customer_list:first').find('div.order_date_option.option[data-value="' + iOrderID + '"]:first').text();
                uiShown.parents('div.search_result_customer_list:first').find('div.frm-custom-dropdown-txt').find('input[name="date-selected"]').val(sDate);
                return false;
            });

            $('section#order-process #customer_list_content').on('click', 'div.order_date_option', function () {
                var iOrderID = $(this).attr('data-value');
                $(this).parents("div.search_result_customer_list").find("div.order_history_container").children('div').hide();
                $(this).parents("div.search_result_customer_list").find("div.order_history_container").find('div[data-order-id="' + iOrderID + '"]').show();
            })

            $('section#order-process #customer_list_content').on('click', 'a[modal-target="deliver-to-different"]', function () {
                $(this).parents().find('div.modal-error-msg p.error_messages').remove();
                $(this).parents().find('div.modal-error-msg').hide();
                $('form#add_address').find('input').val('');
                $('form#add_address').find('input[name="is_default"]').prop('checked', false);
                $('form#add_address').find('input').removeClass('error');
                $('form#add_address').find('div.select').removeClass('has-error');

                $('div[modal-id="deliver-to-different"]').find('button.update_address_button').removeClass("update_address_button").addClass("add_address_button");

                var iCustomerID = $(this).parents('div.search_result_customer_list:first').attr('data-customer-id');
                $('form#add_address').find('input[name="customer_id"]').val(iCustomerID);

                // $('form#add_address').find('input.dd-txt[name="address_type"]').attr({'datavalid': 'required', 'labelinput':'Address Type'});
                $('form#add_address').find('input.dd-txt[name="province"]').attr({'datavalid': 'required', 'labelinput':'Province'});
                $('form#add_address').find('input.dd-txt[name="city"]').attr({'datavalid': 'required', 'labelinput':'City'});
            })

            // $('button.add_address_button').on('click', function (e) {
            //     callcenter.customer_list.show_spinner($('button.add_address_button'), true);
            //     $('form#add_address').submit();
            // });

            $('section#order-process #customer_list_content').on('click', 'div.behavior-option', function (e) {
                var behaviorId = $(this).attr('data-value-id');
                $(this).parents('div.frm-custom-dropdown.behavior').find('input[name="behavior"]').attr('last_behavior_id', behaviorId);
            });

            $('section#order-process #customer_list_content').on('change', 'div.frm-custom-dropdown.behavior input[name="behavior"]', function (e) {
                if($(this).val() == '')
                {
                    $(this).attr('last_behavior_id' , '0');
                }
            });

            $('section#order-process #customer_list_content').on('click', "input[type='checkbox'][name='is_smudged']", function (e) { //disable card number and expiry date if is smudged is checked
                if ($(this).is(':checked')) {
                    $(this).parents('section#order-process #customer_list_content div.happy_plus_card:visible').find("input:not([type='checkbox']):not([name='remarks'])").prop("disabled", true);
                    $(this).parents('section#order-process #customer_list_content div.happy_plus_card:visible').find("div.date-picker").css('pointer-events', 'none');
                }
                else {
                    $(this).parents('section#order-process #customer_list_content div.happy_plus_card:visible').find("input").prop("disabled", false);
                    $(this).parents('section#order-process #customer_list_content div.happy_plus_card:visible').find("div.date-picker").css('pointer-events', 'auto');
                }
            });

            $('section#customer_list_header div.header-container').on('click', 'button.download_customer', function () {
                var oCustomerList = $('section#order-process #customer_list_content').find('div.search-container div.search_result_customer_list')
                if(oCustomerList.is(':visible'))
                {
                    callcenter.customer_list.show_spinner($('button.download_customer'), true);

                    var oIds = $(this).attr('data-value-ids');

                    var oParams = {
                        "params": {
                            "limit" : 99999,
                            "where": [
                                {
                                    "field"   : 'c.id',
                                    "operator": "in",
                                    "value"   : oIds
                                }
                            ]
                        }
                    }
                    var oAjaxConfig = {
                        "type"   : "Get",
                        "data"   : oParams,
                        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                        "url"    : callcenter.config('url.api.jfc.customers') + "/customers/get",
                        "success": function (oData) {
                            var userInfo = '';
                            var csvArray = [];
                            if(typeof(oData) !== 'undefined')
                            {
                                $.each(oData.data, function (key, value){
                                    csvArray.push(callcenter.customer_list.manipulate_csv(userArray, value));
                                });
                            }

                            var obj = {"data" : csvArray};
                            callcenter.customer_list.download_csv(obj);
                        }
                    }
                    callcenter.customer_list.get(oAjaxConfig);
                }
            });

            $('section#order-process #customer_list_content').on('click', 'button.update_address[modal-target="deliver-to-different"]', function () {
                $(this).parents().find('div.modal-error-msg p.error_messages').remove();
                $(this).parents().find('div.modal-error-msg').hide();
                var uiModal = $('div[modal-id="deliver-to-different"] form#add_address');
                uiModal.find('input').val('');
                uiModal.find('input[name="is_default"]').prop('checked', false);
                uiModal.find('input').removeClass('error');
                uiModal.find('div.select').removeClass('has-error');

                var iCustomerID = $(this).parents('div.search_result_customer_list:first').attr('data-customer-id');
                uiModal.find('input[name="customer_id"]').val(iCustomerID);

                $('div[modal-id="deliver-to-different"]').find('button.add_address_button').removeClass("add_address_button").addClass("update_address_button");

                // $('form#add_address').find('input.dd-txt[name="address_type"]').attr({'datavalid': 'required', 'labelinput':'Address Type'});
                uiModal.find('input.dd-txt[name="province"]').attr({'datavalid': 'required', 'labelinput':'Province'});
                uiModal.find('input.dd-txt[name="city"]').attr({'datavalid': 'required', 'labelinput':'City'});

                var address_id = $(this).parents('div.customer_list_address_update:first').attr('address_id'),

                    house_number_text = $(this).parents('div.customer_list_address_update:first').attr('house_number_text'),
                    building_text = $(this).parents('div.customer_list_address_update:first').attr('building_text'),
                    floor_text = $(this).parents('div.customer_list_address_update:first').attr('floor_text'),
                    street_text = $(this).parents('div.customer_list_address_update:first').attr('street_text'),
                    second_street_text = $(this).parents('div.customer_list_address_update:first').attr('second_street_text'),
                    barangay_text = $(this).parents('div.customer_list_address_update:first').attr('barangay_text'),
                    subdivision_text = $(this).parents('div.customer_list_address_update:first').attr('subdivision_text'),
                    city_text = $(this).parents('div.customer_list_address_update:first').attr('city_text'),
                    province_text = $(this).parents('div.customer_list_address_update:first').attr('province_text'),
                    address_label_text = $(this).parents('div.customer_list_address_update:first').attr('address_label_text'),
                    address_type_text = $(this).parents('div.customer_list_address_update:first').attr('address_type_text'),
                    landmark_text = $(this).parents('div.customer_list_address_update:first').attr('landmark_text'),

                    house_number = $(this).parents('div.customer_list_address_update:first').attr('house_number'),
                    building = $(this).parents('div.customer_list_address_update:first').attr('building'),
                    floor = $(this).parents('div.customer_list_address_update:first').attr('floor'),
                    street = $(this).parents('div.customer_list_address_update:first').attr('street'),
                    second_street = $(this).parents('div.customer_list_address_update:first').attr('second_street'),
                    barangay = $(this).parents('div.customer_list_address_update:first').attr('barangay'),
                    subdivision = $(this).parents('div.customer_list_address_update:first').attr('subdivision'),
                    city = $(this).parents('div.customer_list_address_update:first').attr('city'),
                    province = $(this).parents('div.customer_list_address_update:first').attr('province'),
                    address_label = $(this).parents('div.customer_list_address_update:first').attr('address_label'),
                    address_type = $(this).parents('div.customer_list_address_update:first').attr('address_type'),
                    landmark = $(this).parents('div.customer_list_address_update:first').attr('landmark'),
                    is_default = $(this).parents('div.customer_list_address_update:first').attr('is_default');

                uiModal.find('#modal_select_province div.option[data-value="'+province+'"]').trigger('click')
                
                uiModal.find('input[name="address_id"]:hidden').val(address_id);

                uiModal.find('input[name="house_number"]').val(house_number_text);
                uiModal.find('input[name="building"]').val(building_text);
                uiModal.find('input[name="floor"]').val(floor_text);
                uiModal.find('input[name="street"]').val(street_text);
                uiModal.find('input[name="second_street"]').val(second_street_text);
                uiModal.find('input[name="barangay"]').val(barangay_text);
                uiModal.find('input[name="subdivision"]').val(subdivision_text);
                uiModal.find('input[name="city"]').val(city_text);
                uiModal.find('input[name="province"]').val(province_text);
                uiModal.find('input[name="address_label"]').val(address_label_text);
                uiModal.find('input[name="address_type"]').val(address_type_text);
                uiModal.find('input[name="landmark"]').val(landmark_text);
                uiModal.find('input[name="is_default"]').val(is_default);
                
                uiModal.find('input[name="house_number"]').attr("value", house_number);
                uiModal.find('input[name="building"]').attr("value", building);
                uiModal.find('input[name="floor"]').attr("value", floor);
                uiModal.find('input[name="street"]').attr("value", street);
                uiModal.find('input[name="second_street"]').attr("value", second_street);
                uiModal.find('input[name="barangay"]').attr("value", barangay);
                uiModal.find('input[name="subdivision"]').attr("value", subdivision);
                uiModal.find('input[name="city"]').attr("value", city);
                uiModal.find('input[name="province"]').attr("value", province);
                uiModal.find('input[name="address_label"]').attr("value", address_label);
                uiModal.find('input[name="address_type"]').attr("value", address_type);
                uiModal.find('input[name="landmark"]').attr("value", landmark);
                uiModal.find('input[name="is_default"]').attr("value", is_default);
                uiModal.find('input[name="is_default"]').prop('checked', ((uiModal.find('input[name="is_default"]').val() == 1) ? true : false));

            });

            $('div[modal-id="deliver-to-different"] form#add_address').on('change', 'input', function () {
                var uiThis = $(this);
                callcenter.customer_list.change_value_update_address(uiThis, uiThis.attr('name'));
            });


            $('section#order-process #customer_list_content').on('click', 'button.archive_address_btn', function (e) {
                var uiThis = $(this);
                uiThis.attr('data-value', '1');
                // var yearNow = moment().format('YYYY'),
                //     monthNow = moment().format('MM'),
                //     dayNow = moment().format('DD'),
                //     hourNow = moment().format('HH'),
                //     minNow = moment().format('mm'),
                //     secNow = moment().format('ss');
                // var archive_address_date = yearNow+'-'+monthNow+'-'+dayNow+' '+hourNow+':'+minNow+':'+secNow;
                var archive_address_date = moment().format('YYYY-MM-DD HH:mm:ss');

                var oParams = {
                    "params": {
                        "address_id" : uiThis.parents('div.customer_list_default_address_update').attr('address_id'),
                        "customer_id": uiThis.parents('div.customer_list_default_address_update').attr('customer_id'),
                        "is_deleted": uiThis.attr('data-value'),
                        "archive_address_date": archive_address_date
                    }
                }
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.customers') + "/customers/archive_address",
                    "success": function (data) {
                        if(typeof(data) !== 'undefined')
                        {
                            if(data.status == true)
                            {
                                uiThis.text("Undo").removeClass("archive_address_btn").addClass("unarchive_address_btn").attr('data-value', '0');
                            }
                        }
                    }
                };

                callcenter.customer_list.get(oAjaxConfig)
            })

            $('section#order-process #customer_list_content').on('click', 'button.unarchive_address_btn', function (e) {
                var uiThis = $(this);
                uiThis.attr('data-value', '0');
                var archive_address_date = '0000-00-00 00:00:00';

                var oParams = {
                    "params": {
                        "address_id" : uiThis.parents('div.customer_list_default_address_update').attr('address_id'),
                        "customer_id": uiThis.parents('div.customer_list_default_address_update').attr('customer_id'),
                        "is_deleted": uiThis.attr('data-value'),
                        "archive_address_date": archive_address_date
                    }
                }
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.customers') + "/customers/archive_address",
                    "success": function (data) {
                        if(typeof(data) !== 'undefined')
                        {
                            if(data.status == true)
                            {
                                uiThis.text("Archive").removeClass("unarchive_address_btn").addClass("archive_address_btn").attr('data-value', '1');
                            }
                        }
                    }
                };

                callcenter.customer_list.get(oAjaxConfig)
            })

            $('section#order-process #customer_list_content').on('keyup', 'div#update_customer_content input[name="contact_number[]"].mobile', function (e) {
                // $(this).val($(this).val().replace(/[^0-9-\.]/g,''));
                // if($(this).val() != '')
                // {
                //     var uiThis = $(this).val().split("-").join("");
                //     uiThis = uiThis.match(new RegExp('.{1,3}$|.{1,4}', 'g')).join("-");
                //     $(this).val(uiThis);
                // }
                var uiThis = $(this);
                uiThis.val(uiThis.val().replace(/[^0-9\.]/g,''));
                if(uiThis.val() != '')
                {
                    callcenter.call_orders.auto_dash_mobile(e, uiThis);
                }
            });

            $('section#order-process #customer_list_content').on('keyup', 'div#update_customer_content input[name="contact_number[]"].landline.contact-1', function (e) {
                var uiThis = $(this);
                uiThis.val(uiThis.val().replace(/[^0-9\.]/g,''));
                if(uiThis.val() != '')
                {
                    callcenter.call_orders.auto_dash_landline(e, uiThis);
                }
            });

        },

        'change_value_update_address': function(uiThis, nameAttr){
            if(typeof(uiThis) != 'undefined' && typeof(nameAttr) != 'undefined')
            {
                if(uiThis.attr('name') == nameAttr)
                {
                    uiThis.attr('value', '');
                }
            }
        },

        'assemble_list_param_csv': function (sFirstName, sMiddleName, sLastName) {
            var oParamsList = {
                "params": {
                    "limit": 99999,
                    "where": [
                        {
                            "field"   : 'c.first_name',
                            "operator": "=",
                            "value"   : '"'+sFirstName+'"'
                        },
                        {
                            "field"   : 'c.middle_name',
                            "operator": "=",
                            "value"   : '"'+sMiddleName+'"'
                        },
                        {
                            "field"   : 'c.last_name',
                            "operator": "=",
                            "value"   : '"'+sLastName+'"'
                        }
                    ]
                }
            }

            var oListConfig = {
                "type"   : "Get",
                "data"   : oParamsList,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : callcenter.config('url.api.jfc.customers') + "/customers/get",
                "success": function (data) {
                    /*ids for download customer list*/
                    //console.log(data)
                    var userIds = '';
                    $('section#customer_list_header div.header-container button.download_customer').attr('data-value-ids', '');

                    //clear search box
                    $('section#customer_list_header input#search_customer_list').val("");

                    if (count(data.data) > 0) {
                        $.each(data.data, function (key, value) {
                            userIds += value.id+', '; 
                        })

                        var oIds = "(" + userIds.replace(/, +$/,'') + ")";
                        $('section#customer_list_header div.header-container button.download_customer').attr('data-value-ids', oIds);
                    }
                    callcenter.customer_list.show_spinner($('button.download_customer'), false);
                }
            }

            callcenter.customer_list.get(oListConfig);
        },

        'assemble_search_param_csv': function () {
            var oParams = {
                "params": {
                    "offset":0,
                    "limit" : 99999,
                    "where_like": [
                        {
                            "field"   : function () {
                                var uiSearchByValue = $('section #customer_list_header input#customer_list_search_by').val();
                                if (uiSearchByValue == "Contact Number") {
                                    uiSearchByValue = 'ccn.number';
                                }
                                else {
                                    uiSearchByValue = 'c.first_name';
                                }

                                return uiSearchByValue;
                            },
                            "operator": "LIKE",
                            "value"   : function () {
                                return $('section#customer_list_header #search_customer_list').val()
                            }
                        }
                    ],

                    "where" : [],
                    "order_by": "c.first_name",
                    "sorting": "asc",

                }
            };

            if ($("section #customer_list_header #customer_list_search_by_province").find("input.dd-txt").attr("value") !="All Province" ) {
                    oParams.params.where =[];
                    oParams.params.where.push(
                        {
                            "field"   : 'ca.province',
                            "operator": "=",
                            "value"   : function () {
                                return $("section #customer_list_header #customer_list_search_by_province").find("input.dd-txt").attr("value");
                            }
                        }
                    );
            }

            if ($("section #customer_list_header #customer_list_search_by_last_ordered").find("input.dd-txt").attr("value")!="") {
                var last_order_date_filter = $("section #customer_list_header #customer_list_search_by_last_ordered").find("input.dd-txt").attr("value");
                oParams.params.last_order_date_filter = last_order_date_filter;
            }

            if ($("section #customer_list_header #customer_list_search_by_behavior").find("input.dd-txt").attr("value")!="Show All Customer") {
                var last_behavior_id_filter = $("section #customer_list_header #customer_list_search_by_behavior").find("input.dd-txt").attr("value");
                oParams.params.last_behavior_id_filter = last_behavior_id_filter;
            }

            var oAjaxConfig = {
                "type"   : "Get",
                "data"   : oParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : callcenter.config('url.api.jfc.customers') + "/customers/search",
                "success": function (data) {
                    /*ids for download customer list*/
                    var userIds = '';
                    $('section#customer_list_header div.header-container button.download_customer').attr('data-value-ids', '');

                    //clear search box
                    $('section#customer_list_header input#search_customer_list').val("");

                    if (count(data.data) > 0) {
                        $.each(data.data, function (key, value) {
                            userIds += value.id+', '; 
                        })

                        var oIds = "(" + userIds.replace(/, +$/,'') + ")";
                        $('section#customer_list_header div.header-container button.download_customer').attr('data-value-ids', oIds);
                    }
                    callcenter.customer_list.show_spinner($('button.download_customer'), false);
                }
            }
            callcenter.customer_list.get(oAjaxConfig);
        },

        'assemble_search_param_by_address_csv' : function (){
            var oParamsAddress = {
                "params": {
                    "offset":0,
                    "limit" : 99999,
                    "where_like": [
                        {
                            "field"   : 'ca.address_label',
                            "operator": "LIKE",
                            "value"   : function () {
                                return $('section#customer_list_header #search_customer_list').val()
                            }
                        }
                    ],

                    "where" : [],
                    "order_by": "c.first_name",
                    "sorting": "asc",

                }
            };

            if ($("section #customer_list_header #customer_list_search_by_province").find("input.dd-txt").attr("value") !="All Province" ) {
                    oParamsAddress.params.where =[];
                    oParamsAddress.params.where.push(
                        {
                            "field"   : 'ca.province',
                            "operator": "=",
                            "value"   : function () {
                                return $("section #customer_list_header #customer_list_search_by_province").find("input.dd-txt").attr("value");
                            }
                        }
                    );
            }

            if ($("section #customer_list_header #customer_list_search_by_last_ordered").find("input.dd-txt").attr("value")!="") {
                var last_order_date_filter = $("section #customer_list_header #customer_list_search_by_last_ordered").find("input.dd-txt").attr("value");
                oParamsAddress.params.last_order_date_filter = last_order_date_filter;
            }

            if ($("section #customer_list_header #customer_list_search_by_behavior").find("input.dd-txt").attr("value")!="Show All Customer") {
                var last_behavior_id_filter = $("section #customer_list_header #customer_list_search_by_behavior").find("input.dd-txt").attr("value");
                oParamsAddress.params.last_behavior_id_filter = last_behavior_id_filter;
            }
            
            var oAjaxConfigAddress = {
                "type"   : "Get",
                "data"   : oParamsAddress,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : callcenter.config('url.api.jfc.customers') + "/customers/search_by_address",
                "success": function (data) {
                    /*ids for download customer list*/
                    var userIds = '';
                    $('section#customer_list_header div.header-container button.download_customer').attr('data-value-ids', '');
                    //clear search box
                    $('section#customer_list_header input#search_customer_list').val("");

                    if (count(data.data) > 0) {
                        $.each(data.data, function (key, value) {
                            userIds += value.id+', '; 
                        })

                        var oIds = "(" + userIds.replace(/, +$/,'') + ")";
                        $('section#customer_list_header div.header-container button.download_customer').attr('data-value-ids', oIds);
                    }
                    callcenter.customer_list.show_spinner($('button.download_customer'), false);
                }
            }
            callcenter.customer_list.get(oAjaxConfigAddress);
        },

        'manipulate_csv': function (userArray, oValue) {
            if (typeof(userArray) !== 'undefined' && typeof(oValue) !== 'undefined') {
                var value = oValue;
                var addressCtr = 0;
                var numberCtr = 0;
                var happyPlusCtr = 0;
                var userArrayAddress = '';
                var userArrayNumber = '';
                var userArrayHappyPlus = '';

                userArray +=  value.id +', '+ value.first_name +', '+ value.middle_name +', '+ value.last_name +', '+ value.birthdate +', '+ value.email_address +', '+ value.last_order_date +
                        ', '+ ((value.is_senior === '1') ? 'Yes': 'No') +', '+ value.osca_number +', '+ ((value.is_pwd === '1') ? 'Yes': 'No') +', '+ ((value.is_loyalty_card_holder === '1') ? 'Yes': 'No') + 
                        ', '+ ((value.display_label === null) ? 'No Remarks Available' : value.display_label) + ', ';
               
                $.each(value.address, function (k, v) {
                    var house_number = (v.house_number !== '' && v.house_number != "0") ? v.house_number : '',
                        floor = (v.floor !== '' && v.floor != "0") ? v.floor : '',
                        building = (value.address_text[k].building !== null) ? value.address_text[k].building.name : '',
                        barangay = (value.address_text[k].barangay !== null) ? value.address_text[k].barangay.name : '',
                        subdivision = (value.address_text[k].subdivision !== null) ? value.address_text[k].subdivision.name : '',
                        city = (value.address_text[k].city !== null) ? value.address_text[k].city.name : '',
                        street = (value.address_text[k].street !== null) ? value.address_text[k].street.name : '',
                        second_street = (value.address_text[k].second_street !== null && typeof(value.address_text[k].second_street) != 'undefined') ? value.address_text[k].second_street.name : '',
                        province = (value.address_text[k].province !== null) ? value.address_text[k].province.name : '';

                        addressCtr++;
                        userArrayAddress += 'Address ' + addressCtr + ':' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province +' || ';
                });

                userArrayAddress = userArrayAddress.replace(/(\|\|) +$/,'');
                userArray += userArrayAddress+',';

                $.each(value.contact_number, function (k, v) {
                        numberCtr++;
                        userArrayNumber += 'Contact Number ' + numberCtr + ':' + v.number +' || ';
                });

                userArrayNumber = userArrayNumber.replace(/(\|\|) +$/,'');
                userArray += userArrayNumber+',';

                $.each(value.happy_plus_card, function (k, v) {
                        happyPlusCtr++;
                        var expDate = v.expiration_date_date.split(",");
                        var expiry_date_date = expDate[0]+''+expDate[1];
                        userArrayHappyPlus += 'Card Number ' + happyPlusCtr + ':' + v.number + ' Expiration Date:' + expiry_date_date +' || ';
                });

                userArrayHappyPlus = userArrayHappyPlus.replace(/(\|\|) +$/,'');
                userArray += userArrayHappyPlus+',';
                
                return userArray;

            }
        },

        'download_csv': function (oCustomerData) {
            if(typeof(oCustomerData) !== 'undefined')
            {
                var oSettings = {
                    stype  : 'customer',
                    url    : callcenter.config('url.server.base') + 'admin/download_csv',
                    data   : oCustomerData,
                    type   : 'POST',
                    success: function (data) {
                        // //console.log(data);
                        callcenter.customer_list.show_spinner($('button.download_customer'), false);
                        window.location.href = callcenter.config('url.server.base') +''+ data;
                        var filename = {"filename" : data, "action": "delete"};
                        setTimeout(function () {
                            callcenter.customer_list.delete_csv(filename);
                        }, 3000)
                    },
                    error  : function () {

                    }
                }
                callcenter.CconnectionDetector.ajax(oSettings);
            }
        },

        'delete_csv': function (filename) {
            var oSettings = {
                stype  : 'customer',
                url    : callcenter.config('url.server.base') + 'admin/delete_csv',
                data   : filename,
                type   : 'POST',
                success: function (data) {
                    // //console.log(data);
                },
                error  : function () {

                }
            }
            callcenter.CconnectionDetector.ajax(oSettings);
        },

        'assemble_update_form' : function (oData, uiTemplate, updateClone){
            if(typeof(oData) !== 'undefined' && typeof(uiTemplate) !== 'undefined' && typeof(updateClone) !== 'undefined')
            {            
                var oParams = {
                    "params": {
                        "where": [
                            {
                                "field"   : 'c.id',
                                "operator": "=",
                                "value"   : oData
                            }
                        ]
                    }
                }

                var oAjaxConfig = {
                    "type"   : "Get",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.customers') + "/customers/get",
                    "success": function (oData) {
                        var cRemarks = '';
                        if (count(oData.data) > 0 && typeof(oData.data) !== 'undefined') {
                            $.each(oData.data, function (key, value){
                                updateClone = callcenter.customer_list.regenerate_ids(updateClone, value);
                                
                                /*name*/
                                updateClone.find('input[name="first_name"]').val(value.first_name);
                                updateClone.find('input[name="middle_name"]').val(value.middle_name);
                                updateClone.find('input[name="last_name"]').val(value.last_name).attr({"datavalid":"required", "labelinput":"Last Name"});

                                /*email*/
                                updateClone.find('input[name="email_address[]"]:first').val(value.email_address)/*.attr({"datavalid":"required", "labelinput":"Email Address"})*/;
                                
                                /*birthdate*/
                                var dateArr = (value.birthdate !== '' && typeof(value.birthdate) !== 'undefined') ? value.birthdate.split("-"): '' ;
                                var dateStr = dateArr[1] + "/" + dateArr[2] + "/" + dateArr[0];
                                updateClone.find('input[name="birthdate"]').val(dateStr);

                                /*osca_number*/
                                updateClone.find('input[name="osca_number"]').val(value.osca_number);

                                /*contact number*/
                                if(count(value.contact_number) > 0 && typeof(value.contact_number) !== 'undefined')
                                {   
                                    if(count(value.contact_number) > 1)
                                    {
                                        updateClone.find('a.alternate-contact-number').addClass("hidden");
                                        updateClone.find('a.alternate-contact-number').parents('div.primary_contact_number').siblings('div.alternate_contact_container').removeClass("hidden");
                                        updateClone.find('a.alternate-contact-number').parents('div.primary_contact_number').siblings('div.alternate_contact_container').find("input.small").removeClass("hidden");
                                    }

                                    $.each(value.contact_number, function (k, v) {
                                        if(k == 0)
                                        {
                                            if(v.number.indexOf('-') !== -1/*v.number.length >= 7 && v.number.length <= 9*/)
                                            {
                                                var landlineSplit = v.number.split('-');
                                                updateClone.find('div.primary_contact_number div.select div.frm-custom-dropdown input[name="contact_number_type[]"]').val("Landline").attr("value", "Landline");
                                                updateClone.find('div.primary_contact_number').find("input.small").addClass("hidden");
                                                updateClone.find('div.primary_contact_number').find("input.small").removeAttr("datavalid");
                                                updateClone.find('div.primary_contact_number').find("input.xsmall").removeClass("hidden").prop("disabled", true);
                                                updateClone.find('div.primary_contact_number').find("input.xsmall:first").val(landlineSplit[0]);
                                                updateClone.find('div.primary_contact_number').find("input.xsmall:eq(1)").val(landlineSplit[1]);
                                                updateClone.find('div.primary_contact_number').find("input.xsmall:eq(2)").val(landlineSplit[2]);
                                            }
                                            else //if(v.number.length  == 11 /*|| v.number.length == 12*/)
                                            {
                                                updateClone.find('div.primary_contact_number input[name="contact_number[]"]:visible').val(v.number).prop("disabled", true).removeAttr("datavalid");

                                            }

                                            updateClone.find('div.primary_contact_number div.select div.frm-custom-dropdown').prop("disabled", true);
                                            updateClone.find('div.primary_contact_number div.select div.frm-custom-dropdown input[name="contact_number_type[]"]').prop("disabled", true);
                                        }
                                        else if(k == 1)
                                        {
                                            if(v.number.indexOf('-') !== -1/*v.number.length >= 7 && v.number.length <= 9*/)
                                            {
                                                var landlineSplit = v.number.split('-');
                                                updateClone.find('div.alternate_contact_container div.select div.frm-custom-dropdown input[name="contact_number_type[]"]').val("Landline").attr("value", "Landline");
                                                updateClone.find('div.alternate_contact_container').find("input.small").addClass("hidden").val('');
                                                updateClone.find('div.alternate_contact_container').find("input.small").removeAttr("datavalid");
                                                updateClone.find('div.alternate_contact_container').find("input.xsmall").removeClass("hidden").prop("disabled", true);
                                                updateClone.find('div.alternate_contact_container').find("input.xsmall:first").val(landlineSplit[0]);
                                                updateClone.find('div.alternate_contact_container').find("input.xsmall:eq(1)").val(landlineSplit[1]);
                                                updateClone.find('div.alternate_contact_container').find("input.xsmall:eq(2)").val(landlineSplit[2]);
                                            }
                                            else //if(v.number.length == 11 /*|| v.number.length == 12*/)
                                            {
                                                updateClone.find('a.alternate-contact-number').parents('div.primary_contact_number').siblings('div.alternate_contact_container').find('input[name="contact_number[]"]').val(v.number).prop("disabled", true).removeAttr("datavalid");
                                            }

                                            updateClone.find('div.alternate_contact_container div.select div.frm-custom-dropdown').prop("disabled", true);
                                            updateClone.find('div.alternate_contact_container div.select div.frm-custom-dropdown input[name="contact_number_type[]"]').prop("disabled", true);
                                        }
                                        else
                                        {
                                            var oContactTemplate = updateClone.find('div.template.new_contact_number').clone().removeClass('template');
                                            var oContactContainer = updateClone.find('div.alternate_contact_container:first');
                                            oContactTemplate.find('input:not(input[name="contact_number_type[]"])').val('');

                                            if(v.number.indexOf('-') !== -1/*v.number.length >= 7 && v.number.length <= 9*/)
                                            {
                                                var landlineSplit = v.number.split('-');
                                                oContactTemplate.find('div.select div.frm-custom-dropdown input[name="contact_number_type[]"]').val("Landline").attr("value", "Landline");
                                                oContactTemplate.find("input.small").addClass("hidden").val('');
                                                oContactTemplate.find("input.small").removeAttr("datavalid");
                                                oContactTemplate.find("input.xsmall").removeClass("hidden").prop("disabled", true);
                                                oContactTemplate.find("input.xsmall:first").val(landlineSplit[0]);
                                                oContactTemplate.find("input.xsmall:eq(1)").val(landlineSplit[1]);
                                                oContactTemplate.find("input.xsmall:eq(2)").val(landlineSplit[2]);
                                            }
                                            else //if(v.number.length == 11 /*|| v.number.length == 12*/)
                                            {
                                                oContactTemplate.find('input[name="contact_number[]"].small').val(v.number).prop("disabled", true).removeAttr("datavalid");
                                            }

                                            oContactTemplate.find('a.another_remove').hide();
                                            oContactTemplate.find('div.select div.frm-custom-dropdown').prop("disabled", true);
                                            oContactTemplate.find('div.select div.frm-custom-dropdown input[name="contact_number_type[]"]').prop("disabled", true);
                                            if(v.number.length > 0)
                                            {
                                                callcenter.customer_list.clone_append(oContactTemplate, oContactContainer);
                                            }
                                        }
                                    });
                                }

                                /*happy plus*/
                                if(count(value.happy_plus_card) > 0 && typeof(value.happy_plus_card) !== 'undefined')
                                {   
                                    $.each(value.happy_plus_card, function (k, v) {
                                        if(k == 0)
                                        {
                                            updateClone.find('div.happy_plus_card:first').addClass("disabled");
                                            updateClone.find('input[name="number"]:first').val(v.number).prop("disabled", true);
                                            updateClone.find('input[name="expiry_date"]:first').val(v.expiration_date_date).prop("disabled", true);
                                            updateClone.find('input[name="remarks"]:first').val(v.remarks).prop("disabled", true);
                                            updateClone.find("input[name='is_smudged']:first").prop("disabled", true);
                                            updateClone.find('div.happy_plus_card:first div.date-picker').css('pointer-events', 'none');
                                            if(v.is_smudged == 1)
                                            {
                                                updateClone.find("input[name='is_smudged']:first").prop("checked", true);
                                            }
                                        }
                                        else if(k == 1)
                                        {
                                            updateClone.find('div.parent_happy_plus div.happy_plus_card:first a.alternate_happy_plus').addClass("hidden");
                                            updateClone.find('div.another_happy_plus').addClass("disabled");
                                            updateClone.find('div.another_happy_plus').removeClass("hidden");
                                            updateClone.find('div.another_happy_plus').find('input[name="number"]').val(v.number).prop("disabled", true);
                                            updateClone.find('div.another_happy_plus').find('input[name="expiry_date"]').val(v.expiration_date_date).prop("disabled", true);
                                            updateClone.find('div.another_happy_plus').find('input[name="remarks"]').val(v.remarks).prop("disabled", true);
                                            updateClone.find('div.another_happy_plus').find("input[name='is_smudged']").prop("disabled", true);
                                            updateClone.find('div.another_happy_plus div.date-picker').css('pointer-events', 'none');
                                            if(v.is_smudged == 1)
                                            {
                                                updateClone.find("input[name='is_smudged']").prop("checked", true);
                                            }
                                        }
                                        else
                                        {
                                            var oHappyPlusTemplate = updateClone.find('div.parent_happy_plus div.template.new_happy_plus_card').clone().removeClass('template').addClass("disabled");
                                            var oHappyPlusContainer = updateClone.find('div.parent_happy_plus');
                                            var happyPlusQuantity = updateClone.find('div.parent_happy_plus div.happy_plus_card:visible').length + 1;

                                            oHappyPlusTemplate.find('input[name="is_smudged"]').attr('id', 'card-number-' + happyPlusQuantity);
                                            oHappyPlusTemplate.find('label.chck-lbl').attr('for', 'card-number-' + happyPlusQuantity);
                                            oHappyPlusTemplate.find("input[type='text'][name='number']").val(v.number).prop("disabled", true);
                                            oHappyPlusTemplate.find("input[type='text'][name='expiry_date']").val(v.expiration_date_date).prop("disabled", true);
                                            oHappyPlusTemplate.find("input[type='text'][name='remarks']").val(v.remarks).prop("disabled", true);
                                            oHappyPlusTemplate.find("input[name='is_smudged']").prop("disabled", true);
                                            oHappyPlusTemplate.find('div.date-picker').css('pointer-events', 'none');
                                            if(v.is_smudged == 1)
                                            {
                                                oHappyPlusTemplate.find("input[name='is_smudged']").prop("checked", true);
                                            }
                                            oHappyPlusTemplate.find("input[type='text'][name='remarks']").addClass("margin-bottom-15");
                                            oHappyPlusTemplate.find("a.another_remove").hide();
                                            if(updateClone.find('div.parent_happy_plus div.happy_plus_card:not(.template):visible').length > 2)
                                            {
                                                updateClone.find('a.another_happy_plus').removeClass("margin-bottom-15");
                                            }
                                            callcenter.customer_list.clone_append(oHappyPlusTemplate, oHappyPlusContainer);
                                            $(".date-picker").datetimepicker({pickTime: false});
                                        }

                                    })
                                }

                                /*remarks*/
                                callcenter.customer_list.get_remarks(updateClone, value.last_behavior_id, '');
                                // if(typeof(value.remarks) !== 'undefined')
                                // {
                                //     if(count(value.remarks) > 0)
                                //     {   
                                //         $.each(value.remarks, function (k, v) {
                                //             cRemarks += '<div class="option behavior-option" data-value-id="' + v.id + '"><span class="">' + v.display_label + '</span></div>'
                                //             if(v.id == value.last_behavior_id)
                                //             {
                                //                 updateClone.find('input[name="behavior"]').val(v.display_label).attr('last_behavior_id', v.id);
                                //             }
                                //         })
                                //     }
                                //     else
                                //     {
                                //         cRemarks += '<div class="option behavior-option" data-value-id="0"><span class="">No remarks available.</span></div>'
                                //         updateClone.find('input[name="behavior"]').val('No remarks available.').attr('last_behavior_id', '0');
                                //     }

                                //     var oDivs = updateClone.find('div.frm-custom-dropdown.behavior');
                                //     $.each(oDivs, function (k, v) {
                                //         $(this).find('div.frm-custom-dropdown-option').html('').append(cRemarks);
                                //     })
                                // }

                                /*address*/
                                if (count(value.address) > 0 && count(value.address_text) > 0 && typeof(value.address) !== 'undefined' && typeof(value.address_text) !== 'undefined')
                                {
                                    $.each(value.address, function (k, v) {
                                        var house_number = (v.house_number !== '' && v.house_number != "0") ? v.house_number : '',
                                            floor = (v.floor !== '' && v.floor != "0") ? v.floor : '',
                                            building = (value.address_text[k].building !== null) ? value.address_text[k].building.name : '',
                                            barangay = (value.address_text[k].barangay !== null) ? value.address_text[k].barangay.name : '',
                                            subdivision = (value.address_text[k].subdivision !== null) ? value.address_text[k].subdivision.name : '',
                                            city = (value.address_text[k].city !== null) ? value.address_text[k].city.name : '',
                                            street = (value.address_text[k].street !== null) ? value.address_text[k].street.name : '',
                                            second_street = (value.address_text[k].second_street !== null && typeof(value.address_text[k].second_street) != 'undefined') ? value.address_text[k].second_street.name : '',
                                            province = (value.address_text[k].province !== null) ? value.address_text[k].province.name : '';

                                        var uiAddressTemplate = updateClone.find('div.customer_list_default_address_update.template').clone().removeClass('template');

                                        if(v.address_type.toUpperCase() === "HOME")
                                        {
                                            uiAddressTemplate.find('img.default_address_image').attr({'src': callcenter.config('url.server.base') + 'assets/images/Home2.svg', "alt" : "work icon", "class" : "small-thumb"});
                                            uiAddressTemplate.find('img.default_address_image').attr('onError', '$.fn.checkImage(this);');
                                        }
                                        else
                                        {
                                            uiAddressTemplate.find('img.default_address_image').attr({'src': callcenter.config('url.server.base') + 'assets/images/work-icon.png', "alt" : "work icon"});
                                            uiAddressTemplate.find('img.default_address_image').attr('onError', '$.fn.checkImage(this);');
                                        }
                                        uiAddressTemplate.find('strong.default_address_label').text(v.address_label);
                                        uiAddressTemplate.find('p.default_address_complete').text('' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);
                                        uiAddressTemplate.find('strong.default_address_type').text(v.address_type.toUpperCase());
                                        uiAddressTemplate.find('p.default_address_landmark').text('- ' + v.landmark.toUpperCase());
                                        uiAddressTemplate.attr('customer_id', value.id);
                                        uiAddressTemplate.attr('address_id', v.id);
                                        uiAddressTemplate.attr('house_number', v.house_number);
                                        uiAddressTemplate.attr('building', v.building);
                                        uiAddressTemplate.attr('floor', v.floor);
                                        uiAddressTemplate.attr('street', v.street);
                                        uiAddressTemplate.attr('second_street', v.second_street);
                                        uiAddressTemplate.attr('barangay', v.barangay);
                                        uiAddressTemplate.attr('subdivision', v.subdivision);
                                        uiAddressTemplate.attr('city', v.city);
                                        uiAddressTemplate.attr('province', v.province);
                                        uiAddressTemplate.attr('landmark', v.landmark);
                                        uiAddressTemplate.attr('address_label', v.address_label);
                                        uiAddressTemplate.attr('address_type', v.address_type);
                                        uiAddressTemplate.attr('all_address', '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);
                                        uiAddressTemplate.attr('is_default', v.is_default);

                                        if(v.is_deleted == 1)
                                        {
                                            uiAddressTemplate.find('button.archive_address_btn').text("Undo").removeClass("archive_address_btn").addClass("unarchive_address_btn").attr('data-value', '0');
                                        }
                                        else
                                        {
                                            uiAddressTemplate.find('button.archive_address_btn').text("Archive").removeClass("unarchive_address_btn").addClass("archive_address_btn").attr('data-value', '1');
                                        }
                                        /*needed attr for update of address*/
                                        uiAddressTemplate.attr('house_number_text', house_number);
                                        uiAddressTemplate.attr('building_text', building);
                                        uiAddressTemplate.attr('floor_text', floor);
                                        uiAddressTemplate.attr('street_text', street);
                                        uiAddressTemplate.attr('second_street_text', second_street);
                                        uiAddressTemplate.attr('barangay_text', barangay);
                                        uiAddressTemplate.attr('subdivision_text', subdivision);
                                        uiAddressTemplate.attr('city_text', city);
                                        uiAddressTemplate.attr('province_text', province);
                                        uiAddressTemplate.attr('landmark_text', v.landmark);
                                        uiAddressTemplate.attr('address_label_text', v.address_label);
                                        uiAddressTemplate.attr('address_type_text', v.address_type);

                                        if(v.is_default == 1)
                                        {
                                            if(v.is_deleted_date != '0000-00-00 00:00:00' && v.is_deleted == 1)
                                            {
                                                var dateNow = moment().format("YYYY-MM-DD"),
                                                    timeNow = moment().format("HH:mm:ss"),
                                                    is_deleted_fulldate = v.is_deleted_date.split(' '),
                                                    is_deleted_date = is_deleted_fulldate[0].split('-'),
                                                    is_deleted_time = is_deleted_fulldate[1].split(':'),
                                                    time_now = timeNow.split(':');

                                                /*deleted time*/
                                                var is_deleted_hour = is_deleted_time[0],
                                                    is_deleted_min = is_deleted_time[1],
                                                    is_deleted_sec = is_deleted_time[2];

                                                /*time now*/
                                                var hour_now = time_now[0],
                                                    min_now = time_now[1],
                                                    sec_now = time_now[2];

                                                // console.log('DELETED TIME '+is_deleted_hour+':'+is_deleted_min)
                                                // console.log('TIME NOW '+hour_now+':'+min_now)

                                                // console.log(dateNow == is_deleted_fulldate[0])
                                                // console.log(dateNow > is_deleted_fulldate[0]) //do not show address if it is deleted previous date

                                                if(dateNow == is_deleted_fulldate[0])
                                                {
                                                    if( ( parseInt(hour_now) - parseInt(is_deleted_hour) ) > parseInt('1') || ( parseInt(hour_now) - parseInt(is_deleted_hour) ) == parseInt('1') && parseInt(is_deleted_min) <= parseInt(min_now) )
                                                    {
                                                        // console.log("remove/hide address")
                                                    }
                                                    else
                                                    {
                                                        // console.log("undo");
                                                        uiAddressTemplate.find('button.archive_address_btn').text("Undo").removeClass("archive_address_btn").addClass("unarchive_address_btn").attr('data-value', '0');
                                                        uiAddressTemplate.find('strong.default_address_label').html(v.address_label+' - <span class="red-color">Default</span>');
                                                        updateClone.find('div.default_address_container_update').append(uiAddressTemplate);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                uiAddressTemplate.find('strong.default_address_label').html(v.address_label+' - <span class="red-color">Default</span>');
                                                updateClone.find('div.default_address_container_update').append(uiAddressTemplate); 
                                            }
                                        }
                                        else
                                        {
                                            if(v.is_deleted_date != '0000-00-00 00:00:00' && v.is_deleted == 1)
                                            {
                                                var dateNow = moment().format("YYYY-MM-DD"),
                                                    timeNow = moment().format("HH:mm:ss"),
                                                    is_deleted_fulldate = v.is_deleted_date.split(' '),
                                                    is_deleted_date = is_deleted_fulldate[0].split('-'),
                                                    is_deleted_time = is_deleted_fulldate[1].split(':'),
                                                    time_now = timeNow.split(':');

                                                /*deleted time*/
                                                var is_deleted_hour = is_deleted_time[0],
                                                    is_deleted_min = is_deleted_time[1],
                                                    is_deleted_sec = is_deleted_time[2];

                                                /*time now*/
                                                var hour_now = time_now[0],
                                                    min_now = time_now[1],
                                                    sec_now = time_now[2];

                                                if(dateNow == is_deleted_fulldate[0])
                                                {
                                                    if( ( parseInt(hour_now) - parseInt(is_deleted_hour) ) > parseInt('1') || ( parseInt(hour_now) - parseInt(is_deleted_hour) ) == parseInt('1') && parseInt(is_deleted_min) <= parseInt(min_now) )
                                                    {
                                                        // console.log("remove/hide address")
                                                    }
                                                    else
                                                    {
                                                        // console.log("undo");
                                                        uiAddressTemplate.find('button.archive_address_btn').text("Undo").removeClass("archive_address_btn").addClass("unarchive_address_btn").attr('data-value', '0');
                                                        // uiAddressTemplate.removeClass('bggray-light').addClass('white');
                                                        uiAddressTemplate.find('button.set_default_address').removeAttr('disabled');
                                                        updateClone.find('div.secondary_address_container_update').append(uiAddressTemplate);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                // uiAddressTemplate.removeClass('bggray-light').addClass('white');
                                                uiAddressTemplate.find('button.set_default_address').removeAttr('disabled');
                                                updateClone.find('div.secondary_address_container_update').append(uiAddressTemplate);
                                            }
                                        }
                                    });
                                }

                            });
                        }

                    }
                };

                callcenter.customer_list.get(oAjaxConfig);

                var oValidationConfig = {
                        'data_validation'    : 'datavalid',
                        'input_label'        : 'labelinput',
                        'min_length'         : 0,
                        'max_length'         : 75,
                        'class'              : 'error',
                        'add_remove_class'   : true,
                        'Contact Number': 
                        {
                            'min_length': 13
                        },
                        'Landline Number': 
                        {
                            'min_length': 9
                        },
                        'onValidationError'  : function (arrMessages) {
                            var sError = '';
                            var uiContainer = uiTemplate.find('div.update-customer-error-msg');
                            if(typeof(arrMessages) !== 'undefined')
                            {
                                $.each(arrMessages, function (k, v) {
                                    sError += '<p class="error_messages"><i class="fa fa-exclamation-triangle"></i> ' + v.error_message + '</p>';
                                });
                            }
                            uiTemplate.find('div.success-msg').hide();
                            uiTemplate.find('div.error-msg').hide();
                            callcenter.customer_list.add_error_message(sError, uiContainer);
                            callcenter.customer_list.show_spinner($('button.btn-save'), false);
                            $("html, body").animate({scrollTop: updateClone.parents('div.collapsible').offset().top}, 'slow');
                            // //console.log(arrMessages); //shows the errors
                        },
                        'onValidationSuccess': function () {
                            callcenter.customer_list.show_spinner($('button.btn-save'), true);
                            callcenter.customer_list.assemble_update_params(updateClone, uiTemplate);
                        }
                    }
                updateClone.find('form#update-customer').attr('customer_id', oData);
                if(updateClone.find('form#update-customer').attr('customer_id') == oData)
                {
                    callcenter.validate_form(updateClone.find('form#update-customer'), oValidationConfig);
                }
                $(".date-picker").datetimepicker({pickTime: false});
                var date = new Date();
                date.setDate(date.getDate());
                updateClone.find("#birthdate-date-picker").datetimepicker({pickTime: false, maxDate: date });
            }
        },

        'get_remarks': function (updateClone, lastBehaviorId, uiManipulatedTemplate) {
            var oAjaxConfig = {
                "type" : "GET",
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "data"   : {"params": "none"},
                "url"    : callcenter.config('url.api.jfc.customers') + "/customers/behaviors",
                "success": function (oData) {
                    if(typeof(oData) !== 'undefined')
                    {
                        if(updateClone != '')
                        {
                            var cRemarks= '';
                            if(lastBehaviorId != 0)
                            {   
                                $.each(oData, function (k, v) {
                                    cRemarks += '<div class="option behavior-option" data-value-id="' + v.id + '"><span class="">' + v.display_label + '</span></div>'
                                    if(v.id == lastBehaviorId)
                                    {
                                        updateClone.find('input[name="behavior"]').val(v.display_label).attr('last_behavior_id', v.id);
                                    }
                                })
                            }
                            else
                            {
                                cRemarks += '<div class="option behavior-option" data-value-id="0"><span class="">No remarks available.</span></div>'
                                updateClone.find('input[name="behavior"]').val('No remarks available.').attr('last_behavior_id', '0');
                                $.each(oData, function (k, v) {
                                    cRemarks += '<div class="option behavior-option" data-value-id="' + v.id + '"><span class="">' + v.display_label + '</span></div>'
                                })
                            }

                            var oDivs = updateClone.find('div.frm-custom-dropdown.behavior');
                            $.each(oDivs, function (k, v) {
                                $(this).find('div.frm-custom-dropdown-option').html('').append(cRemarks);
                            })
                        }
                        if(uiManipulatedTemplate != '')
                        {
                            if (lastBehaviorId != 0)
                            {
                                $.each(oData, function (k, v) {
                                    if(v.id == lastBehaviorId)
                                    {
                                        uiManipulatedTemplate.find('span.info-remarks').text(v.display_label.toUpperCase());
                                        uiManipulatedTemplate.find('span.info-remarks').css('color', '#' + v.color);
                                        uiManipulatedTemplate.siblings('div.header-search').find('span.info-remarks').text(v.display_label.toUpperCase());
                                        uiManipulatedTemplate.siblings('div.header-search').find('span.info-remarks').css('color', '#' + v.color);
                                    }
                                })
                            }
                            else {
                                uiManipulatedTemplate.find('span.info-remarks').text('NO REMARKS AVAILABLE');
                                uiManipulatedTemplate.find('span.info-remarks').css('color', '#000000');
                                uiManipulatedTemplate.siblings('div.header-search').find('span.info-remarks').text('NO REMARKS AVAILABLE');
                                uiManipulatedTemplate.siblings('div.header-search').find('span.info-remarks').css('color', '#000000');
                            }
                        }
                    }
                }
            }
            callcenter.customer_list.get(oAjaxConfig)
        },

        'add_error_message': function (sMessages, uiContainer) {
            $('p.error_messages').remove();
            if (sMessages.length > 0) {
                if (typeof(uiContainer) !== 'undefined') {
                    uiContainer.empty();
                    uiContainer.show();
                    uiContainer.append(sMessages);
                    uiContainer.find("p.error_messages:contains('is required')").hide();

                    if (uiContainer.find('p.error_messages').is(":contains('is required')")) {
                        uiContainer.append("<p class='error_messages'><i class='fa fa-exclamation-triangle'></i> Please fill up all required fields.</p>");
                    }

                    if($("form#add_address").find('div.select input.dd-txt[name="province"]').hasClass('error'))
                    {   
                        $("form#add_address").find('div.select input.dd-txt[name="province"]').parents('div.select').addClass('has-error');
                    }
                    else
                    {   
                        $("form#add_address").find('div.select input.dd-txt[name="province"]').parents('div.select').removeClass('has-error');
                    }

                    if($("form#add_address").find('div.select input.dd-txt[name="city"]').hasClass('error'))
                    {   
                        $("form#add_address").find('div.select input.dd-txt[name="city"]').parents('div.select').addClass('has-error');
                    }
                    else
                    {   
                        $("form#add_address").find('div.select input.dd-txt[name="city"]').parents('div.select').removeClass('has-error');
                    }

                    // if($("form#add_address").find('div.select input.dd-txt[name="address_type"]').hasClass('error'))
                    // {   
                    //     $("form#add_address").find('div.select input.dd-txt[name="address_type"]').parents('div.select').addClass('has-error');
                    // }
                    // else
                    // {   
                    //     $("form#add_address").find('div.select input.dd-txt[name="address_type"]').parents('div.select').removeClass('has-error');
                    // }
                }

            }
        },

        'assemble_update_params': function (updateClone, uiTemplate) {
            if(typeof(updateClone) !== 'undefined' && typeof(uiTemplate) !== 'undefined')
            {            
                var updateCloneForm = updateClone.find('form#update-customer');

                var aContact = [];

                //primary mobile
                var sPrimaryContactMobile = "";
                var uiPrimaryContactMobile = updateClone.find('div.primary_contact_number input.small[name="contact_number[]"]:visible:enabled');
                if(typeof(uiPrimaryContactMobile)!== 'undefined')
                {
                    $.each(uiPrimaryContactMobile, function () {
                        var sValue = $(this).val().replace(/-/g, '');
                        sPrimaryContactMobile += sValue;
                    });
                    aContact.push(sPrimaryContactMobile);
                }

                //primary landline
                var sPrimaryContactLandline = "";
                var uiPrimaryContactLandline = updateClone.find('div.primary_contact_number input.xsmall[name="contact_number[]"]:visible:enabled');
                if(typeof(uiPrimaryContactLandline)!== 'undefined')
                {
                    var iCounter = 0;
                    $.each(uiPrimaryContactLandline, function () {
                        iCounter++;
                        var sValue = $(this).val().replace(/-/g, '')+'-';
                        sPrimaryContactLandline += sValue;
                        if (iCounter == 3) {

                            if(sPrimaryContactLandline.match(/^\d+-\d+--$/))
                            {
                                sPrimaryContactLandline = sPrimaryContactLandline.slice(0,-2);
                            }
                            else
                            {
                                sPrimaryContactLandline = sPrimaryContactLandline.slice(0,-1);
                            }
                            sPrimaryContactLandline += ",";
                            iCounter = 0;
                        }
                    });
                }
                if(typeof(sPrimaryContactLandline) !== 'undefined')
                {
                    var arsPrimaryContactLandLine = sPrimaryContactLandline.split(",");
                    aContact = aContact.concat(arsPrimaryContactLandLine);
                }

                var uiAlternateContactMobile = updateClone.find('div.new_contact_number input.small[name="contact_number[]"]:visible:enabled');
                if(typeof(uiAlternateContactMobile) !== 'undefined')
                {
                    $.each(uiAlternateContactMobile, function () {
                        var sValue = $(this).val().replace(/-/g, '')
                        aContact.push(sValue);
                    });
                }

                var sAlternateContactLandLine = "";
                var uiAlternateContactLandLine = updateClone.find('div.new_contact_number input.xsmall[name="contact_number[]"]:visible:enabled');
                if(typeof(uiAlternateContactLandLine) !== 'undefined')
                {
                    var iCounter = 0;
                    $.each(uiAlternateContactLandLine, function () {
                        iCounter++;
                        var sValue = $(this).val().replace(/-/g, '')+'-';
                        sAlternateContactLandLine += sValue;
                        if (iCounter == 3) {

                            sAlternateContactLandLine = sAlternateContactLandLine.replace(/--/g, '-')
                            if(sAlternateContactLandLine.lastIndexOf("-") != -1)
                            {
                                sAlternateContactLandLine = sAlternateContactLandLine.slice(0,-1);
                            }

                            sAlternateContactLandLine += ",";
                            iCounter = 0;
                        }
                    });
                }
                if(typeof(sAlternateContactLandLine) !== 'undefined')
                {
                    var arsAlternateContactLandLine = sAlternateContactLandLine.split(",");
                    aContact = aContact.concat(arsAlternateContactLandLine);
                }

                aContact = jQuery.grep(aContact, function(n, i){
                  return (n !== "" && n != null);
                });

                var dateArr = (updateCloneForm.find('input[name="birthdate"]').val() !== '') ? updateCloneForm.find('input[name="birthdate"]').val().split("/"): '';
                if(typeof(dateArr) !== 'undefined' && dateArr !== '')
                {
                    var dateStr = dateArr[2] + "-" + dateArr[0] + "-" + dateArr[1];
                }
                else
                {
                    dateStr = '0000-00-00';
                }

                var oUpdateParams = {
                        "params": {
                            "data" : [
                               {
                                 "field" : "first_name",
                                 "value" : updateCloneForm.find('input[name="first_name"]').val()
                               },
                               {
                                 "field" : "middle_name",
                                 "value" : updateCloneForm.find('input[name="middle_name"]').val()
                               },
                               {
                                 "field" : "last_name",
                                 "value" : updateCloneForm.find('input[name="last_name"]').val()
                               },
                               {
                                 "field" : "birthdate",
                                 "value" : dateStr
                               },
                               {
                                 "field" : "email_address",
                                 "value" : updateCloneForm.find('input[name="email_address[]"]').val()
                               },
                               // {
                               //   "field" : "position",
                               //   "value" : updateCloneForm.find('input[name="position"]').val()
                               // },
                               // {
                               //   "field" : "company",
                               //   "value" : updateCloneForm.find('input[name="company"]').val()
                               // },
                               // {
                               //   "field" : "last_order_date",
                               //   "value" : updateCloneForm.find('input[name="last_order_date"]').val()
                               // },
                               {
                                 "field" : "last_behavior_id",
                                 "value" : updateCloneForm.find('input[name="behavior"]').attr('last_behavior_id')
                               },
                               {
                                 "field" : "is_senior",
                                 "value" : updateCloneForm.find('input[type="radio"][name="senior"]:checked').val()
                               },
                               {
                                 "field" : "is_pwd",
                                 "value" : updateCloneForm.find('input[name="pwd"]:checked').val()
                               },
                               {
                                 "field" : "osca_number",
                                 "value" : updateCloneForm.find('input[name="osca_number"]').val()
                               },
                               {
                                 "field" : "is_loyalty_card_holder",
                                 "value" : updateCloneForm.find('input[name="loyalty"]:checked').val()
                               }
                               ],
                            "customer_id": updateCloneForm.attr('customer_id'),
                            "contact_number": aContact,
                            "qualifiers" : [
                                {
                                 "field" : "id",
                                 "value" : updateCloneForm.attr('customer_id'),
                                 "operator" : "="
                                }
                            ]
                        }
                    }

                var oHappyPlusCards = [];
                var uiHappyPlusCards = updateCloneForm.find('div.happy_plus_card:not(.disabled):not(.template):visible');
                if(typeof(uiHappyPlusCards) !== 'undefined')
                {
                    $.each(uiHappyPlusCards, function () {
                        var oHappyPlusCard = {
                            'number'              : $(this).find('input[name="number"]:first').val(),
                            'expiration_date_date': $(this).find('input[name="expiry_date"]:first').val(),
                            'is_smudged'          : $(this).find('input[name="is_smudged"]:checked').val(),
                            'remarks'             : $(this).find('input[name="remarks"]:first').val()
                        }
                        oHappyPlusCards.push(oHappyPlusCard);
                    })
                }

                var oUpdateConfig = {
                    "type"   : "POST",
                    "data"   : oUpdateParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.customers') + "/customers/update",
                    "success": function (oUpdateData) {
                        callcenter.customer_list.show_spinner($('button.btn-save'), false);
                        if(typeof(oUpdateData) !== 'undefined')
                        {
                            if(oUpdateData.status == true)
                            {
                                if(updateCloneForm.find('input[name="behavior"]').attr('last_behavior_id') != 0)
                                {
                                    callcenter.customer_list.insert_behavior(updateCloneForm.attr('customer_id'), updateCloneForm.find('input[name="behavior"]').attr('last_behavior_id'));
                                }
                                
                                if(oHappyPlusCards.length > 0)
                                {
                                    $.each(oHappyPlusCards, function (k ,v) {
                                        var oHappyPlusConfig = {
                                            "type"   : "POST",
                                            "data"   : v,
                                            "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                                            "url"    : callcenter.config('url.api.jfc.happy_plus') + "happy_plus/add",
                                            "success": function (oHappyData) {
                                                if(oHappyData.status == true)
                                                {
                                                    callcenter.customer_list.insert_happy_plus_map(updateCloneForm, oHappyData.data.happy_plus_id);
                                                    callcenter.customer_list.assemble_customer_info_success(updateCloneForm, uiTemplate, oUpdateData, updateClone);
                                                }
                                                if(oHappyData.status == false)
                                                {
                                                    callcenter.customer_list.insert_happy_plus_map(updateCloneForm, oHappyData.data.happy_plus_id);
                                                    callcenter.customer_list.assemble_customer_info_success(updateCloneForm, uiTemplate, oUpdateData, updateClone);
                                                }
                                            }
                                        }
                                        callcenter.customer_list.get(oHappyPlusConfig);
                                    });
                                }

                                if(oHappyPlusCards.length == 0)
                                {
                                    callcenter.customer_list.assemble_customer_info_success(updateCloneForm, uiTemplate, oUpdateData, updateClone);
                                }
                            }
                            else
                            {
                                updateCloneForm.parents('div#update_customer_content').find('div.success-msg').hide();
                                //updateCloneForm.parents('div#update_customer_content').find('div.update-customer-error-msg').empty().html('<i class="fa fa-exclamation-triangle"></i> '+data.message.error).show();
                                var sError = '';
                                $.each(oUpdateData.message.error, function (k, v) {
                                    sError += '<p class="error_messages"><i class="fa fa-exclamation-triangle"></i> ' + v + '</p>';
                                });
                                updateCloneForm.parents('div#update_customer_content').find('div.update-customer-error-msg').empty().html(sError).show();

                                $("html, body").animate({scrollTop: updateCloneForm.parents('div.collapsible').offset().top}, 'slow');
                                
                                if(oUpdateData.message.error == 'Invalid Email address')
                                {
                                    updateCloneForm.find('input[name="email_address[]"]').addClass('error');
                                }
                                else
                                {
                                    updateCloneForm.find('input[name="email_address[]"]').removeClass('error');
                                }

                                if(oUpdateData.message.error == 'Middle name invalid or is too short')
                                {
                                    updateCloneForm.find('input[name="middle_name"]').addClass('error');
                                }
                                else
                                {
                                    updateCloneForm.find('input[name="middle_name"]').removeClass('error');
                                }

                                if(oUpdateData.message.error == 'Please specify birthdate')
                                {
                                    updateCloneForm.find('input[name="birthdate"]').addClass('error');
                                }
                                else
                                {
                                    updateCloneForm.find('input[name="birthdate"]').removeClass('error');
                                }
                            }
                        }

                    }
                };
                callcenter.customer_list.get(oUpdateConfig);
            }
        },

        'insert_happy_plus_map': function (updateCloneForm, happy_plus_id) {
            if(typeof(updateCloneForm) !== 'undefined' && typeof(happy_plus_id) !== '')
            {
                var oMapConfig = {
                    "data" : []
                }

                oMapConfig.data.push({
                    "customer_id": updateCloneForm.attr('customer_id'),
                    "happy_plus_id": happy_plus_id
                });

                var oHappyPlusMapConfig = {
                    "type"   : "POST",
                    "data"   : oMapConfig,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.happy_plus') + "happy_plus/map",
                    "success": function (oHappyMapData) {

                    }
                }
                callcenter.customer_list.get(oHappyPlusMapConfig);
            }
        },

        'assemble_customer_info_success': function (updateCloneForm, uiTemplate, oUpdateData, updateClone){
            if(typeof(updateCloneForm) !== 'undefined' && typeof(uiTemplate) !== 'undefined' && typeof(oUpdateData) !== 'undefined' && typeof(updateClone) !== 'undefined')
            {
                updateCloneForm.find('input[name="birthdate"]').removeClass('error');
                updateCloneForm.find('input[name="middle_name"]').removeClass('error');
                updateCloneForm.find('input[name="email_address[]"]').removeClass('error');
                updateCloneForm.parents('div#update_customer_content').find('div.update-customer-error-msg').hide();
                updateCloneForm.parents('div#update_customer_content').hide();

                $("html, body").animate({scrollTop: uiTemplate.offset().top}, 'slow');
                uiTemplate.find('div.second_header').find('div.success-msg').empty().html('<i class="fa fa-exclamation-triangle"></i> '+oUpdateData.message[0]).show().delay(3000).fadeOut();;
                uiTemplate.find('div.second_header').show();
                uiTemplate.find('div.second_header').siblings().not(updateClone).show();

                var oGetCustomerParam = {
                    "params": {
                        "where": [
                            {
                                "field"   : 'c.id',
                                "operator": "=",
                                "value"   : updateCloneForm.attr('customer_id')
                            }
                        ]
                    }
                }

                var oGetCustomerConfig = {
                    "type"   : "Get",
                    "data"   : oGetCustomerParam,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.customers') + "/customers/get",
                    "success": function (oCustomerData) {
                        if(typeof(oCustomerData) !== 'undefined')
                        {
                            $.each(oCustomerData.data, function (key, value) {
                                /*name*/
                                uiTemplate.find('strong.info_name').text(value.first_name + ' ' + value.middle_name + ' ' + value.last_name);

                                /*contact numbers*/
                                if (count(value.contact_number) > 0 && typeof(value.contact_number) !== 'undefined') {
                                    var fCarrier = '';
                                    if (typeof(value.contact_number[0].name) != 'undefined') {
                                        if(value.contact_number[0].number.indexOf('-') != -1)
                                        {
                                            fCarrier = 'Landline'; //if landline put landline in carrier prefix
                                        }
                                        else
                                        {
                                            fCarrier = ((value.contact_number[0].name !== 'None')? value.contact_number[0].name : 'Unknown'); //if mobile put unknown
                                        }
                                    }
                                    uiTemplate.find('strong.contact_number').text('Contact Num: ' + value.contact_number[0].number + ' ' + fCarrier);

                                    var uiNumber = '',
                                    //     phone = '',
                                    //     mobile = '',
                                    //     mNumber = uiTemplate.find('div.customer_list_client_info p.customer_list_contact_number_mobile'),
                                    //     pNumber = uiTemplate.find('div.customer_list_client_info p.customer_list_contact_number_phone'),
                                        uiNumContainer = uiTemplate.find('div.customer_list_client_info div.customer_list_contact_number_container');
                                        uiNumContainer.find('p').remove();
                                        uiNumContainer.find('div.clear').remove();
                                    $.each(value.contact_number, function (k, v) {
                                        var sCarrier = '';
                                        if (typeof(v.name) != 'undefined') {
                                            if(v.number.indexOf('-') != -1)
                                            {
                                                sCarrier = 'Landline'; //if landline put landline in carrier prefix
                                            }
                                            else
                                            {
                                                sCarrier = ((v.name !== 'None') ? v.name : 'Unknown' ); //if mobile put unknown
                                            }
                                        }
                                        if(v.number.indexOf('-') !== -1/*v.number.length >= 7 && v.number.length <= 9*/)
                                        {
                                            uiNumber = '<p class="f-right font-12">'+ v.number+" <i class='fa fa-phone margin-right-5'></i> "+sCarrier+"<div class='clear'></div>";
                                        }
                                        else //if(v.number.length == 11/* || v.number.length == 12*/)
                                        {
                                            uiNumber = '<p class="f-right font-12">'+v.number+" <i class='fa fa-mobile margin-right-5'></i> "+sCarrier+"<div class='clear'></div>";
                                        }
                                        uiNumContainer.append(uiNumber);
                                    });

                                    // mNumber.empty().append(mobile);
                                    // pNumber.empty().append(phone);
                                }

                                /*email address*/
                                if(count(value.email_address) > 0)
                                {
                                    uiTemplate.find('p.customer_list_email_address').text(value.email_address);
                                }
                                else
                                {
                                    uiTemplate.find('p.customer_list_email_address').text('');                   
                                }

                                /*date of birth*/
                                if(count(value.birthdate) > 0)
                                {
                                    uiTemplate.find('p.customer_list_date_of_birth').text(value.birthdate);
                                }
                                else
                                {
                                    uiTemplate.find('p.customer_list_date_of_birth').text('');                   
                                }
                                    
                                /*remarks*/
                                // callcenter.customer_list.get_remarks('', value.last_behavior_id, uiTemplate);
                                // if (count(value.remarks) > 0) {
                                //     $.each(value.remarks, function (k, v) {
                                //         if(v.id == value.last_behavior_id)
                                //         {
                                //             uiTemplate.find('span.info-remarks').text(v.display_label.toUpperCase());
                                //             uiTemplate.find('span.info-remarks').css('color', '#' + v.color);
                                //             uiTemplate.siblings('div.header-search').find('span.info-remarks').text(v.display_label.toUpperCase());
                                //             uiTemplate.siblings('div.header-search').find('span.info-remarks').css('color', '#' + v.color);
                                //         }
                                //     })
                                // }
                                // else {
                                //     uiTemplate.find('span.info-remarks').text('NO REMARKS AVAILABLE');
                                //     uiTemplate.find('span.info-remarks').css('color', '#000000');
                                //     uiTemplate.siblings('div.header-search').find('span.info-remarks').text('NO REMARKS AVAILABLE');
                                //     uiTemplate.siblings('div.header-search').find('span.info-remarks').css('color', '#000000');
                                // }
                                if (value.color != null && value.display_label != null) {
                                    uiTemplate.find('span.info-remarks').text(value.display_label.toUpperCase());
                                    uiTemplate.find('span.info-remarks').css('color', '#' + value.color);
                                    uiTemplate.siblings('div.header-search').find('span.info-remarks').text(value.display_label.toUpperCase());
                                    uiTemplate.siblings('div.header-search').find('span.info-remarks').css('color', '#' + value.color);
                                }
                                else {
                                    uiTemplate.find('span.info-remarks').text('NO REMARKS AVAILABLE');
                                    uiTemplate.find('span.info-remarks').css('color', '#000000');
                                    uiTemplate.siblings('div.header-search').find('span.info-remarks').text('NO REMARKS AVAILABLE');
                                    uiTemplate.siblings('div.header-search').find('span.info-remarks').css('color', '#000000');
                                }

                                /*discounts*/
                                if(typeof(value.osca_number) != 'undefined')
                                {
                                    if(value.osca_number.length > 0)
                                    {
                                        uiTemplate.find('div.customer_list_discounts.template').siblings('label.discounts').show();
                                        uiTemplate.find('div.customer_list_discounts.template').removeClass('template');
                                        uiTemplate.find('div.customer_list_discounts p.customer_list_discounts_name').text(value.first_name+" "+value.last_name);
                                        uiTemplate.find('div.customer_list_discounts strong.customer_list_discounts_number').empty().html('<span class="red-color">OSCA Number: </span>'+value.osca_number);
                                    }
                                    else
                                    {
                                        uiTemplate.find('div.customer_list_discounts').after('<div class="clear"></div>');
                                        uiTemplate.find('div.customer_list_discounts').siblings('label.discounts').hide();
                                    }
                                }
                                 else
                                {
                                    uiTemplate.find('div.customer_list_discounts').after('<div class="clear"></div>');
                                    uiTemplate.find('div.customer_list_discounts').siblings('label.discounts').hide();
                                }

                                /*happy plus cards*/
                                if (count(value.happy_plus_card) > 0 && typeof(value.happy_plus_card) !== 'undefined') {
                                    uiTemplate.find('div.customer_list_cards_container div.customer_list_cards:not(.template)').remove();
                                    $.each(value.happy_plus_card, function (k, v) {
                                        var uiHappyPlusTemplate = uiTemplate.find('div.customer_list_cards.template').clone().removeClass('template');
                                        uiHappyPlusTemplate.find('p.customer_list_card_number').text(v.number);
                                        uiHappyPlusTemplate.find('p.customer_list_card_number_expiration').text(v.expiration_date_date);
                                        uiHappyPlusTemplate.attr({'hpc_id': v.id, 'hpc_number' : v.number,'hpc_date' : v.expiration_date_date, 'hpc_remarks': v.remarks})
                                        callcenter.customer_list.clone_append(uiHappyPlusTemplate, uiTemplate.find('div.customer_list_cards_container'));
                                    })
                                }
                                else
                                {
                                    uiTemplate.find('div.customer_list_cards_container').siblings('label.cards').hide();
                                }

                                /*address*/
                                if (count(value.address) > 0 && count(value.address_text) > 0 && typeof(value.address) !== 'undefined' && typeof(value.address_text) !== 'undefined') {
                                    uiTemplate.find('div.customer_list_default_address:not(.template)').remove();
                                    uiTemplate.find('div.other_address_container div.customer_list_address:not(.template)').remove();
                                    uiTemplate.find('div.default_address_container div.customer_list_address:not(.template)').remove();
                                    uiTemplate.find('div.default_address_container div.customer_list_default_address:not(.template)').remove();
                                    $.each(value.address, function (k, v) {
                                        var house_number = (v.house_number !== '' && v.house_number != "0") ? v.house_number : '',
                                            floor = (v.floor !== '' && v.floor != "0") ? v.floor : '',
                                            building = (value.address_text[k].building !== null) ? value.address_text[k].building.name : '',
                                            barangay = (value.address_text[k].barangay !== null) ? value.address_text[k].barangay.name : '',
                                            subdivision = (value.address_text[k].subdivision !== null) ? value.address_text[k].subdivision.name : '',
                                            city = (value.address_text[k].city !== null) ? value.address_text[k].city.name : '',
                                            street = (value.address_text[k].street !== null) ? value.address_text[k].street.name : '',
                                            second_street = (value.address_text[k].second_street !== null && typeof(value.address_text[k].second_street) != 'undefined') ? value.address_text[k].second_street.name : '',
                                            province = (value.address_text[k].province !== null) ? value.address_text[k].province.name : '';

                                        var uiAddressTemplate = uiTemplate.find('div.customer_list_address.template').clone().removeClass('template');


                                        if(v.address_type.toUpperCase() === "HOME")
                                        {
                                            uiAddressTemplate.find('img.address_image').attr({'src': callcenter.config('url.server.base') + 'assets/images/Home2.svg', "alt" : "work icon", "class" : "small-thumb"});
                                            uiAddressTemplate.find('img.address_image').attr('onError', '$.fn.checkImage(this);');
                                        }
                                        else
                                        {
                                            uiAddressTemplate.find('img.address_image').attr({'src': callcenter.config('url.server.base') + 'assets/images/work-icon.png', "alt" : "work icon"});
                                            uiAddressTemplate.find('img.address_image').attr('onError', '$.fn.checkImage(this);');
                                        }
                                        uiAddressTemplate.find('strong.address_label').text(v.address_label);
                                        uiAddressTemplate.find('p.address_complete').text('' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);
                                        uiAddressTemplate.find('strong.address_type').text(v.address_type.toUpperCase());
                                        uiAddressTemplate.find('p.address_landmark').text('- ' + v.landmark.toUpperCase());
                                        uiAddressTemplate.attr('customer_id', i.id);
                                        uiAddressTemplate.attr('address_id', v.id);
                                        uiAddressTemplate.attr('house_number', v.house_number);
                                        uiAddressTemplate.attr('building', v.building);
                                        uiAddressTemplate.attr('floor', v.floor);
                                        uiAddressTemplate.attr('street', v.street);
                                        uiAddressTemplate.attr('second_street', v.second_street);
                                        uiAddressTemplate.attr('barangay', v.barangay);
                                        uiAddressTemplate.attr('subdivision', v.subdivision);
                                        uiAddressTemplate.attr('city', v.city);
                                        uiAddressTemplate.attr('province', v.province);
                                        uiAddressTemplate.attr('address_label', v.address_label);
                                        uiAddressTemplate.attr('landmark', v.landmark);
                                        uiAddressTemplate.attr('address_type', v.address_type);
                                        uiAddressTemplate.attr('all_address', '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);

                                        if (v.is_default == 1)
                                        {
                                            // var all_address = '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province;
                                            // uiTemplate.siblings('div.header-search').find('div.remarks_and_address p.address').empty().html('<span class="red-color info_address"><strong class="">Address: </strong></span>'+all_address);
                                            // uiAddressTemplate.find('strong.address_label').html(v.address_label+' - <span class="red-color">Default</span>');
                                            // uiTemplate.find('div.default_address_container').append(uiAddressTemplate);

                                            if(v.is_deleted_date != '0000-00-00 00:00:00' && v.is_deleted == 1)
                                            {
                                                var dateNow = moment().format("YYYY-MM-DD"),
                                                    timeNow = moment().format("HH:mm:ss"),
                                                    is_deleted_fulldate = v.is_deleted_date.split(' '),
                                                    is_deleted_date = is_deleted_fulldate[0].split('-'),
                                                    is_deleted_time = is_deleted_fulldate[1].split(':'),
                                                    time_now = timeNow.split(':');

                                                /*deleted time*/
                                                var is_deleted_hour = is_deleted_time[0],
                                                    is_deleted_min = is_deleted_time[1],
                                                    is_deleted_sec = is_deleted_time[2];

                                                /*time now*/
                                                var hour_now = time_now[0],
                                                    min_now = time_now[1],
                                                    sec_now = time_now[2];

                                                if(dateNow == is_deleted_fulldate[0])
                                                {
                                                    if( ( parseInt(hour_now) - parseInt(is_deleted_hour) ) > parseInt('1') || ( parseInt(hour_now) - parseInt(is_deleted_hour) ) == parseInt('1') && parseInt(is_deleted_min) <= parseInt(min_now) )
                                                    {
                                                        // console.log("remove/hide address")
                                                    }
                                                    else
                                                    {
                                                        // console.log("undo");
                                                        var all_address = '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province;
                                                        uiTemplate.siblings('div.header-search').find('div.remarks_and_address p.address').empty().html('<span class="red-color info_address"><strong class="">Address: </strong></span>'+all_address);
                                                        uiAddressTemplate.find('strong.address_label').html(v.address_label+' - <span class="red-color">Default</span>');
                                                        uiTemplate.find('div.default_address_container').append(uiAddressTemplate);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                var all_address = '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province;
                                                uiTemplate.siblings('div.header-search').find('div.remarks_and_address p.address').empty().html('<span class="red-color info_address"><strong class="">Address: </strong></span>'+all_address);
                                                uiAddressTemplate.find('strong.address_label').html(v.address_label+' - <span class="red-color">Default</span>');
                                                uiTemplate.find('div.default_address_container').append(uiAddressTemplate);
                                            }
                                        }
                                        else
                                        {
                                            // // uiAddressTemplate.removeClass('bggray-light').addClass('white');
                                            // uiTemplate.find('div.other_address_container').append(uiAddressTemplate);

                                            if(v.is_deleted_date != '0000-00-00 00:00:00' && v.is_deleted == 1)
                                            {
                                                var dateNow = moment().format("YYYY-MM-DD"),
                                                    timeNow = moment().format("HH:mm:ss"),
                                                    is_deleted_fulldate = v.is_deleted_date.split(' '),
                                                    is_deleted_date = is_deleted_fulldate[0].split('-'),
                                                    is_deleted_time = is_deleted_fulldate[1].split(':'),
                                                    time_now = timeNow.split(':');

                                                /*deleted time*/
                                                var is_deleted_hour = is_deleted_time[0],
                                                    is_deleted_min = is_deleted_time[1],
                                                    is_deleted_sec = is_deleted_time[2];

                                                /*time now*/
                                                var hour_now = time_now[0],
                                                    min_now = time_now[1],
                                                    sec_now = time_now[2];

                                                if(dateNow == is_deleted_fulldate[0])
                                                {
                                                    if( ( parseInt(hour_now) - parseInt(is_deleted_hour) ) > parseInt('1') || ( parseInt(hour_now) - parseInt(is_deleted_hour) ) == parseInt('1') && parseInt(is_deleted_min) <= parseInt(min_now) )
                                                    {
                                                        // console.log("remove/hide address")
                                                    }
                                                    else
                                                    {
                                                        // console.log("undo");
                                                        // uiAddressTemplate.removeClass('bggray-light').addClass('white');
                                                        uiTemplate.find('div.other_address_container').append(uiAddressTemplate);
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                // uiAddressTemplate.removeClass('bggray-light').addClass('white');
                                                uiTemplate.find('div.other_address_container').append(uiAddressTemplate);
                                            }
                                        }
                                    })
                                }
                            })
                        }
                    }
                }
                callcenter.customer_list.get(oGetCustomerConfig);
            }
        },

        'insert_happy_plus': function (updateCloneForm, oHappyPlusCards){
            var oHappyDataStatus = [];
            $.each(oHappyPlusCards, function (k ,v) {
                var oHappyPlusConfig = {
                    "type"   : "POST",
                    "data"   : v,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.happy_plus') + "happy_plus/add",
                    "success": function (oHappyData) {
                        oHappyDataStatus.push({'status' : oHappyData.status});
                        //console.log(oHappyDataStatus);
                        //console.log(oHappyData.status);
                        if(oHappyData.status == true)
                        {
                            var oMapConfig = {
                                "data" : []
                            }

                            oMapConfig.data.push({
                                "customer_id": updateCloneForm.attr('customer_id'),
                                "happy_plus_id": oHappyData.data.happy_plus_id
                            });

                            var oHappyPlusMapConfig = {
                                "type"   : "POST",
                                "data"   : oMapConfig,
                                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                                "url"    : callcenter.config('url.api.jfc.happy_plus') + "happy_plus/map",
                                "success": function (oHappyMapData) {

                                }
                            }
                            callcenter.customer_list.get(oHappyPlusMapConfig);
                        }
                        else
                        {
                            updateCloneForm.parents('div#update_customer_content').find('div.success-msg').hide();
                            var sError = '';
                            $.each(oHappyData.message, function (k, v) {
                                sError += '<p class="error_messages"><i class="fa fa-exclamation-triangle"></i> ' + v + '</p>';
                            });
                            updateCloneForm.parents('div#update_customer_content').find('div.update-customer-error-msg').empty().html(sError).show();
                            $("html, body").animate({scrollTop: updateCloneForm.parents('div.collapsible').offset().top}, 'slow');
                        }
                    }
                }
                callcenter.customer_list.get(oHappyPlusConfig);
            });
            return oHappyDataStatus;
        },

        'insert_behavior': function (customerId, lastBehaviorId){
            if(typeof(customerId) !== 'undefined' && typeof(lastBehaviorId) !== 'undefined')
            {
                var oInsertBehaviorParams = {
                        "params": [{
                            "customer_id" : customerId,
                            "difficulty_level" : lastBehaviorId,
                            "set_by" : iConstantUserId
                        }]
                }
                var oInsertBehaviorConfig = {
                                "type"   : "POST",
                                "data"   : oInsertBehaviorParams,
                                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                                "url"    : callcenter.config('url.api.jfc.customers') + "/customers/tag_difficulty_to_handle",
                                "success": function (oData) {
                                    // //console.log(oData)
                                    // //console.log(oData.message[0]);
                                }
                }
                callcenter.customer_list.get(oInsertBehaviorConfig);
            }
        },

        'regenerate_ids': function (uiTemplate, value) {
            if (typeof(uiTemplate) !== 'undefined') {
                /*because of the schmuck plugin of radio buttons that is based on ids LOLLLL*/
                var iIDnumber = Math.floor((Math.random() * 10000000000000000000) + 1);
                var uiInputSenior = uiTemplate.find('input[name="senior"]');
                var uiInputPWD = uiTemplate.find('input[name="pwd"]');
                var uiInputHappyPlus = uiTemplate.find('input[name="happyplus"]');
                var uiInputLoyaltyCard = uiTemplate.find('input[name="loyalty"]');
                var uiInputSmudged = uiTemplate.find('input[name="is_smudged"]');

                $(uiInputSenior[0]).attr('id', 'senior1' + iIDnumber)
                uiTemplate.find('label.senior1').attr('for', 'senior1' + iIDnumber);
                $(uiInputSenior[1]).attr('id', 'senior2' + iIDnumber)
                uiTemplate.find('label.senior2').attr('for', 'senior2' + iIDnumber);

                if(value.is_senior == 1)
                {
                    $(uiInputSenior[0]).prop('checked', true);
                    $(uiInputSenior[0]).val('1');
                    $(uiInputSenior[1]).val('0');
                    uiTemplate.find('div.senior_citizen').show();
                    uiTemplate.find("input[name='osca_number']").attr({'labelinput': 'Osca Number', 'datavalid' : 'required'});
                }
                else
                {
                    $(uiInputSenior[1]).prop('checked', true);
                    $(uiInputSenior[0]).val('1');
                    $(uiInputSenior[1]).val('0');
                    uiTemplate.find('div.senior_citizen').hide();
                    uiTemplate.find("input[name='osca_number']").removeAttr('labelinput datavalid');
                }

                $(uiInputPWD[0]).attr('id', 'pwd1' + iIDnumber)
                uiTemplate.find('label.pwd1').attr('for', 'pwd1' + iIDnumber);
                $(uiInputPWD[1]).attr('id', 'pwd2' + iIDnumber)
                uiTemplate.find('label.pwd2').attr('for', 'pwd2' + iIDnumber);

                if(value.is_pwd == 1)
                {
                    $(uiInputPWD[0]).prop('checked', true);
                    $(uiInputPWD[0]).val('1');
                    $(uiInputPWD[1]).val('0');
                }
                else
                {
                    $(uiInputPWD[1]).prop('checked', true);
                    $(uiInputPWD[0]).val('1');
                    $(uiInputPWD[1]).val('0');
                }

                $(uiInputHappyPlus[0]).attr('id', 'happy1' + iIDnumber)
                uiTemplate.find('label.happy1').attr('for', 'happy1' + iIDnumber);
                $(uiInputHappyPlus[1]).attr('id', 'happy2' + iIDnumber)
                uiTemplate.find('label.happy2').attr('for', 'happy2' + iIDnumber);

                if(value.happy_plus_card.length > 0)
                {
                    $(uiInputHappyPlus[0]).prop('checked', true);
                    $(uiInputHappyPlus[0]).val('1');
                    $(uiInputHappyPlus[1]).val('0');
                    uiTemplate.find('div.happy_plus_card:not(.template):first').show();
                    uiTemplate.find("[name='number']").attr({'labelinput': 'Card Number','datavalid' : 'required'});
                    uiTemplate.find("[name='expiry_date']").attr({'labelinput': 'Expiry Date','datavalid' : 'required'});
                }
                else
                {
                    $(uiInputHappyPlus[1]).prop('checked', true);
                    $(uiInputHappyPlus[0]).val('1');
                    $(uiInputHappyPlus[1]).val('0');
                    uiTemplate.find('div.happy_plus_card').hide();
                    uiTemplate.find("[name='number']").removeAttr('labelinput datavalid');
                    uiTemplate.find("[name='expiry_date']").removeAttr('labelinput datavalid');
                }

                $(uiInputLoyaltyCard[0]).attr('id', 'loyalty1' + iIDnumber)
                uiTemplate.find('label.loyalty1').attr('for', 'loyalty1' + iIDnumber);
                $(uiInputLoyaltyCard[1]).attr('id', 'loyalty2' + iIDnumber)
                uiTemplate.find('label.loyalty2').attr('for', 'loyalty2' + iIDnumber);
             
                if(value.is_loyalty_card_holder == 1)
                {
                    $(uiInputLoyaltyCard[0]).prop('checked', true);
                    $(uiInputLoyaltyCard[0]).val('1');
                    $(uiInputLoyaltyCard[1]).val('0');
                }
                else
                {
                    $(uiInputLoyaltyCard[1]).prop('checked', true);
                    $(uiInputLoyaltyCard[0]).val('1');
                    $(uiInputLoyaltyCard[1]).val('0');
                }

                $(uiInputSmudged[0]).attr('id', 'card-number-1-' + iIDnumber)
                uiTemplate.find('label.is_smudged1').attr('for', 'card-number-1-' + iIDnumber);
                $(uiInputSmudged[1]).attr('id', 'card-number-2-' + iIDnumber)
                uiTemplate.find('label.is_smudged2').attr('for', 'card-number-2-' + iIDnumber);

            }

            return uiTemplate
        },

        'assemble_search_param': function () {
            $('div.list').remove();
            var oParams = {
                "params": {
                    "offset":0,
                    "where_like": [
                        {
                            "field"   : function () {
                                var uiSearchByValue = $('section #customer_list_header input#customer_list_search_by').val();
                                if (uiSearchByValue == "Contact Number") {
                                    uiSearchByValue = 'ccn.number';
                                }
                                else {
                                    uiSearchByValue = 'c.first_name';
                                }

                                return uiSearchByValue;
                            },
                            "operator": "LIKE",
                            "value"   : function () {
                                return $('section#customer_list_header #search_customer_list').val()
                            }
                        }
                    ],

                    "where" : [],
                    "order_by": "c.first_name",
                    "sorting": "asc",

                }
            };

            if ($("section #customer_list_header #customer_list_search_by_province").find("input.dd-txt").attr("value") !="All Province" ) {
                    oParams.params.where =[];
                    oParams.params.where.push(
                        {
                            "field"   : 'ca.province',
                            "operator": "=",
                            "value"   : function () {
                                return $("section #customer_list_header #customer_list_search_by_province").find("input.dd-txt").attr("value");
                            }
                        }
                    );
            }

            if ($("section #customer_list_header #customer_list_search_by_last_ordered").find("input.dd-txt").attr("value")!="") {
                var last_order_date_filter = $("section #customer_list_header #customer_list_search_by_last_ordered").find("input.dd-txt").attr("value");
                oParams.params.last_order_date_filter = last_order_date_filter;
            }

            if ($("section #customer_list_header #customer_list_search_by_behavior").find("input.dd-txt").attr("value")!="Show All Customer") {
                var last_behavior_id_filter = $("section #customer_list_header #customer_list_search_by_behavior").find("input.dd-txt").attr("value");
                oParams.params.last_behavior_id_filter = last_behavior_id_filter;
            }
            
            var oAjaxConfig = {
                "type"   : "Get",
                "data"   : oParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : callcenter.config('url.api.jfc.customers') + "/customers/search",
                "success": function (data) {
                    callcenter.customer_list.show_spinner($('button.search_button'), false);
                    ////console.log(data);
                    //clear search box
                    // $('section#customer_list_header input#search_customer_list').val(""); 

                    if (count(data.data) > 0) {
                            
                            $('p.count_search_result strong').text('Search Result(s) | ' + count(data.data) + ' Customers');
                            callcenter.customer_list.content_panel_toggle('customer_list');
                            var uiContainer = $('div#customer_list_content div.search-container');
                            uiContainer.html('');
                            $.each(data.data, function (key, value) {
                                var uiTemplate = $('div#customer_list_search_result.template').clone().removeClass('template');
                                var uiManipulatedTemplate = callcenter.customer_list.manipulate_template(uiTemplate, value);
                                uiManipulatedTemplate.find('div.customer_secondary_address').hide();
                                callcenter.customer_list.clone_append(uiManipulatedTemplate, uiContainer);
                            })
                        }
                        else {
                            $('p.count_search_result').find('strong').text('No Search Result');
                            callcenter.customer_list.content_panel_toggle('no_search_customer');
                        }
                            var uiContainer = $('div.search-container');
                            uiContainer.html('');
                            $.each(data.data, function (key, value) {
                                var uiTemplate = $('div.search_result_customer_list.template').clone().removeClass('template');
                                var uiManipulatedTemplate = callcenter.customer_list.manipulate_template(uiTemplate, value);
                                uiManipulatedTemplate.find('div.customer_secondary_address').hide();
                                callcenter.customer_list.clone_append(uiManipulatedTemplate, uiContainer);
                            });

                        $('section #customer_list_header input#customer_list_orderby').attr("value","c.first_name");
                        $('section #customer_list_header input#customer_list_sorting').attr("value","asc");

                        //for the pagination class     
                        var page = new paginations("section.content #customer_list_content div#paginations",data.total_rows,15);
                        if(data.total_rows > 10)//hide or show pagination
                        {
                           $("section.content #customer_list_content div#paginations").removeClass("hidden"); 
                        }else{
                           $("section.content #customer_list_content div#paginations").addClass("hidden"); 
                        }  
                        //Delegation of events on the paginations li
                        $("section.content #customer_list_content div#paginations").off('click','li.page:not(.page-ellips)').on('click','li.page:not(.page-ellips)',function(){
                            var no = $(this).text()
                            page.showPage(no,callcenter.config('url.api.jfc.customers') + "/customers/search");
                            page.paginate(this);      
                        });
                        $("section.content #customer_list_content div#paginations").off('click','li.page-prev').on('click','li.page-prev',function(){
                            page.paginates("-");
                            page.showPage(page.currentpage,callcenter.config('url.api.jfc.customers') + "/customers/search");
                        });
                        $("section.content #customer_list_content div#paginations").off('click','li.page-next').on('click','li.page-next',function(){
                            page.paginates("+");
                            page.showPage(page.currentpage,callcenter.config('url.api.jfc.customers') + "/customers/search");
                        });
                        $("section.content #customer_list_content div#paginations").off('click','li.page-max').on('click','li.page-max',function(){
                            var no = $(this).attr("data-no");
                            page.showmaxPage(no);
                        });

                        //for the sorting
                        callcenter.customer_list.clear_customer_list_sortby_classes();

                        $('section#customer_list_header a.customer_list_sortby_name').addClass("red-color active");
                        $('section#customer_list_header a.customer_list_sortby_name').find("i.fa").addClass("fa-angle-up");
                        $('section#customer_list_header a.customer_list_sortby_name').attr("data-value","desc");

                        $('section#customer_list_header').off('click','a.customer_list_sortby_name').on('click', 'a.customer_list_sortby_name', function () {
                            var iPageNo = $("section.content #customer_list_content div#paginations").find("li.page-active").attr("data-no");
                            page.sortPage($(this),"c.first_name",iPageNo,callcenter.config('url.api.jfc.customers') + "/customers/search");
                        })

                        $('section#customer_list_header').off('click','a.customer_list_sortby_behavior').on('click', 'a.customer_list_sortby_behavior', function () {
                            var iPageNo = $("section.content #customer_list_content div#paginations").find("li.page-active").attr("data-no");
                            page.sortPage($(this),"c.last_behavior_id",iPageNo,callcenter.config('url.api.jfc.customers') + "/customers/search");
                        })

                        $('section#customer_list_header').off('click','a.customer_list_sortby_recent_ordered').on('click', 'a.customer_list_sortby_recent_ordered', function () {
                            var iPageNo = $("section.content #customer_list_content div#paginations").find("li.page-active").attr("data-no");
                            page.sortPage($(this),"c.last_order_date",iPageNo,callcenter.config('url.api.jfc.customers') + "/customers/search");
                        })

                        //add class active on coordinator side
                        $('header ul#main_nav_headers li.order_management').addClass('active');

                }
            };

            callcenter.customer_list.get(oAjaxConfig);
        },
		
        'assemble_search_param_by_address': function () {
           //alert("search by address");
           $('div.list').remove();
            var oParamsAddress = {
                "params": {
                    "offset":0,
                    "where_like": [
                        {
                            "field"   : 'ca.address_label',
                            "operator": "LIKE",
                            "value"   : function () {
                                return $('section#customer_list_header #search_customer_list').val()
                            }
                        }
                    ],

                    "where" : [],
                    "order_by": "c.first_name",
                    "sorting": "asc",

                }
            };

            if ($("section #customer_list_header #customer_list_search_by_province").find("input.dd-txt").attr("value") !="All Province" ) {
                    oParamsAddress.params.where =[];
                    oParamsAddress.params.where.push(
                        {
                            "field"   : 'ca.province',
                            "operator": "=",
                            "value"   : function () {
                                return $("section #customer_list_header #customer_list_search_by_province").find("input.dd-txt").attr("value");
                            }
                        }
                    );
            }

            if ($("section #customer_list_header #customer_list_search_by_last_ordered").find("input.dd-txt").attr("value")!="") {
                var last_order_date_filter = $("section #customer_list_header #customer_list_search_by_last_ordered").find("input.dd-txt").attr("value");
                oParamsAddress.params.last_order_date_filter = last_order_date_filter;
            }

            if ($("section #customer_list_header #customer_list_search_by_behavior").find("input.dd-txt").attr("value")!="Show All Customer") {
                var last_behavior_id_filter = $("section #customer_list_header #customer_list_search_by_behavior").find("input.dd-txt").attr("value");
                oParamsAddress.params.last_behavior_id_filter = last_behavior_id_filter;
            }
            
            var oAjaxConfigAddress = {
                "type"   : "Get",
                "data"   : oParamsAddress,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : callcenter.config('url.api.jfc.customers') + "/customers/search_by_address",
                "success": function (data) {
                    callcenter.customer_list.show_spinner($('button.search_button'), false);
                    ////console.log(data);
                    //clear search box
                    // $('section#customer_list_header input#search_customer_list').val(""); 
                    if (count(data.data) > 0) {
                            $('p.count_search_result strong').text('Search Result(s) | ' + count(data.data) + ' Customers');
                            callcenter.customer_list.content_panel_toggle('customer_list');
                            var uiContainer = $('div#customer_list_content div.search-container');
                            uiContainer.html('');
                            $.each(data.data, function (key, value) {
                                var uiTemplate = $('div#customer_list_search_result.template').clone().removeClass('template');
                                var uiManipulatedTemplate = callcenter.customer_list.manipulate_template(uiTemplate, value);
                                uiManipulatedTemplate.find('div.customer_secondary_address').hide();
                                callcenter.customer_list.clone_append(uiManipulatedTemplate, uiContainer);
                            })
                        }
                        else {
                            $('p.count_search_result').find('strong').text('No Search Result');
                            callcenter.customer_list.content_panel_toggle('no_search_customer');
                        }
                            var uiContainer = $('div.search-container');
                            uiContainer.html('');
                            $.each(data.data, function (key, value) {
                                var uiTemplate = $('div.search_result_customer_list.template').clone().removeClass('template');
                                var uiManipulatedTemplate = callcenter.customer_list.manipulate_template(uiTemplate, value);
                                uiManipulatedTemplate.find('div.customer_secondary_address').hide();
                                callcenter.customer_list.clone_append(uiManipulatedTemplate, uiContainer);
                            });

                        $('section #customer_list_header input#customer_list_orderby').attr("value","c.first_name");
                        $('section #customer_list_header input#customer_list_sorting').attr("value","asc");

                        //for the pagination class     
                        var page = new paginations("section.content #customer_list_content div#paginations",data.total_rows,15);
                        if(data.total_rows > 10)//hide or show pagination
                        {
                           $("section.content #customer_list_content div#paginations").removeClass("hidden"); 
                        }else{
                           $("section.content #customer_list_content div#paginations").addClass("hidden"); 
                        }  
                        //Delegation of events on the paginations li
                        $("section.content #customer_list_content div#paginations").off('click','li.page:not(.page-ellips)').on('click','li.page:not(.page-ellips)',function(){
                            var no = $(this).text()
                            page.showPageByAddress(no,callcenter.config('url.api.jfc.customers') + "/customers/search_by_address");
                            page.paginate(this);      
                        });
                        $("section.content #customer_list_content div#paginations").off('click','li.page-prev').on('click','li.page-prev',function(){
                            page.paginates("-");
                            page.showPageByAddress(page.currentpage,callcenter.config('url.api.jfc.customers') + "/customers/search_by_address");
                        });
                        $("section.content #customer_list_content div#paginations").off('click','li.page-next').on('click','li.page-next',function(){
                            page.paginates("+");
                            page.showPageByAddress(page.currentpage,callcenter.config('url.api.jfc.customers') + "/customers/search_by_address");
                        });
                        $("section.content #customer_list_content div#paginations").off('click','li.page-max').on('click','li.page-max',function(){
                            var no = $(this).attr("data-no");
                            page.showmaxPage(no);
                        });

                        //for the sorting
                        callcenter.customer_list.clear_customer_list_sortby_classes();

                        $('section#customer_list_header a.customer_list_sortby_name').addClass("red-color active");
                        $('section#customer_list_header a.customer_list_sortby_name').find("i.fa").addClass("fa-angle-up");
                        $('section#customer_list_header a.customer_list_sortby_name').attr("data-value","desc");

                        $('section#customer_list_header').off('click','a.customer_list_sortby_name').on('click', 'a.customer_list_sortby_name', function () {
                            var iPageNo = $("section.content #customer_list_content div#paginations").find("li.page-active").attr("data-no");
                            page.sortPageByAddress($(this),"c.first_name",iPageNo,callcenter.config('url.api.jfc.customers') + "/customers/search_by_address");
                        })

                        $('section#customer_list_header').off('click','a.customer_list_sortby_behavior').on('click', 'a.customer_list_sortby_behavior', function () {
                            var iPageNo = $("section.content #customer_list_content div#paginations").find("li.page-active").attr("data-no");
                            page.sortPageByAddress($(this),"c.last_behavior_id",iPageNo,callcenter.config('url.api.jfc.customers') + "/customers/search_by_address");
                        })

                        $('section#customer_list_header').off('click','a.customer_list_sortby_recent_ordered').on('click', 'a.customer_list_sortby_recent_ordered', function () {
                            var iPageNo = $("section.content #customer_list_content div#paginations").find("li.page-active").attr("data-no");
                            page.sortPageByAddress($(this),"c.last_order_date",iPageNo,callcenter.config('url.api.jfc.customers') + "/customers/search_by_address");
                        })

                        //add class active on coordinator side
                        $('header ul#main_nav_headers li.order_management').addClass('active');

                }
            };

            callcenter.customer_list.get(oAjaxConfigAddress);
        },

        //select_province
        'render_province_select': function (oAutoCompleteGISProvince) {
            var uiCustomerListHeaderProvince = $("section #customer_list_header div#customer_list_search_by_province")
                        .children("div.select")
                        .children("div.frm-custom-dropdown")
                        .children("div.frm-custom-dropdown-option");

            var sDefaultSelected = "<div class='option' data-value='All Province'>" + "All Province" + "</div>";
                uiCustomerListHeaderProvince.append(sDefaultSelected);
              
            for (var i = 0 ; (oAutoCompleteGISProvince.length - 1) >= i ; i++) {
                var oItem = oAutoCompleteGISProvince[i];
                var sHtml = "<div class='option' data-value='" + oItem.id + "'>" + oItem.name + "</div>";
                uiCustomerListHeaderProvince.append(sHtml);
            }
			
        },

		//populate Customer List Template
		'manipulate_template': function (uiTemplate, oSearchResult) {
            if (typeof(uiTemplate) !== 'undefined' && typeof(oSearchResult) !== 'undefined') {
                var i = oSearchResult;
                var uiManipulatedTemplate = uiTemplate;

                uiManipulatedTemplate.attr('data-customer-id', oSearchResult.id);
				uiManipulatedTemplate.find("div.header-search").attr('data-customer-id', oSearchResult.id);
				uiManipulatedTemplate.find("div.second_header").attr('data-customer-list-id', oSearchResult.id);

                /*name*/
                uiManipulatedTemplate.find('strong.info_name').text(oSearchResult.first_name + ' ' + oSearchResult.middle_name + ' ' + oSearchResult.last_name);

                /*remarks*/
                // callcenter.customer_list.get_remarks('', i.last_behavior_id, uiManipulatedTemplate);
                // if (count(i.remarks) > 0) {
                //     $.each(i.remarks, function (k, v) {
                //         if(v.id == i.last_behavior_id)
                //         {
                //             uiManipulatedTemplate.find('span.info-remarks').text(v.display_label.toUpperCase());
                //             uiManipulatedTemplate.find('span.info-remarks').css('color', '#' + v.color);
                //             uiManipulatedTemplate.siblings('div.header-search').find('span.info-remarks').text(v.display_label.toUpperCase());
                //             uiManipulatedTemplate.siblings('div.header-search').find('span.info-remarks').css('color', '#' + v.color);
                //         }
                //     })
                // }
                // else {
                //     uiManipulatedTemplate.find('span.info-remarks').text('NO REMARKS AVAILABLE');
                //     uiManipulatedTemplate.find('span.info-remarks').css('color', '#000000');
                //     uiManipulatedTemplate.siblings('div.header-search').find('span.info-remarks').text('NO REMARKS AVAILABLE');
                //     uiManipulatedTemplate.siblings('div.header-search').find('span.info-remarks').css('color', '#000000');
                // }
                if (i.color != null && i.display_label != null) {
                    uiManipulatedTemplate.find('span.info-remarks').text(i.display_label.toUpperCase());
                    uiManipulatedTemplate.find('span.info-remarks').css('color', '#' + i.color);
                    uiManipulatedTemplate.siblings('div.header-search').find('span.info-remarks').text(i.display_label.toUpperCase());
                    uiManipulatedTemplate.siblings('div.header-search').find('span.info-remarks').css('color', '#' + i.color);
                }
                else {
                    uiManipulatedTemplate.find('span.info-remarks').text('NO REMARKS AVAILABLE');
                    uiManipulatedTemplate.find('span.info-remarks').css('color', '#000000');
                    uiManipulatedTemplate.siblings('div.header-search').find('span.info-remarks').text('NO REMARKS AVAILABLE');
                    uiManipulatedTemplate.siblings('div.header-search').find('span.info-remarks').css('color', '#000000');
                }

                /*address*/
                if (count(oSearchResult.address) > 0 && count(oSearchResult.address_text) > 0 && typeof(oSearchResult.address) !== 'undefined' && typeof(oSearchResult.address_text) !== 'undefined') {
                    $.each(oSearchResult.address, function (k, v) {

                        var house_number = (v.house_number !== '' && v.house_number != "0") ? v.house_number : '',
                            floor = (v.floor !== '' && v.floor != "0") ? v.floor : '',
                            building = (oSearchResult.address_text[k].building !== null) ? oSearchResult.address_text[k].building.name : '',
                            barangay = (oSearchResult.address_text[k].barangay !== null) ? oSearchResult.address_text[k].barangay.name : '',
                            subdivision = (oSearchResult.address_text[k].subdivision !== null) ? oSearchResult.address_text[k].subdivision.name : '',
                            city = (oSearchResult.address_text[k].city !== null) ? oSearchResult.address_text[k].city.name : '',
                            street = (oSearchResult.address_text[k].street !== null) ? oSearchResult.address_text[k].street.name : '',
                            second_street = (oSearchResult.address_text[k].second_street !== null && typeof(oSearchResult.address_text[k].second_street) != 'undefined') ? oSearchResult.address_text[k].second_street.name : '',
                            province = (oSearchResult.address_text[k].province !== null) ? oSearchResult.address_text[k].province.name : '';

                        var uiAddressTemplate = uiTemplate.find('div.customer_list_address.template').clone().removeClass('template');

                        if(v.address_type.toUpperCase() === "HOME")
                        {
                            uiAddressTemplate.find('img.address_image').attr({'src': callcenter.config('url.server.base') + 'assets/images/Home2.svg', "alt" : "work icon", "class" : "small-thumb"});
                            uiAddressTemplate.find('img.address_image').attr('onError', '$.fn.checkImage(this);');
                        }
                        else
                        {
                            uiAddressTemplate.find('img.address_image').attr({'src': callcenter.config('url.server.base') + 'assets/images/work-icon.png', "alt" : "work icon"});
                            uiAddressTemplate.find('img.address_image').attr('onError', '$.fn.checkImage(this);');
                        }
                        uiAddressTemplate.find('strong.address_label').text(v.address_label);
                        uiAddressTemplate.find('p.address_complete').text('' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);
                        uiAddressTemplate.find('strong.address_type').text(v.address_type.toUpperCase());
                        uiAddressTemplate.find('p.address_landmark').text('- ' + v.landmark.toUpperCase());
                        uiAddressTemplate.attr('customer_id', i.id);
                        uiAddressTemplate.attr('address_id', v.id);
                        uiAddressTemplate.attr('house_number', v.house_number);
                        uiAddressTemplate.attr('building', v.building);
                        uiAddressTemplate.attr('floor', v.floor);
                        uiAddressTemplate.attr('street', v.street);
                        uiAddressTemplate.attr('second_street', v.second_street);
                        uiAddressTemplate.attr('barangay', v.barangay);
                        uiAddressTemplate.attr('subdivision', v.subdivision);
                        uiAddressTemplate.attr('city', v.city);
                        uiAddressTemplate.attr('province', v.province);
                        uiAddressTemplate.attr('address_label', v.address_label);
                        uiAddressTemplate.attr('landmark', v.landmark);
                        uiAddressTemplate.attr('address_type', v.address_type);
                        uiAddressTemplate.attr('all_address', '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);

                        if (v.is_default == 1)
                        {
                            // uiAddressTemplate.find('strong.address_label').html(v.address_label+' - <span class="red-color">Default</span>');
                            // uiTemplate.find('div.remarks_and_address span.info_address').after('' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);
                            // uiTemplate.find('div.default_address_container').append(uiAddressTemplate);

                            if(v.is_deleted_date != '0000-00-00 00:00:00' && v.is_deleted == 1)
                            {
                                var dateNow = moment().format("YYYY-MM-DD"),
                                    timeNow = moment().format("HH:mm:ss"),
                                    is_deleted_fulldate = v.is_deleted_date.split(' '),
                                    is_deleted_date = is_deleted_fulldate[0].split('-'),
                                    is_deleted_time = is_deleted_fulldate[1].split(':'),
                                    time_now = timeNow.split(':');

                                /*deleted time*/
                                var is_deleted_hour = is_deleted_time[0],
                                    is_deleted_min = is_deleted_time[1],
                                    is_deleted_sec = is_deleted_time[2];

                                /*time now*/
                                var hour_now = time_now[0],
                                    min_now = time_now[1],
                                    sec_now = time_now[2];

                                if(dateNow == is_deleted_fulldate[0])
                                {
                                    if( ( parseInt(hour_now) - parseInt(is_deleted_hour) ) > parseInt('1') || ( parseInt(hour_now) - parseInt(is_deleted_hour) ) == parseInt('1') && parseInt(is_deleted_min) <= parseInt(min_now) )
                                    {
                                        // console.log("remove/hide address")
                                    }
                                    else
                                    {
                                        // console.log("undo");
                                        uiAddressTemplate.find('strong.address_label').html(v.address_label+' - <span class="red-color">Default</span>');
                                        uiTemplate.find('div.remarks_and_address span.info_address').after('' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);
                                        uiTemplate.find('div.default_address_container').append(uiAddressTemplate);
                                    }
                                }
                            }
                            else
                            {
                                uiAddressTemplate.find('strong.address_label').html(v.address_label+' - <span class="red-color">Default</span>');
                                uiTemplate.find('div.remarks_and_address span.info_address').after('' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);
                                uiTemplate.find('div.default_address_container').append(uiAddressTemplate);
                            }
                        }
                        else
                        {
                            // // uiAddressTemplate.removeClass('bggray-light').addClass('white');
                            // uiTemplate.find('div.other_address_container').append(uiAddressTemplate);

                            if(v.is_deleted_date != '0000-00-00 00:00:00' && v.is_deleted == 1)
                            {
                                var dateNow = moment().format("YYYY-MM-DD"),
                                    timeNow = moment().format("HH:mm:ss"),
                                    is_deleted_fulldate = v.is_deleted_date.split(' '),
                                    is_deleted_date = is_deleted_fulldate[0].split('-'),
                                    is_deleted_time = is_deleted_fulldate[1].split(':'),
                                    time_now = timeNow.split(':');

                                /*deleted time*/
                                var is_deleted_hour = is_deleted_time[0],
                                    is_deleted_min = is_deleted_time[1],
                                    is_deleted_sec = is_deleted_time[2];

                                /*time now*/
                                var hour_now = time_now[0],
                                    min_now = time_now[1],
                                    sec_now = time_now[2];

                                if(dateNow == is_deleted_fulldate[0])
                                {
                                    if( ( parseInt(hour_now) - parseInt(is_deleted_hour) ) > parseInt('1') || ( parseInt(hour_now) - parseInt(is_deleted_hour) ) == parseInt('1') && parseInt(is_deleted_min) <= parseInt(min_now) )
                                    {
                                        // console.log("remove/hide address")
                                    }
                                    else
                                    {
                                        // console.log("undo");
                                        // uiAddressTemplate.removeClass('bggray-light').addClass('white');
                                        uiTemplate.find('div.other_address_container').append(uiAddressTemplate);
                                    }
                                }
                            }
                            else
                            {
                                // uiAddressTemplate.removeClass('bggray-light').addClass('white');
                                uiTemplate.find('div.other_address_container').append(uiAddressTemplate);
                            }
                        }
                    })
                }

                /*contact numbers*/
                if (count(oSearchResult.contact_number) > 0 && typeof(oSearchResult.contact_number) !== 'undefined') {
                    var fCarrier = '';
                    if (typeof(oSearchResult.contact_number[0].name) != 'undefined') {
                        if(oSearchResult.contact_number[0].number.indexOf('-') != -1)
                        {
                            fCarrier = 'Landline'; //if landline put landline in carrier prefix
                        }
                        else
                        {
                            fCarrier = ((oSearchResult.contact_number[0].name !== 'None')? oSearchResult.contact_number[0].name : 'Unknown'); //if mobile put unknown
                        }
                    }
                    uiManipulatedTemplate.find('strong.contact_number').text('Contact Num: ' + oSearchResult.contact_number[0].number + ' ' + fCarrier);
                    var uiNumber = '',
                        // phone = '',
                        // mobile = '',
                        // mNumber = uiManipulatedTemplate.find('div.customer_list_client_info p.customer_list_contact_number_mobile'),
                        // pNumber = uiManipulatedTemplate.find('div.customer_list_client_info p.customer_list_contact_number_phone'),
                        uiNumContainer = uiManipulatedTemplate.find('div.customer_list_client_info div.customer_list_contact_number_container');
                    $.each(oSearchResult.contact_number, function (k, v) {
                        var sCarrier = '';
                        if (typeof(v.name) != 'undefined') {
                            if(v.number.indexOf('-') != -1)
                            {
                                sCarrier = 'Landline'; //if landline put landline in carrier prefix
                            }
                            else
                            {
                                sCarrier = ((v.name !== 'None') ? v.name : 'Unknown' ); //if mobile put unknown
                            }
                        }
                        if(v.number.indexOf('-') !== -1/*v.number.length >= 7 && v.number.length <= 9*/)
                        {
                            uiNumber = '<p class="f-right font-12">'+ v.number+" <i class='fa fa-phone margin-right-5'></i> "+sCarrier+"<div class='clear'></div>";
                        }
                        else //if(v.number.length == 11/* || v.number.length == 12*/)
                        {
                            uiNumber = '<p class="f-right font-12">'+v.number+" <i class='fa fa-mobile margin-right-5'></i> "+sCarrier+"<div class='clear'></div>";
                        }
                        uiNumContainer.append(uiNumber);
                    });

                    // mNumber.empty().append(mobile);
                    // pNumber.empty().append(phone);
                }

                /*email address*/
                if(count(oSearchResult.email_address) > 0)
                {
                    uiManipulatedTemplate.find('p.customer_list_email_address').text(oSearchResult.email_address);                
                }
                else
                {
                    uiManipulatedTemplate.find('p.customer_list_email_address').text('');                   
                }

                /*date of birth*/
                if(count(oSearchResult.birthdate) > 0)
                {
                    uiManipulatedTemplate.find('p.customer_list_date_of_birth').text(oSearchResult.birthdate);
                }
                else
                {
                    uiManipulatedTemplate.find('p.customer_list_date_of_birth').text('');                   
                }

                /*discounts*/
                if(typeof(oSearchResult.osca_number) != 'undefined')
                {
                    if(oSearchResult.osca_number.length > 0)
                    {
                        uiManipulatedTemplate.find('div.customer_list_discounts.template').siblings('label.discounts').show();
                        uiManipulatedTemplate.find('div.customer_list_discounts.template').removeClass('template');
                        uiManipulatedTemplate.find('div.customer_list_discounts p.customer_list_discounts_name').text(oSearchResult.first_name+" "+oSearchResult.last_name);
                        uiManipulatedTemplate.find('div.customer_list_discounts strong.customer_list_discounts_number').empty().html('<span class="red-color">OSCA Number: </span>'+oSearchResult.osca_number);
                    }
                    else
                    {
                        uiManipulatedTemplate.find('div.customer_list_discounts').after('<div class="clear"></div>');
                        uiManipulatedTemplate.find('div.customer_list_discounts').siblings('label.discounts').hide();
                    }
                }
                 else
                {
                    uiManipulatedTemplate.find('div.customer_list_discounts').after('<div class="clear"></div>');
                    uiManipulatedTemplate.find('div.customer_list_discounts').siblings('label.discounts').hide();
                }

                /*happy plus cards*/
                if (count(oSearchResult.happy_plus_card) > 0 && typeof(oSearchResult.happy_plus_card) !== 'undefined') {
                    $.each(oSearchResult.happy_plus_card, function (k, v) {
                        var uiHappyPlusTemplate = uiTemplate.find('div.customer_list_cards.template').clone().removeClass('template');
                        uiHappyPlusTemplate.find('p.customer_list_card_number').text(v.number);
                        uiHappyPlusTemplate.find('p.customer_list_card_number_expiration').text(v.expiration_date_date);
                        uiHappyPlusTemplate.attr({'hpc_id': v.id, 'hpc_number' : v.number,'hpc_date' : v.expiration_date_date, 'hpc_remarks': v.remarks})
                        callcenter.customer_list.clone_append(uiHappyPlusTemplate, uiTemplate.find('div.customer_list_cards_container'));
                    })
                }
                else
                {
                    uiManipulatedTemplate.find('div.customer_list_cards_container').siblings('label.cards').hide();
                }
                return uiManipulatedTemplate;
            }
        },

        'content_panel_toggle': function (uiString) {
            var uiContents = $('section[section-style="content-panel"]').children('div.row');
            var uiNavs = $('ul.main_nav li');
            var uiSubNav = $('div.sub-nav.' + uiString);
            ////console.log(uiContents)
            $.each(uiNavs, function () {
                $(this).removeClass('active')

                if ($(this).hasClass(uiString)) {
                    $(this).addClass('active');
                }
            })

            $.each(uiContents, function () {
                $(this).addClass('hidden');
                $('#' + uiString + '_content').removeClass('hidden');
            });

            if(uiSubNav.length > 0)
            {
                uiSubNav.show();
            }

        },

       'get': function (oAjaxConfig) {
            if (typeof(oAjaxConfig) != 'undefined') {
                callcenter.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        'clone_append': function (uiTemplate, uiContainer) {
            if (typeof(uiTemplate) !== 'undefined' && typeof(uiContainer) !== 'undefined') {
                uiContainer.append(uiTemplate);
            }
        },

        'remove_append': function (uiElem) {
            if (typeof(uiElem) !== 'undefined') {
                uiElem.remove();
            }
        },

        'show_spinner': function (uiElem, bShow) {
            if (bShow == true) {
                uiElem.find('i.fa-spinner').removeClass('hidden');
                uiElem.prop('disabled', true);
            }
            else {
                uiElem.find('i.fa-spinner').addClass('hidden');
                uiElem.prop('disabled', false);
            }
        },

        'clear_customer_list_sortby_classes': function () {
            $('section#customer_list_header a.customer_list_sort').each(function( i ) {
                $(this).css({"color":"black"});
                $(this).removeClass("active red-color");
                $(this).find("i.fa").removeClass("fa-angle-up");
                $(this).find("i.fa").removeClass("fa-angle-down");
            });
        },

    }

}());

//start pagination class
var paginations = function (container,rows,config){
    //The length of all table records
    this.items_length     = rows;
    //The maximum pages
    this.pagelimit        = Math.ceil(this.items_length/10);
    //The indicator fo the current page
    this.currentpage      = 1 ;
    this.codes            = "<li class='page-prev'><i class='fa fa-angle-left'></i></li>";
    //The current batch count **SET OF ten lIs ''
    this.batch_count      = 1;
    //The maximum batch count
    this.batch_max        = Math.ceil(this.pagelimit/10);
    //The construct function
    this.config_length    = config;
    //
    this.permittedLiCount = ((this.config_length - 5) - 2) ;
    this.middle_count     = this.config_length - 5;

    this.to_be_deducted   = Math.floor( this.middle_count/2 ) ;
    this.to_be_added      = this.middle_count - this.to_be_deducted ;
    this.to_be_deducted   -=1;
    this.to_be_added      -=2;

    this.__construct = function(){

        this.load_paginations();
        //this.showPage(1);

    }
    this.batch_max = 0;
    //Loading of pagination items
    this.load_paginations = function (){

        this.codes = "<li class='page-prev'><i class='fa fa-angle-left'></i></li>";

        var iTurning_point = this.config_length - 5;

        if(this.pagelimit > this.config_length){
            
            var iFirstBatch = Math.ceil(this.config_length/2);
            var iRemaining = (this.config_length - iFirstBatch) - 4;
            iFirstBatch+=iRemaining;
            for (var i = 1; i <= iFirstBatch; i++) {
                if(i==1){
                    this.codes += "<li class='page page-active' data-no='"+i+"'>"+i+"</li>";
                }
                else{
                    this.codes += "<li class='page ' data-no='"+i+"'>"+i+"</li>";
                }
            };

            this.codes += "<li class='page page-ellips' >...</li>";

            var iLastBatch  = this.pagelimit - 2;

            for (var i = iLastBatch; i <= this.pagelimit;i++) {

                this.codes += "<li class='page ' data-no='"+i+"'>"+i+"</li>";

            };
        }
        else{

            for (var i = 1; i <=this.pagelimit; i++) {

                if(i==1){
                    this.codes += "<li class='page page-active' data-no='"+i+"'>"+i+"</li>";
                }
                else{
                    this.codes += "<li class='page ' data-no='"+i+"'>"+i+"</li>";
                }

            }
        }
        this.codes += "<li class='page-next'><i class='fa fa-angle-right'></i></li>";
        $(container).html(this.codes);

    }
    this.reload_default = function (){

        var iTurning_point = this.config_length - 5;

        if(this.pagelimit > this.config_length){
            
            var iFirstBatch = Math.ceil(this.config_length/2);
            var iRemaining = (this.config_length - iFirstBatch) - 4;
            iFirstBatch+=iRemaining;

            for (var i = 1; i <= iFirstBatch; i++) {
                var uiItem = $("li.page")[i-1];
                $(uiItem).text(i);
            };

            var uiItem = $("li.page")[i];
            $(uiItem).text("...");
            var iNewiterator  = i + 1;
            var iLastBatch  = this.pagelimit - 2;
        
            for (var i = iLastBatch; i <= this.pagelimit;i++) {

                var uiItem = $("li.page")[iNewiterator - 1];
                $(uiItem).text(i);
                iNewiterator++;
            };
        }
        else{
            for (var i = 1; i <=this.pagelimit; i++) {
                var uiItem = $("li.page")[i-1];
                $(uiItem).text(i);
            }
        }
        
        var uiFirstLast   = $('li.page')[this.config_length-3];
        $(uiFirstLast).prev("li").text("...");

    }

    this.showPage       = function (iNo,sUrl){

        this.currentpage = iNo;
        var iOffset = (iNo - 1) * 10;
        iOffset = ( iOffset < 0 ) ? 0 : iOffset;    

        var oParamsPaginate = {
            "params": {
                "offset":iOffset,
                "where_like": [
                    {
                        "field"   : function () {
                            var uiSearchByValue = $('section #customer_list_header input#customer_list_search_by').val();
                            if (uiSearchByValue == "Contact Number") {
                                uiSearchByValue = 'ccn.number';
                            }
                            else {
                                uiSearchByValue = 'c.first_name';
                            }

                            return uiSearchByValue;
                        },
                        "operator": "LIKE",
                        "value"   : function () {
                            return $('section#customer_list_header #search_customer_list').val()
                        }
                    }
                ],

                "where" : [],
                "order_by":"",
                "sorting":"",

            }
        };

        if ($("section #customer_list_header #customer_list_search_by_province").find("input.dd-txt").attr("value")!="All Province") {
                oParamsPaginate.params.where =[];
                oParamsPaginate.params.where.push(
                    {
                        "field"   : 'ca.province',
                        "operator": "=",
                        "value"   : function () {
                            return $("section #customer_list_header #customer_list_search_by_province").find("input.dd-txt").attr("value");
                        }
                    }
                );
        }

        if ($("section #customer_list_header #customer_list_search_by_last_ordered").find("input.dd-txt").attr("value")!="") {
                var last_order_date_filter = $("section #customer_list_header #customer_list_search_by_last_ordered").find("input.dd-txt").attr("value");
                oParamsPaginate.params.last_order_date_filter = last_order_date_filter;
        }

        if ($("section #customer_list_header #customer_list_search_by_behavior").find("input.dd-txt").attr("value")!="Show All Customer") {
                var last_behavior_id_filter = $("section #customer_list_header #customer_list_search_by_behavior").find("input.dd-txt").attr("value");
                oParamsPaginate.params.last_behavior_id_filter = last_behavior_id_filter;
            }

        if ($('section #customer_list_header input#customer_list_orderby').attr("value")!="") {
            oParamsPaginate.params.order_by = "";
            oParamsPaginate.params.order_by = $('section #customer_list_header input#customer_list_orderby').attr("value");
        }

        if ($('section #customer_list_header input#customer_list_sorting').attr("value")!="") {
            oParamsPaginate.params.sorting = "";
            oParamsPaginate.params.sorting = $('section #customer_list_header input#customer_list_sorting').attr("value");
        }
        
        var oInfoAjaxConfig = {
        "type"   : "Get",
        "data"   : oParamsPaginate,
        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
        "url"    : sUrl,
        //"url"    : callcenter.config('url.api.jfc.customers') + "/customers/search",
        "success": function (data) {
            var uiContainer = $('div.search-container');
                uiContainer.html('');
                $.each(data.data, function (key, value) {
                    var uiTemplate = $('div.search_result_customer_list.template').clone().removeClass('template');
                    var uiManipulatedTemplate = callcenter.customer_list.manipulate_template(uiTemplate, value);
                    uiManipulatedTemplate.find('div.customer_secondary_address').hide();
                    callcenter.customer_list.clone_append(uiManipulatedTemplate, uiContainer);
                });

            }
        }
        callcenter.customer_list.get(oInfoAjaxConfig);
    }

    this.showPageByAddress       = function (iNo,sUrl){

        this.currentpage = iNo;
        var iOffset = (iNo - 1) * 10;
        iOffset = ( iOffset < 0 ) ? 0 : iOffset;    

        var oParamsPaginate = {
            "params": {
                "offset":iOffset,
                "where_like": [
                    {
                            "field"   : 'ca.address_label',
                            "operator": "LIKE",
                            "value"   : function () {
                                return $('section#customer_list_header #search_customer_list').val()
                            }
                    }
                ],

                "where" : [],
                "order_by":"",
                "sorting":"",

            }
        };

        if ($("section #customer_list_header #customer_list_search_by_province").find("input.dd-txt").attr("value")!="All Province") {
                oParamsPaginate.params.where =[];
                oParamsPaginate.params.where.push(
                    {
                        "field"   : 'ca.province',
                        "operator": "=",
                        "value"   : function () {
                            return $("section #customer_list_header #customer_list_search_by_province").find("input.dd-txt").attr("value");
                        }
                    }
                );
        }

        if ($("section #customer_list_header #customer_list_search_by_last_ordered").find("input.dd-txt").attr("value")!="") {
                var last_order_date_filter = $("section #customer_list_header #customer_list_search_by_last_ordered").find("input.dd-txt").attr("value");
                oParamsPaginate.params.last_order_date_filter = last_order_date_filter;
        }

        if ($("section #customer_list_header #customer_list_search_by_behavior").find("input.dd-txt").attr("value")!="Show All Customer") {
                var last_behavior_id_filter = $("section #customer_list_header #customer_list_search_by_behavior").find("input.dd-txt").attr("value");
                oParamsPaginate.params.last_behavior_id_filter = last_behavior_id_filter;
            }

        if ($('section #customer_list_header input#customer_list_orderby').attr("value")!="") {
            oParamsPaginate.params.order_by = "";
            oParamsPaginate.params.order_by = $('section #customer_list_header input#customer_list_orderby').attr("value");
        }

        if ($('section #customer_list_header input#customer_list_sorting').attr("value")!="") {
            oParamsPaginate.params.sorting = "";
            oParamsPaginate.params.sorting = $('section #customer_list_header input#customer_list_sorting').attr("value");
        }
        
        var oInfoAjaxConfig = {
        "type"   : "Get",
        "data"   : oParamsPaginate,
        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
        "url"    : sUrl,
        //"url"    : callcenter.config('url.api.jfc.customers') + "/customers/search",
        "success": function (data) {
            var uiContainer = $('div.search-container');
                uiContainer.html('');
                if(typeof(data.data) !== 'undefined')
                {                
                    $.each(data.data, function (key, value) {
                        var uiTemplate = $('div.search_result_customer_list.template').clone().removeClass('template');
                        var uiManipulatedTemplate = callcenter.customer_list.manipulate_template(uiTemplate, value);
                        uiManipulatedTemplate.find('div.customer_secondary_address').hide();
                        callcenter.customer_list.clone_append(uiManipulatedTemplate, uiContainer);
                    });
                }

            }
        }
        callcenter.customer_list.get(oInfoAjaxConfig);
    }

    //Sorting of the page requested
    this.sortPage       = function (uiSortField,sOrderby,page_no,sUrl){
        
        callcenter.customer_list.clear_customer_list_sortby_classes();
        
        uiSortField.addClass("active red-color");
        var order_by = sOrderby;
        var sorting = "";
        if(uiSortField.attr("data-value")=="asc"){
            var sorting = uiSortField.attr("data-value");
            uiSortField.attr("data-value","desc");
            uiSortField.find("i.fa").addClass("fa-angle-up");
            uiSortField.find("i.fa").removeClass("fa-angle-down");
        }else{
            var sorting = uiSortField.attr("data-value");
            uiSortField.attr("data-value","asc");
            uiSortField.find("i.fa").addClass("fa-angle-down");
            uiSortField.find("i.fa").removeClass("fa-angle-up");
        }

        //alert(order_by+" = "+ sorting);
        $('section #customer_list_header input#customer_list_orderby').attr("value",order_by);
        $('section #customer_list_header input#customer_list_sorting').attr("value",sorting);

        this.currentpage = page_no;
        var offset = (page_no - 1) * 10;

        offset = ( offset < 0 ) ? 0 : offset;

        var oParamsPaginate = {
            "params": {
                "offset":offset,
                "where_like": [
                    {
                        "field"   : function () {
                            var uiSearchByValue = $('section #customer_list_header input#customer_list_search_by').val();
                            if (uiSearchByValue == "Contact Number") {
                                uiSearchByValue = 'ccn.number';
                            }
                            else {
                                uiSearchByValue = 'c.first_name';
                            }

                            return uiSearchByValue;
                        },
                        "operator": "LIKE",
                        "value"   : function () {
                            return $('section#customer_list_header #search_customer_list').val()
                        }
                    }
                ],

                "where" : [],
                "order_by" : order_by,
                "sorting" : sorting,

            }
        };

        if ($("section #customer_list_header #customer_list_search_by_province").find("input.dd-txt").attr("value")!="All Province") {
                oParamsPaginate.params.where =[];
                oParamsPaginate.params.where.push(
                    {
                        "field"   : 'ca.province',
                        "operator": "=",
                        "value"   : function () {
                            return $("section #customer_list_header #customer_list_search_by_province").find("input.dd-txt").attr("value");
                        }
                    }
                );
        }

        if ($("section #customer_list_header #customer_list_search_by_last_ordered").find("input.dd-txt").attr("value")!="") {
                var last_order_date_filter = $("section #customer_list_header #customer_list_search_by_last_ordered").find("input.dd-txt").attr("value");
                oParamsPaginate.params.last_order_date_filter = last_order_date_filter;
        }

        if ($("section #customer_list_header #customer_list_search_by_behavior").find("input.dd-txt").attr("value")!="Show All Customer") {
                var last_behavior_id_filter = $("section #customer_list_header #customer_list_search_by_behavior").find("input.dd-txt").attr("value");
                oParamsPaginate.params.last_behavior_id_filter = last_behavior_id_filter;
            }

        var oInfoAjaxConfig = {
        "type"   : "Get",
        "data"   : oParamsPaginate,
        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
        //"url"    : callcenter.config('url.api.jfc.customers') + "/customers/search",
        "url"    : sUrl,
        "success": function (data) {
            var uiContainer = $('div.search-container');
                uiContainer.html('');
                $.each(data.data, function (key, value) {
                    var uiTemplate = $('div.search_result_customer_list.template').clone().removeClass('template');
                    var uiManipulatedTemplate = callcenter.customer_list.manipulate_template(uiTemplate, value);
                    uiManipulatedTemplate.find('div.customer_secondary_address').hide();
                    callcenter.customer_list.clone_append(uiManipulatedTemplate, uiContainer);
                });

            }
        }
        callcenter.customer_list.get(oInfoAjaxConfig);
    }

    //Sorting of the page requested by address filter
    this.sortPageByAddress       = function (uiSortField,sOrderby,page_no,sUrl){
        
        callcenter.customer_list.clear_customer_list_sortby_classes();
        
        uiSortField.addClass("active red-color");
        var order_by = sOrderby;
        var sorting = "";
        if(uiSortField.attr("data-value")=="asc"){
            var sorting = uiSortField.attr("data-value");
            uiSortField.attr("data-value","desc");
            uiSortField.find("i.fa").addClass("fa-angle-up");
            uiSortField.find("i.fa").removeClass("fa-angle-down");
        }else{
            var sorting = uiSortField.attr("data-value");
            uiSortField.attr("data-value","asc");
            uiSortField.find("i.fa").addClass("fa-angle-down");
            uiSortField.find("i.fa").removeClass("fa-angle-up");
        }

        //alert(order_by+" = "+ sorting);
        $('section #customer_list_header input#customer_list_orderby').attr("value",order_by);
        $('section #customer_list_header input#customer_list_sorting').attr("value",sorting);

        this.currentpage = page_no;
        var offset = (page_no - 1) * 10;

        offset = ( offset < 0 ) ? 0 : offset;

        var oParamsPaginate = {
            "params": {
                "offset":offset,
                "where_like": [
                    {
                            "field"   : 'ca.address_label',
                            "operator": "LIKE",
                            "value"   : function () {
                                return $('section#customer_list_header #search_customer_list').val()
                            }
                    }
                ],

                "where" : [],
                "order_by" : order_by,
                "sorting" : sorting,

            }
        };

        if ($("section #customer_list_header #customer_list_search_by_province").find("input.dd-txt").attr("value")!="All Province") {
                oParamsPaginate.params.where =[];
                oParamsPaginate.params.where.push(
                    {
                        "field"   : 'ca.province',
                        "operator": "=",
                        "value"   : function () {
                            return $("section #customer_list_header #customer_list_search_by_province").find("input.dd-txt").attr("value");
                        }
                    }
                );
        }

        if ($("section #customer_list_header #customer_list_search_by_last_ordered").find("input.dd-txt").attr("value")!="") {
                var last_order_date_filter = $("section #customer_list_header #customer_list_search_by_last_ordered").find("input.dd-txt").attr("value");
                oParamsPaginate.params.last_order_date_filter = last_order_date_filter;
        }

        if ($("section #customer_list_header #customer_list_search_by_behavior").find("input.dd-txt").attr("value")!="Show All Customer") {
                var last_behavior_id_filter = $("section #customer_list_header #customer_list_search_by_behavior").find("input.dd-txt").attr("value");
                oParamsPaginate.params.last_behavior_id_filter = last_behavior_id_filter;
            }

        var oInfoAjaxConfig = {
        "type"   : "Get",
        "data"   : oParamsPaginate,
        "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
        //"url"    : callcenter.config('url.api.jfc.customers') + "/customers/search",
        "url"    : sUrl,
        "success": function (data) {
            var uiContainer = $('div.search-container');
                uiContainer.html('');
                $.each(data.data, function (key, value) {
                    var uiTemplate = $('div.search_result_customer_list.template').clone().removeClass('template');
                    var uiManipulatedTemplate = callcenter.customer_list.manipulate_template(uiTemplate, value);
                    uiManipulatedTemplate.find('div.customer_secondary_address').hide();
                    callcenter.customer_list.clone_append(uiManipulatedTemplate, uiContainer);
                });

            }
        }
        callcenter.customer_list.get(oInfoAjaxConfig);
    }

    this.changeHovers    = function ( iIndex ){

        $("li.page").removeClass("page-active");
        $.each($("li.page"),function(i,uiLi){

            if( $(uiLi).text() == iIndex){
                $(uiLi).addClass("page-active");
            }

        });
    
    }
    this.paginates      = function(sSign){



        if(sSign=="+"){
            this.currentpage++;
            if(this.currentpage>this.pagelimit){
                this.currentpage =this.pagelimit;
            }
        }
        else{
            this.currentpage--;
            if(this.currentpage<0){
                this.currentpage =1;
            }
        }

        var oObject = {};
        var iCurrentpage = this.currentpage;
        
        $.each($("li.page"),function(i,item){

            if( $(item).text() == iCurrentpage){
                oObject = item;
            }

        });

        var iIndex      = parseInt($(oObject).text());
        var iFirstRange = Math.ceil(this.config_length/2);

        var uiFirst       = $('li.page')[0];
        var uiSecond      = $('li.page')[1];
        var uiFirstLast   = $('li.page')[this.config_length-4];
        var uiSecondLast  = $('li.page')[this.config_length-3];
        var uiLast        = $('li.page')[this.config_length-2];

        var iLastRange    = (this.pagelimit - this.config_length);
        var iMiddle       = Math.ceil(this.pagelimit - (this.config_length/2));

        var iFirstBatch     = Math.ceil(this.config_length/2);
        var iRemaining     = [(this.config_length - iFirstBatch) - 4] ;
        var iIndicator     = (this.pagelimit - (this.config_length-5))-1;

        // iFirstBatch+=iRemaining;
        $("li.page").removeClass("page-active")
        $(oObject).addClass("page-active")
        if(this.pagelimit>15)
        {
            if(iIndex>=iFirstRange)
            {
                if(iIndex>=iIndicator)
                {
                                var iRange = iIndicator + 3;        
                                var iIterator = this.config_length;
                                if(iIndex<=iRange)
                                {
                                    var iLength  = $(".page-ellips").length;
                                    if(iLength==2)
                                    {

                                            var uiThird  = $("li.page")[2];

                                            $("li.page").removeClass("page-ellips")
                                            $(uiSecond).addClass("page-ellips").text("...");

                                            var iFrom     = (this.pagelimit - (this.config_length-5))-1;
                                            var iTo       = this.pagelimit;

                                            $(uiThird).text(iFrom - 1)
                                            
                                            for (iTo; iFrom <= iTo; iTo--) {

                                                var uiItem = $("li.page")[iIterator-1];
                                                $(uiItem).text(iTo);
                                                iIterator--;
                                            };

                                    }
                            
                                }
                                else
                                {           
                                            var uiThird  = $("li.page")[2];

                                            $("li.page").removeClass("page-ellips")
                                            $(uiSecond).addClass("page-ellips").text("...");

                                            var iFrom     = (this.pagelimit - (this.config_length-5))-1;
                                            var iTo       = this.pagelimit;

                                            $(uiThird).text(iFrom - 1)
                                        
                                            for (iTo; iFrom <= iTo; iTo--) {
                                                var uiItem = $("li.page")[iIterator-1];
                                                $(uiItem).text(iTo);
                                                iIterator--;
                                            };
                                }
                }
                else
                {
                    var uiLi = $(oObject).prev("li");

                    if($(uiLi).hasClass("page-ellips")){

                        var iIterator = 3;

                        $(uiSecond).text('2');

                        $("li.page").removeClass("page-ellips")

                        var uiThird  = $("li.page")[2];

                        $(uiThird).addClass("page-ellips").text("...");
                        
                        var iFrom     = iIndex - this.to_be_deducted;
                        var iTo       = iIndex + this.to_be_added;
                        if(iTo>this.pagelimit) iTo = this.pagelimit;
                        iIterator++;

                        for (iFrom; iFrom <= iTo; iFrom++) {
                        
                            var uiItem = $("li.page")[iIterator-1];
                            $(uiItem).text(iFrom);

                            iIterator++;

                        };

                        $(uiFirstLast).prev("li").addClass("page-ellips").text("...");

                    }
                    else{
                        
                        var iIterator = 2;
                        var uiThird  = $("li.page")[2];

                        $(uiThird).addClass("page-ellips").text("...");

                        var iFrom     = iIndex - this.to_be_deducted;
                        var iTo       = iIndex + this.to_be_added;

                        if(iTo>this.pagelimit) iTo = this.pagelimit;
                        iIterator++;
                        for (iFrom; iFrom <= iTo; iFrom++) {

                            var uiItem = $("li.page")[iIterator];
                            $(uiItem).text(iFrom);
                            iIterator++;
                        };

                        var uiLi = $(uiFirstLast).prev("li");

                        if($(uiLi).hasClass("page-ellips"))
                        {
                            $(uiFirstLast).prev("li").addClass("page-ellips").text("...");
                        }
                    }
                }
            }
            else{
                this.reload_default();
            }
        }
    }

    this.paginate       = function(oObject){

        var iIndex      = parseInt($(oObject).text());
        var iFirstRange = Math.ceil(this.config_length/2);

        var uiFirst       = $('li.page')[0];
        var uiSecond      = $('li.page')[1];
        var uiFirstLast   = $('li.page')[this.config_length-4];
        var uiSecondLast  = $('li.page')[this.config_length-3];
        var uiLast        = $('li.page')[this.config_length-2];

        var iLastRange    = (this.pagelimit - this.config_length);
        var iMiddle       = Math.ceil(this.pagelimit - (this.config_length/2));

        var iFirstBatch     = Math.ceil(this.config_length/2);
        var iRemaining     = [(this.config_length - iFirstBatch) - 4] ;
        var iIndicator     = (this.pagelimit - (this.config_length-5))-1;

        // iFirstBatch+=iRemaining;
        

        if(this.pagelimit>15)
        {
            if(iIndex>=iFirstRange)
            {
                if(iIndex>=iIndicator)
                {
                                var iRange = iIndicator + 3;        
                                var iIterator = this.config_length;
                                if(iIndex<=iRange)
                                {
                                    var iLength  = $(".page-ellips").length;
                                    if(iLength==2)
                                    {

                                            var uiThird  = $("li.page")[2];

                                            $("li.page").removeClass("page-ellips")
                                            $(uiSecond).addClass("page-ellips").text("...");

                                            var iFrom     = (this.pagelimit - (this.config_length-5))-1;
                                            var iTo       = this.pagelimit;

                                            $(uiThird).text(iFrom - 1)
                                            
                                            for (iTo; iFrom <= iTo; iTo--) {

                                                var uiItem = $("li.page")[iIterator-1];
                                                $(uiItem).text(iTo);
                                                iIterator--;
                                            };

                                    }
                            
                                }
                                else
                                {           
                                            var uiThird  = $("li.page")[2];

                                            $("li.page").removeClass("page-ellips")
                                            $(uiSecond).addClass("page-ellips").text("...");

                                            var iFrom     = (this.pagelimit - (this.config_length-5))-1;
                                            var iTo       = this.pagelimit;

                                            $(uiThird).text(iFrom - 1)
                                        
                                            for (iTo; iFrom <= iTo; iTo--) {
                                                var uiItem = $("li.page")[iIterator-1];
                                                $(uiItem).text(iTo);
                                                iIterator--;
                                            };
                                }
                }
                else
                {
                    var uiLi = $(oObject).prev("li");

                    if($(uiLi).hasClass("page-ellips")){

                        var iIterator = 3;

                        $(uiSecond).text('2');

                        $("li.page").removeClass("page-ellips")

                        var uiThird  = $("li.page")[2];

                        $(uiThird).addClass("page-ellips").text("...");
                        
                        var iFrom     = iIndex - this.to_be_deducted;
                        var iTo       = iIndex + this.to_be_added;
                        if(iTo>this.pagelimit) iTo = this.pagelimit;
                        iIterator++;

                        for (iFrom; iFrom <= iTo; iFrom++) {
                        
                            var uiItem = $("li.page")[iIterator-1];
                            $(uiItem).text(iFrom);

                            iIterator++;

                        };

                        $(uiFirstLast).prev("li").addClass("page-ellips").text("...");

                    }
                    else{
                        
                        var iIterator = 2;
                        var uiThird  = $("li.page")[2];

                        $(uiThird).addClass("page-ellips").text("...");

                        var iFrom     = iIndex - this.to_be_deducted;
                        var iTo       = iIndex + this.to_be_added;

                        if(iTo>this.pagelimit) iTo = this.pagelimit;
                        iIterator++;
                        for (iFrom; iFrom <= iTo; iFrom++) {

                            var uiItem = $("li.page")[iIterator];
                            $(uiItem).text(iFrom);
                            iIterator++;
                        };

                        var uiLi = $(uiFirstLast).prev("li");

                        if($(uiLi).hasClass("page-ellips"))
                        {
                            $(uiFirstLast).prev("li").addClass("page-ellips").text("...");
                        }
                    }
                }
            }
            else{
                this.reload_default();
            }
        }
        this.changeHovers(iIndex)
    }
    this.__construct();

}
//end pagination class

$(window).load(function () {
    callcenter.customer_list.initialize();
});


