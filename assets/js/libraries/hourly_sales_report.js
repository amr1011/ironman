/**
 *  Hourly Sales Report Class
 *
 *
 *
 */
(function () {
    "use strict";

    var generateByDateBtn,
        generateByPeriodBtn,
        generateByDateDiv,
        generateByPeriodDiv,
        showGenerateReport,
        noGenerateReport,
        datepickerDateFrom,
        datepickerDateTo,
        reportContent,
        hourlySalesReportTable,
        hourlySalesReportXlsBtn,
        selectPeriodDropdownDiv,
        hourlySalesReportLink,
        selectStoreDropdownDiv,
        selectProvinceDropdownDiv,
        selectCallcenterDropdownDiv,
        selectTransactionDropdownDiv,
        getOrderLimit = 9999999;

    CPlatform.prototype.hourly_sales_report = {

        'initialize' : function () {

            generateByDateBtn               = $('section#hourly_report_header').find('button.generate_by_date');
            generateByPeriodBtn             = $('section#hourly_report_header').find('button.generate_by_period');
            generateByDateDiv               = $('section#hourly_report_header').find('div.generate_by_date');
            generateByPeriodDiv             = $('section#hourly_report_header').find('div.generate_by_period');
            showGenerateReport              = $('section.content').find('div[content="hourly_report"] div.show_generate_report');
            noGenerateReport                = $('section.content').find('div[content="hourly_report"] div.no_generate_report');
            reportContent                   = $('section.content').find('div[content="hourly_report"]');
            datepickerDateFrom              = $('section#hourly_report_header').find('div#hourly-sales-report-date-from');
            datepickerDateTo                = $('section#hourly_report_header').find('div#hourly-sales-report-date-to');
            hourlySalesReportTable          = $('section.content').find('div[content="hourly_report"] table#hourly_sales_report_table');
            hourlySalesReportXlsBtn         = $('section#hourly_report_header').find('button.hourly_sales_report_xls_button');
            selectPeriodDropdownDiv         = $('section#hourly_report_header').find('div.generate_by_period div.select_period');
            selectStoreDropdownDiv          = $('section#hourly_report_header').find('div.hourly_store_selection');
            selectProvinceDropdownDiv       = $('section#hourly_report_header').find('div.hourly_province_selection');
            selectCallcenterDropdownDiv     = $('section#hourly_report_header').find('div.hourly_callcenter_selection');
            selectTransactionDropdownDiv    = $('section#hourly_report_header').find('div.hourly_transaction_selection');
            hourlySalesReportLink           = $('ul.report-tabs.sub_nav').find('li[content="hourly_report"]');

            //hide/show on initialize
            noGenerateReport.show();
            showGenerateReport.hide();
            
            if ($().datetimepicker !== undefined) {
                var dateTimePickerOptions = {
                    format  : 'MMMM DD, YYYY',
                    pickTime: false
                };

                //set date picker options
                datepickerDateFrom.datetimepicker(dateTimePickerOptions);
                datepickerDateTo.datetimepicker(dateTimePickerOptions);

                //change min date of date to not allow to pass 
                datepickerDateFrom.on("dp.change", function (e) {
                    datepickerDateTo.data('DateTimePicker').setMinDate(e.date);
                });

                datepickerDateTo.on("dp.change", function (e) {
                    datepickerDateFrom.data('DateTimePicker').setMaxDate(e.date);
                });
            }

            //sales report li click
            hourlySalesReportLink.on('click', function(){
                //show no generate report, clear input values
                noGenerateReport.show();
                showGenerateReport.hide();
                admin.dst_reports.show_spinner(hourlySalesReportXlsBtn, true);
                datepickerDateFrom.find('input[name="hourly_sales_report_date_from"]').val('');
                datepickerDateTo.find('input[name="hourly_sales_report_date_to"]').val('');
                selectStoreDropdownDiv.find('input[name="hourly_stores"]').attr('int-value', 0).val('Show All Stores')/*.removeAttr('readonly')*/
                selectCallcenterDropdownDiv.find('input[name="hourly_callcenters"]').attr('int-value', 0).val('Show All Call Centers')/*.removeAttr('readonly')*/
                selectProvinceDropdownDiv.find('input[name="hourly_provinces"]').attr('int-value', 0).val('Show All Provinces')/*.removeAttr('readonly')*/
                selectTransactionDropdownDiv.find('input[name="hourly_transactions"]').attr('data-value', 'Show All Transactions').val('Show All Transactions');

            });

            //generate by date button
            generateByDateBtn.on('click', function(){
                var dateFromInput = generateByDateDiv.find('input[name="hourly_sales_report_date_from"]')
                var dateToInput = generateByDateDiv.find('input[name="hourly_sales_report_date_to"]')

                if(dateFromInput.val() == '' || dateToInput.val() == '')
                {
                    //show no generate report
                    noGenerateReport.show();
                    showGenerateReport.hide();
                    selectStoreDropdownDiv.find('input[name="hourly_stores"]').attr('int-value', 0).val('Show All Stores')/*.removeAttr('readonly')*/
                    selectCallcenterDropdownDiv.find('input[name="hourly_callcenters"]').attr('int-value', 0).val('Show All Call Centers')/*.removeAttr('readonly')*/
                    selectProvinceDropdownDiv.find('input[name="hourly_provinces"]').attr('int-value', 0).val('Show All Provinces')/*.removeAttr('readonly')*/
                    selectTransactionDropdownDiv.find('input[name="hourly_transactions"]').attr('data-value', 'Show All Transactions').val('Show All Transactions');
                    admin.dst_reports.show_spinner(hourlySalesReportXlsBtn, true);
                }
                else
                {
                    var dateFrom = moment(dateFromInput.val()),
                        dateTo = moment(dateToInput.val()),
                        dateFromValue = moment(dateFrom).format("YYYY-MM-DD HH:mm:ss"),
                        dateToValue = moment(dateTo).format("YYYY-MM-DD");
                        dateToValue = moment(dateToValue+" 23:59:59").format("YYYY-MM-DD HH:mm:ss");

                    if(dateTo.diff(dateFrom, 'days') < 0)
                    {
                        noGenerateReport.show();
                        showGenerateReport.hide();
                        selectStoreDropdownDiv.find('input[name="hourly_stores"]').attr('int-value', 0).val('Show All Stores')/*.removeAttr('readonly')*/
                        selectCallcenterDropdownDiv.find('input[name="hourly_callcenters"]').attr('int-value', 0).val('Show All Call Centers')/*.removeAttr('readonly')*/
                        selectProvinceDropdownDiv.find('input[name="hourly_provinces"]').attr('int-value', 0).val('Show All Provinces')/*.removeAttr('readonly')*/
                        selectTransactionDropdownDiv.find('input[name="hourly_transactions"]').attr('data-value', 'Show All Transactions').val('Show All Transactions');
                        //date to should be larger than the date from
                        console.log('date to should be larger than the date from')
                        admin.dst_reports.show_spinner(hourlySalesReportXlsBtn, true);
                    }
                    else
                    {
                        var transType = selectTransactionDropdownDiv.find('input[name="hourly_transactions"]').attr('data-value');
                        //ajax call
                        var oParams = {
                            "params": {
                                "where": [
                                    {
                                        "field"   : "o.order_status",
                                        "operator": "=",
                                        "value"   : 19
                                    }
                                ],
                                "date_from" : dateFromValue,
                                "date_to"   : dateToValue,
                                "limit"     : getOrderLimit,
                                "transaction_type" : transType
                            },
                            "user_id" : iConstantUserId
                        }

                        var iStoreID = selectStoreDropdownDiv.find('input[name="hourly_stores"]').attr('int-value');
                        if (iStoreID > 0) {
                            oParams.params.where.push({
                                'field'   : 'o.store_id',
                                'operator': '=',
                                'value'   : iStoreID
                            });
                            oParams.params.store_id = iStoreID
                        }

                        var iProvinceID = selectProvinceDropdownDiv.find('input[name="hourly_provinces"]').attr('int-value');
                        if (iProvinceID > 0) {
                            oParams.params.where.push({
                                'field'   : 'o.province_id',
                                'operator': '=',
                                'value'   : iProvinceID
                            });
                        }

                        var iCCenterID = selectCallcenterDropdownDiv.find('input[name="hourly_callcenters"]').attr('int-value');
                        if (iCCenterID > 0) {
                            oParams.params.where.push({
                                'field'   : 'cu.callcenter_id',
                                'operator': '=',
                                'value'   : iCCenterID
                            });
                        }

                        admin.dst_reports.show_spinner(generateByDateBtn, true);
                        admin.hourly_sales_report.get_hourly_sales_report(oParams);
                    }
                }

            });

            //generate by period button
            generateByPeriodBtn.on('click', function(){
                var dateFromInput = generateByDateDiv.find('input[name="hourly_sales_report_date_from"]')
                var dateToInput = generateByDateDiv.find('input[name="hourly_sales_report_date_to"]')

                if(dateFromInput.val() == '' || dateToInput.val() == '')
                {
                    //show no generate report
                    noGenerateReport.show();
                    showGenerateReport.hide();
                    selectStoreDropdownDiv.find('input[name="hourly_stores"]').attr('int-value', 0).val('Show All Stores')/*.removeAttr('readonly')*/
                    selectCallcenterDropdownDiv.find('input[name="hourly_callcenters"]').attr('int-value', 0).val('Show All Call Centers')/*.removeAttr('readonly')*/
                    selectProvinceDropdownDiv.find('input[name="hourly_provinces"]').attr('int-value', 0).val('Show All Provinces')/*.removeAttr('readonly')*/
                    selectTransactionDropdownDiv.find('input[name="hourly_transactions"]').attr('data-value', 'Show All Transactions').val('Show All Transactions');
                    admin.dst_reports.show_spinner(hourlySalesReportXlsBtn, true);
                }
                else
                {
                    var dateFrom = moment(dateFromInput.val()),
                        dateTo = moment(dateToInput.val()),
                        dateFromValue = moment(dateFrom).format("YYYY-MM-DD HH:mm:ss"),
                        dateToValue = moment(dateTo).format("YYYY-MM-DD");
                        dateToValue = moment(dateToValue+" 23:59:59").format("YYYY-MM-DD HH:mm:ss");

                    if(dateTo.diff(dateFrom, 'days') < 0)
                    {
                        noGenerateReport.show();
                        showGenerateReport.hide();
                        selectStoreDropdownDiv.find('input[name="hourly_stores"]').attr('int-value', 0).val('Show All Stores')/*.removeAttr('readonly')*/
                        selectCallcenterDropdownDiv.find('input[name="hourly_callcenters"]').attr('int-value', 0).val('Show All Call Centers')/*.removeAttr('readonly')*/
                        selectProvinceDropdownDiv.find('input[name="hourly_provinces"]').attr('int-value', 0).val('Show All Provinces')/*.removeAttr('readonly')*/
                        selectTransactionDropdownDiv.find('input[name="hourly_transactions"]').attr('data-value', 'Show All Transactions').val('Show All Transactions');
                        //date to should be larger than the date from
                        console.log('date to should be larger than the date from');
                        admin.dst_reports.show_spinner(hourlySalesReportXlsBtn, true);
                    }
                    else
                    {
                        var transType = selectTransactionDropdownDiv.find('input[name="hourly_transactions"]').attr('data-value');
                        //ajax call
                        var oParams = {
                            "params": {
                                "where": [
                                    {
                                        "field"   : "o.order_status",
                                        "operator": "=",
                                        "value"   : 19
                                    }
                                ],
                                "date_from" : dateFromValue,
                                "date_to"   : dateToValue,
                                "limit"     : getOrderLimit,
                                "transaction_type" : transType
                            },
                            "user_id" : iConstantUserId
                        }

                        // var iStoreID = selectStoreDropdownDiv.find('input[name="sales_report_store"]').attr('int-value');
                        // if (iStoreID > 0) {
                        //     oParams.params.where.push({
                        //         'field'   : 'o.store_id',
                        //         'operator': '=',
                        //         'value'   : iStoreID
                        //     });
                        // }

                        admin.dst_reports.show_spinner(generateByPeriodBtn, true);
                        admin.hourly_sales_report.get_hourly_sales_report(oParams);
                    }
                }
            });

            //generate excel button
            hourlySalesReportXlsBtn.on('click', function() {
                var hourlySalesReportTable = $('section.content').find('div[content="hourly_report"] table#hourly_sales_report_table');
                var hourlySalesReportTableXls = $('section.content').find('div[content="hourly_report"] table#hourly_sales_report_table_hidden');
                
                admin.dst_reports.show_spinner(hourlySalesReportXlsBtn, true);
                
                if(hourlySalesReportTable.is(':visible'))
                {
                    hourlySalesReportTableXls.table2excel({
                        exclude: ".notthis",
                        name: "Hourly Sales Report",
                        filename: "Hourly_Sales_Report"/*+moment().unix('X')*/
                    });
                }

                setTimeout(function(){
                    admin.dst_reports.show_spinner(hourlySalesReportXlsBtn, false);
                }, 3000) 
            });

            //period dropdown
            selectPeriodDropdownDiv.find('div.option').on('click', function(){
                var uiThis = $(this);
                selectPeriodDropdownDiv.find('input[name="period"]').attr('value', uiThis.attr('data-value'))
            });

            //store dropdown
            selectStoreDropdownDiv.on('click', 'div.option', function(){
                var uiThis = $(this);
                selectStoreDropdownDiv.find('input[name="hourly_stores"]').attr('int-value', uiThis.attr('int-value'))

                // admin.hourly_sales_report.trigger_ajax_on_filter_click();
            });


            //province dropdown
            selectProvinceDropdownDiv.on('click', 'div.option', function(){
                var uiThis = $(this);
                selectProvinceDropdownDiv.find('input[name="hourly_provinces"]').attr('int-value', uiThis.attr('int-value'))

                // admin.hourly_sales_report.trigger_ajax_on_filter_click();
            });

            //callcenter dropdown
            selectCallcenterDropdownDiv.on('click', 'div.option', function(){
                var uiThis = $(this);
                selectCallcenterDropdownDiv.find('input[name="hourly_callcenters"]').attr('int-value', uiThis.attr('int-value'))

                // admin.hourly_sales_report.trigger_ajax_on_filter_click();
            });

            //transaction dropdown
            selectTransactionDropdownDiv.on('click', 'div.option', function(){
                var uiThis = $(this);
                selectTransactionDropdownDiv.find('input[name="hourly_transactions"]').attr('data-value', uiThis.attr('data-value'))

                // admin.hourly_sales_report.trigger_ajax_on_filter_click();
            });
        },

        'trigger_ajax_on_filter_click': function(){
            var dateFromInput = generateByDateDiv.find('input[name="hourly_sales_report_date_from"]')
            var dateToInput = generateByDateDiv.find('input[name="hourly_sales_report_date_to"]')
            var dateFrom = moment(dateFromInput.val()),
                dateTo = moment(dateToInput.val()),
                dateFromValue = moment(dateFrom).format("YYYY-MM-DD HH:mm:ss"),
                dateToValue = moment(dateTo).format("YYYY-MM-DD");
                dateToValue = moment(dateToValue+" 23:59:59").format("YYYY-MM-DD HH:mm:ss");
            var transType = selectTransactionDropdownDiv.find('input[name="hourly_transactions"]').attr('data-value');


            if(dateTo.diff(dateFrom, 'days') < 0)
            {
                noGenerateReport.show();
                showGenerateReport.hide();
                selectStoreDropdownDiv.find('input[name="hourly_stores"]').attr('int-value', 0).val('Show All Stores')/*.removeAttr('readonly')*/
                selectCallcenterDropdownDiv.find('input[name="hourly_callcenters"]').attr('int-value', 0).val('Show All Call Centers')/*.removeAttr('readonly')*/
                selectProvinceDropdownDiv.find('input[name="hourly_provinces"]').attr('int-value', 0).val('Show All Provinces')/*.removeAttr('readonly')*/
                selectTransactionDropdownDiv.find('input[name="hourly_transactions"]').attr('data-value', 'Show All Transactions').val('Show All Transactions');
                //date to should be larger than the date from
                console.log('date to should be larger than the date from');
                admin.dst_reports.show_spinner(hourlySalesReportXlsBtn, true);
            }
            else
            {
                //ajax call
                var oParams = {
                    "params": {
                        "where": [
                            {
                                "field"   : "o.order_status",
                                "operator": "=",
                                "value"   : 19
                            }
                        ],
                        "date_from" : dateFromValue,
                        "date_to"   : dateToValue,
                        "limit"     : getOrderLimit,
                        "transaction_type" : transType
                    },
                    "user_id" : iConstantUserId
                }

                var iStoreID = selectStoreDropdownDiv.find('input[name="hourly_stores"]').attr('int-value');
                if (iStoreID > 0) {
                    oParams.params.where.push({
                        'field'   : 'o.store_id',
                        'operator': '=',
                        'value'   : iStoreID
                    });
                    oParams.params.store_id = iStoreID
                }

                var iProvinceID = selectProvinceDropdownDiv.find('input[name="hourly_provinces"]').attr('int-value');
                if (iProvinceID > 0) {
                    oParams.params.where.push({
                        'field'   : 'o.province_id',
                        'operator': '=',
                        'value'   : iProvinceID
                    });
                }

                var iCCenterID = selectCallcenterDropdownDiv.find('input[name="hourly_callcenters"]').attr('int-value');
                if (iCCenterID > 0) {
                    oParams.params.where.push({
                        'field'   : 'cu.callcenter_id',
                        'operator': '=',
                        'value'   : iCCenterID
                    });
                }

                admin.dst_reports.show_spinner(generateByDateBtn, true);
                admin.hourly_sales_report.get_hourly_sales_report(oParams);
            }
        },

        'get_hourly_sales_report' :function(oParams)
        {
            var oAjaxConfig = {
                "type"   : "GET",
                "data"   : oParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : admin.config('url.api.jfc.reports') + "/reports/hourly_sales_report",
                "success": function (oData) {
                    if(typeof(oData.data) != "undefined")
                    {
                        //console.log(oData)

                        //show table
                        noGenerateReport.hide();
                        showGenerateReport.show().removeClass('hidden');

                        $('tbody.hourly_sales_report_tr_container').find('tr.hourly_sales_report_tr:not(.template)').remove();
                        $('tbody.hourly_sales_report_tr_container_hidden').find('tr.hourly_sales_report_tr_hidden:not(.template)').remove();
                        $('tbody.hourly_sales_report_tr_container_day_part').find('tr.hourly_sales_report_tr_day_part:not(.template)').remove();

                        var uiContentContainer = $('tbody.hourly_sales_report_tr_container');
                        var uiContentContainerHidden = $('tbody.hourly_sales_report_tr_container_hidden');
                        
                        $.each(oData.data , function (key, value){
                            var uiTemplate = $('table#hourly_sales_report_table').find('tr.hourly_sales_report_tr.template').clone().removeClass('template notthis');
                            admin.hourly_sales_report.manipulate_template(uiContentContainer, uiTemplate, value);

                            //for xls download
                            var uiTemplateHidden = $('table#hourly_sales_report_table_hidden').find('tr.hourly_sales_report_tr_hidden.template').clone().removeClass('template notthis');
                            admin.hourly_sales_report.manipulate_template(uiContentContainerHidden, uiTemplateHidden, value);
                        })


                        //percentage = Total Sales/Total Net Sales*100
                        //total net sales
                        var total_net_sales = 0;
                        $('table#hourly_sales_report_table').find('td.total_sales:not(.template):visible').each(function(){
                            var total_net = $(this).text().replace(',', '');
                            total_net = total_net.replace('PHP ', '');
                            total_net_sales += parseFloat(total_net);
                        })
                        //console.log(total_net_sales)
                        //compute for percentage
                        var total_percentage = 0;
                        $('table#hourly_sales_report_table').find('td.percentage:not(.template):visible').each(function(){
                            var total_sales = $(this).siblings('td.total_sales').text();
                            total_sales = total_sales.replace(',','');
                            total_sales = parseFloat(total_sales.replace('PHP ',''));

                            var percentage = (total_sales/ total_net_sales ) * 100;
                            //console.log(percentage)
                            if(isNaN(percentage))
                            {
                                percentage = 0;
                            }
                            total_percentage += parseFloat(percentage);
                            $(this).text( percentage.toFixed(2) +'%')
                        })

                        //average Order = Total Sales/Transaction Count
                        //compute for average order
                        var total_average_order = 0;
                        $('table#hourly_sales_report_table').find('td.average_order:not(.template):visible').each(function(){
                            var transaction_count = $(this).siblings('td.transaction_count').text();
                            transaction_count = parseFloat(transaction_count);

                            var total_sales = $(this).siblings('td.total_sales').text();
                            total_sales = total_sales.replace(',','');
                            total_sales = parseFloat(total_sales.replace('PHP ',''));

                            var average_order = (total_sales/transaction_count);

                            if(isNaN(average_order))
                            {
                                average_order = 0;
                            }
                            total_average_order += parseFloat(average_order);
                            $(this).text(/* 'PHP '+ */average_order.toFixed(2))
                        })

                        var total_food_sales = 0;
                        $('table#hourly_sales_report_table').find('td.food_sales:not(.template):visible').each(function(){
                            var total_food_net = $(this).text().replace(',', '');
                            total_food_net = total_food_net.replace('PHP ', '');
                            total_food_sales += parseFloat(total_food_net);
                        });

                        var total_transaction_count = 0;
                        $('table#hourly_sales_report_table').find('td.transaction_count:not(.template):visible').each(function(){
                            var total_trans = $(this).text().replace(',', '');
                            total_transaction_count += parseFloat(total_trans);
                        });

                        //manipulate day part view
                        admin.hourly_sales_report.manipulate_template_day_part(oData);

                        var uiContentDayPartContainer = $('tbody.hourly_sales_report_tr_container_day_part');

                        //compute for percentage for day part
                        var total_percentage = 0;
                        $('table#hourly_sales_report_table_day_part').find('td.percentage:not(.template):visible').each(function(){
                            var total_sales = $(this).siblings('td.total_sales').text();
                            total_sales = total_sales.replace(',','');
                            total_sales = parseFloat(total_sales.replace('PHP ',''));

                            var percentage = (total_sales/ total_net_sales ) * 100;
                            //console.log(percentage)
                            if(isNaN(percentage))
                            {
                                percentage = 0;
                            }
                            total_percentage += parseFloat(percentage);
                            $(this).text( percentage.toFixed(2) +'%')
                        });

                        //average Order = Total Sales/Transaction Count
                        //compute for average order for day part
                        var total_average_order = 0;
                        $('table#hourly_sales_report_table_day_part').find('td.average_order:not(.template):visible').each(function(){
                            var transaction_count = $(this).siblings('td.transaction_count').text();
                            transaction_count = parseFloat(transaction_count);

                            var total_sales = $(this).siblings('td.total_sales').text();
                            total_sales = total_sales.replace(',','');
                            total_sales = parseFloat(total_sales.replace('PHP ',''));

                            var average_order = (total_sales/transaction_count);

                            if(isNaN(average_order))
                            {
                                average_order = 0;
                            }
                            total_average_order += parseFloat(average_order);
                            $(this).text(/* 'PHP '+ */average_order.toFixed(2))
                        });

                        var total_net_sales = total_net_sales.toFixed(2);
                        total_net_sales = admin.hourly_sales_report.numberWithCommas(total_net_sales);
                        var total_food_sales = total_food_sales.toFixed(2);
                        total_food_sales = admin.hourly_sales_report.numberWithCommas(total_food_sales);
                        var total_percentage = total_percentage.toFixed(2);
                        total_percentage = admin.hourly_sales_report.numberWithCommas(total_percentage);
                        var total_average_order = total_average_order.toFixed(2);
                        total_average_order = admin.hourly_sales_report.numberWithCommas(total_average_order);

                        //for total summary at the bottom
                        var uiTemplate = $('table#hourly_sales_report_table').find('tr.hourly_sales_report_tr.template').clone().removeClass('template notthis');
                        var uiTemplateHidden = $('table#hourly_sales_report_table_hidden').find('tr.hourly_sales_report_tr_hidden.template').clone().removeClass('template notthis');
                        var uiTemplateDayPart = $('table#hourly_sales_report_table_day_part').find('tr.hourly_sales_report_tr_day_part.template').clone().removeClass('template notthis');

                        uiTemplate.find('.date_range').text('').css('font-weight', 'bold');
                        uiTemplate.find('.hour_interval').text('').css('font-weight', 'bold');
                        uiTemplate.find('.store_name').text('Net Sales').css('font-weight', 'bold');
                        uiTemplate.find('.total_sales').text( 'PHP '+ total_net_sales).css('font-weight', 'bold');
                        uiTemplate.find('.food_sales').text( 'PHP '+ total_food_sales).css('font-weight', 'bold');
                        uiTemplate.find('.transaction_count').text(total_transaction_count).css('font-weight', 'bold');
                        uiTemplate.find('.percentage').text(total_percentage + " %").css('font-weight', 'bold');
                        uiTemplate.find('.average_order').text(total_average_order).css('font-weight', 'bold');

                        uiTemplateHidden.find('.date_range').text('').css('font-weight', 'bold');
                        uiTemplateHidden.find('.hour_interval').text('').css('font-weight', 'bold');
                        uiTemplateHidden.find('.store_name').text('Net Sales').css('font-weight', 'bold');
                        uiTemplateHidden.find('.total_sales').text( 'PHP '+ total_net_sales).css('font-weight', 'bold');
                        uiTemplateHidden.find('.food_sales').text( 'PHP '+ total_food_sales).css('font-weight', 'bold');
                        uiTemplateHidden.find('.transaction_count').text(total_transaction_count).css('font-weight', 'bold');
                        uiTemplateHidden.find('.percentage').text(total_percentage + "%").css('font-weight', 'bold');
                        uiTemplateHidden.find('.average_order').text(total_average_order).css('font-weight', 'bold');
                        
                        uiTemplateDayPart.find('.date_range').text('').css('font-weight', 'bold');
                        uiTemplateDayPart.find('.hour_interval').text('').css('font-weight', 'bold');
                        uiTemplateDayPart.find('.store_name').text('Net Sales').css('font-weight', 'bold');
                        uiTemplateDayPart.find('.total_sales').text( 'PHP '+ total_net_sales).css('font-weight', 'bold');
                        uiTemplateDayPart.find('.food_sales').text( 'PHP '+ total_food_sales).css('font-weight', 'bold');
                        uiTemplateDayPart.find('.transaction_count').text(total_transaction_count).css('font-weight', 'bold');
                        uiTemplateDayPart.find('.percentage').text(total_percentage + " %").css('font-weight', 'bold');
                        uiTemplateDayPart.find('.average_order').text(total_average_order).css('font-weight', 'bold');

                        uiContentContainer.append(uiTemplate);
                        uiContentContainerHidden.append(uiTemplateHidden);
                        uiContentDayPartContainer.append(uiTemplateDayPart);

                    }
                    //enable download excel button
                    admin.dst_reports.show_spinner(generateByDateBtn, false);
                    admin.dst_reports.show_spinner(hourlySalesReportXlsBtn, false);
                }
            }   
            admin.hourly_sales_report.ajax(oAjaxConfig);
        },

        'manipulate_template': function(uiContentContainer, uiTemplate, oHourlyData){
            if(typeof(uiTemplate) != 'undefined' && typeof(oHourlyData) != 'undefined')
            {
                var sDateRange = moment(oHourlyData.date_from).format('MMMM DD, YYYY') +' to '+ moment(oHourlyData.date_to).format('MMMM DD, YYYY')
                var sTotalSales = parseFloat(oHourlyData.total_sales).toFixed(2);
                    sTotalSales = admin.hourly_sales_report.numberWithCommas(sTotalSales);
                var sFoodSales = parseFloat(oHourlyData.food_sales).toFixed(2);
                    sFoodSales = admin.hourly_sales_report.numberWithCommas(sFoodSales);

                uiTemplate.find('.date_range').text(sDateRange)
                uiTemplate.find('.hour_interval').text(oHourlyData.hour)
                uiTemplate.find('.store_name').text(oHourlyData.store_name)
                uiTemplate.find('.total_sales').text( 'PHP '+ sTotalSales)
                uiTemplate.find('.food_sales').text( 'PHP '+ sFoodSales)
                uiTemplate.find('.transaction_count').text(oHourlyData.transaction_count)
                uiTemplate.find('.percentage').text(oHourlyData.percentage)
                uiTemplate.find('.average_order').text(oHourlyData.average_order)

                uiContentContainer.append(uiTemplate);
            }
        },

        'manipulate_template_day_part': function(oHourlyData){
            if(typeof(oHourlyData) != 'undefined')
            {
                var uiContentDayPartContainer = $('tbody.hourly_sales_report_tr_container_day_part');

                $.each(oHourlyData.day_part_view, function (key, value){
                    var uiTemplate = $('table#hourly_sales_report_table_day_part').find('tr.hourly_sales_report_tr_day_part.template').clone().removeClass('template notthis');
                    var sDateRange = moment(value.date_from).format('MMMM DD, YYYY') +' to '+ moment(value.date_to).format('MMMM DD, YYYY');
                    var sTotalSales = parseFloat(value.total_sales).toFixed(2);
                        sTotalSales = admin.hourly_sales_report.numberWithCommas(sTotalSales);
                    var sFoodSales = parseFloat(value.food_sales).toFixed(2);
                        sFoodSales = admin.hourly_sales_report.numberWithCommas(sFoodSales);

                    var dayPartName = value.day_part_name;
                    var startingHour = value.hour_start.split(' to ');
                    var endingHour = value.hour_end.split(' to ');
                    
                    startingHour = moment(startingHour[0], "HH:mm:ss").format('hh:mm a');
                    endingHour = moment(endingHour[1], "HH:mm:ss").format('hh:mm a');

                    uiTemplate.find('.date_range').text(sDateRange);
                    uiTemplate.find('.hour_interval').text(dayPartName +' ('+ startingHour +'-'+ endingHour +')')
                    uiTemplate.find('.store_name').text(value.store_name);
                    uiTemplate.find('.total_sales').text( 'PHP '+ sTotalSales)
                    uiTemplate.find('.food_sales').text( 'PHP '+ sFoodSales)
                    uiTemplate.find('.transaction_count').text(value.transaction_count)
                    uiTemplate.find('.percentage').text(value.percentage)
                    uiTemplate.find('.average_order').text(value.average_order)

                    uiContentDayPartContainer.append(uiTemplate);
                });
            }
        },
        'ajax': function (oAjaxConfig) {
            if (typeof(oAjaxConfig) != 'undefined') {
                admin.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        'numberWithCommas': function(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

    }

}());

$(window).load(function () {
    "use strict";
    admin.hourly_sales_report.initialize();
});


