/* 
	The Class that will transform normal select box into custom (stylable) drop down.
	
	@author: D. Canillo
			 dominic@cr8vwebsolutions.com
			 
	
	@dependecy: jQuery 1.7 + 
 */
 
(function($){
	

	
	var 
		el, // the element
		oDefaults = {}, // default options
		sDDValue = '', // value of the dropdown.
		oMethods = {},  // method list
		sBaseUrl = localStorage.getItem("base_url");
	
	/* class definition */
	function search (uiElement, oOptions)
	{
		this.initialize(uiElement, oOptions);	
	}
	
	
	/* function that will assemble the Dropdown markup. */
	function assembleMarkup (oOptions)
	{
		
	}
	
	
	function bindEvents (uiElement, oOptions)
	{
		//setup before functions
		var typingTimer;                //timer identifier
		var iDoneTypingInterval = 500;  //time in ms, 5 second for example
        var iMinLength = 1;
		//on keyup, start the countdown
		uiElement.on('keyup', function () {
            if(uiElement.val().length > iMinLength )
            {
                clearTimeout(typingTimer);
                typingTimer = setTimeout(doneTyping, iDoneTypingInterval);
                $('div.list').remove();
            }
		});

		//on keydown, clear the countdown 
		uiElement.on('keydown', function () {
            if(uiElement.val().length > iMinLength )
            {
                clearTimeout(typingTimer);
                $('div.list').remove();
            }

		});

		//user is "finished typing," do something
		function doneTyping () {
		  doSearch(oOptions);
		}
	}
	
	function doSearch(oParams)
	{
        //console.log( oParams.data)
		if (typeof (oParams) != 'undefined')
		{
			$.ajax({
				"type" : "Get",
				"data" : oParams.data,
				"url"  : oParams.url + '?X-API-KEY=IEYRtW2cb7A5Gs54A1wKElECBL65GVls',

				"success" : function(data)
				{
					if (typeof (oParams.callback) == 'function')
					{
						oParams.callback(data);
					}
				}
			});
		}
	}
	
	
	
	/* 
		Extends the main class to have initialize function, this will be the centralized method that will call all required method to transform the dropdown.
	*/
	search.prototype.initialize = function(uiElement, oOptions)
	{

		uiElement.addClass("search-autocomplete");
		bindEvents (uiElement, oOptions);
	}
	
	
	$.fn.search_autocomplete = function(param)
	{

		if (typeof (param) == 'string' && typeof (oMethods[param]) == 'function')
		{
			return oMethods[param]( this );
		}
		else
		{

			return this.each(function(){
				if ( ! $(this).hasClass("search-autocomplete"))
				{
					new search($(this), param);
				}
			});
		}
	}	
	
 })(jQuery);