/**
 *  Coordinator management Class
 *
 *
 *
 */
(function () {
    "use strict";
    var oStores = [];
    var oTable;
    var oTableStoreMessenger;
    var iSelectedStore = 1;
    var oStoreCount = 0;

    var nServerTimeInterval;
    var momentServerTime;
    var serverTime;
    var oEllapsedTime = {};

    function getServerTime(fnCallback) {
        var oAjaxConfig = {
            "type"   : "GET",
            "data"   : [],
            "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
            "url"    : callcenter.config('url.api.jfc.users') + "/users/time",
            "success": function (sTime) {
                serverTime = sTime;
                momentServerTime =  moment(serverTime);

                clearInterval(nServerTimeInterval);
                nServerTimeInterval = setInterval(function () {
                    momentServerTime.add(1, 's');
                    ////console.log(momentServerTime.hours() + ':' + momentServerTime.minutes() + ':' + momentServerTime.seconds() );
                }, 1000);

                if(fnCallback !== undefined && typeof fnCallback === 'function') {
                    fnCallback();
                }
            }
        };
        callcenter.coordinator_management.cajax(oAjaxConfig);
    }
    function createElapsedTime(iOrderID, sDateAddedExact, uiTemplate) {
        var momentOrderTime, momentOrderDiff,
            nOrderId = iOrderID;

        // get difference between the server time and the date added time
        momentOrderTime = moment(sDateAddedExact);
        momentOrderDiff = momentServerTime.diff(momentOrderTime);

        // update the elapsed time
        oEllapsedTime[nOrderId] = new Timer(momentOrderDiff, false, function (hr, mn, se, oOtherData) {
            //$('div[data-transaction-order-id="' + oOtherData + '"]').find('span[data-label="transaction_elapsed_time"]').text(hr + ':' + mn + ':' + se);
            uiTemplate.find('info.elapsed_time').text(hr + ':' + mn + ':' + se);
            var tElapsedTime = hr + ':' + mn + ':' + se;
            var s = moment.duration(tElapsedTime);
            if(s._milliseconds > 300000) // 5 minutes
            {
                uiTemplate.find('div.block-content').find('button.edit_order').addClass('hidden');
                uiTemplate.find('div.block-content').find('button[modal-target="void-order"]').addClass('hidden');
            }

        }, function (oOtherData) {}, nOrderId);
        oEllapsedTime[nOrderId].start();
        
        //console.log(nOrderId)
    }

    function timeDiff(iSecondsDiff) {
        var hours = Math.floor(iSecondsDiff / (60 * 60));
   
        var divisor_for_minutes = iSecondsDiff % (60 * 60);
        var minutes = Math.floor(divisor_for_minutes / 60);
     
        var divisor_for_seconds = divisor_for_minutes % 60;
        var seconds = Math.ceil(divisor_for_seconds);

        hours = (hours < 10) ? '0' + hours.toString() : hours.toString();
        minutes = (minutes < 10) ? '0' + minutes.toString() : minutes.toString();
        seconds = (seconds < 10) ? '0' + seconds.toString() : seconds.toString();
        var obj = {
            "h": hours,
            "m": minutes,
            "s": seconds
        };
        return obj;
    }

    CPlatform.prototype.coordinator_management = {

        'clone_append': function (uiTemplate, uiContainer) {
            if (typeof(uiTemplate) !== 'undefined' && typeof(uiContainer) !== 'undefined') {
                uiContainer.append(uiTemplate);
            }
        },


        'remove_append': function (uiElem) {
            if (typeof(uiElem) !== 'undefined') {
                uiElem.remove();
            }
        },

        'show_spinner': function (uiElem, bShow) {
            if (bShow == true) {
                if(uiElem.find('i.fa-spinner').length > 0)
                {
                    uiElem.find('i.fa-spinner').removeClass('hidden');
                }
                else
                {
                    uiElem.append('<i class="fa fa-spinner fa-pulse"></i>');
                }

                uiElem.prop('disabled', true);
            }
            else {
                if(uiElem.find('i.fa-spinner').length > 0)
                {
                    uiElem.find('i.fa-spinner').addClass('hidden');
                    uiElem.find('i.fa-spinner').remove();
                }
                uiElem.prop('disabled', false);
            }
        },

        'update_order_id' : function(iOrderID, iOrderStatus) {
            if(typeof(cr8v.get_from_storage('orders', '')) != 'undefined' && typeof(iOrderID) !== 'undefined' && typeof(iOrderStatus) !== 'undefined')
            {
                var oOrder = cr8v.get_from_storage('orders', '');
                if(typeof(oOrder) !== 'undefined')
                {
                    oOrder = oOrder[0];
                    $.each(oOrder, function(key, value){
                        if(value.id == iOrderID)
                        {
                            value.order_status = iOrderStatus;
                        }
                    })

                    cr8v.pop('orders');
                    cr8v.add_to_storage('orders', oOrder);
                }
            }
        },

        'initialize': function () {
            getServerTime();
            var uiButton;
            var uiAddMealButton;
            oSock.bind({
                'event'   : 'pusher:member_added',
                'channel' : 'presence-ironman',
                'callback': function (oData) {
                    //console.log(oData)
                    callcenter.store_management.redraw_presence();
                    //alert(JSON.stringify(oData))
                }
            });

            oSock.bind({
                'event'   : 'pusher:subscription_succeeded',
                'channel' : 'presence-ironman',
                'callback': function (oData) {
                    //console.log(oData)
                    callcenter.store_management.redraw_presence();
                }
            });

            oSock.bind({
                'event'   : 'pusher:member_removed',
                'channel' : 'presence-ironman',
                'callback': function (oData) {
                    //console.log(oData)
                    callcenter.store_management.redraw_presence();
                    //alert(JSON.stringify(oData))
                }
            });

            oSock.bind({
                'event'   : 'pusher:member_added',
                'channel' : 'presence-ironman',
                'callback': function (oData) {
                    var storeIdStorage = cr8v.get_from_storage('active_storage', '');
                    if(typeof(storeIdStorage) !== 'undefined')
                    {
                        if(oData.info.hasOwnProperty('store_user_id') && oData.info.store_id == storeIdStorage[0].current_store_id) //if it is a store
                        {
                            var bAvailability = callcenter.gis.check_store_availability(oData.info.store_id);

                            if ($('section.content div#add_customer_content').is(':visible') == true) //if in add customer find RTA
                            {
                                uiButton = $('section.content div#add_customer_content').find('button.order_process.rta_proceed');
                                
                                if(bAvailability){
                                    if($('div.rta-address-info-container:visible').length > 0)
                                    {
                                        var sStoreName = $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text()
                                        $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
                                        uiButton.text('Proceed to Order').attr('data-order-rta', 1);
                                    }
                                }
                                else
                                {
                                    if($('div.rta-address-info-container:visible').length > 0)
                                    {
                                        var sStoreName = $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text()
                                        sStoreName = sStoreName.replace(' (Offline)', '');
                                        //console.log()
                                        $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text(sStoreName + ' (Offline)').css('color', 'red');
                                        uiButton.text('Proceed to Order (Manual Order)').attr('data-order-rta', 4);
                                    }
                                    //manual order
                                }
                            }
                            else if ($('section.content div#search_customer_content').is(':visible') == true) //if in add customer find RTA
                            {
                                uiButton = $('section.content div#search_customer_content').find('button.proceed_order_from_search');
                                uiAddMealButton = $('section.content div#search_customer_content').find('button.add_last_meal_to_order');

                                if(bAvailability){
                                    if($('div.rta-address-info-container:visible').length > 0)
                                    {
                                        var sStoreName = $('div.rta-address-info-container:visible').find('span.retail-store-name').text()
                                        $('div.rta-address-info-container:visible').find('span.retail-store-name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
                                        uiAddMealButton.text('Add Last Order to Cart').attr('data-order-rta', 1);
                                        uiButton.text('Proceed to Order').attr('data-order-rta', 1);
                                    }
                                }
                                else
                                {
                                    if($('div.rta-address-info-container:visible').length > 0)
                                    {
                                        var sStoreName = $('div.rta-address-info-container:visible').find('span.retail-store-name').text()
                                        sStoreName = sStoreName.replace(' (Offline)', '');
                                        //console.log()
                                        $('div.rta-address-info-container:visible').find('span.retail-store-name').text(sStoreName + ' (Offline)').css('color', 'red');
                                        uiAddMealButton.text('Add Last Order to Cart (Manual Order)').attr('data-order-rta', 4);
                                        uiButton.text('Proceed to Order (Manual Order)').attr('data-order-rta', 4);
                                    }
                                }
                            }

                            else if($('section.content div#order_process_content').is(':visible') == true)
                            {
                                uiButton = $('section.content div#order_process_content').find('button[modal-target="manual-order-summary"]');
                                var orderType = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_type"]').val()
                                //var customerType = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="customer_type"]').val()
                                var customerType = 'new';
                                var total_bill = $('div.cart-container table.cart-table-table').find('td.total_bill').attr('data-total-bill');
                                // var existingCustomerConfigVal = parseInt('2500');
                                var newCustomerConfigVal = parseFloat('2500.00');
                                // orderType = iRetailFound;
                                // $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_type"]').val(iRetailFound);

                                if(bAvailability){
                                    //if online
                                    if($('div.rta-container div.rta_store:visible').length > 0)
                                    {
                                        var sStoreName = $('div.rta-container div.rta_store:visible').find('strong.store_name').text()
                                        $('div.rta-container div.rta_store:visible').find('strong.store_name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
                                    }

                                    if(customerType === 'existing')
                                    {
                                        // if(total_bill > existingCustomerConfigVal)
                //        {
                                        //     $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                                        //     $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
                                        //     $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                //                    }
                //                }
                //                else
                //                {
                                            $('section.content #order_process_content').find('button.send_order').text('Send Order');
                                            $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
                                            $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('23');
                //                    }
                                    }
                                    if(customerType === 'new')
                                    {
                                        if(parseFloat(total_bill) > newCustomerConfigVal)
                                        {
                                            $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                                            $('section.content #order_process_content').find('button.advance_order')/*.text('Verify Advance Order')*/.hide();
                                            $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                                        }
                                        else
                                        {
                                            $('section.content #order_process_content').find('button.send_order').text('Send Order');
                                            $('section.content #order_process_content').find('button.advance_order').text('Advance Order').show();
                                            $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
                                        }
                                    }

                                    var uiModalOrderComplete = $('div[modal-id="manual-order-complete"]').find('h4.order_type');
                                    uiModalOrderComplete.text('Regular Order Complete');
                                }
                                else
                                {
                                    //if offline
                                    if($('div.rta-container div.rta_store:visible').length > 0)
                                    {
                                        var sStoreName = $('div.rta-container div.rta_store:visible').find('strong.store_name').text()
                                        sStoreName = sStoreName.replace(' (Offline)', '');
                                        $('div.rta-container div.rta_store:visible').find('strong.store_name').text(sStoreName + ' (Offline)').css('color', 'red');
                                    }


                                    if(customerType === 'existing')
                                    {
                                        // if(total_bill > existingCustomerConfigVal)
                //            {
                                        //     $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                                        //     $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
                                        //     $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                //                    }
                //                else
                //                {
                                            $('section.content #order_process_content').find('button.send_order').text('Manual Order');
                                            $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
                                            $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
                //                    }
                                    }
                                    if(customerType === 'new')
                                    {
                                        if(parseFloat(total_bill) > newCustomerConfigVal)
                                        {
                                            $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                                            $('section.content #order_process_content').find('button.advance_order')/*.text('Verify Advance Order')*/.hide();
                                            $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                                        }
                                        else
                                        {
                                            $('section.content #order_process_content').find('button.send_order').text('Manual Order');
                                            $('section.content #order_process_content').find('button.advance_order').text('Advance Order').show();
                                            $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
                                        }
                                    }
                                    var uiModalOrderComplete = $('div[modal-id="manual-order-complete"]').find('h4.order_type');
                                    uiModalOrderComplete.text('Manual Order Complete');
                                }
                            }


                            else if ($('form#coordinator_update_address').is(':visible') == true) //if in update address find RTA
                            {

                                var uiButton = $('form#coordinator_update_address').find('button.confirm_update_address');

                                if(bAvailability)
                                {
                                    //if onlines 
                                    if($('div.rta-address-info-container:visible').find(".retail-store-name").html() != 'Out of Delivery Area')
                                    {
                                        uiButton.attr('data-order-status','23');
                                        uiButton.text('Send Order');

                                        if($('div.rta-address-info-container:visible').length > 0)
                                        {
                                            var sStoreName = $('div.rta-address-info-container:visible').find('span.retail-store-name').text()
                                            $('div.rta-address-info-container:visible').find('span.retail-store-name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
                                        }
                                    }
                                }
                                else
                                {
                                    //if offline
                                    if($('div.rta-address-info-container:visible').find(".retail-store-name").html() != 'Out of Delivery Area')
                                    {
                                        uiButton.attr('data-order-status','17');
                                        uiButton.text('Manual Order');

                                        if($('div.rta-address-info-container:visible').length > 0)
                                        {
                                            if($('div.rta-address-info-container:visible').find(".retail-store-name").html() != 'Out of Delivery Area')
                                            {
                                                var sStoreName = $('div.rta-address-info-container:visible').find('span.retail-store-name').text()
                                                sStoreName = sStoreName.replace(' (Offline)', '');
                                                $('div.rta-address-info-container:visible').find('span.retail-store-name').text(sStoreName + ' (Offline)').css('color', 'red');
                                            }
                                        }
                                    }
                                }
                            }

                            else if($('div[modal-id="process-order"]:visible'))
                            {
                                var uiModal = $('div[modal-id="process-order"]');
                                var uiButton = uiModal.find('button.confirm_process');
                                if(bAvailability)
                                {
                                    var sStore = uiModal.find('span.retail-store-name').text();
                                    sStore = sStore.replace(' (Offline)', '');
                                    uiModal.find('span.retail-store-name').text(sStore).css('color', 'black');
                                    uiButton.text('Send Order');
                                    uiModal.attr('order_status', '23')
                                }
                                else
                                {
                                    var sStore = uiModal.find('span.retail-store-name').text();
                                    sStore += " (Offline)";
                                    uiModal.find('span.retail-store-name').text(sStore).css('color', 'red');
                                    uiButton.text('Manual Order');
                                    uiModal.attr('order_status', '17')
                                }
                            }

                            //alert(JSON.stringify(oData))
                        }
                    }
                    //callcenter.store_management.redraw_presence();

                }
            });

            oSock.bind({
                'event'   : 'pusher:member_removed',
                'channel' : 'presence-ironman',
                'callback': function (oData) {
                    var storeIdStorage = cr8v.get_from_storage('active_storage', '');
                    if (typeof(storeIdStorage) !== 'undefined') {
                        if (oData.info.hasOwnProperty('store_user_id') && oData.info.store_id == storeIdStorage[0].current_store_id)//if it is a store
                        {
                            var bAvailability = callcenter.gis.check_store_availability(oData.info.store_id);
                            if ($('section.content div#add_customer_content').is(':visible') == true) //if in add customer find RTA
                            {
                                uiButton = $('section.content div#add_customer_content').find('button.order_process.rta_proceed');

                                if (bAvailability) {
                                    if ($('div.rta-address-info-container div.found-retail:visible').length > 0) {
                                        var sStoreName = $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text()
                                        $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
                                        uiButton.text('Proceed to Order').attr('data-order-rta', 1);
                                    }
                                }
                                else {
                                    if ($('div.rta-address-info-container div.found-retail:visible').length > 0) {
                                        var sStoreName = $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text()
                                        sStoreName = sStoreName.replace(' (Offline)', '');
                                        $('div.rta-address-info-container div.found-retail:visible').find('span.retail-store-name').text(sStoreName + ' (Offline)').css('color', 'red');
                                        uiButton.text('Proceed to Order (Manual Order)').attr('data-order-rta', 4);
                                    }
                                }
                            }
                            else if ($('section.content div#search_customer_content').is(':visible') == true) //if in add customer find RTA
                            {
                                uiButton = $('section.content div#search_customer_content').find('button.proceed_order_from_search');
                                uiAddMealButton = $('section.content div#search_customer_content').find('button.add_last_meal_to_order');

                                if (bAvailability) {
                                    if ($('div.rta-address-info-container:visible').length > 0) {
                                        var sStoreName = $('div.rta-address-info-container:visible').find('span.retail-store-name').text()
                                        $('div.rta-address-info-container:visible').find('span.retail-store-name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
                                        uiAddMealButton.text('Add Last Order to Cart').attr('data-order-rta', 1);
                                        uiButton.text('Proceed to Order').attr('data-order-rta', 1);
                                    }
                                }
                                else {
                                    if ($('div.rta-address-info-container:visible').length > 0) {
                                        var sStoreName = $('div.rta-address-info-container:visible').find('span.retail-store-name').text()
                                        sStoreName = sStoreName.replace(' (Offline)', '');
                                        // //console.log()
                                        $('div.rta-address-info-container:visible').find('span.retail-store-name').text(sStoreName + ' (Offline)').css('color', 'red');
                                        uiAddMealButton.text('Add Last Order to Cart (Manual Order)').attr('data-order-rta', 4);
                                        uiButton.text('Proceed to Order (Manual Order)').attr('data-order-rta', 4);
                                    }
                                }
                            }

                            else if ($('section.content div#order_process_content').is(':visible') == true) {
                                uiButton = $('section.content div#order_process_content').find('button[modal-target="manual-order-summary"]');
                                var orderType = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_type"]').val()
                                //var customerType = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="customer_type"]').val()
                                var customerType = 'new';
                                var total_bill = $('div.cart-container table.cart-table-table').find('td.total_bill').attr('data-total-bill');
                                // var existingCustomerConfigVal = parseInt('2500');
                                var newCustomerConfigVal = parseFloat('2500.00');
                                // orderType = iRetailFound;
                                // $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_type"]').val(iRetailFound);

                                if (bAvailability) {
                                    //if online
                                    if ($('div.rta-container div.rta_store:visible').length > 0) {
                                        var sStoreName = $('div.rta-container div.rta_store:visible').find('strong.store_name').text()
                                        $('div.rta-container div.rta_store:visible').find('strong.store_name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
                                    }

                                    if (customerType === 'existing') {
                                        if (total_bill > existingCustomerConfigVal) {
                                            uiButton = $('section.content div#order_process_content').find('button[modal-target="manual-order-summary"]');
                                            var orderType = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_type"]').val()
                                            //var customerType = $('section.content #order_process_content').find('form#order_customer_information').find('input[name="customer_type"]').val()
                                            var customerType = 'new';
                                            var total_bill = $('div.cart-container table.cart-table-table').find('td.total_bill').attr('data-total-bill');
                                            // var existingCustomerConfigVal = parseInt('2500');
                                            var newCustomerConfigVal = parseInt('2500');
                                            // orderType = iRetailFound;
                                            // $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_type"]').val(iRetailFound);

                                            if (bAvailability) {
                                                //if online
                                                if ($('div.rta-container div.rta_store:visible').length > 0) {
                                                    var sStoreName = $('div.rta-container div.rta_store:visible').find('strong.store_name').text()
                                                    $('div.rta-container div.rta_store:visible').find('strong.store_name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
                                                }

                                                if (customerType === 'existing') {
                                                    if (total_bill > existingCustomerConfigVal) {
                                                        $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                                                        $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
                                                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                                                    }
                                                    else {
                                                        $('section.content #order_process_content').find('button.send_order').text('Send Order');
                                                        $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
                                                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('23');
                                                    }
                                                }
                                                if (customerType === 'new') {
                                                    if (total_bill > newCustomerConfigVal) {
                                                        $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                                                        //     $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
                                                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                                                    }
                                                    else {
                                                        $('section.content #order_process_content').find('button.send_order').text('Send Order');
                                                        $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
                                                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('23');
                                                    }
                                                }
                                                if (customerType === 'new') {
                                                    if (parseFloat(total_bill) > newCustomerConfigVal) {
                                                        $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                                                        $('section.content #order_process_content').find('button.advance_order')/*.text('Verify Advance Order')*/.hide();
                                                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                                                    }
                                                    else {
                                                        $('section.content #order_process_content').find('button.send_order').text('Send Order');
                                                        $('section.content #order_process_content').find('button.advance_order').text('Advance Order').show();
                                                        $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('23');
                                                    }
                                                }
                                                var uiModalOrderComplete = $('div[modal-id="manual-order-complete"]').find('h4.order_type');
                                                uiModalOrderComplete.text('Regular Order Complete');
                                            }
                                            else {
                                                //if offline
                                                if ($('div.rta-container div.rta_store:visible').length > 0) {
                                                    var sStoreName = $('div.rta-container div.rta_store:visible').find('strong.store_name').text()
                                                    sStoreName = sStoreName.replace(' (Offline)', '');
                                                    $('div.rta-container div.rta_store:visible').find('strong.store_name').text(sStoreName + ' (Offline)').css('color', 'red');
                                                }


                                                if (customerType === 'existing') {
                                                    if (total_bill > existingCustomerConfigVal) {
                                                        //if offline
                                                        if ($('div.rta-container div.rta_store:visible').length > 0) {
                                                            var sStoreName = $('div.rta-container div.rta_store:visible').find('strong.store_name').text()
                                                            sStoreName = sStoreName.replace(' (Offline)', '');
                                                            $('div.rta-container div.rta_store:visible').find('strong.store_name').text(sStoreName + ' (Offline)').css('color', 'red');
                                                        }


                                                        if (customerType === 'existing') {
                                                            // if(total_bill > existingCustomerConfigVal)
                                                            // {
                                                            //     $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                                                            //     $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
                                                            //     $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                                                            // }
                                                            // else
                                                            // {
                                                            $('section.content #order_process_content').find('button.send_order').text('Manual Order');
                                                            $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
                                                            $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('23');
                                                            // }
                                                        }

                                                        if (customerType === 'new') {
                                                            if (total_bill > newCustomerConfigVal) {
                                                                $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                                                                $('section.content #order_process_content').find('button.advance_order').text('Verify Advance Order');
                                                                $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                                                            }
                                                            else {
                                                                $('section.content #order_process_content').find('button.send_order').text('Manual Order');
                                                                $('section.content #order_process_content').find('button.advance_order').text('Advance Order');
                                                                $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('23');
                                                            }
                                                        }

                                                        if (customerType === 'new') {
                                                            if (parseFloat(total_bill) > newCustomerConfigVal) {
                                                                $('section.content #order_process_content').find('button.send_order').text('Verify Order');
                                                                $('section.content #order_process_content').find('button.advance_order')/*.text('Verify Advance Order')*/.hide();
                                                                $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('20');
                                                            }
                                                            else {
                                                                $('section.content #order_process_content').find('button.send_order').text('Manual Order');
                                                                $('section.content #order_process_content').find('button.advance_order').text('Advance Order').show();
                                                                $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val('17');
                                                            }
                                                        }
                                                        var uiModalOrderComplete = $('div[modal-id="manual-order-complete"]').find('h4.order_type');
                                                        uiModalOrderComplete.text('Manual Order Complete');
                                                    }
                                                }

                                                else if ($('form#coordinator_update_address').is(':visible') == true) //if in update address find RTA
                                                {
                                                    var uiButton = $('form#coordinator_update_address').find('button.confirm_update_address');

                                                    if (bAvailability) {
                                                        //if online
                                                        if ($('div.rta-address-info-container:visible').find(".retail-store-name").html() != 'Out of Delivery Area') {
                                                            uiButton.attr('data-order-status', '23');
                                                            uiButton.text('Send Order');

                                                            if ($('div.rta-address-info-container:visible').length > 0) {
                                                                var sStoreName = $('div.rta-address-info-container:visible').find('span.retail-store-name').text()
                                                                $('div.rta-address-info-container:visible').find('span.retail-store-name').text(sStoreName.replace('(Offline)', '')).css('color', 'black');
                                                            }
                                                        }
                                                    }
                                                    else {
                                                        //if offline
                                                        if ($('div.rta-address-info-container:visible').find(".retail-store-name").html() != 'Out of Delivery Area') {
                                                            uiButton.attr('data-order-status', '17');
                                                            uiButton.text('Manual Order');

                                                            if ($('div.rta-address-info-container:visible').length > 0) {
                                                                var sStoreName = $('div.rta-address-info-container:visible').find('span.retail-store-name').text()
                                                                sStoreName = sStoreName.replace(' (Offline)', '');
                                                                $('div.rta-address-info-container:visible').find('span.retail-store-name').text(sStoreName + ' (Offline)').css('color', 'red');
                                                            }
                                                        }
                                                    }
                                                }

                                                else if ($('div[modal-id="process-order"]:visible')) {
                                                    var uiModal = $('div[modal-id="process-order"]');
                                                    var uiButton = uiModal.find('button.confirm_process');
                                                    if (bAvailability) {
                                                        var sStore = uiModal.find('span.retail-store-name').text();
                                                        sStore = sStore.replace(' (Offline)', '');
                                                        uiModal.find('span.retail-store-name').text(sStore).css('color', 'black');
                                                        uiButton.text('Send Order');
                                                        uiModal.attr('order_status', '23')
                                                    }
                                                    else {
                                                        var sStore = uiModal.find('span.retail-store-name').text();
                                                        sStore += " (Offline)";
                                                        uiModal.find('span.retail-store-name').text(sStore).css('color', 'red');
                                                        uiButton.text('Manual Order');
                                                        uiModal.attr('order_status', '17')
                                                    }
                                                }
                                            }
                                        }
                                        //callcenter.store_management.redraw_presence();

                                    }
                                }
                            }
                        }
                    }
                }

            });

            oSock.bind({
                'event'   : 'receive_order',
                'channel' : 'private-ironman',
                'callback': function (oOrderData) {
                    // console.log('order_update');
                    var uiOrderContainer = $('section#search_order_list_container').find('div.search_result_container');
                    $.each(oOrderData, function (key, value) {
                        console.log('run receive order');
                        if (value.callcenter_id == iConstantCallcenterId) {
                            //console.log(value);
                            var oOrders = cr8v.get_from_storage('orders', '');
                            if(typeof(oOrders) != 'undefined')
                            {
                                oOrders = oOrders[0];
                                $.each(oOrders, function(storage_order_key, storage_order){
                                    if(storage_order.id == value.id)
                                    {
                                        oOrders.splice(storage_order_key, 1);
                                        return false;
                                    }
                                })

                                oOrders.push(value);
                                cr8v.pop('orders');
                                cr8v.add_to_storage('orders', oOrders)
                                //console.log(oOrders);
                            }
                            console.log(value)
                            uiOrderContainer.find('div[data-order-id="' + value.id + '"]').remove();
                            var uiTemplate = $('div.order_search_result_block.template').clone().removeClass('template');
                            callcenter.coordinator_management.manipulate_template(uiTemplate, value);
                            var iActiveNav = $('header div.sub-nav.coordinator_management').find('li.active');
                            if (value.order_status == iActiveNav.attr('data-order-type-id-park')) {
                                uiOrderContainer.prepend(uiTemplate);
                            }
                            else {
                                var iNotifyCount = $('header div.sub-nav.coordinator_management').find('li[data-order-type-id-park="' + value.order_status + '"]').find('div.notify').text();
                                iNotifyCount++;
                                $('header div.sub-nav.coordinator_management').find('li[data-order-type-id-park="' + value.order_status + '"]').find('div.notify').text(iNotifyCount);
                                $('header div.sub-nav.coordinator_management').find('li[data-order-type-id-park="' + value.order_status + '"]').find('div.notify').removeClass('hidden');
                            }

                        }
                    })
                }
            });

            $('section#search_order_list_container').on('click', 'div.order_search_result_block button.verification_order', function () {
                var sAddressData = $(this).parents('div.order_search_result_block:first').find('div.customer_address:not(.template)').find('p[data-address]').attr('data-address');
                var iCity = $(this).parents('div.order_search_result_block:first').find('div.customer_address:not(.template)').attr('city');
                var iProvince = $(this).parents('div.order_search_result_block:first').find('div.customer_address:not(.template)').attr('province');
                //var iCity = $(this).parents('div.order_search_result_block:first').find('div.customer_address:not(.template)').attr('city');
                if(typeof(sAddressData) !== 'undefined')
                {
                    var sAddressData = sAddressData.split('/##/');
                    $('div[modal-id="process-order"]').find('input[name="house_number"]').val(sAddressData[1]);
                    $('div[modal-id="process-order"]').find('input[name="building"]').val(sAddressData[2]);
                    $('div[modal-id="process-order"]').find('input[name="floor"]').val(sAddressData[3]);
                    $('div[modal-id="process-order"]').find('input[name="street"]').val(sAddressData[4]);
                    $('div[modal-id="process-order"]').find('input[name="second_street"]').val(sAddressData[5]);
                    $('div[modal-id="process-order"]').find('input[name="barangay"]').val(sAddressData[6]);
                    $('div[modal-id="process-order"]').find('input[name="subdivision"]').val(sAddressData[7]);
                    $('div[modal-id="process-order"]').find('input[name="city"]').val(sAddressData[8]);
                    $('div[modal-id="process-order"]').find('input[name="province"]').val(sAddressData[9]);
                    $('div[modal-id="process-order"]').find('input[type="hidden"][name="building"]').val(sAddressData[2]);
                }
                $('div[modal-id="process-order"]').find('input[type="hidden"][name="province"]').val(iProvince);
                $('div[modal-id="process-order"]').find('input[type="hidden"][name="city"]').val(iCity);

                $('div[modal-id="process-order"]').attr('data-order-id', $(this).attr('data-order-id'));
            })
            //for the dashboard count bubbles
            $('header div.coordinator_management ul li.sub-nav-coordinator-order').on('click','div.notify', function(e){
                $(this).next('a').trigger("click");
            }).css({"cursor":"pointer"});



            $('div[modal-id="process-order"]').on('click', 'button.close-me', function () {
                callcenter.coordinator_management.reset_rta_process_order();
            })

            $('div[modal-id="process-order"]').on('click', 'button.confirm_process', function () {
                //alert();
                var uiThis = $(this);
                var iOrderID = $('div[modal-id="process-order"]').attr('data-order-id');
                var iAgentId = $('div[modal-id="process-order"]').attr('data-admin-id');
                var iStoreId = $('div[modal-id="process-order"]').attr('store_id');
                callcenter.coordinator_management.show_spinner($(this), true)
                var oParams = {
                    data      : [
                        {
                            field: "order_status",
                            value: $('div[modal-id="process-order"]').attr('order_status')
                        },
                        {
                            field: "locked_by",
                            value: 0
                        },
                        {
                            field: "store_id",
                            value: ((typeof(iStoreId) !== 'undefined') ? iStoreId : '0') 
                        }
                    ],
                    qualifiers: [{
                        field   : "o.id",
                        value   : iOrderID,
                        operator: "="
                    }],

                    "user_id": iAgentId,
                    "type"   : 'processed_order'
                };

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.orders') + "/orders/update",
                    "success": function (oData) {
                        if (oData.status == true) {
                            $('div[modal-id="process-order"]').removeClass('showed')
                            $('div[modal-id="process-order"]').find('input').val('');
                            $('body').css('overflow', 'auto')
                            $('section#search_order_list_container').find('div.order_search_result_block[data-order-id="' + iOrderID + '"]').remove();
                            callcenter.coordinator_management.reset_rta_process_order();
                            callcenter.coordinator_management.show_spinner(uiThis, false)
                            //callcenter.coordinator_management.update_order_id(iOrderID, 23);
                        }

                        callcenter.coordinator_management.show_spinner(uiThis, false)
                    }
                };

                callcenter.coordinator_management.cajax(oAjaxConfig);
            })


            //var sPrivateChannel = 'private-225';

            /*oSock.bind({
                'event'   : 'update_order',
                'channel' : sPrivateChannel,
                'callback': function (oOrderData) {
                    // //console.log('order_update');
                    var uiOrderContainer = $('section#search_order_list_container').find('div.search_result_container');
                    $.each(oOrderData, function (key, value) {

                        var oOrders = cr8v.get_from_storage('orders', '');
                        if(typeof(oOrders) != 'undefined')
                        {
                            oOrders = oOrders[0];
                            $.each(oOrders, function(storage_order_key, storage_order){
                                if(storage_order.id == value.id)
                                {
                                    oOrders.splice(storage_order_key, 1);
                                    return false;
                                }
                            })
                            oOrders.push(value);
                            cr8v.pop('orders');
                            cr8v.add_to_storage('orders', oOrders)
                            ////console.log(oOrders);
                        }

                        if (value.callcenter_id == iConstantCallcenterId && value.user_id != iConstantUserId) {
                            ////console.log(value);


                            uiOrderContainer.find('div[data-order-id="' + value.id + '"]').remove();
                            var uiTemplate = $('div.order_search_result_block.template').clone().removeClass('template');
                            callcenter.coordinator_management.manipulate_template(uiTemplate, value);
                            var iActiveNav = $('header div.sub-nav.coordinator_management').find('li.active');

                            if (value.order_status == iActiveNav.attr('data-order-type-id')) {
                                uiOrderContainer.prepend(uiTemplate);
                            }
                            else if (iActiveNav.attr('data-order-type-id') == 'all') {
                                uiOrderContainer.prepend(uiTemplate);
                            }
                            else {
                                var iNotifyCount = $('header div.sub-nav.coordinator_management').find('li[data-order-type-id="' + value.order_status + '"]').find('div.notify').text();
                                iNotifyCount++;
                                $('header div.sub-nav.coordinator_management').find('li[data-order-type-id="' + value.order_status + '"]').find('div.notify').text(iNotifyCount);
                                $('header div.sub-nav.coordinator_management').find('li[data-order-type-id="' + value.order_status + '"]').find('div.notify').removeClass('hidden');
                            }

                        }
                    })
                }
            });

            oSock.bind({
                'event'   : 'receive_order',
                'channel' : sPrivateChannel,
                'callback': function (oOrderData) {
                    // //console.log('order_update');
                    var uiOrderContainer = $('section#search_order_list_container').find('div.search_result_container');
                    //console.log(oOrderData)
                    $.each(oOrderData, function (key, value) {
                        if (value.callcenter_id == iConstantCallcenterId) {
                            ////console.log(value);
                            var oOrders = cr8v.get_from_storage('orders', '');
                            if(typeof(oOrders) != 'undefined')
                            {
                                oOrders = oOrders[0];
                                $.each(oOrders, function(storage_order_key, storage_order){
                                    if(storage_order.id == value.id)
                                    {
                                        oOrders.splice(storage_order_key, 1);
                                        return false;
                                    }
                                })
                                oOrders.push(value);
                                cr8v.pop('orders');
                                cr8v.add_to_storage('orders', oOrders)
                                ////console.log(oOrders);
                            }

                            uiOrderContainer.find('div[data-order-id="' + value.id + '"]').remove();
                            var uiTemplate = $('div.order_search_result_block.template').clone().removeClass('template');
                            callcenter.coordinator_management.manipulate_template(uiTemplate, value);
                            var iActiveNav = $('header div.sub-nav.coordinator_management').find('li.active');
                            if (value.order_status == iActiveNav.attr('data-order-type-id')) {
                                uiOrderContainer.prepend(uiTemplate);
                            }
                            else {
                                var iNotifyCount = $('header div.sub-nav.coordinator_management').find('li[data-order-type-id="' + value.order_status + '"]').find('div.notify').text();
                                iNotifyCount++;
                                $('header div.sub-nav.coordinator_management').find('li[data-order-type-id="' + value.order_status + '"]').find('div.notify').text(iNotifyCount);
                                $('header div.sub-nav.coordinator_management').find('li[data-order-type-id="' + value.order_status + '"]').find('div.notify').removeClass('hidden');
                            }

                        }
                    })
                }
            });

            oSock.bind({
                'event'   : 'update_order',
                'channel' : 'private-345',
                'callback': function (oOrderData) {
                    // //console.log('order_update');
                    var uiOrderContainer = $('section#search_order_list_container').find('div.search_result_container');
                    $.each(oOrderData, function (key, value) {

                        var oOrders = cr8v.get_from_storage('orders', '');
                        if(typeof(oOrders) != 'undefined')
                        {
                            oOrders = oOrders[0];
                            $.each(oOrders, function(storage_order_key, storage_order){
                                if(storage_order.id == value.id)
                                {
                                    oOrders.splice(storage_order_key, 1);
                                    return false;
                                }
                            })
                            oOrders.push(value);
                            cr8v.pop('orders');
                            cr8v.add_to_storage('orders', oOrders)
                            ////console.log(oOrders);
                        }

                        if (value.callcenter_id == iConstantCallcenterId && value.user_id != iConstantUserId) {
                            ////console.log(value);



                            uiOrderContainer.find('div[data-order-id="' + value.id + '"]').remove();
                            var uiTemplate = $('div.order_search_result_block.template').clone().removeClass('template');
                            callcenter.coordinator_management.manipulate_template(uiTemplate, value);
                            var iActiveNav = $('header div.sub-nav.coordinator_management').find('li.active');

                            if (value.order_status == iActiveNav.attr('data-order-type-id')) {
                                uiOrderContainer.prepend(uiTemplate);
                            }
                            else if (iActiveNav.attr('data-order-type-id') == 'all') {
                                uiOrderContainer.prepend(uiTemplate);
                            }
                            else {
                                var iNotifyCount = $('header div.sub-nav.coordinator_management').find('li[data-order-type-id="' + value.order_status + '"]').find('div.notify').text();
                                iNotifyCount++;
                                $('header div.sub-nav.coordinator_management').find('li[data-order-type-id="' + value.order_status + '"]').find('div.notify').text(iNotifyCount);
                                $('header div.sub-nav.coordinator_management').find('li[data-order-type-id="' + value.order_status + '"]').find('div.notify').removeClass('hidden');
                            }

                        }
                    })
                }
            });

            oSock.bind({
                'event'   : 'receive_order',
                'channel' : 'private-345',
                'callback': function (oOrderData) {
                    // //console.log('order_update');
                    var uiOrderContainer = $('section#search_order_list_container').find('div.search_result_container');
                    //console.log(oOrderData)
                    $.each(oOrderData, function (key, value) {
                        if (value.callcenter_id == iConstantCallcenterId) {
                            ////console.log(value);

                            var oOrders = cr8v.get_from_storage('orders', '');
                            if(typeof(oOrders) != 'undefined')
                            {
                                oOrders = oOrders[0];
                                $.each(oOrders, function(storage_order_key, storage_order){
                                    if(storage_order.id == value.id)
                                    {
                                        oOrders.splice(storage_order_key, 1);
                                        return false;
                                    }
                                })
                                oOrders.push(value);
                                cr8v.pop('orders');
                                cr8v.add_to_storage('orders', oOrders)
                                ////console.log(oOrders);
                            }

                            uiOrderContainer.find('div[data-order-id="' + value.id + '"]').remove();
                            var uiTemplate = $('div.order_search_result_block.template').clone().removeClass('template');
                            callcenter.coordinator_management.manipulate_template(uiTemplate, value);
                            var iActiveNav = $('header div.sub-nav.coordinator_management').find('li.active');
                            if (value.order_status == iActiveNav.attr('data-order-type-id')) {
                                uiOrderContainer.append(uiTemplate);
                            }
                            else {
                                var iNotifyCount = $('header div.sub-nav.coordinator_management').find('li[data-order-type-id="' + value.order_status + '"]').find('div.notify').text();
                                iNotifyCount++;
                                $('header div.sub-nav.coordinator_management').find('li[data-order-type-id="' + value.order_status + '"]').find('div.notify').text(iNotifyCount);
                                $('header div.sub-nav.coordinator_management').find('li[data-order-type-id="' + value.order_status + '"]').find('div.notify').removeClass('hidden');
                            }

                        }
                    })
                }
            });*/

            if (iConstantUserLevel == 2) {
                $('section#add_new_customer_header').hide();
                callcenter.store_management.content_panel_toggle('store_management');
                callcenter.store_management.sub_content_panel_toggle('online_stores');
                $('div.row.promo').hide();
            }

            $('section#search_order_form_container form#search_order_form_params a.search_order_list_sort').each(function (i) {
                $(this).css({"color": "black"});
                $(this).removeClass("active red-color");
                $(this).find("i.fa").removeClass("fa-angle-up");
                $(this).find("i.fa").removeClass("fa-angle-down");
            });

            /*search order*/
            $("body").on("click.followup-order", "#search_order_list_container .followup-order", function (e) {

                // //console.log("HERE clicked the followup button");
                var iOrderId = $(this).closest("[data-order-id]").attr("data-order-id");
                var iStoreId = $(this).closest("[data-store-id]").attr("data-store-id");
                var oParams = {
                    'order_id': iOrderId,
                    'user_id' : iConstantUserId,
                    'store_id': iStoreId
                }
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.orders') + "/orders/send_followup",
                    "success": function (oData) {
                        if (oData.status == true) {

                        }
                    }
                };

                callcenter.coordinator_management.cajax(oAjaxConfig);
            });

            $('section#search_order_form_container').on('click', 'button.generate_search_order_button', function (e) {
                e.preventDefault();
            });

            $('section#search_order_form_container').on('click', 'button.search_order_button', function (e) {
                e.preventDefault();
                var uiButton = $(this);
                var uiForm = $('section#search_order_form_container').find('form#search_order_form_params'),
                    uiOrderContainer = $('section#search_order_form_container').find('div.search_result_container');

                if (uiForm.find('input[name="search_string"]').val().length > 0) {
                    callcenter.coordinator_management.show_spinner($(this), true);
                    //callcenter.coordinator_management.assembled_fetch_order_params(uiForm, uiButton);

                    var oOrders = cr8v.get_from_storage('orders', '');
                    if(typeof(oOrders) !== 'undefined')
                    {
                        oOrders = oOrders[0];
                        var sSearchEval = " el['id'] != '' ";

                        var sSearchString = uiForm.find('input[name="search_string"]').val();
                        var sSearchBy = uiForm.find('input[name="search_by"]').val();
                        var sSearchByProvinceID = uiForm.find('input[name="province_id"]').attr("value");
                        var sStoreID = uiForm.find('input[name="store_id"]').attr("value");
                        var sCallcenterID = uiForm.find('input[name="callcenter_id"]').attr("value");

                        var sDateFrom = uiForm.find('input[name="date_from"]').val().split("/");
                        if (uiForm.find('input[name="date_from"]').val().length > 0) {
                            sDateFrom = sDateFrom[2] + '-' + sDateFrom[0] + '-' + sDateFrom[1] + ' 00:00:00';
                        }
                        else {
                            sDateFrom = "";
                        }

                        var sDateTo = uiForm.find('input[name="date_to"]').val().split("/");
                        if (uiForm.find('input[name="date_from"]').val().length > 0) {

                            sDateTo = sDateTo[2] + '-' + sDateTo[0] + '-' + sDateTo[1] + ' 23:59:59';
                        }
                        else {
                            sDateTo = "";
                        }

                        var sOrderType = uiForm.find('input[name="order_type"]').val();
                        
                        if (sSearchBy == 'Order ID') {
                            sSearchEval += " && el['id'] == "+sSearchString+" ";
                        }else if (sSearchBy == 'Contact Number' || sSearchBy == 'Customer Name') {
                            sSearchEval += " && (el['display_data'].indexOf('"+sSearchString+"') > -1) ";
                        }

                        if (sOrderType > 0) {
                            sSearchEval += " && el['order_status'] == "+sOrderType+" ";
                        }

                        if (sStoreID !="All Stores") {
                            sSearchEval += " && el['store_id'] == "+sStoreID+" ";
                        }

                        if (sCallcenterID !="All Call Centers") {
                            sSearchEval += " && el['callcenter_id'] == "+sCallcenterID+" ";
                        }

                        if (sSearchByProvinceID != "All Areas") {
                            sSearchEval += " && el['province'] == "+sSearchByProvinceID+" ";
                        }

                        if (sDateFrom.length > 0) {
                            sSearchEval += " && el['province'] <= "+sDateFrom+" ";
                        }

                        if (sDateTo.length > 0) {
                            sSearchEval += " && el['province'] == "+sDateTo+" ";
                        }
                        if(typeof(sSearchEval) !== 'undefined')
                        {
                            oOrders = oOrders.filter(function (el) {
                                return eval(sSearchEval);
                            })
                        }

                        var uiContainer = $('section#search_order_list_container').find('div.search_result_container');
                        uiContainer.html('');

                        //no search result toggle
                        if(count(oOrders) > 0)
                        {
                            $('section#search_order_form_container div.no_search_result').addClass('hidden');
                            $('section#search_order_form_container div.search_result').removeClass('hidden');
                        }
                        else
                        {
                            $('section#search_order_form_container div.search_result').addClass('hidden');
                            $('section#search_order_form_container div.no_search_result strong.no_search_result').text('No Search Result');
                            $('section#search_order_form_container div.no_search_result').removeClass('hidden');
                        }
                        if(typeof(oOrders) !== 'undefined')
                        {
                            $.each(oOrders, function (key, order) {
                                var uiTemplate = $('div.order_search_result_block.template').clone().removeClass('template');
                                callcenter.coordinator_management.manipulate_template(uiTemplate, order);
                                uiContainer.append(uiTemplate);
                            })
                        }


                    }

                    setTimeout(function(){
                        callcenter.coordinator_management.show_spinner(uiButton, false);
                        uiForm.find('input[name="search_string"]').val("");
                        $('section#search_order_list_container div.search_result_container').removeClass("hidden");
                    }, 500)

                }
                ;
            });

            $('li.coordinator_management_head').on('click', function () {
                $(this).addClass('active')
                callcenter.coordinator_management.content_panel_toggle('coordinator_management');
                callcenter.coordinator_management.sub_content_panel_toggle('search_order');
                $('section#customer_list_header').addClass("hidden");

                $('section#search_order_list_container div.search_result_container').addClass("hidden");

                //clear form values
                $('section#search_order_form_container form#search_order_form_params input[name="search_string"]').val("");
                
            })

            $('section#search_order_list_container div.search_result_container').on('click', 'div.block-header', function () {
                $(this).addClass('hidden');
                $(this).parents('div.order_search_result_block:first').find('div.block-content').removeClass('hidden').css({"background-color":"#FCE8A9"});
        
            })

            $('section#search_order_list_container div.search_result_container').on('click', 'header-group.header', function () {
                $(this).parents('div.block-content:first').addClass('hidden');
                $(this).parents('div.order_search_result_block:first').find('div.block-header').removeClass('hidden');
            })

            $('header div.coordinator_management.sub-nav').on('click', 'li', function () {
                callcenter.coordinator_management.sub_content_panel_toggle(($(this).attr('page')));
            })

            $('header div.sub-nav.coordinator_management').on('click', 'li.sub-nav-coordinator-order', function () {
                $(this).parents('ul:first').find('li').removeClass('active');
                $(this).addClass('active');
                $(this).find('div.notify').text(0).addClass('hidden');
                var sActiveLink = $(this).find('a').text();
                var sOrderType = $(this).attr('data-order-type-id');
                var uiForm = $('section#search_order_form_container').find('form#search_order_form_params');
                uiForm.find('input[name="order_type"]').val(sOrderType);
                $('section#search_order_form_container').find('info.coordinator-management-header').text(sActiveLink + ' Order');

                var oOrders = [];
                
                var sFilter = 'el["order_status"] == sOrderType ';

                if(sOrderType == 20)
                {
                    sFilter += '|| el["order_status"] == 22 ';
                }

                if (typeof(sOrderType) != 'undefined') {

                    oOrders = cr8v.get_from_storage('orders', '');

                    if (typeof(oOrders) !== 'undefined') {
                        oOrders = oOrders[0];

                        oOrders = oOrders.filter(function (el) {
                            return eval(sFilter);
                        });
                    }
                    $('section#search_order_list_container div.search_result_container').removeClass("hidden");
                }
                else {
                    oOrders = cr8v.get_from_storage('orders', '');
                    if (typeof(oOrders) !== 'undefined') {
                        oOrders = oOrders[0];
                    }
                    $('section#search_order_list_container div.search_result_container').addClass("hidden");
                }


                var uiContainer = $('section#search_order_list_container').find('div.search_result_container');
                uiContainer.html('');

                if(typeof(oOrders) !== 'undefined')
                {
                    $.each(oOrders, function (key, order) {
                        var uiTemplate = $('div.order_search_result_block.template').clone().removeClass('template');
                        callcenter.coordinator_management.manipulate_template(uiTemplate, order);
                        uiContainer.append(uiTemplate);
                    })
                }

                //callcenter.coordinator_management.assembled_fetch_order_params(uiForm);
                //$('section#search_order_list_container div.search_result_container').addClass("hidden");

                //clear form values
                $('section#search_order_form_container form#search_order_form_params input[name="search_string"]').val("");

                //no search result hide
                $('section#search_order_form_container div.no_search_result').addClass('hidden');
                $('section#search_order_form_container div.search_result').removeClass('hidden');
            });


            $('section#search_order_list_container div.search_result_container').on('click', 'button.order_lock', function () {
                var bLock = $(this).attr('dolock');
                var uiThis = $(this);
                var user_id = 0;
                if (bLock > 0) {
                    user_id = iConstantUserId;
                }
                // //console.log(bLock);
                if (bLock > 0) {
                    uiThis.parents('div.order_search_result_block:first').find('button-group').addClass('hidden');
                    uiThis.parents('div.order_search_result_block:first').find('button-group.locked').removeClass('hidden');
                    uiThis.parents('div.order_search_result_block:first').find('div.locked').removeClass('hidden');
                    uiThis.parents('div.order_search_result_block:first').find('div.locked').removeClass('hidden');
                    uiThis.parents('div.order_search_result_block:first').find('info.agent_locker').text(sConstantUserName)
                }
                else {
                    uiThis.parents('div.order_search_result_block:first').find('button-group').addClass('hidden').find('button-group.unlocked').removeClass('hidden');
                    uiThis.parents('div.order_search_result_block:first').find('button-group.unlocked').removeClass('hidden');
                    uiThis.parents('div.order_search_result_block:first').find('div.locked').addClass('hidden');
                    uiThis.parents('div.order_search_result_block:first').find('button-group.unlocked button.disabled').prop('disabled', true);
                }

                var oParams = {
                    data      : [
                        {
                            field: "locked_by",
                            value: user_id
                        },
                        {
                            field: "has_been_locked",
                            value: 1
                        }
                    ],
                    qualifiers: [{
                        field   : "o.id",
                        value   : $(this).parents('div.order_search_result_block:first').attr('data-order-id'),
                        operator: "="
                    }],

                    "user_id": iConstantUserId,
                    "type"   : 'order_lock_update'
                };

                callcenter.coordinator_management.show_spinner(uiThis, true)

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.orders') + "/orders/update",
                    "success": function (oData) {
                        if (oData.status == true) {
                            setTimeout(function(){
                                callcenter.coordinator_management.show_spinner(uiThis, false)
                            }, 500)

                        }
                    }
                };

                callcenter.coordinator_management.cajax(oAjaxConfig);
            })

            $('section#search_order_list_container div.search_result_container').on('click', 'button[modal-target="manual-relay"]', function () {
                $('div[modal-id="manual-relay"]').find('info.customer_name').text($(this).parents('div.order_search_result_block:first').attr('data-customer-name'))
                $('div[modal-id="manual-relay"]').attr('data-order-id', $(this).parents('div.order_search_result_block:first').attr('data-order-id'))
                $('div[modal-id="manual-relay"]').attr('data-admin-id', $(this).parents('div.order_search_result_block:first').attr('data-admin-id'))
            })

            $('div[modal-id="manual-relay"]').on('click', 'button.confirm-relay', function () {
                var uiModalParent = $(this).parents('div[modal-id="manual-relay"]:first');
                var uiButton = $(this);
                var iOrderId = uiModalParent.attr('data-order-id');
                var iAgentId = uiModalParent.attr('data-admin-id');
                callcenter.coordinator_management.show_spinner($(this), true)
                var oParams = {
                    data      : [
                        {
                            field: "order_status",
                            value: 19
                        },
                        {
                            field: "locked_by",
                            value: 0
                        },
                        {
                            field: "has_been_locked",
                            value: 0
                        }
                    ],
                    qualifiers: [{
                        field   : "o.id",
                        value   : iOrderId,
                        operator: "="
                    }],

                    "user_id": iAgentId,
                    "type"   : 'manually_relay'
                };

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.orders') + "/orders/update",
                    "success": function (oData) {
                        if (oData.status == true) {
                            uiModalParent.removeClass('showed');
                            $("body").css({overflow:'auto'});
                            $('section#search_order_list_container').find('div.order_search_result_block[data-order-id="' + iOrderId + '"]').remove();
                            callcenter.coordinator_management.show_spinner( uiButton, false)
                            //callcenter.coordinator_management.update_order_id(iOrderId, 19);
                        }
                    }
                };

                callcenter.coordinator_management.cajax(oAjaxConfig);
            });

            $('div.sub-nav.coordinator_management').on('click', 'li.sub-nav-coordinator-order', function () {
                if ($(this).hasClass('archive_order')) {
                    //alert('here');
                    $('section#search_order_form_container').find('form-group.archive_order').removeClass('hidden');
                }
                else {
                    $('section#search_order_form_container').find('form-group.archive_order').addClass('hidden');
                    $('section#search_order_form_container').find('input[name="date_from"], input[name="date_to"]').val("");
                }
            })

            $('section#search_order_list_container div.search_result_container').on('click', 'button[modal-target="void-order"]', function () {
                $('div[modal-id="void-order"]').find('info.customer_name').text($(this).parents('div.order_search_result_block:first').attr('data-customer-name'))
                $('div[modal-id="void-order"]').find('info.order_id').text($(this).parents('div.order_search_result_block:first').attr('data-order-id'))
                $('div[modal-id="void-order"]').attr('data-order-id', $(this).parents('div.order_search_result_block:first').attr('data-order-id'))
                $('div[modal-id="void-order"]').attr('data-admin-id', $(this).parents('div.order_search_result_block:first').attr('data-admin-id'))
            })


            $('div[modal-id="void-order"]').on('click', 'button.confirm_void', function () {
                var uiModalParent = $(this).parents('div[modal-id="void-order"]:first');
                var iOrderId = uiModalParent.attr('data-order-id');
                var iAgentId = uiModalParent.attr('data-admin-id');
                var uiThis = $(this);
                callcenter.coordinator_management.show_spinner(uiThis, true)
                var oParams = {
                    data      : [
                        {
                            field: "order_status",
                            value: 21
                        },
                        {
                            field: "locked_by",
                            value: 0
                        }
                    ],
                    qualifiers: [{
                        field   : "o.id",
                        value   : iOrderId,
                        operator: "="
                    }],

                    "user_id": iAgentId,
                    "type"   : 'voided'
                };

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.orders') + "/orders/update",
                    "success": function (oData) {
                        if (oData.status == true) {
                            uiModalParent.removeClass('showed');
                            callcenter.coordinator_management.show_spinner(uiThis, false)
                            $('section#search_order_list_container').find('div.order_search_result_block[data-order-id="' + iOrderId + '"]').remove();
                            $('body').css('overflow', 'auto');
                        }
                    }
                };

                callcenter.coordinator_management.cajax(oAjaxConfig);
            });


            $("section#search_order_form_container form#search_order_form_params div.province_select").on('click', 'div.option', function () {
                $("section#search_order_form_container form#search_order_form_params").find('input[name="province_id"]').val($(this).attr('data-value'));
            })

            $("section#search_order_form_container form#search_order_form_params div.stores_select").on('click', 'div.option', function () {
                $("section#search_order_form_container form#search_order_form_params").find('input[name="store_id"]').val($(this).attr('data-value'));
            })

            $("section#search_order_form_container form#search_order_form_params div.callcenters_select").on('click', 'div.option', function () {
                $("section#search_order_form_container form#search_order_form_params").find('input[name="callcenter_id"]').val($(this).attr('data-value'));
            })

            $('section#search_order_list_container div.search_result_container').on('click', 'button[modal-target="process-order"]', function () {
                $('div[modal-id="process-order"]').find('info.customer_name').text($(this).parents('div.order_search_result_block:first').attr('data-customer-name'))
                $('div[modal-id="process-order"]').find('info.order_id').text($(this).parents('div.order_search_result_block:first').attr('data-order-id'))
                $('div[modal-id="process-order"]').attr('data-order-id', $(this).parents('div.order_search_result_block:first').attr('data-order-id'))
                $('div[modal-id="process-order"]').attr('data-admin-id', $(this).parents('div.order_search_result_block:first').attr('data-admin-id'))
            })

           

            $('section#search_order_list_container div.search_result_container').on('click', 'button[modal-target="reroute-order"]', function () {
                $('div[modal-id="reroute-order"]').find('info.customer_name').text($(this).parents('div.order_search_result_block:first').attr('data-customer-name'))
                $('div[modal-id="reroute-order"]').find('info.order_id').text($(this).parents('div.order_search_result_block:first').attr('data-order-id'))
                $('div[modal-id="reroute-order"]').attr('data-order-id', $(this).parents('div.order_search_result_block:first').attr('data-order-id'))
                $('div[modal-id="reroute-order"]').attr('data-admin-id', $(this).parents('div.order_search_result_block:first').attr('data-admin-id'))
            })

            $('div[modal-id="reroute-order"]').on('click', 'button.confirm_reroute', function () {
                var uiModalParent = $(this).parents('div[modal-id="reroute-order"]:first');
                var iOrderId = uiModalParent.attr('data-order-id');
                var iAgentId = uiModalParent.attr('data-admin-id');
                var uiThis = $(this);
                var oParams = {
                    data      : [
                        {
                            field: "store_id",
                            value: iSelectedStore //reroute where
                        },
                        {
                            field: "order_status",
                            value: 23 //reroute where
                        },
                        {
                            field: "locked_by",
                            value: 0
                        }
                    ],
                    qualifiers: [{
                        field   : "o.id",
                        value   : iOrderId,
                        operator: "="
                    }],

                    "user_id": iAgentId,
                    "type"   : 'reroute'
                };

                callcenter.coordinator_management.show_spinner(uiThis, true)

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.orders') + "/orders/update",
                    "success": function (oData) {
                        if (oData.status == true) {
                            uiModalParent.removeClass('showed')
                            $('section#search_order_list_container').find('div.order_search_result_block[data-order-id="' + iOrderId + '"]').remove();
                            $('body').css('overflow', 'auto');
                            callcenter.coordinator_management.show_spinner(uiThis, false)
                        }
                    }
                };

                callcenter.coordinator_management.cajax(oAjaxConfig);
            });



            $('section#search_order_list_container div.search_result_container').on('click', 'button.edit_order', function () {
                $("body").css({overflow: 'auto'});
                $('section.content #order_process_content').find('tr.cart-item:not(.template)').remove();
                var uiLastMeal = $('section#search_order_list_container div.order_search_result_block').find('div.order_history_container').find('div.order_history:visible');
                var items = uiLastMeal.find('tr.product-breakdown-item:not(.template)');
                var uiContainer = $(this).parents('section#search_order_list_container div.search_result_container:first div.order_search_result_block');
                var iOrderId = uiContainer.attr('data-order-id');
                var iAgentId = uiContainer.attr('data-admin-id');
                var iOrderStatus = uiContainer.attr('data-order-status');
                /*send ajax*/
                var oParams = {
                    data      : [
                        // {
                        //     field: "order_status",
                        //     value: 28 //reroute where
                        // },
                        {
                            field: "is_edited",
                            value: 1
                        },
                        {
                            field: "locked_by",
                            value: iConstantUserId
                        }
                    ],
                    qualifiers: [{
                        field   : "o.id",
                        value   : iOrderId,
                        operator: "="
                    }],

                    "user_id": iAgentId,
                    "type"   : 'edited'
                };

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.orders') + "/orders/update",
                    "success": function (oData) {
                        if (oData.status == true) {

                        }
                    }
                };

                callcenter.coordinator_management.cajax(oAjaxConfig);

                var uiRTA;
                var iStoreCode;
                var iStoreID;
                var iStoreTime;
                var sStoreName;

                callcenter.gis.find_rta(uiContainer, function (oData, rta_data_all, store_data_all) {
                    var orderProcessContainer = $('section.content div#order_process_content');
                    $('div.rta_store:not(.template)').removeClass('hidden');
                    $('div.no_rta_store:not(.template)').hide();
                    $('div.no_rta_store:not(.template)').find('strong.has_spinner').find('i').addClass("hidden")

                    if (oData.message != 0) {
                        if (oData.message == store_data_all[0].store3) {
                            if (oData.message != 1) {
                                var bAvailability = callcenter.gis.check_store_availability(oData.data.store_id);
                                if (bAvailability) {
                                    orderProcessContainer.find("strong.store_name").html(oData.data.name).attr({
                                        'store_code': oData.data.code,
                                        'store_id'  : oData.data.store_id,
                                        'store_name': oData.data.name,
                                        'store_time': rta_data_all[0].rta_data3[2]
                                    }).css('color', 'black');
                                    $('section.content div#order_process_content').find('form#order_customer_information').find('input[name="store_id"]').val(oData.data.store_id);
                                    $('section.content div#order_process_content').find('button.send_order').text('Send Order');
                                }
                                else {
                                    orderProcessContainer.find("strong.store_name").html(oData.data.name + " (Offline)").attr({
                                        'store_code': oData.data.code,
                                        'store_id'  : oData.data.store_id,
                                        'store_name': oData.data.name,
                                        'store_time': rta_data_all[0].rta_data3[2]
                                    }).css('color', 'red');
                                    $('section.content div#order_process_content').find('button.send_order').text('Manual Order');
                                }

                            } else {
                                orderProcessContainer.find("strong.store_name").html('Delivery Not Available This Time');
                            }
                            orderProcessContainer.find("strong.store_code").html(oData.data.code);
                            orderProcessContainer.find("strong.store_time_label").html("Delivery Time:");
                            orderProcessContainer.find("td.store_time").html(rta_data_all[0].rta_data3[2] + ' Minutes');
                        }
                        else if (oData.message == store_data_all[0].store1) {
                            if (oData.message != 1) {
                                var bAvailability = callcenter.gis.check_store_availability(oData.data.store_id);
                                if (bAvailability) {
                                    orderProcessContainer.find("strong.store_name").html(oData.data.name).attr({
                                        'store_code': oData.data.code,
                                        'store_id'  : oData.data.store_id,
                                        'store_name': oData.data.name,
                                        'store_time': rta_data_all[0].rta_data1[2]
                                    }).css('color', 'black');
                                    $('section.content div#order_process_content').find('form#order_customer_information').find('input[name="store_id"]').val(oData.data.store_id);
                                    $('section.content div#order_process_content').find('button.send_order').text('Send Order');
                                }
                                else {
                                    orderProcessContainer.find("strong.store_name").html(oData.data.name + " (Offline)").attr({
                                        'store_code': oData.data.code,
                                        'store_id'  : oData.data.store_id,
                                        'store_name': oData.data.name,
                                        'store_time': rta_data_all[0].rta_data1[2]
                                    }).css('color', 'red');
                                    $('section.content div#order_process_content').find('button.send_order').text('Manual Order');
                                }
                            } else {
                                orderProcessContainer.find("strong.store_name").html('Delivery Not Available This Time');
                            }
                            orderProcessContainer.find("strong.store_code").html(oData.data.code);
                            orderProcessContainer.find("strong.store_time_label").html("Delivery Time:");
                            orderProcessContainer.find("td.store_time").html(rta_data_all[0].rta_data1[2] + ' Minutes');
                        }
                        else if (oData.message == store_data_all[0].store2) {
                            if (oData.message != 1) {
                                var bAvailability = callcenter.gis.check_store_availability(oData.data.store_id);
                                if (bAvailability) {
                                    orderProcessContainer.find("strong.store_name").html(oData.data.name).attr({
                                        'store_code': oData.data.code,
                                        'store_id'  : oData.data.store_id,
                                        'store_name': oData.data.name,
                                        'store_time': rta_data_all[0].rta_data2[2]
                                    }).css('color', 'black');
                                    $('section.content div#order_process_content').find('form#order_customer_information').find('input[name="store_id"]').val(oData.data.store_id);
                                    $('section.content div#order_process_content').find('button.send_order').text('Send Order');
                                }
                                else {
                                    orderProcessContainer.find("strong.store_name").html(oData.data.name + " (Offline)").attr({
                                        'store_code': oData.data.code,
                                        'store_id'  : oData.data.store_id,
                                        'store_name': oData.data.name,
                                        'store_time': rta_data_all[0].rta_data2[2]
                                    }).css('color', 'red');
                                    $('section.content div#order_process_content').find('button.send_order').text('Manual Order');
                                }
                            } else {
                                orderProcessContainer.find("strong.store_name").html('Delivery Not Available This Time');
                            }
                            orderProcessContainer.find("strong.store_code").html(oData.data.code);
                            orderProcessContainer.find("strong.store_time_label").html("Delivery Time:");
                            orderProcessContainer.find("td.store_time").html(rta_data_all[0].rta_data2[2] + ' Minutes');
                        }
                        else if (oData.message == store_data_all[0].store4) {
                            if (oData.message != 1) {
                                var bAvailability = callcenter.gis.check_store_availability(oData.data.store_id);
                                if (bAvailability) {
                                    orderProcessContainer.find("strong.store_name").html(oData.data.name).attr({
                                        'store_code': oData.data.code,
                                        'store_id'  : oData.data.store_id,
                                        'store_name': oData.data.name,
                                        'store_time': rta_data_all[0].rta_data4[2]
                                    }).css('color', 'black');
                                    $('section.content div#order_process_content').find('form#order_customer_information').find('input[name="store_id"]').val(oData.data.store_id);
                                    $('section.content div#order_process_content').find('button.send_order').text('Send Order');
                                }
                                else {
                                    orderProcessContainer.find("strong.store_name").html(oData.data.name + " (Offline)").attr({
                                        'store_code': oData.data.code,
                                        'store_id'  : oData.data.store_id,
                                        'store_name': oData.data.name,
                                        'store_time': rta_data_all[0].rta_data4[2]
                                    }).css('color', 'red');
                                    $('section.content div#order_process_content').find('button.send_order').text('Manual Order');
                                }
                            } else {
                                orderProcessContainer.find("strong.store_name").html('Delivery Not Available This Time');
                            }
                            orderProcessContainer.find("strong.store_code").html(oData.data.code);
                            orderProcessContainer.find("strong.store_time_label").html("Delivery Time:");
                            orderProcessContainer.find("td.store_time").html(rta_data_all[0].rta_data4[2] + ' Minutes');
                        }
                        else if (oData.message == store_data_all[0].store5) {
                            if (oData.message != 1) {
                                var bAvailability = callcenter.gis.check_store_availability(oData.data.store_id);
                                if (bAvailability) {
                                    orderProcessContainer.find("strong.store_name").html(oData.data.name).attr({
                                        'store_code': oData.data.code,
                                        'store_id'  : oData.data.store_id,
                                        'store_name': oData.data.name,
                                        'store_time': rta_data_all[0].rta_data5[2]
                                    }).css('color', 'black');
                                    $('section.content div#order_process_content').find('form#order_customer_information').find('input[name="store_id"]').val(oData.data.store_id);
                                    $('section.content div#order_process_content').find('button.send_order').text('Send Order');
                                }
                                else {
                                    orderProcessContainer.find("strong.store_name").html(oData.data.name + " (Offline)").attr({
                                        'store_code': oData.data.code,
                                        'store_id'  : oData.data.store_id,
                                        'store_name': oData.data.name,
                                        'store_time': rta_data_all[0].rta_data5[2]
                                    }).css('color', 'red');
                                    $('section.content div#order_process_content').find('button.send_order').text('Manual Order');
                                }
                            } else {
                                orderProcessContainer.find("strong.store_name").html('Delivery Not Available This Time');
                            }
                            orderProcessContainer.find("strong.store_code").html(oData.data.code);
                            orderProcessContainer.find("strong.store_time_label").html("Delivery Time:");
                            orderProcessContainer.find("td.store_time").html(rta_data_all[0].rta_data5[2] + ' Minutes');
                        }
                        else {
                            orderProcessContainer.find("strong.store_name").html('RTA Data Conflict');
                            orderProcessContainer.find("td.store_time").html("");
                            orderProcessContainer.find("strong.store_time_label").html("");
                        }
                    }
                    else {
                        orderProcessContainer.find("strong.store_name").html('Out of Delivery Area').removeAttr('store_code store_id store_name store_time');
                        orderProcessContainer.find("td.store_time").html("");
                        orderProcessContainer.find("strong.store_code").html("");
                        orderProcessContainer.find("strong.store_time_label").html("");
                        $('section.content div#order_process_content').find('button.send_order').text('Parked Order');
                    }


                    uiRTA = $('section.content div#order_process_content').find("strong.store_name");
                    iStoreCode = (typeof(uiRTA.attr('store_code')) !== 'undefined') ? uiRTA.attr('store_code') : 0;
                    iStoreID = (typeof(uiRTA.attr('store_id')) !== 'undefined') ? uiRTA.attr('store_id') : 0;
                    iStoreTime = (typeof(uiRTA.attr('store_time')) !== 'undefined') ? uiRTA.attr('store_time') : 0;
                    sStoreName = (typeof(uiRTA.attr('store_name')) !== 'undefined') ? uiRTA.attr('store_name') : '';

                    $('section.content div#order_process_content').find('form#order_customer_information').find('input[name="store_id"]').val(iStoreID);
                    $('section.content div#order_process_content').find('form#order_customer_information').find('input[name="serving_time"]').val(iStoreTime);
                });

                callcenter.order_process.add_to_cart(null, uiLastMeal);

                $('header ul#main_nav_headers').find('li.order_management').trigger('click');
                callcenter.call_orders.content_panel_toggle('order_process');
                $('header ul#main_nav_headers').find('li.order_management').addClass('active');

                $('section.content #order_process_content').find('div.meal-record-container').find('div.no-order-cart').addClass('hidden');
                $('section.content #order_process_content').find('div.meal-record-container').find('div.cart-table').removeClass('hidden');
                $('section.content #order_process_content').find('div.meal-record-container').find('table.cart-table-table').removeClass('hidden').show();

                $('section.content #order_process_content').find('button.show_bill_breakdown').prop('disabled', false);
                $('section.content #order_process_content').find('button.advance_order').prop('disabled', false);
                $('section.content #order_process_content').find('form#order_customer_information').find('input[name="customer_type"]').val('existing');

                var uiAddress = uiContainer.find('div.customer_address_container').find('div.customer_address:not(.template):first');
                // var iStoreID = (uiContainer.attr('data-store-id') !== undefined) ? uiContainer.attr('data-store-id') : 0;

                var iAddressID = (typeof(uiAddress.attr('address_id'))!== 'undefined') ? uiAddress.attr('address_id') : 0 ;
                var sAddressText = uiAddress.find('p.info_address_complete').text();
                //uiAddressTemplate.find('p.info_address_complete').attr('data-address','/##/' + house_number + '/##/' + building + '/##/' + floor + '/##/' + street + '/##/' + barangay + '/##/' + subdivision + '/##/' + city + '/##/' + province);
                var sAddressData = uiAddress.find('p.info_address_complete').attr('data-address');
                var sAddressLandmark = uiAddress.find('p.info_address_landmark').text();
                var sImage = uiAddress.find('img.info_address_type').clone();
                var sAddressLabel = uiAddress.find('strong.info_address_label').text();
                var sAddressType = uiAddress.find('strong.info_address_type').text();
                var sCustomerName = uiContainer.find('info.customer_name:first').text();
                var sCustomerMobile = uiContainer.find('info.customer_contact:first').text();
                var sCustomerMobileAlternate = (uiContainer.find('info.customer_contact:first').attr('customer_mobile_alternate') !== 'undefined') ? uiContainer.find('info.customer_contact:first').attr('customer_mobile_alternate').split(',').join(',</p><p>') : '';
                sCustomerMobileAlternate = '<p>'+sCustomerMobileAlternate+'</p>';
                var iCustomerID = uiContainer.attr('data-customer-id');
                sAddressData = (typeof(sAddressData) !== 'undefined') ? sAddressData.split('/##/') : '';
                var iOrderType = 4;
                var sIsPWD = (typeof(uiContainer.attr('is_pwd')) != 'undefined') ? uiContainer.attr('is_pwd') : 0 ;
                var sIsSenior = (typeof(uiContainer.attr('is_senior')) != 'undefined') ? uiContainer.attr('is_senior') : 0 ;
                var paymentModeId = (typeof(uiContainer.attr('data-payment-mode-id')) != 'undefined') ? uiContainer.attr('data-payment-mode-id') : 0 ;
                var paymentMode = (typeof(uiContainer.attr('data-payment-mode')) != 'undefined') ? uiContainer.attr('data-payment-mode') : '' ;

                if(paymentModeId == 1)
                {
                    $('section.content #order_process_content').find('input[name="change_for"]').val('').parents('div.change_for:first').show();        
                    $('div[modal-id="manual-order-summary"]').find('input[name="change_for"]').val('').parents('div.change_for:first').show().prev('label').show();          
                }
                else
                {
                    $('section.content #order_process_content').find('input[name="change_for"]').val('').parents('div.change_for:first').hide();        
                    $('div[modal-id="manual-order-summary"]').find('input[name="change_for"]').val('').parents('div.change_for:first').hide().prev('label').hide();  
                }

                $('section.content #order_process_content').find('form#order_customer_information').find('input[name="order_status"]').val(iOrderStatus);

                var sHappyPlusData = [];
                if(uiContainer.find('div.happy_plus_card_container div.happy_plus_card:not(.template)').length > 0)
                {
                    var hpcContainer = uiContainer.find('div.happy_plus_card_container div.happy_plus_card:not(.template)');
                    $.each(hpcContainer, function (key, value){
                        sHappyPlusData.push({'id' : $(value).attr('hpc_id'), 'number' : $(value).attr('hpc_number'), 'date' : $(value).attr('hpc_date'), 'remarks' : $(value).attr('hpc_remarks')})
                    });
                }

                var oOrderInfo = {
                    'address_id'               : iAddressID,
                    'address_text'             : sAddressText,
                    'address_landmark'         : sAddressLandmark,
                    'address_label'            : sAddressLabel,
                    'address_type_image'       : sImage,
                    'address_type'             : sAddressType,
                    'customer_full_name'       : sCustomerName,
                    'customer_mobile'          : sCustomerMobile.replace('Contact Num:', ''),
                    'customer_mobile_alternate': sCustomerMobileAlternate,
                    'customer_id'              : iCustomerID,
                    'store_id'                 : iStoreID,
                    'house_number'             : /*sAddressData[1]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[1] : ''),
                    'building'                 : /*sAddressData[2]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[2] : ''),
                    'floor'                    : /*sAddressData[3]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[3] : ''),
                    'street'                   : /*sAddressData[4]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[4] : ''),
                    'second_street'            : /*sAddressData[5]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[5] : ''),
                    'barangay'                 : /*sAddressData[6]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[6] : ''),
                    'subdivision'              : /*sAddressData[7]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[7] : ''),
                    'city'                     : /*sAddressData[8]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[8] : ''),
                    'province'                 : /*sAddressData[9]*/((typeof(sAddressData) !== 'undefined') ? sAddressData[9] : ''),
                    'order_type'               : iOrderType,
                    'order_id'                 : iOrderId,
                    'hpc_data'                 : sHappyPlusData,
                    'is_pwd'                   : sIsPWD,
                    'is_senior'                : sIsSenior,
                    'payment_mode_id'          : paymentModeId,
                    'payment_mode'             : paymentMode
                }

                callcenter.coordinator_management.populate_edit_order_information(oOrderInfo);
                $('section#search_order_list_container').find('div.order_search_result_block[data-order-id="' + iOrderId + '"]').remove();
                $('section.content #order_process_content').find('button.show_last_order').prop('disabled', false);
                callcenter.call_orders.get_order_history($('section.content #order_process_content div.meal-record-container'), iCustomerID);

                callcenter.order_process.identify_order_rta($(this).attr('data-order-rta'));
            })

            $('section#search_order_list_container div.search_result_container').on('click', 'button.editing_order', function () { //cancel edit order
                $("body").css({overflow: 'auto'});
                var uiContainer = $(this).parents('section#search_order_list_container div.search_result_container:first div.order_search_result_block');
                var iOrderId = uiContainer.attr('data-order-id');
                var iAgentId = uiContainer.attr('data-admin-id');
                var iOrderStatus = uiContainer.attr('data-order-status');
                /*send ajax*/
                var oParams = {
                    data      : [
                        {
                            field: "is_edited",
                            value: 0
                        },
                        {
                            field: "locked_by",
                            value: iConstantUserId
                        }
                    ],
                    qualifiers: [{
                        field   : "o.id",
                        value   : iOrderId,
                        operator: "="
                    }],

                    "user_id": iAgentId,
                    "type"   : 'edited'
                };

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.orders') + "/orders/update",
                    "success": function (oData) {
                        if (oData.status == true) {
                            uiContainer.hide();
                        }
                    }
                };

                callcenter.coordinator_management.cajax(oAjaxConfig);
            });

            $('section#search_order_list_container div.search_result_container').on('click', 'button[modal-target="update-address"]', function () {
                var uiAddressBlock = $(this).parents('div.order_search_result_block:first').find('container.customer_address_block').clone();
                uiAddressBlock.find('div.edit_container').removeClass('hidden');
                $('div[modal-id="update-address"]').find('container.customer_address_block').html('');
                $('div[modal-id="update-address"]').find('container.customer_address_block').replaceWith(uiAddressBlock);
                $('div[modal-id="update-address"]').find('info.customer_name').text($(this).parents('div.order_search_result_block:first').attr('data-customer-name'))
                $('div[modal-id="update-address"]').find('info.order_id').text($(this).parents('div.order_search_result_block:first').attr('data-order-id'))
                $('div[modal-id="update-address"]').attr('data-order-id', $(this).parents('div.order_search_result_block:first').attr('data-order-id'))
                $('div[modal-id="update-address"]').attr('data-admin-id', $(this).parents('div.order_search_result_block:first').attr('data-admin-id'))
                $('div[modal-id="update-address"]').attr('data-customer-id', $(this).parents('div.order_search_result_block:first').attr('data-customer-id'))
                $('div[modal-id="update-address"]').find('div.modal-content').hide();
                $('div[modal-id="update-address"]').find('label[for="choose"]').trigger('click');

                var uiAddressAttr = uiAddressBlock.find('div.customer_address_container').find('div.customer_address:not(.template)');
                var uiModalParent = uiAddressAttr.parents('div[modal-id="update-address"]');
                uiModalParent.find('a.edit_address').html('Change<br>Address<br><div class="arrow-down"></div>');
                uiModalParent.find('div#city_select input[name="city"]').val(uiAddressAttr.attr('city'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="house_number"]').val(uiAddressAttr.attr('house_number'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="building"]').val(uiAddressAttr.attr('building'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="floor"]').val(uiAddressAttr.attr('floor'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="street"]').val(uiAddressAttr.attr('street'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="barangay"]').val(uiAddressAttr.attr('barangay'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="subdivision"]').val(uiAddressAttr.attr('subdivision'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="province"]').val(uiAddressAttr.attr('province'));

                $('button.confirm_update_address').prop('disabled', true);
            });

            $('div[modal-id="update-address"]').on('click', 'label[for="enter"]', function (e) {
                $(this).parents('div[modal-id="update-address"]').find('div.modal-content').show();
                $(this).parents('div[modal-id="update-address"]').find('container.customer_address_block').hide();
                $(this).parents('div[modal-id="update-address"]').find('div.update-address-error-msg').hide();
                $(this).parents('div[modal-id="update-address"]').find('div.has-error').removeClass('has-error');
                $(this).parents('div[modal-id="update-address"]').find('input').removeClass('error');

                $(this).parents('div[modal-id="update-address"]').find('input[name="city"]').attr({
                    'datavalid' : 'required',
                    'labelinput': 'City'
                });
                $(this).parents('div[modal-id="update-address"]').find('input[name="province"]').attr({
                    'datavalid' : 'required',
                    'labelinput': 'Province'
                });
                // $(this).parents('div[modal-id="update-address"]').find('input[name="address_label"]').attr({
                //     'datavalid' : 'required',
                //     'labelinput': 'Address Label'
                // });
                // $(this).parents('div[modal-id="update-address"]').find('input[name="address_type"]').attr({
                //     'datavalid' : 'required',
                //     'labelinput': 'Address Type'
                // });

                var uiThis = $(this);
                var uiModalParent = uiThis.parents('div[modal-id="update-address"]');
                uiModalParent.find('a.edit_address').html('Change<br>Address<br><div class="arrow-down"></div>');
                uiModalParent.find('div#city_select input[name="city"]').val(0);
                uiModalParent.find('div.hidden_fields_for_rta input[name="house_number"]').val(0);
                uiModalParent.find('div.hidden_fields_for_rta input[name="building"]').val(0);
                uiModalParent.find('div.hidden_fields_for_rta input[name="floor"]').val(0);
                uiModalParent.find('div.hidden_fields_for_rta input[name="street"]').val(0);
                uiModalParent.find('div.hidden_fields_for_rta input[name="barangay"]').val(0);
                uiModalParent.find('div.hidden_fields_for_rta input[name="subdivision"]').val(0);
                uiModalParent.find('div.hidden_fields_for_rta input[name="province"]').val(0);

                uiModalParent.find('input[name="city"]:visible').val('');
                uiModalParent.find('input[name="house_number"]:visible').val('');
                uiModalParent.find('input[name="building"]:visible').val('');
                uiModalParent.find('input[name="floor"]:visible').val('');
                uiModalParent.find('input[name="street"]:visible').val('');
                uiModalParent.find('input[name="second_street"]:visible').val('');
                uiModalParent.find('input[name="barangay"]:visible').val('');
                uiModalParent.find('input[name="subdivision"]:visible').val('');
                uiModalParent.find('input[name="province"]:visible').val('');

                $('button.confirm_update_address').text('Send Order');
                $('button.confirm_update_address').prop('disabled', true);
            });

            $('div[modal-id="update-address"]').on('click', 'label[for="choose"]', function (e) {
                $(this).parents('div[modal-id="update-address"]').find('container.customer_address_block').show();
                $(this).parents('div[modal-id="update-address"]').find('div.modal-content').hide();

                $(this).parents('div[modal-id="update-address"]').find('input[name="city"]').removeAttr('datavalid labelinput');
                $(this).parents('div[modal-id="update-address"]').find('input[name="province"]').removeAttr('datavalid labelinput');
                // $(this).parents('div[modal-id="update-address"]').find('input[name="address_label"]').removeAttr('datavalid labelinput');
                // $(this).parents('div[modal-id="update-address"]').find('input[name="address_type"]').removeAttr('datavalid labelinput');

                var uiAddressAttr = $(this).parents('form#coordinator_update_address').find('container.customer_address_block div.customer_address_container div.customer_address:not(.template)');
                var uiModalParent = uiAddressAttr.parents('div[modal-id="update-address"]');
                uiModalParent.find('a.edit_address').html('Change<br>Address<br><div class="arrow-down"></div>');
                uiModalParent.find('div#city_select input[name="city"]').val(uiAddressAttr.attr('city'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="house_number"]').val(uiAddressAttr.attr('house_number'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="building"]').val(uiAddressAttr.attr('building'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="floor"]').val(uiAddressAttr.attr('floor'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="street"]').val(uiAddressAttr.attr('street'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="second_street"]').val(uiAddressAttr.attr('second_street'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="barangay"]').val(uiAddressAttr.attr('barangay'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="subdivision"]').val(uiAddressAttr.attr('subdivision'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="province"]').val(uiAddressAttr.attr('province'));

                $('button.confirm_update_address').text('Send Order');
                $('button.confirm_update_address').prop('disabled', true);
            });

            $('form#coordinator_update_address').on('submit', function (e) {
                e.preventDefault();
            });

            $('button.confirm_update_address').on('click', function (e) {
                callcenter.call_orders.show_spinner($('button.confirm_update_address'), true);
                $('form#coordinator_update_address').submit();
            });

            $('button.cancel_update_address').on('click', function (e) {
                $(this).parents('form#coordinator_update_address').find('div.update-address-error-msg').hide();
                $(this).parents('form#coordinator_update_address').find('input').removeClass('error');
                $(this).parents('form#coordinator_update_address').find('div.select').removeClass('has-error');
                $(this).parents('form#coordinator_update_address').find('div.found-retail').hide();
                $(this).parents('form#coordinator_update_address').find('div.find-rta-container').show();
            })

            // $('form#coordinator_update_address').on('keypress', 'input', function (e) {
            //     if (e.keyCode != 13) {
            //         $('div[modal-id="update-address"]').find('label[for="enter"]').trigger('click');
            //     }
            // });

            $('form#coordinator_update_address div#coordinator_modal_select_province').on('click', 'div.option', function (e) {
                var oProvinceId = $(this).attr('data-value');
                $(this).parents('form#coordinator_update_address').find('div.hidden_fields_for_rta input[name="province"][type="hidden"]').val(oProvinceId);
                $("div[modal-id='update-address'] form#coordinator_update_address").find('div.found-retail').hide();
                $("div[modal-id='update-address'] form#coordinator_update_address").find('div.find-rta-container').show()
            });

            $('form#coordinator_update_address div#coordinator_modal_select_city').on('click', 'div.option', function (e) {
                var oCityId = $(this).attr('data-value');
                $(this).parents('form#coordinator_update_address').find('div#city_select input[name="city"][type="hidden"]').val(oCityId);
                $("div[modal-id='update-address'] form#coordinator_update_address").find('div.found-retail').hide();
                $("div[modal-id='update-address'] form#coordinator_update_address").find('div.find-rta-container').show()
            });

            $('form#coordinator_update_address').on('change', 'input', function (e) {
                $(this).parents('form#coordinator_update_address').find('div.found-retail').hide();
                $(this).parents('form#coordinator_update_address').find('div.find-rta-container').show();
                $('button.confirm_update_address').prop('disabled', true);
            });

            $('form#coordinator_update_address').on('change', 'input[name="building"]', function (e) {
                $(this).parents('form#coordinator_update_address').find('input[name="building"]').attr('value', '');
                $(this).parents('form#coordinator_update_address').find('div.hidden_fields_for_rta input[name="building"][type="hidden"]').attr('value', '');
            });

            $('form#coordinator_update_address').on('change', 'input[name="street"]', function (e) {
                $(this).parents('form#coordinator_update_address').find('input[name="street"]').attr('value', '');
                $(this).parents('form#coordinator_update_address').find('div.hidden_fields_for_rta input[name="street"][type="hidden"]').attr('value', '');
            });

            $('form#coordinator_update_address').on('change', 'input[name="second_street"]', function (e) {
                $(this).parents('form#coordinator_update_address').find('input[name="second_street"]').attr('value', '');
                $(this).parents('form#coordinator_update_address').find('div.hidden_fields_for_rta input[name="second_street"][type="hidden"]').attr('value', '');
            });

            $('form#coordinator_update_address').on('change', 'input[name="subdivision"]', function (e) {
                $(this).parents('form#coordinator_update_address').find('input[name="subdivision"]').attr('value', '');
                $(this).parents('form#coordinator_update_address').find('div.hidden_fields_for_rta input[name="subdivision"][type="hidden"]').attr('value', '');
            });

            $('form#coordinator_update_address').on('change', 'input[name="barangay"]', function (e) {
                $(this).parents('form#coordinator_update_address').find('input[name="barangay"]').attr('value', '');
                $(this).parents('form#coordinator_update_address').find('div.hidden_fields_for_rta input[name="barangay"][type="hidden"]').attr('value', '');
            });

            $('form#coordinator_update_address').on('change', 'input[name="city"]', function (e) {
                $(this).parents('form#coordinator_update_address').find('input[name="city"]').attr('value', '');
                $(this).parents('form#coordinator_update_address').find('div.hidden_fields_for_rta input[name="city"][type="hidden"]').attr('value', '');
            });

            $('form#coordinator_update_address').on('change', 'input[name="province"]', function (e) {
                $(this).parents('form#coordinator_update_address').find('input[name="province"]').attr('value', '');
                $(this).parents('form#coordinator_update_address').find('div.hidden_fields_for_rta input[name="province"][type="hidden"]').attr('value', '');
            });

            var oUpdateAddressModalValidationConfig = {
                'data_validation'    : 'datavalid',
                'input_label'        : 'labelinput',
                'min_length'         : 0,
                'max_length'         : 20,
                'class'              : 'error',
                'add_remove_class'   : true,
                'onValidationError'  : function (arrMessages) {
                    var sError = '';
                    var uiContainer = $('div.update-address-error-msg');
                    if(typeof(arrMessages) !== 'undefined')
                    {
                        $.each(arrMessages, function (k, v) {
                            sError += '<p class="error_messages"><i class="fa fa-exclamation-triangle"></i> ' + v.error_message + '</p>';
                        });
                    }

                    callcenter.call_orders.add_error_message(sError, uiContainer);
                    callcenter.call_orders.show_spinner($('button.confirm_update_address'), false);
                },
                'onValidationSuccess': function () {
                    callcenter.call_orders.show_spinner($('button.confirm_update_address'), true);
                    if ($('div[modal-id="update-address"]').find('div.modal-content').is(':visible')) {
                        callcenter.coordinator_management.assemble_address_information();
                    }
                    else {
                        callcenter.coordinator_management.update_address_deliver_to_different($('button.confirm_update_address'), $('div[modal-id="update-address"]').find('div.customer_address:visible').attr('all_address'));
                    }
                }
            }
            callcenter.validate_form($('form#coordinator_update_address'), oUpdateAddressModalValidationConfig);

            // $('div[modal-id="update-address"]').on('click', 'button.confirm_update_address', function (e) {
            //     e.preventDefault();
            //     ////console.log("orayt")
            //     var uiModalParent = $(this).parents('div[modal-id="update-address"]:first');
            //     if (uiModalParent.find('span.retail-store-name').attr('store_id') > 0) //if rta is found
            //     {
            //         var iOrderId = uiModalParent.attr('data-order-id');
            //         var iAgentId = uiModalParent.attr('data-admin-id');
            //         var iStoreId = uiModalParent.find('span[store_id]').attr('store_id');
            //         var iAddressId = uiModalParent.find('div.customer_address:visible').attr('address_id');
            //         var oParams = {
            //             data      : [
            //                 {
            //                     field: "deliver_to_customer_address",
            //                     value: iAddressId //reroute where
            //                 },
            //                 {
            //                     field: "order_status",
            //                     value: 17 //reroute where
            //                 },
            //                 {
            //                     field: "store_id",
            //                     value: iStoreId //reroute where
            //                 },
            //                 {
            //                     field: "locked_by",
            //                     value: 0
            //                 }
            //             ],
            //             qualifiers: [{
            //                 field   : "o.id",
            //                 value   : iOrderId,
            //                 operator: "="
            //             }],

            //             "user_id": iAgentId,
            //         };

            //         var oAjaxConfig = {
            //             "type"   : "POST",
            //             "data"   : oParams,
            //             "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
            //             "url"    : callcenter.config('url.api.jfc.orders') + "/orders/update",
            //             "success": function (oData) {
            //                 if (oData.status == true) {
            //                     uiModalParent.removeClass('showed')
            //                     $('section#search_order_list_container').find('div.order_search_result_block[data-order-id="' + iOrderId + '"]').remove();
            //                 }
            //             }
            //         };

            //         callcenter.coordinator_management.cajax(oAjaxConfig);
            //     }
            // });


            //callcenter.coordinator_management.fetch_route_stores(1);


            $('div[modal-id="reroute-order"] table.table_store_container').on('click', 'tr.store_route', function () {

                iSelectedStore = $(this).attr('data-store-id');
                var selected = $(this).parents('table.table_store_container:first').find('td.selected');
                selected.removeClass('selected');
                selected.find('i.fa-check').remove();
                selected.find('div.clear').remove();
                $(this).find('td').find('i.fa-check').remove();
                $(this).find('td').find('div.clear').remove();
                $(this).find('td').addClass('selected').append('<i class="fa fa-check font-18 margin-right-10 margin-top-5 f-right green-color"></i> <div class="clear"></div>')
                $(this).find('p').addClass('f-left')
                $('div[modal-id="reroute-order"]').find('button.confirm_reroute').prop('disabled', false);
            })

            $('div[modal-id="update-address"]').on('click', 'a.edit_address', function (e) {
                e.preventDefault();
                $(this).toggleHtml('Cancel<div class="arrow-down"></div>', 'Change<br>Address<br><div class="arrow-down"></div>');
                $(this).parents('div.customer_address_container:first').next('div.customer_secondary_address').toggleClass('hidden');
            })


            $('div[modal-id="update-address"]').on('click', 'div.customer_secondary_address div.customer_address', function (e) {
                var uiThis = $(this);
                var uiParent = uiThis.parents('div.customer_secondary_address:first');
                $('div[modal-id="update-address"]').find('label[for="choose"]').trigger('click');
                $('div[modal-id="update-address"]').find('div.found-retail').hide();
                $('div[modal-id="update-address"]').find('div.find-rta-container').show();

                var oOldPrimaryAddress = uiThis.parents('div.customer_secondary_address').prev('div.customer_address_container').find('div.customer_address:visible').clone();
                var changeaddress = oOldPrimaryAddress.find('div.edit_container').clone();
                uiThis.parents('div.customer_secondary_address').prev('div.customer_address_container').find('div.customer_address:visible').replaceWith(uiThis.removeClass('white').addClass('bggray-light').append(changeaddress));
                uiParent.prepend(oOldPrimaryAddress.removeClass('bggray-light').addClass('white')).find('div.edit_container').remove();
                uiParent.toggleClass('hidden');

                var uiModalParent = uiThis.parents('div[modal-id="update-address"]');
                uiModalParent.find('a.edit_address').html('Change<br>Address<br><div class="arrow-down"></div>');
                uiModalParent.find('div#city_select input[name="city"]').val(uiThis.attr('city'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="house_number"]').val(uiThis.attr('house_number'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="building"]').val(uiThis.attr('building'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="floor"]').val(uiThis.attr('floor'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="street"]').val(uiThis.attr('street'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="barangay"]').val(uiThis.attr('barangay'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="subdivision"]').val(uiThis.attr('subdivision'));
                uiModalParent.find('div.hidden_fields_for_rta input[name="province"]').val(uiThis.attr('province'));

            })

            $('div[modal-id="update-address"]').on('click', 'a.find_retail_search', function (e) {
                callcenter.gis.show_spinner($('a.find_retail_search'), true);
                var uiContainer = $(this).parents('div[modal-id="update-address"]:first');
                var bOrder = callcenter.gis.find_rta(uiContainer);


            })

            $('div[modal-id="update-address"]').on('click', 'div.rta-address-info-container a.show_map', function () {
                // var uiContainer = $('div.rta-address-info-container');
                var uiContainerClass = 'rta-address-info-container';
                var mode = 1;
                callcenter.gis.show_map(mode, uiContainerClass);
            });

            $('section#search_order_form_container form#search_order_form_params input[name="search_string"]').on('keypress', function (e){
                if(e.keyCode == 13)
                {
                    e.preventDefault();
                }
/*
                if($('input[name="search_by"]:visible').val() == 'Contact Number')
                {
                    $(this).removeClass('input_alpha_num_space').addClass('input_num_space_dash').removeClass('input_numeric');  
                }
                else if($('input[name="search_by"]:visible').val() == 'Customer Name')
                {
                    $(this).removeClass('input_num_space_dash').removeClass('input_numeric');
                }
                else if($('input[name="search_by"]:visible').val() == 'Order ID')
                {
                    $(this).removeClass('input_num_space_dash').addClass('input_numeric');  
                }*/

            });

            $("section#search_order_form_container form#search_order_form_params").on('click', 'button[modal-target="messenger"]', function (e) {
                var uiMessengerButtons = $('button[modal-target="messenger"]:not(".global")');
                if (typeof(uiMessengerButtons) != 'undefined') {
                    $.each(uiMessengerButtons, function () {
                        $(this).find('div.notify').text(0).addClass('hidden');
                    });
                }
                e.preventDefault();
                setTimeout(function () {
                    $('div[modal-id="messenger"]').find('div.chat_box_container').scrollTop(99999999999);
                }, 1000)
            })


            $('div[modal-id="messenger"]').on('click', 'button.send_message', function () {
                var uiTextArea = $('div[modal-id="messenger"]').find('textarea.message_content');
                if (uiTextArea.val().length > 0) {
                    var oMessage = {
                        message_from : iConstantUserId,
                        message_to   : iSelectedStore,
                        message      : uiTextArea.val(),
                        date_sent    : 'Today ' + moment().format('h:mm:ss a'),
                        username     : sConstantUserName,
                        callcenter_id: iConstantCallcenterId

                    };

                    uiTextArea.val('');
                    callcenter.coordinator_management.submit_message(oMessage);
                }


            })

            $('div[modal-id="reroute-order"]').on('click', 'label.radio-lbl', function(){
                    if($(this).attr('for') == 'reroute1')
                    {
                         if($(this).prev('input').is(':checked'))
                         {
                                $('div[modal-id="reroute-order"]').find('section.route-schedule').hide();
                         }
                         else
                         {
                                $('div[modal-id="reroute-order"]').find('section.route-schedule').show();  
                         } 
                    }
                    else if($(this).attr('for') == 'reroute2')
                    {
                          if($(this).prev('input').is(':checked'))
                         {
                              $('div[modal-id="reroute-order"]').find('section.route-schedule').show();
                                 
                         }
                         else
                         {
                               $('div[modal-id="reroute-order"]').find('section.route-schedule').hide();
                         }   
                         
                    }    
            })

            callcenter.coordinator_management.fetch_messenger_messages(iConstantUserId, iSelectedStore);

            $('div[modal-id="messenger"]').on('click', 'textarea.message_content', function (e) {
                var uiLastMessage = $('div[modal-id="messenger"]').find('div.chat_box_container').find('div.chat_message:last');
                var iMessageId = uiLastMessage.attr('data-message-id'),
                    iAuthorId = uiLastMessage.attr('data-author-id'),
                    oParams = {
                        "message_id": iMessageId,
                        "author_id" : iAuthorId,
                        "params"    : {
                            "data": [
                                {
                                    "field": "mrm.is_seen",
                                    "value": "1"


                                },
                                {
                                    "field": "mrm.date_seen",
                                    "value": moment().format('YYYY-MM-DD H:m:s')
                                }
                            ],

                            "qualifiers": [
                                {
                                    "field"   : "mrm.message_id",
                                    "value"   : iMessageId,
                                    "operator": "="
                                }
                            ],

                            "store_id": iSelectedStore
                        }

                    };

                ////console.log(uiLastMessage.attr('data-is-seen') == '0');
                if (uiLastMessage.attr('data-is-seen') == '0' && uiLastMessage.attr('data-author-id') != iConstantUserId) {
                    var oAjaxConfig = {
                        'type'   : "POST",
                        'headers': {"X-API-KEY": "IEYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                        'url'    : callcenter.config('url.api.jfc.messenger') + "/messenger/update",
                        'data'   : oParams,
                        'success': function (sData) {
                            uiLastMessage.attr('data-is-seen', 1);
                        }
                    }

                    callcenter.coordinator_management.cajax(oAjaxConfig);
                }
            });

            $('div[modal-id="messenger"] table.table_store_messenger_container').on('click', 'tr', function () {
                var uiParentTable = $(this).parents('table:first');
                uiParentTable.find('td.store-selected').removeClass('store-selected');
                $(this).find('td').addClass('store-selected').find('div.store-msg-notify').addClass('hidden');
                iSelectedStore = $(this).attr('data-store-id');
                ////console.log(iConstantUserId)
                ////console.log(iSelectedStore)
                callcenter.coordinator_management.fetch_messenger_messages(iConstantUserId, iSelectedStore);

            });

            callcenter.coordinator_management.fetch_callcenters();

            $('div[modal-id="reroute-order"]').on('keyup', 'input[name="search_store"]', function () {

                var uiVal = $(this).val();
                if (uiVal.length > 1) {
                    $('div[modal-id="reroute-order"]').find('div.result-list.route-store').removeClass('hidden');
                    var sSearchResults = $('div[modal-id="reroute-order"]').find('div.result-list.route-store').children('div.list');
                    $('div[modal-id="reroute-order"]').find('div.result-list.route-store').children('div.list').addClass('hidden')
                    $.each(sSearchResults, function (key, value) {
                        if (($(value)).text().toLowerCase().indexOf(uiVal) > -1) {
                            $(value).removeClass('hidden');
                        }
                    });
                }
                else {
                    $('div[modal-id="reroute-order"]').find('div.result-list.route-store').addClass('hidden');
                }

            })

            $('div[modal-id="reroute-order"] div.result-list.route-store').on('click', 'div.list', function () {
                $('div[modal-id="reroute-order"]').find('div.result-list.route-store').addClass('hidden');
                var store_id = $(this).attr('data-store-id');
                $('div[modal-id="reroute-order"]').find('table.table_store_container').find('tr[data-store-id="' + store_id + '"]').trigger('click');


            })

            $('section#search_order_list_container').on('click', 'div.order_search_result_block button.order_logs:visible', function () {
                var iOrderID = $(this).parents('div.order_search_result_block:first').attr('data-order-id');
                var uiThis = $(this);
                var oParams = {
                    "params": {
                        "where": [
                            {
                                'field'   : 'opl.order_id',
                                'operator': '=',
                                'value'   : iOrderID,
                            }
                        ],
                        "limit" : 9999999999999
                    }
                }
                //callcenter.coordinator_management.show_spinner(uiThis, true);
                var oAjaxConfig = {
                    'type'   : "GET",
                    'headers': {"X-API-KEY": "IEYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    'url'    : callcenter.config('url.api.jfc.orders') + "/orders/logs",
                    'data'   : oParams,
                    'success': function (oData) {
                        //console.log(oData);
                        var sAgentLogsHtml = '';
                        var sStoreLogsHtml = '';
                        var uiAgentLogsContainer = $('div[modal-id="order-logs"]').find('tbody.agent_logs_container');
                        //var uiStoreLogsContainer = $('div[modal-id="order-logs"]').find('tbody.store_logs_container');
                        if (typeof(oData) !== 'undefined') {
                            $.each(oData.data, function (key, log) {
                                if (log.type == 1 || log.type == 2) {
                                    sAgentLogsHtml += '<tr><td>' + log.action + '</td><td class="text-center">' + moment(log.date_processed).format('MM-DD-YYYY | H:mm ') + '</td><td class="text-center">' + log.full_name + '</td>';
                                }
                                else if (log.type == 3) {
                                    sAgentLogsHtml += '<tr><td>' + log.action + '</td><td class="text-center">' + moment(log.date_processed).format('MM-DD-YYYY | H:mm ') + '</td><td class="text-center">' + log.username.toUpperCase() + '</td></tr>'
                                }
                            })

                            uiAgentLogsContainer.html("").append(sAgentLogsHtml);
                            //uiStoreLogsContainer.html("").append(sStoreLogsHtml);
                        }
                        //callcenter.coordinator_management.show_spinner(uiThis, false);
                    }
                }

                callcenter.coordinator_management.cajax(oAjaxConfig);


            })

            var oOrderParams = {
                "params": {
                    "where": [
                        {
                            'field'   : 'cu.callcenter_id',
                            'operator': '=',
                            'value'   : iConstantCallcenterId
                        }
                    ],
                    "limit": 999999,
                    "order_by" : 'o.id',
                    "sorting" : 'DESC'
                }

            };

            callcenter.coordinator_management.orders_get(oOrderParams);
        },

        'reset_rta_process_order': function () {
            $('div[modal-id="process-order"] div.find-rta-container').show();
            $('div[modal-id="process-order"] div.found-retail').hide();
        },

        'populate_edit_order_information': function (oOrderInfo) {

            if (typeof(oOrderInfo) !== 'undefined') {
                // //console.log(oOrderInfo)
                var uiContent = $('section.content #order_process_content');

                var uiCustomerInformation = $('div.order_customer_information.template').clone().removeClass('template');
                uiCustomerInformation.find('td.info-customer-full-name').text(oOrderInfo.customer_full_name);
                uiCustomerInformation.find('td.info-customer-mobile').text(oOrderInfo.customer_mobile);
                if(oOrderInfo.customer_mobile_alternate !== '' || oOrderInfo.customer_mobile_alternate !== 'undefined')
                {
                    uiCustomerInformation.find('td.info-customer-mobile-alternate').html(oOrderInfo.customer_mobile_alternate);
                } 
                else
                {
                    uiCustomerInformation.find('td.info-customer-mobile-alternate').html('');
                }

                var uiAddress = uiContent.find('div.order_primary_address.template').clone().removeClass('template');
                var addressTypeImg = $(oOrderInfo.address_type_image).clone()
                uiAddress.find('img.address_type_image').replaceWith(addressTypeImg);
                uiAddress.find('p.address_text').text(oOrderInfo.address_text);
                uiAddress.find('p.address_landmark').text(oOrderInfo.address_landmark);
                uiAddress.find('strong.address_type').text(oOrderInfo.address_type);
                uiAddress.find('strong.address_label').text(oOrderInfo.address_label);
                uiAddress.attr('address_id', oOrderInfo.address_id);

                if (typeof(oOrderInfo.store_id) !== 'undefined' && oOrderInfo.store_id != 0) // if there is rta store found
                {
                    var bAvailability = callcenter.gis.check_store_availability(oOrderInfo.store_id);
                    var uiRTA = $('div.rta_store.template').clone().removeClass('template');
                    // uiRTA.find('strong.store_name').text(oOrderInfo.store_name)
                    // uiRTA.find('strong.store_name').attr('store_id', oOrderInfo.store_id)
                    // uiRTA.find('strong.store_code').text(oOrderInfo.store_code)
                    // uiRTA.find('td.store_time').text(oOrderInfo.store_time + ' Minutes');

                    (typeof(oOrderInfo.store_time) !== 'undefined') ? uiRTA.find('td.store_time').text(oOrderInfo.store_time + ' Minutes') : uiRTA.find('td.store_time').text('0');
                    (typeof(oOrderInfo.store_code) !== 'undefined') ? uiRTA.find('strong.store_code').text(oOrderInfo.store_code) : uiRTA.find('strong.store_code').text('0');

                    if (typeof(oOrderInfo.store_name) !== 'undefined') {
                        if (bAvailability) {
                            uiRTA.find('strong.store_name').text(oOrderInfo.store_name).attr('store_id', oOrderInfo.store_id)
                        }
                        else {
                            uiRTA.find('strong.store_name').text(oOrderInfo.store_name + ' (Offline)').css('color', 'red').attr('store_id', oOrderInfo.store_id);
                        }
                    }
                    else {
                        uiRTA.find('strong.store_name').text('').attr('store_id', oOrderInfo.store_id);
                    }

                }
                else {
                    var uiRTA = $('div.no_rta_store.template').clone().removeClass('template');
                    var forEditOrderCallback = $('div.rta_store.template').clone().removeClass('template').addClass('hidden');
                    uiRTA.find('strong.has_spinner').find('i').removeClass("hidden")
                }

                var uiBillBreakdown = $('div.billbreakdown.template').clone().removeClass('template');
                uiBillBreakdown.find('strong.info-customer-full-name').text(oOrderInfo.customer_full_name);
                uiBillBreakdown.find('strong.info-customer-mobile').text(oOrderInfo.customer_mobile);
                if(oOrderInfo.customer_mobile_alternate !== '' || oOrderInfo.customer_mobile_alternate !== 'undefined')
                {
                    uiBillBreakdown.find('strong.info-customer-mobile-alternate').html(oOrderInfo.customer_mobile_alternate);
                    uiBillBreakdown.find('strong.info-customer-mobile-alternate').find('p').addClass('font-12');
                } 
                else
                {
                    uiBillBreakdown.find('strong.info-customer-mobile-alternate').html('');
                }
                var addressTypeImg = $(oOrderInfo.address_type_image).clone()
                uiBillBreakdown.find('img.address_type_image').replaceWith(addressTypeImg);
                uiBillBreakdown.find('strong.address_label').text(oOrderInfo.address_label);
                uiBillBreakdown.find('p.address_text').text(oOrderInfo.address_text);
                uiBillBreakdown.find('p.address_landmark').text(oOrderInfo.address_landmark);
                uiBillBreakdown.find('strong.address_type').text(oOrderInfo.address_type);

                $('div[modal-id="manual-order-complete"]').find('strong.customer-full-name').text(oOrderInfo.customer_full_name);

                //fetch store announcements

                var uiHiddenForm = uiContent.find('form#order_customer_information');
                uiHiddenForm.find('input[name="address_id"]').val(oOrderInfo.address_id);
                uiHiddenForm.find('input[name="customer_id"]').val(oOrderInfo.customer_id);
                //uiHiddenForm.find('input[name="store_id"]').val(oOrderInfo.store_id);

                uiHiddenForm.find('input[name="house_number"]').val(oOrderInfo.house_number);
                uiHiddenForm.find('input[name="building"]').val(oOrderInfo.building);
                uiHiddenForm.find('input[name="floor"]').val(oOrderInfo.floor);
                uiHiddenForm.find('input[name="street"]').val(oOrderInfo.street);
                uiHiddenForm.find('input[name="second_street"]').val(oOrderInfo.second_street);
                uiHiddenForm.find('input[name="order_type"]').val(oOrderInfo.order_type);
                uiHiddenForm.find('input[name="barangay"]').val(oOrderInfo.barangay);
                uiHiddenForm.find('input[name="subdivision"]').val(oOrderInfo.subdivision);
                uiHiddenForm.find('input[name="city"]').val(oOrderInfo.city);
                uiHiddenForm.find('input[name="province"]').val(oOrderInfo.province);

                uiHiddenForm.find('input[name="order_id"]').val(oOrderInfo.order_id);
                uiHiddenForm.find('input[name="hpc_data"]').val(JSON.stringify(oOrderInfo.hpc_data));
                uiHiddenForm.find('input[name="is_pwd"]').val(oOrderInfo.is_pwd);
                uiHiddenForm.find('input[name="is_senior"]').val(oOrderInfo.is_senior);
                uiHiddenForm.find('input[name="payment_type"]').val(oOrderInfo.payment_mode_id);
                // uiHiddenForm.find('input[name="serving_time"]').val(oOrderInfo.store_time);

                //previous payment mode
                uiContent.find('input[name="payment_type[]"]').attr('value', oOrderInfo.payment_mode);
                uiContent.find('input[name="payment_type[]"]').attr('data-val', oOrderInfo.payment_mode_id);

                uiContent.find('div.rta-container').html('');
                $('div.modal-container[modal-id="manual-order-summary"]').find('div.customer-information-container').html('');
                uiContent.find('div.order_customer_information_container').html('');
                uiContent.find('div.order_customer_information_container').html('');
                uiContent.find('div.order_address_container').find('div.order_primary_address:not(.template)').remove();

                if(oOrderInfo.is_senior == 0 && oOrderInfo.is_pwd == 0)
                {
                    uiContent.find('div.discount_container input#shopping-cart-none').prop("checked", true);
                }
                else if(oOrderInfo.is_pwd == 1 && oOrderInfo.is_senior == 0)
                {
                    uiContent.find('div.discount_container input#shopping-cart-pwd').prop("checked", true);
                }
                else if(oOrderInfo.is_senior == 1 && oOrderInfo.is_pwd == 0)
                {
                    uiContent.find('div.discount_container input#shopping-cart-senior').prop("checked", true);
                }

                uiContent.find('div.rta-container').append(uiRTA)
                uiContent.find('div.rta-container').append(forEditOrderCallback)
                $('div.modal-container[modal-id="manual-order-summary"]').find('div.customer-information-container').append(uiBillBreakdown)
                uiContent.find('div.order_customer_information_container').append(uiCustomerInformation)
                uiContent.find('div.order_address_container').append(uiAddress);

            }

        },

        'update_address_deliver_to_different': function (buttonConfirmUpdateAddress, fullAddress) {
            if(typeof(buttonConfirmUpdateAddress) !== 'undefined')
            {
                var uiModalParent = buttonConfirmUpdateAddress.parents('div[modal-id="update-address"]:first');
                var iOrderId = uiModalParent.attr('data-order-id');
                var iAgentId = uiModalParent.attr('data-admin-id');
                var iOrderStatus = buttonConfirmUpdateAddress.attr('data-order-status');
                var iStoreId = ( (uiModalParent.find('span[store_id]').attr('store_id') > 0) ? uiModalParent.find('span[store_id]').attr('store_id') : 0);
                var bAddressId = $('button.confirm_update_address').attr('address_id')
                var btnAddressId = ((typeof(bAddressId) !== 'undefined') ? bAddressId : 0);
                var iAddressId = ( (uiModalParent.find('div.customer_address:visible').length > 0) ? uiModalParent.find('div.customer_address:visible').attr('address_id') : btnAddressId );
                var oParams = {
                    data      : [
                        {
                            field: "deliver_to_customer_address",
                            value: iAddressId //reroute where
                        },
                        {
                            field: "order_status",
                            value: iOrderStatus //reroute where
                        },
                        {
                            field: "store_id",
                            value: iStoreId //reroute where
                        },
                        {
                            field: "locked_by",
                            value: 0
                        }
                    ],
                    qualifiers: [{
                        field   : "o.id",
                        value   : iOrderId,
                        operator: "="
                    }],

                    "user_id": iAgentId,
                    "type"   : 'change_address',
                    "full_address" : ((typeof(fullAddress) != 'undefined') ? fullAddress : '' )
                };

                callcenter.coordinator_management.show_spinner(buttonConfirmUpdateAddress, true)

                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.orders') + "/orders/update",
                    "success": function (oData) {
                        if (oData.status == true) {
                            $('div[modal-id="update-address"]').removeClass('showed');
                            $('body').css('overflow', 'auto');

                            $('div[modal-id="update-address"] form#coordinator_update_address').find('div.found-retail').hide();
                            $('div[modal-id="update-address"] form#coordinator_update_address').find('div.find-rta-container').show();
                
                            callcenter.coordinator_management.show_spinner(buttonConfirmUpdateAddress, false);
                            $('section#search_order_list_container').find('div.order_search_result_block[data-order-id="' + iOrderId + '"]').remove();
                        }
                    }
                };

                callcenter.coordinator_management.cajax(oAjaxConfig);
            }
        },

        'fetch_callcenters': function () {
            var oParams = {
                "params": {
                    "limit": 99999
                }
            };

            var oAjaxConfig = {
                "type"   : "GET",
                "data"   : oParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : callcenter.config('url.api.jfc.users') + "/users/call",
                "success": function (oData) {
                    //alert(JSON.stringify(oData));
                    var uiContainer = $('section#search_order_form_container div.select.callcenter-select').find('div.frm-custom-dropdown-option');
                    var sHtml = '';
                    if(typeof(oData) !== 'undefined')
                    {
                        $.each(oData.data, function (key, callcenter) {
                            sHtml += '<div class="option" data-value="' + callcenter.id + '">' + callcenter.name + '</div>';
                        })
                    }

                    uiContainer.append(sHtml)

                }
            };

            callcenter.coordinator_management.cajax(oAjaxConfig);
        },

        'submit_message': function (oMessage) {
            ////console.log(iConstantCallcenterId);
            var uiTemplate = callcenter.coordinator_management.append_message(oMessage, iConstantUserId, true);
            var iMessageID = 0;
            if (typeof(oMessage) !== 'undefined') {
                var oParams = {
                    "params": {
                        "to"           : iSelectedStore,
                        "message"      : oMessage.message,
                        "type"         : 1,
                        "from"         : iConstantUserId,
                        "store_id"     : iSelectedStore,
                        "callcenter_id": iConstantCallcenterId
                    }
                };
                var oAjaxConfig = {
                    "type"   : "POST",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IEYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.messenger') + "/messenger/send",
                    "success": function (oData) {
                        if(typeof(oData) !== 'undefined')
                        {
                            uiTemplate.attr('data-message-id', oData.data.message_id)
                        }
                    }
                };

                callcenter.coordinator_management.cajax(oAjaxConfig);
            }

            return iMessageID;
        },

        'append_message': function (message, iUserID, bFromUser) {
            var uiContainer = $('div[modal-id="messenger"]').find('div.chat_box_container');

            var uiTemplate = $('div.chat_message.template').clone().removeClass('template').unwrap('div');

            uiTemplate.find('p.data-message-content').text(message.message);
            uiTemplate.find('span.data-username-details').text(' - ' + message.username);
            if (moment().format('MMMM Do YYYY') != moment(message.date_sent).format('MMMM Do YYYY')) {
                uiTemplate.find('span.data-date-sent').text(message.date_sent);
            }
            else {
                uiTemplate.find('span.data-date-sent').text('Today ' + moment(message.date_sent).format('h:mm:ss a'));
            }


            if (message.message_from == iUserID) {
                var sMessageType = 'receiver';
                uiTemplate.find('img.sender-profile').addClass('hidden');
                uiTemplate.find('img.receiver-profile').removeClass('hidden');
                uiTemplate.find('p.data-date-indention').addClass('text-right');
            }
            else {
                var sMessageType = 'sender';
                uiTemplate.find('img.sender-profile').removeClass('hidden');
                uiTemplate.find('img.receiver-profile').addClass('hidden');
                uiTemplate.find('p.data-date-indention').addClass('text-left');
            }

            uiTemplate.find('div.message-identity').addClass(sMessageType);

            uiTemplate.attr('data-message-id', message.message_id)
            if (bFromUser) {
                uiTemplate.attr('data-is-seen', 0);

                uiTemplate.attr('data-author-id', message.message_from)
            }
            else {
                uiTemplate.attr('data-is-seen', message.is_seen)

                if (typeof(message.from) !== 'undefined') {
                    uiTemplate.attr('data-author-id', message.from)
                }
                else {
                    uiTemplate.attr('data-author-id', message.message_from)
                }

            }


            uiContainer.append(uiTemplate);
            $('div[modal-id="messenger"]').find('div.chat_box_container').scrollTop(99999999999);

            return uiTemplate
        },

        'fetch_messenger_messages': function (iUserID, iStoreID) {

            if (typeof(iUserID) !== 'undefined') {
                var oParams = {
                    "params": {
                        "where": [
                            {
                                "special": true,
                                "query"  : " AND m.store_id = " + iStoreID + " AND m.callcenter_id = " + iConstantCallcenterId + " "
                            }
                        ]
                    }
                };

                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IEYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.messenger') + "/messenger/get",
                    "success": function (oData) {
                        $('div[modal-id="messenger"]').find('div.chat_box_container').html("");
                        if (oData.data.length > 0) {

                            $.each(oData.data, function (key, message) {
                                callcenter.coordinator_management.append_message(message, iUserID)
                            })

                        }

                        $('div[modal-id="messenger"]').find('div.chat_box_container').scrollTop(99999999999);
                    }
                };

                callcenter.coordinator_management.cajax(oAjaxConfig);
            }
        },

        'fetch_route_stores': function (sbu_id, sFetchRouteStores) {
            ////console.log(sFetchRouteStores);
            if (typeof(sbu_id) !== 'undefined') {
                var uiContainer = $('div[modal-id="reroute-order"]').find('table.table_store_container');

                var oParams = {
                    "params": {
                        "sbu_id": sbu_id,
                        "in"    : [],
                        "limit" : 999999
                    }
                };

                oParams.params.in.push({
                    'field'   : 's.province',
                    'operator': 'IN',
                    'value'   : sFetchRouteStores //set this dynamically
                })

                var oAjaxConfig = {
                    "type"   : "GET",
                    "data"   : oParams,
                    "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                    "url"    : callcenter.config('url.api.jfc.store') + "/stores/get",
                    "success": function (oData) {

                        oStoreCount = oData.data.length;
                        $('section#online_stores_count_container').find('span.all_store_count').text(oStoreCount);
                        $('section#offline_stores_count_container').find('span.all_store_count').text(oStoreCount);

                        if (oData.status == true && oData.data.length > 0) {
                            var sHtml = '';
                            var sMessengerHtml = '';
                            var sStoreManamentHtml = '';
                            oLocalData.store_messenger = {}

                            var uiFilterStoreCoordinatorManagement = $("section#search_order_form_container form#search_order_form_params").find('div.stores_select')
                                .children("div.select")
                                .children("div.frm-custom-dropdown")
                                .children("div.frm-custom-dropdown-option");

                            $.each(oData.data, function (key, store) {
                                sHtml += '<tr class="store_route" data-store-id="' + store.id + '"><td><p class="font-12">' + store.name + ' | ' + store.code + '</p></td></tr>';
                                if (key == 0) {
                                    sMessengerHtml += '<tr data-store-id="' + store.id + '"><td class="padding-all-10 store-selected">' + store.id + ' - ' + store.name + '<div class="store-msg-notify hidden"></div></td></tr>';
                                }
                                else {
                                    sMessengerHtml += '<tr data-store-id="' + store.id + '"><td class="padding-all-10">' + store.id + ' - ' + store.name + '<div class="store-msg-notify hidden"></div></td></tr>';
                                }
                                var sProvince = '';

                                $.each(oProvinces, function (key, value) {
                                    if (value.id == store.province) {
                                        sProvince = oProvinces[key].name;
                                    }
                                });

                                sStoreManamentHtml += '<tr class="bottom-border store" data-store-id="' + store.id + '">' +
                                                      '<td class="padding-all-10" data-store-id="' + store.id + '">' + store.id + '</td>' +
                                                      '<td class="padding-all-10" data-store-name="' + store.name + '">' + store.name + '</td>' +
                                                      '<td class="padding-all-10" data-store-code="' + store.code + '">' + store.code + '</td>' +
                                                      '<td class="padding-all-10" data-store-province="' + store.province + '">' + sProvince + '</td>' +
                                                      '<td class="padding-all-10" data-store-ownership-type="' + store.id + '">Company</td>' +
                                                      '<td class="padding-all-10" data-store-connection-type="' + store.id + '">Bayan-Tel IPVN</td>' +
                                                      '<td class="padding-all-10" data-store-trading-time="' + store.id + '">7:00 AM - 10:00 PM</td>' +
                                                      '<td class="padding-all-10" data-store-on-time="' + store.id + '">6hrs</td>' +
                                                      '<td class="padding-all-10" data-store-time="' + store.id + '">45mins</td>' +
                                                      '</tr>';





                                var sFilterStoresHtml = "<div class='option' data-value='" + store.id + "'>" + store.name + "</div>";
                                uiFilterStoreCoordinatorManagement.append(sFilterStoresHtml);


                            });

                            callcenter.store_management.redraw_presence();
                            $('section#offline_stores_search_container').find('tbody.offline_store_container').append(sStoreManamentHtml)
                            //uiContainer.find('tbody').html("").append(sHtml);
                            $('div[modal-id="messenger"]').find('tbody.store_messenger_container').html("").append(sMessengerHtml);
                            $('div[modal-id="messenger"]').find('tbody.store_messenger_container').append('<tr data-store-id="225"><td class="padding-all-10">225 - JBNCR <div class="store-msg-notify hidden"></div></td></tr>')

                            //oSock.bind(
                            //    {
                            //        'event'   : 'messenger_send',
                            //        'channel' : 'presence-225',
                            //        'callback': function (oData) {
                            //            ////console.log(oData);
                            //            if (oData.from != iConstantUserId) {
                            //                //console.log(oData);
                            //                if (oData.callcenter_id == iConstantCallcenterId) {
                            //                    //console.log(oData.to + " == " + iConstantCallcenterId)
                            //                    if (oData.store_id == iSelectedStore) {
                            //                        callcenter.coordinator_management.append_message(oData, oData.to, false)
                            //                    }
                            //
                            //                    else {
                            //
                            //                        var uiTrStore = $('div[modal-id="messenger"]').find('tbody.store_messenger_container').find('tr[data-store-id="' + oData.store_id + '"]');
                            //                        var uiTrNotify = uiTrStore.find('div.store-msg-notify').text();
                            //                        if (uiTrNotify.length == 0) {
                            //                            uiTrNotify = 1;
                            //                        }
                            //                        else {
                            //                            uiTrNotify = parseInt(uiTrNotify) + 1;
                            //                        }
                            //
                            //                        uiTrStore.find('div.store-msg-notify').text(uiTrNotify).removeClass('hidden');
                            //
                            //                    }
                            //
                            //                    if ($('div[modal-id="messenger"]').is(':not(:visible)')) {
                            //                        var iButtonNotify = 0;
                            //                        var uiButton = $('button[modal-target="messenger"]');
                            //                        $.each(uiButton, function () {
                            //
                            //                            var uiButtonNotify = $(this).find('div.notify').text();
                            //                            if (uiButtonNotify.length == 0) {
                            //                                iButtonNotify = 1;
                            //                            }
                            //                            else {
                            //                                iButtonNotify = parseInt(uiButtonNotify) + 1;
                            //                            }
                            //
                            //                            $(this).find('div.notify').text(iButtonNotify).removeClass('hidden');
                            //
                            //                        })
                            //
                            //                    }
                            //                }
                            //
                            //
                            //            }
                            //
                            //
                            //        }
                            //    }
                            //)
                            //
                            //
                            //oSock.bind(
                            //    {
                            //        'event'   : 'messenger_seen',
                            //        'channel' : 'presence-225',
                            //        'callback': function (oData) {
                            //            if (oData.author_id == iConstantUserId) {
                            //                var sDate = '';
                            //                if (moment().format('MMMM Do YYYY') == moment(oData.date_seen).format('MMMM Do YYYY')) {
                            //                    sDate = 'Today';
                            //                }
                            //                else {
                            //                    sDate = moment(oData.date_seen).format('MMMM Do YYYY');
                            //                }
                            //                var uiMessage = $('div[modal-id="messenger"]').find('div.chat_box_container').find('div.chat_message[data-message-id="' + oData.message_id + '"]');
                            //                uiMessage.append('<p class="gray-color font-12 text-right">(Seen - ' + sDate + ' ' + moment(oData.date_seen).format('h:mm:ss a') + ')</p>');
                            //                uiMessage.attr('data-is-seen', 1);
                            //            }
                            //
                            //        }
                            //    }
                            //);
                            //
                            //oSock.bind(
                            //    {
                            //        'event'   : 'messenger_send',
                            //        'channel' : 'presence-345',
                            //        'callback': function (oData) {
                            //            ////console.log(oData);
                            //            if (oData.from != iConstantUserId) {
                            //                //console.log(oData);
                            //                if (oData.callcenter_id == iConstantCallcenterId) {
                            //                    //console.log(oData.to + " == " + iConstantCallcenterId)
                            //                    if (oData.store_id == iSelectedStore) {
                            //                        callcenter.coordinator_management.append_message(oData, oData.to, false)
                            //                    }
                            //
                            //                    else {
                            //
                            //                        var uiTrStore = $('div[modal-id="messenger"]').find('tbody.store_messenger_container').find('tr[data-store-id="' + oData.store_id + '"]');
                            //                        var uiTrNotify = uiTrStore.find('div.store-msg-notify').text();
                            //                        if (uiTrNotify.length == 0) {
                            //                            uiTrNotify = 1;
                            //                        }
                            //                        else {
                            //                            uiTrNotify = parseInt(uiTrNotify) + 1;
                            //                        }
                            //
                            //                        uiTrStore.find('div.store-msg-notify').text(uiTrNotify).removeClass('hidden');
                            //
                            //                    }
                            //
                            //                    if ($('div[modal-id="messenger"]').is(':not(:visible)')) {
                            //                        var iButtonNotify = 0;
                            //                        var uiButton = $('button[modal-target="messenger"]');
                            //                        $.each(uiButton, function () {
                            //
                            //                            var uiButtonNotify = $(this).find('div.notify').text();
                            //                            if (uiButtonNotify.length == 0) {
                            //                                iButtonNotify = 1;
                            //                            }
                            //                            else {
                            //                                iButtonNotify = parseInt(uiButtonNotify) + 1;
                            //                            }
                            //
                            //                            $(this).find('div.notify').text(iButtonNotify).removeClass('hidden');
                            //
                            //                        })
                            //
                            //                    }
                            //                }
                            //
                            //
                            //            }
                            //
                            //
                            //        }
                            //    }
                            //)
                            //
                            //
                            //oSock.bind(
                            //    {
                            //        'event'   : 'messenger_seen',
                            //        'channel' : 'presence-345',
                            //        'callback': function (oData) {
                            //            if (oData.author_id == iConstantUserId) {
                            //                var sDate = '';
                            //                if (moment().format('MMMM Do YYYY') == moment(oData.date_seen).format('MMMM Do YYYY')) {
                            //                    sDate = 'Today';
                            //                }
                            //                else {
                            //                    sDate = moment(oData.date_seen).format('MMMM Do YYYY');
                            //                }
                            //                var uiMessage = $('div[modal-id="messenger"]').find('div.chat_box_container').find('div.chat_message[data-message-id="' + oData.message_id + '"]');
                            //                uiMessage.append('<p class="gray-color font-12 text-right">(Seen - ' + sDate + ' ' + moment(oData.date_seen).format('h:mm:ss a') + ')</p>');
                            //                uiMessage.attr('data-is-seen', 1);
                            //            }
                            //
                            //        }
                            //    }
                            //);

                            $('div[modal-id="messenger"]').find('table.table_store_messenger_container').filterTable({
                                inputSelector: '#messenger_search_store'
                            });
                        }


                    }
                };

                callcenter.coordinator_management.cajax(oAjaxConfig);
            }
        },

        'assembled_fetch_order_params': function (uiForm, uiButton) {
            //var oParams = {
            //    "params": {
            //        "where_like": [],
            //        "where"     : [],
            //        "limit" : 999999
            //    }
            //
            //};
            //var sSearchString = uiForm.find('input[name="search_string"]').val();
            //var sSearchBy = uiForm.find('input[name="search_by"]').val();
            //var sSearchByProvinceID = uiForm.find('input[name="province_id"]').attr("value");
            //var sStoreID = uiForm.find('input[name="store_id"]').attr("value");
            //var sCallcenterID = uiForm.find('input[name="callcenter_id"]').attr("value");
            //
            //if (uiForm.find('input[name="date_from"]').val().length > 0) {
            //    var sDateFrom = uiForm.find('input[name="date_from"]').val().split("/");
            //    sDateFrom = sDateFrom[2] + '-' + sDateFrom[0] + '-' + sDateFrom[1] + ' 00:00:00';
            //}
            //else {
            //    sDateFrom = "";
            //}
            //
            //if (uiForm.find('input[name="date_from"]').val().length > 0) {
            //    var sDateTo = uiForm.find('input[name="date_to"]').val().split("/");
            //    sDateTo = sDateTo[2] + '-' + sDateTo[0] + '-' + sDateTo[1] + ' 23:59:59';
            //}
            //else {
            //    sDateTo = "";
            //}
            //
            //var sOrderType = uiForm.find('input[name="order_type"]').val();
            //if (sSearchBy == 'Order ID') {
            //    oParams.params.where_like.push({
            //        'field'   : 'o.id',
            //        'operator': 'LIKE',
            //        'value'   : sSearchString
            //    })
            //}else if (sSearchBy == 'Contact Number') {
            //    oParams.params.where_like.push({
            //        'field'   : 'o.display_data',
            //        'operator': 'LIKE',
            //        'value'   : sSearchString
            //    })
            //}
            //
            //if (sOrderType > 0) {
            //    oParams.params.where.push({
            //        'field'   : 'o.order_status',
            //        'operator': '=',
            //        'value'   : sOrderType
            //    })
            //}
            //
            //if (sStoreID !="All Stores") {
            //    oParams.params.where.push({
            //        'field'   : 'o.store_id',
            //        'operator': '=',
            //        'value'   : sStoreID
            //    })
            //}
            //
            //if (sCallcenterID !="All Call Centers") {
            //    oParams.params.where.push({
            //        'field'   : 'cu.callcenter_id',
            //        'operator': '=',
            //        'value'   : sCallcenterID
            //    })
            //}
            //
            //if (sSearchByProvinceID != "All Areas") {
            //    oParams.params.where.push({
            //        'field'   : 'ca.province',
            //        'operator': '=',
            //        'value'   : sSearchByProvinceID
            //    })
            //}
            //
            //if (sDateFrom.length > 0) {
            //    oParams.params.where.push({
            //        'field'   : 'o.date_added',
            //        'operator': '>=',
            //        'value'   : sDateFrom
            //    })
            //}
            //
            //if (sDateTo.length > 0) {
            //    oParams.params.where.push({
            //        'field'   : 'o.date_added',
            //        'operator': '<=',
            //        'value'   : sDateTo
            //    })
            //}
            //
            //
            //////console.log(oParams);
            //callcenter.coordinator_management.orders_get(oParams, uiButton);
        },

        'orders_get': function (oParams, uiButton) {
            var oAjaxConfig = {
                "type"   : "Get",
                "data"   : oParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : callcenter.config('url.api.jfc.orders') + "/orders/get",
                "success": function (oData) {
                    var uiContainer = $('section#search_order_list_container').find('div.search_result_container');
                    uiContainer.html('');
                    //clear search box
                    $('section#search_order_form_container form#search_order_form_params input[name="search_string"]').val("");
                    if(typeof(oData) !== 'undefined')
                    {
                        if (oData.status == true) {
                            // console.log(oData.data)
                            //$.each(oData.data, function (key, order) {
                            //    var uiTemplate = $('div.order_search_result_block.template').clone().removeClass('template');
                            //    // ////console.log(JSON.stringify(order));
                            //    callcenter.coordinator_management.manipulate_template(uiTemplate, order);
                            //    uiContainer.append(uiTemplate);
                            //})

                            cr8v.add_to_storage('orders', oData.data);

                            //for the sorting
                            callcenter.coordinator_management.clear_search_order_list_sortby_classes();
                            //default the customer name sort
                            $('section#search_order_form_container form#search_order_form_params a.search_order_list_sortby_name').addClass("red-color active");
                            $('section#search_order_form_container form#search_order_form_params a.search_order_list_sortby_name').find("i.fa").addClass("fa-angle-up");
                            $('section#search_order_form_container form#search_order_form_params a.search_order_list_sortby_name').attr("data-value", "desc");

                            $('section#search_order_form_container form#search_order_form_params').off('click', 'a.search_order_list_sortby_name').on('click', 'a.search_order_list_sortby_name', function () {
                                callcenter.coordinator_management.assemble_search_order_list_sort_by($(this), "c.first_name");
                            })

                            $('section#search_order_form_container form#search_order_form_params').off('click', 'a.search_order_list_sortby_store').on('click', 'a.search_order_list_sortby_store', function () {
                                callcenter.coordinator_management.assemble_search_order_list_sort_by($(this), "o.store_id");
                            })

                            $('section#search_order_form_container form#search_order_form_params').off('click', 'a.search_order_list_sortby_criticality').on('click', 'a.search_order_list_sortby_criticality', function () {
                                callcenter.coordinator_management.assemble_search_order_list_sort_by($(this), "o.store_id");
                            })
                        }
                        else {
                            ////console.log('api error')
                        }
                    }

                    if (typeof(uiButton) !== 'undefined') {
                        callcenter.coordinator_management.show_spinner(uiButton, false);
                    }

                }
            };

            callcenter.coordinator_management.cajax(oAjaxConfig);
        },

        'clear_search_order_list_sortby_classes': function () {
            $('section#search_order_form_container form#search_order_form_params a.search_order_list_sort').each(function (i) {
                $(this).css({"color": "black"});
                $(this).removeClass("active red-color");
                $(this).find("i.fa").removeClass("fa-angle-up");
                $(this).find("i.fa").removeClass("fa-angle-down");
            });
        },

        'assemble_search_order_list_sort_by': function (uiSortField, sOrderby) {
            callcenter.coordinator_management.clear_search_order_list_sortby_classes();

            uiSortField.addClass("active red-color");
            var order_by = sOrderby;
            var sorting = "";
            if (uiSortField.attr("data-value") == "asc") {
                var sorting = uiSortField.attr("data-value");
                uiSortField.attr("data-value", "desc");
                uiSortField.find("i.fa").addClass("fa-angle-up");
                uiSortField.find("i.fa").removeClass("fa-angle-down");
            } else {
                var sorting = uiSortField.attr("data-value");
                uiSortField.attr("data-value", "asc");
                uiSortField.find("i.fa").addClass("fa-angle-down");
                uiSortField.find("i.fa").removeClass("fa-angle-up");
            }

            $('section#search_order_form_container form#search_order_form_params input[name="search_order_list_orderby"]').attr("value", order_by);
            $('section#search_order_form_container form#search_order_form_params input[name="search_order_list_sorting"]').attr("value", sorting);

            var uiForm = $('section#search_order_form_container form#search_order_form_params');
            var oParams = {
                "params": {
                    "where_like": [],
                    "where"     : [],
                    "order_by"  : "",
                    "sorting"   : "",
                    "limit"     : 999999
                }

            };

            var sSearchString = uiForm.find('input[name="search_string"]').val();
            var sSearchBy = uiForm.find('input[name="search_by"]').val();
            var sSearchByProvinceID = uiForm.find('input[name="province_id"]').attr("value");
            var sStoreID = uiForm.find('input[name="store_id"]').attr("value");
            var sCallcenterID = uiForm.find('input[name="callcenter_id"]').attr("value");
            var sOrderBy = uiForm.find('input[name="search_order_list_orderby"]').attr("value");
            var sSorting = uiForm.find('input[name="search_order_list_sorting"]').attr("value");

            if (uiForm.find('input[name="date_from"]').val().length > 0) {
                var sDateFrom = uiForm.find('input[name="date_from"]').val().split("/");
                sDateFrom = sDateFrom[2] + '-' + sDateFrom[0] + '-' + sDateFrom[1] + ' 00:00:00';
            }
            else {
                sDateFrom = "";
            }

            if (uiForm.find('input[name="date_from"]').val().length > 0) {
                var sDateTo = uiForm.find('input[name="date_to"]').val().split("/");
                sDateTo = sDateTo[2] + '-' + sDateTo[0] + '-' + sDateTo[1] + ' 23:59:59';
            }
            else {
                sDateTo = "";
            }

            var sOrderType = uiForm.find('input[name="order_type"]').val();
            if (sSearchBy == 'Order ID') {
                oParams.params.where_like.push({
                    'field'   : 'o.id',
                    'operator': 'LIKE',
                    'value'   : sSearchString
                })
            } else if (sSearchBy == 'Contact Number') {
                oParams.params.where_like.push({
                    'field'   : 'o.display_data',
                    'operator': 'LIKE',
                    'value'   : sSearchString
                })
            }

            if (sOrderType > 0) {
                oParams.params.where.push({
                    'field'   : 'o.order_status',
                    'operator': '=',
                    'value'   : sOrderType
                })
            }

            if (sStoreID != "All Stores") {
                oParams.params.where.push({
                    'field'   : 'o.store_id',
                    'operator': '=',
                    'value'   : sStoreID
                })
            }

            if (sCallcenterID != "All Call Centers") {
                oParams.params.where.push({
                    'field'   : 'cu.callcenter_id',
                    'operator': '=',
                    'value'   : sCallcenterID
                })
            }


            if (sSearchByProvinceID != "All Areas") {
                oParams.params.where.push({
                    'field'   : 'ca.province',
                    'operator': '=',
                    'value'   : sSearchByProvinceID
                })
            }

            if (sDateFrom.length > 0) {
                oParams.params.where.push({
                    'field'   : 'o.date_added',
                    'operator': '>=',
                    'value'   : sDateFrom
                })
            }

            if (sDateTo.length > 0) {
                oParams.params.where.push({
                    'field'   : 'o.date_added',
                    'operator': '<=',
                    'value'   : sDateTo
                })
            }

            if (sOrderBy != "") {
                oParams.params.order_by = sOrderby;
            }

            if (sSorting != "") {
                oParams.params.sorting = sSorting;
            }

            var oAjaxConfigSort = {
                "type"   : "Get",
                "data"   : oParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : callcenter.config('url.api.jfc.orders') + "/orders/get",
                "success": function (oData) {
                    var uiContainer = $('section#search_order_list_container').find('div.search_result_container');
                    uiContainer.html('')
                    if(typeof(oData) !== 'undefined')
                    {
                        if (oData.status == true) {
                            //////console.log(oData.data)
                            $.each(oData.data, function (key, order) {
                                var uiTemplate = $('div.order_search_result_block.template').clone().removeClass('template');
                                //////console.log(JSON.stringify(order));
                                callcenter.coordinator_management.manipulate_template(uiTemplate, order);
                                uiContainer.append(uiTemplate);
                            })
                        }
                        else {
                            ////console.log('api error')
                        }
                    }
                }
            };
            ////console.log(oParams);
            callcenter.coordinator_management.cajax(oAjaxConfigSort);

        },

        'manipulate_template': function (uiTemplate, oOrderData) {

            var oDisplayData = $.parseJSON(oOrderData.display_data);
            // console.log(oOrderData)
            uiTemplate.find('info.order_id').text(oOrderData.id);
            uiTemplate.find('info.customer_name').text(oDisplayData.customer_name);
            uiTemplate.attr('data-customer-name', oDisplayData.customer_name);
            uiTemplate.attr('data-store-id', oOrderData.store_id);
            uiTemplate.attr('data-customer-id', oOrderData.customer_id);
            uiTemplate.attr('data-order-id', oOrderData.id);
            uiTemplate.attr('data-admin-id', iConstantUserId);
            uiTemplate.attr('data-payment-mode-id', oOrderData.payment_mode_id);
            uiTemplate.attr('data-payment-mode', oOrderData.payment_mode);
            uiTemplate.find('info.store_summary').text(oDisplayData.store_summary);
            uiTemplate.find('info.customer_contact').text(oDisplayData.contact_number).attr('customer_mobile_alternate', oDisplayData.contact_number_alternate);
            uiTemplate.find('info.date_added').text(oOrderData.date_added);
            uiTemplate.find('info.payment_mode').text(oOrderData.payment_mode);
            uiTemplate.find('info.change_for').text(oOrderData.change_for + ' PHP');
            uiTemplate.attr('data-order-id', oOrderData.id);
            uiTemplate.attr('data-order-status', oOrderData.order_status);
            uiTemplate.attr('data-date-added-exact', oOrderData.date_added_exact);
            if(oOrderData.hasOwnProperty("customer_information"))
            {
                uiTemplate.attr('is_pwd', (typeof(oOrderData.customer_information.is_pwd) != 'undefined') ?oOrderData.customer_information.is_pwd : 0);
                uiTemplate.attr('is_senior', (typeof(oOrderData.customer_information.is_senior) != 'undefined') ?oOrderData.customer_information.is_senior : 0);
            }
            else
            {
                uiTemplate.attr('is_pwd', (typeof(oOrderData.is_pwd) != 'undefined') ? oOrderData.is_pwd : 0);
                uiTemplate.attr('is_senior', (typeof(oOrderData.is_senior) != 'undefined') ? oOrderData.is_senior : 0);
            }

            /*getServerTime(function () {
                //uiTemplate.find('info.elapsed_time').text(oOrderData.date_added_exact);
                createElapsedTime(oOrderData, uiTemplate);
            });*/
            if(oOrderData.order_status == 19 || oOrderData.order_status == 3 || oOrderData.order_status == 21){
                //oEllapsedTime[oOrderData.id].stop();
                var d2 = moment(oOrderData.date_added_exact);
                var d1 = moment(oOrderData.date_updated);
                var iSecondsDiff =  (d1- d2)/1000;
                var sTimeDiff = timeDiff(iSecondsDiff); 
                //console.log(sTimeDiff);
                uiTemplate.find('info.elapsed_time').text(sTimeDiff.h + ':' + sTimeDiff.m + ':' + sTimeDiff.s);
            }else{
                createElapsedTime(oOrderData.id,oOrderData.date_added_exact, uiTemplate);     
            }
            
            /*happy plus cards*/
            if (count(oDisplayData.happy_plus) > 0 && typeof(oDisplayData.happy_plus) !== 'undefined') {
                $.each(oDisplayData.happy_plus, function (k, v) {
                    var uiHappyPlusTemplate = uiTemplate.find('div.happy_plus_card.template').clone().removeClass('template');
                    uiHappyPlusTemplate.find('p.info-happy-plus-number').text(v.number);
                    uiHappyPlusTemplate.find('p.expiration_date').text(v.date);
                    uiHappyPlusTemplate.attr({'hpc_id': v.id, 'hpc_number' : v.number,'hpc_date' : v.date, 'hpc_remarks': v.remarks})
                    callcenter.call_orders.clone_append(uiHappyPlusTemplate, uiTemplate.find('div.happy_plus_card_container'));
                })
            }

            var i = oOrderData.customer_information;

            if(typeof(i) !== 'undefined')
            {
                if (typeof(i.other_address) !== 'undefined') {
                    if (count(i.other_address) > 0 && count(i.other_address_text) > 0) {
                        /*default address*/
                        //////console.log(i.address_text);
                        i.address = i.other_address;
                        i.address_text = i.other_address_text;

                        var house_number = (i.address[0].house_number !== '' && i.address[0].house_number != "0") ? i.address[0].house_number : '',
                            floor = (i.address[0].floor !== '' && i.address[0].floor != "0") ? i.address[0].floor : '',
                            building = (i.address_text[0].building !== null) ? i.address_text[0].building.name : '',
                            barangay = (i.address_text[0].barangay !== null) ? i.address_text[0].barangay.name : '',
                            subdivision = (i.address_text[0].subdivision !== null) ? i.address_text[0].subdivision.name : '',
                            city = (i.address_text[0].city !== null) ? i.address_text[0].city.name : '',
                            street = (i.address_text[0].street !== null) ? i.address_text[0].street.name : '',
                            second_street = (i.address_text[0].second_street !== null && typeof(i.address_text[0].second_street) != 'undefined') ? i.address_text[0].second_street.name : '',
                            province = (i.address_text[0].province !== null) ? i.address_text[0].province.name : '';
                            
                        /*other address*/
                        $.each(i.address, function (k, v) {

                            var house_number = (v.house_number !== '' && v.house_number != "0") ? v.house_number : '',
                                floor = (v.floor !== '' && v.floor != "0") ? v.floor : '',
                                building = (i.address_text[k].building !== null) ? i.address_text[k].building.name : '',
                                barangay = (i.address_text[k].barangay !== null) ? i.address_text[k].barangay.name : '',
                                subdivision = (i.address_text[k].subdivision !== null) ? i.address_text[k].subdivision.name : '',
                                city = (i.address_text[k].city !== null) ? i.address_text[k].city.name : '',
                                street = (i.address_text[k].street !== null) ? i.address_text[k].street.name : '',
                                second_street = (i.address_text[k].second_street !== null && typeof(i.address_text[k].second_street) != 'undefined') ? i.address_text[k].second_street.name : '',
                                province = (i.address_text[k].province !== null) ? i.address_text[k].province.name : '';

                            var uiAddressTemplate = uiTemplate.find('div.customer_address.template').clone().removeClass('template');
                            //////console.log(uiAddressTemplate)
                            uiAddressTemplate.find('strong.info_address_label').text(v.address_label);
                            uiAddressTemplate.find('p.info_address_complete').text('' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);
                            uiAddressTemplate.find('p.info_address_complete').attr('data-address', '/##/' + house_number + '/##/' + building + '/##/' + floor + '/##/' + street + '/##/' + second_street + '/##/' + barangay + '/##/' + subdivision + '/##/' + city + '/##/' + province);
                            uiAddressTemplate.find('strong.info_address_type').text(v.address_type.toUpperCase());
                            uiAddressTemplate.find('p.info_address_landmark').text('- ' + v.landmark.toUpperCase());

                            uiAddressTemplate.attr('customer_id', i.id);
                            uiAddressTemplate.attr('address_id', v.id);
                            uiAddressTemplate.attr('house_number', v.house_number);
                            //uiAddressTemplate.attr('building', v.building);
                            uiAddressTemplate.attr('building', building);
                            uiAddressTemplate.attr('floor', v.floor);
                            uiAddressTemplate.attr('street', v.street);
                            uiAddressTemplate.attr('second_street', v.second_street);
                            uiAddressTemplate.attr('barangay', v.barangay);
                            uiAddressTemplate.attr('subdivision', v.subdivision);
                            uiAddressTemplate.attr('city', v.city);
                            uiAddressTemplate.attr('province', v.province);
                            uiAddressTemplate.attr('all_address', '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);

                            if (v.is_default == 1) {
                                uiAddressTemplate.attr('address_id', v.id);

                                /* hidden fields for find rta */
                                uiTemplate.find('div.hidden_fields_for_rta input[name="house_number"]').val(v.house_number);
                                uiTemplate.find('div.hidden_fields_for_rta input[name="building"]').val(building);
                                uiTemplate.find('div.hidden_fields_for_rta input[name="floor"]').val(v.floor);
                                uiTemplate.find('div.hidden_fields_for_rta input[name="street"]').val(v.street);
                                uiTemplate.find('div.hidden_fields_for_rta input[name="barangay"]').val(v.barangay);
                                uiTemplate.find('div.hidden_fields_for_rta input[name="subdivision"]').val(v.subdivision);
                                uiTemplate.find('div.hidden_fields_for_rta input[name="city"]').val(v.city);
                                uiTemplate.find('div.hidden_fields_for_rta input[name="province"]').val(v.province);

                                callcenter.call_orders.clone_append(uiAddressTemplate, uiTemplate.find('div.customer_address_container'));
                            }
                            else {
                                uiAddressTemplate.removeClass('bggray-light').addClass('white');
                                uiAddressTemplate.find('a.edit_address').remove();
                                uiTemplate.find('div.customer_secondary_address').prepend(uiAddressTemplate);
                            }


                            if (v.address_type.length > 0) {
                                uiAddressTemplate.find('img.info_address_type').attr('src', callcenter.config('url.server.base') + 'assets/images/' + v.address_type.toLocaleLowerCase() + '.png');
                                uiAddressTemplate.find('img.info_address_type').attr('onError', '$.fn.checkImage(this);');
                            }
                        })

                    }
                }
            }



            callcenter.coordinator_management.populate_order_history(oOrderData, uiTemplate);
            callcenter.coordinator_management.manipulate_according_to_order_status(oOrderData, uiTemplate);
            return uiTemplate;

        },

        'manipulate_according_to_order_status': function (oOrderData, uiTemplate) {
            if (typeof(oOrderData) !== 'undefined' && typeof(uiTemplate) !== 'undefined') {
                //uiTemplate.find('button.search_order').removeClass('hidden');
                //uiTemplate.find('button-group.locked').removeClass('hidden');


                /*check iuf locked*/
                if (oOrderData.locked_by > 0) {

                    if (iConstantUserId == oOrderData.locked_by) {
                        uiTemplate.find('info.agent_locker').text(oOrderData.agent_locker.username);
                        uiTemplate.find('div.locked').removeClass('hidden');
                        uiTemplate.find('button-group.locked').removeClass('hidden');
                    }
                    else {
                        uiTemplate.css('pointer-events', 'none');
                        uiTemplate.find('info.agent_locker').text(oOrderData.agent_locker.username);
                        uiTemplate.find('div.locked').removeClass('hidden');
                    }

                }
                else {
                    uiTemplate.find('button-group.unlocked').removeClass('hidden');
                    uiTemplate.find('button-group.unlocked button.disabled').prop('disabled', true);
                }

                /*advanced order*/
                if (oOrderData.order_status == 15) {
                    uiTemplate.find('info.follow_up').text('Yes');
                    uiTemplate.find('info.delivery_date').text(oOrderData.delivery_date);
                    uiTemplate.find('info.advanced_order').show();
                    uiTemplate.find('button.search_order').removeClass('hidden');

                }
                else if (oOrderData.order_status == 3) {
                    uiTemplate.find('button.rejected_order').removeClass('hidden');
                    //uiTemplate.find('button.manually_relay').addClass('hidden');

                }
                else if (oOrderData.order_status == 17) {
                    uiTemplate.find('button.manual_order').removeClass('hidden');
                }
                else if (oOrderData.order_status == 19) {
                    uiTemplate.find('button.completed_order').removeClass('hidden');
                }
                else if (oOrderData.order_status == 20 || oOrderData.order_status ==  22) {
                    uiTemplate.find('button.verification_order').removeClass('hidden');
                }else if (oOrderData.order_status == 12){//assembling order
                    uiTemplate.find('button.order_lock').addClass('hidden');
                    uiTemplate.find('button.order_logs').removeClass('hidden');
                }else if (oOrderData.order_status == 13){//rider out
                    uiTemplate.find('button.order_lock').addClass('hidden');
                    uiTemplate.find('button.order_logs').removeClass('hidden');
                }else if (oOrderData.order_status == 24){//for dispatch
                    uiTemplate.find('button.order_lock').addClass('hidden');
                    uiTemplate.find('button.order_logs').removeClass('hidden');
                }
                else {
                    uiTemplate.find('button.order_logs').removeClass('hidden');
                    //uiTemplate.find('button.edit_order').removeClass('hidden');
                    uiTemplate.find('button.order_lock').removeClass('hidden');
                }
                
                // console.log(oOrderData);

                if(oOrderData.is_edited == 1)
                {
                    // uiTemplate.find('button.order_lock').addClass('hidden');
                    uiTemplate.find('button.editing_order').removeClass('hidden');
                    uiTemplate.find('button.order_logs').removeClass('hidden');
                    uiTemplate.find('button.order_lock').removeClass('hidden');
                    uiTemplate.find('info.order_type').text('Editing Order');
                    uiTemplate.find('info.order_type').css('color', 'red');
                }
                else
                {
                    uiTemplate.find('info.order_type').text(oOrderData.order_status_description + ' Order');
                    ////console.log(uiTemplate.find('info.order_type'));
                    uiTemplate.find('info.order_type').css('color', oOrderData.order_status_color);
                }

                if( oOrderData.order_status == 23)
                {
                    // uiTemplate.find('button.verification_order').removeClass('hidden');
                    uiTemplate.find('button.edit_order').removeClass('hidden');
                    uiTemplate.find('button.search_order[modal-target="void-order"]').removeClass('hidden');
                }




                //if ($('section#search_order_form_container').find('form#search_order_form_params').find('input[name="order_type"]').val() == "") {
                //    uiTemplate.find('button.search_order').removeClass('hidden');
                //    uiTemplate.find('button-group.locked').removeClass('hidden');
                //}
                //else {
                //    /*check iuf locked*/
                //    if (oOrderData.locked_by > 0) {
                //
                //        if (iConstantUserId == oOrderData.locked_by) {
                //            uiTemplate.find('info.agent_locker').text('Current User');
                //            uiTemplate.find('div.locked').removeClass('hidden');
                //            uiTemplate.find('button-group.locked').removeClass('hidden');
                //        }
                //        else {
                //            uiTemplate.css('pointer-events', 'none');
                //            uiTemplate.find('info.agent_locker').text(oOrderData.agent_locker.username);
                //            uiTemplate.find('div.locked').removeClass('hidden');
                //        }
                //
                //    }
                //    else {
                //        uiTemplate.find('button-group.unlocked').removeClass('hidden');
                //    }
                //
                //    /*advanced order*/
                //    if (oOrderData.order_status == 15) {
                //        uiTemplate.find('info.follow_up').text('Yes');
                //        uiTemplate.find('info.delivery_date').text(oOrderData.delivery_date);
                //        uiTemplate.find('info.advanced_order').show();
                //        uiTemplate.find('button.search_order').removeClass('hidden');
                //
                //    }
                //    else if (oOrderData.order_status == 3) {
                //        uiTemplate.find('button.rejected_order').removeClass('hidden');
                //    }
                //    else if (oOrderData.order_status == 17) {
                //        uiTemplate.find('button.manual_order').removeClass('hidden');
                //    }
                //    else if (oOrderData.order_status == 19) {
                //        uiTemplate.find('button.completed_order').removeClass('hidden');
                //    }
                //    else if (oOrderData.order_status == 20) {
                //        uiTemplate.find('button.verification_order').removeClass('hidden');
                //    }
                //
                //
                //}
                //
                //
                //uiTemplate.find('info.order_type').text(oOrderData.order_status_description + ' Order');
                //////console.log(uiTemplate.find('info.order_type'));
                //uiTemplate.find('info.order_type').css('color', oOrderData.order_status_color);
            }
        },

        'populate_order_history': function (oOrders, uiContainer) {
            if (typeof(oOrders) !== 'undefined' && typeof(uiContainer) !== 'undefined') {
                ////console.log('here');
                // console.log(oOrders);
                uiContainer.find('div.order_history_container').children('div.order_history:not(.template)').remove()
                ////console.log(uiContainer.find('div.order_history_container'))
                var oDisplayData = $.parseJSON(oOrders.display_data);
                var uiHistoryTemplate = $('div.order_history.template').clone().removeClass('template');

                if(typeof(oDisplayData.products) !== 'undefined')
                {
                    $.each(oDisplayData.products, function (key, oItemBreakDown) {
                        if(oItemBreakDown != "" && oItemBreakDown !== null)
                        {
                            var uiBreakdownItemTemplate = uiHistoryTemplate.find('tr.product-breakdown-item.template').clone().removeClass('template');
                            uiBreakdownItemTemplate.find('td.product-name').text(oItemBreakDown.product_name);
                            uiBreakdownItemTemplate.find('td.product-quantity').text(oItemBreakDown.quantity);

                            uiBreakdownItemTemplate.find('td.product-price').text(oItemBreakDown.price + ' PHP');
                            uiBreakdownItemTemplate.find('td.product-total').text(oItemBreakDown.sub_total + ' PHP');

                            uiBreakdownItemTemplate.attr('data-product-quantity', oItemBreakDown.quantity);
                            uiBreakdownItemTemplate.attr('data-product-name', oItemBreakDown.product_name);
                            uiBreakdownItemTemplate.attr('data-main-product-id', oItemBreakDown.main_product_id);
                            uiBreakdownItemTemplate.attr('data-product-id', oItemBreakDown.item_id);
                            uiBreakdownItemTemplate.attr('data-product-price', oItemBreakDown.price);
                            uiBreakdownItemTemplate.attr('data-product-row-total', (oItemBreakDown.price * oItemBreakDown.quantity).toFixed(2));
                            uiHistoryTemplate.find('tbody.cart-container').append(uiBreakdownItemTemplate);
                            
                            if(typeof(oItemBreakDown.addons) != 'undefined')
                            {
                                 $.each(oItemBreakDown.addons, function(key, oAddonBreakDown) {
                                            var uiBreakdownItemTemplate = uiHistoryTemplate.find('tr.product-breakdown-item.template').clone().removeClass('template');
                                            uiBreakdownItemTemplate.find('td.product-name').text(oAddonBreakDown.product_name);
                                            uiBreakdownItemTemplate.find('td.product-quantity').text(oAddonBreakDown.quantity);
                                            uiBreakdownItemTemplate.find('td.product-price').text(oAddonBreakDown.price + ' PHP');
                                            uiBreakdownItemTemplate.find('td.product-total').text(oAddonBreakDown.sub_total + ' PHP');
                                            uiBreakdownItemTemplate.attr('data-product-quantity', oAddonBreakDown.quantity);
                                            uiBreakdownItemTemplate.attr('data-product-name', oAddonBreakDown.product_name);
                                            uiBreakdownItemTemplate.attr('data-main-product-id', oAddonBreakDown.main_product_id);
                                            uiBreakdownItemTemplate.attr('data-product-id', oAddonBreakDown.item_id);
                                            uiBreakdownItemTemplate.attr('data-product-price', oAddonBreakDown.price);
                                            uiBreakdownItemTemplate.attr('data-product-row-total', (oAddonBreakDown.price * oAddonBreakDown.quantity).toFixed(2));
                                            uiHistoryTemplate.find('tbody.cart-container').append(uiBreakdownItemTemplate);  
                                    })   
                            }

                            if(typeof(oItemBreakDown.special) != 'undefined')
                            {
                                 $.each(oItemBreakDown.special, function(key, oSpecialItemBreakDown) {
                                            var uiBreakdownItemTemplate = uiHistoryTemplate.find('tr.product-breakdown-item.template').clone().removeClass('template');
                                            uiBreakdownItemTemplate.find('td.product-name').text(oSpecialItemBreakDown.product_name);
                                            uiBreakdownItemTemplate.find('td.product-quantity').text(oSpecialItemBreakDown.quantity);
                                            uiBreakdownItemTemplate.find('td.product-price').text(oSpecialItemBreakDown.price + ' PHP');
                                            uiBreakdownItemTemplate.find('td.product-total').text(oSpecialItemBreakDown.sub_total + ' PHP');
                                            uiBreakdownItemTemplate.attr('data-product-quantity', oSpecialItemBreakDown.quantity);
                                            uiBreakdownItemTemplate.attr('data-product-name', oSpecialItemBreakDown.product_name);
                                            uiBreakdownItemTemplate.attr('data-main-product-id', oSpecialItemBreakDown.main_product_id);
                                            uiBreakdownItemTemplate.attr('data-product-id', oSpecialItemBreakDown.item_id);
                                            uiBreakdownItemTemplate.attr('data-special-product-id', oSpecialItemBreakDown.item_id);
                                            uiBreakdownItemTemplate.attr('data-product-price', oSpecialItemBreakDown.price);
                                            uiBreakdownItemTemplate.attr('data-product-row-total', (oSpecialItemBreakDown.price * oSpecialItemBreakDown.quantity).toFixed(2));
                                            uiBreakdownItemTemplate.attr('data-special-item', 1);
                                            uiHistoryTemplate.find('tbody.cart-container').append(uiBreakdownItemTemplate);  
                                    })   
                            }

                            if(typeof(oItemBreakDown.upgrades) != 'undefined')
                            {
                                 $.each(oItemBreakDown.upgrades, function(key, oUpgradesItemBreakDown) {
                                            var uiBreakdownItemTemplate = uiHistoryTemplate.find('tr.product-breakdown-item.template').clone().removeClass('template');
                                            uiBreakdownItemTemplate.find('td.product-name').text(oUpgradesItemBreakDown.product_name);
                                            uiBreakdownItemTemplate.find('td.product-quantity').text(oUpgradesItemBreakDown.quantity);
                                            uiBreakdownItemTemplate.find('td.product-price').text(oUpgradesItemBreakDown.price + ' PHP');
                                            uiBreakdownItemTemplate.find('td.product-total').text(oUpgradesItemBreakDown.sub_total + ' PHP');
                                            uiBreakdownItemTemplate.attr('data-product-quantity', oUpgradesItemBreakDown.quantity);
                                            uiBreakdownItemTemplate.attr('data-product-name', oUpgradesItemBreakDown.product_name);
                                            uiBreakdownItemTemplate.attr('data-main-product-id', oUpgradesItemBreakDown.main_product_id);
                                            uiBreakdownItemTemplate.attr('data-product-id', oUpgradesItemBreakDown.item_id);
                                            uiBreakdownItemTemplate.attr('data-upgrade-product-id', oUpgradesItemBreakDown.item_id);
                                            uiBreakdownItemTemplate.attr('data-product-price', oUpgradesItemBreakDown.price);
                                            uiBreakdownItemTemplate.attr('data-product-row-total', (oUpgradesItemBreakDown.price * oUpgradesItemBreakDown.quantity).toFixed(2));
                                            uiBreakdownItemTemplate.attr('data-upgrade', 1);
                                            uiHistoryTemplate.find('tbody.cart-container').append(uiBreakdownItemTemplate);  
                                    })   
                            }
                            
                        }


                    })
                }

                uiHistoryTemplate.find('td.total-vat').text(parseFloat(oDisplayData.added_vat).toFixed(2) + ' PHP');
                uiHistoryTemplate.find('td.delivery-charge').text(parseFloat(oDisplayData.delivery_charge).toFixed(2) + ' PHP');
                uiHistoryTemplate.find('strong.total-bill').text(parseFloat(oDisplayData.total_bill).toFixed(2) + ' PHP');
                uiHistoryTemplate.find('td.total-cost').text(parseFloat(oDisplayData.total_cost).toFixed(2) + ' PHP');

                uiHistoryTemplate.attr('data-order-id', oOrders.id)
                uiContainer.find('div.order_history_container').append(uiHistoryTemplate);

            }
        },

        'cajax': function (oAjaxConfig) {
            if (typeof(oAjaxConfig) !== 'undefined') {
                callcenter.CconnectionDetector.ajax(oAjaxConfig);
            }
        },

        'content_panel_toggle': function (uiString) {
            $('section#add_new_customer_header').addClass('hidden')
            $('section#order-process').children('div.row').addClass('hidden')
            var uiContents = $('section[section-style="content-panel"]').children('div.row');
            var uiNavs = $('ul.main_nav li');
            var uiSubNav = $('div.sub-nav.' + uiString);
            //////console.log(uiContents)
            $.each(uiNavs, function () {
                $(this).removeClass('active')

                if ($(this).hasClass(uiString)) {
                    $(this).addClass('active');
                }
            })

            $.each(uiContents, function () {
                $('section#order-process').hide();
                $('section#' + uiString + '_form_container').removeClass('hidden');
                $('section#' + uiString + '_list_container').removeClass('hidden');
            });

            if (uiSubNav.length > 0) {
                $('div.sub-nav').hide();
                uiSubNav.show();
            }

        },

        'sub_content_panel_toggle': function (uiString) {
            //$('section').hide();
            $('section.store_management').addClass('hidden');
            $('section#' + uiString + '_form_container').removeClass('hidden');
            $('section#' + uiString + '_list_container').removeClass('hidden');

            if (uiString == 'archive_order') {
                $('section#' + uiString + '_form_container').find('form-group.' + uiString).removeClass('hidden');
            }
            else {
                $('section#' + uiString + '_form_container').find('form-group.' + uiString).addClass('hidden');
            }
        },

        'assemble_address_information': function () {
            var uiAddressAddForm = $("div[modal-id='update-address']").find('form#coordinator_update_address');
            var oParams = {
                "params": {
                    "id"     : $("div[modal-id='update-address']").attr('data-customer-id'),
                    "address": {
                        /*"address_id" : $('#add_address input[name="address_id"]').val(),*/
                        "address_type" : uiAddressAddForm.find('input[name="address_type"]:visible').val(),
                        "address_label": uiAddressAddForm.find('input[name="address_label"]:visible').val(),
                        "house_number" : uiAddressAddForm.find('input[name="house_number"]:visible').val(),
                        "street"       : uiAddressAddForm.find('input[name="street"]:visible').attr("value"),
                        "second_street": uiAddressAddForm.find('input[name="second_street"]:visible').attr("value"),
                        "subdivision"  : uiAddressAddForm.find('input[name="subdivision"]:visible').attr("value"),
                        "barangay"     : uiAddressAddForm.find('input[name="barangay"]:visible').attr("value"),
                        "city"         : uiAddressAddForm.find('input[name="city"]:visible').attr("value"),
                        "province"     : uiAddressAddForm.find('input[name="province"]:visible').attr("value"),
                        "building"     : uiAddressAddForm.find('input[name="building"]:visible').attr("value"),
                        "floor"        : uiAddressAddForm.find('input[name="floor"]:visible').val(),
                        "unit"         : uiAddressAddForm.find('input[name="unit"]:visible').val(),
                        "landmark"     : uiAddressAddForm.find('input[name="landmark"]:visible').val(),
                        "is_default"   : 0
                    }
                }
            };

            var oAjaxConfig = {
                "type"   : "Post",
                "data"   : oParams,
                "headers": {"X-API-KEY": "IFYRtW2cb7A5Gs54A1wKElECBL65GVls"},
                "url"    : callcenter.config('url.api.jfc.customers') + "/customers/add_address",
                "success": function (oData) {
                    if (typeof(oData) !== 'undefined') {
                        if (oData.status == false && count(oData.message) > 0) //failed
                        {
                            var uiContainer = $('div.update-address-error-msg');
                            var sError = '';
                            $.each(oData.message, function (k, v) {
                                sError += '<p class="error_messages"><i class="fa fa-exclamation-triangle"></i> ' + v + '</p>';
                            });
                            callcenter.call_orders.add_error_message(sError, uiContainer);
                        }
                        else {
                            var house_number    = uiAddressAddForm.find('input[name="house_number"]:visible').val(),
                                building        = uiAddressAddForm.find('input[name="building"]:visible').val(),
                                floor           = uiAddressAddForm.find('input[name="floor"]:visible').val(),
                                street          = uiAddressAddForm.find('input[name="street"]:visible').val(),
                                second_street   = uiAddressAddForm.find('input[name="second_street"]:visible').val(),
                                barangay        = uiAddressAddForm.find('input[name="barangay"]:visible').val(),
                                subdivision     = uiAddressAddForm.find('input[name="subdivision"]:visible').val(),
                                city            = uiAddressAddForm.find('input[name="city"]:visible').val(),
                                province        = uiAddressAddForm.find('input[name="province"]:visible').val();

                            var full_address = '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' '+ second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province;

                            $('button.confirm_update_address').attr('address_id', oData.data.address_id);
                            callcenter.coordinator_management.populate_address(oData.data.address, oData.data.address_text, oParams.params.id);
                            callcenter.coordinator_management.update_address_deliver_to_different($('button.confirm_update_address'), full_address);

                            // $('div[modal-id="update-address"]').removeClass('showed');
                            // $('html, body').css('overflowY', 'auto');
                        }
                        callcenter.call_orders.show_spinner($('button.confirm_update_address'), false);
                    }
                }
            };

            callcenter.call_orders.get(oAjaxConfig)
        },

        'populate_address': function (v, address_text, customer_id) {
            if(typeof(v) !== 'undefined' && typeof(address_text) !== 'undefined' && typeof(customer_id) !== 'undefined')
            {
                var uiTemplate = $('div.search_result_container.template').clone().removeClass('template');
                ////console.log(v);
                ////console.log(address_text);
                var house_number = (v.house_number !== '' && v.house_number != "0") ? v.house_number : '',
                    floor = (v.floor !== '' && v.floor != "0") ? v.floor : '',
                    building = (address_text.building !== null) ? address_text.building.name : '',
                    barangay = (address_text.barangay !== null) ? address_text.barangay.name : '',
                    subdivision = (address_text.subdivision !== null) ? address_text.subdivision.name : '',
                    city = (address_text.city !== null) ? address_text.city.name : '',
                    street = (address_text.street !== null) ? address_text.street.name : '',
                    second_street = (address_text.second_street !== null && typeof(address_text.second_street) != 'undefined') ? address_text.second_street.name : '',
                    province = (address_text.province !== null) ? address_text.province.name : '';

                var uiAddressTemplate = uiTemplate.find('div.customer_address.template').clone().removeClass('template');
                uiAddressTemplate.attr('address_id', v.id);
                uiAddressTemplate.find('strong.info_address_label').text(v.address_label);
                uiAddressTemplate.find('p.info_address_complete').text('' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);
                uiAddressTemplate.find('p.info_address_complete').attr('data-address', '/##/' + house_number + '/##/' + building + '/##/' + floor + '/##/' + street + '/##/' + second_street + '/##/' + barangay + '/##/' + subdivision + '/##/' + city + '/##/' + province);
                uiAddressTemplate.find('strong.info_address_type').text(v.address_type.toUpperCase());
                uiAddressTemplate.find('p.info_address_landmark').text('- ' + v.landmark.toUpperCase());

                uiAddressTemplate.attr('house_number', v.house_number);
                uiAddressTemplate.attr('building', building);
                uiAddressTemplate.attr('floor', v.floor);
                uiAddressTemplate.attr('street', v.street);
                uiAddressTemplate.attr('second_street', v.second_street);
                uiAddressTemplate.attr('barangay', v.barangay);
                uiAddressTemplate.attr('subdivision', v.subdivision);
                uiAddressTemplate.attr('city', v.city);
                uiAddressTemplate.attr('province', v.province);
                uiAddressTemplate.attr('all_address', '' + house_number + ' ' + building + ' ' + floor + ' ' + street + ' ' + second_street + ' ' + barangay + '  ' + subdivision + ' ' + city + ' ' + province);

                if (v.address_type.length > 0) {
                    uiAddressTemplate.find('img.info_address_type').attr('src', callcenter.config('url.server.base') + 'assets/images/' + v.address_type.toLocaleLowerCase() + '.png');
                    uiAddressTemplate.find('img.info_address_type').attr('onError', '$.fn.checkImage(this);');
                }

                if (v.is_default == 1) {
                    // uiAddressTemplate.removeClass('white').addClass('bggray-light')
                    // $('div.search_result_container[data-customer-id="' + customer_id + '"]').find('div.customer_secondary_address').toggle();
                    // var oOldPrimary = $('div.search_result_container[data-customer-id="' + customer_id + '"]').find('div.customer_address_container').find('div.customer_address:visible').clone();
                    // oOldPrimary.find('div.edit_container').remove();
                    // oOldPrimary.removeClass('bggray-light').addClass('white');
                    // $('div.search_result_container[data-customer-id="' + customer_id + '"]').find('div.customer_address_container').find('div.customer_address:visible').replaceWith(uiAddressTemplate);
                    // $('div.search_result_container[data-customer-id="' + customer_id + '"]').find('div.customer_secondary_address').prepend(oOldPrimary)
                }
                else {
                    uiAddressTemplate.find('div.edit_container').remove();
                    $('div.order_search_result_block[data-customer-id="' + customer_id + '"]').find('div.customer_secondary_address').prepend(uiAddressTemplate)
                }
            }
        },

    }

}());

$(window).load(function () {
    callcenter.coordinator_management.initialize();
});

function Timer(time, bIsCountDown, cbEverySecond, cbOnStop, otherData) {
    "use strict";

    var cThis = this,
        oMomentDuration, hours, minutes, seconds, nTimer,
        sHours = '00', sMinutes = '00', sSeconds = '00',
        sTimerType;

    /**
     * Initializes the moment duration and other settings
     */
    function initializeDuration() {
        // assigning default if certain variable is not set properly
        time = (time !== undefined) ? time : '00:00:00';
        bIsCountDown = (bIsCountDown !== undefined) ? bIsCountDown : false;
        cbEverySecond = (cbEverySecond !== undefined && typeof cbEverySecond === 'function') ? cbEverySecond : logger;
        cbOnStop = (cbOnStop !== undefined && typeof  cbOnStop === 'function' && bIsCountDown) ? cbOnStop : logger;

        sTimerType = (bIsCountDown) ? 'subtract' : 'add';

        oMomentDuration = moment.duration(time);
    }
    initializeDuration();

    /**
     * If cbEverySecond is not set this is the default callback
     */
    function logger() {
        //console.log(sHours + ':' + sMinutes + ':' + sSeconds);
    }

    /**
     * Sets the time of the timer
     *  When called it will override the instantiated time
     *
     * @param {String} time
     */
    this.set_time = function (newTime) {
        time = newTime;
    };

    /**
     * Returns the current hours
     * Format hh. Example 56
     *
     * @return {String}
     */
    this.get_hours = function () {
        return sHours;
    };

    /**
     * Returns the current minutes
     * Format mm. Example 10
     *
     * @return {String}
     */
    this.get_minutes = function () {
        return sMinutes;
    };

    /**
     * Returns the current seconds
     * Format mm. Example 10
     *
     * @return {String}
     */
    this.get_seconds = function () {
        return sSeconds;
    };

    /**
     * Returns the current duration of the time
     * Format hh:mm:ss. Example 10:59:10
     *
     * @return {String}
     */
    this.get_duration = function () {
        return sHours + ':' + sMinutes + ':' + sSeconds;
    };

    /**
     * Starts the timer
     */
    this.start = function () {
        nTimer = setInterval(function () {
            oMomentDuration[sTimerType](1, 's');

            hours = oMomentDuration.hours();
            minutes = oMomentDuration.minutes();
            seconds = oMomentDuration.seconds();

            sHours = hours < 10 ? "0" + hours : hours;
            sMinutes = minutes < 10 ? "0" + minutes : minutes;
            sSeconds = seconds < 10 ? "0" + seconds : seconds;

            if(hours <= 0 && minutes <= 0 && seconds <=0) {
                sHours = sMinutes = sSeconds = '00';
                cThis.stop();
            }

            cbEverySecond(sHours, sMinutes, sSeconds, otherData);
        }, 1000);
    };

    /**
     * Stops the timer
     */
    this.stop = function () {
        clearInterval(nTimer);
        cbOnStop(otherData);
    };

    /**
     * Resets the timer. This also calls the stop function
     *   If you want to start again the timer just call the start function
     */
    this.reset = function () {
        clearInterval(nTimer);
        oMomentDuration = moment.duration(time);
    };
}

