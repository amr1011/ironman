/*

*  PAGINATION FOR REPORTS

*/

(function(){

	function ReportPagination()
	{
		var oItems = {};
		
		function makeid()
		{
			var text = "";
			var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

			for( var i=0; i < 5; i++ )
				text += possible.charAt(Math.floor(Math.random() * possible.length));

			return text;
		}

		var generateList = function(totalPages){
			var sListHtml = "";

			if(totalPages == 1){
				sListHtml += '<li><a><i class="fa fa-angle-left"></i></a></li>';
				sListHtml += '<li><a class="active" href="javascript:void(0)">1</a></li>';
				sListHtml += '<li><a><i class="fa fa-angle-right"></i></a></li>';
			}else{
				for(var i = 1; i <= totalPages; i++){
					if(i == 1){
						sListHtml += '<li><a paginate-page="previous" href="javascript:void(0)"><i class="fa fa-angle-left"></i></a></li>';
					}
					
					if(i > 1 || i != totalPages){
						sListHtml += '<li><a paginate-page="'+i+'" href="javascript:void(0)">'+(i)+'</a></li>';
					}
					
					if(i == totalPages){
						sListHtml += '<li><a paginate-page="next" href="javascript:void(0)"><i class="fa fa-angle-right"></i></a></li>';
					}

					
				}
			}
			
			return sListHtml;
		};
		
		function getItemsPerPage(id, perpage, index){
			var items = oItems[id], array_items = [], response = {},
				from = 0, to = perpage;
			for(var i in items){
				array_items.push(items[i]);
			}
			
			if(index > 1){
				from = (index - 1) * perpage;
				to = index * perpage;
			}

			var extractedItems = array_items.splice(from, perpage);
			for(var i in extractedItems){
				response[i] = extractedItems[i];
			}

			return response;
		}
		
		function getActiveIndex(id){
			var sIndex = $('#'+id).find('a.active').html();
			return parseInt(sIndex);
		}

		function activateAction(action, id, perpage){
			var index = getActiveIndex(id);
			var items = getItemsPerPage(id, perpage, index);
			action(items);
		}

		this.paginate = function(ui, perpage, data, action, sPeriodType){
			var id = makeid();
			if(sPeriodType == 'daily' || sPeriodType == '' || !sPeriodType ){
				var total = Object.keys(data).length;
				var perpage = perpage;
				var totalPages = Math.ceil(total / perpage);

				if(total == 0) return;

				oItems[id] = data;

				var ul = $('<ul></ul>');
				ul.append(generateList(totalPages));
				ul.wrap('<div id="'+id+'" class="paganation"></div>');
				ui.html(ul.closest('.paganation'));

				reportPagination.activateClickEvent(id, action, perpage, totalPages);
				activateAction(action, id, perpage);
			}
			if(sPeriodType == 'weekly'){
				var oData = seperateDataWeekly(data);
				generateFilteredDisplay(oData, id, action, ui);
			}
			if(sPeriodType == 'monthly'){
				var oData = seperateDataMonthly(data);
				generateFilteredDisplay(oData, id, action, ui);
			}
		}
		

		function seperateDataWeekly(data){
			var response = {};
			for(var i in data){
				var weekofYear = moment(data[i]['date']).format('ww');
				if(!response[weekofYear]){
					response[weekofYear] = {};
				}
				response[weekofYear][i] = data[i];
			}
			return response;
		}

		function seperateDataMonthly(data){
			var response = {};
			for(var i in data){
				var monthoOfYear = moment(data[i]['date']).format('MM');
				if(!response[monthoOfYear]){
					response[monthoOfYear] = {};
				}
				response[monthoOfYear][i] = data[i];
			}
			return response;
		}
		
		function generateFilteredDisplay(oData, id, action, ui){
			var ul = $('<ul></ul>');
				ul.append(generateList(Object.keys(oData).length));
				ul.wrap('<div id="'+id+'" class="paganation"></div>');
				ui.html(ul.closest('.paganation'));
			
			$('#'+id).on('click', 'a[paginate-page]', function(e){
				var uiThis = $(e.target),
					currentIndex =  getActiveIndex(id),
					sType = uiThis.attr('paginate-page');
				
				uiThis.prop('disabled', true);
				switch(sType){
					case 'next':
						var index = currentIndex + 1;
						if(index <= Object.keys(oData).length){
							$('#'+id).find('a.active').removeClass('active');
							var uiNextIndex = $('#'+id).find('a[paginate-page="'+(index)+'"]').addClass('active');
							
							var oGroupItem = getItemViaObjectIndex(oData, index);
							if(oGroupItem){
								action(oGroupItem);
							}

						}
						break;
					case 'previous':
						var index = currentIndex - 1;
						if(index > 0){
							$('#'+id).find('a.active').removeClass('active');
							var uiNextIndex = $('#'+id).find('a[paginate-page="'+(index)+'"]').addClass('active');
							var oGroupItem = getItemViaObjectIndex(oData, index);
							if(oGroupItem){
								action(oGroupItem);
							}	
						}
						break;
					case 'skip':
						break;
					default :
						$('#'+id).find('a.active').removeClass('active');
						uiThis.addClass('active');
						var index = uiThis.html();
						var oGroupItem = getItemViaObjectIndex(oData, parseInt(index));
						if(oGroupItem){
							action(oGroupItem);
						}
						break;
				}
				uiThis.prop('disabled', false);
			});

			var oGroupItem = getItemViaObjectIndex(oData, 1);
			if(oGroupItem){
				action(oGroupItem);
			}
			
		}

		function getItemViaObjectIndex(oData, index){
			var counter = 1;
			for(var i in oData){
				if(counter == index){
					return oData[i];
				}
				counter = counter + 1;
			}
			return false;
		}

		this.activateClickEvent = function(id, action, perpage, totalPages){
			$('#'+id).on('click', 'a[paginate-page]', function(e){
				var uiThis = $(e.target),
					currentIndex =  getActiveIndex(id),
					sType = uiThis.attr('paginate-page');
				
				uiThis.prop('disabled', true);
				switch(sType){
					case 'next':
						var index = currentIndex + 1;
						if(index <= totalPages){
							$('#'+id).find('a.active').removeClass('active');
							var uiNextIndex = $('#'+id).find('a[paginate-page="'+(index)+'"]').addClass('active');
							activateAction(action, id, perpage);
						}
						break;
					case 'previous':
						var index = currentIndex - 1;
						if(index > 0){
							$('#'+id).find('a.active').removeClass('active');
							var uiNextIndex = $('#'+id).find('a[paginate-page="'+(index)+'"]').addClass('active');
							activateAction(action, id, perpage);	
						}
						break;
					case 'skip':
						break;
					default :
						$('#'+id).find('a.active').removeClass('active');
						uiThis.addClass('active');
						activateAction(action, id, perpage);
						break;
				}
				uiThis.prop('disabled', false);
			})
		}
	}


	if(!window.reportPagination){
		window.reportPagination = new ReportPagination();
	}

}());