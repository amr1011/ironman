/**
 *	The JS file that will handle the events for the stores websocket integration.
 *
 */
(function(){

	oPusher.connection.bind('error', function (err){
		// console.log(err);
		if(typeof(err.error.data) != 'undefined' && err.error.data.hasOwnProperty('code'))
		{
			// console.log(typeof(err.error.data))
			if(err.error.data.code == 1006)
			{
				// alert('Disconnected from the internet.');
			}
		}
	});

	oPusher.connection.bind('state_change', function (states) {
		oPusherStatus = states.current;
		console.log(states);
	
		if(states.current == 'unavailable' || states.current == 'failed')
		{
			oPusherStatus = 'No connection';
		}
	});

	oPusher.connection.bind('connected', function () {
		// console.log('connected');
	});

	oPusher.connection.bind('disconnected', function () {
		// console.log('disconnected');
	});

	oPusher.connection.bind('connecting_in', function (delay) {
		// console.log('connecting_in');
		// console.log(delay);
	});

	oPusher.bind('pusher:subscription_error', function(status) {
	  if(status == 408 || status == 503){
	    // retry?
	  }
	  console.log(status);
	});
//	var
		//sPresenceChannel = 'presence-2',
		//sPrivateChannel = 'private-2';
        //sPresenceChannel = 'presence-' + sStoreCode,
		//sPrivateChannel = 'private-' + sStoreCode;



	/* event for success subscription */
	//oSock.bind({
	//	'event'    : 'pusher:member_added',
	//	'channel'  : sPrivateChannel,
	//	'callback' : function(oData)
	//	{
	//		console.log("member added");
	//	}
	//});
	//
	///* for offline of store */
	//oSock.bind({
	//	'event'    : 'pusher:member_removed',
	//	'channel'  : sPresenceChannel,
	//	'callback' : function(oData)
	//	{
	//		console.log("member removed");
	//	}
	//});
	
	/* will receive the callcenter order */
// 	oSock.bind({
// 		'event'    : 'receive_order',
// 		'channel'  : sPrivateChannel,
// 		'callback' : function(oOrderData)
// 		{
// 			alert('here');
// 			var uiContainer = $('section#search_order_list_container').find('div.search_result_container');
//             var uiTemplate = $('div.order_search_result_block.template').clone().removeClass('template');
//             //console.log(JSON.stringify(order));
//             var oDisplayData = $.parseJSON(oOrderData);
//             console.log(oDisplayData)
//             callcenter.coordinator_management.manipulate_template(uiTemplate, oDisplayData);
//             uiContainer.append(uiTemplate);
            
// 		}				
// 	});

	///* will receive the callcenter order */
	//oSock.bind({
	//	'event'    : 'update_order',
	//	'channel'  : sPrivateChannel,
	//	'callback' : function(oData)
	//	{
	//		console.log(oData);
	//	}
	//});
})()