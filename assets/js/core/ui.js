/*
 * "Giraffe Tools" - cr8vwebsolutions's JS framework
 * ui.js - an extension that handles all interface-related functions
 *
 * Version: 0.0.2
 * Date Started: March 7, 2013
 * Last Update: March 7, 2013
 *
 * Copyright (c) 2013 Chuck Cerrillo (chuck@cr8vwebsolutions.com)
 * JSLint Valid (http://www.jslint.com/)
 *
 */
/*jslint plusplus: true, evil: true */
/*global jQuery:true */

(function () {
	"use strict";
	function single_download (oItem) {
		var oConfig = {},
			sTemplate = '',
			uiContainer = {}
			;
		if (oItem) {
			if (oItem.template && oItem.template.length > 0) {
				sTemplate = oItem.template;
			}
			if (oItem.container && oItem.container.length > 0) {
				uiContainer = oItem.container;
			}
			if (sTemplate.length > 0 && uiContainer.length > 0) {
				oConfig = {
					url: cr8v_platform.config('url.server.ui'),
					data: {
						template: sTemplate
					},
					success: function(sData){
						var oData = validate_json(sData);
						if(oData){
							if (oData.status) {
								if (oData.html && oData.html.length > 0) {
									uiContainer.append(oData.html);
								}
								if (typeof(oItem.success) === 'function') {
									oItem.success(oData);
								}
							} else {
								if (typeof(oItem.failure) === 'function') {
									oItem.failure(oData);
								}
							}
						} else {
							if (typeof(oItem.failure) === 'function') {
								oItem.failure(oData);
							}
						}
					}
				};
				cr8v_platform.ajax.run(oConfig);
			}
		}
	}
	function multiple_download (arItems){
		var oRow = {};
		if (arItems && typeof(arItems) === 'object' && arItems.length > 0) {
			oRow = arItems.splice(0,1);
			oRow.success = function () {
				multiple_download(arItems);
			}
			cr8v.ui.download(oRow);
		}
	}
	/* Extend the original platform class and add AJAX-related functionality */
	CPlatform.prototype.ui = {
		/*
			DOWNLOAD
			The download function triggers an AJAX request that will be used to request for HTML
			data from the server.
		*/
		download : function (oSettings) {
			if (oSettings) {
				if (typeof(oSettings) === 'object' && oSettings.length > 0) {
					multiple_download(oSettings);
				} else {
					single_download(oSettings);
				}
			}
		},
		script: function (oSettings) {
			if (oSettings) {
				if (oSettings.path && oSettings.path.length > 0) {
					$('body').append('<script type="text/javascript" src="' + oSettings.path + '"></script>');
				}
			}
		},
		css: function (oSettings) {
			if (oSettings) {
				if (oSettings.path && oSettings.path.length > 0) {
					$('body').append('<link rel="Stylesheet" type="text/css" href="' + oSettings.path + '" />');
				}
			}
		},
		clone: function (oSettings) {
			var uiContainer = {},
				uiTemplate = {},
				uiObject = false,
				sPosition = 'append';
			if (oSettings) {
				if (
					oSettings.template && oSettings.template.length === 1 &&
					oSettings.target && oSettings.target.length > 0
				) {
					if (oSettings.position) {
						if (oSettings.position === 'append' ||
							oSettings.position === 'before' ||
							oSettings.position === 'after') {
							sPosition = oSettings.position;
						}
					}
					uiContainer = oSettings.target;
					uiTemplate = oSettings.template;
					uiObject = uiTemplate
								.clone()
								.removeAttr('data-template');
					if (sPosition === 'append') {
						uiObject.appendTo(uiContainer);
					} else if (sPosition === 'before') {
						uiObject.insertBefore(uiContainer);
					} else if (sPosition === 'after') {
						uiObject.insertAfter(uiContainer);
					}
				}
			}
			if (oSettings && typeof(oSettings.success) === 'function') {
				oSettings.success(uiObject);
			} else {
				return uiObject;
			}
		}
	};
}());