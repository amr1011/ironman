/*
 * "Giraffe Tools" - cr8vwebsolutions's JS framework
 * platform.js - handles low level platform functions
 *
 * Version: 0.0.4
 * Date Started: October 26, 2012
 * Last Update: March 11, 2013
 *
 * Copyright (c) 2013 Chuck Cerrillo (chuck@cr8vwebsolutions.com)
 * JSLint Valid (http://www.jslint.com/)
 *
 */
/*jslint plusplus: true, evil: true */
/*global jQuery:true */

var oLocalData = {};

function CPlatform() {
    "use strict";
    //var oLocalData = {},
    var iSizeLimit = 9999999;


    var config = {};
    // Assign Config
    // This function performs the adding new config data into the system
    function assign_config(keys, value) {
        var x = 0,
            y,
            path = '',
            configuration = config,
            test = {};
        if (keys && keys.length > 0) {
            // Traverse through each segment and determine if that segment already exists in config
            for (x = 0; x < keys.length; x++) {
                path += '["' + keys[x] + '"]';
                eval('test = config' + path);
                // Otherwise add it as a blank object
                if (typeof(test) == 'undefined') {
                    eval('config' + path + ' = {};');
                }
            }
            eval('config' + path + ' = "' + value + '";');
            return true;
        }
        return false;
    }

    /*
     CONFIG
     Performs config traversal and assignment.
     */
    function configuration(sKey, sValue) {
        var oKeywords = {};
        // Check if the provided key is actually an object
        if (typeof(sKey) === 'object') {
            // It's an object, which could only mean that it's a multiple-assignment
            oKeywords = sKey;
            $.each(oKeywords, function (sKey, sValue) {
                // Explode the key to get the config tree
                var arKey = sKey.split('.'),
                    x = 0,
                    y = 0,
                    configuration,
                    keys = [];
                // Check if the array of keys isn't empty
                if (arKey.length > 0) {
                    for (x = 0; x < arKey.length; x++) {
                        if (arKey[x] && arKey[x].length > 0) {
                            if (y > 0) {
                                if (configuration[arKey[x]]) {
                                    configuration = configuration[arKey[x]];
                                    y++;
                                } else {
                                    configuration = '';
                                }
                            } else {
                                if (config[arKey[x]]) {
                                    configuration = config[arKey[x]];
                                    y++;
                                } else {
                                    configuration = '';
                                }
                            }
                        }
                        keys.push(arKey[x]);
                    }
                } else {
                    // If it is empty, just copy it over
                    configuration = arKey[0];
                }
                // Let's just make sure a value is provided, this means that there's an
                // intention of updating the stored value.
                if (sValue && sValue !== 'undefined' && sValue.length >= 0) {
                    assign_config(keys, sValue);
                }
            });
            // It's not an object, so it's likely a string
        } else {
            if (sKey && sKey.length > 0) {
                // Explode the key to get the config tree
                var arKey = sKey.split('.'),
                    x = 0,
                    y = 0,
                    configuration,
                    keys = [];
                // Check if the array of keys isn't empty
                if (arKey.length > 0) {
                    for (x = 0; x < arKey.length; x++) {
                        if (arKey[x] && arKey[x].length > 0) {
                            if (y > 0) {
                                if (configuration[arKey[x]]) {
                                    configuration = configuration[arKey[x]];
                                    y++;
                                } else {
                                    configuration = '';
                                }
                            } else {
                                if (config[arKey[x]]) {
                                    configuration = config[arKey[x]];
                                    y++;
                                } else {
                                    configuration = '';
                                }
                            }
                        }
                        keys.push(arKey[x]);
                    }
                } else {
                    // If it is empty, just copy it over
                    configuration = arKey[0];
                }
                // Check if a value parameter is provided and not empty
                if (sValue && sValue !== 'undefined' && sValue.length >= 0) {
                    // Then assign it to the specified key
                    assign_config(keys, sValue);
                    // This allows us to pass the node's assigned value as the return value of the function
                    configuration = sValue;
                }
                return configuration;
            }
        }
        return config;
    }

    // System.config allows you to retrieve or set the value of a
    // custom config variable.
    // Usage:
    //    System.config(name) = returns value of "name" config
    //    System.config(name,value) = sets the value of "name" config to "value"

    this.config = function (sKey, sValue) {
        return configuration(sKey, sValue);
    };

    /**
     * @method_id:  Jspl04
     * @param : { sType(string), qualifiers(object) }
     * @description : el form validation
     * @developer : Lorenz
     * @dependency : integr8validation.js
     *
     * */

    this.validate_form = function (uiForm, oFormConfig) {
        if (typeof(uiForm) != 'undefined' && Object.getOwnPropertyNames(oFormConfig).length != 0) {
            uiForm.cr8vformvalidation(oFormConfig);
        }
    };

    /**
     * @method_id:  Jspl01
     * @param : { sType(string), oData(object)
     * @description : The function that will be used to add data in the local storage and will also be used to categorize the data entries.
     *   Before adding new data entry, this must check for the limit of the local storage so it can delete the old data before inserting new record
     * @developer : Lorenz
     *
     * */

    this.add_to_storage = function (sType, oData) {
        if (sType.length > 0 && typeof(sType) == 'string' && Object.getOwnPropertyNames(oData).length !== 0) {
            var sNewData = JSON.stringify(oData); //length of new data to be added
            var sLocalDataType = JSON.stringify(oLocalData);
            var iNewDataLength = sNewData.length + sLocalDataType.length;
            if (typeof(oLocalData[sType]) == 'undefined') {
                oLocalData[sType] = [];
            }

            if (iNewDataLength < iSizeLimit) {
                oLocalData[sType].push(oData);
            }

            else {
                cr8v.pop(sType); //this will delete index zero of the localstorage stype
                oLocalData[sType] = oData;

            }

            //console.log(oLocalData[sType]);
        }
        else {
            alert('params error');
        }
    }

    /**
     * @method_id:  Jspl02
     * @param : { none }
     * @description : The function that will check the limit of the local storage data.
     * @developer : Lorenz
     *
     * */

    this.check_limit = function () {
        return iSizeLimit;
    }

    /**
     * @method_id:  Jspl03
     * @param : { sType(string) }
     * @description : The function that will be used to delete the old data in the specified data type.
     * @developer : Lorenz
     *
     * */

    this.pop = function (sType) {
        if (sType.length > 0 && typeof(sType) == 'string') {
            oLocalData[sType].splice(0, 1);  //delete stype data index 0 array element, first in first out
        }
        else {
            alert('params error');
        }
    }

    /**
     * @method_id:  Jspl03
     * @param : { sType(string) }
     * @description : The function that will be used to get the record from the local storage based on the given record type and qualifiers.
     * @developer : Lorenz
     *
     * */

    this.get_from_storage = function (sType, oQualifiers) {
        if (sType.length > 0 && typeof(sType) != 'undefined') {
            if (typeof(oQualifiers) == 'undefined') //if oQualifiers is not passed
            {
                if(typeof(oLocalData[sType]) != 'undefined')
                {
                    return oLocalData[sType][0];
                }

            }
            else {

                var sFilter = 'oLocalData[sType].filter(function (el) { return ';
                $.each(oQualifiers, function (k, v) {
                    console.log(v);
                    sFilter += 'el["' + v.key + '"] ' + v.operator + ' "' + v.value + '"';
                    if ((k + 1) != oQualifiers.length) {
                        sFilter += ' && ';
                    }
                })
                sFilter += '});';

                var FilteredData = eval(sFilter);
                return FilteredData;
            }
        }
        else {
            alert('params error');
        }
    }


}
var cr8v_platform = new CPlatform(),
    cr8v          = cr8v_platform,
    GT            = cr8v,
    admin    = cr8v;