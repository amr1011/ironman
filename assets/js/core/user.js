/*
 * "Giraffe Tools" - cr8vwebsolutions's JS framework
 * user.js - an extension that handles all user-account-related functions
 *
 * Version: 0.0.2
 * Date Started: March 6, 2013
 * Last Update: March 7, 2013
 *
 * Copyright (c) 2013 Chuck Cerrillo (chuck@cr8vwebsolutions.com)
 * JSLint Valid (http://www.jslint.com/)
 *
 */
/*jslint plusplus: true, evil: true */
/*global jQuery:true */

(function () {
	"use strict";
	/* Extend the original platform class and add user-related functionality */
	CPlatform.prototype.user = {
		/*
			AUTHENTICATE
			This function is used to check if the user still has a valid session in the application server.
		*/
		authenticate: function (oSettings) {
			var oConfig = {
				data : {
					session_id: '',
					location: ''
				},
				jsonp: true,
				url: cr8v_platform.config('url.server.authenticate'),
				success: function (oData) {
					if (oData) {
						if (oData.status) {
							if (typeof(oSettings.success) === 'function') {
								oSettings.success(oData);
							}
						} else {
							if (typeof(oSettings.failure) === 'function') {
								oSettings.failure(oData);
							}
						}
					} else {
						if (typeof(oSettings.failure) === 'function') {
							oSettings.failure({
								status: false,
								message: 'Invalid data format.'
							});
						}
					}
				}
			}
			if (oSettings) {
				if (oSettings.session_id && oSettings.session_id.length > 0) {
					oConfig.data.session = oSettings.session_id;
				}
				if (oSettings.api_key && oSettings.api_key.length > 0) {
					oConfig.data.key = oSettings.api_key;
				}
				if (oSettings.location && oSettings.location.length > 0) {
					oConfig.data.ref = oSettings.location;
				}
			}
			cr8v_platform.ajax.run(oConfig);
		},
		/* 
			LOGIN
			The login function is used to initiate a login request to the central server. The request
			is made over JSONP. CORS has also been enabled on the server, just in case (although not
			all browsers support it yet). We're falling back to JSONP just to be safe.
		*/
		login : function (oSettings) {
			var uiIndicator = {},
				oData = {};
			if (oSettings && oSettings.data && oSettings.data.username && oSettings.data.password && oSettings.data.username.length > 0 && oSettings.data.password.length > 0) {
				if (oSettings && oSettings.indicator && oSettings.indicator.length === 1) {
					uiIndicator = oSettings.indicator;
					uiIndicator.html('Logging into application...');
				}
				// Prepare the configuration that will be sent to the ajax.run function
				var oConfig = {
						data : {
							username: oSettings.data.username,
							password: oSettings.data.password
						},
						url: cr8v_platform.config('url.server.login'),
						success : function(sData) {
							var oData = validate_json(sData),
								uiIndicator = {};
							// Update the indicator string
							if (oSettings && oSettings.indicator && oSettings.indicator.length === 1) {
								uiIndicator = oSettings.indicator;
								uiIndicator.html('Logging into central server...');
								oData.indicator = uiIndicator;
							}
							if(oData){
								if (oData.status) {
									if (typeof(oSettings.success) === 'function') {
										oSettings.success(oData);
									}
								} else {
									if (typeof(oSettings.failure) === 'function') {
										oSettings.failure(oData);
									}
								}
							} else {
								if (typeof(oSettings.failure) === 'function') {
									oSettings.failure(oData);
								}
							}
						},
						failure: function(oData) {
							alert(oData.message);
						}
					};
				// ...and off we go!
				cr8v_platform.ajax.run(oConfig);
			} else {
				if (oSettings && typeof(oSettings.failure) === 'function') {
					oData = {
						status: false,
						message: 'Invalid login'
					}
					oSettings.failure(oData);
				}
			}
		},
		/*
			LOGOUT
			The logout function triggers an AJAX request that forces the server to clear session data,
			as well as updating the database session record accordingly.
		*/
		logout : function (oSettings) {
			var oConfig = {
				jsonp: true,
				url: cr8v_platform.config('url.server.logout'),
				success: function(sData){
					var oData = validate_json(sData);
					if(oData){
						if (oData.status) {
							if (typeof(oSettings.success) === 'function') {
								oSettings.success(oData);
							}
						} else {
							if (typeof(oSettings.failure) === 'function') {
								oSettings.failure(oData);
							}
						}
					} else {
						if (typeof(oSettings.failure) === 'function') {
							oSettings.failure(oData);
						}
					}
				}
			};
			cr8v_platform.ajax.run(oConfig);
		}
	};
}());