$(window).on("load",function(){
	// accordion
	var ac = $('section[section-style~="content-panel"] .content-container-collapse');
	var c = {},acc = {},rp = {};

	ac.each(function(i){
		c[i] = $(this);
		acc[i] = 0;
		c[i].children().each(function(){
			acc[i] += $(this).outerHeight();
		});

		rp[i] = 0;
		if($(".revealed-panel").length > 0){
			rp[i] = $(".revealed-panel").outerHeight();
		}else{
			rp[i] = 70;
		}

		if(c[i].hasClass("hide-panel")){
			c[i].css({height:rp[i]});
		}else{
			c[i].css({height:acc[i]});
		}

		$(this).find('.collapse-trigger').on("click",function(){
			if(c[i].hasClass("hide-panel")){
				c[i].css({height:acc[i]});
			}else{
				c[i].css({height:rp[i]});
			}
			c[i].toggleClass("hide-panel");
		});
	});
});

$(document).ready(function(){
	// date picker
	$(".date-picker:not(#cn-report-date-from):not(#cn-report-date-to)"+
		":not(#pu-report-date-from):not(#pu-report-date-to)"+
		":not(#hourly-sales-report-date-from):not(#hourly-sales-report-date-to)"+
		":not(#psr_date_from):not(#psr_date_to)"+
		":not(#csr_date_from):not(#csr_date_to)"+
		":not(#order_reports_date_from):not(#order_reports_date_to)"+
		":not(#sales-report-date-from):not(#sales-report-date-to)"+
		":not(#dst_for_day_date_from_dp):not(#dst_for_day_date_to_dp)"+
		":not(#dst_for_store_date_from_dp):not(#dst_for_store_date_to_dp)"+
		"").datetimepicker({pickTime: false});

	//for add-core-item only
	$('div[data-condition~="exclusive-store"] input[type="checkbox"]').on("click",function(){
		if($(this).is(":checked")){
			$('div[data-condition~="no-item-cover"]').addClass("disabled");
			$('div[data-condition~="no-item-cover"] select, div[data-condition~="no-item-cover"] button').prop("disabled",true)
		}else{
			$('div[data-condition~="no-item-cover"]').removeClass("disabled");
			$('div[data-condition~="no-item-cover"] select, div[data-condition~="no-item-cover"] button').prop("disabled",false)
		}
	});

    $('body').on('click', 'div.option.custom-filter' , function() {
        if($(this).attr('data-value') != 'undefined')
        {
            $(this).parents('div.select:first').find('input[type="text"]').attr('data-value', $(this).attr('data-value'))
        }
    })

	//enabling and disabling data-inputs
	$('input[type="checkbox"].triggerer').on("click",function(){
		var classTarget = $(this).attr("content-target")
		if($(this).is(":checked")){
			$('.'+classTarget).removeClass("disabled");
			$('.'+classTarget+' input, .'+classTarget+' button').prop("disabled",false);
		}else{
			$('.'+classTarget).addClass("disabled");
			$('.'+classTarget+' input, .'+classTarget+' button').prop("disabled",true);
		}
	});

	//hide/show data-inputs
	$(this).find('[sh-toggler^="toggle"]').on("click",function(){
		var parent = $(this).closest(".content-container,.content-container-collapse");
		var toggleType = $(this).attr("sh-toggler").split(" ")[0];
		var target = $(this).attr("sh-toggler").split(" ")[1];

		if(toggleType == "toggle-text"){
			var txt1 = parent.find("[sh-item*='"+target+"'][sh-item^='toggle']").attr("sh-item").split(" ")[0] ;
			var txt2 = txt1.substr(txt1.indexOf("-")+1,txt1.length).split("/");
		}
		
		parent.toggleClass("reveal")
		if(parent.hasClass("reveal")){
			if(toggleType == "toggle-display"){
				parent.find("[sh-item*='"+target+"'][sh-item~='showed']").css({"display":"none"});
				parent.find("[sh-item*='"+target+"'][sh-item~='hidden']").css({"display":"block"});
			}else if(toggleType == "toggle-text"){
				parent.find("[sh-item*='"+target+"']").html(txt2[1]);
			}
			
		}else{
			if(toggleType == "toggle-display"){
				parent.find("[sh-item*='"+target+"'][sh-item~='showed']").attr("style","");
				parent.find("[sh-item*='"+target+"'][sh-item~='hidden']").attr("style","");
			}else if(toggleType == "toggle-text"){
				parent.find("[sh-item*='"+target+"']").html(txt2[0]);
			}
		}
	});

	//modal
	$(".modal-trigger").on("click",function(){
		$("body").css({overflow:'hidden'});
		var tm = $(this).attr("modal-target");

		$("div[modal-id~='"+tm+"']").addClass("showed");

		$("div[modal-id~='"+tm+"'] .close-me").on("click",function(){
			$("body").css({'overflow-y':'initial'});
			$("div[modal-id~='"+tm+"']").removeClass("showed");
		});

		// click outside to modal
		// $("div[modal-id~='"+tm+"']").on("mouseup",function(e){
		// 	var md = $("div[modal-id~='"+tm+"'] .modal-body")

		// 	if(!md.is(e.target) && md.has(e.target).length === 0){
		// 		$(this).removeClass("showed");
		// 	}
		// });
	});


	//list-view
	$(".list-view-toggle").keypress(function(e){
		var ohtml,lv,li,px,py;

		if($(this).val().length >= 2){
			ohtml = '<div class="list-view"></div>';

			$("body").append(ohtml);
			lv = $(".list-view");
			li = "";

			for(x = 0; x < 10; x++){
				li += 	'<div class="list-item">'+
						'<img src="assets/images/champ.png"/>'+
						'<p>Champ</p>'+
						'</div>';
			}
			lv.html(li);

			px = $(this).offset().top - $(".list-view").height();
			py = $(this).offset().left;
			lv.css({top:px,left:py});
		}
	});

	$(".list-view-toggle").blur(function(){
		$("body").find(".list-view").remove();
	});

	//item-list select
	$(".item-select").each(function(){
		var $this = $(this);
		var isCheckBox = $(this).find("input[type=checkbox]").length > 0 ?  $(this).find("input[type=checkbox]") : false;

		if(isCheckBox != false){
			isCheckBox.on("click",function(){
				$this.toggleClass("selected");
			});
		}

		$this.on("click",function(){
			$(this).toggleClass("selected");
			if(isCheckBox != false){
				isCheckBox.prop("checked", !isCheckBox.prop("checked"));
			}
		});
	});

	//gallery
	if($(".image-gallery").length > 0){
		var imgID = 0;

		$(".image-gallery .arrow-left").on("click",function(){
			$(".bullets a").each(function(){
				$(this).removeClass("active");
			});
			
			$(".img-content img.active").stop().fadeOut(300,function(){	
				imgID = $(this).attr("gallery-img");
				$(this).removeClass("active");
				$("[gallery-desc~='"+imgID+"']").fadeOut(300,function(){
					$(this).removeClass("active");
				});
			});
			if($(".img-content img.active").next().length > 0){
				$(".desc-content.active").next().delay(550).fadeIn(500,function(){
					$(this).addClass("active").css({"display":"inline-block"});	
				});
				$(".img-content img.active").next().delay(550).fadeIn(500,function(){
					$(this).addClass("active").css({"display":"inline-block"});	
				});
			}else{
				imgID = 1;
				$(".desc-content:nth-of-type(1)").delay(550).fadeIn(500,function(){
					$(this).addClass("active").css({"display":"inline-block"});	
				});
				$(".img-content img:nth-of-type(1)").delay(550).fadeIn(500,function(){
					$(this).addClass("active").css({"display":"inline-block"});
				});
			}
		});

		$(".image-gallery .arrow-right").on("click",function(){
			$(".bullets a").each(function(){
				$(this).removeClass("active");
			});

			$(".img-content img.active").stop().fadeOut(300,function(){
				imgID = $(this).attr("gallery-img");
				$(this).removeClass("active");
				$("[gallery-desc~='"+imgID+"']").fadeOut(300,function(){
					$(this).removeClass("active");
				});
			});
			if($(".img-content img.active").prev().length > 0){
				$(".desc-content.active").prev().delay(550).fadeIn(500,function(){
					$(this).addClass("active").css({"display":"inline-block"});	
				});
				$(".img-content img.active").prev().delay(550).fadeIn(500,function(){
					$(this).addClass("active").css({"display":"inline-block"});	
				});
			}else{
				$(".desc-content:last-child").delay(550).fadeIn(500,function(){
					$(this).addClass("active").css({"display":"inline-block"});	
				});
				$(".img-content img:last-child").delay(550).fadeIn(500,function(){

					$(this).addClass("active").css({"display":"inline-block"});
				});
			}
		});
	}

	/*  table parent */
	$(".table-inside tr td .chck-box").each(function() {		
		var default_color = $(this).css("background-color");
		$(this).off("click").on("click",function(){
		
			if($(this).closest('.first-td').hasClass("click-bg")){
				$(this).closest('.first-td').removeClass("click-bg");
				$(this).closest('.first-td').css({ backgroundColor: 'default-color' });
			}else{
				$(this).closest('.first-td').addClass("click-bg");
			}
		})
	});

	fots = $(".fix-on-top-scroll");
	if($("body").find(fots).length > 0){
		ot = fots.offset().top;
	}
	
    /*image 404*/
    $.fn.checkImage = function(img){
        $(img).attr({"onError":"this.onerror=null;", "src":"", "alt":"", "width": "", "height": ""});
    };
});


$(window).scroll(function(){
	st = $(window).scrollTop();
	if($("body").find(fots).length > 0){
		if(st > ot){
			fots.addClass("top");
		}else{
			fots.removeClass("top");
		}
	}
});

jQuery.fn.sortDomElements = (function() {
    return function(comparator) {
        return Array.prototype.sort.call(this, comparator).each(function(i) {
            this.parentNode.appendChild(this);
        });
    };
})();