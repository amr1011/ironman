<?php include "../construct/header.php"; ?>

	<section class="container-fluid" section-style="top-panel">
		<div class="row header-container">
			<div class="contents">
				<h1 class="f-left">Search Customer</h1>
				<div class="f-right">
					<button class="btn btn-light margin-top-20 margin-right-10">Add New Customer</button>
					<button class="btn btn-dark margin-top-20 margin-right-10">Skip to Cart</button>
					<button class="btn btn-dark margin-top-20">FAQ</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div class="row">
			<div class="contents margin-top-20">
				<div class="f-left">
					<label class="margin-bottom-5">search:</label><br>
					<input class="search f-left" type="text">
				</div>
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">search by:</label><br>
					<div class="select">
						<select>
							<option value="All Categories">Contact Number</option>
							<option value="Burgers">Address</option>
						</select>
					</div>
				</div>
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">Province:</label><br>
					<div class="select">
						<select>
							<option value="All Province">All Province</option>
							<option value="Abra">Abra</option>
							<option value="Batangas">Batangas</option>
							<option value="Bulacan">Bulacan</option>
							<option value="Bicol">Bicol</option>
							<option value="Cavite">Cavite</option>
							<option value="Bulacan">Laguna</option>
						</select>
					</div>
				</div>
				<button class="f-left btn btn-light margin-top-20 margin-left-20">Search</button>
			</div>
		</div>

		<div class="row">
			<div class="contents margin-top-20 line">
				<label class="f-right margin-top-10 bggray-white font-14 gray-color">No Search Results</label>
				<span class="f-right white-space"></span>
				<div class="clear"></div>
			</div>
		</div>
	</section>

	<section class="container-fluid" section-style="content-panel">
		<div class="row">
			<div class="centerer">
				<div class="content-container f-left half unboxed margin-left-40">
					<img class="ad-content img-responsive curved-border margin-bottom-20" src="../assets/images/jollibee-ad.jpg">
					<img class="ad-content img-responsive curved-border margin-bottom-20" src="../assets/images/jollibee-ad.jpg">
					<img class="ad-content img-responsive curved-border margin-bottom-20" src="../assets/images/jollibee-ad.jpg">
					<img class="ad-content img-responsive curved-border margin-bottom-20" src="../assets/images/jollibee-ad.jpg">
					<img class="ad-content img-responsive curved-border margin-bottom-20" src="../assets/images/jollibee-ad.jpg">
				</div>
				<div class="content-container f-left half">
					
					<div class="image-gallery">
						<div class="img-container">
							<p class="red-color font-16 text-center"><strong>irReese'sitible Deal!</strong></p>
							<div class="arrow-left"><i class="fa fa-angle-left"></i></div>
							<div class="text-center img-content">
								<img class="img-responsive active" src="../assets/images/jollibee-promo-1.jpg" gallery-img="1"/>
								<img class="img-responsive" src="../assets/images/jollibee-promo-2.jpg" gallery-img="2"/>
								<img class="img-responsive" src="../assets/images/jollibee-promo-3.jpg" gallery-img="3"/>
							</div>
							<div class="arrow-right"><i class="fa fa-angle-right"></i></div>
							<div class="bullets">
								<a class="active" href="#"><i class="fa fa-circle"></i></a>
								<a href="#"><i class="fa fa-circle"></i></a>
								<a href="#"><i class="fa fa-circle"></i></a>
							</div>
						</div>

						<div class="desc-container">
							<div class="desc-content active" gallery-desc="1">
								<p class="font-12">Buy the Reese's Peanut Butter Jollibee promo pack save 20 pesos when you buy Jollibee Reese's Mix-ins.</p>
								<p class="font-12 light-red-color">Promo Mechanics</p>
								<ul>
									<li class="font-12">Promo is from March 30 to May 31, 2015.</li>
									<li class="font-12">Promo is valid for Jollibee Dine-in, Take Out or Drive Thru transactions only.</li>
									<li class="font-12">The Reese's promo bundle consists of 2 pieces 34g Reese's Peanut Butter Cup and a serialized promo coupon.</li>
									<li class="font-12">The promo bundles are sold at selected supermarkets nationwide (see list below).</li>
									<li class="font-12">The promo coupon should be surrendered to any Jollibee outlet nationwide to be able to availa of a discount for every purchase of a Jollibee Reese's Mix-ins.</li>
									<li class="font-12">Only one promo coupon will be honored for discount for every Reese's Mix-ins purchase.</li>
									<li class="font-12">Promo one coupon will not be honored if it has been 
								</ul>
							</div>
							<div class="desc-content" gallery-desc="2">
								<p class="font-12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eget molestie massa.</p>
								<p class="font-12 red-color">Aenean ut erat</p>
								<ul>
									<li class="font-12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eget molestie massa. Aenean ut erat mi. Maecenas a accumsan elit.</li>
									<li class="font-12">Nullam quis feugiat neque. Quisque eget mi non ex suscipit tincidunt nec eget elit. Suspendisse molestie lacus eu aliquam cursus.</li>
									<li class="font-12">Morbi eleifend faucibus nibh, eu sodales mauris gravida sed. Mauris in quam vitae ex volutpat facilisis eget id ipsum. Sed ut laoreet felis.</li>
									<li class="font-12">Fusce porta, orci ut ornare tristique, nunc purus feugiat dolor, a malesuada purus mi et justo. Sed non imperdiet elit. Pellentesque mollis venenatis tempus. </li>
									<li class="font-12">Donec vel tortor non arcu pellentesque volutpat in vel diam. Ut sit amet consequat massa.</li>
									<li class="font-12">Nullam varius mattis nibh convallis accumsan. Nunc vel velit ultricies dolor commodo tincidunt. Fusce congue felis est, sed lacinia tortor auctor vel.</li>
									<li class="font-12">In purus lorem, faucibus vel finibus at, bibendum vel magna. Donec tempus urna nec odio pellentesque, sed pulvinar leo lacinia.</li>
								</ul>
							</div>
							<div class="desc-content" gallery-desc="3">
								<p class="font-12">Cras euismod odio sed porttitor dictum. Vivamus id </p>
								<p class="font-12 red-color">Suspendisse potenti</p>
								<ul>
									<li class="font-12">Vestibulum ipsum turpis, dignissim vel imperdiet a, tempor ut lorem. In ullamcorper, leo nec</li>
									<li class="font-12">hendrerit pulvinar, enim tellus tristique nibh, eget sollicitudin urna nisi nec turpis. Nunc in tincidunt leo, ultricies feugiat libero.</li>
									<li class="font-12">Fusce eros est, bibendum sit amet tincidunt ut, hendrerit sed nulla. Aenean mollis leo et mi suscipit</li>
									<li class="font-12">suscipit imperdiet ligula elementum. Morbi laoreet lobortis neque, at rutrum sem venenatis lacinia</li>
								</ul>
							</div>

						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</section>

<?php include "../construct/footer.php"; ?>