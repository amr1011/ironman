<body>
	<?php include "../construct/header.php"; ?>

	<section class="container-fluid" section-style="top-panel">
        <div class="row header-container">
            <div class="contents">
                <h1 class="f-left">Search Customer</h1>
                <div class="f-right">
                    <button class="btn btn-light margin-top-20 margin-right-10">Add New Customer</button>
                    <button class="btn btn-dark margin-top-20 margin-right-10">Skip to Cart</button>
                    <button class="btn btn-dark margin-top-20">FAQ</button>
                </div>
                <div class="clear"></div>
            </div>
        </div>

        <div class="row">
            <div class="contents margin-top-20">
                <div class="f-left">
                    <label class="margin-bottom-5">search:</label><br>
                    <input class="search f-left" type="text">
                </div>
                <div class="f-left margin-left-20">
                    <label class="margin-bottom-5">search by:</label><br>
                    <div class="select">
                        <select>
                            <option value="All Categories">Contact Number</option>
                            <option value="Burgers">Address</option>
                        </select>
                    </div>
                </div>
                <div class="f-left margin-left-20">
                    <label class="margin-bottom-5">Province:</label><br>
                    <div class="select">
                        <select>
                            <option value="All Province">All Province</option>
                            <option value="Abra">Abra</option>
                            <option value="Batangas">Batangas</option>
                            <option value="Bulacan">Bulacan</option>
                            <option value="Bicol">Bicol</option>
                            <option value="Cavite">Cavite</option>
                            <option value="Bulacan">Laguna</option>
                        </select>
                    </div>
                </div>
                <button class="f-left btn btn-light margin-top-20 margin-left-20">Search</button>
            </div>
        </div>

        <div class="row">
            <div class="contents margin-top-20 line">
                <p class="f-right margin-top-10 bggray-white font-14"><strong>Search Results | 2 Customers</strong></p>
                <span class="f-right white-space"></span>
                <div class="clear"></div>
            </div>
        </div>
    </section>

	<section class="container-fluid" section-style="content-panel">
		<div class="row">
            
			<!-- sample-1 -->
			<div class="content-container viewable">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-16"><strong>Mark Anthony D. Dulay</strong></p>
                        <p class="font-14 margin-top-10"><span class="red-color"><strong>Contact Num:</strong></span>(+63) 910-146-4178 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    <div class="width-60per f-right">
                        <p class="font-14 no-margin-bottom"><strong><span class="red-color">THIS CUSTOMER IS DIFFICULT TO HANDLE</span></strong></p>
                        <p class="font-14 margin-top-10"><span class="red-color"><strong>Address:</strong></span> 168 San Ramon St., Brgy. San Pedro, Tarlac City - Tarlac</p>    
                    </div>
                    <div class="clear"></div>
                </div>
			</div>
            <!-- sample end-1 -->

            <!-- sample-1 -->
            <div class="content-container viewable">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-16"><strong>Ron Frederick T. San Juan</strong></p>
                        <p class="font-14 margin-top-10"><span class="red-color"><strong>Contact Num:</strong></span>(+63) 917-146-4178 <i class="fa fa-mobile"></i> Smart</p>
                    </div>
                    <div class="width-60per f-right">
                        <p class="font-14 no-margin-bottom"><strong><span class="green-color">THIS CUSTOMER IS EASY TO HANDLE</span></strong></p>
                        <p class="font-14 margin-top-10"><span class="red-color"><strong>Address:</strong></span> 1234 - E, San Kaman St., Brgy. Ellias, Manila City - NCR</p>    
                    </div>
                     <div class="clear"></div>
                </div>
            </div>
            <!-- sample end-1 -->
		</div>
	</section>
<?php include "../construct/footer.php"; ?>