<?php include "../construct/header.php"; ?>
	
	<section class="container-fluid" section-style="top-panel">
		

		<div class="row header-container">	
			<div class="contents">
				<h1 class="f-left">Search Customer</h1>
				<div class="f-right">
					<button type="button" class="btn btn-light margin-top-20 margin-right-10">Add New Customer</button>
					<button type="button" class="btn btn-dark margin-top-20 margin-right-10">Skip to Cart</button>
					<button type="button" class="btn btn-dark margin-top-20 margin-right-10">FAQ</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div class="row">
			<div class="contents">
				<div class="f-left margin-top-20 margin-right-20">
					<label >SEARCH</label>
					<br />
					<input type="text" class="search" />
				</div>
				<div class="f-left margin-top-20 margin-right-20">
					<label>SEARCH BY:</label>
					<br />
					<div class="select">
						<select>
							<option value="xxx">Contact Number</option>
						</select>
					</div>
				</div>
				<div class="f-left margin-top-20">
					<label>PROVINCE:</label>
					<br />
					<div class="select margin-right-20">
						<select>
							<option value="Province">Province</option>
						</select>
					</div>
					<button type="button" class="btn btn-light width-5vw">Search</button>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="contents margin-top-20 line text-right">
				<span class="white-space"> </span>	
				<p class="f-right bggray-white font-14 black-color margin-top-5"><strong>No Search Result</strong></p>
			</div>
		</div>

		<div class="row">
			<div class="contents text-center margin-top-100">
				<img src="../assets/images/Customer_404.svg" alt="person image" />
				<p class="gray-color font-14 margin-top-20">The Contact number cannot be found in the system.</p>
				<p class="gray-color font-14"> Add this caller as 
				a new customer?</p>
				<div class="margin-top-30">
					<button type="button" class="btn btn-light margin-right-10">Add New Customer</button>
					<button type="button" class="btn btn-light">Update Existing Customer</button>
				</div>
			</div>

		</div>
	</section>
	
	
<?php include "../construct/footer.php";?>