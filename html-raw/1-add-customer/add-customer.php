<?php include "../construct/header.php"; ?>
	
	<section class="container-fluid" section-style="top-panel">
		

		<div class="row header-container">	
			<div class="contents ">
				<h1 class="f-left">Search Customer</h1>
				<div class="f-right">
					<button type="button" class="btn btn-gray margin-top-20 margin-right-10">Add New Customer</button>
					<button type="button" class="btn btn-dark margin-top-20 margin-right-10">Skip to Cart</button>
					<button type="button" class="btn btn-dark margin-top-20 margin-right-10">FAQ</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<!-- search -->
		<div class="row" >
			<div class="contents">
				<div class="f-left margin-top-20 margin-right-20">
					<label class="">SEARCH</label>
					<br />
					<input type="text" class="search" />
				</div>
				<div class="f-left margin-top-20 margin-right-20">
					<label>SEARCH BY:</label>
					<br />
					<div class="select">
						<select>
							<option value="Contact Number">Contact Number</option>
							<option value="Contact Number">Contact Number</option>
							<option value="Contact Number">Contact Number</option>
						</select>
					</div>
				</div>
				<div class="f-left margin-top-20">
					<label>PROVINCE:</label>
					<br />
					<div class="select margin-right-20">
						<select>
							<option value="Province">Province</option>	
						</select>
					</div>
					<button type="button" class="btn btn-light ">Search</button>
				</div>
			</div>
		</div>
		<!-- add new customer -->
		<div class="row">
			<div class="contents margin-top-20 line text-right">
				<span class="white-space"> </span>	
				<p class="f-right bggray-white font-14 black-color margin-top-5"><strong>No Search Result</strong></p>
			</div>
		</div>
	</section>


	<section class="container-fluid" section-style="content-panel">
		<div class="row">

			<div class="content-container">
				<div>
					<h3 class="f-left no-margin-top">Add New Customer</h3>
					<p class="f-right red-color font-14 margin-top-5"><strong>FIELDS WITH * ARE REQUIRED</strong></p>
					<div class="clear"> </div>
					<hr  />

					
					
					<!-- content panel -->
					<div class="data-container split">
						<!-- contact number -->
						<div>
							<label>Contact Number: <span class="red-color font-16">*</span></label>
							<br />
							<div class="select small margin-right-10">
								<select>
									<option value="Mobile">Mobile</option>
								</select>
							</div>
							<input type="text" class="small">
						</div>
						<!-- alternate number -->
						<div>
							<label>Altername Number: <span class="red-color font-16">*</span></label>
							<br />
							<div class="select small margin-right-10 f-left">
								<select>
									<option value="Mobile">Mobile</option>
								</select>
							</div>
							<input type="text" class="xsmall f-left margin-right-10">
							<input type="text" class="xsmall f-left margin-right-10">
							<div class="f-left margin-left-5 text-right">
								<a href="#" class="font-12 ">+ Add Another <br />Contact Number</a>
							</div>
							<div class="clear"></div>
						</div>
						<!-- first name -->
						<div>
							<label class="margin-top-10">First name</label>
							<input type="text" class="large">
						</div>
						<!-- middle name -->
						<div>
							<label class="margin-top-10">Middle Name</label>
							<input type="text" class="large margin-top-5">
						</div>
						<!-- last name -->
						<div>
							<label class="margin-top-10">Last Name: <span class="red-color font-16">*</span></label>
							<input type="text" class="large margin-top-5">
						</div>
						<!-- email address -->
						<div >
							<label class="margin-top-10">Email Address: </label>
							<br />
							<input type="text" class="normal f-left">
							<div class="f-left margin-left-100 text-right">
								<a href="#" class="font-12 ">+ Add Another <br />Contact Number</a>
							</div>
							<div class="clear"></div>
						</div>
						<!-- date of birth -->
						<div>
							<label class="margin-top-10">Date of birth: </label>
							<div class="date-picker">
								<input type="text" data-date-format="MM/DD/YYYY">
								<span class="fa fa-calendar text-center red-color"></span>
							</div>
						</div>

						<!-- discount -->
						<div>
							<label class="margin-top-10">DISCOUNTS: </label>
							<br />

							<!-- senior -->
							<div class="margin-right-20">
								<div class=" bggray-dark">
									<p class="font-12 f-left margin-left-10 margin-top-10"><strong>Is this customer a Senior Citizen?</strong></p>
									<div class="form-group f-right padding-top-10 no-margin-bottom">
										<input id="senior1" type="radio" class="radio-box" name="senior">
										<label class="radio-lbl margin-right-10 " for="senior1">Yes</label>
										<input id="senior2" type="radio" class="radio-box" name="senior">
										<label class="radio-lbl margin-right-10" for="senior2">No</label>
									</div>
									<div class="clear"> </div>
									<hr />
									<label class="font-12 margin-left-15">OSCA NUMBER: <span class="red-color font-12">*</span></label>
									<br />
									<input type="text" class="normal margin-top-5 margin-bottom-10 margin-left-10" />
								</div>
							</div>

							<!-- customer -->
							<div class="margin-right-20">
								<div class="bggray-dark margin-top-15">
									<p class="font-12 f-left margin-left-10 margin-top-10"><strong>Is this customer a PWD?</strong></p>
									<div class="form-group f-right padding-top-10 no-margin-bottom">
										<input id="pwd1" type="radio" class="radio-box" name="pwd">
										<label class="radio-lbl margin-right-10" for="pwd1">Yes</label>
										<input id="pwd2" type="radio" class="radio-box" name="pwd">
										<label class="radio-lbl margin-right-10" for="pwd2">No</label>
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>

						<div>
							<label class="margin-top-10 no-margin-bottom">CARDS: </label>
							<br />
							<!-- happy plus card -->
							<div class="margin-right-20">
								<div class="bggray-dark margin-top-15">
									<p class="font-12 f-left margin-left-10 margin-top-10"><strong>Is this customer a Happy Plus Card holder?</strong></p>
									<div class="form-group f-right padding-top-10 no-margin-bottom">
										<input id="happy1" type="radio" class="radio-box" name="happyplus">
										<label class="radio-lbl margin-right-10" for="happy1">Yes</label>
										<input id="happy2" type="radio" class="radio-box" name="happyplus">
										<label class="radio-lbl margin-right-10" for="happy2">No</label>
									</div>
									<div class="clear" ></div>
									<hr />

									<!-- happy card plus display bottom -->
									<div>
										<table border="0" class="margin-left-10">
											<tr>
												<td><label class="padding-left-10"><strong>CARD NUMBER: <span class="red-color">*</span></strong></label></td>
												<td><label class="padding-left-10"><strong>EXPIRY DATE: <span class="red-color">*</span></strong></label></td>
											</tr>
											<tr>
												<td>
													<input type="text" class="small">
												</td>
												<td>
													<div class="date-picker margin-left-10">
														<input type="text" data-date-format="MM/DD/YYYY" >
														<span class="fa fa-calendar red-color"></span> 
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class="form-group padding-top-10" >
														<input class="chck-box " id="card-number"  type="checkbox">
														<label class="chck-lbl" for="card-number">Is the card number <br /> smudged  or not visible?</label>
													</div>
												</td>
												<td>
													<label class="margin-left-10"> REMARKS:</label>												
													<input type="text" class="small margin-left-10">
												</td>
											</tr>
										</table>
									</div>
									
									<hr />

									<div>
										<table border="0" class="margin-left-10">
											<tr>
												<td><label class="padding-left-10"><strong>CARD NUMBER: <span class="red-color">*</span></strong></label></td>
												<td><label class="padding-left-10"><strong>EXPIRY DATE: <span class="red-color">*</span></strong></label></td>
											</tr>
											<tr>
												<td>
													<input type="text" class="small" disabled>
												</td>
												<td>
													<div class="date-picker margin-left-10">
														<input type="text" data-date-format="MM/DD/YYYY" disabled>
														<span class="fa fa-calendar red-color"></span>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class="form-group padding-top-10">
														<input class="chck-box " id="card-number"  type="checkbox">
														<label class="chck-lbl" for="card-number">Is the card number <br /> smudged  or not visible?</label>
													</div>
												</td>
												<td>
													<label class="margin-left-10 ">REMARKS:</label>												
													<input type="text" class="small margin-left-10 ">
												</td>
											</tr>
										</table>									
									</div>
									<a href="#" class="margin-bottom-15 margin-right-15 font-12 f-right">+ Add Another Happy Plus Card</a>
									<div class="clear"> </div>
								</div>
							</div>

							<div class="margin-right-20">
								<div class="bggray-dark margin-top-15">
									<p class="font-12 f-left margin-left-10 margin-top-10"><strong>Is this customer a Loyalty Card holder?</strong></p>
									<div class="form-group f-right padding-top-10 no-margin-bottom">
										<input id="loyalty1" type="radio" class="radio-box" name="loyalty">
										<label class="radio-lbl margin-right-10" for="loyalty1">Yes</label>
										<input id="loyalty2" type="radio" class="radio-box" name="loyalty">
										<label class="radio-lbl margin-right-10" for="loyalty2">No</label>
									</div>
									<div class="clear" > </div>
								</div>
							</div>
						</div>
					</div>

					<div class="data-container split">
						<p class="font-14"><strong>ADDRESS:</strong></p>

						<div class="bggray-dark">
							<!-- address type & address label -->
							<div>
								<div class="f-left margin-right-75">
									<p class="font-12 margin-top-10 margin-left-20 margin-bottom-10"><strong>ADDRESS TYPE: <span class="red-color font-14">*</span></strong></p>
								</div>
								<div class="f-left">
									<p class="font-12 margin-top-10 margin-left-10 margin-bottom-10"><strong>ADDRESS LABEL: <span class="red-color font-14">*</span></strong></p>	
								</div>
								<div class="clear"> </div>
							</div>

							<!-- input for address -->
							<div>
								<div class="f-left">
									<div class="select margin-left-20 margin-right-10">
										<select>
											<option value="Mobile">Mobile</option>
										</select>
									</div>
								</div>
								<div class="f-left">
									<input type="text" class="small">
								</div>

								<div class="clear"> </div>
							</div>
							<hr class="margin-bottom-15 margin-top-15" />

							<!-- province & city -->
							<div>
								<div class="f-left margin-right-100 padding-right-25">
									<p class="font-12 margin-left-20"><strong>PROVINCE: </strong></p>
								</div>
								<div class="f-left">
									<p class="font-12"><strong>CITY: <span class="red-color">*</span></strong></p>
								</div>
								<div class="clear"></div>
							</div>
							<!-- input for provice & and city -->
							<div>
								<div class="f-left margin-left-20">
									<div class="select">
										<select>
											<option value="Home">Home</option>
										</select>
									</div>
								</div>
								<div class="f-left margin-left-10">
									<div class="select">
										<select>
											<option value="Select City">Select City</option>
										</select>
									</div>
								</div>
								<div class="clear"></div>
							</div>
							
							<!-- landmark -->
							<div>
								<p class="font-12 margin-top-10 margin-left-20"><strong>LANDMARK</strong></p>
								<input type="text" class="large margin-left-20">
							</div>

							<!-- housenumber and building -->
							<div>
								<div class="f-left margin-right-90">
									<p class="font-12 margin-left-20 margin-top-10"><strong>HOUSE NUMBER: </strong></p>
								</div>	
								<div class="f-left">
									<p class="font-12 margin-top-10"><strong>BUILDING: </strong></p>
								</div>
								<div class="clear"></div>
							</div>
							
							<!-- input for housenumber & building -->
							<div>
								<div class="f-left margin-left-20">
									<input type="text" class="small">
								</div>
								<div class="f-left">
									<input type="text" class="small margin-left-10">
								</div>
								<div class="clear"> </div>
							</div>
							
							<!-- unit and floor -->
							<div>
								<div class="f-left margin-right-100 padding-right-60">
									<p class="font-12 margin-left-20 margin-top-10"><strong>UNIT: </strong></p>
								</div>	
								<div class="f-left">
									<p class="font-12 margin-top-10"><strong>FLOOR: </strong></p>
								</div>
								<div class="clear"></div>
							</div>
							
							<!-- input for unit and floor -->
							<div>
								<div class="f-left margin-left-20">
									<input type="text" class="small">
								</div>
								<div class="f-left">
									<input type="text" class="small margin-left-10">
								</div>
								<div class="clear"> </div>
							</div>
							
							<!-- streets -->
							<div>
								<p class="font-12 margin-top-10 margin-left-20"><strong>STREET: </strong></p>
								<input type="text" class="search margin-left-20">
							</div>
							
							<!-- subdivision -->
							<div>
								<p class="font-12 margin-top-10 margin-left-20"><strong>SUBDIVISION: </strong></p>
								<input type="text" class="search margin-left-20">
							</div>
							
							<!-- barangay -->
							<div>
								<p class="font-12 margin-top-10 margin-left-20"><strong>BARANGAY: </strong></p>
								<input type="text" class="search margin-left-20 margin-bottom-20">
							</div>
						</div>

						<p class="font-12 margin-top-10"><strong>NEAREST STORE:</strong></p>
						
						<div class="bggray-dark">
							<div>
								<p class="font-14 f-left margin-top-20 margin-left-15"><strong>MM Ortigas Roosevelt | MI0444</strong></p>
								<a class="font-14 f-right margin-top-20 margin-right-15"><strong><i class="fa fa-map-marker font-16 margin-right-5"></i>Show Map</strong></a>
								<div class="clear"></div>
							</div>
							<div>
								<p class="font-14 f-left green-color margin-left-15"><strong>Delivery Time:</strong></p>
								<p class="font-14 f-right margin-right-15 margin-bottom-20"><strong>20 Minutes</strong></p>
								<div class="clear"></div>
							</div>
						</div>
						
						<div class="bggray-dark margin-top-20">
							<p class="font-14 padding-top-20 padding-bottom-20 padding-left-20 padding-right-20 yellow-bg"><strong>Store Announcement!</strong> Delivery will be delayed for 10 mnutes 
							due to bad weather. Thank you.</p>
						</div>

					</div>		
					<!-- button bottom -->
					<div class="f-right margin-top-20">
						<button type="button" class="btn btn-light margin-right-10">Save Customer Info</button>
						<button type="button" class="btn btn-light margin-right-10 modal-trigger" modal-target="information-match">Confirm &amp; Proceed to Order</button>
						<button type="button" class="btn btn-dark">Cancel</button>
					</div>
					<div class="clear"></div>
				</div>
			</div>	
		</div>

	</section>

	<div class="modal-container" modal-id="information-match">
		
		<div class="modal-body small">
			<div class="modal-head ">
				<p>Information Match</p>
				<div class="modal-close close-me"></div>
			</div>

			<div class="modal-content">
				<div class="col-sm-2">
					<img src="../assets/images/ui/warning.svg" alt="warning logo" class="img-responsive" />
				</div>
				<div class="col-sm-10 ">
					<p >An existing customer have the same information. <br />Are you sure you want proceed</p>
				</div>
				<div class="clear"></div>

				<div class="row bggray-white margin-top-10  padding-bottom-20 no-padding">
					<p class="font-18 no-margin-bottom margin-left-20 padding-top-20"><strong>Samuel O. Hernandez</strong></p>
					
					<p class="margin-left-20"><strong>DISCOUNTS:</strong></p>
			
					<div class="bggray-dark padding-top-10 padding-left-10 padding-bottom-20 margin-left-20 margin-right-20">
						<div class="f-left font-12 margin-right-20"><strong>Senior <br />Citizen</strong></div>
						<div class="f-left font-12"><strong>Samuel O. Hernandez<br /><span class="red-color">OSCA Number: </span>0014-7845</strong></div>
						<div class="clear"></div>
					</div>

				</div>

			</div>

			<div class="f-right margin-right-30 margin-bottom-10">
					<button type="button" class="btn btn-dark margin-right-10">Confirm &amp; Proceed to Order</button>
					<button type="button" class="btn btn-dark">Cancel</button>
				</div>
				<div class="clear"></div>
		</div>
	</div>

<?php include "../construct/footer.php";?>