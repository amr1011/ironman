<!DOCTYPE html>

<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="keywords" content="">
		<link rel="shortcut icon" href="assets/ico/favicon.ico">

		<title>Ironman Categroy Management | Add Category</title>

		<!-- Bootstrap core CSS -->
		<link href="../assets/css/bootstrap/bootstrap.min.css" rel="stylesheet">
		<link href="../assets/css/bootstrap/bootstrap-theme.min.css" rel="stylesheet">
		<!-- Font Awesome -->
		<link href="../assets/css/font-awesome/font-awesome.min.css" rel="stylesheet">

		<!-- Core CSS -->
		<link href="../assets/css/global/commons.css" rel="stylesheet">
		<!-- Custom CSS -->
		<link href="../assets/css/global/header.css" rel="stylesheet">
		<link href="../assets/css/global/ui-widgets.css" rel="stylesheet">
		<link href="../assets/css/global/unique-widget-style.css" rel="stylesheet">
		<link href="../assets/css/global/section-top-panel.css" rel="stylesheet">
		<link href="../assets/css/global/section-content-panel.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

		<style id="holderjs-style" type="text/css"></style>
	</head>
	<header class="container-fluid" custom-style="header">
		<div class="row">
			<div class="col-xs-12">
				<div class="logo-container f-left">
					<img class="img-responsive" src="../assets/images/jollibe-logo-call-center.svg" alt="jolibee logo"/>
				</div>

				<div class="profile-container f-right">
					
					<div class="text-right">
						<p>Status: Online | 100ms<br/>May 18, 2015 | 01:01 PM</p>
					</div>

					<!-- NOTE: **required to input user's profile name inttials(str) at hte 'prof-name' attr -->
					<div class="profile-pic" prof-name="HP">
						<img class="img-circle img-responsive" src="../assets/images/profile-pic.jpg"/>
					</div>

					<div class="text-left">
						<p>Hannah Perez<br/><sub>DATA MANAGER</sub></p>
					</div>
					
					<button class="btn btn-logout">Logout</button>
				</div>
			</div>
		</div>

		<div class="row">
			<nav class="f-left">
				<ul>
					<li><a href="#">Item Management</a></li>
					<li class="active"><a href="#">Category Management</a></li>
					<li><a href="#">Promo Management</a></li>
					<li><a href="#">Free Item Management</a></li>
					<li><a href="#" class="modal-trigger" modal-target="rider-management">Rider Management</a></li>
				</ul>
			</nav>
			<div class="icon-btn f-right">
				<a href="#"><img src="../assets/images/ui/icon-msg.png"></a>
				<a href="#"><img src="../assets/images/ui/icon-speaker.png" class="modal-trigger" modal-target="global-announcement"></a>
				<a href="#"><img src="../assets/images/ui/icon-info.png" class="modal-trigger" modal-target="special-instruction"></a>
			</div>
			<div class="clear"></div>
		</div>


		<div class="row sub-nav">
            <nav>
                <ul>
                    <li class="active"><div class="notify">99</div><a hhref="#">Item Management</a></li>
                    <li><a hhref="#">Category Management</a></li>
                    <li><a hhref="#">Promo Management</a></li>
                    <li><a hhref="#">Free Item Management</a></li>
                </ul>
            </nav>
        </div>


	<!-- modal RIDER MANAGEMENT-->
	<div class="modal-container " modal-id="rider-management">
		<div class="modal-body small">
			<div class="modal-head">
				<h4 class="text-left">Rider Management</h4>
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content">
				<div class="error-msg margin-bottom-20">
					<i class="fa fa-exclamation-triangle margin-right-10 "></i>Incorrect Password! Please try again.
				</div>
				<table>
					<tbody><tr>
						<td>
							<i class="fa fa-lock yellow-color fa-5x / margin-left-20"></i><br />		
							<img src="../assets/images/lock.png" alt="lock image" class="small-thumb / margin-left-20 ">							
						</td>
						<td>
							<p class="font-14 margin-left-20 margin-bottom-10"><strong>Enter store manager password to proceed.</strong></p>
							<label class="margin-left-20">Password:</label>
							<br />
							<input type="password" class="margin-left-20 / normal / error-form">
						</td>
					</tr>
				</tbody></table>
			
				
			</div>
			<!-- button -->
			<div class="f-right margin-right-20 margin-bottom-10">
					<button type="button" class="btn btn-dark margin-right-10">Confirm</button>
					<button type="button" class="btn btn-dark close-me">Cancel</button>
			</div>
			<div class="clear"></div>
		</div>
	</div>


	<!-- modal SPECIAL INSTRUCTION-->
	<div class="modal-container " modal-id="special-instruction">
		<div class="modal-body small">
			<div class="modal-head">
				<h4 class="text-left">Special Instruction</h4>
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content">
				<p class="font-14 / gray-color / margin-bottom-10 margin-top-10 "><strong>180 Characters Remaining</strong></p>
				<textarea class="" placeholder="Type your message here ..."></textarea>

				<div class="bggray-dark padding-all-20">
					<p class="red-color margin-bottom-5"><strong>To All Stories:</strong></p>
					<p class="font-12 / text-justify"><strong>Please DO NOT USE the special instructions tab to send PRODUCT AVAILABILITY status  to call center.
						Special Instructions TAB is intended for product information not included in product management like
						chicken part and/or change of delivery time or area due to weather condition, unforseen events and
						items that store wants to push/suggest. FOR YOUR STRICT COMPLIANCE
					</strong></p>
				</div>
			</div>
			<!-- button -->
			<div class="f-right margin-right-20 margin-bottom-10">					
					<button type="button" class="btn btn-light close-me">Save</button>
			</div>
			<div class="clear"></div>
		</div>
	</div>

	<!-- modal GLOBAL ANNOUNCEMENT-->
	<div class="modal-container " modal-id="global-announcement">
		<div class="modal-body small">
			<div class="modal-head">
				<h4 class="text-left">Global Announcement</h4>
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content">
				<span class="f-right"><strong><a href="#">Mark All as Read</a></strong></span>
				<div class="clear"></div>
			
			

			<div>
				<div class="div-inside-noline long">
					<table class="table-inside">
						<tbody>
							<tr class="margin-top-20 bggray-light">
								<td class="padding-all-5 ">
									<div class="yellow-bg padding-all-5">
										<p class="font-12 f-left"><strong>Chickenjoy Promo</strong></p>
										<p class="f-right font-12"><em>5 hours ago</em></p>
										<div class="clear"></div>

										<p class"font-12">This is to notify you all that there would be a promo form all 
											chickenjoy products. All chickenjoy products have a discount of PHP 20.00. 
											Stores, please adjust your price for such.
										</p>
									</div>
									
								</td>
							</tr>
							<tr>
								<td class="padding-all-5 ">
									<div class="yellow-bg padding-all-5">
										<p class="font-12 f-left"><strong>Chickenjoy Promo</strong></p>
										<p class="f-right font-12 "><em>5 hours ago</em></p>
										<div class="clear"></div>

										<p class"font-12">This is to notify you all that there would be a promo form all 
											chickenjoy products. All chickenjoy products have a discount of PHP 20.00. 
											Stores, please adjust your price for such.
										</p>
									</div>
								</td>
							</tr>
							<tr>
								<td class="padding-all-5 bggray-light">
									<div class="padding-all-5 bggray-dark ">
										<p class="font-12 f-left"><strong>Yum Promo</strong></p>
										<p class="f-right font-12"><em>2 days ago</em></p>
										<div class="clear"></div>

										<p class"font-12">This is to notify you all that there would be a promo form all 
											chickenjoy products. All chickenjoy products have a discount of PHP 20.00. 
											Stores, please adjust your price for such.
										</p>
									</div>
								</td>
							</tr>
							<tr>
								<td class="padding-all-5 bggray-light">
									<div class=" padding-all-5 bggray-dark">
										<p class="font-12 f-left"><strong>Yum Promo</strong></p>
										<p class="f-right font-12"><em>2 days ago</em></p>
										<div class="clear"></div>

										<p class"font-12">This is to notify you all that there would be a promo form all 
											chickenjoy products. All chickenjoy products have a discount of PHP 20.00. 
											Stores, please adjust your price for such.
										</p>
									</div>
								</td>
							</tr>	
							<tr>
								<td class="padding-all-5 bggray-light">
									<div class=" padding-all-5 bggray-dark">
										<p class="font-12 f-left"><strong>Yum Promo</strong></p>
										<p class="f-right font-12"><em>2 days ago</em></p>
										<div class="clear"></div>

										<p class"font-12">This is to notify you all that there would be a promo form all 
											chickenjoy products. All chickenjoy products have a discount of PHP 20.00. 
											Stores, please adjust your price for such.
										</p>
									</div>
								</td>
							</tr>							
							
						</tbody>
					</table>
				</div>
			</div>

			</div>

			
		</div>
	</div>
	</header>