<?php include "../construct/header.php"; ?>

	<section class="container-fluid" section-style="top-panel">
		<div class="row header-container">
			<div class="contents">
				<h1 class="f-left">Archive Search</h1>
				<div class="f-right">
					<button class="btn btn-dark modal-trigger margin-top-20 margin-right-10" modal-target="messenger">Messenger<div class="notify">99</div></button>
					<button class="btn btn-dark margin-top-20">FAQ</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div class="row">
			<div class="contents margin-top-20">
				<div class="f-left">
					<label>Date from</label></br>
					<div class="date-picker">
						<input data-date-format="MM/DD/YYYY" type="text" readonly="">
						<span class="fa fa-calendar text-center red-color"></span>
					</div>
				</div>
				<div class="f-left margin-left-15">
					<label>Date to</label></br>
					<div class="date-picker">
						<input data-date-format="MM/DD/YYYY" type="text" readonly="">
						<span class="fa fa-calendar text-center red-color"></span>
					</div>
				</div>
				
				<button class="f-right btn btn-dark margin-top-20 margin-left-20">Generate</button>
				<div class="clear"></div>
			
				<hr class="margin-top-15 margin-bottom-15"/>

				<div class="f-left">
					<label class="margin-bottom-5">search:</label><br>
					<input class="search" type="text">
				</div>
				<div class="f-left margin-left-15">
					<label class="margin-bottom-5">search by:</label><br>
					<div class="select">
						<select>
							<option value="All Area">Order ID</option>
							<option value="Metro Manila">Order Complete</option>
							<option value="Quezon City">Void Order</option>
							<option value="Makati City">Manual Relayed</option>
						</select>
					</div>
				</div>
				<div class="f-left margin-left-15">
					<label class="margin-bottom-5">area:</label><br>
					<div class="select">
						<select>
							<option value="All Area">All area</option>
							<option value="Metro Manila">Manila</option>
							<option value="Quezon City">Quezon City</option>
							<option value="Makati City">Makati City</option>
						</select>
					</div>
				</div>
				<button class="f-right btn btn-dark margin-top-20 margin-left-20">Search</button>
				<div class="clear"></div>

				<div class="f-left margin-top-15">
					<label class="margin-bottom-5">stores:</label><br>
					<div class="select">
						<select>
							<option value="All Area">Order ID</option>
							<option value="Metro Manila">Order Complete</option>
							<option value="Quezon City">Void Order</option>
							<option value="Makati City">Manual Relayed</option>
						</select>
					</div>
				</div>
				<div class="f-left margin-left-15 margin-top-15">
					<label class="margin-bottom-5">call center:</label><br>
					<div class="select">
						<select>
							<option value="All Area">All area</option>
							<option value="Metro Manila">Manila</option>
							<option value="Quezon City">Quezon City</option>
							<option value="Makati City">Makati City</option>
						</select>
					</div>
				</div>
				<div class="clear"></div>

				<div class="contents margin-top-20 line">
					<p class="bggray-white margin-top-5 f-right"><strong>Sort By:Store Name | Status | <span class="red-color">Date &#9650;</span></strong></p>
					<span class="white-space f-right"></span>
					<div class="clear"></div>
				</div>
			</div>
		</div>

		
	</section>


	<section class="container-fluid" section-style="content-panel">
		<div class="row">
			<div class="content-container unboxed">
				<div class="no-padding-all">
					<div class="paganation">
						<ul>
							<li><a href="#"><i class="fa fa-angle-left"></i></a></li>
							<li><a class="active" href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li><a href="#">6</a></li>
							<li><a href="#">7</a></li>
							<li><a href="#">8</a></li>
							<li><a href="#">9</a></li>
							<li><a href="#">10</a></li>
							<li><a href="#">11</a></li>
							<li><a href="#">12</a></li>
							<li><a href="#">13</a></li>
							<li><a class="continue" href="#">...</a></li>
							<li><a href="#">14</a></li>
							<li><a href="#">15</a></li>
							<li><a href="#">16</a></li>
							<li><a href="#">17</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i></a></li>
						</ul>
					</div>
				</div>

						<!-- sample 1 -->
				<div class="content-container viewable">
					<div>
						<div class="width-40per f-left">
							<p class="font-16 margin-bottom-5"><strong>Order ID: 734784</strong></p>
							<p class="font-16 margin-bottom-5"><strong>MM Ortigas Roosevelt | GW0444</strong></p>
							<p><strong><span class="red-color">Transaction Time:</span> May 18, 2015 | 10:11 PM </strong> </p>
						</div>
						
						<div class="width-40per f-left">
							<p class="margin-bottom-5"><strong><span class="green-color">ORDER COMPLETE</span></strong></p>
							<p class="margin-bottom-5"><strong><span class="red-color">Name:</span></strong> Jonathan R. Omido</p>
							<p class="margin-bottom-5"><strong><span class="red-color">Contact No.:</span></strong> (+63) 915-578-6147 <i class="fa fa-mobile font-16"></i> Globe</p>
						</div>
											
						<div class="clear"></div>
					</div>
				</div>

				<!-- sample 2 -->
				<div class="content-container viewable">
					<div>
						<div class="width-40per f-left">
							<p class="font-16 margin-bottom-5"><strong>Order ID: 964070</strong></p>
							<p class="font-16 margin-bottom-5"><strong>MM Ortigas Roosevelt | GW0444</strong></p>
							<p><strong><span class="red-color">Transaction Time: </span></strong> May 18, 2015 | 12:11 PM</p>
						</div>
						
						<div class="width-40per f-left">
							<p class="margin-bottom-5"><strong><span class="green-color">ORDER COMPLETE</span></strong></p>
							<p class="margin-bottom-5"><strong><span class="red-color">Name:</span></strong> Julie J. Laroya</p>
							<p class="margin-bottom-5"><strong><span class="red-color">Contact No.:</span></strong> (+63) 939-848-1458 <i class="fa fa-mobile font-16"></i> TNT</p>
						</div>
						<div class="clear"></div>
					</div>
				</div>

				<!-- sample 3 -->
				<div class="content-container red-bg viewable">
					<div>
						<div class="width-40per f-left">
							<p class="font-16 margin-bottom-5"><strong>Order ID: 734699</strong></p>
							<p class="font-16 margin-bottom-5"><strong>MM Ortigas Roosevelt | GW0444</strong></p>
							<p><strong><span class="red-color">Transaction Time: </span></strong> May 18, 2015 | 12:41 PM</p>
						</div>
						
						<div class="width-40per f-left">
							<p class="margin-bottom-5"><strong><span class="green-color">ORDER COMPLETE</span></strong></p>
							<p class="margin-bottom-5"><strong><span class="red-color">Name:</span></strong> Mark Anthony D. Dulay</p>
							<p class="margin-bottom-5"><strong><span class="red-color">Contact No.:</span></strong> (+63) 910-586-4159 <i class="fa fa-mobile font-16"></i> Smart</p>
						</div>                  
						<div class="clear"></div>
					</div>
				</div>

				<!-- sample 4 -->
				<div class="content-container viewable">
					<div>
						<div class="width-40per f-left">
							<p class="font-16 margin-bottom-5"><strong>Order ID: 764687 </strong></p>
							<p class="font-16 margin-bottom-5"><strong>MM Ortigas Roosevelt | JB0444</strong></p>
							<p><strong><span class="red-color">Transaction Time: </span></strong> May 18, 2015 | 12:11 PM</p>
						</div>
						
						<div class="width-40per f-left">
							<p class="margin-bottom-5"><strong><span class="green-color">ORDER COMPLETE</span></strong></p>
							<p class="margin-bottom-5"><strong><span class="red-color">Name:</span></strong> Jun M. Andrada</p>
							<p class="margin-bottom-5"><strong><span class="red-color">Contact No.:</span></strong> (02) 257-68-95 <i class="fa fa-phone font-16"></i> PLDT</p>
						</div>

						<div class="clear"></div>
					</div>
				</div>

				<!-- sample 5 -->
				<div class="content-container viewable">
					<div>
						<div class="width-40per f-left">
							<p class="font-16 margin-bottom-5"><strong>Order ID: 734601</strong></p>
							<p class="font-16 margin-bottom-5"><strong>MM Ortigas Roosevelt | GW0124</strong></p>
							<p><strong><span class="red-color">Transaction Time: </span></strong> May 18, 2015 | 12:45 PM</p>
						</div>
						
						<div class="width-40per f-left">
							<p class="margin-bottom-5"><strong><span class="green-color">ORDER COMPLETE</span></strong></p>
							<p class="margin-bottom-5"><strong><span class="red-color">Name:</span></strong> Mae G. Flores</p>
							<p class="margin-bottom-5"><strong><span class="red-color">Contact No.:</span></strong> (+63) 910-148-1436 <i class="fa fa-mobile font-16"></i> Smart</p>
						</div>
											
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!-- modal messenger -->
	<div class="modal-container" modal-id="messenger">
		<div class="modal-body">
			<div class="modal-head ">
				<h4 class="text-left">Manual Order - Order Summary</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content messenger">
				<!-- messages -->
				<div class="msg-content">
					<div class="chat-box">
						<p class="text-left font-12"><span class="gray-color">May 18, 2015 (12:57 PM)</span> - MM Sampaloc | JB1021</p>
						<div class="sender">
							<img class="profile" src="../assets/images/jollibe-face.png"/>
							<div class="msg">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis felis consectetur, sodales nunc quis, sagittis augue. Proin eu molestie libero, vitae volutpat eros.</p><br/>
								<p>Sodales nunc quis, sagittis augue. Proin eu molestie libero, vitae volutpat eros.</p><br/>
								<p>TY</p>
							</div>
						</div>

						<p class="text-right font-12"><span class="gray-color">May 18, 2015 (1:45 PM)</span> - MM Q.C. | JB1021</p>
						<div class="receiver">
							<div class="msg">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis felis consectetur, sodales nunc quis, sagittis augue. Proin eu molestie libero, vitae volutpat eros.</p><br/>
								<p>Sodales nunc quis, sagittis augue. Proin eu molestie libero, vitae volutpat eros.</p><br/>
								<p>TY</p>
							</div>
							<img class="profile" src="../assets/images/profile-pic.jpg"/>
						</div>

						<p class="text-left font-12"><span class="gray-color">May 18, 2015 (1:46 PM)</span> - MM Sampaloc | JB1021</p>
						<div class="sender">
							<img class="profile" src="../assets/images/jollibe-face.png"/>
							<div class="msg">
								<p>Thanks :)</p>
							</div>
						</div>
					</div>
					<!-- text input -->
					<div class="text-input padding-all-10">
						<textarea row="5" class="margin-top-20"></textarea>
						<div class="btn-container padding-top-20 margin-top-20">
							<button class="btn btn-dark width-100per">Send</button>
							<button class="btn btn-dark margin-top-10 margin-right-10 width-50per"><img class="width-90per" src="../assets/images/ui/smile.png"></button>
							<button class="btn btn-dark margin-top-10 margin-left-10 no-padding-hor width-50per close-me">Cancel</button>
						</div>
					</div>
				</div>

				<!-- contacts -->
				<div class="contacts">
					<label>Search Store:</label>
					<input type="text" class="width-100per normal"/>
					<div class="content">
						<table class="width-100per">
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php include "../construct/footer.php"; ?>