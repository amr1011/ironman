
	<?php include "../construct/header.php"; ?>

	<section class="container-fluid" section-style="top-panel">
		<div class="row header-container">
			<div class="contents">
				<h1 class="f-left">Rejected Order</h1>
				<div class="f-right margin-top-20">
					<button class="btn btn-dark margin-right-10">Messenger</button>
					<button class="btn btn-dark ">FAQ</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div class="row">
			<div class="contents margin-top-20">
				<div class="f-left">
					<label class="margin-bottom-5">search:</label><br>
					<input class="search f-left" type="text">
				</div>
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">search by:</label><br>
					<div class="select">
						<select>
							<option value="All Categories">Contact Number</option>
							<option value="Burgers">Address</option>
						</select>
					</div>
				</div>
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">Area:</label><br>
					<div class="select">
						<select>
							<option value="All Province">All Province</option>
							<option value="Abra">Abra</option>
							<option value="Batangas">Batangas</option>
							<option value="Bulacan">Bulacan</option>
							<option value="Bicol">Bicol</option>
							<option value="Cavite">Cavite</option>
							<option value="Bulacan">Laguna</option>
						</select>
					</div>
				</div>				
				<button class="f-left btn btn-dark margin-top-20 margin-left-20">Search</button>
				<div class="clear"></div>
				
				<!-- stories -->
				<div class="f-left margin-top-15 margin-right-15">
					<label >Stories:</label>
					<br />
					<div class="select">
						<select>
							<option value="All Stories">All Stories</option>
						</select>
					</div>
				</div>
				<!-- call center -->
				<div class="f-left margin-top-15">
					<label >Call Center: </label>
					<br />
					<div class="select">
						<select>
							<option value="All Stories">All Stories</option>
						</select>
					</div>
				</div>
				<div class="clear"></div>			
			</div>			
		</div>

		<div class="row">
			<div class="contents margin-top-20 line ">
				<p class="f-right margin-top-5 bggray-white  gray-color font-14"><strong>Sort By: Store Name | Province | <span class="light-red-color">Critically <img src="../assets/images/ui/sort-top-arrow.png"></span></strong></p>
				<span class="f-right white-space"></span>
				<div class="clear"></div>
			</div>
		</div>
	</section>

	<section class="container-fluid" section-style="content-panel">
		<!-- sample 1 -->
		<div class="content-container viewable">
        	<div>
                <div class="width-40per f-left">
                    <p class="font-16 margin-bottom-5"><strong>Order ID: 734784</strong></p>
                    <p class="font-16 margin-bottom-5"><strong>MM Ortigas Roosevelt | GW0444</strong></p>
                    <p><strong><span class="red-color">Transaction Time:</span></strong> May 18, 2015 | 10:11 PM  </p>
                </div>
                
                <div class="width-40per f-left">
                    <p class="margin-bottom-5"><strong><span class="red-color">WRONG ROUTING</span></strong></p>
                    <p class="margin-bottom-5"><strong><span class="red-color">Name:</span></strong> Jonathan R. Omido</p>
                    <p class="margin-bottom-5"><strong><span class="red-color">Contact No.:</span></strong> (+63) 915-578-6147 <i class="fa fa-mobile font-16"></i> Globe</p>
                </div>
                                    
                <div class="clear"></div>
            </div>
		</div>

		<!-- sample 2 -->
		<div class="content-container viewable">
        	<div>
                <div class="width-40per f-left">
                    <p class="font-16 margin-bottom-5"><strong>Order ID: 964070</strong></p>
                    <p class="font-16 margin-bottom-5"><strong>MM Ortigas Roosevelt | GW0444</strong></p>
                    <p><strong><span class="red-color">Transaction Time: </span></strong> May 18, 2015 | 12:11 PM</p>
                </div>
                
                <div class="width-40per f-left">
                    <p class="margin-bottom-5"><strong><span class="red-color">WRONG ROUTING</span></strong></p>
                    <p class="margin-bottom-5"><strong><span class="red-color">Name:</span></strong> Julie J. Laroya</p>
                    <p class="margin-bottom-5"><strong><span class="red-color">Contact No.:</span></strong> (+63) 939-848-1458 <i class="fa fa-mobile font-16"></i> TNT</p>
                </div>
                  
                <div class="f-left margin-left-20">
                	<p class=""><i class="fa fa-lock fa-5x red-color margin-left-30"></i></p>
                	<p class="red-color font-12 "><strong>phub_loreyjenkings</strong></p>

                </div>


                <div class="clear"></div>
            </div>
		</div>

		<!-- sample 3 -->
		<div class="content-container red-bg viewable">
        	<div>
                <div class="width-40per f-left">
                    <p class="font-16 margin-bottom-5"><strong>Order ID: 734699</strong></p>
                    <p class="font-16 margin-bottom-5"><strong>MM Ortigas Roosevelt | GW0444</strong></p>
                    <p><strong><span class="red-color">Transaction Time: </span></strong> May 18, 2015 | 12:41 PM</p>
                </div>
                
                <div class="width-40per f-left">
                    <p class="margin-bottom-5"><strong><span class="red-color">WRONG ROUTING</span></strong></p>
                    <p class="margin-bottom-5"><strong><span class="red-color">Name:</span></strong> Mark Anthony D. Dulay</p>
                    <p class="margin-bottom-5"><strong><span class="red-color">Contact No.:</span></strong> (+63) 910-586-4159 <i class="fa fa-mobile font-16"></i> Smart</p>
                </div>
                                    
                <div class="clear"></div>
            </div>
		</div>

		<!-- sample 4 -->
		<div class="content-container viewable">
        	<div>
                <div class="width-40per f-left">
                    <p class="font-16 margin-bottom-5"><strong>Order ID: 764687 <span class="green-color">*WEB*</span> </strong></p>
                    <p class="font-16 margin-bottom-5"><strong>MM Ortigas Roosevelt | JB0444</strong></p>
                    <p><strong><span class="red-color">Transaction Time: </span></strong> May 18, 2015 | 12:11 PM</p>
                </div>
                
                <div class="width-40per f-left">
                    <p class="margin-bottom-5"><strong><span class="red-color">WRONG ROUTING</span></strong></p>
                    <p class="margin-bottom-5"><strong><span class="red-color">Name:</span></strong> Jun M. Andrada</p>
                    <p class="margin-bottom-5"><strong><span class="red-color">Contact No.:</span></strong> (02) 257-68-95 <i class="fa fa-phone font-16"></i> PLDT</p>
                </div>

                <div class="f-left margin-left-20">
                	<p class=""><i class="fa fa-lock fa-5x red-color margin-left-30"></i></p>
                	<p class="red-color font-12 "><strong>phub_hercgamboa</strong></p>

                </div>
                                                   
                <div class="clear"></div>
            </div>
		</div>

		<!-- sample 5 -->
		<div class="content-container viewable">
        	<div>
                <div class="width-40per f-left">
                    <p class="font-16 margin-bottom-5"><strong>Order ID: 734601</strong></p>
                    <p class="font-16 margin-bottom-5"><strong>MM Ortigas Roosevelt | GW0124</strong></p>
                    <p><strong><span class="red-color">Transaction Time: </span></strong> May 18, 2015 | 12:45 PM</p>
                </div>
                
                <div class="width-40per f-left">
                    <p class="margin-bottom-5"><strong><span class="red-color">WRONG ROUTING</span></strong></p>
                    <p class="margin-bottom-5"><strong><span class="red-color">Name:</span></strong> Mae G. Flores</p>
                    <p class="margin-bottom-5"><strong><span class="red-color">Contact No.:</span></strong> (+63) 910-148-1436 <i class="fa fa-mobile font-16"></i> Smart</p>
                </div>
                                    
                <div class="clear"></div>
            </div>
		</div>

		

	</section>

	
	
<?php include "../construct/footer.php"; ?>