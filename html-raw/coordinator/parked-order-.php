<?php include "../construct/header.php"; ?>

	<section class="container-fluid" section-style="top-panel">
		<div class="row header-container">
			<div class="contents">
				<h1 class="f-left">For Verification</h1>
				<div class="f-right margin-top-20">
					<button class="btn btn-dark margin-right-10">Messenger</button>
					<button class="btn btn-dark ">FAQ</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div class="row">
			<div class="contents margin-top-20">
				<div class="f-left">
					<label class="margin-bottom-5">search:</label><br>
					<input class="search f-left" type="text">
				</div>
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">search by:</label><br>
					<div class="select">
						<select>
							<option value="All Categories">Contact Number</option>
							<option value="Burgers">Address</option>
						</select>
					</div>
				</div>
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">Area:</label><br>
					<div class="select">
						<select>
							<option value="All Province">All Province</option>
							<option value="Abra">Abra</option>
							<option value="Batangas">Batangas</option>
							<option value="Bulacan">Bulacan</option>
							<option value="Bicol">Bicol</option>
							<option value="Cavite">Cavite</option>
							<option value="Bulacan">Laguna</option>
						</select>
					</div>
				</div>				
				<button class="f-left btn btn-dark margin-top-20 margin-left-20">Search</button>
				<div class="clear"></div>
				
				<!-- stories -->
				<div class="f-left margin-top-15 margin-right-15">
					<label >Stories:</label>
					<br />
					<div class="select">
						<select>
							<option value="All Stories">All Stories</option>
						</select>
					</div>
				</div>
				<!-- call center -->
				<div class="f-left margin-top-15">
					<label >Call Center: </label>
					<br />
					<div class="select">
						<select>
							<option value="All Stories">All Stories</option>
						</select>
					</div>
				</div>
				<div class="clear"></div>	

				<div class="row">
					<div class="contents margin-top-20 line">
					<p class="f-right margin-top-5 bggray-white font-14"><strong>Sort By: Store Name | Province | <span class="red-color">Criticality <img src="../assets/images/ui/sort-top-arrow.png"></span></strong></p>
					<span class="f-right white-space"></span>
					<div class="clear"></div>
				</div>
				</div>		
			</div>			
		</div>

		
	</section>

	<section class="container-fluid" section-style="content-panel">
		<div class="row">
			

			<!-- sample-1 -->
			<div class="content-container">
				<div>
					<div class="f-left margin-bottom-10">
						<p class="font-16"><strong>Order ID: 734784</strong></p>									
						<p class="font-16 no-margin-all"><strong>MM Ortigas Roosevelt | JB0444</strong></p>
					</div>					
					
					<p class="font-14 no-margin-bottom f-right"><strong><span class="red-color">PARKED ORDER</span></strong></p>
					<br />
					<p class="font-14 f-right"><strong><span class="red-color">Elapsed Time:</span></strong> 00:01:45</p>
					<div class="clear"></div>
					<hr/>

					<div class="data-container split">
						
						<!-- clients information -->
						<div class="margin-top-20">
							<p class="f-left red-color font-12 margin-bottom-5"><strong>Name: </strong></p>
							<p class="f-right font-12">Jonathan R. Omido</p>
							<div class="clear"></div>

							<p class="f-left red-color font-12 margin-bottom-5"><strong>Contact Number: </strong></p>
							<p class="f-right font-12">(+63) 915-516-6153 <i class="fa fa-mobile margin-right-5"></i>Globe</p>
							<div class="clear"></div>

							<p class="f-left red-color font-12 margin-bottom-5"><strong>Transaction Time:</strong></p>
							<p class="f-right font-12">May 18, 2015 | 10:11:31</p>
							<div class="clear"></div>						
						</div>

						<!-- delivery address -->
						<div>
							<label class="margin-top-10 margin-bottom-10 ">Delivery Address:</label>
							<div class="bggray-light padding-all-5 font-14 small-curved-border">
								<div class="display-inline-mid margin-right-10 padding-left-20">
									<img src="../assets/images/work-icon.png" alt="work icon"/>
									<p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
								</div>
								<div class="display-inline-mid margin-left-10  padding-left-10">
									<p class="no-margin-all"><strong>Cr8v Web Solutions, Inc.</strong></p>
									<p class="no-margin-all">66C &amp; 66D, San Rafael St.<br/>Brgy. Kapitolyo, Pasig City. - NCR</p>
									<p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
								</div>							
							</div>							
						</div>

						<!-- cards -->
						<label class="margin-top-15">CARDS:</label>
						<div class="bggray-light padding-all-5 font-14 small-curved-border">
							<div class="display-inline-mid margin-right-10">
								<img class="thumb" src="../assets/images/happy-plus.jpg" alt="happy-plus"/>
							</div>
							<div class="display-inline-mid margin-left-10 divider padding-left-10">
								<p class="no-margin-all">0083-123456-46578<br/>
								<span class="red-color"><strong>Exp. Date:</strong></span> September 20, 2016</p>
							</div>
							<div class="display-inline-mid text-center margin-left-10">
								<a href="#">Show Card<br/>History</a>
							</div>
						</div>


						<div class="margin-top-10">
							<p class="f-left font-12 red-color  margin-bottom-5"><strong>Payment Method: </strong></p>
							<p class="f-right font-12">Cash</p>
							<div class="clear"></div>

							<p class="f-left font-12 red-color  margin-bottom-5"><strong>Change For: </strong></p>
							<p class="f-right font-12">500.00 PHP</p>
							<div class="clear"></div>

							<p class="f-left font-12 red-color  margin-bottom-5"><strong>Remarks: </strong></p>
							<p class="f-right font-12 gray-color">No Remarks</p>
							<div class="clear"></div>
							
						</div>
					</div>



					<div class="data-container split margin-left-15">
						<label class="margin-top-15">order: </label>
						 

						

						<div class="small-curved-border">
							<table class="font-14">
								<thead class="bggray-dark">
									<tr>
										<th class="padding-all-10">Quantitiy</th>
										<th class="padding-all-10">Product</th>
										<th class="padding-all-10">Price</th>
										<th class="padding-all-10">Subtotal</th>
									</tr>
								</thead>
								<tbody class="bggray-light">
									<tr>
										<td class="padding-all-10 text-center">2</td>
										<td class="padding-all-10"><div class="arrow-down"></div> Champ Amazing aloha + French Fries + Choice o..</td>
										<td class="padding-all-10">203.50</td>
										<td class="padding-all-10">407.00</td>
									</tr>
									<tr>
										<td class="padding-all-10 text-center gray-color">2</td>
										<td class="padding-all-10 padding-left-35">Large Coca-Cola</td>
										<td class="padding-all-10">17.60</td>
										<td class="padding-all-10">35.20</td>
									</tr>
									<tr>
										<td class="padding-all-10 text-center gray-color">2</td>
										<td class="padding-all-10 padding-left-35">Regular French Fries</td>
										<td class="padding-all-10">0.00</td>
										<td class="padding-all-10">0.00</td>
									</tr>
									<tr>
										<td class="padding-all-10" colspan="4"><hr/></td>
									</tr>
									<tr>
										<td class="padding-all-10 text-center">2</td>
										<td class="padding-all-10">Sundae Strawberry</td>
										<td class="padding-all-10">30.80</td>
										<td class="padding-all-10">61.60</td>
									</tr>
									<tr>
										<td class="padding-all-10" colspan="4"><hr/></td>
									</tr>
									<tr>
										<td class="padding-all-10 text-center">2</td>
										<td class="padding-all-10">Yum w/ Cheese</td>
										<td class="padding-all-10">44.00</td>
										<td class="padding-all-10">88.00</td>
									</tr>																		
									<tr class="bggray-middark">
										<td colspan="2" class="padding-all-10 text-left">Total Cost</td>
										<td colspan="2" class="padding-all-10 text-right">265.10 PHP</td>
									</tr>
									<tr class="bggray-middark">
										<td colspan="2" class="padding-all-10 text-left">Added VAT</td>
										<td colspan="2" class="padding-all-10 text-right">26.50 PHP</td>
									</tr>
									<tr class="bggray-middark">
										<td colspan="2" class="padding-all-10 text-left">Delivery Charge</td>
										<td colspan="2" class="padding-all-10 text-right">20.00 PHP</td>
									</tr>
									<tr class="bggray-dark">
										<td colspan="2" class="padding-all-10 text-left font-16"><strong>Total Bill</strong></td>
										<td colspan="2" class="padding-all-10 text-right font-14"><strong>611.80 PHP</strong></td>
									</tr>
								</tbody>
							</table>

						</div>

					</div>
					<div class="margin-top-20">
						<button type="button" class="btn btn-dark f-right margin-right-10 modal-trigger" modal-target="order-logs">Logs</button>	
						<button type="button" class="btn btn-dark f-right margin-right-10  modal-trigger" modal-target="void-order">Void Order</button>	
						<button type="button" class="btn btn-dark f-right margin-right-10  modal-trigger" modal-target="process-order">Process Order</button>	
						<button type="button" class="btn btn-dark f-right margin-right-10 modal-trigger">Unlock</button>				
						<div class="clear"></div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="content-container opaque">
        	<div>
                <div class="width-40per f-left">
                    <p class="font-16 margin-bottom-5"><strong>Order ID: 764687 </strong></p>
                    <p class="font-16 margin-bottom-5"><strong>NO STORE ASSIGNED</strong></p>
                    <p><strong><span class="red-color">Transaction Time: </span></strong> May 18, 2015 | 12:11 PM</p>
                </div>
                
                <div class="width-40per f-left">
                    <p class="margin-bottom-5"><strong><span class="red-color">PARKED ORDER</span></strong></p>
                    <p class="margin-bottom-5"><strong><span class="red-color">Name:</span></strong> Jun M. Andrada</p>
                    <p class="margin-bottom-5"><strong><span class="red-color">Contact No.:</span></strong> (02) 257-68-95 <i class="fa fa-phone font-16"></i> PLDT</p>
                </div>

                <div class="f-left margin-left-20">
                	<p class=""><i class="fa fa-lock fa-5x red-color margin-left-30"></i></p>
                	<p class="red-color font-12 "><strong>phub_hercgamboa</strong></p>
                </div>
                                                   
                <div class="clear"></div>
            </div>
		
		</div>
	</section>

	<!-- modal LOGS -->
	<div class="modal-container " modal-id="order-logs">
		<div class="modal-body small">
			<div class="modal-head">
				<h4 class="text-left">Order Logs</h4>
				<div class="modal-close close-me"></div>
			</div>
			<div class="modal-content">
				<label class="margin-bottom-10">AGENT LOGS:</label>
				<div class="tbl">
					<table class="no-margin-all">
						<thead>
							<tr>
								<th class="width-50per">Logs</th>
								<th>Date / Time</th>
								<th>Agents</th>
								<th>Call Center</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Start Over</td>
								<td  class="text-center">05-18-2015 | 12:41 PM</td>
								<td  class="text-center">Stephanie Jocson</td>
								<td class="text-center">PHUB</td>
							</tr>
							<tr>
								<td>Send Order</td>
								<td  class="text-center">05-18-2015 | 12:43 PM</td>
								<td  class="text-center">Stephanie Jocson</td>
								<td  class="text-center">PHUB</td>
							</tr>
							<tr>
								<td>Lock Order</td>
								<td  class="text-center">05-18-2015 | 12:43 PM</td>
								<td  class="text-center">Kevin Miranda </td>
								<td  class="text-center">PHUB</td>
							</tr>							
						</tbody>
					</table>					
				</div>

				<label class="margin-top-10 margin-bottom-10">Store Logs:</label>
				<div class="tbl">
					<table class="no-margin-all">
						<thead>
							<tr>
								<th class="width-70per">Store Name</th>
								<th>Logs</th>
								<th>Date / Time</th>								
							</tr>
						</thead>
						
					</table>					
				</div>				
			</div>
			<button type="button" class="btn btn-dark f-right margin-right-20 close-me">Close</button>
			<div class="clear"></div>
		</div>
	</div>

	<!-- modal  VOID ORDER  -->
	<div class="modal-container " modal-id="void-order">
		<div class="modal-body small">
			<div class="modal-head">
				<h4 class="text-left">Void Order</h4>
				<div class="modal-close close-me"></div>
			</div>

			<div class="modal-content no-margin-all">
				
				<!-- logo and name -->
				
				<img src="../assets/images/warning.svg" alt="warning message" class="width-20per f-left">

				<table>
					<tr>
						<td><p class="font-12 f-left margin-top-20 margin-left-20">Are you sure you want to void <strong>Jonathan R. Ornido's </strong> order?</p>
						</td>
					</tr>
					<tr>
						<td>
							<div class="bggray-dark margin-left-20 margin-top-10 padding-all-10">
								<p class="font-14 f-left margin-bottom-5"><strong>Order ID: 734784</strong></p>				
								<div class="clear"></div>

								<p class="font-12 f-left margin-bottom-5 margin-right-50"><strong><span class="red-color">Name: </span></strong></p>
								<p class="font-12 f-left">Jonathan R. Ornido</p>
								<div class="clear"></div>

								<p class="font-12 f-left margin-bottom-5 margin-right-20"><strong><span class="red-color">Contact No: </span></strong></p>
								<p class="font-12 f-left">(+63) 915-578-6147 <i class="fa fa-mobile"></i> Globe</p>
								<div class="clear"></div>
							</div>
						</td>
					</tr>							

				</table>
				
				<div class="clear"></div>
					
				
				
				

				



			</div>
			<!-- button -->
			<div class="margin-top-20">
				<button type="button" class="btn btn-dark f-right margin-right-20 close-me">Cancel</button>
				<button type="button" class="btn btn-dark f-right margin-right-20">Confirm</button>			
				<div class="clear"></div>
			</div>



		</div>
	</div>


	<!-- modal PROCESS ORDER-->
	<div class="modal-container" modal-id="process-order">

		<!-- modal body -->
		<div class="modal-body small">

			<!-- modal head -->
			<div class="modal-head ">
				<h4 class="text-left">Process Order</h4>
				<div class="modal-close close-me"></div>
			</div>

			<div class="bggray-light margin-top-20">
				<p class="font-12 text-left margin-left-40 margin-right-40 margin-bottom-10 padding-top-15">Please enter the correct address information inorder to 
					find proper Retail Trade Area (RTA) for this order.</p>
				<hr />
			</div>	
				

			<!-- modal content -->
			<div class="modal-content no-margin-all">			
				<!-- address -->
				<table class="margin-bottom-15 margin-left-15">
					<tr>
						<td><label>Address Type: <span class="red-color">*</span></label></td>
						<td><label>Address Label: <span class="red-color">*</span></label></td>
					</tr>
					<tr>
						<td>
							<div class="select margin-right-50">
								<select>
									<option value="Home">Home</option>
								</select>
							</div>
						</td>
						<td>
							<input type="text" class="normal">	
						</td>
					</tr>
				</table>
				<hr />
				<!-- province -->
				<table class="margin-top-15 margin-left-15">
					<tr>
						<td><label>Province: <span class="red-color">*</span></label></td>
						<td><label>City: <span class="red-color">*</span></label></td>
					</tr>
					<tr>
						<td>
							<div class="select margin-right-50">
								<select >
									<option value="Home">Home</option>
								</select>
							</div>
						</td>
						<td>
							<div class="select">
								<select>
									<option value="Mom's House">Mom's House</option>
								</select>
							</div>
						</td>
					</tr>
				</table>

				<div class="margin-left-15	">
					<label class="margin-top-10">Landmark:</label>
					<input type="text" class="xlarge">
				</div>
				
				<!-- 2 column table -->
				<table class="margin-top-10 margin-left-15">
					<tr>
						<td><label>House Number: </label></td>
						<td><label>Building: </label></td>
					</tr>
					<tr>
						<td><input type="text" class="normal margin-right-30"></td>
						<td><input type="text" class="normal"></td>
					</tr>
					<tr>
						<td><label class="margin-top-10">Unit: </label></td>
						<td><label>Floor: </label></td>
					</tr>
					<tr>
						<td><input type="text" class="normal"></td>
						<td><input type="text" class="normal"></td>
					</tr>
				</table>

				<!-- single column table -->
				<table class="margin-left-15">
					<tr>
						<td ><label class="margin-top-10">Street:</label></td>
					</tr>
					<tr>
						<td><input type="text" class="xlarge"></td>
					</tr>
					<tr>
						<td><label class="margin-top-10">Subdivision:</label></td>
					</tr>
					<tr>
						<td><input type="text" class="xlarge"></td>
					</tr>
					<tr>
						<td><label class="margin-top-10">Barangay:</label></td>
					</tr>
					<tr>
						<td><input type="text" class="xlarge"></td>
					</tr>				
				</table>			
			</div>

			<div class="bggray-light padding-bottom-20">
				<hr />
				<div class="text-left margin-left-40 margin-right-40 margin-top-15">
					<label >Nearest Store:</label>
					<br />

					<div class="text-center margin-top-10 margin-bottom-20">
						<button type="button" class="btn btn-dark margin-right-10">Find Retail Trade Area</button> 
						<button type="button" class="btn btn-dark">Hide Map</button>
					</div>

					<div class="bggray-dark padding-all-10">
						<p class="f-left font-14"><strong>MM Ortigas Roosevelt | JB0444</strong></p>
						<a href="#" class="f-right"><i class="fa fa-map-marker margin-right-10"></i>Show Map</a>
						<div class="clear"></div>

						<p class="f-left font-14"><strong><span class="red-color">Delivery Time:</span></strong></p>
						<p class="f-right font-14">20 Minutes</p>
						<div class="clear"></div>
					</div>

				</div>
			</div>


			<div class="f-right margin-right-40 margin-bottom-10 margin-top-20">
				<button type="button" class="btn btn-dark margin-right-10">Send Order </button>
				<button type="button" class="btn btn-dark close-me">Cancel</button>
			</div>
			<div class="clear"></div>
			
		</div>
	</div>
	
	
<?php include "../construct/footer.php"; ?>