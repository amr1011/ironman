<?php include "../construct/header.php"; ?>

	<section class="container-fluid" section-style="top-panel">
		<div class="row header-container">
			<div class="contents padding-all-20">
				<div class="select f-left">
					<select>
						<option value="show-all">Show All Agents</option>
						<option value="show-offline">Show Offline Agents</option>
						<option value="show-online">Show Online Agents</option>
					</select>
				</div>
				<button class="f-right btn btn-dark margin-left-15">FAQ</button>
				<button class="f-right btn btn-dark modal-trigger" modal-target="messenger">Messenger<div class="notify">99</div></button>
				<div class="clear"></div>

				<div class="data-container margin-top-15 text-center">
					<div class="data-content">
						<p class="title">metro manila</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">bacolod</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">bataan</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">batangas</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">bulacan</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">CDO</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">cavite</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">cebu</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">davao</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">ilo-ilo</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">laguna</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">nueva ecija</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">pampanga</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">pangasinan</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">tarlac</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">zambales</p>
						<p class="desc">10</p>
					</div>


				</div>
			</div>
		</div>

		<div class="row">
			<div class="contents margin-top-20">
				<label class="margin-bottom-5">search</label><br>
				<input class="search" type="text">
				<div class="select margin-left-20">
					<select>
						<option value="All Stores">All Stores</option>
						<option value="Offline Stores">Offline Stores</option>
						<option value="Online Stores">Online Stores</option>
					</select>
				</div>
				<button class="btn btn-dark margin-left-20">Search</button>
			</div>

			<div class="contents margin-top-20 line">
				<p class="bggray-white margin-top-5 f-right gray-color"><strong>170 Online Agents</strong></p>
				<span class="white-space f-right"></span>
				<div class="clear"></div>
			</div>
		</div>
	</section>

	<section class="container-fluid" section-style="content-panel">
		<div class="row">
			<div class="content-container unboxed">
				<table class="width-100per">
					<thead class="white-color">
						<tr>
							<th class="padding-all-10 light-red-bg color width-10per">Agent #</th>
							<th class="padding-all-10 dark-red-bg text-center width-45per">User Name</th>
							<th class="padding-all-10 light-red-bg text-center width-45per">Area </th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr class="bottom-border">
							<td class="padding-all-10">473</td>
							<td class="padding-all-10">JFC_Coord201</td>
							<td class="padding-all-10">Metro Manila</td>
						</tr>
						<tr class="bottom-border">
							<td class="padding-all-10">473</td>
							<td class="padding-all-10">JFC_Coord201</td>
							<td class="padding-all-10">Metro Manila</td>
						</tr>
						<tr class="bottom-border">
							<td class="padding-all-10">473</td>
							<td class="padding-all-10">JFC_Coord201</td>
							<td class="padding-all-10">Metro Manila</td>
						</tr>
						<tr class="bottom-border">
							<td class="padding-all-10">473</td>
							<td class="padding-all-10">JFC_Coord201</td>
							<td class="padding-all-10">Metro Manila</td>
						</tr>
						<tr class="bottom-border">
							<td class="padding-all-10">473</td>
							<td class="padding-all-10">JFC_Coord201</td>
							<td class="padding-all-10">Metro Manila</td>
						</tr>
						<tr class="bottom-border">
							<td class="padding-all-10">473</td>
							<td class="padding-all-10">JFC_Coord201</td>
							<td class="padding-all-10">Metro Manila</td>
						</tr>
						<tr class="bottom-border">
							<td class="padding-all-10">473</td>
							<td class="padding-all-10">JFC_Coord201</td>
							<td class="padding-all-10">Metro Manila</td>
						</tr>
						<tr class="bottom-border">
							<td class="padding-all-10">473</td>
							<td class="padding-all-10">JFC_Coord201</td>
							<td class="padding-all-10">Metro Manila</td>
						</tr>
						<tr class="bottom-border">
							<td class="padding-all-10">473</td>
							<td class="padding-all-10">JFC_Coord201</td>
							<td class="padding-all-10">Metro Manila</td>
						</tr>
						<tr class="bottom-border">
							<td class="padding-all-10">473</td>
							<td class="padding-all-10">JFC_Coord201</td>
							<td class="padding-all-10">Metro Manila</td>
						</tr>
						<tr class="bottom-border">
							<td class="padding-all-10">473</td>
							<td class="padding-all-10">JFC_Coord201</td>
							<td class="padding-all-10">Metro Manila</td>
						</tr>
						<tr class="bottom-border">
							<td class="padding-all-10">473</td>
							<td class="padding-all-10">JFC_Coord201</td>
							<td class="padding-all-10">Metro Manila</td>
						</tr>
						<tr class="bottom-border">
							<td class="padding-all-10">473</td>
							<td class="padding-all-10">JFC_Coord201</td>
							<td class="padding-all-10">Metro Manila</td>
						</tr>
						<tr class="bottom-border">
							<td class="padding-all-10">473</td>
							<td class="padding-all-10">JFC_Coord201</td>
							<td class="padding-all-10">Metro Manila</td>
						</tr>
						<tr class="bottom-border">
							<td class="padding-all-10">473</td>
							<td class="padding-all-10">JFC_Coord201</td>
							<td class="padding-all-10">Metro Manila</td>
						</tr>
						<tr class="bottom-border">
							<td class="padding-all-10">473</td>
							<td class="padding-all-10">JFC_Coord201</td>
							<td class="padding-all-10">Metro Manila</td>
						</tr>
						<tr class="bottom-border">
							<td class="padding-all-10">473</td>
							<td class="padding-all-10">JFC_Coord201</td>
							<td class="padding-all-10">Metro Manila</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</section>

	<!-- modal messenger -->
	<div class="modal-container" modal-id="messenger">
		<div class="modal-body">
			<div class="modal-head ">
				<h4 class="text-left">Manual Order - Order Summary</h4>				
				<div class="modal-close close-me"></div>
			</div>

			<!-- content -->
			<div class="modal-content messenger">
				<!-- messages -->
				<div class="msg-content">
					<div class="chat-box">
						<p class="text-left font-12"><span class="gray-color">May 18, 2015 (12:57 PM)</span> - MM Sampaloc | JB1021</p>
						<div class="sender">
							<img class="profile" src="../assets/images/jollibe-face.png"/>
							<div class="msg">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis felis consectetur, sodales nunc quis, sagittis augue. Proin eu molestie libero, vitae volutpat eros.</p><br/>
								<p>Sodales nunc quis, sagittis augue. Proin eu molestie libero, vitae volutpat eros.</p><br/>
								<p>TY</p>
							</div>
						</div>

						<p class="text-right font-12"><span class="gray-color">May 18, 2015 (1:45 PM)</span> - MM Q.C. | JB1021</p>
						<div class="receiver">
							<div class="msg">
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis felis consectetur, sodales nunc quis, sagittis augue. Proin eu molestie libero, vitae volutpat eros.</p><br/>
								<p>Sodales nunc quis, sagittis augue. Proin eu molestie libero, vitae volutpat eros.</p><br/>
								<p>TY</p>
							</div>
							<img class="profile" src="../assets/images/profile-pic.jpg"/>
						</div>

						<p class="text-left font-12"><span class="gray-color">May 18, 2015 (1:46 PM)</span> - MM Sampaloc | JB1021</p>
						<div class="sender">
							<img class="profile" src="../assets/images/jollibe-face.png"/>
							<div class="msg">
								<p>Thanks :)</p>
							</div>
						</div>
					</div>
					<!-- text input -->
					<div class="text-input padding-all-10">
						<textarea row="5" class="margin-top-20"></textarea>
						<div class="btn-container padding-top-20 margin-top-20">
							<button class="btn btn-dark width-100per">Send</button>
							<button class="btn btn-dark margin-top-10 margin-right-10 width-50per"><img class="width-90per" src="../assets/images/ui/smile.png"></button>
							<button class="btn btn-dark margin-top-10 margin-left-10 no-padding-hor width-50per close-me">Cancel</button>
						</div>
					</div>
				</div>

				<!-- contacts -->
				<div class="contacts">
					<label>Search Store:</label>
					<input type="text" class="width-100per normal"/>
					<div class="content">
						<table class="width-100per">
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
							<tr>
								<td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php include "../construct/footer.php"; ?>