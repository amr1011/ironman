
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keywords" content="">
        <link rel="shortcut icon" href="assets/ico/favicon.ico">

        <title>Ironman Categroy Management | Add Category</title>

        <!-- Bootstrap core CSS -->
        <link href="assets/css/bootstrap/bootstrap.min.css" rel="stylesheet">
        <link href="assets/css/bootstrap/bootstrap-theme.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="assets/css/font-awesome/font-awesome.min.css" rel="stylesheet">

        <!-- Core CSS -->
        <link href="assets/css/global/commons.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="assets/css/global/header.css" rel="stylesheet">
        <link href="assets/css/global/ui-widgets.css" rel="stylesheet">
        <link href="assets/css/global/unique-widget-style.css" rel="stylesheet">
        <link href="assets/css/global/section-top-panel.css" rel="stylesheet">
        <link href="assets/css/global/section-content-panel.css" rel="stylesheet">
        <link href="assets/css/global/login.css" rel="stylesheet">
    </head>
    <body>
        <section class="login-parent">
            <div class="login-form">
                <form>
                    <img class="call-center-logo" src="assets/images/jollibe-logo-call-center-white.svg" alt="jollibee logo">
                    <div class="form-group">
                        <p>Username</p>
                        <input type="text" class="normal">
                    </div>
                    <div class="form-group">
                        <p>Password</p>
                        <input type="password" class="normal">
                    </div>
                    <p><a href="">Forgot your password?</a></p>
                    <button type="button" class="btn btn-dark"> Sign in</button>
                </form>

                <div  class="image-content1">
                    <table border="0">
                        <tr>
                            <td>
                                <a href="#" alt="Jollibee Link">
                                    <img src="assets/images/affliates/jb-logo.png" alt="Jollibee Logo">
                                </a>
                            </td>
                            <td>
                                <a href="#" alt="Chowking Link">
                                    <img src="assets/images/affliates/ck-logo.png" alt="Chowking Logo">
                                </a>
                            </td>
                            <td>
                                <a href="#" alt="Greenwich Link">
                                    <img src="assets/images/affliates/gw-logo.png" alt="Greenwich Logo">
                                </a>
                            </td>
                            <td>                                
                                <a href="#" alt="Mang Inasal Link">
                                    <img src="assets/images/affliates/mi-logo.png" alt="MangInasal Logo">
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="image-content2">
                    <table border="0">
                        <tr>
                            <td>
                            </td>
                            <td>
                                <a href="#" alt="RedRibbon Logo">
                                    <img src="assets/images/affliates/rb-logo.png" alt="RedRibbon Logo">
                                </a>
                            </td>
                            <td>
                                <a href="#" alt="Burger King Logo">
                                    <img src="assets/images/affliates/bk-logo.png" alt="BurgerKing Logo">
                                </a>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>                        
                </div>
            </div> 

        </section>
    </body>
</html>