<?php include "../construct/header.php"; ?>

    <section class="container-fluid" section-style="top-panel">


        <!-- search order -->
        <div class="row header-container">


            <div class="contents">
                <div class="breadcrumbs f-left  margin-top-20">
                    <p><a href="#">DST Report</a> <i class="fa fa-angle-right"></i> <strong>May 02, 2015 (View Data)</strong></p>
                </div>                
                <div class="clear"></div>
            </div>

            <div class="contents">

                <h1 class="f-left">May 02, 2015</h1>
                <div class="f-right margin-top-20">
                    <button class="btn btn-dark / margin-right-10">Back to DST Report</button>
                    <button class="btn btn-dark">FAQ</button>
                </div>
                <div class="clear"></div>
            </div>
        </div>

        
        <div class="row">
            <div class="contents margin-top-20">


                <!-- rounded boxes -->
                <div class= " margin-bottom-20">
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / medium  ">
                            <p class="font-16 / padding-top-15">20 Minutes | 0 - 500 PHP</p>
                            <br />
                            <div><strong>
                                <p class="f-left / red-color / margin-left-20">Yes:</p>
                                <p class="f-right / margin-right-20">10</p>
                                <div class="clear"></div>
                                <br />

                                <p class="f-left / red-color / margin-left-20">No:</p>
                                <p class="f-right / margin-right-20">15</p>
                                <div class="clear"></div>
                                <br />

                                <p class="f-left / red-color / margin-left-20">Total:</p>
                                <p class="f-right / margin-right-20">25</p>
                                <div class="clear"></div>
                                <br />

                                <p class="f-left / red-color / margin-left-20">Percentage:</p>
                                <p class="f-right / margin-right-20 / margin-bottom-20">95.3%</p>
                                <div class="clear"></div>
                                <br />

                                                            
                                </strong>
                            </div>  
                        </div>
                    </div>
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / medium">
                            <p class="font-16 / padding-top-15">30 Minutes | 0 - 1100 PHP</p>
                            <br />
                            <div><strong>
                                <p class="f-left / red-color / margin-left-20">Yes:</p>
                                <p class="f-right / margin-right-20">10</p>
                                <div class="clear"></div>
                                <br />

                                <p class="f-left / red-color / margin-left-20">No:</p>
                                <p class="f-right / margin-right-20">15</p>
                                <div class="clear"></div>
                                <br />

                                <p class="f-left / red-color / margin-left-20">Total:</p>
                                <p class="f-right / margin-right-20">25</p>
                                <div class="clear"></div>
                                <br />

                                <p class="f-left / red-color / margin-left-20">Percentage:</p>
                                <p class="f-right / margin-right-20 / margin-bottom-20">95.3%</p>
                                <div class="clear"></div>
                                <br />

                                                            
                                </strong>
                            </div>  
                        </div>
                    </div>

                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / medium">
                           <p class="font-16 / padding-top-15">45 Minutes | 0 - 500 PHP</p>
                            <br />
                            <div><strong>
                                <p class="f-left / red-color / margin-left-20">Yes:</p>
                                <p class="f-right / margin-right-20">10</p>
                                <div class="clear"></div>
                                <br />

                                <p class="f-left / red-color / margin-left-20">No:</p>
                                <p class="f-right / margin-right-20">15</p>
                                <div class="clear"></div>
                                <br />

                                <p class="f-left / red-color / margin-left-20">Total:</p>
                                <p class="f-right / margin-right-20">25</p>
                                <div class="clear"></div>
                                <br />

                                <p class="f-left / red-color / margin-left-20">Percentage:</p>
                                <p class="f-right / margin-right-20 / margin-bottom-20">95.3%</p>
                                <div class="clear"></div>
                                <br />
                                                        
                                </strong>
                            </div>  
                        </div>                    
                    </div>
                    <div class="clear"></div>

                <!-- rounded small boxes -->
                <div class="margin-bottom-20 / margin-top-20">
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / small">
                            <p class="font-20 padding-top-20 / padding-bottom-10">Advance Order</p>
                            <br />
                            <p class="font-20 / padding-bottom-15">10</p>
                        </div>
                    </div>
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / small">
                            <p class="font-20 padding-top-20 / padding-bottom-10">Bulk Order</p>
                            <br />
                            <p class="font-20 / padding-bottom-10 / padding-top-30">13</p>
                        </div>
                    </div>
                    
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / small">
                            <p class="font-20 padding-top-20 / padding-bottom-10 ">Exception</p>
                            <br />
                           <p class="font-20 padding-bottom-10 / padding-top-30 ">5</p>
                        </div>
                    </div>
                    
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / small">
                            <p class="font-20 padding-top-20 / padding-bottom-10">Total Hitrate</p>
                            <br />
                            <div class="margin-left-10 / margin-right-10 / padding-bottom-10">
                                <strong>
                                <p class="f-left / red-color">Yes:</p>
                                <p class="f-right">20</p>
                                <div class="clear"></div>

                                <p class="f-left / red-color">No:</p>
                                <p class="f-right">45</p>
                                <div class="clear"></div>

                                <p class="f-left / red-color">Total:</p>
                                <p class="f-right">25</p>
                                <div class="clear"></div>
                                </strong>
                            </div>
                        </div>
                    </div>

                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / medium">
                            <p class="font-20 padding-top-15">Total Hitrate Percentage</p>
                            <br />
                            <h1 class=" / red-color / padding-bottom-35 ">94.5%</h1>
                        </div>
                    </div>

                    <div class="clear"></div>
                </div>

                <!-- search -->
                <div class="f-left">
                    <label class="margin-bottom-5">search:</label><br>
                    <input class="search f-left xlarge" type="text">
                </div>
                <!-- search by -->
                <div class="f-left margin-left-20">
                    <label class="margin-bottom-5">search by:</label><br>
                    <div class="select xlarge">
                        <select>
                            <option value="Order ID">Order ID</option>                          
                        </select>
                    </div>
                    <buton type="button" class="btn btn-dark / margin-left-20">Search</buton>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        
        <div class="row margin-top-20">
            <div class="contents line">
                <div class="select xlarge">
                    <select>
                        <option value="All Transaction">Show all Transaction</option>
                        <option value="Accepted Transaction">Show Accepted Transaction</option>
                        <option value="Assembling Transaction">Show Assembling Transaction</option>
                        <option value="Dispatch Transaction">Show for Dispatch Transaction</option>
                        <option value="Rider Out">Show Rider Out Transaction</option>
                        <option value="Rider In">Show RIder In Transaction</option>
                        <option value="Completed Transaction">Show Completed Transaction</option>                        
                    </select>
                </div>
                <span class="white-space"></span>
                
                
                <span class="white-space"></span>
                
                <div class="f-right bggray-white">
                    <p class="f-left font-12 padding-left-10 padding-top-5">
                        <strong>Sort By:</strong>
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">   
                        <strong>Customer Name</strong>  
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">  
                        <strong>Order ID</strong>     
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">  
                        <strong>Date / Time Ordered</strong>     
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-top-5">
                        <a class="red-color active" href="">
                            <strong>Hitrate</strong>
                            <img src="../assets/images/ui/sort-top-arrow.png">
                        </a>
                    </p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </section>

    <section class="container-fluid" section-style="content-panel">
        <div class="row">

            <!-- sample-1-->
           
            <div class="content-container viewable ">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>                        
                             <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per / f-left / margin-top-25">                                        
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Date/Time Accepted: </span></strong>May 18, 2015 | 11:42:02</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Date/Time Accepted: </span></strong>May 18, 2015 | 11:42:02</p>
                    </div>
                    
                    <div class="width-25per f-right">                        
                        <label class="font-20 / gray-color / margin-left-10">Hitrate</label>
                        <h1 class="no-margin-all">100.0 %</h1>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="content-container viewable ">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>                        
                             <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per / f-left / margin-top-25">                                        
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Date/Time Accepted: </span></strong>May 18, 2015 | 11:42:02</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Date/Time Accepted: </span></strong>May 18, 2015 | 11:42:02</p>
                    </div>
                    
                    <div class="width-25per f-right">                        
                        <label class="font-20 / gray-color / margin-left-10">Hitrate</label>
                        <h1 class="no-margin-all">100.0 %</h1>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="content-container viewable ">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>                        
                             <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per / f-left / margin-top-25">                                        
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Date/Time Accepted: </span></strong>May 18, 2015 | 11:42:02</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Date/Time Accepted: </span></strong>May 18, 2015 | 11:42:02</p>
                    </div>
                    
                    <div class="width-25per f-right">                        
                        <label class="font-20 / gray-color / margin-left-10">Hitrate</label>
                        <h1 class="no-margin-all">100.0 %</h1>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="content-container">
                <div>
                    <div class="f-left margin-top-15">
                        <p class="font-20"><strong>Order ID: 954458</strong></p>    
                                                                          
                    </div>                  
                    
                     <div class="width-25per f-right / margin-bottom-10">                        
                        <label class="font-20 / gray-color / margin-left-15">Hitrate</label>
                        <h1 class="no-margin-all">100.0 %</h1>
                    </div>
                       
                                            
                    
                    <div class="clear"></div>
                    <hr>

                    <div class="data-container split">
                        
                        <!-- clients information -->
                        <div class="margin-top-20">
                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Name: </strong></p>
                            <p class="f-right font-12">Jonathan R. Omido</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Contact Number: </strong></p>
                            <p class="f-right font-12">(+63) 915-516-6153 <i class="fa fa-mobile fa-2x margin-right-5"></i>Globe</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Transaction Time:</strong></p>
                            <p class="f-right font-12">May 18, 2015 | 10:11:31</p>
                            <div class="clear"></div>                       
                        </div>

                        <!-- delivery address -->
                        <div>
                            <label class="margin-top-10 margin-bottom-10 ">Delivery Address:</label>
                            <div class="bggray-light padding-all-5 font-14 small-curved-border">
                                <div class="display-inline-mid margin-right-10 padding-left-20">
                                    <img src="../assets/images/work-icon.png" alt="work icon">
                                    <p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
                                </div>
                                <div class="display-inline-mid margin-left-10  padding-left-10">
                                    <p class="no-margin-all"><strong>Cr8v Web Solutions, Inc.</strong></p>
                                    <p class="no-margin-all">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR</p>
                                    <p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
                                </div>                          
                            </div>                          
                        </div>

                        <!-- cards -->
                        <label class="margin-top-15">CARDS:</label>
                        <div class="bggray-light padding-all-5 font-14 small-curved-border">
                            <div class="display-inline-mid margin-right-10">
                                <img class="thumb" src="../assets/images/happy-plus.jpg" alt="happy-plus">
                            </div>
                            <div class="display-inline-mid margin-left-10 divider padding-left-10">
                                <p class="no-margin-all">0083-123456-46578<br>
                                <span class="red-color"><strong>Exp. Date:</strong></span> September 20, 2016</p>
                            </div>
                            <div class="display-inline-mid text-center margin-left-10">
                                <a href="#">Show Card<br>History</a>
                            </div>
                        </div>


                        <div class="margin-top-10">
                            <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Payment Method: </strong></p>
                            <p class="f-right / font-12">Cash</p>
                            <div class="clear"></div>

                            <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Change For: </strong></p>
                            <p class="f-right / font-12">500.00 PHP</p>
                            <div class="clear"></div>

                            <p class="f-left font-12 red-color  margin-bottom-5"><strong>Remarks: </strong></p>
                            <p class="f-right font-12 gray-color">No Remarks</p>
                            <div class="clear"></div>
                            
                        </div>
                    </div>

                    <div class="data-container split margin-left-15">

                        <label class="margin-top-15">order: </label>
                        <div class="small-curved-border">
                            <table class="font-14">
                                <thead class="bggray-dark">
                                    <tr>
                                        <th class="padding-all-10">Quantitiy</th>
                                        <th class="padding-all-10">Product</th>
                                        <th class="padding-all-10">Price</th>
                                        <th class="padding-all-10">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody class="bggray-light">
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10"><div class="arrow-down"></div> Champ Amazing aloha + French Fries + Choice o..</td>
                                        <td class="padding-all-10">203.50</td>
                                        <td class="padding-all-10">407.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Large Coca-Cola</td>
                                        <td class="padding-all-10">17.60</td>
                                        <td class="padding-all-10">35.20</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Regular French Fries</td>
                                        <td class="padding-all-10">0.00</td>
                                        <td class="padding-all-10">0.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10">Sundae Strawberry</td>
                                        <td class="padding-all-10">30.80</td>
                                        <td class="padding-all-10">61.60</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10">Yum w/ Cheese</td>
                                        <td class="padding-all-10">44.00</td>
                                        <td class="padding-all-10">88.00</td>
                                    </tr>                                                                       
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Total Cost</td>
                                        <td colspan="2" class="padding-all-10 text-right">265.10 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Added VAT</td>
                                        <td colspan="2" class="padding-all-10 text-right">26.50 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Delivery Charge</td>
                                        <td colspan="2" class="padding-all-10 text-right">20.00 PHP</td>
                                    </tr>
                                    <tr class="bggray-dark">
                                        <td colspan="2" class="padding-all-10 text-left font-16"><strong>Total Bill</strong></td>
                                        <td colspan="2" class="padding-all-10 text-right font-14"><strong>611.80 PHP</strong></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>

                    </div>
                    <div class="margin-top-20">
                       
                        
                        <button type="button" class="btn btn-dark f-right margin-right-10 modal-trigger" modal-target="reject-order">Order Logs</button>
                        
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
                             



            

        </div>
    </div>


<?php include "../construct/footer.php"; ?>