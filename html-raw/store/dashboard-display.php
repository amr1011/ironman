<?php include "../construct/header.php"; ?>

	<section class="container-fluid" section-style="top-panel">


        <!-- search order -->
		<div class="row header-container">
			<div class="contents">
				<h1 class="f-left">Dashboard</h1>
				<div class="f-right margin-top-20">
                    
					<button class="btn btn-dark">FAQ</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>

        
		<div class="row">
			<div class="contents margin-top-20">


                <!-- rounded boxes -->
                <div class= " margin-bottom-20">
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / small  ">
                            <p class="font-16 / padding-top-15">Waiting</p>
                            <br />
                            <p class="font-16 / padding-bottom-10">2</p>
                        </div>
                    </div>
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / small">
                            <p class="font-16  padding-top-15">Assembling</p>
                            <br />
                            <p class="font-16 / padding-bottom-10">2</p>
                        </div>
                    </div>

                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / small">
                            <p class="font-16  padding-top-15">For Dispatch</p>
                            <br />
                            <p class="font-16 / padding-bottom-10">2</p>
                        </div>
                    </div>
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / small">
                            <p class="font-16  padding-top-15">Rider Out</p>
                            <br />
                            <p class="font-16 / padding-bottom-10 ">1</p>
                        </div>
                    </div>

                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / small">
                            <p class="font-16  padding-top-15">Rider Out</p>
                            <br />
                            <p class="font-16 / padding-bottom-10">0</p>
                        </div>
                    </div>
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / small">
                            <p class="font-16  padding-top-15">For Pick-up</p>
                            <br />
                            <p class="font-16 / padding-bottom-10">0</p>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>

                <!-- rounded big boxes -->
                <div class="margin-bottom-20">
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / medium">
                            <p class="font-16 padding-top-15">Completed On-Time</p>
                            <br />
                            <p class="font-20 / padding-bottom-15">245</p>
                        </div>
                    </div>
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / medium">
                            <p class="font-16 padding-top-15">Total Transaction (Today)</p>
                            <br />
                            <p class="font-20 / padding-bottom-15">253</p>
                        </div>
                    </div>
                    
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / medium">
                            <p class="font-16 padding-top-15">Hit-Rate (Today)</p>
                            <br />
                            <p class="font-20 / red-color / padding-bottom-15 ">94.5%</p>
                        </div>
                    </div>
                    
                    <div class="clear"></div>
                </div>

                <!-- search -->
				<div class="f-left">
					<label class="margin-bottom-5">search:</label><br>
					<input class="search f-left xlarge" type="text">
				</div>
                <!-- search by -->
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">search by:</label><br>
					<div class="select xlarge">
						<select>
							<option value="Order ID">Order ID</option>							
						</select>
					</div>
                    <buton type="button" class="btn btn-dark / margin-left-20">Search</buton>
				</div>
               
			</div>
		</div>
        
        <div class="row margin-top-20">
            <div class="contents line">
                <div class="select xlarge">
					<select>
						<option value="All Transaction">Show all Transaction</option>
						<option value="Accepted Transaction">Show Accepted Transaction</option>
                        <option value="Assembling Transaction">Show Assembling Transaction</option>
                        <option value="Dispatch Transaction">Show for Dispatch Transaction</option>
                        <option value="Rider Out">Show Rider Out Transaction</option>
                        <option value="Rider In">Show RIder In Transaction</option>
                        <option value="Completed Transaction">Show Completed Transaction</option>                        
					</select>
				</div>
				<span class="white-space"></span>
                
                
				<span class="white-space"></span>
                
                <div class="f-right bggray-white">
                    <p class="f-left font-12 padding-left-10 padding-top-5">
                        <strong>Sort By:</strong>
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">   
                        <strong>Customer Name</strong>  
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">  
                        <strong>Call Center</strong>     
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-top-5">
                        <a class="red-color active" href="">
                            <strong>Criticality</strong>
                            <img src="../assets/images/ui/sort-top-arrow.png">
                        </a>
                    </p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
	</section>

	<section class="container-fluid" section-style="content-panel">
		<div class="row">

			<!-- sample-1-->
            <div class="content-container unboxed ">
                <h3 class="f-left no-margin-all">Waiting Transactions</h3>                
                <p class="f-right font-14 no-margin-bottom"><strong>1 Transaction</strong></p>
                <div class="clear"></div>
            </div>
			<div class="content-container viewable opaque">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>House Address # 1</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">Rm. 404 3rd Floor, 691 Eduaredo Bldg, GreenField St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Beside gas station</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20    ">
                        
                        <button type="button" class="btn btn-dark / margin-top-10">Confirm Order</button>
                    </div>
                    <div class="clear"></div>
                </div>
			</div>
            <div class="content-container">
                <div>
                    <div class="f-left margin-top-15">
                        <p class="font-20"><strong>Order ID: 954458</strong></p>    
                                                                          
                    </div>                  
                    <div class=" f-right margin-bottom-10">
                        <p class="font-16 margin-bottom-5 gray-color"><strong>WAITING ORDER</strong></p>                                                                        
                        <p class="font-14 margin-bottom-5  "><strong><span class="red-color">Elapsed Time:</span></strong> 00:01:36</p>
                                            
                    </div>
                    <div class="clear"></div>
                    <hr>

                    <div class="data-container split">
                        
                        <!-- clients information -->
                        <div class="margin-top-20">
                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Name: </strong></p>
                            <p class="f-right font-12">Jonathan R. Omido</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Contact Number: </strong></p>
                            <p class="f-right font-12">(+63) 915-516-6153 <i class="fa fa-mobile fa-2x margin-right-5"></i>Globe</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Transaction Time:</strong></p>
                            <p class="f-right font-12">May 18, 2015 | 10:11:31</p>
                            <div class="clear"></div>                       
                        </div>

                        <!-- delivery address -->
                        <div>
                            <label class="margin-top-10 margin-bottom-10 ">Delivery Address:</label>
                            <div class="bggray-light padding-all-5 font-14 small-curved-border">
                                <div class="display-inline-mid margin-right-10 padding-left-20">
                                    <img src="../assets/images/work-icon.png" alt="work icon">
                                    <p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
                                </div>
                                <div class="display-inline-mid margin-left-10  padding-left-10">
                                    <p class="no-margin-all"><strong>Cr8v Web Solutions, Inc.</strong></p>
                                    <p class="no-margin-all">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR</p>
                                    <p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
                                </div>                          
                            </div>                          
                        </div>

                        <!-- cards -->
                        <label class="margin-top-15">CARDS:</label>
                        <div class="bggray-light padding-all-5 font-14 small-curved-border">
                            <div class="display-inline-mid margin-right-10">
                                <img class="thumb" src="../assets/images/happy-plus.jpg" alt="happy-plus">
                            </div>
                            <div class="display-inline-mid margin-left-10 divider padding-left-10">
                                <p class="no-margin-all">0083-123456-46578<br>
                                <span class="red-color"><strong>Exp. Date:</strong></span> September 20, 2016</p>
                            </div>
                            <div class="display-inline-mid text-center margin-left-10">
                                <a href="#">Show Card<br>History</a>
                            </div>
                        </div>


                        <div class="margin-top-10">
                            <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Payment Method: </strong></p>
                            <p class="f-right / font-12">Cash</p>
                            <div class="clear"></div>

                            <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Change For: </strong></p>
                            <p class="f-right / font-12">500.00 PHP</p>
                            <div class="clear"></div>

                            <p class="f-left font-12 red-color  margin-bottom-5"><strong>Remarks: </strong></p>
                            <p class="f-right font-12 gray-color">No Remarks</p>
                            <div class="clear"></div>
                            
                        </div>
                    </div>

                    <div class="data-container split margin-left-15">

                        <label class="margin-top-15">order: </label>
                        <div class="small-curved-border">
                            <table class="font-14">
                                <thead class="bggray-dark">
                                    <tr>
                                        <th class="padding-all-10">Quantitiy</th>
                                        <th class="padding-all-10">Product</th>
                                        <th class="padding-all-10">Price</th>
                                        <th class="padding-all-10">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody class="bggray-light">
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10"><div class="arrow-down"></div> Champ Amazing aloha + French Fries + Choice o..</td>
                                        <td class="padding-all-10">203.50</td>
                                        <td class="padding-all-10">407.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Large Coca-Cola</td>
                                        <td class="padding-all-10">17.60</td>
                                        <td class="padding-all-10">35.20</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Regular French Fries</td>
                                        <td class="padding-all-10">0.00</td>
                                        <td class="padding-all-10">0.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10">Sundae Strawberry</td>
                                        <td class="padding-all-10">30.80</td>
                                        <td class="padding-all-10">61.60</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10">Yum w/ Cheese</td>
                                        <td class="padding-all-10">44.00</td>
                                        <td class="padding-all-10">88.00</td>
                                    </tr>                                                                       
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Total Cost</td>
                                        <td colspan="2" class="padding-all-10 text-right">265.10 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Added VAT</td>
                                        <td colspan="2" class="padding-all-10 text-right">26.50 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Delivery Charge</td>
                                        <td colspan="2" class="padding-all-10 text-right">20.00 PHP</td>
                                    </tr>
                                    <tr class="bggray-dark">
                                        <td colspan="2" class="padding-all-10 text-left font-16"><strong>Total Bill</strong></td>
                                        <td colspan="2" class="padding-all-10 text-right font-14"><strong>611.80 PHP</strong></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>

                    </div>
                    <div class="margin-top-20">
                       
                        
                        <button type="button" class="btn btn-dark f-right margin-right-10 modal-trigger" modal-target="reject-order">Reject Order</button>
                        <button type="button" class="btn btn-dark f-right margin-right-10 ">Accept Order</button>                           
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
               <div class="content-container">
                <div>
                    <div class="f-left margin-top-15">
                        <p class="font-20"><strong>Order ID: 9544582</strong></p>    
                                                                          
                    </div>                  
                    <div class=" f-right margin-bottom-10">
                        <p class="font-16 margin-bottom-5 gray-color"><strong>WAITING ORDER</strong></p>                                                                        
                        <p class="font-14 margin-bottom-5  "><strong><span class="red-color">Elapsed Time:</span></strong> 00:01:36</p>
                                            
                    </div>
                    <div class="clear"></div>
                    <hr>

                    <div class="data-container split">
                        
                        <!-- clients information -->
                        <div class="margin-top-20">
                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Name: </strong></p>
                            <p class="f-right font-12">Jonathan R. Omido</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Contact Number: </strong></p>
                            <p class="f-right font-12">(+63) 915-516-6153 <i class="fa fa-mobile fa-2x margin-right-5"></i>Globe</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Transaction Time:</strong></p>
                            <p class="f-right font-12">May 18, 2015 | 10:11:31</p>
                            <div class="clear"></div>                       
                        </div>

                        <!-- delivery address -->
                        <div>
                            <label class="margin-top-10 margin-bottom-10 ">Delivery Address:</label>
                            <div class="bggray-light padding-all-5 font-14 small-curved-border">
                                <div class="display-inline-mid margin-right-10 padding-left-20">
                                    <img src="../assets/images/work-icon.png" alt="work icon">
                                    <p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
                                </div>
                                <div class="display-inline-mid margin-left-10  padding-left-10">
                                    <p class="no-margin-all"><strong>Cr8v Web Solutions, Inc.</strong></p>
                                    <p class="no-margin-all">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR</p>
                                    <p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
                                </div>                          
                            </div>                          
                        </div>

                        <!-- cards -->
                        <label class="margin-top-15">CARDS:</label>
                        <div class="bggray-light padding-all-5 font-14 small-curved-border">
                            <div class="display-inline-mid margin-right-10">
                                <img class="thumb" src="../assets/images/happy-plus.jpg" alt="happy-plus">
                            </div>
                            <div class="display-inline-mid margin-left-10 divider padding-left-10">
                                <p class="no-margin-all">0083-123456-46578<br>
                                <span class="red-color"><strong>Exp. Date:</strong></span> September 20, 2016</p>
                            </div>
                            <div class="display-inline-mid text-center margin-left-10">
                                <a href="#">Show Card<br>History</a>
                            </div>
                        </div>


                        <div class="margin-top-10">
                            <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Payment Method: </strong></p>
                            <p class="f-right / font-12">Cash</p>
                            <div class="clear"></div>

                            <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Change For: </strong></p>
                            <p class="f-right / font-12">500.00 PHP</p>
                            <div class="clear"></div>

                            <p class="f-left font-12 red-color  margin-bottom-5"><strong>Remarks: </strong></p>
                            <p class="f-right font-12 gray-color">No Remarks</p>
                            <div class="clear"></div>
                            
                        </div>
                    </div>

                    <div class="data-container split margin-left-15">

                        <label class="margin-top-15">order: </label>
                        <div class="small-curved-border">
                            <table class="font-14">
                                <thead class="bggray-dark">
                                    <tr>
                                        <th class="padding-all-10">Quantitiy</th>
                                        <th class="padding-all-10">Product</th>
                                        <th class="padding-all-10">Price</th>
                                        <th class="padding-all-10">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody class="bggray-light">
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10"><div class="arrow-down"></div> Champ Amazing aloha + French Fries + Choice o..</td>
                                        <td class="padding-all-10">203.50</td>
                                        <td class="padding-all-10">407.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Large Coca-Cola</td>
                                        <td class="padding-all-10">17.60</td>
                                        <td class="padding-all-10">35.20</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Regular French Fries</td>
                                        <td class="padding-all-10">0.00</td>
                                        <td class="padding-all-10">0.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10">Sundae Strawberry</td>
                                        <td class="padding-all-10">30.80</td>
                                        <td class="padding-all-10">61.60</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center"><a href="#">2</a></td>
                                        <td class="padding-all-10"><a href="#">Yum w/ Cheese</a></td>
                                        <td class="padding-all-10"><a href="#">44.00</a></td>
                                        <td class="padding-all-10"><a href="#">88.00</a></td>
                                    </tr>                                                                       
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Total Cost</td>
                                        <td colspan="2" class="padding-all-10 text-right">265.10 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Added VAT</td>
                                        <td colspan="2" class="padding-all-10 text-right">26.50 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Delivery Charge</td>
                                        <td colspan="2" class="padding-all-10 text-right">20.00 PHP</td>
                                    </tr>
                                    <tr class="bggray-dark">
                                        <td colspan="2" class="padding-all-10 text-left font-16"><strong>Total Bill</strong></td>
                                        <td colspan="2" class="padding-all-10 text-right font-14"><strong>611.80 PHP</strong></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>

                    </div>
                    <div class="margin-top-20">
                       
                        
                        <button type="button" class="btn btn-dark f-right margin-right-10 " >Reject Order</button>
                        <button type="button" class="btn btn-dark f-right margin-right-10 ">Accept Order</button>                           
                        <div class="clear"></div>
                    </div>
                </div>
            </div>





            <!-- sample-2 -->

            
            <div class="content-container unboxed ">
                <h3 class="f-left no-margin-all">Assembling Transactions</h3>                
                <p class="f-right font-14 no-margin-bottom"><strong>2 Transactions</strong></p>
                <div class="clear"></div>
            </div>
            <div class="content-container viewable opaque">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>Cr8v Websolutions, Inc.</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">66D 2nd Floor, 591 Cr8v Bldg, San Rafael St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20">
                        <p class="font-16 margin-bottom-5 yellow-color"><strong>ASSEMBLING ORDER</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:01:39</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:01:39</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="content-container viewable opaque">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>Cr8v Websolutions, Inc.</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">66D 2nd Floor, 591 Cr8v Bldg, San Rafael St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20">
                        <p class="font-16 margin-bottom-5 yellow-color"><strong>ASSEMBLING ORDER</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:01:39</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:01:39</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="content-container">
                <div>
                    <div class="f-left margin-top-15">
                        <p class="font-20"><strong>Order ID: 734784</strong></p>    
                                                                          
                    </div>                  
                    <div class=" f-right margin-bottom-10">
                        <p class="font-16 margin-bottom-5 yellow-color"><strong>ASSEMBLING ORDER</strong></p>                                                
                        <p class="font-14 margin-bottom-5  "><strong><span class="red-color">Timer:</span></strong> 00:02:25</p>
                        <p class="font-14 margin-bottom-5  "><strong><span class="red-color">Elapsed Time:</span></strong> 00:01:30</p>
                                            
                    </div>
                    <div class="clear"></div>
                    <hr>

                    <div class="data-container split">
                        
                        <!-- clients information -->
                        <div class="margin-top-20">
                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Name: </strong></p>
                            <p class="f-right font-12">Jonathan R. Omido</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Contact Number: </strong></p>
                            <p class="f-right font-12">(+63) 915-516-6153 <i class="fa fa-mobile fa-2x margin-right-5"></i>Globe</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Transaction Time:</strong></p>
                            <p class="f-right font-12">May 18, 2015 | 10:11:31</p>
                            <div class="clear"></div>                       
                        </div>

                        <!-- delivery address -->
                        <div>
                            <label class="margin-top-10 margin-bottom-10 ">Delivery Address:</label>
                            <div class="bggray-light padding-all-5 font-14 small-curved-border">
                                <div class="display-inline-mid margin-right-10 padding-left-20">
                                    <img src="../assets/images/work-icon.png" alt="work icon">
                                    <p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
                                </div>
                                <div class="display-inline-mid margin-left-10  padding-left-10">
                                    <p class="no-margin-all"><strong>Cr8v Web Solutions, Inc.</strong></p>
                                    <p class="no-margin-all">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR</p>
                                    <p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
                                </div>                          
                            </div>                          
                        </div>

                        <!-- cards -->
                        <label class="margin-top-15">CARDS:</label>
                        <div class="bggray-light padding-all-5 font-14 small-curved-border">
                            <div class="display-inline-mid margin-right-10">
                                <img class="thumb" src="../assets/images/happy-plus.jpg" alt="happy-plus">
                            </div>
                            <div class="display-inline-mid margin-left-10 divider padding-left-10">
                                <p class="no-margin-all">0083-123456-46578<br>
                                <span class="red-color"><strong>Exp. Date:</strong></span> September 20, 2016</p>
                            </div>
                            <div class="display-inline-mid text-center margin-left-10">
                                <a href="#">Show Card<br>History</a>
                            </div>
                        </div>


                        <div class="margin-top-10">
                            <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Payment Method: </strong></p>
                            <p class="f-right / font-12">Cash</p>
                            <div class="clear"></div>

                            <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Change For: </strong></p>
                            <p class="f-right / font-12">500.00 PHP</p>
                            <div class="clear"></div>

                            <p class="f-left font-12 red-color  margin-bottom-5"><strong>Remarks: </strong></p>
                            <p class="f-right font-12 gray-color">No Remarks</p>
                            <div class="clear"></div>
                            
                        </div>
                    </div>

                    <div class="data-container split margin-left-15">

                        <label class="margin-top-15">order: </label>
                        <div class="small-curved-border">
                            <table class="font-14">
                                <thead class="bggray-dark">
                                    <tr>
                                        <th class="padding-all-10">Quantitiy</th>
                                        <th class="padding-all-10">Product</th>
                                        <th class="padding-all-10">Price</th>
                                        <th class="padding-all-10">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody class="bggray-light">
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10"><div class="arrow-down"></div> Champ Amazing aloha + French Fries + Choice o..</td>
                                        <td class="padding-all-10">203.50</td>
                                        <td class="padding-all-10">407.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Large Coca-Cola</td>
                                        <td class="padding-all-10">17.60</td>
                                        <td class="padding-all-10">35.20</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Regular French Fries</td>
                                        <td class="padding-all-10">0.00</td>
                                        <td class="padding-all-10">0.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10">Sundae Strawberry</td>
                                        <td class="padding-all-10">30.80</td>
                                        <td class="padding-all-10">61.60</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10">Yum w/ Cheese</td>
                                        <td class="padding-all-10">44.00</td>
                                        <td class="padding-all-10">88.00</td>
                                    </tr>                                                                       
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Total Cost</td>
                                        <td colspan="2" class="padding-all-10 text-right">265.10 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Added VAT</td>
                                        <td colspan="2" class="padding-all-10 text-right">26.50 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Delivery Charge</td>
                                        <td colspan="2" class="padding-all-10 text-right">20.00 PHP</td>
                                    </tr>
                                    <tr class="bggray-dark">
                                        <td colspan="2" class="padding-all-10 text-left font-16"><strong>Total Bill</strong></td>
                                        <td colspan="2" class="padding-all-10 text-right font-14"><strong>611.80 PHP</strong></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>

                    </div>
                    <div class="margin-top-20">
                       
                        
                        <button type="button" class="btn btn-dark f-right margin-right-10 ">Done Assembling Order</button>
                        <button type="button" class="btn btn-dark f-right margin-right-10 "> Re-print Receipt</button>                           
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
			

             <!-- sample-3 -->

           
            <div class="content-container unboxed ">
                <h3 class="f-left no-margin-all">For Dispatch Transaction</h3>                
                <p class="f-right font-14 no-margin-bottom"><strong>1 Transaction</strong></p>
                <div class="clear"></div>
            </div>
            <div class="content-container viewable opaque ">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>Cr8v Websolutions, Inc.</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">66D 2nd Floor, 591 Cr8v Bldg, San Rafael St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20    ">
                        <p class="font-16 margin-bottom-5 "><strong>FOR DISPATCH</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:01:39</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:01:39</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="content-container">
                <div>
                    <div class="f-left margin-top-15">
                        <p class="font-20"><strong>Order ID: 734784</strong></p>    
                                                                        
                    </div>                  
                    <div class=" f-right margin-bottom-10">
                        <p class="font-16 margin-bottom-5 "><strong>FOR DISPATCH</strong></p>                                                
                        <p class="font-14 margin-bottom-5  "><strong><span class="red-color">Timer:</span></strong> 00:15:27</p>
                        <p class="font-14 margin-bottom-5  "><strong><span class="red-color">Elapsed Time:</span></strong> 00:01:36</p>
                                            
                    </div>
                    <div class="clear"></div>
                    <hr>

                    <div class="data-container split">
                        
                        <!-- clients information -->
                        <div class="margin-top-20">
                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Name: </strong></p>
                            <p class="f-right font-12">Jonathan R. Omido</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Contact Number: </strong></p>
                            <p class="f-right font-12">(+63) 915-516-6153 <i class="fa fa-mobile fa-2x margin-right-5"></i>Globe</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Transaction Time:</strong></p>
                            <p class="f-right font-12">May 18, 2015 | 10:11:31</p>
                            <div class="clear"></div>                       
                        </div>

                        <!-- delivery address -->
                        <div>
                            <label class="margin-top-10 margin-bottom-10 ">Delivery Address:</label>
                            <div class="bggray-light padding-all-5 font-14 small-curved-border">
                                <div class="display-inline-mid margin-right-10 padding-left-20">
                                    <img src="../assets/images/work-icon.png" alt="work icon">
                                    <p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
                                </div>
                                <div class="display-inline-mid margin-left-10  padding-left-10">
                                    <p class="no-margin-all"><strong>Cr8v Web Solutions, Inc.</strong></p>
                                    <p class="no-margin-all">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR</p>
                                    <p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
                                </div>                          
                            </div>                          
                        </div>

                        <!-- cards -->
                        <label class="margin-top-15">CARDS:</label>
                        <div class="bggray-light padding-all-5 font-14 small-curved-border">
                            <div class="display-inline-mid margin-right-10">
                                <img class="thumb" src="../assets/images/happy-plus.jpg" alt="happy-plus">
                            </div>
                            <div class="display-inline-mid margin-left-10 divider padding-left-10">
                                <p class="no-margin-all">0083-123456-46578<br>
                                <span class="red-color"><strong>Exp. Date:</strong></span> September 20, 2016</p>
                            </div>
                            <div class="display-inline-mid text-center margin-left-10">
                                <a href="#">Show Card<br>History</a>
                            </div>
                        </div>


                        <div class="margin-top-10">
                            <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Payment Method: </strong></p>
                            <p class="f-right / font-12">Cash</p>
                            <div class="clear"></div>

                            <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Change For: </strong></p>
                            <p class="f-right / font-12">500.00 PHP</p>
                            <div class="clear"></div>

                            <p class="f-left font-12 red-color  margin-bottom-5"><strong>Remarks: </strong></p>
                            <p class="f-right font-12 gray-color">No Remarks</p>
                            <div class="clear"></div>
                            
                        </div>
                    </div>

                    <div class="data-container split margin-left-15">

                        <label class="margin-top-15">order: </label>
                        <div class="small-curved-border">
                            <table class="font-14">
                                <thead class="bggray-dark">
                                    <tr>
                                        <th class="padding-all-10">Quantitiy</th>
                                        <th class="padding-all-10">Product</th>
                                        <th class="padding-all-10">Price</th>
                                        <th class="padding-all-10">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody class="bggray-light">
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10"><div class="arrow-down"></div> Champ Amazing aloha + French Fries + Choice o..</td>
                                        <td class="padding-all-10">203.50</td>
                                        <td class="padding-all-10">407.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Large Coca-Cola</td>
                                        <td class="padding-all-10">17.60</td>
                                        <td class="padding-all-10">35.20</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Regular French Fries</td>
                                        <td class="padding-all-10">0.00</td>
                                        <td class="padding-all-10">0.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10">Sundae Strawberry</td>
                                        <td class="padding-all-10">30.80</td>
                                        <td class="padding-all-10">61.60</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10">Yum w/ Cheese</td>
                                        <td class="padding-all-10">44.00</td>
                                        <td class="padding-all-10">88.00</td>
                                    </tr>                                                                       
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Total Cost</td>
                                        <td colspan="2" class="padding-all-10 text-right">265.10 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Added VAT</td>
                                        <td colspan="2" class="padding-all-10 text-right">26.50 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Delivery Charge</td>
                                        <td colspan="2" class="padding-all-10 text-right">20.00 PHP</td>
                                    </tr>
                                    <tr class="bggray-dark">
                                        <td colspan="2" class="padding-all-10 text-left font-16"><strong>Total Bill</strong></td>
                                        <td colspan="2" class="padding-all-10 text-right font-14"><strong>611.80 PHP</strong></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>

                    </div>
                    <div class="margin-top-20">
                        <button type="button" class="btn btn-dark f-right margin-right-10 modal-trigger" modal-target="assign-rider">Assign Rider</button>
                        <button type="button" class="btn btn-dark f-right margin-right-10">Rider Has Arrived</button>
                        <div class="clear"></div>
                        <br />
                   
                    </div>
                </div>
            </div>
             <div class="content-container">
                <div>
                    <div class="f-left margin-top-15">
                        <p class="font-20"><strong>Order ID: 734784</strong></p>    
                        <p class="font-16"><strong><span class="yellow-color">BULK ORDER</span></strong></p>    
                                                                        
                    </div>                  
                    <div class=" f-right margin-bottom-10">
                        <p class="font-16 margin-bottom-5 "><strong>FOR DISPATCH</strong></p>                                                
                        <p class="font-14 margin-bottom-5  "><strong><span class="red-color">Timer:</span></strong> 00:02:25</p>
                        <p class="font-14 margin-bottom-5  "><strong><span class="red-color">Elapsed Time:</span></strong> 00:01:30</p>
                                            
                    </div>
                    <div class="clear"></div>
                    <hr>

                    <div class="data-container split">
                        
                        <!-- clients information -->
                        <div class="margin-top-20">
                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Name: </strong></p>
                            <p class="f-right font-12">Jonathan R. Omido</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Contact Number: </strong></p>
                            <p class="f-right font-12">(+63) 915-516-6153 <i class="fa fa-mobile fa-2x margin-right-5"></i>Globe</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Transaction Time:</strong></p>
                            <p class="f-right font-12">May 18, 2015 | 10:11:31</p>
                            <div class="clear"></div>                       
                        </div>

                        <!-- delivery address -->
                        <div>
                            <label class="margin-top-10 margin-bottom-10 ">Delivery Address:</label>
                            <div class="bggray-light padding-all-5 font-14 small-curved-border">
                                <div class="display-inline-mid margin-right-10 padding-left-20">
                                    <img src="../assets/images/work-icon.png" alt="work icon">
                                    <p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
                                </div>
                                <div class="display-inline-mid margin-left-10  padding-left-10">
                                    <p class="no-margin-all"><strong>Cr8v Web Solutions, Inc.</strong></p>
                                    <p class="no-margin-all">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR</p>
                                    <p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
                                </div>                          
                            </div>                          
                        </div>

                        <!-- cards -->
                        <label class="margin-top-15">CARDS:</label>
                        <div class="bggray-light padding-all-5 font-14 small-curved-border">
                            <div class="display-inline-mid margin-right-10">
                                <img class="thumb" src="../assets/images/happy-plus.jpg" alt="happy-plus">
                            </div>
                            <div class="display-inline-mid margin-left-10 divider padding-left-10">
                                <p class="no-margin-all">0083-123456-46578<br>
                                <span class="red-color"><strong>Exp. Date:</strong></span> September 20, 2016</p>
                            </div>
                            <div class="display-inline-mid text-center margin-left-10">
                                <a href="#">Show Card<br>History</a>
                            </div>
                        </div>


                        <div class="margin-top-10">
                            <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Payment Method: </strong></p>
                            <p class="f-right / font-12">Cash</p>
                            <div class="clear"></div>

                            <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Change For: </strong></p>
                            <p class="f-right / font-12">500.00 PHP</p>
                            <div class="clear"></div>

                            <p class="f-left font-12 red-color  margin-bottom-5"><strong>Remarks: </strong></p>
                            <p class="f-right font-12 gray-color">No Remarks</p>
                            <div class="clear"></div>
                            
                        </div>
                    </div>

                    <div class="data-container split margin-left-15">

                        <label class="margin-top-15">order: </label>
                        <div class="small-curved-border">
                            <table class="font-14">
                                <thead class="bggray-dark">
                                    <tr>
                                        <th class="padding-all-10">Quantitiy</th>
                                        <th class="padding-all-10">Product</th>
                                        <th class="padding-all-10">Price</th>
                                        <th class="padding-all-10">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody class="bggray-light">
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10"><div class="arrow-down"></div> Champ Amazing aloha + French Fries + Choice o..</td>
                                        <td class="padding-all-10">203.50</td>
                                        <td class="padding-all-10">407.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Large Coca-Cola</td>
                                        <td class="padding-all-10">17.60</td>
                                        <td class="padding-all-10">35.20</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Regular French Fries</td>
                                        <td class="padding-all-10">0.00</td>
                                        <td class="padding-all-10">0.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10">Sundae Strawberry</td>
                                        <td class="padding-all-10">30.80</td>
                                        <td class="padding-all-10">61.60</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10">Yum w/ Cheese</td>
                                        <td class="padding-all-10">44.00</td>
                                        <td class="padding-all-10">88.00</td>
                                    </tr>                                                                       
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Total Cost</td>
                                        <td colspan="2" class="padding-all-10 text-right">265.10 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Added VAT</td>
                                        <td colspan="2" class="padding-all-10 text-right">26.50 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Delivery Charge</td>
                                        <td colspan="2" class="padding-all-10 text-right">20.00 PHP</td>
                                    </tr>
                                    <tr class="bggray-dark">
                                        <td colspan="2" class="padding-all-10 text-left font-16"><strong>Total Bill</strong></td>
                                        <td colspan="2" class="padding-all-10 text-right font-14"><strong>611.80 PHP</strong></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>

                    </div>
                    <div class="margin-top-20">
                        <button type="button" class="btn btn-dark f-right margin-right-10 ">Change Assigned Rider</button>
                        <button type="button" class="btn btn-dark f-right margin-right-10 ">Send Rider Out</button>                           
                        <button type="button" class="btn / btn-dark / f-right / margin-right-10 / ">Re-print Receipt</button>  
                        <div class="clear"></div>
                        <br />
                   
                    </div>
                </div>
            </div>

            
            <!-- sample-4 -->


            <div class="content-container unboxed ">
                <h3 class="f-left no-margin-all">Rider Out Transactions</h3>                
                <p class="f-right font-14 no-margin-bottom"><strong>1 Transaction</strong></p>
                <div class="clear"></div>
            </div>
            <div class="content-container red-bg opaque">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>Cr8v Websolutions, Inc.</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">66D 2nd Floor, 591 Cr8v Bldg, San Rafael St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="margin-left-10  "> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20    ">
                        <p class="font-16 margin-bottom-5 red-color"><strong>RIDER'S OUT (LATE)</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:01:39</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:01:39</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
             <div class="content-container">
                <div>
                    <div class="f-left margin-top-15">
                        <p class="font-20"><strong>Order ID: 734784</strong></p>    
                                                                                    
                    </div>                  
                    <div class=" f-right margin-bottom-10">
                        <p class="font-16 margin-bottom-5 / red-color "><strong>RIDER'S OUT LATE</strong></p>                                                
                        <p class="font-14 margin-bottom-5  "><strong><span class="red-color">Timer:</span></strong> 00:02:25</p>
                        <p class="font-14 margin-bottom-5  "><strong><span class="red-color">Elapsed Time:</span></strong> 00:01:30</p>
                                            
                    </div>
                    <div class="clear"></div>
                    <hr>

                    <div class="data-container split">
                        
                        <!-- clients information -->
                        <div class="margin-top-20">
                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Name: </strong></p>
                            <p class="f-right font-12">Jonathan R. Omido</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Contact Number: </strong></p>
                            <p class="f-right font-12">(+63) 915-516-6153 <i class="fa fa-mobile fa-2x margin-right-5"></i>Globe</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Transaction Time:</strong></p>
                            <p class="f-right font-12">May 18, 2015 | 10:11:31</p>
                            <div class="clear"></div>                       
                        </div>

                        <!-- delivery address -->
                        <div>
                            <label class="margin-top-10 margin-bottom-10 ">Delivery Address:</label>
                            <div class="bggray-light padding-all-5 font-14 small-curved-border">
                                <div class="display-inline-mid margin-right-10 padding-left-20">
                                    <img src="../assets/images/work-icon.png" alt="work icon">
                                    <p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
                                </div>
                                <div class="display-inline-mid margin-left-10  padding-left-10">
                                    <p class="no-margin-all"><strong>Cr8v Web Solutions, Inc.</strong></p>
                                    <p class="no-margin-all">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR</p>
                                    <p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
                                </div>                          
                            </div>                          
                        </div>

                        <!-- cards -->
                        <label class="margin-top-15">CARDS:</label>
                        <div class="bggray-light padding-all-5 font-14 small-curved-border">
                            <div class="display-inline-mid margin-right-10">
                                <img class="thumb" src="../assets/images/happy-plus.jpg" alt="happy-plus">
                            </div>
                            <div class="display-inline-mid margin-left-10 divider padding-left-10">
                                <p class="no-margin-all">0083-123456-46578<br>
                                <span class="red-color"><strong>Exp. Date:</strong></span> September 20, 2016</p>
                            </div>
                            <div class="display-inline-mid text-center margin-left-10">
                                <a href="#">Show Card<br>History</a>
                            </div>
                        </div>


                        <div class="margin-top-10">
                            <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Payment Method: </strong></p>
                            <p class="f-right / font-12">Cash</p>
                            <div class="clear"></div>

                            <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Change For: </strong></p>
                            <p class="f-right / font-12">500.00 PHP</p>
                            <div class="clear"></div>

                            <p class="f-left font-12 red-color  margin-bottom-5"><strong>Remarks: </strong></p>
                            <p class="f-right font-12 gray-color">No Remarks</p>
                            <div class="clear"></div>
                            
                        </div>
                    </div>

                    <div class="data-container split margin-left-15">

                        <label class="margin-top-15">order: </label>
                        <div class="small-curved-border">
                            <table class="font-14">
                                <thead class="bggray-dark">
                                    <tr>
                                        <th class="padding-all-10">Quantitiy</th>
                                        <th class="padding-all-10">Product</th>
                                        <th class="padding-all-10">Price</th>
                                        <th class="padding-all-10">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody class="bggray-light">
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10"><div class="arrow-down"></div> Champ Amazing aloha + French Fries + Choice o..</td>
                                        <td class="padding-all-10">203.50</td>
                                        <td class="padding-all-10">407.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Large Coca-Cola</td>
                                        <td class="padding-all-10">17.60</td>
                                        <td class="padding-all-10">35.20</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Regular French Fries</td>
                                        <td class="padding-all-10">0.00</td>
                                        <td class="padding-all-10">0.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10">Sundae Strawberry</td>
                                        <td class="padding-all-10">30.80</td>
                                        <td class="padding-all-10">61.60</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10">Yum w/ Cheese</td>
                                        <td class="padding-all-10">44.00</td>
                                        <td class="padding-all-10">88.00</td>
                                    </tr>                                                                       
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Total Cost</td>
                                        <td colspan="2" class="padding-all-10 text-right">265.10 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Added VAT</td>
                                        <td colspan="2" class="padding-all-10 text-right">26.50 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Delivery Charge</td>
                                        <td colspan="2" class="padding-all-10 text-right">20.00 PHP</td>
                                    </tr>
                                    <tr class="bggray-dark">
                                        <td colspan="2" class="padding-all-10 text-left font-16"><strong>Total Bill</strong></td>
                                        <td colspan="2" class="padding-all-10 text-right font-14"><strong>611.80 PHP</strong></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>

                    </div>
                    <div class="margin-top-20">
                        
                        <button type="button" class="btn btn-dark f-right margin-right-10 ">Locate Rider</button>                           
                        <button type="button" class="btn / btn-dark / f-right / margin-right-10 / modal-trigger" modal-target="rider-arrived-late">Rider Has Arrived</button>  
                        <div class="clear"></div>
                        <br>
                   
                    </div>
                </div>
            </div>
            <div class="content-container viewable opaque">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>Cr8v Websolutions, Inc.</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">66D 2nd Floor, 591 Cr8v Bldg, San Rafael St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20    ">
                        <p class="font-16 margin-bottom-5"><strong>RIDER'S OUT</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:01:39</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:01:39</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="content-container">
                <div>
                    <div class="f-left margin-top-15">
                        <p class="font-20"><strong>Order ID: 734784</strong></p>    
                        <p class="font-16"><strong><span class="yellow-color">BULK ORDER</span></strong></p>    
                                                                        
                    </div>                  
                    <div class=" f-right margin-bottom-10">
                        <p class="font-16 margin-bottom-5 "><strong>RIDER'S OUT</strong></p>                                                
                        <p class="font-14 margin-bottom-5  "><strong><span class="red-color">Timer:</span></strong> 00:02:25</p>
                        <p class="font-14 margin-bottom-5  "><strong><span class="red-color">Elapsed Time:</span></strong> 00:01:30</p>
                                            
                    </div>
                    <div class="clear"></div>
                    <hr>

                    <div class="data-container split">
                        
                        <!-- clients information -->
                        <div class="margin-top-20">
                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Name: </strong></p>
                            <p class="f-right font-12">Jonathan R. Omido</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Contact Number: </strong></p>
                            <p class="f-right font-12">(+63) 915-516-6153 <i class="fa fa-mobile fa-2x margin-right-5"></i>Globe</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Transaction Time:</strong></p>
                            <p class="f-right font-12">May 18, 2015 | 10:11:31</p>
                            <div class="clear"></div>                       
                        </div>

                        <!-- delivery address -->
                        <div>
                            <label class="margin-top-10 margin-bottom-10 ">Delivery Address:</label>
                            <div class="bggray-light padding-all-5 font-14 small-curved-border">
                                <div class="display-inline-mid margin-right-10 padding-left-20">
                                    <img src="../assets/images/work-icon.png" alt="work icon">
                                    <p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
                                </div>
                                <div class="display-inline-mid margin-left-10  padding-left-10">
                                    <p class="no-margin-all"><strong>Cr8v Web Solutions, Inc.</strong></p>
                                    <p class="no-margin-all">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR</p>
                                    <p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
                                </div>                          
                            </div>                          
                        </div>

                        <!-- cards -->
                        <label class="margin-top-15">CARDS:</label>
                        <div class="bggray-light padding-all-5 font-14 small-curved-border">
                            <div class="display-inline-mid margin-right-10">
                                <img class="thumb" src="../assets/images/happy-plus.jpg" alt="happy-plus">
                            </div>
                            <div class="display-inline-mid margin-left-10 divider padding-left-10">
                                <p class="no-margin-all">0083-123456-46578<br>
                                <span class="red-color"><strong>Exp. Date:</strong></span> September 20, 2016</p>
                            </div>
                            <div class="display-inline-mid text-center margin-left-10">
                                <a href="#">Show Card<br>History</a>
                            </div>
                        </div>


                        <div class="margin-top-10">
                            <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Payment Method: </strong></p>
                            <p class="f-right / font-12">Cash</p>
                            <div class="clear"></div>

                            <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Change For: </strong></p>
                            <p class="f-right / font-12">500.00 PHP</p>
                            <div class="clear"></div>

                            <p class="f-left font-12 red-color  margin-bottom-5"><strong>Remarks: </strong></p>
                            <p class="f-right font-12 gray-color">No Remarks</p>
                            <div class="clear"></div>
                            
                        </div>
                    </div>

                    <div class="data-container split margin-left-15">

                        <label class="margin-top-15">order: </label>
                        <div class="small-curved-border">
                            <table class="font-14">
                                <thead class="bggray-dark">
                                    <tr>
                                        <th class="padding-all-10">Quantitiy</th>
                                        <th class="padding-all-10">Product</th>
                                        <th class="padding-all-10">Price</th>
                                        <th class="padding-all-10">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody class="bggray-light">
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10"><div class="arrow-down"></div> Champ Amazing aloha + French Fries + Choice o..</td>
                                        <td class="padding-all-10">203.50</td>
                                        <td class="padding-all-10">407.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Large Coca-Cola</td>
                                        <td class="padding-all-10">17.60</td>
                                        <td class="padding-all-10">35.20</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Regular French Fries</td>
                                        <td class="padding-all-10">0.00</td>
                                        <td class="padding-all-10">0.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10">Sundae Strawberry</td>
                                        <td class="padding-all-10">30.80</td>
                                        <td class="padding-all-10">61.60</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10">Yum w/ Cheese</td>
                                        <td class="padding-all-10">44.00</td>
                                        <td class="padding-all-10">88.00</td>
                                    </tr>                                                                       
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Total Cost</td>
                                        <td colspan="2" class="padding-all-10 text-right">265.10 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Added VAT</td>
                                        <td colspan="2" class="padding-all-10 text-right">26.50 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Delivery Charge</td>
                                        <td colspan="2" class="padding-all-10 text-right">20.00 PHP</td>
                                    </tr>
                                    <tr class="bggray-dark">
                                        <td colspan="2" class="padding-all-10 text-left font-16"><strong>Total Bill</strong></td>
                                        <td colspan="2" class="padding-all-10 text-right font-14"><strong>611.80 PHP</strong></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>

                    </div>
                    <div class="margin-top-20">
                        
                        <button type="button" class="btn btn-dark f-right margin-right-10 ">Locate Rider</button>                           
                        <button type="button" class="btn / btn-dark / f-right / margin-right-10 / modal-trigger" modal-target="rider-arrived">Rider Has Arrived</button>  
                        <div class="clear"></div>
                        <br>
                   
                    </div>
                </div>
            </div>
             <!-- sample 5 -->


            <div class="content-container unboxed ">
                <h3 class="f-left no-margin-all">Rider In Transactions</h3>                
                <p class="f-right font-14 no-margin-bottom"><strong>1 Transaction</strong></p>
                <div class="clear"></div>
            </div>
            <div class="content-container viewable opaque">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>Cr8v Websolutions, Inc.</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">66D 2nd Floor, 591 Cr8v Bldg, San Rafael St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="margin-left-10  "> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20    ">
                        <p class="font-16 margin-bottom-5"><strong>RIDER IN</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:01:39</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:01:39</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>        
			
            <!-- sample 6 -->

            <div class="content-container unboxed ">
                <h3 class="f-left no-margin-all">Completed Transactions</h3>                
                <p class="f-right font-14 no-margin-bottom"><strong>221 Transaction</strong></p>
                <div class="clear"></div>
            </div>
            <div class="content-container">
                <div>
                    <div class="f-left margin-top-15">
                        <p class="font-20"><strong>Order ID: 734784</strong></p>    
                        
                                                                        
                    </div>                  
                    <div class=" f-right margin-bottom-10">
                        <p class="font-16 margin-bottom-5 "><strong><span class="green-color"> ORDER COMPLETED</span></strong></p>                                                
                        <p class="font-14 margin-bottom-5  "><strong><span class="red-color">Timer:</span></strong> 00:25:36</p>
                        <p class="font-14 margin-bottom-5  "><strong><span class="red-color">Elapsed Time:</span></strong> 11:25:36</p>
                                            
                    </div>
                    <div class="clear"></div>
                    <hr>

                    <div class="data-container split">
                        
                        <!-- clients information -->
                        <div class="margin-top-20">
                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Name: </strong></p>
                            <p class="f-right font-12">Jonathan R. Omido</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Contact Number: </strong></p>
                            <p class="f-right font-12">(+63) 915-516-6153 <i class="fa fa-mobile fa-2x margin-right-5"></i>Globe</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Transaction Time:</strong></p>
                            <p class="f-right font-12">May 18, 2015 | 10:11:31</p>
                            <div class="clear"></div>                       
                        </div>

                        <!-- delivery address -->
                        <div>
                            <label class="margin-top-10 margin-bottom-10 ">Delivery Address:</label>
                            <div class="bggray-light padding-all-5 font-14 small-curved-border">
                                <div class="display-inline-mid margin-right-10 padding-left-20">
                                    <img src="../assets/images/work-icon.png" alt="work icon">
                                    <p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
                                </div>
                                <div class="display-inline-mid margin-left-10  padding-left-10">
                                    <p class="no-margin-all"><strong>Cr8v Web Solutions, Inc.</strong></p>
                                    <p class="no-margin-all">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR</p>
                                    <p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
                                </div>                          
                            </div>                          
                        </div>

                        <!-- cards -->
                        <label class="margin-top-15">CARDS:</label>
                        <div class="bggray-light padding-all-5 font-14 small-curved-border">
                            <div class="display-inline-mid margin-right-10">
                                <img class="thumb" src="../assets/images/happy-plus.jpg" alt="happy-plus">
                            </div>
                            <div class="display-inline-mid margin-left-10 divider padding-left-10">
                                <p class="no-margin-all">0083-123456-46578<br>
                                <span class="red-color"><strong>Exp. Date:</strong></span> September 20, 2016</p>
                            </div>
                            <div class="display-inline-mid text-center margin-left-10">
                                <a href="#">Show Card<br>History</a>
                            </div>
                        </div>


                        <div class="margin-top-10">
                            <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Payment Method: </strong></p>
                            <p class="f-right / font-12">Cash</p>
                            <div class="clear"></div>

                            <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Change For: </strong></p>
                            <p class="f-right / font-12">500.00 PHP</p>
                            <div class="clear"></div>

                            <p class="f-left font-12 red-color  margin-bottom-5"><strong>Remarks: </strong></p>
                            <p class="f-right font-12 gray-color">No Remarks</p>
                            <div class="clear"></div>
                            
                        </div>
                    </div>

                    <div class="data-container split margin-left-15">

                        <label class="margin-top-15">order: </label>
                        <div class="small-curved-border">
                            <table class="font-14">
                                <thead class="bggray-dark">
                                    <tr>
                                        <th class="padding-all-10">Quantitiy</th>
                                        <th class="padding-all-10">Product</th>
                                        <th class="padding-all-10">Price</th>
                                        <th class="padding-all-10">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody class="bggray-light">
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10"><div class="arrow-down"></div> Champ Amazing aloha + French Fries + Choice o..</td>
                                        <td class="padding-all-10">203.50</td>
                                        <td class="padding-all-10">407.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Large Coca-Cola</td>
                                        <td class="padding-all-10">17.60</td>
                                        <td class="padding-all-10">35.20</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Regular French Fries</td>
                                        <td class="padding-all-10">0.00</td>
                                        <td class="padding-all-10">0.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10">Sundae Strawberry</td>
                                        <td class="padding-all-10">30.80</td>
                                        <td class="padding-all-10">61.60</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10">Yum w/ Cheese</td>
                                        <td class="padding-all-10">44.00</td>
                                        <td class="padding-all-10">88.00</td>
                                    </tr>                                                                       
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Total Cost</td>
                                        <td colspan="2" class="padding-all-10 text-right">265.10 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Added VAT</td>
                                        <td colspan="2" class="padding-all-10 text-right">26.50 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Delivery Charge</td>
                                        <td colspan="2" class="padding-all-10 text-right">20.00 PHP</td>
                                    </tr>
                                    <tr class="bggray-dark">
                                        <td colspan="2" class="padding-all-10 text-left font-16"><strong>Total Bill</strong></td>
                                        <td colspan="2" class="padding-all-10 text-right font-14"><strong>611.80 PHP</strong></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>

                    </div>
                
                </div>
            </div>
            <div class="content-container viewable opaque">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>Cr8v Websolutions, Inc.</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">66D 2nd Floor, 591 Cr8v Bldg, San Rafael St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20    ">
                        <p class="font-16 margin-bottom-5 green-color"><strong>ORDER COMPLETED</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:01:39</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:01:39</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
           
		</div>
	</section>

    <!-- modal ASSIGN RIDER -->
    <div class="modal-container" modal-id="assign-rider">
        <div class="modal-body small">
            <div class="modal-head">
                <h4 class="text-left">Assign Rider</h4>
                <div class="modal-close close-me"></div>
            </div>

            <!-- content -->
            <div class="modal-content">
                <p class="f-left no-margin-all"><strong>Is this a bulk order? </strong></p>
                <div class="form-group f-left margin-left-20 ">
                    <input id="bulkOrder1" type="radio" class="radio-box" name="bulkOrder">
                    <label class="radio-lbl margin-right-10 " for="bulkOrder1">Yes</label>
                    <input id="bulkOrder2" type="radio" class="radio-box" name="bulkOrder">
                    <label class="radio-lbl margin-right-10" for="bulkOrder2">No</label>
                </div>
                <div class="clear"></div>
                <label>Available Riders:</label>
                <br />
                <div class="select xlarge margin-bottom-10">
                    <select>
                        <option>James Dela Cruz | <span class="yellow-color">On Hold</span></option>
                    </select>
                </div>
                <br />
                <div class="select xlarge">
                    <select>
                        <option>John De Guzman</option>
                    </select>
                </div>
                <br />
                <a href="#" class="f-right margin-top-20">+ Assign Another Rider</a>
                <div class="clear"></div>
            </div>
            <!-- button -->
            <div class="f-right margin-right-10 margin-bottom-10">
                    <button type="button" class="btn btn-dark / margin-right-10">Assign &amp; Send Rider Out</button>
                    <button type="button" class="btn btn-dark / margin-right-10">Assign &amp; Hold Rider</button>
                    <button type="button" class="btn btn-dark / close-me">Cancel</button>
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <!-- modal REJECT ORDER -->
    <div class="modal-container  " modal-id="reject-order">
        <div class="modal-body small">
            <div class="modal-head">
                <h4 class="text-left">Void Order</h4>
                <div class="modal-close close-me"></div>
            </div>

            <div class="modal-content no-margin-all">
                
                <!-- logo and name -->
                
                <p class="yellow-color / f-left / margin-left-30"><i class="fa fa-exclamation-triangle fa-5x"></i>
                <p class="font-14 f-left margin-top-20 margin-left-30">Are you sure you want to void <br /><strong>Jonathan R. Ornido's </strong> order?</p>
                
                <div class="clear"></div>

            </div>
            <!-- button -->
            <div class="margin-top-20">
                <button type="button" class="btn btn-dark f-right margin-right-20 close-me ">Cancel</button>
                <button type="button" modal-target="reason-reject" class="btn btn-dark f-right margin-right-20 modal-trigger close-me" >Confirm</button>         
                <div class="clear"></div>
            </div>



        </div>
    </div>

    <!-- modal REASON FOR REJECT ORDER -->
    <div class="modal-container" modal-id="reason-reject">
        <div class="modal-body small">
            <div class="modal-head">
                <h4 class="text-left">Reason For Rejecting Order</h4>
                <div class="modal-close close-me"></div>
            </div>

            <div class="modal-content no-margin-all">
                
                <!-- lots of radio button -->
                <table >
                    <tr>
                        <td>
                            <p class="red-color / margin-right-30  / margin-bottom-50"><i class="fa fa-question-circle fa-5x"></i></p>
                        </td>
                        <td>
                            <p class="font-14 / margin-bottom-10 ">Why did you reject <strong>Jonathan R. Ornido's</strong> Order?</p>

                            <div class="f-left / margin-right-20  ">

                                <div class="form-group">
                                    <input id="op1" type="radio" class="radio-box" name="reject">
                                    <label class="radio-lbl / margin-right-10 / margin-bottom-10 " for="op1">No Delivery Driver</label>  
                                    
                                    <br />
                                    <input id="op2" type="radio" class="radio-box" name="reject">
                                    <label class="radio-lbl / margin-right-10 / margin-bottom-10 " for="op2">Product is Unavailable</label>    

                                    <br />
                                    <input id="op3" type="radio" class="radio-box" name="reject">
                                    <label class="radio-lbl / margin-right-10 / margin-bottom-10 " for="op3">Out of Delivery Area</label>     

                                    <br />
                                    <input id="op4" type="radio" class="radio-box" name="reject">
                                    <label class="radio-lbl / margin-right-10 / margin-bottom-10 " for="op4">Wrong Routing</label>   

                                </div>        

                            </div>

                            <div class="f-left">

                                <div class="form-group">

                                    <input id="op5" type="radio" class="radio-box" name="reject">
                                    <label class="radio-lbl / margin-bottom-10 " for="op5">Flooded Area</label>

                                    <br />

                                    <input id="op6" type="radio" class="radio-box" name="reject">
                                    <label class="radio-lbl / margin-bottom-10 " for="op6">Area Undeliverable</label>

                                    <br />
                                    <input id="op7" type="radio" class="radio-box" name="reject">
                                    <label class="radio-lbl / margin-bottom-10 " for="op7">Special Event</label>

                                    <br />
                                    <input id="op8" type="radio" class="radio-box" name="reject">
                                    <label class="radio-lbl / margin-bottom-10 " for="op8">NBC Overrides</label>

                                </div>
                            </div>


                        
                        </td>
                    </tr>
                </table>
            </div>

            <!-- button -->
            <div class="margin-top-20">
                <button type="button" class="btn btn-dark f-right margin-right-20 close-me ">Confirm</button>
                    
                <div class="clear"></div>
            </div>
        </div>
    </div>

    <!-- modal RIDER HAS ARRIVED -->
   <div class="modal-container " modal-id="rider-arrived">
        <div class="modal-body small">

            <div class="modal-head">
                <h4 class="text-left">Set Time Delivered</h4>
                <div class="modal-close close-me"></div>
            </div>

            <div class="modal-content">
                <div class="margin-left-100">
                    <label>Delivery Date:</label>
                    <br>
                    <div class="date-picker  ">
                        <input type="text">
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>

                
                    <label class="margin-top-15">Time Delivered:</label>                
                    <table border="0">
                        <thead>
                            <tr>
                                <th class="padding-left-20 padding-right-20 padding-top-5 padding-bottom-5 font-16">Hour</th>
                                <th>:</th>                                
                                <th class="padding-left-30 padding-right-20 padding-top-5 padding-bottom-5 font-16">Minute</th>
                                <th class="padding-left-20 padding-right-20 padding-top-5 padding-bottom-5 font-16">Period</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="2"><button type="button" class="btn / btn-dark / margin-top-10 margin-bottom-10 margin-left-15  "><i class="fa fa-chevron-up "></i></button></td>
                                <td><button type="button" class="btn / btn-dark / margin-top-10 margin-bottom-10 margin-left-30"><i class="fa fa-chevron-up"></i></button></td>
                                <td><button type="button" class="btn / btn-dark / margin-top-10 margin-bottom-10 margin-left-20"><i class="fa fa-chevron-up"></i></button></td>
                            </tr>
                            <tr>
                                <td colspan="2"><input type="text" class="xsmall / margin-left-5"></td>
                                <td><input type="text" class="xsmall / margin-left-15 "></td>
                                <td><input type="text" class="xsmall / margin-left-10 "></td>
                            </tr>
                            <tr>
                                <td colspan="2"><button type="button" class="btn / btn-dark / margin-top-10 margin-bottom-10 margin-left-15"><i class="fa fa-chevron-down"></i></button></td>
                                <td><button type="button" class="btn / btn-dark / margin-top-10 margin-bottom-10 margin-left-30"><i class="fa fa-chevron-down"></i></button></td>
                                <td><button type="button" class="btn / btn-dark /  margin-top-10 margin-bottom-10 margin-left-20"><i class="fa fa-chevron-down"></i></button></td>
                            </tr>
                        </tbody>
                    </table>                    
                </div>
            </div> 

            <!-- button -->
            <div class="margin-top-20 text-center">                
                <button type="button" class="btn btn-dark close-me">Confirm</button>         
            </div>    
            

        </div>
    </div>

    <!-- modal RIDER HAS ARRIVED LATE -->
   <div class="modal-container " modal-id="rider-arrived-late">
        <div class="modal-body small">

            <div class="modal-head">
                <h4 class="text-left">Set Time Delivered - <span class="red-color">Delivery Late</span></h4>
                <div class="modal-close close-me"></div>
            </div>

            <div class="modal-content">
                <div class="f-left width-50per">
                    <label class="margin-bottom-10">Delivery Date:</label>
                    <br>
                    <div class="date-picker  / margin-bottom-10">
                        <input type="text">
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                    <label>Any Gift Certificate Given?:</label>
                    
                    <div class="form-group">

                        <input id="cert1" type="radio" class="radio-box" name="certificate">
                        <label class="radio-lbl margin-right-10 " for="cert1">Yes</label>
                        <input id="cert2" type="radio" class="radio-box" name="certificate">
                        <label class="radio-lbl margin-right-10" for="cert2">No</label>
                    </div>
                
                    
                </div>

                <div class="f-left">
                
                    <label class="">Time Delivered:</label>                
                    <table border="0">
                        <thead>
                            <tr>
                                <th class=" font-16 / padding-left-15">Hour</th>
                                <th>:</th>                                
                                <th class=" font-16 / padding-left-10">Minute</th>
                                <th class=" font-16 / padding-left-10">Period</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="2"><button type="button" class="btn / btn-dark / margin-left-10 / margin-bottom-10 "><i class="fa fa-chevron-up "></i></button></td>
                                <td><button type="button" class="btn / btn-dark / margin-left-10 / margin-bottom-10 "><i class="fa fa-chevron-up"></i></button></td>
                                <td><button type="button" class="btn / btn-dark / margin-left-10 / margin-bottom-10 "><i class="fa fa-chevron-up"></i></button></td>
                            </tr>
                            <tr>
                                <td colspan="2"><input type="text" class="xsmall / margin-right-10 / margin-bottom-10"></td>
                                <td><input type="text" class="xsmall / margin-right-10 / margin-bottom-10"></td>
                                <td><input type="text" class="xsmall / margin-right-10 / margin-bottom-10"></td>
                            </tr>
                            <tr>
                                <td colspan="2"><button type="button" class="btn / btn-dark / margin-left-10 "><i class="fa fa-chevron-down"></i></button></td>
                                <td><button type="button" class="btn / btn-dark / margin-left-10  "><i class="fa fa-chevron-down"></i></button></td>
                                <td><button type="button" class="btn / btn-dark / margin-left-10 "><i class="fa fa-chevron-down"></i></button></td>
                            </tr>
                        </tbody>
                    </table>                    
                </div>
                <div class="clear"></div>
            </div> 

            <!-- button -->
            <div class="margin-top-20 text-center">                
                <button type="button" class="btn btn-dark close-me">Confirm</button>         
            </div>    
            

        </div>
    </div>


<?php include "../construct/footer.php"; ?>