<?php include "../construct/header.php"; ?>

	<section class="container-fluid" section-style="top-panel">

        <!-- search order -->
		<div class="row header-container">

			<div class="contents">
                <div class="breadcrumbs f-left  margin-top-20">
                    <p><a href="#">DST Report</a> <i class="fa fa-angle-right"></i> <strong>Rider Archieves</strong></p>
                </div>
                <div class="clear"></div>
				<h1 class="f-left">Rider Archieves</h1>
				<div class="f-right margin-top-20">
                    <button class="btn btn-dark margin-right-20">Rider Archives</button>
                    <button class="btn btn-dark margin-right-20 ">Add New Rider</button>
					<button class="btn btn-dark">FAQ</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>
        
		<div class="row">
			<div class="contents margin-top-20">
                <!-- search -->
				<div class="f-left">
					<label class="margin-bottom-5">search:</label><br>
					<input class="search f-left xlarge" type="text">
				</div>
                <!-- search by -->
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">search by:</label><br>
					<div class="select xlarge">
						<select>
							<option value="Order ID">Order ID</option>							
						</select>
					</div>
				</div>
                            
				<button class="f-left btn btn-dark margin-top-20 margin-left-20">Search</button>
                <div class="clear"></div>
			</div>
		</div>
        
        <div class="row margin-top-20">
            <div class="contents line">
                
                
                <div class="f-right bggray-white">
                    <p class="f-left font-12 padding-left-10 padding-top-5">
                        <strong>Sort By:</strong>
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">   
                        <strong>Rider Name</strong>  
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-top-5">
                        <a class="red-color active" href="">
                            <strong>Date Archieved</strong>
                            <img src="../assets/images/ui/sort-top-arrow.png">
                        </a>
                    </p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
	</section>

	<section class="container-fluid" section-style="content-panel">
		<div class="row">
			<!-- sample -1-->

            
			<div class="content-container">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name:</span> Juan Manuel R. Garcia</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Contact Num: </span> </strong>(+63) 915-148-3691 <i class="fa fa-mobile"></i> Globe</p>                        
                    </div>
                    
                    <div class="width-35per f-left">
                       <p class="font-14 margin-bottom-5"><strong><span class="red-color">Date Archieved:</span></strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="gray-color">May 18, 2015 | 11:23 AM</span></strong></p>
                    </div>
                    
                    <div class="width-25per f-left">
                        <button type="button" class="btn / btn-dark / margin-top-10">Activate Rider</button>
                    </div>
                    <div class="clear"></div>
                </div>
			</div>
            <div class="content-container">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name:</span> Edgardo A. Gutierez</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Contact Num: </span> </strong>(+63) 915-148-3691 <i class="fa fa-mobile"></i> Globe</p>                        
                    </div>
                    
                    <div class="width-35per f-left">
                       <p class="font-14 margin-bottom-5"><strong><span class="red-color">Date Archieved:</span></strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="gray-color">May 18, 2015 | 11:23 AM</span></strong></p>
                    </div>
                    
                     <div class="width-25per f-left">
                        <button type="button" class="btn / btn-dark / margin-top-10">Activate Rider</button>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <!-- sample-2 -->
            <div class="content-container">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name:</span> Benjie A. Gutierez</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Contact Num: </span> </strong>(+63) 915-148-3691 <i class="fa fa-mobile"></i> Globe</p>                        
                    </div>
                    
                     
                    <div class="width-35per f-left">
                       <p class="font-14 margin-bottom-5"><strong><span class="red-color">Date Archieved:</span></strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="gray-color">May 18, 2015 | 11:23 AM</span></strong></p>
                    </div>
                    
                     <div class="width-25per f-left">
                        <button type="button" class="btn / btn-dark / margin-top-10">Activate Rider</button>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="content-container">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name:</span> Miguel R. Perez</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Contact Num: </span> </strong>(+63) 915-148-3691 <i class="fa fa-mobile"></i> Globe</p>                        
                    </div>
                    
                    <div class="width-35per f-left">
                       <p class="font-14 margin-bottom-5"><strong><span class="red-color">Date Archieved:</span></strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="gray-color">May 18, 2015 | 11:23 AM</span></strong></p>
                    </div>
                    
                     <div class="width-25per f-left">
                        <button type="button" class="btn / btn-dark / margin-top-10">Activate Rider</button>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="content-container">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name:</span> John A. Cruz</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Contact Num: </span> </strong>(+63) 915-148-3691 <i class="fa fa-mobile"></i> Globe</p>                        
                    </div>
                    
                    
                    <div class="width-35per f-left">
                       <p class="font-14 margin-bottom-5"><strong><span class="red-color">Date Archieved:</span></strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="gray-color">May 18, 2015 | 11:23 AM</span></strong></p>
                    </div>
                    
                   <div class="width-25per f-left">
                        <button type="button" class="btn / btn-dark / margin-top-10">Activate Rider</button>
                    </div>
                    <div class="clear"></div>
                </div>
            </div> 
		</div>
	</section>

<?php include "../construct/footer.php"; ?>