<?php include "../construct/header.php"; ?>

	<section class="container-fluid" section-style="top-panel">

        <!-- title name with buttons -->
		<div class="row header-container">
			<div class="contents">

                        
                <div class="clear"></div>
				<h1 class="f-left">Product Management</h1>
				<div class="f-right margin-top-20">
                    <button class="btn btn-dark margin-right-20 ">Product Status Schedule</button>                    
					<button class="btn btn-dark">FAQ</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>
    
		<div class="row">
			<div class="contents margin-top-15">
                <!-- search -->
				<div class="f-left">
					<label class="margin-bottom-5">search:</label><br>
					<input class="search f-left xlarge" type="text">
				</div>
                <!-- search by -->
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">search by:</label><br>
					<div class="select xlarge">
						<select>
							<option value="Product-Name">Product Name</option>							
						</select>
					</div>
				</div>
                            
				<button class="f-left btn btn-dark margin-top-20 margin-left-20">Search</button>
                <div class="clear"></div>

                <div class="select xlarge / margin-right-10 / margin-top-15">
                    <select>
                        <option value="Specific-Products">Filter By Specific Product</option>                          
                        <option value="Category">Filter By Category</option>         
                    </select>
                </div>
                <div class="select xlarge / margin-right-10 /  margin-top-15">
                    <select>
                        <option value="all-products">Show All Products</option>                          
                        <option value="turned-on">Show All Turned ON</option>
                        <option value="turned-off">Show All Turned OFF</option>
                    </select>
                </div>
                <button type="button" class="btn btn-dark / margin-right-10 /  margin-top-15" >Turn All Selected OFF</button>
                <button type="button" class="btn btn-dark /  margin-top-15 / green-bg" >Turn All Selected On</button>
			</div>
		</div>
        
        <div class="row margin-top-20">

            <div class="contents line">
            <hr />                            
            </div>
        </div>
	</section>

	<section class="container-fluid" section-style="content-panel">
		<div class="row">			
            <div class="content-container unboxed">
                <div class="tbl no-padding-all">
                    <table>
                        <thead >
                            <tr>
                                <th class="width-10per text-center ">                                    
                                    <div class="form-group / no-margin-all / no-padding-all / min-width-20">
                                        <input class="chck-box /  " id="check-all" type="checkbox">
                                        <label class="chck-lbl" for="check-all"></label>
                                    </div>
                                </th>
                                <th class="text-center / width-10per / no-padding-all">Category</th>
                          
                                <th class="text-center / no-padding-all ">Daily Status Report</th>
                                <th class="text-center / no-padding-all ">Availability Status</th>
                                
                            </tr>
                        </thead>
                        <tbody class="font-12 / text-center">
                            <tr >
                                <td >                                
                                    <div class="form-group / no-margin-all / padding-top-10 / min-width-20">
                                        <input class="chck-box /  " id="check1" type="checkbox">
                                        <label class="chck-lbl" for="check1"></label>
                                    </div>
                                </td>
                                <td>Chicken</td>                              
                                <td>
                                    <a href="#" class="special">Auto Reset</a>
                                </td>
                                <td>                                
                                    <div class="toggleSwitch / margin-left-100">
                                        <input type="checkbox" id="switch1" name="switch1" class="switch" />
                                        <label for="switch1">f</label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="no-padding-all">                                
                                    <div class="form-group / no-margin-all / padding-top-10 / min-width-20">
                                        <input class="chck-box /  " id="check2" type="checkbox">
                                        <label class="chck-lbl" for="check2"></label>
                                    </div>
                                </td>
                                <td>Burger</td>                              
                                <td><a href="#" class="special">Auto Reset</a></td>
                                <td>                                
                                    <div class="toggleSwitch / margin-left-100">
                                        <input type="checkbox" id="switch2" name="switch1" class="switch" />
                                        <label for="switch2">f</label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>                                
                                    <div class="form-group / no-margin-all / padding-top-10 / min-width-20">
                                        <input class="chck-box /  " id="check3" type="checkbox">
                                        <label class="chck-lbl" for="check3"></label>
                                    </div>
                                </td>
                                <td>Rice Meal</td>
                              
                                <td><a href="#" class="special">Auto Reset</a></td>
                                <td>                                
                                    <div class="toggleSwitch / margin-left-100">
                                        <input type="checkbox" id="switch3" name="switch1" class="switch" />
                                        <label for="switch3">f</label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>                                
                                    <div class="form-group / no-margin-all / padding-top-10 / min-width-20">
                                        <input class="chck-box /  " id="check4" type="checkbox">
                                        <label class="chck-lbl" for="check4"></label>
                                    </div>
                                </td>
                                <td>Sundae</td>
                              
                                <td><a href="#" class="special">Auto Reset</a></td>
                                <td>                                
                                    <div class="toggleSwitch / margin-left-100">
                                        <input type="checkbox" id="switch4" name="switch1" class="switch" />
                                        <label for="switch4">f</label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>                                
                                    <div class="form-group / no-margin-all / padding-top-10 / min-width-20">
                                        <input class="chck-box /  " id="check5" type="checkbox">
                                        <label class="chck-lbl" for="check5"></label>
                                    </div>
                                </td>
                                <td>Drinks</td>
                              
                                <td><a href="#" class="special">Auto Reset</a></td>
                                <td>                                
                                    <div class="toggleSwitch / margin-left-100">
                                        <input type="checkbox" id="switch5" name="switch1" class="switch" />
                                        <label for="switch5">f</label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>                                
                                    <div class="form-group / no-margin-all / padding-top-10 / min-width-20">
                                        <input class="chck-box /  " id="check6" type="checkbox">
                                        <label class="chck-lbl" for="check6"></label>
                                    </div>
                                </td>
                                <td>Hotdog</td>
                              
                                <td><a href="#" class="special">Auto Reset</a></td>
                                <td>                                
                                    <div class="toggleSwitch / margin-left-100">
                                        <input type="checkbox" id="switch6" name="switch1" class="switch" />
                                        <label for="switch6">f</label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>                                
                                    <div class="form-group / no-margin-all / padding-top-10 / min-width-20">
                                        <input class="chck-box /  " id="check7" type="checkbox">
                                        <label class="chck-lbl" for="check7"></label>
                                    </div>
                                </td>
                                <td>French Fries</td>
                              
                                <td><a href="#" class="special">Auto Reset</a></td>
                                <td>                                
                                    <div class="toggleSwitch / margin-left-100">
                                        <input type="checkbox" id="switch7" name="switch1" class="switch" />
                                        <label for="switch7">f</label>
                                    </div>
                                </td>
                            </tr>
                                                    
                        </tbody>
                    </table>
                </div>
            </div>           
		</div>
	</section>

   

<?php include "../construct/footer.php"; ?>