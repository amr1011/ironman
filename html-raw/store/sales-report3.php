    <?php include "../construct/header.php"; ?>

    <section class="container-fluid" section-style="top-panel">
        <div class="row header-container">
            <div class="contents">
                <h1 class="f-left">DST Reports</h1>
                <div class="f-right margin-top-20">
                    <button class="btn btn-dark margin-right-10 disabled">Download Excel File</button>
                    <button class="btn btn-dark ">FAQ</button>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="row">
            <div class="contents margin-top-20">

                <div class="display-inline-mid">
                    <!-- date from -->
                    <div class="f-left margin-right-10 ">
                        <label class="margin-bottom-5">Date From:</label><br>
                        <div class="date-picker">
                            <input type="text">
                            <span class="fa fa-calendar text-center red-color"></span>
                        </div>
                    </div>
                    <!-- date from -->
                    <div class="f-left">
                        <label class="margin-bottom-5">Date From:</label><br>
                        <div class="date-picker">
                            <input type="text">
                            <span class="fa fa-calendar text-center red-color"></span>
                        </div>
                    </div>
                    <!-- generate report -->
                    <button type="button" class="btn btn-dark / margin-left-20 margin-top-20">Generate Report</button>
                    <div class="clear"></div>
                </div>


                <div class="display-inline-mid  padding-left-10 margin-left-10">
                    <section class="container-fluid" section-style="content-panel">        
                        <!-- generate by period -->
                        <div class="divider padding-left-20">
                            <label>Generate by Period:</label><br />
                            
                            <div class="select small">
                                <select>
                                    <option value="Daily">Daily</option>
                                    <option value="Weekly">Weekly</option>
                                    <option value="Monthly">Monthly</option>
                                </select>
                            </div>
                            
                            <button type="button" class="btn / btn-dark / margin-left-20">Generate Report</button>
                        </div>                                    
                    </section>

                </div>
                <div class="margin-top-20">
                    <hr>
                </div>
            </div>            
        </div>
       
       
        <div class="row">
            <div class="contents">
                <div class="tbl / text-center ">
                    <table class="viewable">
                        <thead class="font-10">
                            <tr>
                                <th class="text-center / min-width-20" rowspan="2">Date</th>
                                <th class="text-center / " colspan="4">20 Minutes 0 to 500 Php</th>
                                
                                <th class="text-center / " colspan="4">30 Minutes 0 to 1100 Php</th>

                                <th class="text-center / " colspan="4">45 Minutes 0 to 1100 Php</th>                                

                            
                                
                                
                                
                                <th class="text-center / min-width-20" rowspan="2">Advance Orders</th>

                                <th class="text-center / min-width-20" rowspan="2">Big Orders</th>

                                <th class="text-center / min-width-20" rowspan="2">Exempted</th>

                                <th class="text-center" colspan="4">Total Hit Rates</th>
                                
                                <th class="text-center / min-width-20" rowspan="4">Total Hitrate Percentatge</th>
                            </tr>
                            <tr>
                                
                                <th class="text-center / min-width-20 / bggray-darkred">Y</th>
                                <th class="text-center / min-width-20 / bggray-brightred">N</th>
                                <th class="text-center / min-width-20 / bggray-darkred">Total</th>
                                <th class="text-center / min-width-20 / bggray-brightred">%</th>

                                <th class="text-center / min-width-20 / bggray-darkred ">Y</th>
                                <th class="text-center / min-width-20 / bggray-brightred">N</th>
                                <th class="text-center / min-width-20 / bggray-darkred">Total</th>
                                <th class="text-center / min-width-20 / bggray-brightred">%</th>

                                <th class="text-center / min-width-20 / bggray-darkred">Y</th>
                                <th class="text-center / min-width-20 / bggray-brightred">N</th>
                                <th class="text-center / min-width-20 / bggray-darkred">Total</th>
                                <th class="text-center / min-width-20 / bggray-brightred">%</th>

                                                                
                                <th class="text-center / min-width-20 / bggray-darkred">Y</th>
                                <th class="text-center / min-width-20 / bggray-brightred">N</th>
                                <th class="text-center / min-width-20 / bggray-darkred">Total</th>
                                <th class="text-center / min-width-20 / bggray-brightred">%</th>
                                
                            </tr>
                        </thead>
                        <tbody class="font-14">
                            <tr >
                                <td >
                                    <div class="f-left">
                                        <p class="f-left font-10">May <br>2015</p>
                                        <p class="f-left font-20"><strong>01</strong></p>
                                        <div class="clear"></div>
                                        <p class="font-10 margin-left-5 / margin-right-10">Friday</p>                                        
                                    </div>
                                </td>
                                <td>10</td>
                                <td>15</td>
                                <td>25</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>13</td>
                                <td>23</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>0</td>
                                <td>10</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>13</td>
                                <td>5</td>
                                <td>10</td>
                                <td>5</td>
                                <td>15</td>
                                <td><strong>95.3</strong></td>
                                <td><span class="font-20">100.0%</span> <a href="#">View Data</a></td>
                            </tr>
                             <tr>
                                <td>
                                    <div class="f-left">
                                        <p class="f-left font-10">May <br>2015</p>
                                        <p class="f-left font-20"><strong>02</strong></p>
                                        <div class="clear"></div>
                                        <p class="font-10 margin-left-5 / margin-right-10">Saturday</p>                                        
                                    </div>

                                </td>
                                <td>10</td>
                                <td>15</td>
                                <td>25</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>13</td>
                                <td>23</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>0</td>
                                <td>10</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>13</td>
                                <td>5</td>
                                <td>10</td>
                                <td>5</td>
                                <td>15</td>
                                <td><strong>95.3</strong></td>
                                <td><span class="font-20 / red-color">94.5%</span> <a href="#">View Data</a></td>
                            </tr>
                             <tr>
                                <td>
                                    <div class="f-left">
                                        <p class="f-left font-10">May <br>2015</p>
                                        <p class="f-left font-20"><strong>03</strong></p>
                                        <div class="clear"></div>
                                        <p class="font-10 margin-left-5 / margin-right-10">Sunday</p>                                        
                                    </div>

                                </td>
                                <td>10</td>
                                <td>15</td>
                                <td>25</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>13</td>
                                <td>23</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>0</td>
                                <td>10</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>13</td>
                                <td>5</td>
                                <td>10</td>
                                <td>5</td>
                                <td>15</td>
                                <td><strong>95.3</strong></td>
                                <td><span class="font-20 / red-color">91.2% </span> <a href="#">View Data</a></td>
                            </tr>
                             <tr>
                                <td>
                                    <div class="f-left">
                                        <p class="f-left font-10">May <br>2015</p>
                                        <p class="f-left font-20"><strong>04</strong></p>
                                        <div class="clear"></div>
                                        <p class="font-10 margin-left-5 / margin-right-10">Monday</p>                                        
                                    </div>

                                </td>
                                <td>10</td>
                                <td>15</td>
                                <td>25</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>13</td>
                                <td>23</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>0</td>
                                <td>10</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>13</td>
                                <td>5</td>
                                <td>10</td>
                                <td>5</td>
                                <td>15</td>
                                <td><strong>95.3</strong></td>
                                <td><span class="font-20">100.0% </span><a href="#">View Data</a></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="f-left">
                                        <p class="f-left font-10">May <br>2015</p>
                                        <p class="f-left font-20"><strong>05</strong></p>
                                        <div class="clear"></div>
                                        <p class="font-10 margin-left-5 / margin-right-10">Tuesday</p>                                        
                                    </div>

                                </td>
                                <td>10</td>
                                <td>15</td>
                                <td>25</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>13</td>
                                <td>23</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>0</td>
                                <td>10</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>13</td>
                                <td>5</td>
                                <td>10</td>
                                <td>5</td>
                                <td>15</td>
                                <td><strong>95.3</strong></td>
                                <td><span class="font-20">100.0% </span><br /><a href="#">View Data</a></td>
                            </tr>
                             <tr>
                                <td>
                                    <div class="f-left">
                                        <p class="f-left font-10">May <br>2015</p>
                                        <p class="f-left font-20"><strong>06</strong></p>
                                        <div class="clear"></div>
                                        <p class="font-10 margin-left-0 / ">Wednesday</p>                                        
                                    </div>


                                </td>
                                <td>10</td>
                                <td>15</td>
                                <td>25</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>13</td>
                                <td>23</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>0</td>
                                <td>10</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>13</td>
                                <td>5</td>
                                <td>10</td>
                                <td>5</td>
                                <td>15</td>
                                <td><strong>95.3</strong></td>
                                <td><span class="font-20 / red-color">80.4% </span> <a href="#">View Data</a></td>
                            </tr>
                             <tr>
                                <td>
                                    <div class="f-left">
                                        <p class="f-left font-10">May <br>2015</p>
                                        <p class="f-left font-20"><strong>07</strong></p>
                                        <div class="clear"></div>
                                        <p class="font-10 margin-left-10 / margin-right-10">Thursday</p>                                        
                                    </div>


                                </td>
                                <td>10</td>
                                <td>15</td>
                                <td>25</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>13</td>
                                <td>23</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>0</td>
                                <td>10</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>13</td>
                                <td>5</td>
                                <td>10</td>
                                <td>5</td>
                                <td>15</td>
                                <td><strong>95.3</strong></td>
                                <td><span class="font-20">100.0% </span> <a href="#">View Data</a></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="f-left">
                                        <p class="f-left font-10">May <br>2015</p>
                                        <p class="f-left font-20"><strong>08</strong></p>
                                        <div class="clear"></div>
                                        <p class="font-10 margin-left-10 / margin-right-10">Friday</p>                                        
                                    </div>


                                </td>
                                <td>10</td>
                                <td>15</td>
                                <td>25</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>13</td>
                                <td>23</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>0</td>
                                <td>10</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>13</td>
                                <td>5</td>
                                <td>10</td>
                                <td>5</td>
                                <td>15</td>
                                <td><strong>95.3</strong></td>
                                <td><span class="font-20">100.0% </span> <a href="#">View Data</a></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="f-left">
                                        <p class="f-left font-10">May <br>2015</p>
                                        <p class="f-left font-20"><strong>09</strong></p>
                                        <div class="clear"></div>
                                        <p class="font-10 margin-left-10 / margin-right-10">Saturday</p>                                        
                                    </div>


                                </td>
                                <td>10</td>
                                <td>15</td>
                                <td>25</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>13</td>
                                <td>23</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>0</td>
                                <td>10</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>13</td>
                                <td>5</td>
                                <td>10</td>
                                <td>5</td>
                                <td>15</td>
                                <td><strong>95.3</strong></td>
                                <td><span class="font-20">100.0% </span> <a href="#">View Data</a></td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="f-left">
                                        <p class="f-left font-10">May <br>2015</p>
                                        <p class="f-left font-20"><strong>10</strong></p>
                                        <div class="clear"></div>
                                        <p class="font-10 margin-left-10 / margin-right-10">Sunday</p>                                        
                                    </div>


                                </td>
                                <td>10</td>
                                <td>15</td>
                                <td>25</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>13</td>
                                <td>23</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>0</td>
                                <td>10</td>
                                <td><strong>95.3</strong></td>
                                <td>10</td>
                                <td>13</td>
                                <td>5</td>
                                <td>10</td>
                                <td>5</td>
                                <td>15</td>
                                <td><strong>95.3</strong></td>
                                <td><span class="font-20">100.0% </span> <a href="#">View Data</a></td>
                            </tr>

                        </tbody>
                       
                    </table>
                </div>
            </div>
        </div>

    </section>



<?php include "../construct/footer.php"; ?>