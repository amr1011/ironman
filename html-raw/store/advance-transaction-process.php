<?php include "../construct/header.php"; ?>

	<section class="container-fluid" section-style="top-panel">

        <!-- search order -->
		<div class="row header-container">
			<div class="contents">
				<h1 class="f-left">Advance Transactions</h1>
				<div class="f-right margin-top-20">
                    
					<button class="btn btn-dark">FAQ</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>
        <div class="row">
            <div class="contents margin-top-20">
                <!-- search -->
                <div class="f-left">
                    <label class="margin-bottom-5">search:</label><br>
                    <input class="search f-left xlarge" type="text">
                </div>
                <!-- search by -->
                <div class="f-left margin-left-20">
                    <label class="margin-bottom-5">search by:</label><br>
                    <div class="select large">
                        <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt"></div><div class="frm-custom-icon"></div><div class="frm-custom-dropdown-option"><div class="option" data-value="Order ID">Order ID</div></div></div><select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="Order ID">Order ID</option>                          
                        </select>
                    </div>
                </div>
    
                <button class="f-left btn btn-dark margin-top-20 margin-left-20">Search</button>
                <div class="clear"></div>
            </div>
        </div>
        <div class="row margin-top-20">
            <div class="contents line">

                
                <div class="f-right bggray-white">
                    <p class="f-left font-12 padding-left-10 padding-top-5">
                        <strong>Sort By:</strong>
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">   
                        <strong>Customer Name</strong>  
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">  
                        <strong>Call Center</strong>     
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-top-5">
                        <a class="red-color active" href="">
                            <strong>Criticality</strong>
                            <img src="../assets/images/ui/sort-top-arrow.png">
                        </a>
                    </p>
                </div>
                <div class="clear"></div>
            </div>
        </div>

	</section>

	<section class="container-fluid" section-style="content-panel">
		<div class="row">
			<!-- sample-1-->

            <div class="content-container unboxed">
                <h3 class="f-left no-margin-all">Waiting Advance Transactions</h3>                
                <p class="f-right font-14 margin-top-5"><strong>1 Transaction</strong></p>
                <div class="clear"></div>
            </div>
			<div class="content-container">
                <div>
                    <div class="f-left margin-top-15">
                        <p class="font-20"><strong>Order ID: 734784</strong></p>                                    
                        
                    </div>                  
                    <div class=" f-right margin-bottom-10">
                        <p class="font-14 margin-bottom-5 "><strong><span class="gray-color">WAITING ORDER</span></strong></p>                        
                        
                        <p class="font-14 margin-bottom-5  "><strong><span class="red-color">Elapsed Time:</span></strong> 00:01:45</p>
                        
                        
                    </div>
                    <div class="clear"></div>
                    <hr>

                    <div class="data-container split">
                        
                        <!-- clients information -->
                        <div class="margin-top-20">
                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Name: </strong></p>
                            <p class="f-right font-12">Jonathan R. Omido</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Contact Number: </strong></p>
                            <p class="f-right font-12">(+63) 915-516-6153 <i class="fa fa-mobile margin-right-5"></i>Globe</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Transaction Time:</strong></p>
                            <p class="f-right font-12">May 18, 2015 | 10:11:31</p>
                            <div class="clear"></div>                       
                        </div>

                        <!-- delivery address -->
                        <div>
                            <label class="margin-top-10 margin-bottom-10 ">Delivery Address:</label>
                            <div class="bggray-light padding-all-5 font-14 small-curved-border">
                                <div class="display-inline-mid margin-right-10 padding-left-20">
                                    <img src="../assets/images/work-icon.png" alt="work icon">
                                    <p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
                                </div>
                                <div class="display-inline-mid margin-left-10  padding-left-10">
                                    <p class="no-margin-all"><strong>Cr8v Web Solutions, Inc.</strong></p>
                                    <p class="no-margin-all">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR</p>
                                    <p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
                                </div>                          
                            </div>                          
                        </div>

                        <!-- cards -->
                        <label class="margin-top-15">CARDS:</label>
                        <div class="bggray-light padding-all-5 font-14 small-curved-border">
                            <div class="display-inline-mid margin-right-10">
                                <img class="thumb" src="../assets/images/happy-plus.jpg" alt="happy-plus">
                            </div>
                            <div class="display-inline-mid margin-left-10 divider padding-left-10">
                                <p class="no-margin-all">0083-123456-46578<br>
                                <span class="red-color"><strong>Exp. Date:</strong></span> September 20, 2016</p>
                            </div>
                            <div class="display-inline-mid text-center margin-left-10">
                                <a href="#">Show Card<br>History</a>
                            </div>
                        </div>


                        <div class="margin-top-10">
                            <p class="f-left font-12 red-color  margin-bottom-5"><strong>Payment Method: </strong></p>
                            <p class="f-right font-12">Cash</p>
                            <div class="clear"></div>

                            <p class="f-left font-12 red-color  margin-bottom-5"><strong>Change For: </strong></p>
                            <p class="f-right font-12">500.00 PHP</p>
                            <div class="clear"></div>

                            <p class="f-left font-12 red-color  margin-bottom-5"><strong>Remarks: </strong></p>
                            <p class="f-right font-12 gray-color">No Remarks</p>
                            <div class="clear"></div>
                            
                        </div>
                    </div>



                    <div class="data-container split margin-left-15">
                        
                        <div class="padding-all-5 margin-top-10">
                            <p class="font-12 red-color f-left"><strong>Delivery Date &amp; Time:</strong></p>
                            <p class="font-12 f-right"><strong>May 19, 2015 | 11:00 PM</strong></p>
                            <div class="clear"></div>
                        </div>
                        <label class="margin-top-15">order: </label>
                         

                        

                        <div class="small-curved-border">
                            <table class="font-14">
                                <thead class="bggray-dark">
                                    <tr>
                                        <th class="padding-all-10">Quantitiy</th>
                                        <th class="padding-all-10">Product</th>
                                        <th class="padding-all-10">Price</th>
                                        <th class="padding-all-10">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody class="bggray-light">
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10"><div class="arrow-down"></div> Champ Amazing aloha + French Fries + Choice o..</td>
                                        <td class="padding-all-10">203.50</td>
                                        <td class="padding-all-10">407.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Large Coca-Cola</td>
                                        <td class="padding-all-10">17.60</td>
                                        <td class="padding-all-10">35.20</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Regular French Fries</td>
                                        <td class="padding-all-10">0.00</td>
                                        <td class="padding-all-10">0.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10">Sundae Strawberry</td>
                                        <td class="padding-all-10">30.80</td>
                                        <td class="padding-all-10">61.60</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10">Yum w/ Cheese</td>
                                        <td class="padding-all-10">44.00</td>
                                        <td class="padding-all-10">88.00</td>
                                    </tr>                                                                       
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Total Cost</td>
                                        <td colspan="2" class="padding-all-10 text-right">265.10 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Added VAT</td>
                                        <td colspan="2" class="padding-all-10 text-right">26.50 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Delivery Charge</td>
                                        <td colspan="2" class="padding-all-10 text-right">20.00 PHP</td>
                                    </tr>
                                    <tr class="bggray-dark">
                                        <td colspan="2" class="padding-all-10 text-left font-16"><strong>Total Bill</strong></td>
                                        <td colspan="2" class="padding-all-10 text-right font-14"><strong>611.80 PHP</strong></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>

                    </div>
                    <div class="margin-top-20">
                        <button type="button" class="btn btn-dark f-right margin-right-10 modal-trigger">Order Logs</button>
                        <button type="button" class="btn btn-dark f-right margin-right-10 modal-trigger" modal-target="accept-order">Accept Order</button>                           
                        <div class="clear"></div>
                    </div>
                </div>
            </div> 
		</div>
	</section>

    <!-- modal ACCEPT ORDER -->
    <div class="modal-container" modal-id="accept-order">
        <div class="modal-body xsmall">

            <div class="modal-head">
                <h4 class="text-left">Set Alarm for Advance Order</h4>
                <div class="modal-close close-me"></div>
            </div>

            <div class="modal-content no-margin-all">
               <label>Set Alarm:</label>
               <br />
                <div class="select large">
                    <select>
                        <option value="1hour">1 Hour before Delivery Date / Time</option>
                    </select>
                </div>
                <div class="form-group padding-top-10">
                    <input class="chck-box " id="card-number" type="checkbox">
                    <label class="chck-lbl" for="card-number">Set custom Time</label>
                </div>
                
                <div>
                    <label class="gray-color">Alarm Time:</label>
                </div>

                <div class="date-picker time f-left ">
                    <input type="text" disabled>
                    <span class="fa fa-clock-o text-center red-color"></span>
                </div>
                <div class="clear"></div>
            </div> 

            <!-- button -->
            <div class="margin-top-20 text-center">                
                <button type="button" class="btn btn-dark close-me">Confirm</button>         
            </div>    
            

        </div>
    </div>
<?php include "../construct/footer.php"; ?>