    <?php include "../construct/header.php"; ?>

    <section class="container-fluid" section-style="top-panel">
        <div class="row header-container">
            <div class="contents">
                <h1 class="f-left">Search Order</h1>
                <div class="f-right margin-top-20">
                    <button class="btn btn-dark margin-right-10 disabled">Download Excel File</button>
                    <button class="btn btn-dark ">FAQ</button>
                </div>
                <div class="clear"></div>
            </div>
        </div>
<div class="row">
            <div class="contents margin-top-20">

                <div class="display-inline-mid">
                    <!-- date from -->
                    <div class="f-left margin-right-10 ">
                        <label class="margin-bottom-5">Date From:</label><br>
                        <div class="date-picker">
                            <input type="text">
                            <span class="fa fa-calendar text-center red-color"></span>
                        </div>
                    </div>
                    <!-- date from -->
                    <div class="f-left">
                        <label class="margin-bottom-5">Date From:</label><br>
                        <div class="date-picker">
                            <input type="text">
                            <span class="fa fa-calendar text-center red-color"></span>
                        </div>
                    </div>
                    <!-- generate report -->
                    <button type="button" class="btn btn-dark / margin-left-20 margin-top-20">Generate Report</button>
                    <div class="clear"></div>
                </div>


                <div class="display-inline-mid  padding-left-10 margin-left-10">
                    <section class="container-fluid" section-style="content-panel">        
                        <!-- generate by period -->
                        <div class="divider padding-left-20">
                            <label>Generate by Period:</label><br />
                            
                            <div class="select small">
                                <select>
                                    <option value="Daily">Daily</option>
                                    <option value="Weekly">Weekly</option>
                                    <option value="Monthly">Monthly</option>
                                </select>
                            </div>
                            
                            <button type="button" class="btn / btn-dark / margin-left-20">Generate Report</button>
                        </div>                                    
                    </section>

                </div>
                <div class="margin-top-20">
                    <hr>
                </div>
            </div>            
        </div>
       

        <div class="row">
            <div class="contents text-center margin-top-100">

                <p class="font-50"><img src="../assets/images/report_1_.svg" alt="report image"></i></p>
                <h4 class="gray-color margin-top-30 font-20">Please select date range <br />to generate report </h4>

            </div>
        </div>
    </section>



<?php include "../construct/footer.php"; ?>