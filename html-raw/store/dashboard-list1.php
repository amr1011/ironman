<?php include "../construct/header.php"; ?>

	<section class="container-fluid" section-style="top-panel">


        <!-- search order -->
		<div class="row header-container">
			<div class="contents">
				<h1 class="f-left">Dashboard</h1>
				<div class="f-right margin-top-20">
                    
					<button class="btn btn-dark">FAQ</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>

        
		<div class="row">
			<div class="contents margin-top-20">

                <!-- rounded boxes -->
                <div class= " margin-bottom-20">
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / small">
                            <p class="font-16 margin-top-5 / padding-top-20">Waiting</p>
                            <br />
                            <p class="font-16 / padding-bottom-10">2</p>
                        </div>
                    </div>
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / small">
                            <p class="font-16 / margin-top-5 / padding-top-20 ">Assembling</p>
                            <br />
                            <p class="font-16 / padding-bottom-10">2</p>
                        </div>
                    </div>

                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / small">
                            <p class="font-16 margin-top-5 / padding-top-20">For Dispatch</p>
                            <br />
                            <p class="font-16 / padding-bottom-10">2</p>
                        </div>
                    </div>
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / small">
                            <p class="font-16 margin-top-5 / padding-top-20">Rider Out</p>
                            <br />
                            <p class="font-16 / padding-bottom-10">1</p>
                        </div>
                    </div>

                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / small">
                            <p class="font-16 margin-top-5 / padding-top-20" >Rider Out</p>
                            <br />
                            <p class="font-16 / padding-bottom-10">0</p>
                        </div>
                    </div>
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / small">
                            <p class="font-16 margin-top-5 / padding-top-20">For Pick-up</p>
                            <br />
                            <p class="font-16 / padding-bottom-10">0</p>
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>

                <!-- rounded big boxes -->
                <div class="margin-bottom-20">
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / medium">
                            <p class="font-16 margin-top-5 / padding-top-20">Completed On-Time</p>
                            <br />
                            <p class="font-20 / padding-bottom-10">245</p>
                        </div>
                    </div>
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / medium">
                            <p class="font-16 margin-top-5 / padding-top-20">Total Transaction (Today)</p>
                            <br />
                            <p class="font-20 / padding-bottom-10">253</p>
                        </div>
                    </div>
                    
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / medium">
                            <p class="font-16 margin-top-5 / padding-top-20">Hit-Rate (Today)</p>
                            <br />
                            <p class="font-20 / red-color / padding-bottom-10">94.5%</p>
                        </div>
                    </div>
                    
                    <div class="clear"></div>
                </div>

                <!-- search -->
				<div class="f-left">
					<label class="margin-bottom-5">search:</label><br>
					<input class="search f-left xlarge" type="text">
				</div>
                <!-- search by -->
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">search by:</label><br>
					<div class="select xlarge">
						<select>
							<option value="Order ID">Order ID</option>							
						</select>
					</div>
                    <buton type="button" class="btn btn-dark / margin-left-20">Search</buton>
				</div>
               
			</div>
		</div>
        
        <div class="row margin-top-20">
            <div class="contents line">
                <div class="select xlarge">
					<select>
						<option value="All Transaction">Show all Transaction</option>
						<option value="Accepted Transaction">Show Accepted Transaction</option>
                        <option value="Assembling Transaction">Show Assembling Transaction</option>
                        <option value="Dispatch Transaction">Show for Dispatch Transaction</option>
                        <option value="Rider Out">Show Rider Out Transaction</option>
                        <option value="Rider In">Show RIder In Transaction</option>
                        <option value="Completed Transaction">Show Completed Transaction</option>                        
					</select>
				</div>
				<span class="white-space"></span>
                
                
				<span class="white-space"></span>
                
                <div class="f-right bggray-white">
                    <p class="f-left font-12 padding-left-10 padding-top-5">
                        <strong>Sort By:</strong>
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">   
                        <strong>Customer Name</strong>  
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">  
                        <strong>Call Center</strong>     
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-top-5">
                        <a class="red-color active" href="">
                            <strong>Criticality</strong>
                            <img src="../assets/images/ui/sort-top-arrow.png">
                        </a>
                    </p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
	</section>

	<section class="container-fluid" section-style="content-panel">
		<div class="row">
            <div class="content-container unboxed">

                <div class="error-msg">
                    As of May 18, 2015 | 1:10 PM, Order ID: 9554482 has been cancelled
                    <div class="f-right / close-error / padding-top-5">
                        <img src="../assets/images/ui/icon-close-white.png" alt="close button" class="padding-bottom-5"></div>
                    <div class="clear"></div>
                </div>

                 <div class="error-msg / light-blue-bg">
                    As of May 18, 2015 | 1:10 PM, Order ID: 9554482 has been edited
                    <div class="f-right / close-error / padding-top-5">
                        <img src="../assets/images/ui/icon-close-white.png" alt="close button" class="padding-bottom-5"></div>
                    <div class="clear"></div>
                </div>

                <div class="error-msg / light-yellow-bg ">
                    As of May 18, 2015 | 1:10 PM, Order ID: 9554482 has been followed-up by the customer                    
                    <div class="f-right / close-error / padding-top-5">
                        <img src="../assets/images/ui/icon-close-white.png" alt="close button" class="padding-bottom-5"></div>
                    <div class="clear"></div>
                </div>
            </div>
			<!-- sample-1-->
            <div class="content-container unboxed">
                <h3 class="f-left no-margin-all">Waiting Transactions</h3>                
                <p class="f-right font-14 no-margin-bottom"><strong>2 Transaction</strong></p>
                <div class="clear"></div>
            </div>
			<div class="content-container viewable">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>House Address # 1</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">Rm. 404 3rd Floor, 691 Eduaredo Bldg, Green Field St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Beside gas station</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20    ">
                        <p class="font-16 margin-bottom-5 gray-color"><strong>WAITING ORDER</strong></p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:02:39</p>
                        <button type="button" class="btn btn-dark / margin-top-10">Confirm Order</button>
                    </div>
                    <div class="clear"></div>
                </div>
			</div>
            <div class="content-container viewable">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>House Address # 1</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">674 Balingit Bldg, Green Field St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Beside gas station</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20    ">
                        <p class="font-16 margin-bottom-5 gray-color"><strong>WAITING ORDER</strong></p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:02:39</p>
                        <button type="button" class="btn btn-dark / margin-top-10">Confirm Order</button>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>



            <!-- sample-2 -->

            
            <div class="content-container unboxed">
                <h3 class="f-left no-margin-all">Assembling Transactions</h3>                
                <p class="f-right font-14 no-margin-bottom"><strong>2 Transactions</strong></p>
                <div class="clear"></div>
            </div>
            <div class="content-container viewable">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>Cr8v Websolutions, Inc.</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">66D 2nd Floor, 591 Cr8v Bldg, San Rafael St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20">
                        <p class="font-16 margin-bottom-5 yellow-color"><strong>ASSEMBLING ORDER</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:01:39</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:01:39</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="content-container viewable">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>Cr8v Websolutions, Inc.</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">66D 2nd Floor, 591 Cr8v Bldg, San Rafael St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20">
                        <p class="font-16 margin-bottom-5 yellow-color"><strong>ASSEMBLING ORDER</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:01:39</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:01:39</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="content-container viewable">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>Cr8v Websolutions, Inc.</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">66D 2nd Floor, 591 Cr8v Bldg, San Rafael St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20">
                        <p class="font-16 margin-bottom-5 yellow-color"><strong>ASSEMBLING ORDER</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:01:39</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:01:39</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
			

             <!-- sample-3 -->

           
            <div class="content-container unboxed">
                <h3 class="f-left no-margin-all">For Dispatch Transaction</h3>                
                <p class="f-right font-14 no-margin-bottom"><strong>1 Transaction</strong></p>
                <div class="clear"></div>
            </div>
            <div class="content-container viewable ">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>Cr8v Websolutions, Inc.</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">66D 2nd Floor, 591 Cr8v Bldg, San Rafael St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20    ">
                        <p class="font-16 margin-bottom-5 "><strong>FOR DISPATCH</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> <span class="red-color">00:00:00</span></p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:01:39</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="content-container viewable ">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954478</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>House Address #1</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">647 Balingit Bldg, Green Field St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Beside gas station</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20    ">
                        <p class="font-16 margin-bottom-5 "><strong>FOR DISPATCH</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:01:39</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:01:39</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
             <div class="content-container viewable ">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954478</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>House Address #1</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">647 Balingit Bldg, Green Field St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Beside gas station</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20    ">
                        <p class="font-16 margin-bottom-5 "><strong>FOR DISPATCH</strong></p>
                        <p><strong><span class="yellow-color">BULK ORDER </span></strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:00:00</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:25:36</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
             <div class="content-container viewable ">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954478</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>House Address #1</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">647 Balingit Bldg, Green Field St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Beside gas station</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20    ">
                        <p class="font-16 margin-bottom-5 "><strong>NO ASSIGNED RIDER</strong></p>                        
                        <p><strong><span class="red-color">Timer: </span></strong> <span class="red-color">00:00:00</span></p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:25:36</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            
            <!-- sample-4 -->


            <div class="content-container unboxed">
                <h3 class="f-left no-margin-all">Rider Out Transactions</h3>                
                <p class="f-right font-14 no-margin-bottom"><strong>1 Transaction</strong></p>
                <div class="clear"></div>
            </div>
            <div class="content-container red-bg">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>Cr8v Websolutions, Inc.</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">66D 2nd Floor, 591 Cr8v Bldg, San Rafael St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="margin-left-10  "> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20    ">
                        <p class="font-16 margin-bottom-5 red-color"><strong>RIDER'S OUT (LATE)</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong><span class="red-color"> 00:01:39</span></p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:01:39</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
             <div class="content-container viewable">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>Cr8v Websolutions, Inc.</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">66D 2nd Floor, 591 Cr8v Bldg, San Rafael St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20    ">
                        <p class="font-16 margin-bottom-5"><strong>RIDER'S OUT</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:01:39</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:01:39</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="content-container viewable">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954478</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>House Address #1</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">647 Balingit Bldg, Green Field St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20    ">
                        <p class="font-16 margin-bottom-5"><strong>RIDER'S OUT</strong></p>
                        <p><strong><span class="yellow-color">BULK ORDER</span></strong> </p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:01:39</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:01:39</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

             <!-- sample 5 -->


            <div class="content-container unboxed">
                <h3 class="f-left no-margin-all">Rider In Transactions</h3>                
                <p class="f-right font-14 no-margin-bottom"><strong>1 Transaction</strong></p>
                <div class="clear"></div>
            </div>
            <div class="content-container viewable">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>Cr8v Websolutions, Inc.</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">66D 2nd Floor, 591 Cr8v Bldg, San Rafael St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="margin-left-10  "> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20    ">
                        <p class="font-16 margin-bottom-5"><strong>RIDER IN</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:01:39</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:01:39</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>        
			
            <!-- sample 6 -->

            <div class="content-container unboxed">
                <h3 class="f-left no-margin-all">Completed Transactions</h3>                
                <p class="f-right font-14 no-margin-bottom"><strong>221 Transaction</strong></p>
                <div class="clear"></div>
            </div>
            <div class="content-container viewable">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954478</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>House Address #1</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">647 Balingit Bldg, Green Field St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20    ">
                        <p class="font-16 margin-bottom-5 green-color"><strong>ORDER COMPLETED</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:25:36</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 11:25:36</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
             <div class="content-container viewable">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>Cr8v Websolutions, Inc.</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">66D 2nd Floor, 591 Cr8v Bldg, San Rafael St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20    ">
                        <p class="font-16 margin-bottom-5 green-color"><strong>ORDER COMPLETED</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:25:36</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 11:25:36</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
			
           
            
		</div>
	</section>

<?php include "../construct/footer.php"; ?>