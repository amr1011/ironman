<?php include "../construct/header.php"; ?>

	<section class="container-fluid" section-style="top-panel">

        <!-- search order -->
		<div class="row header-container">
			<div class="contents">
				<h1 class="f-left">All Transaction</h1>
				<div class="f-right margin-top-20">
                    <button class="btn btn-dark margin-right-20">Product Status Schedule</button>
					<button class="btn btn-dark">FAQ</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>

        
		<div class="row">
			<div class="contents margin-top-20">
                <!-- search -->
				<div class="f-left">
					<label class="margin-bottom-5">search:</label><br>
					<input class="search f-left normal" type="text">
				</div>
                <!-- search by -->
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">search by:</label><br>
					<div class="select small">
						<select>
							<option value="Order ID">Order ID</option>							
						</select>
					</div>
				</div>
                <!-- from -->
				<div class="f-left margin-left-20">
                    <label>From: </label>
					<div class="date-picker">
                        <input type="text">
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
				</div>
                <!-- to -->
                <div class="f-left margin-left-20">
                    <label>To: </label>
                    <div class="date-picker">
                        <input type="text">
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                </div>                
				<button class="f-left btn btn-dark margin-top-20 margin-left-20">Search</button>
                <div class="clear"></div>
			</div>
		</div>
        
        <div class="row margin-top-20">
            <div class="contents line">
                <div class="select xlarge">
					<select>
						<option value="All Transaction">Show all Transaction</option>
						<option value="Accepted Transaction">Show Accepted Transaction</option>
                        <option value="Assembling Transaction">Show Assembling Transaction</option>
                        <option value="Dispatch Transaction">Show for Dispatch Transaction</option>
                        <option value="Rider Out">Show Rider Out Transaction</option>
                        <option value="Rider In">Show RIder In Transaction</option>
                        <option value="Completed Transaction">Show Completed Transaction</option>                        
					</select>
				</div>
				<span class="white-space"></span>
                
                
				<span class="white-space"></span>
                
                <div class="f-right bggray-white">
                    <p class="f-left font-12 padding-left-10 padding-top-5">
                        <strong>Sort By:</strong>
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">   
                        <strong>Customer Name</strong>  
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">  
                        <strong>Call Center</strong>     
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-top-5">
                        <a class="red-color active" href="">
                            <strong>Criticality</strong>
                            <img src="../assets/images/ui/sort-top-arrow.png">
                        </a>
                    </p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
	</section>

	<section class="container-fluid" section-style="content-panel">
		<div class="row">
			<!-- sample-1-->

            <div class="content-container unboxed">
                <h3 class="f-left no-margin-all">Completed Transactions</h3>                
                <p class="f-right font-14 no-margin-bottom"><strong>221 Transaction</strong></p>
                <div class="clear"></div>
            </div>
			<div class="content-container">
                <div>
                    <div class="f-left margin-top-15">
                        <p class="font-20"><strong>Order ID: 734784</strong></p>                                    
                        
                    </div>                  
                    <div class=" f-right margin-bottom-10">
                        <p class="font-14 margin-bottom-5 "><strong><span class="green-color">ORDER COMPLETED</span></strong></p>                        
                        
                        <p class="font-14 margin-bottom-5  "><strong><span class="red-color">Elapsed Time:</span></strong> 00:01:45</p>
                        <p class="font-14 margin-bottom-5 "><strong><span class="red-color">Time Completed:</span></strong> 11:25:36</p>
                        
                    </div>
                    <div class="clear"></div>
                    <hr>

                    <div class="data-container split">
                        
                        <!-- clients information -->
                        <div class="margin-top-20">
                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Name: </strong></p>
                            <p class="f-right font-12">Jonathan R. Omido</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Contact Number: </strong></p>
                            <p class="f-right font-12">(+63) 915-516-6153 <i class="fa fa-mobile margin-right-5"></i>Globe</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Transaction Time:</strong></p>
                            <p class="f-right font-12">May 18, 2015 | 10:11:31</p>
                            <div class="clear"></div>                       
                        </div>

                        <!-- delivery address -->
                        <div>
                            <label class="margin-top-10 margin-bottom-10 ">Delivery Address:</label>
                            <div class="bggray-light padding-all-5 font-14 small-curved-border">
                                <div class="display-inline-mid margin-right-10 padding-left-20">
                                    <img src="../assets/images/work-icon.png" alt="work icon">
                                    <p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
                                </div>
                                <div class="display-inline-mid margin-left-10  padding-left-10">
                                    <p class="no-margin-all"><strong>Cr8v Web Solutions, Inc.</strong></p>
                                    <p class="no-margin-all">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR</p>
                                    <p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
                                </div>                          
                            </div>                          
                        </div>

                        <!-- cards -->
                        <label class="margin-top-15">CARDS:</label>
                        <div class="bggray-light padding-all-5 font-14 small-curved-border">
                            <div class="display-inline-mid margin-right-10">
                                <img class="thumb" src="../assets/images/happy-plus.jpg" alt="happy-plus">
                            </div>
                            <div class="display-inline-mid margin-left-10 divider padding-left-10">
                                <p class="no-margin-all">0083-123456-46578<br>
                                <span class="red-color"><strong>Exp. Date:</strong></span> September 20, 2016</p>
                            </div>
                            <div class="display-inline-mid text-center margin-left-10">
                                <a href="#">Show Card<br>History</a>
                            </div>
                        </div>


                        <div class="margin-top-10">
                            <p class="f-left font-12 red-color  margin-bottom-5"><strong>Payment Method: </strong></p>
                            <p class="f-right font-12">Cash</p>
                            <div class="clear"></div>

                            <p class="f-left font-12 red-color  margin-bottom-5"><strong>Change For: </strong></p>
                            <p class="f-right font-12">500.00 PHP</p>
                            <div class="clear"></div>

                            <p class="f-left font-12 red-color  margin-bottom-5"><strong>Remarks: </strong></p>
                            <p class="f-right font-12 gray-color">No Remarks</p>
                            <div class="clear"></div>
                            
                        </div>
                    </div>



                    <div class="data-container split margin-left-15">
                        <label class="margin-top-15">order: </label>
                         

                        

                        <div class="small-curved-border">
                            <table class="font-14">
                                <thead class="bggray-dark">
                                    <tr>
                                        <th class="padding-all-10">Quantitiy</th>
                                        <th class="padding-all-10">Product</th>
                                        <th class="padding-all-10">Price</th>
                                        <th class="padding-all-10">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody class="bggray-light">
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10"><div class="arrow-down"></div> Champ Amazing aloha + French Fries + Choice o..</td>
                                        <td class="padding-all-10">203.50</td>
                                        <td class="padding-all-10">407.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Large Coca-Cola</td>
                                        <td class="padding-all-10">17.60</td>
                                        <td class="padding-all-10">35.20</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center gray-color">2</td>
                                        <td class="padding-all-10 padding-left-35">Regular French Fries</td>
                                        <td class="padding-all-10">0.00</td>
                                        <td class="padding-all-10">0.00</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10">Sundae Strawberry</td>
                                        <td class="padding-all-10">30.80</td>
                                        <td class="padding-all-10">61.60</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10" colspan="4"><hr></td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10 text-center">2</td>
                                        <td class="padding-all-10">Yum w/ Cheese</td>
                                        <td class="padding-all-10">44.00</td>
                                        <td class="padding-all-10">88.00</td>
                                    </tr>                                                                       
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Total Cost</td>
                                        <td colspan="2" class="padding-all-10 text-right">265.10 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Added VAT</td>
                                        <td colspan="2" class="padding-all-10 text-right">26.50 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" class="padding-all-10 text-left">Delivery Charge</td>
                                        <td colspan="2" class="padding-all-10 text-right">20.00 PHP</td>
                                    </tr>
                                    <tr class="bggray-dark">
                                        <td colspan="2" class="padding-all-10 text-left font-16"><strong>Total Bill</strong></td>
                                        <td colspan="2" class="padding-all-10 text-right font-14"><strong>611.80 PHP</strong></td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>

                    </div>
                    <div class="margin-top-20">
                        <button type="button" class="btn btn-dark f-right margin-right-10 modal-trigger" modal-target="order-logs">Order Logs</button>                           
                        <div class="clear"></div>
                    </div>
                </div>
            </div> 
            
		</div>

        <div class="content-container opaque">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>Cr8v Websolutions, Inc.</strong></p>
                        <p class=" margin-left-10 margin-bottom-5">66D 2nd Floor, 591 Cr8v Bldg, San Rafael St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20    ">
                        <p class="font-16 margin-bottom-5 green-color"><strong>ORDER COMPLETED</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:01:39</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:01:39</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
	</section>



    <!-- modal ORDER LOGS -->
    <div class="modal-container " modal-id="order-logs">
        <div class="modal-body small">
            <div class="modal-head">
                <h4 class="text-left">Order ID: 954458 - Order Logs </h4>
                <div class="modal-close close-me"></div>
            </div>


            <div class="modal-content">

                <div class="width-60per f-left">
                    <p class="font-12 red-color f-left"><strong>Name: </strong></p>
                    <p class="font-12 f-right">Jonathan R. Ornido</p>
                    <div class="clear"></div>
                 
                    <p class="font-12 red-color f-left"><strong>Contact Number: </strong></p>
                    <p class="font-12 f-right">(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    <div class="clear"></div>
                </div>

                <div class="width-40per f-right">
                <p class="font-12 red-color f-left"><strong>Delivery Time: </strong></p>
                    <p class="font-12 f-right">20 Minutes</p>
                    <div class="clear"></div>
                 
                    <p class="font-12 red-color f-left"><strong>Payment Mode: </strong></p>
                    <p class="font-12 f-right">Cash</p>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                
                <div class="tbl">
                    <table class="text-center">
                        <thead>
                            <tr>
                                <th>Store Logs</th>
                                <th>Time</th>
                                <th>Duration</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Order Accepted</td>
                                <td class="text-center">11:24:30 AM</td>
                                <td class="text-center">00:00:22</td>
                                
                            </tr>
                            <tr>
                                <td>Assembled Order</td>
                                <td class="text-center">11:25:30 AM</td>
                                <td class="text-center">00:01:00</td>
                                
                            </tr>
                            <tr>
                                <td>Waiting for a Rider</td>
                                <td class="text-center">11:26:08 AM</td>
                                <td class="text-center">00:00:38 </td>                                
                            </tr>
                            <tr>
                                <td>Assign Rider - <strong>John Ramos</strong></td>
                                <td class="text-center">11:26:37 AM</td>
                                <td class="text-center">00:00:29</td>
                                
                            </tr>
                            <tr>
                                <td>Rider Out</td>
                                <td class="text-center">11:28:17 AM</td>
                                <td class="text-center">00:02:20 </td>                                
                            </tr>
                            <tr>
                                <td>Rider In</td>
                                <td class="text-center">12:42:30 PM</td>
                                <td class="text-center">00:20:13 </td>                                
                            </tr>                                                                                     
                        </tbody>                        
                    </table>    
                    <table>
                        <thead>
                            <tr>
                                <th>
                                    <p class="font-12 f-left">Date and Time Completed:</p>
                                    <p class="font-12 f-right">May 18, 2015 | 11:25:36 AM</p>
                                    <div class="clear"></div>
                                </th>
                            </tr>
                        </thead>
                    </table>    
                </div>              
            </div>

            <button type="button" class="btn btn-dark f-right margin-right-20 close-me">Close</button>
            <div class="clear"></div>
        </div>
    </div>
<?php include "../construct/footer.php"; ?>