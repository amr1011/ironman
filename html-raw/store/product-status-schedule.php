<?php include "../construct/header.php"; ?>

	<section class="container-fluid" section-style="top-panel">

        <!-- title name with buttons -->
		<div class="row header-container">
			<div class="contents">

                <div class="contents">
                    <div class="breadcrumbs f-left  margin-top-20">
                        <p><a href="#">Product Management</a> <i class="fa fa-angle-right"></i> <strong>Product Status Schedule</strong></p>
                    </div>                
                    <div class="clear"></div>
                </div>               
                <div class="clear"></div>
				<h1 class="f-left">Product Status Schedule</h1>
				<div class="f-right margin-top-20">
                    <button class="btn btn-dark margin-right-20 modal-trigger" modal-target="set-status">Set Status Schedule</button>                    
					<button class="btn btn-dark">FAQ</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>
    



		<div class="row">
			<div class="contents margin-top-15">
                <!-- search -->
				<div class="f-left">
					<label class="margin-bottom-5">search:</label><br>
					<input class="search f-left xlarge" type="text">
				</div>
                <!-- search by -->
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">search by:</label><br>
					<div class="select xlarge">
						<select>
							<option value="Product-Name">Product Name</option>							
						</select>
					</div>
				</div>
                            
				<button class="f-left btn btn-dark margin-top-20 margin-left-20">Search</button>
                <div class="clear"></div>
			</div>
		</div>
        
        <div class="row margin-top-20">

            <div class="contents line">
            <hr />                            
            </div>
        </div>
	</section>

	<section class="container-fluid" section-style="content-panel">
		<div class="row">			
            <div class="content-container unboxed">
                <div class="tbl no-padding-all">
                    <table>
                        <thead >
                            <tr>
                                <th class="width-10per text-center ">Category</th>
                                <th class="text-left">Product</th>
                                <th class="text-center / min-width-20">Mon</th>
                                <th class="text-center / min-width-20">Tue</th>
                                <th class="text-center / min-width-20">Wed</th>
                                <th class="text-center / min-width-20">Thu</th>
                                <th class="text-center / min-width-20">Fri</th>
                                <th class="text-center / min-width-20">Sat</th>
                                <th class="text-center / min-width-20">Sun</th>
                            </tr>
                        </thead>
                        <tbody class="font-12 / text-center">
                            <tr>
                                <td><strong>Burger</strong></td>
                                <td class="text-left"><strong>Champ</strong></td>
                                <td>
                                    <span class="green-color "><strong>Available</strong></span>
                                    <br />
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>
                                    <br />
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>
                                    <br />
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>
                                    <br />
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>
                                    <br />
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color / font-12"><strong>Available</strong></span>
                                    <br />
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <p class="red-color / font-12"><strong>Not <br />Available</strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Burger</strong></td>
                                <td class="text-left"><strong>Champ Amazing Aloha</strong></td>
                                <td colspan="7">
                                    <span class="green-color / font-20"><strong>Available</strong></span>
                                    
                                    <p class="font-20">7:00 am - 10:00 pm</p>
                                </td>
                                
                            </tr>
                            <tr>
                                <td><strong>Noodles</strong></td>
                                <td class="text-left"><strong>Jolly Spaghetti</strong></td>
                                <td colspan="7">
                                    <span class="green-color / font-20"><strong>Available</strong></span>                                    
                                    <p class="font-20">7:00 am - 10:00 pm</p>
                                </td>                               
                            </tr>
                            <tr>
                                <td><strong>Chicken</strong></td>
                                <td class="text-left"><strong>Chickenjoy</strong></td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>                                
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>
                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>
                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <p class="red-color font-12" ><strong>Not<br /> Available</strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Breakfast</strong></td>
                                <td class="text-left"><strong>Pancake with Hotdog</strong></td>
                                 <td colspan="7">
                                    <span class="green-color / font-20"><strong>Available</strong></span>                                    
                                    <p class="font-20">7:00 am - 10:00 pm</p>
                                </td> 
                                
                            </tr>
                            <tr>
                                <td><strong>Drinks</strong></td>
                                <td class="text-left"><strong>Coca-Cola</strong></td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>                                
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>
                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>
                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <p class="red-color font-12" ><strong>Not<br /> Available</strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>Hotdog</strong></td>
                                <td class="text-left"><strong>Jolly Hotdog Classic</strong></td>
                                <td>
                                     <p class="red-color font-12" ><strong>Not<br /> Available</strong></p>
                                </td>
                                <td>
                                    <p class="red-color font-12" ><strong>Not<br /> Available</strong></p>
                                </td>
                                <td>
                                     <p class="red-color font-12" ><strong>Not<br /> Available</strong></p>
                                </td>
                                <td>
                                    <p class="red-color font-12" ><strong>Not<br /> Available</strong></p>
                                </td>
                                <td>
                                     <p class="red-color font-12" ><strong>Not<br /> Available</strong></p>
                                </td>
                                <td>
                                  <span class="green-color"><strong>Available</strong></span>
                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                   <span class="green-color"><strong>Available</strong></span>
                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                            </tr>
                            <tr>
                                <td><strong>French Fries</strong></td>
                                <td class="text-left"><strong>Jolly Crispy Fries</strong></td>
                               <td>
                                    <span class="green-color"><strong>Available</strong></span>                                
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>
                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>
                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <p class="red-color font-12" ><strong>Not<br /> Available</strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td>Noodles</td>
                                <td class="text-left">Palabok</td>
                               <td>
                                    <span class="green-color"><strong>Available</strong></span>                                
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>
                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <span class="green-color"><strong>Available</strong></span>
                                    
                                    <p class="font-12">7:00 am -<br /> 10:00 pm</p>
                                </td>
                                <td>
                                    <p class="red-color font-12" ><strong>Not<br /> Available</strong></p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>           
		</div>
	</section>

    <!-- modal SET STATUS -->
    <div class="modal-container " modal-id="set-status">
        <div class="modal-body small">
            <div class="modal-head">
                <h4 class="text-left">Set Status Schedule</h4>
                <div class="modal-close close-me"></div>
            </div>

            <!-- content -->
            <div class="modal-content">
                <label>Search Product:</label>
                <input type="text" class="xlarge">

                <div class="bggray-dark">
                    <p class="padding-bottom-30 / padding-top-30 / margin-top-10 / text-center / font-20 / gray-color">PLEASE SELECT PRODUCTS</p>
                </div>
                 <div class="bggray-dark">

                    <div class="div-inside-noline / small / margin-top-20 / margin-bottom-10">
                    <table class="table-inside">
                        <tbody>
                            <tr >
                                <td >                                    
                                    <p class="font-12 f-left"><strong>Champ</strong></p>  
                                    <p class="f-right / gray-color ">Burger <img src="../assets/images/ui/icon-close.png" alt="close-icon" class="margin-left-30"></p> 
                                </td>
                            </tr>
                            <tr>
                                 <td>            
                                    <p class="font-12 f-left"><strong>Yam</strong></p>  
                                    <p class="f-right / gray-color ">Burger <img src="../assets/images/ui/icon-close.png" alt="close-icon" class="margin-left-30"></p>
                                </td>
                            </tr>
                            <tr>
                                 <td class="">   
                                 <p class="font-12 f-left"><strong>Chocolate Sundae</strong></p>                                   
                                    <p class="f-right / gray-color "> Sundae <img src="../assets/images/ui/icon-close.png" alt="close-icon" class="margin-left-30"></p>                                                                           
                                </td>
                            </tr>
                            <tr>
                                 <td class="">                                    
                                <p class="font-12 f-left"><strong>Jolly Spaghetti</strong></p>                                   
                                    <p class="f-right / gray-color "> Spaghetti<img src="../assets/images/ui/icon-close.png" alt="close-icon" class="margin-left-30"></p>                                                                           
                                </td>
                            </tr>   
                            <tr>
                                 <td class="">                                    
                                 <p class="font-12 f-left"><strong>Chicken Joy</strong></p>                                   
                                    <p class="f-right / gray-color "> Chicken<img src="../assets/images/ui/icon-close.png" alt="close-icon" class="margin-left-30"></p>                                                                                                                             
                                </td>
                            </tr>     
                             <tr>
                                 <td class="">                                    
                                 <p class="font-12 f-left"><strong>Pancake with Ham</strong></p>                                   
                                    <p class="f-right / gray-color "> Breakfast<img src="../assets/images/ui/icon-close.png" alt="close-icon" class="margin-left-30"></p>                                                                                                                                           
                                </td>
                            </tr>    
                             <tr>
                                 <td class="">                                    
                                    <p class="font-12 f-left"><strong>Jolly Crispy Fries</strong></p>                                   
                                    <p class="f-right / gray-color "> French Fries<img src="../assets/images/ui/icon-close.png" alt="close-icon" class="margin-left-30"></p>                                                                                                                                                       
                                </td>
                            </tr>    
                             <tr>
                                 <td class="">                                    
                                    <p class="font-12 f-left"><strong>Palabok</strong></p>                                   
                                    <p class="f-right / gray-color "> Noodles <img src="../assets/images/ui/icon-close.png" alt="close-icon" class="margin-left-30"></p>                                                                                                                                             
                                </td>
                            </tr>    
                             <tr>
                                 <td class="">                                    
                                    <p class="font-12 f-left"><strong>Jolly Hotdot Classic</strong></p>                                   
                                    <p class="f-right / gray-color "> Hotdog <img src="../assets/images/ui/icon-close.png" alt="close-icon" class="margin-left-30"></p>                                                                                                                                                     
                                </td>
                            </tr>                          

                            
                        </tbody>
                    </table>
                </div>
                </div>

                

                <div class="f-left">
                    <div class=" margin-top-10">                        
                        <label >Available from: </label>
                        
                        <div class="margin-left-5 / position-rel">
                            <input type="text" class="normal">
                             <div class="time-display">                                                                            
                            <table border="0">
                                <thead>
                                    <tr>
                                        <th class=" font-16 / padding-left-15">Hour</th>
                                        <th>:</th>                                
                                        <th class=" font-16 / padding-left-10">Minute</th>
                                        <th class="font-16 / padding-left-10">Period</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="2"><button type="button" class="btn / btn-dark / margin-left-10 / margin-bottom-10 "><i class="fa fa-chevron-up / "></i></button></td>
                                        <td><button type="button" class="btn / btn-dark / margin-left-10 / margin-bottom-10"><i class="fa fa-chevron-up"></i></button></td>
                                        <td><button type="button" class="btn / btn-dark / margin-left-10 / margin-bottom-10"><i class="fa fa-chevron-up"></i></button></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><input type="text" class="xsmall / margin-right-5 "></td>
                                        <td><input type="text" class="xsmall /  margin-right-5"></td>
                                        <td><input type="text" class="xsmall /  "></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><button type="button" class="btn / btn-dark / margin-left-10 / margin-top-10"><i class="fa fa-chevron-down"></i></button></td>
                                        <td><button type="button" class="btn / btn-dark / margin-left-10 / margin-top-10 "><i class="fa fa-chevron-down"></i></button></td>
                                        <td><button type="button" class="btn / btn-dark / margin-left-10 / margin-top-10"><i class="fa fa-chevron-down"></i></button></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="padding-top-10 / text-center">
                                            <button type="button" class="btn btn-dark">Confirm</button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>                                                
                        </div> 
                        
                        </div>
                                              
                    </div>                    
                    <div class=" margin-top-10">
                        <label>Available from: </label>

                        <div class="margin-left-5 / position-rel">
                        <input type="text" class="normal">
                            <div class="time-display">                                                                            
                                <table border="0">
                                    <thead>
                                        <tr>
                                            <th class=" font-16 / padding-left-15">Hour</th>
                                            <th>:</th>                                
                                            <th class=" font-16 / padding-left-10">Minute</th>
                                            <th class="font-16 / padding-left-10">Period</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="2"><button type="button" class="btn / btn-dark / margin-left-10 / margin-bottom-10 "><i class="fa fa-chevron-up / "></i></button></td>
                                            <td><button type="button" class="btn / btn-dark / margin-left-10 / margin-bottom-10"><i class="fa fa-chevron-up"></i></button></td>
                                            <td><button type="button" class="btn / btn-dark / margin-left-10 / margin-bottom-10"><i class="fa fa-chevron-up"></i></button></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><input type="text" class="xsmall / margin-right-5 "></td>
                                            <td><input type="text" class="xsmall /  margin-right-5"></td>
                                            <td><input type="text" class="xsmall /  "></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><button type="button" class="btn / btn-dark / margin-left-10 / margin-top-10"><i class="fa fa-chevron-down"></i></button></td>
                                            <td><button type="button" class="btn / btn-dark / margin-left-10 / margin-top-10 "><i class="fa fa-chevron-down"></i></button></td>
                                            <td><button type="button" class="btn / btn-dark / margin-left-10 / margin-top-10"><i class="fa fa-chevron-down"></i></button></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="padding-top-10 / text-center">
                                                <button type="button" class="btn btn-dark">Confirm</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>                                                
                            </div>
                        </div>       
                    </div>
                                                                
                </div>
                <div class="f-right / margin-top-10 / margin-left-40">
                    <p class="red-color / margin-bottom-10 "><strong>Available From:</strong></p>
                    
                    <!-- monday -->
                    <div class="form-group f-left / margin-right-50">
                        <input class="chck-box " id="monday" type="checkbox">
                        <label class="chck-lbl / no-margin-all" for="monday">Monday</label>
                    </div>
                    <!-- friday -->
                    <div class="form-group f-left">
                        <input class="chck-box " id="friday" type="checkbox">
                        <label class="chck-lbl / no-margin-all" for="friday">Friday</label>
                    </div>
                    <div class="clear"></div>

                    <!-- tuesday -->
                    <div class="form-group f-left / margin-right-45">
                        <input class="chck-box " id="tuesday" type="checkbox">
                        <label class="chck-lbl / " for="tuesday">Tuesday</label>
                    </div>
                    <!-- saturday -->
                    <div class="form-group f-left">
                        <input class="chck-box " id="saturday" type="checkbox">
                        <label class="chck-lbl / " for="saturday">Saturday</label>
                    </div>
                    <div  class="clear"></div>


                    <!-- wednesday -->
                    <div class="form-group f-left / margin-right-30 ">
                        <input class="chck-box " id="wednesday" type="checkbox">
                        <label class="chck-lbl / " for="wednesday">Wednesday</label>
                    </div>
                    <!-- sunday -->
                    <div class="form-group f-left">
                        <input class="chck-box " id="sunday" type="checkbox">
                        <label class="chck-lbl / " for="sunday">Sunday</label>
                    </div>
                    <div  class="clear"></div>

                    <!-- thursday -->
                    <div class="form-group f-left">
                        <input class="chck-box " id="thursday" type="checkbox">
                        <label class="chck-lbl / " for="thursday">Thursday</label>
                    </div>
                    <div  class="clear"></div>


                </div>
                <div class="clear"></div>

             

            </div>
            <!-- button -->
            <div class="f-right margin-right-20 margin-bottom-10">
                    <button type="button" class="btn btn-dark margin-right-10">Confirm</button>
                    <button type="button" class="btn btn-dark close-me">Cancel</button>
            </div>
            <div class="clear"></div>
        </div>
    </div>

<?php include "../construct/footer.php"; ?>