<?php include "../construct/header.php"; ?>

	<section class="container-fluid" section-style="top-panel">

        <!-- title name with buttons -->
		<div class="row header-container">
			<div class="contents">               
                <div class="clear"></div>
				<h1 class="f-left">Sales Reports by Product</h1>
				<div class="f-right margin-top-20">
                    <button class="btn btn-dark margin-right-20">Download Excel File</button>                    
					<button class="btn btn-dark">FAQ</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>
        
        <!-- upper forms -->
        <div class="row">
            <div class="contents margin-top-20">

                <div class="display-inline-mid">
                    <!-- date from -->
                    <div class="f-left margin-right-10 ">
                        <label class="margin-bottom-5">Date From:</label><br>
                        <div class="date-picker">
                            <input type="text">
                            <span class="fa fa-calendar text-center red-color"></span>
                        </div>
                    </div>
                    <!-- date from -->
                    <div class="f-left">
                        <label class="margin-bottom-5">Date From:</label><br>
                        <div class="date-picker">
                            <input type="text">
                            <span class="fa fa-calendar text-center red-color"></span>
                        </div>
                    </div>
                    <!-- generate report -->
                    <button type="button" class="btn btn-dark / margin-left-10 margin-top-20">Generate Report</button>
                    <div class="clear"></div>
                </div>


                <div class="display-inline-mid  padding-left-10 margin-left-10">
                    <section class="container-fluid" section-style="content-panel">        
                        <!-- generate by period -->
                        <div class="divider padding-left-20">
                            <label>Generate by Period:</label>
                            <br />
                            <div class="select small margin-left-10">
                                <select>
                                    <option value="monthly">Monthly</option>
                                </select>
                            </div>
                            <button type="button" class="btn / btn-dark / margin-left-20">Generate Report</button>
                        </div>                                    
                    </section>

                </div>
                <div class="margin-top-20">
                    <hr />
                </div>
            </div>            
        </div>
        



		<div class="row">
			<div class="contents margin-top-15">
                <!-- search -->
				<div class="f-left">
					<label class="margin-bottom-5">search:</label><br>
					<input class="search f-left xlarge" type="text">
				</div>
                <!-- search by -->
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">search by:</label><br>
					<div class="select xlarge">
						<select>
							<option value="Product-Name">Product Name</option>							
						</select>
					</div>
				</div>
                            
				<button class="f-left btn btn-dark margin-top-20 margin-left-20">Search</button>
                <div class="clear"></div>
			</div>
		</div>
        
        <div class="row margin-top-20">

            <div class="contents line">
            <hr />                            
            </div>
        </div>
	</section>

	<section class="container-fluid" section-style="content-panel">
		<div class="row">			
            <div class="content-container unboxed">
                <div class="tbl no-padding-all">
                    <table>
                        <thead>
                            <tr>
                                <th class="width-10per text-center">Date</th>
                                <th class="width-10per">Product</th>
                                <th>Food Sales</th>
                                <th>Sales</th>
                                <th>Quantity</th>
                                <th>Sales %</th>
                                <th>Quantity %</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="margin-left-20">
                                        <div class="f-left">
                                            <p class="f-left font-10">May <br />2015</p>
                                            <p class="f-left font-16"><strong>01</strong></p>
                                            <div class="clear"></div>
                                            <p class="font-10 margin-left-10">Friday</p>                                        
                                        </div>                   
                                        <div class="f-left margin-left-20 margin-right-20">
                                            <p class="font-20"><strong>-</strong></p>
                                        </div>
                                        <div class="f-left">
                                            <p class="f-left font-10">May <br />2015</p>
                                            <p class="f-left font-16"><strong>01</strong></p>
                                            <div class="clear"></div>
                                            <p class="font-10 margin-left-10 ">Friday</p>                                        
                                        </div>                                               
                                        <div class="clear"></div>
                                    </div>                                   
                                </td>
                                <td> Coca-Cola</td>
                                <td>15,487.75 PHP </td>
                                <td>15,487.75 PHP</td>
                                <td>584</td>
                                <td>84.2 %</td>
                                <td>75.3 %</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="margin-left-20">
                                        <div class="f-left">
                                            <p class="f-left font-10">May <br />2015</p>
                                            <p class="f-left font-16"><strong>01</strong></p>
                                            <div class="clear"></div>
                                            <p class="font-10 margin-left-10">Friday</p>                                        
                                        </div>                   
                                        <div class="f-left margin-left-20 margin-right-20">
                                            <p class="font-20"><strong>-</strong></p>
                                        </div>
                                        <div class="f-left">
                                            <p class="f-left font-10">May <br />2015</p>
                                            <p class="f-left font-16"><strong>01</strong></p>
                                            <div class="clear"></div>
                                            <p class="font-10 margin-left-10 ">Friday</p>                                        
                                        </div>                                               
                                        <div class="clear"></div>
                                    </div>                                   
                                </td>
                                <td>Champ</td>
                                <td>15,487.75 PHP </td>
                                <td>15,487.75 PHP</td>
                                <td>584</td>
                                <td>84.2 %</td>
                                <td>75.3 %</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="margin-left-20">
                                        <div class="f-left">
                                            <p class="f-left font-10">May <br />2015</p>
                                            <p class="f-left font-16"><strong>01</strong></p>
                                            <div class="clear"></div>
                                            <p class="font-10 margin-left-10">Friday</p>                                        
                                        </div>                   
                                        <div class="f-left margin-left-20 margin-right-20">
                                            <p class="font-20"><strong>-</strong></p>
                                        </div>
                                        <div class="f-left">
                                            <p class="f-left font-10">May <br />2015</p>
                                            <p class="f-left font-16"><strong>01</strong></p>
                                            <div class="clear"></div>
                                            <p class="font-10 margin-left-10 ">Friday</p>                                        
                                        </div>                                               
                                        <div class="clear"></div>
                                    </div>                                   
                                </td>
                                <td>Champ Amazing Aloha</td>
                                <td>15,487.75 PHP </td>
                                <td>15,487.75 PHP</td>
                                <td>584</td>
                                <td>84.2 %</td>
                                <td>75.3 %</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="margin-left-20">
                                        <div class="f-left">
                                            <p class="f-left font-10">May <br />2015</p>
                                            <p class="f-left font-16"><strong>01</strong></p>
                                            <div class="clear"></div>
                                            <p class="font-10 margin-left-10">Friday</p>                                        
                                        </div>                   
                                        <div class="f-left margin-left-20 margin-right-20">
                                            <p class="font-20"><strong>-</strong></p>
                                        </div>
                                        <div class="f-left">
                                            <p class="f-left font-10">May <br />2015</p>
                                            <p class="f-left font-16"><strong>01</strong></p>
                                            <div class="clear"></div>
                                            <p class="font-10 margin-left-10 ">Friday</p>                                        
                                        </div>                                               
                                        <div class="clear"></div>
                                    </div>                                   
                                </td>
                                <td>Chessy Bacon Mushroom Champ</td>
                                <td>15,487.75 PHP </td>
                                <td>15,487.75 PHP</td>
                                <td>584</td>
                                <td>84.2 %</td>
                                <td>75.3 %</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="margin-left-20">
                                        <div class="f-left">
                                            <p class="f-left font-10">May <br />2015</p>
                                            <p class="f-left font-16"><strong>01</strong></p>
                                            <div class="clear"></div>
                                            <p class="font-10 margin-left-10">Friday</p>                                        
                                        </div>                   
                                        <div class="f-left margin-left-20 margin-right-20">
                                            <p class="font-20"><strong>-</strong></p>
                                        </div>
                                        <div class="f-left">
                                            <p class="f-left font-10">May <br />2015</p>
                                            <p class="f-left font-16"><strong>01</strong></p>
                                            <div class="clear"></div>
                                            <p class="font-10 margin-left-10 ">Friday</p>                                        
                                        </div>                                               
                                        <div class="clear"></div>
                                    </div>                                   
                                </td>
                                <td>1pc Chickenjoy</td>
                                <td>15,487.75 PHP </td>
                                <td>15,487.75 PHP</td>
                                <td>584</td>
                                <td>84.2 %</td>
                                <td>75.3 %</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="margin-left-20">
                                        <div class="f-left">
                                            <p class="f-left font-10">May <br />2015</p>
                                            <p class="f-left font-16"><strong>01</strong></p>
                                            <div class="clear"></div>
                                            <p class="font-10 margin-left-10">Friday</p>                                        
                                        </div>                   
                                        <div class="f-left margin-left-20 margin-right-20">
                                            <p class="font-20"><strong>-</strong></p>
                                        </div>
                                        <div class="f-left">
                                            <p class="f-left font-10">May <br />2015</p>
                                            <p class="f-left font-16"><strong>01</strong></p>
                                            <div class="clear"></div>
                                            <p class="font-10 margin-left-10 ">Friday</p>                                        
                                        </div>                                               
                                        <div class="clear"></div>
                                    </div>                                   
                                </td>
                                <td>2pcs Chickenjoy</td>
                                <td>15,487.75 PHP </td>
                                <td>15,487.75 PHP</td>
                                <td>584</td>
                                <td>84.2 %</td>
                                <td>75.3 %</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="margin-left-20">
                                        <div class="f-left">
                                            <p class="f-left font-10">May <br />2015</p>
                                            <p class="f-left font-16"><strong>01</strong></p>
                                            <div class="clear"></div>
                                            <p class="font-10 margin-left-10">Friday</p>                                        
                                        </div>                   
                                        <div class="f-left margin-left-20 margin-right-20">
                                            <p class="font-20"><strong>-</strong></p>
                                        </div>
                                        <div class="f-left">
                                            <p class="f-left font-10">May <br />2015</p>
                                            <p class="f-left font-16"><strong>01</strong></p>
                                            <div class="clear"></div>
                                            <p class="font-10 margin-left-10 ">Friday</p>                                        
                                        </div>                                               
                                        <div class="clear"></div>
                                    </div>                                   
                                </td>
                                <td>1pc Chickenjoy with Jolly Spaghetti</td>
                                <td>15,487.75 PHP </td>
                                <td>15,487.75 PHP</td>
                                <td>584</td>
                                <td>84.2 %</td>
                                <td>75.3 %</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="margin-left-20">
                                        <div class="f-left">
                                            <p class="f-left font-10">May <br />2015</p>
                                            <p class="f-left font-16"><strong>01</strong></p>
                                            <div class="clear"></div>
                                            <p class="font-10 margin-left-10">Friday</p>                                        
                                        </div>                   
                                        <div class="f-left margin-left-20 margin-right-20">
                                            <p class="font-20"><strong>-</strong></p>
                                        </div>
                                        <div class="f-left">
                                            <p class="f-left font-10">May <br />2015</p>
                                            <p class="f-left font-16"><strong>01</strong></p>
                                            <div class="clear"></div>
                                            <p class="font-10 margin-left-10 ">Friday</p>                                        
                                        </div>                                               
                                        <div class="clear"></div>
                                    </div>                                   
                                </td>
                                <td>Chickenjoy Bucket (8pcs)</td>
                                <td>15,487.75 PHP </td>
                                <td>15,487.75 PHP</td>
                                <td>584</td>
                                <td>84.2 %</td>
                                <td>75.3 %</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="margin-left-20">
                                        <div class="f-left">
                                            <p class="f-left font-10">May <br />2015</p>
                                            <p class="f-left font-16"><strong>01</strong></p>
                                            <div class="clear"></div>
                                            <p class="font-10 margin-left-10">Friday</p>                                        
                                        </div>                   
                                        <div class="f-left margin-left-20 margin-right-20">
                                            <p class="font-20"><strong>-</strong></p>
                                        </div>
                                        <div class="f-left">
                                            <p class="f-left font-10">May <br />2015</p>
                                            <p class="f-left font-16"><strong>01</strong></p>
                                            <div class="clear"></div>
                                            <p class="font-10 margin-left-10 ">Friday</p>                                        
                                        </div>                                               
                                        <div class="clear"></div>
                                    </div>                                   
                                </td>
                                <td>Chickenjoy with Palabok</td>
                                <td>15,487.75 PHP </td>
                                <td>15,487.75 PHP</td>
                                <td>584</td>
                                <td>84.2 %</td>
                                <td>75.3 %</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>           
		</div>
	</section>

<?php include "../construct/footer.php"; ?>