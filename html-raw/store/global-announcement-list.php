<?php include "../construct/header.php"; ?>

    <section class="container-fluid" section-style="top-panel">

        <!-- search order -->
        <div class="row header-container">
            <div class="contents">
                <h1 class="f-left">All Transaction</h1>
                <div class="f-right margin-top-20">
                    <button class="btn btn-dark margin-right-20">Product Status Schedule</button>
                    <button class="btn btn-dark">FAQ</button>
                </div>
                <div class="clear"></div>
            </div>
        </div>

        
        <div class="row">
            <div class="contents margin-top-20">
                <!-- search -->
                <div class="f-left">
                    <label class="margin-bottom-5">search:</label><br>
                    <input class="search f-left xlarge" type="text">
                </div>
                <!-- search by -->
                <div class="f-left margin-left-20">
                    <label class="margin-bottom-5">search by:</label><br>
                    <div class="select large">
                        <select>
                            <option value="Order ID">Order ID</option>                          
                        </select>
                    </div>
                </div>
                             
                <button class="f-left btn btn-dark margin-top-20 margin-left-20">Search</button>
                <div class="clear"></div>
            </div>
        </div>
        
        <div class="row margin-top-20">
            <div class="contents line">
                <div class="select xlarge">
                    <select>
                        <option value="All Transaction">Show all Transaction</option>
                        <option value="Accepted Transaction">Show Accepted Transaction</option>
                        <option value="Assembling Transaction">Show Assembling Transaction</option>
                        <option value="Dispatch Transaction">Show for Dispatch Transaction</option>
                        <option value="Rider Out">Show Rider Out Transaction</option>
                        <option value="Rider In">Show RIder In Transaction</option>
                        <option value="Completed Transaction">Show Completed Transaction</option>                        
                    </select>
                </div>
                <span class="white-space"></span>
                
                
                <span class="white-space"></span>
                
                <div class="f-right bggray-white">
                    <p class="f-left font-12 padding-left-10 padding-top-5">
                        <strong>Sort By:</strong>
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">   
                        <strong>Customer Name</strong>  
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">  
                        <strong>Call Center</strong>     
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-top-5">
                        <a class="red-color active" href="">
                            <strong>Criticality</strong>
                            <img src="../assets/images/ui/sort-top-arrow.png">
                        </a>
                    </p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </section>

    <section class="container-fluid" section-style="content-panel">
        <div class="row">
            <!-- sample-1-->

            <div class="content-container unboxed">
                <h3 class="f-left no-margin-all">Waiting Transactions</h3>                
                <p class="f-right font-14 no-margin-bottom"><strong>1 Transaction</strong></p>
                <div class="clear"></div>
            </div>
            <div class="content-container">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>Cr8v Websolutions, Inc.</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">66D 2nd Floor, 591 Cr8v Bldg, San Rafael St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20    ">
                        <p class="font-16 margin-bottom-5 gray-color"><strong>ORDER ACCEPTED</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:01:39</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:01:39</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>



            <!-- sample-2 -->

            
            <div class="content-container unboxed">
                <h3 class="f-left no-margin-all">Assembling Transactions</h3>                
                <p class="f-right font-14 no-margin-bottom"><strong>2 Transactions</strong></p>
                <div class="clear"></div>
            </div>
            <div class="content-container">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>Cr8v Websolutions, Inc.</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">66D 2nd Floor, 591 Cr8v Bldg, San Rafael St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20">
                        <p class="font-16 margin-bottom-5 yellow-color"><strong>ASSEMBLING ORDER</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:01:39</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:01:39</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="content-container">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>Cr8v Websolutions, Inc.</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">66D 2nd Floor, 591 Cr8v Bldg, San Rafael St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20">
                        <p class="font-16 margin-bottom-5 yellow-color"><strong>ASSEMBLING ORDER</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:01:39</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:01:39</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            
            

             <!-- sample-3 -->

           
            <div class="content-container unboxed">
                <h3 class="f-left no-margin-all">For Dispatch Transaction</h3>                
                <p class="f-right font-14 no-margin-bottom"><strong>2 Transaction</strong></p>
                <div class="clear"></div>
            </div>
            <div class="content-container ">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Delivery Time:</span> </strong>20 Minutes</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Address: </span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5"><strong>Cr8v Websolutions, Inc.</span></strong></p>
                        <p class=" margin-left-10 margin-bottom-5">66D 2nd Floor, 591 Cr8v Bldg, San Rafael St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="gray-color"> - Near Jewels Convinience Store</p>
                    </div>
                    
                    <div class="width-25per f-left margin-top-20    ">
                        <p class="font-16 margin-bottom-5 "><strong>NO ASSIGNED RIDER</strong></p>
                        <p><strong><span class="red-color">Timer: </span></strong> 00:01:39</p>
                        <p><strong><span class="red-color">Elapsed Time: </span></strong> 00:01:39</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            
          
           
            
        </div>
    </section>

<?php include "../construct/footer.php"; ?>