    <?php include "../construct/header.php"; ?>

    <section class="container-fluid" section-style="top-panel">
        <div class="row header-container">
            <div class="contents">
                <h1 class="f-left">Sales Reports</h1>
                <div class="f-right margin-top-20">
                    <button class="btn btn-dark margin-right-10 disabled">Download Excel File</button>
                    <button class="btn btn-dark ">FAQ</button>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="row">
            <div class="contents margin-top-20">

                <div class="display-inline-mid">
                    <!-- date from -->
                    <div class="f-left margin-right-10 ">
                        <label class="margin-bottom-5">Date From:</label><br>
                        <div class="date-picker">
                            <input type="text">
                            <span class="fa fa-calendar text-center red-color"></span>
                        </div>
                    </div>
                    <!-- date from -->
                    <div class="f-left">
                        <label class="margin-bottom-5">Date From:</label><br>
                        <div class="date-picker">
                            <input type="text">
                            <span class="fa fa-calendar text-center red-color"></span>
                        </div>
                    </div>
                    <!-- generate report -->
                    <button type="button" class="btn btn-dark / margin-left-20 margin-top-20">Generate Report</button>
                    <div class="clear"></div>
                </div>


                <div class="display-inline-mid  padding-left-10 margin-left-10">
                    <section class="container-fluid" section-style="content-panel">        
                        <!-- generate by period -->
                        <div class="divider padding-left-20">
                            <label>Generate by Period:</label><br />
                            
                            <div class="select small">
                                <select>
                                    <option value="Daily">Daily</option>
                                    <option value="Weekly">Weekly</option>
                                    <option value="Monthly">Monthly</option>
                                </select>
                            </div>
                            
                            <button type="button" class="btn / btn-dark / margin-left-20">Generate Report</button>
                        </div>                                    
                    </section>

                </div>
                <div class="margin-top-20">
                    <hr>
                </div>
            </div>            
        </div>
       
        <div class="row">
            <div class="contents margin-top-20">
                <div class="rounded-container / f-left / margin-right-20 / padding-left-15">
                    <div class="rounded-box / xmedium">
                        <p class="font-16 margin-top-10 / padding-top-10">Date</p>
                        <br />
                        <p class="font-16 / margin-left-20 / margin-right-20 ">May 1, 2015 to</p>
                        <br />
                        <p class="font-16 / margin-left-20 / margin-right-20 / padding-bottom-10">May 18, 2015</p>
                    </div>
                </div>
                <div class="rounded-container / f-left / margin-right-20">
                    <div class="rounded-box / xmedium">
                        <p class="font-16 margin-top-10 / padding-top-10">Total Sales</p>
                        <br>
                        <p class="font-20 / margin-top-10 / padding-bottom-15 ">501,487.00 PHP</p>
                    </div>
                </div>
                <div class="rounded-container / f-left / margin-right-20">
                    <div class="rounded-box / xmedium">
                        <p class="font-16 margin-top-10 / padding-top-10">Food Sales</p>
                        <br>
                        <p class="font-20 / margin-top-10 / padding-bottom-15">501,487.00 PHP</p>
                    </div>
                </div>
                <div class="rounded-container / f-left / ">
                    <div class="rounded-box / xmedium">
                        <p class="font-16 margin-top-10 / padding-top-10">Transaction Count</p>
                        <br>
                        <p class="font-20 / margin-top-10 / padding-bottom-15">4,578</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="contents">
                <div class="tbl / text-center">
                    <table>
                        <thead>
                            <tr>
                                <th class="width-10per / text-center">No.</th>
                                <th class="text-center">Date</th>
                                <th class="text-center">Total Sales</th>
                                <th class="text-center"> Food Sales</th>
                                <th class="text-center">Transaction Court</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>May 1, 2015</td>
                                <td>15,021.25 Php</td>
                                <td>15,124.75 Php</td>
                                <td>215</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>May 1, 2015</td>
                                <td>15,021.25 Php</td>
                                <td>15,124.75 Php</td>
                                <td>215</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>May 1, 2015</td>
                                <td>15,021.25 Php</td>
                                <td>15,124.75 Php</td>
                                <td>215</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>May 1, 2015</td>
                                <td>15,021.25 Php</td>
                                <td>15,124.75 Php</td>
                                <td>215</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>May 1, 2015</td>
                                <td>15,021.25 Php</td>
                                <td>15,124.75 Php</td>
                                <td>215</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>May 1, 2015</td>
                                <td>15,021.25 Php</td>
                                <td>15,124.75 Php</td>
                                <td>215</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td>May 1, 2015</td>
                                <td>15,021.25 Php</td>
                                <td>15,124.75 Php</td>
                                <td>215</td>
                            </tr>
                            <tr>
                                <td>8</td>
                                <td>May 1, 2015</td>
                                <td>15,021.25 Php</td>
                                <td>15,124.75 Php</td>
                                <td>215</td>
                            </tr>
                            <tr>
                                <td>9</td>
                                <td>May 1, 2015</td>
                                <td>15,021.25 Php</td>
                                <td>15,124.75 Php</td>
                                <td>215</td>
                            </tr>
                            <tr>
                                <td>10</td>
                                <td>May 1, 2015</td>
                                <td>15,021.25 Php</td>
                                <td>15,124.75 Php</td>
                                <td>215</td>
                            </tr>
                            <tr>
                                <td>11</td>
                                <td>May 1, 2015</td>
                                <td>15,021.25 Php</td>
                                <td>15,124.75 Php</td>
                                <td>215</td>
                            </tr>
                            <tr>
                                <td>12</td>
                                <td>May 1, 2015</td>
                                <td>15,021.25 Php</td>
                                <td>15,124.75 Php</td>
                                <td>215</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>May 1, 2015</td>
                                <td>15,021.25 Php</td>
                                <td>15,124.75 Php</td>
                                <td>215</td>
                            </tr>
                            <tr>
                                <td>13</td>
                                <td>May 1, 2015</td>
                                <td>15,021.25 Php</td>
                                <td>15,124.75 Php</td>
                                <td>215</td>
                            </tr>
                            <tr>
                                <td>14</td>
                                <td>May 1, 2015</td>
                                <td>15,021.25 Php</td>
                                <td>15,124.75 Php</td>
                                <td>215</td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>



<?php include "../construct/footer.php"; ?>