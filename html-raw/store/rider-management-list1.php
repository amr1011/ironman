<?php include "../construct/header.php"; ?>

	<section class="container-fluid" section-style="top-panel">

        <!-- search order -->
		<div class="row header-container">
			<div class="contents">
				<h1 class="f-left">Rider Management</h1>
				<div class="f-right margin-top-20">
                    <button class="btn btn-dark margin-right-20">Rider Archives</button>
                    <button class="btn btn-dark margin-right-20 / modal-trigger" modal-target="add-riders">Add New Rider</button>
					<button class="btn btn-dark">FAQ</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>
        
		<div class="row">
			<div class="contents margin-top-20">
                <!-- search -->
				<div class="f-left">
					<label class="margin-bottom-5">search:</label><br>
					<input class="search f-left xlarge" type="text">
				</div>
                <!-- search by -->
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">search by:</label><br>
					<div class="select xlarge">
						<select>
							<option value="Order ID">Rider Name</option>							
						</select>
					</div>
				</div>
                            
				<button class="f-left btn btn-dark margin-top-20 margin-left-20">Search</button>
                <div class="clear"></div>
			</div>
		</div>
        
        <div class="row margin-top-20">
            <div class="contents line">
                <div class="select xlarge">
					<select>
						<option value="All Riders">Show All Riders</option>						
					</select>
				</div>
				<span class="white-space"></span>
                
                
				<span class="white-space"></span>
                
                <div class="f-right bggray-white">
                    <p class="f-left font-12 padding-left-10 padding-top-5">
                        <strong>Sort By:</strong>
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">   
                        <strong>Rider Name</strong>  
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-top-5">
                        <a class="red-color active" href="">
                            <strong>Availability</strong>
                            <img src="../assets/images/ui/sort-top-arrow.png">
                        </a>
                    </p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
	</section>

	<section class="container-fluid" section-style="content-panel">
		<div class="row">
			<!-- sample-1-->

            <div class="content-container unboxed">
                <h3 class="f-left no-margin-all">Available Riders</h3>                
                <p class="f-right font-14 no-margin-bottom"><strong>2 Riders</strong></p>
                <div class="clear"></div>
            </div>
			<div class="content-container">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name:</span> Juan Manuel R. Garcia</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Contact Num: </span> </strong>(+63) 915-148-3691 <i class="fa fa-mobile"></i> Globe</p>                        
                    </div>
                    
                    <div class="width-35per f-left">
                       <p class="font-14 margin-bottom-5"><strong><span class="red-color">Location:</span></strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="gray-color">In Store</span></strong></p>
                    </div>
                    
                    <div class="width-25per f-left">
                       <p class="font-14 margin-bottom-5"><strong><span class="yellow-color">ON HOLD</span></strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Order On Hand: </span>3 Orders</strong></p>
                    </div>
                    <div class="clear"></div>
                </div>
			</div>
            <div class="content-container">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name:</span> Edgardo A. Gutierez</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Contact Num: </span> </strong>(+63) 915-148-3691 <i class="fa fa-mobile"></i> Globe</p>                        
                    </div>
                    
                    <div class="width-35per f-left">
                       <p class="font-14 margin-bottom-5"><strong><span class="red-color">Location:</span></strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="gray-color">In Store</span></strong></p>
                    </div>
                    
                    <div class="width-25per f-left">
                       <p class="font-14 margin-bottom-5"><strong><span class="green-color">AVAILABLE</span></strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Order On Hand: </span><span class="gray-color">None</span></strong></p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>




            <!-- sample-2 -->

            
            <div class="content-container unboxed">
                <h3 class="f-left no-margin-all">Riders on Field</h3>                
                <p class="f-right font-14 no-margin-bottom"><strong>3 Riders</strong></p>
                <div class="clear"></div>
            </div>
            <div class="content-container viewable">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name:</span> Benjie A. Gutierez</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Contact Num: </span> </strong>(+63) 915-148-3691 <i class="fa fa-mobile"></i> Globe</p>                        
                    </div>
                    
                    <div class="width-35per f-left">
                       <p class="font-14 margin-bottom-5"><strong><span class="red-color">Location:</span></strong></p>
                        <p class="font-14 margin-bottom-5">2nd St, Brgy. Kapitolyo, Pasig City - NCR</p>
                        <p class="font-14 margin-bottom-5"><strong><span class="gray-color">12:34 PM</span></strong></p>
                    </div>
                    
                    <div class="width-25per f-left">
                       <p class="font-14 margin-bottom-5"><strong><span class="red-color">RIDERS OUT</span></strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Orders On Hand: </span>3 Orders</strong></p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="content-container viewable">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name:</span> Miguel R. Perez</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Contact Num: </span> </strong>(+63) 915-148-3691 <i class="fa fa-mobile"></i> Globe</p>                        
                    </div>
                    
                    <div class="width-35per f-left">
                       <p class="font-14 margin-bottom-5"><strong><span class="red-color">Location:</span></strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="gray-color">-</span></strong></p>
                    </div>
                    
                    <div class="width-25per f-left">
                       <p class="font-14 margin-bottom-5"><strong><span class="red-color">RIDER OUT</span></strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Orders On Hand: </span>2 Orders</strong></p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="content-container viewable">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name:</span> John A. Cruz</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Contact Num: </span> </strong>(+63) 915-148-3691 <i class="fa fa-mobile"></i> Globe</p>                        
                    </div>
                    
                    <div class="width-35per f-left">
                       <p class="font-14 margin-bottom-5"><strong><span class="red-color">Location:</span></strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="gray-color">-</span></strong></p>
                    </div>
                    
                    <div class="width-25per f-left">
                       <p class="font-14 margin-bottom-5"><strong><span class="red-color">RIDER OUT</span></strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Orders On Hand: </span>3 Orders</strong></p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div> 
		</div>
	</section>

    <!-- modal ADD NEW RIDERS -->

   <div class="modal-container" modal-id="add-riders">
        <div class="modal-body small">
            <div class="modal-head">
                <h4 class="text-left padding-left-20">Add New Riders</h4>
                <div class="modal-close close-me"></div>
            </div>


            <div class="modal-content">
                <div class="margin-left-15 margin-right-15">
                    <label>Contact Number: <span class="red-color">*</span></label>
                    <br />
                    
                    <input type="text" class="f-left normal">
                    <a href="#" class="f-right / margin-top-5">+ Add Another Contact Number</a>
                    <div class="clear"></div>

                    <label class="margin-top-10">First Name: <span class="red-color">*</span></label>
                    <br />
                    <input type="text" class="xlarge">

                    <label class="margin-top-10">Middle Name: <span class="red-color">*</span></label>
                    <br />
                    <input type="text" class="xlarge">

                    <label class="margin-top-10">Last Name: <span class="red-color">*</span></label>
                    <br />
                    <input type="text" class="xlarge">
                </div>


                          
            </div>

                <button type="button" class="btn btn-dark f-right margin-right-20 close-me">Cancel</button>
                <button type="button" class="btn btn-dark f-right margin-right-20 close-me">Confirm</button>
                <div class="clear"></div>
        </div>
    </div>

<?php include "../construct/footer.php"; ?>