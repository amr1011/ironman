<?php include "../construct/header.php"; ?>

    <section class="container-fluid" section-style="top-panel">

        <!-- search order -->
        <div class="row header-container">
            <div class="contents">
                <h1 class="f-left">Rider Management</h1>
                <div class="f-right margin-top-20">
                    <button class="btn btn-dark margin-right-20">Rider Archives</button>
                    <button class="btn btn-dark margin-right-20 / modal-trigger" modal-target="add-riders">Add New Rider</button>
                    <button class="btn btn-dark">FAQ</button>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        
        <div class="row">
            <div class="contents margin-top-20">
                <!-- search -->
                <div class="f-left">
                    <label class="margin-bottom-5">search:</label><br>
                    <input class="search f-left xlarge" type="text">
                </div>
                <!-- search by -->
                <div class="f-left margin-left-20">
                    <label class="margin-bottom-5">search by:</label><br>
                    <div class="select xlarge">
                        <select>
                            <option value="Rider Name">Rider Name</option>                          
                        </select>
                    </div>
                </div>
                            
                <button class="f-left btn btn-dark margin-top-20 margin-left-20">Search</button>
                <div class="clear"></div>
            </div>
        </div>
        
        <div class="row margin-top-20">
            <div class="contents line">
                <div class="select xlarge">
                    <select>
                        <option value="All Riders">Show All Riders</option>                     
                    </select>
                </div>
                <span class="white-space"></span>
                
                
                <span class="white-space"></span>
                
                <div class="f-right bggray-white">
                    <p class="f-left font-12 padding-left-10 padding-top-5">
                        <strong>Sort By:</strong>
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">   
                        <strong>Rider Name</strong>  
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-top-5">
                        <a class="red-color active" href="">
                            <strong>Availability</strong>
                            <img src="../assets/images/ui/sort-top-arrow.png">
                        </a>
                    </p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </section>

    <section class="container-fluid" section-style="content-panel">
        <div class="row">
            <!-- sample-1-->

            <div class="content-container unboxed">
                <h3 class="f-left / no-margin-all / gray-color">Available Riders</h3>                
                <p class="f-right / font-14 / no-margin-bottom / gray-color"><strong>2 Riders</strong></p>
                <div class="clear"></div>
            </div>
            <div class="content-container opaque">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name:</span> Juan Manuel R. Garcia</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Contact Num: </span> </strong>(+63) 915-148-3691 <i class="fa fa-mobile"></i> Globe</p>                        
                    </div>
                    
                    <div class="width-35per f-left">
                       <p class="font-14 margin-bottom-5"><strong><span class="red-color">Location:</span></strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="gray-color">In Store</span></strong></p>
                    </div>
                    
                    <div class="width-25per f-left">
                       <p class="font-14 margin-bottom-5"><strong><span class="yellow-color">ON HOLD</span></strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Order On Hand: </span>3 Orders</strong></p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="content-container opaque">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name:</span> Edgardo A. Gutierez</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Contact Num: </span> </strong>(+63) 915-148-3691 <i class="fa fa-mobile"></i> Globe</p>                        
                    </div>
                    
                    <div class="width-35per f-left">
                       <p class="font-14 margin-bottom-5"><strong><span class="red-color">Location:</span></strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="gray-color">In Store</span></strong></p>
                    </div>
                    
                    <div class="width-25per f-left">
                       <p class="font-14 margin-bottom-5"><strong><span class="green-color">AVAILABLE</span></strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Order On Hand: </span><span class="gray-color">None</span></strong></p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>




            <!-- sample-2 -->

            
            <div class="content-container unboxed">
                <h3 class="f-left no-margin-all">Riders on Fields</h3>                
                <p class="f-right font-14 no-margin-bottom"><strong>3 Riders</strong></p>
                <div class="clear"></div>
            </div>
           <div class="content-container">
                <div>
                    <div class="f-left margin-bottom-20 ">
                        <p class="font-20"><strong>Benjie B. Ramos</strong></p>                                                            
                    </div>                  
                    <div class=" f-right">
                        <p class="font-16 "><strong><span class="red-color">RIDER OUT</span></strong></p>                                                                                    
                    </div>
                    <div class="clear"></div>
                    <hr>

                    <div class="data-container split">
                        
                        <!-- clients information -->
                        <div class="margin-top-20">
                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Contact Number: </strong></p>
                            <p class="f-right font-12"><strong>(+63) 915-148-3691 <i class="fa fa-mobile / margin-left-5"></i> Globe</strong></p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Orders On-hand </strong></p>
                            <p class="f-right font-12"><strong>3 Orders</strong></p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Time On Field:</strong></p>
                            <p class="f-right font-12"><strong>00:15:36</strong></p>
                            <div class="clear"></div>                       
                        </div>

                        <!-- order 1 -->
                        <div>
                            <label class="margin-top-10 margin-bottom-10 ">Order 1:</label>
                            <div class="bggray-light  font-14 small-curved-border">

                                <div class="display-inline-mid padding-all-10">
                                    <p class="font-12 margin-bottom-5"><span class="red-color"><strong>Order ID:</strong></span></p>
                                    <p class="font-12"><span class="red-color"><strong>Delivery Time:</strong></span></p>                                                                        
                                </div>

                                <div class="display-inline-mid margin-left-20 ">
                                    <a href="#" class="f-right">734699</a>
                                    <br />
                                    <p class="font-12 f-right">20 Minutes</p>                                    
                                    <div class="clear"></div>
                                </div>                          

                                <div class="display-inline-mid margin-left-30">
                                    <p class="font-12"><span class="red-color"><strong>Timer:</strong></span> <span class="font-20 margin-left-10">00:18:36</span></p>
                                </div>                          
                            </div>                          
                        </div>

                        
                        <!-- order 3 -->
                        <div>
                            <label class="margin-top-10 margin-bottom-10 ">Order 3:</label>
                            <div class="bggray-light padding-all-5 font-14 small-curved-border">

                               
                                <div class="display-inline-mid padding-all-10">
                                    <p class="font-12 margin-bottom-5"><span class="red-color"><strong>Order ID:</strong></span></p>
                                    <p class="font-12"><span class="red-color"><strong>Delivery Time:</strong></span></p>                                                                        
                                </div>

                                <div class="display-inline-mid margin-left-20 ">
                                    <a href="#" class="f-right">734699</a>
                                    <br />
                                    <p class="font-12 f-right">20 Minutes</p>                                    
                                    <div class="clear"></div>
                                </div>                          

                                <div class="display-inline-mid margin-left-30">
                                    <p class="font-12"><span class="red-color"><strong>Timer:</strong></span> <span class="font-20 margin-left-10">00:15:21</span></p>
                                </div>                         
                            </div>                          
                        </div>                        
                    </div>



                    <div class="data-container split margin-left-15">
                                                                                        
                         <!-- clients information -->
                        <div class="margin-top-20">
                            <div>
                                <p class=" red-color font-12 margin-bottom-5"><strong>Location: </strong></p>                                                        
                                <div class="clear"></div>
                            </div>
                            
                            <div>
                                <p class="font-12 / margin-bottom-5"><strong>2nd St, Brgy. Kapitolyo, Pasig City - NCR </strong></p>                                                        
                                <div class="clear"></div>
                            </div>
                            <br />
                            <p class="gray-color font-12 margin-bottom-5">12:48 PM</p>                            
                        </div>

                        <!-- order 2 -->
                        <div>
                            <label class="margin-top-10 margin-bottom-10 ">Order 2:</label>
                            <div class="bggray-light padding-all-5 font-14 small-curved-border">
                            
                                <div class="display-inline-mid padding-left-10 padding-top-5 padding-bottom-5">
                                    <p class="font-12 margin-bottom-5"><span class="red-color"><strong>Order ID:</strong></span></p>
                                    <p class="font-12"><span class="red-color"><strong>Delivery Time:</strong></span></p>                                                                        
                                </div>

                                <div class="display-inline-mid margin-left-20 ">
                                    <a href="#" class="f-right">734699</a>
                                    <br />
                                    <p class="font-12 f-right">15 Minutes</p>                                    
                                    <div class="clear"></div>
                                </div>                          

                                <div class="display-inline-mid margin-left-30">
                                    <p class="font-12"><span class="red-color"><strong>Timer:</strong></span> <span class="font-20 margin-left-10">00:09:36</span></p>
                                </div>                       
                            </div>                          
                        </div>  

                    </div>
                    <div class="margin-top-20">
                        <button type="button" class="btn btn-dark f-right margin-right-10">Locate Rider</button>                        
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class="content-container opaque">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name:</span> Miguel R. Perez</strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Contact Num: </span> (+63) 915-148-3691 <i class="fa fa-mobile"></i> Globe</strong></p>
                    </div>
                    
                    <div class="width-35per f-left">
                       <p class="font-14 margin-bottom-5"><strong><span class="red-color">Location:</span></strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="gray-color">-</span></strong></p>
                    </div>
                    
                    <div class="width-25per f-left">
                       <p class="font-14 margin-bottom-5"><strong><span class="red-color">RIDER OUT</span></strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Orders On Hand: </span>2 Orders</strong></p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
           
        </div>
    </section>

    <!-- modal ADD NEW RIDERS -->

   <div class="modal-container" modal-id="add-riders">
        <div class="modal-body small">
            <div class="modal-head">
                <h4 class="text-left padding-left-20">Add New Riders</h4>
                <div class="modal-close close-me"></div>
            </div>


            <div class="modal-content">
                <div class="margin-left-15 margin-right-15">
                    <label>Contact Number: <span class="red-color">*</span></label>
                    <br />
                    
                    <input type="text" class="f-left normal">
                    <a href="#" class="f-right / margin-top-5">+ Add Another Contact Number</a>
                    <div class="clear"></div>

                    <label class="margin-top-10">First Name: <span class="red-color">*</span></label>
                    <br />
                    <input type="text" class="xlarge">

                    <label class="margin-top-10">Middle Name: <span class="red-color">*</span></label>
                    <br />
                    <input type="text" class="xlarge">

                    <label class="margin-top-10">Last Name: <span class="red-color">*</span></label>
                    <br />
                    <input type="text" class="xlarge">
                </div>


                          
            </div>

                <button type="button" class="btn btn-dark f-right margin-right-20">Cancel</button>
                <button type="button" class="btn btn-dark f-right margin-right-20 close-me">Confirm</button>
                <div class="clear"></div>
        </div>
    </div>

<?php include "../construct/footer.php"; ?>