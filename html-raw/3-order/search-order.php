<?php include "../construct/header.php"; ?>

	<section class="container-fluid" section-style="top-panel">

        <!-- search order -->
		<div class="row header-container">
			<div class="contents">
				<h1 class="f-left">Search Order</h1>
                <h1 class="f-left">Customers List</h1>
				<div class="f-right">
					<button class="btn btn-dark margin-top-20">FAQ</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>

        
		<div class="row">
			<div class="contents margin-top-20">
				<div class="f-left">
					<label class="margin-bottom-5">search:</label><br>
					<input class="search f-left" type="text">
				</div>
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">search by:</label><br>
					<div class="select">
						<select>
							<option value="All Categories">Contact Number</option>
							<option value="Burgers">Address</option>
						</select>
					</div>
				</div>
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">Province:</label><br>
					<div class="select">
						<select>
							<option value="All Province">All Province</option>
							<option value="Abra">Abra</option>
							<option value="Batangas">Batangas</option>
							<option value="Bulacan">Bulacan</option>
							<option value="Bicol">Bicol</option>
							<option value="Cavite">Cavite</option>
							<option value="Bulacan">Laguna</option>
						</select>
					</div>
				</div>
				<button class="f-left btn btn-dark margin-top-20 margin-left-20">Search</button>
			</div>
		</div>
        
        <div class="row margin-top-20">
            <div class="contents line">
                <div class="select larger">
					<select>
						<option value="All Item Types">Show All Stores</option>
						<option value="1pc Chicken"></option>
					</select>
				</div>
				<span class="white-space"></span>
                
                <div class="select larger">
					<select>
						<option value="All Item Types">From All Call Center</option>
						<option value="1pc Chicken"></option>
					</select>
				</div>
				<span class="white-space"></span>
                
                <div class="f-right bggray-white">
                    <p class="f-left font-12 padding-left-10 padding-top-5">
                        <strong>Sort By:</strong>
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">   
                        <strong>Customer Name</strong>  
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">  
                        <strong>Store</strong>     
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-top-5">
                        <a class="red-color active" href="">
                            <strong>Criticality</strong>
                            <img src="../assets/images/ui/sort-top-arrow.png">
                        </a>
                    </p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
	</section>

	<section class="container-fluid" section-style="content-panel">
		<div class="row">
			<!-- sample-1-->
			<div class="content-container">
                <div class="">
                    <div class="width-40per f-left">
                        <p class="font-16 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-16 margin-bottom-5">MM Ortigas Roosevelt | JB0444</p>
                        <p><strong><span class="red-color">Delivery Time:</span></strong> 20 Minutes</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class="margin-left-40 margin-bottom-5"><strong><span class="red-color">Name:</span></strong> Jonathan R. Ornido</p>
                        <p class="margin-left-40 margin-bottom-5"><strong><span class="red-color">Contact Number:</span></strong> (+63) 915-516-6153 | Globe</p>
                        <p class="margin-left-40 margin-bottom-5"><strong><span class="red-color">Transaction Time:</span></strong> May 18, 2015 | 11:24:00</p>
                    </div>
                    
                    <div class="width-20per f-right">
                        <p class="font-16 margin-bottom-5 red-color"><strong>RIDER'S OUT (LATE)</strong></p>
                        <p><strong><span class="red-color">Elapsed Time:</span></strong> 00:25:36</p>
                    </div>
                    <div class="clear"></div>
                </div>
			</div>
            <!-- sample-2-->
			<div class="content-container viewable">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-16 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-16 margin-bottom-5">MM Ortigas Roosevelt | JB0444</p>
                        <p><strong><span class="red-color">Delivery Time:</span></strong> 20 Minutes</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class="margin-left-40 margin-bottom-5"><strong><span class="red-color">Name:</span></strong> Jonathan R. Ornido</p>
                        <p class="margin-left-40 margin-bottom-5"><strong><span class="red-color">Contact Number:</span></strong> (+63) 915-516-6153 | Globe</p>
                        <p class="margin-left-40 margin-bottom-5"><strong><span class="red-color">Transaction Time:</span></strong> May 18, 2015 | 11:24:00</p>
                    </div>
                    
                    <div class="width-20per f-right">
                        <p class="font-16 margin-bottom-5"><strong>RIDER'S OUT</strong></p>
                        <p><strong><span class="red-color">Elapsed Time:</span></strong> 00:25:36</p>
                    </div>
                    <div class="clear"></div>
                </div>
			</div>

             <!-- sample-3                   -->
            <div class="content-container viewable">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-16 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-16 margin-bottom-5">MM Ortigas Roosevelt | JB0444</p>
                        <p><strong><span class="red-color">Delivery Time:</span></strong> 20 Minutes</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class="margin-left-40 margin-bottom-5"><strong><span class="red-color">Name:</span></strong> Jonathan R. Ornido</p>
                        <p class="margin-left-40 margin-bottom-5"><strong><span class="red-color">Contact Number:</span></strong> (+63) 915-516-6153 | Globe</p>
                        <p class="margin-left-40 margin-bottom-5"><strong><span class="red-color">Transaction Time:</span></strong> May 18, 2015 | 11:24:00</p>
                    </div>
                    
                    <div class="width-20per f-right">
                        <p class="font-16 margin-bottom-5"><strong>RIDER'S OUT</strong></p>
                        <p><strong><span class="red-color">Elapsed Time:</span></strong> 00:25:36</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <!-- sample-4-->
			<div class="content-container viewable">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-16 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-16 margin-bottom-5">MM Ortigas Roosevelt | JB0444</p>
                        <p><strong><span class="red-color">Delivery Time:</span></strong> 20 Minutes</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class="margin-left-40 margin-bottom-5"><strong><span class="red-color">Name:</span></strong> Jonathan R. Ornido</p>
                        <p class="margin-left-40 margin-bottom-5"><strong><span class="red-color">Contact Number:</span></strong> (+63) 915-516-6153 | Globe</p>
                        <p class="margin-left-40 margin-bottom-5"><strong><span class="red-color">Transaction Time:</span></strong> May 18, 2015 | 11:24:00</p>
                    </div>
                    
                    <div class="width-20per f-right">
                        <p class="font-16 margin-bottom-5"><strong>ASSEMBLING ORDER</strong></p>
                        <p><strong><span class="red-color">Elapsed Time:</span></strong> 00:25:36</p>
                    </div>
                    <div class="clear"></div>
                </div>
			</div>
            <!-- sample-5-->
			<div class="content-container viewable">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-16 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-16 margin-bottom-5">MM Ortigas Roosevelt | JB0444</p>
                        <p><strong><span class="red-color">Delivery Time:</span></strong> 20 Minutes</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class="margin-left-40 margin-bottom-5"><strong><span class="red-color">Name:</span></strong> Jonathan R. Ornido</p>
                        <p class="margin-left-40 margin-bottom-5"><strong><span class="red-color">Contact Number:</span></strong> (+63) 915-516-6153 | Globe</p>
                        <p class="margin-left-40 margin-bottom-5"><strong><span class="red-color">Transaction Time:</span></strong> May 18, 2015 | 11:24:00</p>
                    </div>
                    
                    <div class="width-20per f-right">
                        <p class="font-16 margin-bottom-5"><strong>ASSIGNING RIDER</strong></p>
                        <p><strong><span class="red-color">Elapsed Time:</span></strong> 00:25:36</p>
                    </div>
                    <div class="clear"></div>
                </div>
			</div>
            <!-- sample-6-->
			<div class="content-container viewable">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-16 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-16 margin-bottom-5">MM Ortigas Roosevelt | JB0444</p>
                        <p><strong><span class="red-color">Delivery Time:</span></strong> 20 Minutes</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class="margin-left-40 margin-bottom-5"><strong><span class="red-color">Name:</span></strong> Jonathan R. Ornido</p>
                        <p class="margin-left-40 margin-bottom-5"><strong><span class="red-color">Contact Number:</span></strong> (+63) 915-516-6153 | Globe</p>
                        <p class="margin-left-40 margin-bottom-5"><strong><span class="red-color">Transaction Time:</span></strong> May 18, 2015 | 11:24:00</p>
                    </div>
                    
                    <div class="width-20per f-right">
                        <p class="font-16 margin-bottom-5 red-color"><strong>ORDER CANCELLED</strong></p>
                        <p><strong><span class="red-color">Elapsed Time:</span></strong> 00:25:36</p>
                    </div>
                    <div class="clear"></div>
                </div>
			</div>

            <!-- sample-7-->
            <div class="content-container viewable">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-16 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                        <p class="font-16 margin-bottom-5">MM Ortigas Roosevelt | JB0444</p>
                        <p><strong><span class="red-color">Delivery Time:</span></strong> 20 Minutes</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class="margin-left-40 margin-bottom-5"><strong><span class="red-color">Name:</span></strong> Jonathan R. Ornido</p>
                        <p class="margin-left-40 margin-bottom-5"><strong><span class="red-color">Contact Number:</span></strong> (+63) 915-516-6153 | Globe</p>
                        <p class="margin-left-40 margin-bottom-5"><strong><span class="red-color">Transaction Time:</span></strong> May 18, 2015 | 11:24:00</p>
                    </div>
                    
                    <div class="width-20per f-right">
                        <p class="font-16 margin-bottom-5 green-color"><strong>ORDER COMPLETE</strong></p>
                        <p><strong><span class="red-color">Elapsed Time:</span></strong> 00:25:36</p>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
		</div>
	</section>

<?php include "../construct/footer.php"; ?>