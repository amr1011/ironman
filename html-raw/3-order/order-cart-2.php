<?php include "../construct/header.php"; ?>

	<section class="container-fluid" section-style="top-panel">
		<div class="row header-container">
			<div class="contents">
				<h1 class="f-left">Order Cart</h1>
				<div class="f-right">
					<button class="btn btn-dark margin-top-20 margin-right-10">Back to Customer Search</button>
					<button class="btn btn-dark margin-top-20">FAQ</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</section>

	<section class="container-fluid" section-style="content-panel">
		<div class="centerer padding-top-20">
			<!-- left side -->
				<div class="display-inline-top width-60per">
					<div class="content-container unboxed">
						<div class="no-padding-hor">
							<label>search products:</label></br>
							<input type="text" class="xlarge"/>
							<button class="btn btn-dark margin-left-15">Search</button>
						</div>
						
						<hr/>

						<label class="margin-top-20">browse product by category:</label></br>

						<div class="category-box-container no-padding-hor">
							<div class="category-box">
								Noodles
							</div>
							<div class="category-box">
								Chicken
							</div>
							<div class="category-box">
								Burger
							</div>
							<div class="category-box">
								Deserts
							</div>
							<div class="category-box">
								Main Items
							</div>
							<div class="category-box">
								Breakfast
							</div>
							<div class="category-box">
								Kiddie Meals
							</div>
							<div class="category-box">
								Promos
							</div>
							<div class="category-box">
								39ers
							</div>
							<div class="category-box">
								Novelty Items
							</div>
						</div>

						<div class="category-item width-100per viewable small-curved-border">
							<div class="pop-up">
								<div class="display-inline-mid width-30per">
									<img class="imp-responsive" src="../assets/images/champ.png"/>
								</div>
								<div class="display-inline-mid width-70per">
									<p class="font-16">Aloha Champ</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel mi a elit hendrerit consectetur eget a neque. Etiam eleifend blandit quam.</p>
								</div>
							</div>
							<p class="f-left no-margin-bottom">Champ Amazing Aloha</p>
							<p class="f-right margin-left-15 no-margin-bottom">165.00 PHP</p>
							<p class="f-right no-margin-bottom">81</p>
							<div class="clear"></div>
						</div>

						<div class="category-item width-100per viewable small-curved-border">
							<div class="pop-up">
								<div class="display-inline-mid width-30per">
									<img class="imp-responsive" src="../assets/images/champ.png"/>
								</div>
								<div class="display-inline-mid width-70per">
									<p class="font-16">Aloha Champ</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel mi a elit hendrerit consectetur eget a neque. Etiam eleifend blandit quam.</p>
								</div>
							</div>
							<p class="f-left no-margin-bottom">Champ Amazing Aloha</p>
							<p class="f-right margin-left-15 no-margin-bottom">165.00 PHP</p>
							<p class="f-right no-margin-bottom">81</p>
							<div class="clear"></div>
						</div>

						<div class="content-container no-padding-hor">
							<div class="display-inline-bot width-60per padding-left-20 no-padding-right no-padding-ver">
								<p class="no-margin-bottom panel-title no-padding-bottom"><strong>Champ Amazing Aloha</strong></p>
								<p class="">165.00 PHP | B1</p>
							</div>
							<div class="display-inline-bot width-40per text-right padding-right-20 no-padding-left no-padding-ver">
								<label class="gray-color">Number of meals:</label>
								<div class="arrow-selector">
									<div class="plus no-padding-top"></div>
									<input type="text" class="xsmall"/>
									<div class="minus no-padding-top"></div>
								</div>
							</div>
							<hr class="margin-top-15"/>

							<div class="bggray-light">


								<div class="margin-left-25">
									<label class="margin-right-50">BUCKET TYPE</label>
									<label class="margin-left-50 margin-right-50 padding-left-35 m">Quantity:</label>
									<label class="gray-color margin-left-50 padding-left-10">remove:</label><br/>

								</div>								
								
								<div class="margin-left-25 ">
									<div class="f-left width-50per "> 
										<label class="margin-top-10" >8 PCS. ORIGINAL CHICKEN</label>
									</div>								
									<div class="f-left width-40per">
										
										<div class= "arrow-selector">										
											<div class="plus no-padding-top"></div>
											<input type="text" class="xsmall"/>
											<div class="minus no-padding-top"></div>									
										</div>
									</div>
									<button class="btn close-panel no-padding-all margin-left-15" disabled></button>
									
									<div class="clear"></div>
								</div>


								<div class="margin-left-25 ">
									<div class="f-left width-50per "> 
										<label class="margin-top-30" >8 PCS. HOT AND SPICY CHICKEN</label>
									</div>								
									<div class="f-left width-50per">
										<label>Quantity</label>
										<div class= "arrow-selector">										
											<div class="plus no-padding-top"></div>
											<input type="text" class="xsmall"/>
											<div class="minus no-padding-top"></div>									
										</div>
									</div>

									<div class="clear"></div>
								</div>	
								<div class="margin-left-25 		">
									<div class="f-left width-50per "> 
										<label class="margin-top-30" >8 PCS. GLAZE CHICKEN</label>
									</div>								
									<div class="f-left width-50per">
										<label>Quantity</label>
										<div class= "arrow-selector">										
											<div class="plus no-padding-top"></div>
											<input type="text" class="xsmall"/>
											<div class="minus no-padding-top"></div>									
										</div>
									</div>
									<div class="clear"></div>
								</div>

					
								<div class="display-inline-top width-45per margin-left-25">
									
								
									<br/>
									<label class="margin-top-15">drinks:</label></br>
									<div class="select">
										<select>
											<option value="coke">Coke</option>
											<option value="sarsi">Sarsi</option>
											<option value="coke zero">Coke Zero</option>
											<option value="sprite">Sprite</option>
											<option value="royal">Royal</option>
											<option value="coke max">Coke Max</option>
											<option value="orange juice">Eight-O-Clock</option>
											<option value="iced tea">Iced Tea</option>
										</select>
									</div>
									<br/>								
								</div>

								<div class="display-inline-top width-35per margin-left-10">
									

									<label class="margin-top-15">Drink Size:</label><br/>
									<div class="item-sizes">
										<input id="ds1" type="radio" name="drink-size-1" checked/>
										<label class="drinks size small" for="ds1"></label>
									</div>
									<div class="item-sizes">
										<input id="dm1" type="radio" name="drink-size-1"/>
										<label class="drinks size medium" for="dm1"></label>
									</div>
									<div class="item-sizes">
										<input id="dl1"type="radio" name="drink-size-1" />
										<label class="drinks size large" for="dl1"></label>
									</div>
									
								</div>

								

								<div class="margin-left-25">
									<div class="display-inline-top width-45per">
										<div class="display-inline-top margin-top-15">
											<label class="margin-top-15">add-ons:</label></br>
											<div class="select small">
												<select>
													<option value="no add-ons">No Add-Ons</option>
													<option value="sundae">Sundae</option>
													<option value="pie">pie</option>
												</select>
											</div>
										</div>
										<div class="display-inline-top margin-top-15">
											<label class="margin-top-15 gray-color">Quantity:</label></br>
											<input class="xsmall display-inline-top" type="text" disabled/>
										</div>
										<p class="text-right">
											<a href="#">+ Add another Add-ons</a>
										</p>
									</div>
									<div class="display-inline-top margin-left-20 width-45per">
										<div class="display-inline-top margin-top-15">
											<label class="margin-top-15">extra items:</label></br>
											<div class="select small">
												<select>
													<option value="no extra">No Extra Items</option>
													<option value="ketsup">Extra Ketsup</option>
													<option value="gravy">Extra Gravy</option>
												</select>
											</div>
										</div>
										<div class="display-inline-top margin-top-15">
											<label class="margin-top-15 gray-color">Quantity:</label></br>
											<input class="xsmall display-inline-top" type="text" disabled/>
										</div>
										<p class="text-right">
											<a href="#">+ Add another Extra Item</a>
										</p>
									</div>
								</div>
							</div>
							<hr/>
							<div class="bggray-light">
								<table class="width-100per">
									<tbody>
										<tr>
											<td class="text-left padding-all-5">
												<label class="gray-color">product</label>
											</td>
											<td class="text-center padding-all-5">
												<label class="gray-color">quantity</label>
											</td>
											<td class="text-right padding-all-5">
												<label class="gray-color">price</label>
											</td>
										</tr>
										<tr>
											<td class="text-left padding-all-5">
												<p>Chicken Order 1 <span class="gray-color">(Solo)</span></p>
											</td>
											<td class="text-center padding-all-5">
												<p>1</p>
											</td>
											<td class="text-right padding-all-5">
												<p>165.00 PHP</p>
											</td>
										</tr>
										<tr>
											<td class="text-left padding-all-5">
												<p>Large Nestea Iced Tea</p>
											</td>
											<td class="text-center padding-all-5">
												<p>1</p>
											</td>
											<td class="text-right padding-all-5">
												<p>52.20 PHP</p>
											</td>
										</tr>
										<tr>
											<td class="text-left padding-all-5">
												<p>Chicken Order 2</p>
											</td>
											<td class="text-center padding-all-5">
												<p>3</p>
											</td>
											<td class="text-right padding-all-5">
												<p>89.10 PHP</p>
											</td>
										</tr>
										<tr>
											<td class="padding-top-10 padding-bottom-10"colspan="3"><hr/></td>
										</tr>
										<tr>
											<td class="text-left padding-all-5">
												<a href="">Hide Order Breakdown</a>
											</td>
											<td class="text-center padding-all-5">
												<p class="red-color">Total Price:</p>
											</td>
											<td class="text-right padding-all-5">
												<p class="font-16"><strong>89.10 PHP</strong></p>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<hr/>

							<div class="notify-msg">
								<p class="f-left"><strong>Meal has been removed</strong></p>
								<a class="f-right" href="#">Undo</a>
								<div class="clear"></div>
							</div>

							<hr class="margin-bottom-20"/>

							<div class="f-left no-padding-ver">
								<label>total price</label><br/>
								<p class="font-20"><strong>203.00 PHP</strong></p>
							</div>
							<div class="f-right no-padding-ver">
								<button class="btn btn-dark">Add to Cart</button>
								<button class="margin-left-15 btn btn-dark">Cancel</button>
							</div>
							<div class="clear"></div>
						</div>

						<div class="category-item width-100per viewable small-curved-border">
							<div class="pop-up">
								<div class="display-inline-mid width-30per">
									<img class="imp-responsive" src="../assets/images/champ.png"/>
								</div>
								<div class="display-inline-mid width-70per">
									<p class="font-16">Aloha Champ</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel mi a elit hendrerit consectetur eget a neque. Etiam eleifend blandit quam.</p>
								</div>
							</div>	
							<p class="f-left no-margin-bottom">Champ Amazing Aloha</p>
							<p class="f-right margin-left-15 no-margin-bottom">165.00 PHP</p>
							<p class="f-right no-margin-bottom">81</p>
							<div class="clear"></div>
						</div>
						<div class="category-item width-100per viewable small-curved-border">
							<div class="pop-up">
								<div class="display-inline-mid width-30per">
									<img class="imp-responsive" src="../assets/images/champ.png"/>
								</div>
								<div class="display-inline-mid width-70per">
									<p class="font-16">Aloha Champ</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel mi a elit hendrerit consectetur eget a neque. Etiam eleifend blandit quam.</p>
								</div>
							</div>
							<p class="f-left no-margin-bottom">Champ Amazing Aloha</p>
							<p class="f-right margin-left-15 no-margin-bottom">165.00 PHP</p>
							<p class="f-right no-margin-bottom">81</p>
							<div class="clear"></div>
						</div>

						<div class="category-item width-100per viewable small-curved-border">
							<div class="pop-up">
								<div class="display-inline-mid width-30per">
									<img class="imp-responsive" src="../assets/images/champ.png"/>
								</div>
								<div class="display-inline-mid width-70per">
									<p class="font-16">Aloha Champ</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque vel mi a elit hendrerit consectetur eget a neque. Etiam eleifend blandit quam.</p>
								</div>
							</div>
							<p class="f-left no-margin-bottom">Champ Amazing Aloha</p>
							<p class="f-right margin-left-15 no-margin-bottom">165.00 PHP</p>
							<p class="f-right no-margin-bottom">81</p>
							<div class="clear"></div>
						</div>
					</div>

				</div>
			<!-- end of left side -->

			<!-- right side -->
				<div class="display-inline-top width-40per margin-left-15">
					<div class="content-container bggray-light unboxed small-curved-border">
						<div>
							<table  class="width-100per">
								<tbody>
									<tr>
										<td class="light-red-color padding-all-5">Order ID:</td>
										<td class="text-right padding-all-5">733158</td>
									</tr>
									<tr>
										<td class="light-red-color padding-all-5">Name:</td>
										<td class="text-right padding-all-5">Mark Anthony D. Dulay</td>
									</tr>
									<tr>
										<td class="light-red-color padding-all-5">Contact Num:</td>
										<td class="text-right padding-all-5">(+63) 940-848-1458 <i class="fa fa-mobile"></i> TNT</td>
									</tr>
									<tr>
										<td class="light-red-color padding-all-5">Alternate Num:</td>
										<td class="text-right padding-all-5">(+63) 910-576-1248 <i class="fa fa-mobile"></i> SMART</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="content-container">
						<div>
							<table class="width-100per">
								<tbody>
									<tr>
										<td class="padding-all-5"><p class="panel-title no-padding-bottom f-left"><strong>Store Information</strong></p></td>
										<td class="text-right padding-all-5"><a class="f-right" href="#"><i class="fa fa-map-marker"></i> Show Map</a></td>
									</tr>
									<tr>
										<td class="padding-all-5"><strong>MM Ortigas Roosevelt</strong></td>
										<td class="text-right padding-all-5"><strong>JB0044</strong></td>
									</tr>
									<tr>
										<td class="padding-all-5 light-red-color"><strong>Delivey Time:</strong></td>
										<td class="text-right padding-all-5">40 Minutes</td>
									</tr>
								</tbody>
							</table>

							<div class="notify-msg margin-top-10">
								<strong>Store Announcement!!</strong> Delivery will be delayed for 10 minutes due to bad weather. Thank you.
							</div>
							
							<div class="warn-msg margin-top-15">
								<p class="f-left"><strong>Unvailable Products</strong></p>
								<p class="f-right"><a href="#">Show</a></p>
								<div class="clear"></div>
							</div>
						</div>
					</div>

					<div class="content-container">
						<div>
							<p class="panel-title no-padding-bottom margin-bottom-10"><strong>Shopping Cart</strong></p>
							<p class="red-color f-left margin-right-10"><strong>Discount:</strong></p>
							<div class="form-group f-left">
								<input id="shopping-cart-none" type="radio" class="radio-box" name="shopping-cart" checked/>
								<label class="radio-lbl" for="shopping-cart-none">None</label>
								<input id="shopping-cart-pwd" type="radio" class="radio-box" name="shopping-cart"/>
								<label class="radio-lbl" for="shopping-cart-pwd">PWD</label>
								<input id="shopping-cart-senior" type="radio" class="radio-box" name="shopping-cart"/>
								<label class="radio-lbl" for="shopping-cart-senior">Senior Citizen</label>
							</div>
							<div class="clear"></div>
						</div>

						<div class="no-padding-ver">
							<!-- if shopping cart has no items -->
							<div class="text-center">
								<img class="img-responsive display-inline-mid width-60per opaque" src="../assets/images/jollibe-face.png"/>
								<p class="font-16 margin-top-10 gray-color">Please Select Meals to Order<p>
								<hr class="margin-top-15 margin-bottom-15"/>
							</div>
						</div>

						<!-- show/hide last order button and selector -->
						<div  class="no-padding-ver">
							<div class="arrow-selector margin-top-15 margin-bottom-15 text-center">
								<button class="btn btn-dark margin-bottom-15">Hide Last Order</button><br/>
								<div class="arrow-left no-padding-all"></div>
								<div class="select">
									<select>
										<option value="April 27, 2015 | 4:20 PM">April 27, 2015 | 4:20 PM</option>
										<option value="April 28, 2015 | 4:20 PM">April 28, 2015 | 4:20 PM</option>
									</select>
								</div>
								<div class="arrow-right no-padding-all"></div>
							</div>
						</div>

						<!-- if shopping cart has items -->
						<div class="no-padding-all">
							<table class="font-12 width-100per">
								<thead class="bggray-dark">
									<tr>
										<th class="padding-all-10">Qty</th>
										<th class="padding-all-10">Product</th>
										<th class="padding-all-10">Price</th>
										<th class="padding-all-10" colspan="2">Subtotal</th>
									</tr>
								</thead>
								<tbody class="bggray-light">
									<tr>
										<td class="padding-all-10 text-center">2</td>
										<td class="padding-all-10"><div class="arrow-down"></div> Champ Amazing aloha</td>
										<td class="padding-all-10 text-right">203.50</td>
										<td class="padding-all-10 text-right">407.00</td>
										<td class="padding-all-5"><a class="red-color" href="#"><i class="fa fa-times-circle"></i></a></td>
									</tr>
									<tr>
										<td class="padding-all-10 text-center gray-color">2</td>
										<td class="padding-all-10 padding-left-35">Champ Amazing aloha</td>
										<td class="padding-all-10 text-right">17.60</td>
										<td class="padding-all-10 text-right">35.20</td>
										<td></td>
									</tr>
									<tr>
										<td class="padding-all-10 text-center gray-color">2</td>
										<td class="padding-all-10 padding-left-35">Regular French Fries</td>
										<td class="padding-all-10 text-right">0.00</td>
										<td class="padding-all-10 text-right">0.00</td>
										<td></td>
									</tr>
									<tr>
										<td class="padding-all-10" colspan="5"><hr></td>
									</tr>
									<tr>
										<td class="padding-all-10 text-center">2</td>
										<td class="padding-all-10"><div class="arrow-down"></div> Champ Amazing aloha</td>
										<td class="padding-all-10 text-right">203.50</td>
										<td class="padding-all-10 text-right">407.00</td>
										<td  class="padding-all-5"><a class="red-color" href="#"><i class="fa fa-times-circle"></i></a></td>
									</tr>
									<tr>
										<td class="padding-all-10 text-center gray-color">2</td>
										<td class="padding-all-10 padding-left-35">Champ Amazing aloha</td>
										<td class="padding-all-10 text-right">17.60</td>
										<td class="padding-all-10 text-right">35.20</td>
										<td></td>
									</tr>
									<tr>
										<td class="padding-all-10 text-center gray-color">2</td>
										<td class="padding-all-10 padding-left-35">Regular French Fries</td>
										<td class="padding-all-10 text-right">0.00</td>
										<td class="padding-all-10 text-right">0.00</td>
										<td></td>
									</tr>
									<tr class="bggray-middark">
										<td colspan="2" class="padding-all-10 text-left">Total Cost</td>
										<td colspan="3" class="padding-all-10 text-right">591.80 PHP</td>									
									</tr>
									<tr class="bggray-middark">
										<td colspan="2" class="padding-all-10 text-left">Added VAT</td>
										<td colspan="3" class="padding-all-10 text-right">71.01 PHP</td>
									</tr>
									<tr class="bggray-dark">
										<td colspan="2" class="padding-all-10 text-left font-16"><strong>Total Bill</strong></td>
										<td colspan="3" class="padding-all-10 text-right font-14"><strong>662.81 PHP</strong></td>
									</tr>
								</tbody>
							</table>
						</div>

						<div class="text-center">
							<label class="text-left width-100per">order remarks</label>
							<textarea row="5"></textarea>

							<button class=" margin-top-15 btn btn-dark">Show Bill Breakdown</button>
							<hr class="margin-top-15 margin-bottom-15"/>
						</div>
					</div>

					<div class="content-container">
						<div>
							<div>
								<p class="panel-title no-padding-bottom margin-bottom-10"><strong>Order Details</strong></p>
								<p class="red-color f-left margin-right-10"><strong>Order mode:</strong></p>
								<div class="form-group f-right">
									<input id="order-details-delivery" type="radio" class="radio-box" name="order-details" checked/>
									<label class="radio-lbl" for="order-details-delivery">Delivery</label>
									<input id="order-details-pick-up" type="radio" class="radio-box" name="order-details"/>
									<label class="radio-lbl" for="order-details-pick-up">For Pick-up</label>
								</div>
								<div class="clear"></div>

								<label>delivery address:</label>
								<div class="bggray-light padding-all-10 font-14 small-curved-border">
									<div class="display-inline-mid margin-right-10 padding-left-20 width-20per">
										<img src="../assets/images/work-icon.png" alt="work icon">
										<p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
									</div>
									<div class="display-inline-mid padding-left-10 width-80per">
										<p class="no-margin-all"><strong>Cr8v Web Solutions, Inc.</strong> <br/>66C &amp; 66D, San Rafael St. Brgy. Kapitolyo, Pasig City. - NCR</p>
										<p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
									</div>
								</div>

								<label class="margin-top-20">delivery address:</label>
								<div class="select width-100per">
									<select>
										<option value="cash">Cash</option>
										<option value="credit">Credit</option>
										<option value="cheque">Cheque</option>
										<option value="happy-plus">Happy Plus</option>
									</select>
								</div>

								<label class="margin-top-20">change for</label>
								<div class="price full">
									<input type="text"/>
								</div>
								<a class="f-right font-12" href="#">+ Add Another Payment Type</a>

								<label class="margin-top-20">pricing differentiator:</label>
								<div class="select width-100per">
									<select>
										<option value="pd-01">10% Delivery Charge with VAT</option>
										<option value="pd-02">10% Delivery Charge without VAT</option>
										<option value="pd-03">15% Senior Citizen Discount</option>
									v</select>
								</div>
								
								<p class="f-left margin-top-20 light-red-color">Has Special Instruction?</p>
								<div class="form-group f-right margin-top-20 no-margin-bottom">
									<input id="order-details-delivery" type="radio" class="radio-box" name="has-special-instruction" checked/>
									<label class="radio-lbl" for="order-details-delivery">Yes</label>
									<input id="order-details-pick-up" type="radio" class="radio-box" name="has-special-instruction"/>
									<label class="radio-lbl" for="order-details-pick-up">No</label>
								</div>
								<div class="clear"></div>

								<div class="text-center">
									<button class=" margin-top-15 width-50per btn btn-dark modal-trigger" modal-target="manual-order-summary">Send Order</bu

...