<?php include "../construct/header.php"; ?>
	<section class="container-fluid" section-style="top-panel">

        <!-- search order -->
		<div class="row header-container">
			<div class="contents">
				<h1 class="f-left">Customer List</h1>
				<div class="f-right margin-top-20">
					<button class="btn btn-dark margin-right-10">Add New Customer</button>
					<button class="btn btn-dark margin-right-10">Download Customer List</button>
					<button class="btn btn-dark ">FAQ</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>

        
		<div class="row">
			<div class="contents margin-top-20">
				<!-- search -->
				<div class="f-left">
					<label class="margin-bottom-5">search:</label><br>
					<input class="small f-left" type="text">
				</div>
				<!-- search by -->
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">search by:</label><br>
					<div class="select">
						<select>
							<option value="All Categories">Contact Number</option>
							<option value="Burgers">Address</option>
						</select>
					</div>
				</div>
				<!-- province -->
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">Province:</label><br>
					<div class="select">
						<select>
							<option value="All Province">All Province</option>
							<option value="Abra">Abra</option>
							<option value="Batangas">Batangas</option>
							<option value="Bulacan">Bulacan</option>
							<option value="Bicol">Bicol</option>
							<option value="Cavite">Cavite</option>
							<option value="Bulacan">Laguna</option>
						</select>
					</div>
				</div>

				<!-- last ordered -->
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">Last Ordered:</label><br>
					<div class="select">
						<select>
							<option value="All Province">Today</option>
						</select>
					</div>
				</div>


				<!-- button -->
				<button class="f-left btn btn-dark margin-top-20 margin-left-20">Search</button>
				<div class="clear"></div>
			</div>
		</div>
        
        <div class="row margin-top-20">
            <div class="contents line">
                <div class="select larger">
					<select>
						<option value="All Item Types">Show All Stores</option>
						<option value="1pc Chicken"></option>
					</select>
				</div>
				<span class="white-space"></span>
                
              
				<span class="white-space"></span>
                
                <div class="f-right bggray-white">
                    <p class="f-left font-12 padding-left-10 padding-top-5">
                        <strong>Sort By:</strong>
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">   
                        <strong>Customer Name</strong>  
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">  
                        <strong>Behavior</strong>     
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-top-5">
                        <a class="red-color active" href="">
                            <strong>Recent Ordered</strong>
                            <img src="../assets/images/ui/sort-top-arrow.png">
                        </a>
                    </p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
	</section>

	<section class="container-fluid" section-style="content-panel">
		<div class="row">
			<!-- sample-1-->
			<div class="content-container viewable">
                <div class="">
                    <div class="width-40per f-left">
                        <p class="font-16 margin-bottom-5"><strong>Mark Anthony D. Dulay</strong></p>                        
                        <p><strong><span class="red-color">Contact Num: </span></strong>(+63) 915-516-6153 <i class="fa fa-mobile margin-right-5"></i>Globe</p>
                    </div>
                    
                    <div class="f-left">
                        <p class=" margin-bottom-5"><strong><span class="red-color">THIS CUSTOMER IS DIFFICULT TO HANDLE</span></strong></p>
                        <p class=" margin-bottom-5"><strong><span class="red-color">Address: </span></strong> 98 San Rafael Street, Brgy. Kapitolyo, Pasig City - NCR</p>                        
                    </div>                 
                    <div class="clear"></div>
                </div>
			</div>
            
			<!-- sample-2-->
			<div class="content-container viewable">
                <div class="">
                    <div class="width-40per f-left">
                        <p class="font-16 margin-bottom-5"><strong>Audrey D. Hepburn</strong></p>                        
                        <p><strong><span class="red-color">Contact Num: </span></strong>(02) 257-68-95 <i class="fa fa-phone margin-right-5"></i>PLDT</p>
                    </div>
                    
                    <div class="width-40per f-left">
                        <p class=" margin-bottom-5"><strong><span class="green-color">THIS CUSTOMER IS EASY TO HANLDE</span></strong></p>
                        <p class=" margin-bottom-5"><strong><span class="red-color">Address: </span></strong> 66D 2nd Floor, 591 Cr8v Bldg, San Rafael St. Brgy. Kapitolyo, Pasig City - NCR</p>                        
                    </div>                 
                    <div class="clear"></div>
                </div>
			</div>

			<!-- sample-3-->
			<div class="content-container viewable">
                <div class="">
                    <div class="width-40per f-left">
                        <p class="font-16 margin-bottom-5"><strong>Norly N. Tabora</strong></p>                        
                        <p><strong><span class="red-color">Contact Num: </span></strong>(02) 910-487-1475 <i class="fa fa-mobile margin-right-5"></i>Smart</p>
                    </div>
                    
                    <div class="f-left">
                        <p class=" margin-bottom-5"><strong><span class="green-color">THIS CUSTOMER IS EASY TO HANDLE</span></strong></p>
                        <p class=" margin-bottom-5"><strong><span class="red-color">Address: </span></strong> 98 San Rafael Street, Brgy. Kapitolyo, Pasig City - NCR</p>                        
                    </div>                 
                    <div class="clear"></div>
                </div>
			</div>

			<!-- sample-4-->
			<div class="content-container viewable">
                <div class="">
                    <div class="width-40per f-left">
                        <p class="font-16 margin-bottom-5"><strong>Rachelle P. Batto</strong></p>                        
                        <p><strong><span class="red-color">Contact Num: </span></strong>(+63) 939-259-1458 <i class="fa fa-mobile margin-right-5"></i>TNT</p>
                    </div>
                    
                    <div class="f-left">
                        <p class=" margin-bottom-5"><strong><span class="green-color">THIS CUSTOMER IS EASY TO HANDLE</span></strong></p>
                        <p class=" margin-bottom-5"><strong><span class="red-color">Address: </span></strong> 23 San Joaquin Street, Brgy. Kapitolyo, Pasig City - NCR</p>                        
                    </div>                 
                    <div class="clear"></div>
                </div>
			</div>
			<!-- sample-5-->
			<div class="content-container viewable">
                <div class="">
                    <div class="width-40per f-left">
                        <p class="font-16 margin-bottom-5"><strong>Mae D. Flores</strong></p>                        
                        <p><strong><span class="red-color">Contact Num: </span></strong>(+63) 915-389-1473 <i class="fa fa-mobile margin-right-5"></i>Globe</p>
                    </div>
                    
                    <div class="f-left">
                        <p class=" margin-bottom-5"><strong><span class="red-color">THIS CUSTOMER IS DIFFICULT TO HANDLE</span></strong></p>
                        <p class=" margin-bottom-5"><strong><span class="red-color">Address: </span></strong> 918 Corazon Street, Brgy. Valerino Pasig City - NCR</p>                        
                    </div>                 
                    <div class="clear"></div>
                </div>
			</div>
			<!-- sample-6-->
			<div class="content-container viewable">
                <div class="">
                    <div class="width-40per f-left">
                        <p class="font-16 margin-bottom-5"><strong>Jenny Marie R. Flores</strong></p>                        
                        <p><strong><span class="red-color">Contact Num: </span></strong>(+63) 915-516-6153 <i class="fa fa-mobile margin-right-5"></i>Globe</p>
                    </div>
                    
                    <div class="f-left">
                        <p class=" margin-bottom-5"><strong><span class="green-color">THIS CUSTOMER IS EASY TO HANDLE</span></strong></p>
                        <p class=" margin-bottom-5"><strong><span class="red-color">Address: </span></strong> 98 San Rafael Street, Brgy. Kapitolyo Pasig City - NCR</p>                        
                    </div>                 
                    <div class="clear"></div>
                </div>
			</div>
			<!-- sample-7-->
			<div class="content-container viewable">
                <div class="">
                    <div class="width-40per f-left">
                        <p class="font-16 margin-bottom-5"><strong>Meggy Stephanie F. Tinio</strong></p>                        
                        <p><strong><span class="red-color">Contact Num: </span></strong>(+63) 939-259-1458 <i class="fa fa-mobile margin-right-5"></i>TNT</p>
                    </div>
                    
                    <div class="f-left">
                        <p class=" margin-bottom-5"><strong><span class="red-color">THIS CUSTOMER IS DIFFICULT TO HANDLE</span></strong></p>
                        <p class=" margin-bottom-5"><strong><span class="red-color">Address: </span></strong> 23 San Joaquin Street, Brgy. Kapitolyo, Pasig City - NCR</p>                        
                    </div>                 
                    <div class="clear"></div>
                </div>
			</div>
		</div>
	</section>


<?php include "../construct/footer.php"; ?>