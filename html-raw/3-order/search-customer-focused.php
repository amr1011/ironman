<body>
	<?php include "../construct/header.php"; ?>

	<section class="container-fluid" section-style="top-panel">
		<div class="row header-container">
			<div class="contents">
				<h1 class="f-left">Search Customer</h1>
				<div class="f-right">
					<button class="btn btn-dark margin-top-20 margin-right-10">Add New Customer</button>
					<button class="btn btn-dark margin-top-20 margin-right-10">Skip to Cart</button>
					<button class="btn btn-dark margin-top-20">FAQ</button>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div class="row">
			<div class="contents margin-top-20">
				<div class="f-left">
					<label class="margin-bottom-5">search:</label><br>
					<input class="search" type="text">

					<div class="result-list">
						<div class="list">Lorem ipsum dolor sit amet, consectetur</div>
						<div class="list">Lorem ipsum dolor sit amet, consectetur</div>
						<div class="list">Lorem ipsum dolor sit amet, consectetur</div>
						<div class="list">Lorem ipsum dolor sit amet, consectetur</div>
						<div class="list">Lorem ipsum dolor sit amet, consectetur</div>
						<div class="list">Lorem ipsum dolor sit amet, consectetur</div>
						<div class="list">Lorem ipsum dolor sit amet, consectetur</div>
						<div class="list">Lorem ipsum dolor sit amet, consectetur</div>
						<div class="list">Lorem ipsum dolor sit amet, consectetur</div>
					</div>
				</div>
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">search by:</label><br>
					<div class="select">
						<select>
							<option value="All Categories">Contact Number</option>
							<option value="Burgers">Address</option>
						</select>
					</div>
				</div>
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">Province:</label><br>
					<div class="select">
						<select>
							<option value="All Province">All Province</option>
							<option value="Abra">Abra</option>
							<option value="Batangas">Batangas</option>
							<option value="Bulacan">Bulacan</option>
							<option value="Bicol">Bicol</option>
							<option value="Cavite">Cavite</option>
							<option value="Bulacan">Laguna</option>
						</select>
					</div>
				</div>
				<button class="f-left btn btn-dark margin-top-20 margin-left-20">Search</button>
			</div>
		</div>

		<div class="row">
			<div class="contents margin-top-20 line">
				<p class="f-right margin-top-10 bggray-white font-14"><strong>Search Results | 2 Customers</strong></p>
				<span class="f-right white-space"></span>
				<div class="clear"></div>
			</div>
		</div>
	</section>

	<section class="container-fluid" section-style="content-panel">
		<div class="row">
			
			<!-- sample-1 -->
			<div class="content-container opaque">
				<div>
					<div class="width-40per f-left">
						<p class="font-16"><strong>Mark Anthony D. Dulay</strong></p>
						<p class="font-14 margin-top-10"><span class="red-color"><strong>Contact Num:</strong></span>(+63) 910-146-4178 <i class="fa fa-mobile"></i> Globe</p>
					</div>
					<div class="width-60per f-right">
						<p class="font-14 no-margin-bottom"><strong><span class="red-color">THIS CUSTOMER IS DIFFICULT TO HANDLE</span></strong></p>
						<p class="font-14 margin-top-10"><span class="red-color"><strong>Address:</strong></span> 168 San Ramon St., Brgy. San Pedro, Tarlac City - Tarlac</p>    
					</div>
					<div class="clear"></div>
				</div>
			</div>
			<!-- sample end-1 -->

			<!-- sample-1 -->
			<div class="content-container">
				<div>
					<p class="font-16 f-left"><strong>Mark Anthony D. Dulay</strong></p>
					<p class="font-14 no-margin-bottom f-right"><strong><span class="green-color">THIS CUSTOMER IS EASY TO HANDLE</span></strong></p>
					<div class="clear"></div>
					<hr/>

					<div class="data-container split">
						<div class="margin-top-15">
							<label>Contact Number</label><br/>
							<div class="select large">
								<select>
									<option>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</option>
									<option>(+63) 910-516-6153 <i class="fa fa-mobile"></i> Smart</option>
								</select>
							</div>
						</div>
						<div class="margin-top-15">
							<label>alternate Number</label><br/>
							<div class="select large">
								<select>
									<option>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</option>
									<option>(+63) 910-516-6153 <i class="fa fa-mobile"></i> Smart</option>
								</select>
							</div>
						</div>

						<div class="margin-top-15">
							<p class="f-left red-color font-14"><strong>Email Address:</strong> </p>
							<p class="f-right font-14">markdulay@gmail.com</p>
							<div class="clear"></div>
						</div>

						<label class="margin-top-15">CARDS:</label>
						 <div class="bggray-light padding-all-5 font-14 small-curved-border">
							<div class="display-inline-mid margin-right-10">
								<img class="thumb" src="../assets/images/happy-plus.jpg" alt="happy-plus"/>
							</div>
							<div class="display-inline-mid margin-left-10 divider padding-left-10">
								<p class="no-margin-all">0083-123456-46578<br/>
								<span class="red-color"><strong>Exp. Date:</strong></span> September 20, 2016</p>
							</div>
							<div class="display-inline-mid text-center margin-left-10">
								<a href="#">Show Card<br/>History</a>
							</div>
						</div>

						<label class="margin-top-15">DELIVERY ADDRESS:</label>
						 <div class="bggray-light padding-all-5 font-14 small-curved-border">
							<div class="display-inline-mid margin-right-10 padding-left-20">
								<img src="../assets/images/work-icon.png" alt="work icon"/>
								<p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
							</div>
							<div class="display-inline-mid margin-left-10  padding-left-10">
								<p class="no-margin-all"><strong>Cr8v Web Solutions, Inc.</strong></p>
								<p class="no-margin-all">66C &amp; 66D, San Rafael St.<br/>Brgy. Kapitolyo, Pasig City. - NCR</p>
								<p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
							</div>
							<div class="display-inline-mid text-center margin-left-25">
								<a class="red-color" href="#">Change<br/>Address<br/><div class="arrow-down"></div></a>
							</div>
						</div>
						<div class="bggray-light padding-all-5 font-14 small-curved-border">
							<div class="display-inline-mid margin-right-10 padding-left-20">
								<img src="../assets/images/work-icon.png" alt="work icon"/>
								<p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
							</div>
							<div class="display-inline-mid margin-left-10  padding-left-10">
								<p class="no-margin-all"><strong>Mom's House</strong></p>
								<p class="no-margin-all">258 Flores Bldg, San Isidor St, Brgy <br />Kapitolyo, Pasig City - NCR</p>
								<p class="no-margin-all gray-color">- Beside gas station</p>
							</div>
							<div class="display-inline-mid text-center margin-left-10">
								<a class="red-color" href="#">Cancel<div class="arrow-up"></div></a>
							</div>
						</div>
						<div class=" padding-all-5 font-14 small-curved-border">
							<div class="display-inline-mid margin-right-10 padding-left-20">
								<img src="../assets/images/Home2.svg" alt="work icon" class="small-thumb"/>
								<p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
							</div>
							<div class="display-inline-mid margin-left-10  padding-left-10">
								<p class="no-margin-all"><strong>Mom's House</strong></p>
								<p class="no-margin-all">258 Flores Bldg, San Isidor St, Brgy <br />Kapitolyo, Pasig City - NCR</p>
								<p class="no-margin-all gray-color">- Beside gas station</p>
							</div>
							<div class="display-inline-mid text-center margin-left-10">
								<a class="red-color" href="#">Change<br/>Address<br/><div class="arrow-down"></div></a>
							</div>
						</div>
						<hr />
						<div class="margin-top-10 margin-left-20 ">
							<p class="font-12"><i class="fa fa-plus-circle fa-3x margin-right-10  yellow-color f-left"></i></p>
							<p class="font-12 f-left margin-top-10 margin-left-10 modal-trigger" modal-target="deliver-to-different"><strong>Deliver to a Different Address</strong></p>
							<div class="clear"></div>
						</div>					
						
						
						
						<label class="margin-top-15">NEAREST STORE:</label>
						<div class="bggray-light padding-all-10 font-14 text-center small-curved-border">
							<button class="btn btn-light ">Find Retail Trade Area</button>
							<button class="btn btn-light margin-left-20">Show Map</button>
						</div>
					</div>



					<div class="data-container split margin-left-15">
						<label class="margin-top-15">order history:</label>
						 <div class="arrow-selector">
							<div class="arrow-left"></div>
							<div class="select">
								<select>
									<option>April 27, 2015 | 4:20 PM</option>
									<option>April 28, 2015 | 4:20 PM</option>
								</select>
							</div>
							<div class="arrow-right"></div>
						</div>

						<div class="margin-top-15 font-12">
							<p class="red-color f-left margin-bottom-10"><strong>Last Payment Method Used:</strong></p>
							<p class="f-right"><strong>Credit Card</strong></p>
							<div class="clear"></div>
						</div>

						<div class="small-curved-border">
							<table class="font-14">
								<thead class="bggray-dark">
									<tr>
										<th class="padding-all-10">Quantitiy</th>
										<th class="padding-all-10">Product</th>
										<th class="padding-all-10">Price</th>
										<th class="padding-all-10">Subtotal</th>
									</tr>
								</thead>
								<tbody class="bggray-light">
									<tr>
										<td class="padding-all-10 text-center">2</td>
										<td class="padding-all-10"><div class="arrow-down"></div> Champ Amazing aloha</td>
										<td class="padding-all-10">203.50</td>
										<td class="padding-all-10">407.00</td>
									</tr>
									<tr>
										<td class="padding-all-10 text-center gray-color">2</td>
										<td class="padding-all-10 padding-left-35">Champ Amazing aloha</td>
										<td class="padding-all-10">17.60</td>
										<td class="padding-all-10">35.20</td>
									</tr>
									<tr>
										<td class="padding-all-10 text-center gray-color">2</td>
										<td class="padding-all-10 padding-left-35">Regular French Fries</td>
										<td class="padding-all-10">0.00</td>
										<td class="padding-all-10">0.00</td>
									</tr>
									<tr>
										<td class="padding-all-10" colspan="4"><hr/></td>
									</tr>
									<tr>
										<td class="padding-all-10 text-center">2</td>
										<td class="padding-all-10">Sundae Strawberry</td>
										<td class="padding-all-10">30.80</td>
										<td class="padding-all-10">61.60</td>
									</tr>
									<tr>
										<td class="padding-all-10" colspan="4"><hr/></td>
									</tr>
									<tr>
										<td class="padding-all-10 text-center">2</td>
										<td class="padding-all-10">Yum with Cheese</td>
										<td class="padding-all-10">44.00</td>
										<td class="padding-all-10">88.60</td>
									</tr>
									<tr class="bggray-middark">
										<td colspan="2" class="padding-all-10 text-left">Total Cost</td>
										<td colspan="2" class="padding-all-10 text-right">591.80 PHP</td>
									</tr>
									<tr class="bggray-middark">
										<td colspan="2" class="padding-all-10 text-left">Added VAT</td>
										<td colspan="2" class="padding-all-10 text-right">71.01 PHP</td>
									</tr>
									<tr class="bggray-dark">
										<td colspan="2" class="padding-all-10 text-left font-16"><strong>Total Bill</strong></td>
										<td colspan="2" class="padding-all-10 text-right font-14"><strong>662.81 PHP</strong></td>
									</tr>
								</tbody>
							</table>
						</div>

						<label class="margin-top-15">other brand order history</label>
						<div class="bggray-light">
							<table class="font-14 width-100per">
								<tbody>
									<tr>
										<td class="padding-all-10"><img class="small-thumb" src="../assets/images/affliates/bk-logo.png" alt="burger king"></td>
										<td class="padding-all-10">Burger King</td>
										<td class="padding-all-10 text-right">May 1, 2015 | 4:20 PM</td>
									</tr>
									<tr>
										<td class="padding-all-10"><img class="small-thumb" src="../assets/images/affliates/gw-logo.png" alt="greenwich"></td>
										<td class="padding-all-10">Greenwich</td>
										<td class="padding-all-10 text-right">May 7, 2015 | 12:25 PM</td>
									</tr>
									<tr>
										<td class="padding-all-10"><img class="small-thumb" src="../assets/images/affliates/ck-logo.png" alt="chowking"></td>
										<td class="padding-all-10">Chowking</td>
										<td class="padding-all-10 text-right">April 27, 2015 | 9:14 PM</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="text-right margin-top-20">
						<button type="button" class="btn btn-dark margin-right-10">Update Customer Information</button>
						<button type="button" class="btn btn-dark margin-right-10" disabled>Find RTA to Add Last Meal to Order</button>
						<button type="button" class="btn btn-dark" disabled>Find RTA to Proceed to Order</button>
					</div>

				</div>
			</div>
			<!-- sample end-1 -->
		</div>
	</section>

	<!-- modal -->
	<div class="modal-container" modal-id="deliver-to-different">
		<div class="modal-body small">
			<div class="modal-head ">
				<p>Deliver to a Different Address</p>
				<div class="modal-close close-me"></div>
			</div>

			<div class="modal-content">

				<!-- error message -->
				<div class="error-msg margin-bottom-10">
					<i class="fa fa-exclamation-triangle margin-right-10"></i>Please fill up all requred fields.
				</div>

				<!-- address -->
				<table class="margin-bottom-15 margin-left-15">
					<tr>
						<td><label>Address Type: <span class="red-color">*</span></label></td>
						<td><label>Address Label: <span class="red-color">*</span></label></td>
					</tr>
					<tr>
						<td>
							<div class="select margin-right-50">
								<select>
									<option value="Home">Home</option>
								</select>
							</div>
						</td>
						<td>
							<input type="text" class="normal">	
						</td>
					</tr>
				</table>
				<hr />
				<!-- province -->
				<table class="margin-top-15 margin-left-15">
					<tr>
						<td><label>Province: <span class="red-color">*</span></label></td>
						<td><label>City: <span class="red-color">*</span></label></td>
					</tr>
					<tr>
						<td>
							<div class="select margin-right-50">
								<select >
									<option value="Home">Home</option>
								</select>
							</div>
						</td>
						<td>
							<div class="select error-form">
								<select>
									<option value="Mom's House">Mom's House</option>
								</select>
							</div>
						</td>
					</tr>
				</table>

				<div class="margin-left-15	">
					<label class="margin-top-10">Landmark:</label>
					<input type="text" class="xlarge">
				</div>
				
				<!-- 2 column table -->
				<table class="margin-top-10 margin-left-15">
					<tr>
						<td><label>House Number: </label></td>
						<td><label>Building: </label></td>
					</tr>
					<tr>
						<td><input type="text" class="normal margin-right-30"></td>
						<td><input type="text" class="normal"></td>
					</tr>
					<tr>
						<td><label class="margin-top-10">Unit: </label></td>
						<td><label>Floor: </label></td>
					</tr>
					<tr>
						<td><input type="text" class="normal"></td>
						<td><input type="text" class="normal"></td>
					</tr>
				</table>

				<!-- single column table -->
				<table class="margin-left-15">
					<tr>
						<td ><label class="margin-top-10">Street:</label></td>
					</tr>
					<tr>
						<td><input type="text" class="xlarge"></td>
					</tr>
					<tr>
						<td><label class="margin-top-10">Subdivision:</label></td>
					</tr>
					<tr>
						<td><input type="text" class="xlarge"></td>
					</tr>
					<tr>
						<td><label class="margin-top-10">Barangay:</label></td>
					</tr>
					<tr>
						<td><input type="text" class="xlarge"></td>
					</tr>				
				</table>
				<div class="form-group margin-top-10 margin-left-15">
					<input class="chck-box" id="card-number" type="checkbox">
					<label class="chck-lbl" for="card-number">Set this address as the default for this computer</label>
				</div>				
			</div>

			<div class="f-right margin-right-30 margin-bottom-10">
					<button type="button" class="btn btn-dark margin-right-10">Confirm </button>
					<button type="button" class="btn btn-dark">Cancel</button>
				</div>
				<div class="clear"></div>
		</div>
	</div>
<?php include "../construct/footer.php"; ?>