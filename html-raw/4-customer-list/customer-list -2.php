<?php include "../construct/header.php"; ?>
<section class="container-fluid" section-style="top-panel">

		<!-- customers list-->
		<div class="row header-container">
			<div class="contents">
				<h1 class="f-left">Customers List</h1>
				<div class="f-right margin-top-20">
					<button class="btn btn-dark margin-right-10">Add New Customer</button>
					<button class="btn btn-dark margin-right-10">Download Customers List</button>
					<button class="btn btn-dark ">FAQ</button>

				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div class="row">
			<div class="contents margin-top-20">
				<div class="f-left">
					<label class="margin-bottom-5 ">search:</label><br>
					<input class="normal f-left" type="text">
				</div>
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">search by:</label><br>
					<div class="select">
						<select>
							<option value="All Categories">Contact Number</option>
							<option value="Burgers">Address</option>
						</select>
					</div>
				</div>
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">Province:</label><br>
					<div class="select">
						<select>
							<option value="All Province">All Province</option>
							<option value="Abra">Abra</option>
							<option value="Batangas">Batangas</option>
							<option value="Bulacan">Bulacan</option>
							<option value="Bicol">Bicol</option>
							<option value="Cavite">Cavite</option>
							<option value="Bulacan">Laguna</option>
						</select>
					</div>
				</div>
				<div class="f-left margin-left-20">
					<label class="margin-bottom-5">Last Ordered:</label><br>
					<div class="select">
						<select>
							<option value="Today">Today</option>                            
						</select>
					</div>
				</div>
				<button class="f-left btn btn-dark margin-top-20 margin-left-20">Search</button>
			</div>
		</div>


		
		<div class="row margin-top-20">
			<div class="contents line">
				<div class="select larger">
					<select>
						<option value="All Item Types">Show All Customer</option>
						<option value="1pc Chicken"></option>
					</select>
				</div>
				<span class="white-space"></span>
				
				<div class="f-right bggray-white">
					<p class="f-left font-12 padding-left-10 padding-top-5">
						<strong>Sort By:</strong>
					</p>
					<p class="f-left font-12 padding-left-5 padding-top-5">   
						<strong>Customer Name</strong>  
					</p>
					<p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
					<p class="f-left font-12 padding-left-5 padding-top-5">  
						<strong>Behavior</strong>     
					</p>
					<p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
					<p class="f-left font-12 padding-top-5">
						<a class="red-color active" href="">
							<strong>Recently Ordered</strong>
							<img src="../assets/images/ui/sort-top-arrow.png">
						</a>
					</p>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</section>

	<section class="container-fluid" section-style="content-panel">
		<div class="row">
			<!-- sample customer -->
			<div class="content-container opaque">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-16"><strong>Mark Anthony D. Dulay</strong></p>
                        <p class="font-14 margin-top-10"><span class="red-color"><strong>Contact Num:</strong></span>(+63) 910-146-4178 <i class="fa fa-mobile"></i> Globe</p>
                    </div>
                    <div class="width-60per f-right">
                        <p class="font-14 no-margin-bottom"><strong><span class="red-color">THIS CUSTOMER IS DIFFICULT TO HANDLE</span></strong></p>
                        <p class="font-14 margin-top-10"><span class="red-color"><strong>Address:</strong></span> 168 San Ramon St., Brgy. San Pedro, Tarlac City - Tarlac</p>    
                    </div>
                    <div class="clear"></div>
                </div>
			</div>

			<!-- Update Custoner -->
			<div class="content-container">
				<div>
					<h3 class="f-left no-margin-top">Update Customer Information</h3>
					<p class="f-right red-color font-14 margin-top-5"><strong>FIELDS WITH * ARE REQUIRED</strong></p>
					<div class="clear"> </div>
					<hr class="margin-top-15 margin-bottom-15"/>
					
					<!-- content panel -->
					<div class="data-container split">
						<!-- contact number -->
						<div>
							<label>Contact Number: <span class="red-color font-16">*</span></label>
							<br />
							<div class="select small margin-right-10">
								<select>
									<option value="Mobile">Mobile</option>
								</select>
							</div>
							<input type="text" class="small">
						</div>
						<!-- alternate number -->
						<div class="margin-top-10">
							<label>Altername Number: <span class="red-color font-16">*</span></label>
							<br />
							<div class="select small margin-right-10 f-left">
								<select>
									<option value="Mobile">Mobile</option>
								</select>
							</div>
							<input type="text" class="xsmall f-left margin-right-10">
							<input type="text" class="xsmall f-left margin-right-10">
							<div class="f-left margin-left-5 text-right">
								<a href="#" class="font-12 ">+ Add Another <br />Contact Number</a>
							</div>
							<div class="clear"></div>
						</div>
						<!-- first name -->
						<div>
							<label class="margin-top-10">First name</label>
							<input type="text" class="large">
						</div>
						<!-- middle name -->
						<div>
							<label class="margin-top-10">Middle Name</label>
							<input type="text" class="large margin-top-5">
						</div>
						<!-- last name -->
						<div>
							<label class="margin-top-10">Last Name: <span class="red-color font-16">*</span></label>
							<input type="text" class="large margin-top-5">
						</div>
						<!-- email address -->
						<div >
							<label class="margin-top-10">Email Address: </label>
							<br />
							<input type="text" class="normal f-left">
							<div class="f-left margin-left-100 text-right">
								<a href="#" class="font-12 ">+ Add Another <br />Contact Number</a>
							</div>
							<div class="clear"></div>
						</div>
						<!-- date of birth -->
						<div>
							<label class="margin-top-10">Date of birth: </label>
							<div class="date-picker">
								<input type="text" data-date-format="MM/DD/YYYY">
								<span class="fa fa-calendar text-center red-color"></span>
							</div>
						</div>

						<!-- discount -->
						<div>
							<label class="margin-top-10">DISCOUNTS: </label>
							<br />

							<!-- senior -->
							<div class="margin-right-20">
								<div class=" bggray-dark">
									<p class="font-12 f-left margin-left-10 margin-top-10"><strong>Is this customer a Senior Citizen?</strong></p>
									<div class="form-group f-right padding-top-10 no-margin-bottom">
										<input id="senior1" type="radio" class="radio-box" name="senior">
										<label class="radio-lbl margin-right-10 " for="senior1">Yes</label>
										<input id="senior2" type="radio" class="radio-box" name="senior">
										<label class="radio-lbl margin-right-10" for="senior2">No</label>
									</div>
									<div class="clear"> </div>
									<hr class="margin-top-15 margin-bottom-15"/>
									<label class="font-12 margin-left-15">OSCA NUMBER: <span class="red-color font-12">*</span></label>
									<br />
									<input type="text" class="normal margin-top-5 margin-bottom-10 margin-left-10" />
								</div>
							</div>

							<!-- customer -->
							<div class="margin-right-20">
								<div class="bggray-dark margin-top-15">
									<p class="font-12 f-left margin-left-10 margin-top-10"><strong>Is this customer a PWD?</strong></p>
									<div class="form-group f-right padding-top-10 no-margin-bottom">
										<input id="pwd1" type="radio" class="radio-box" name="pwd">
										<label class="radio-lbl margin-right-10" for="pwd1">Yes</label>
										<input id="pwd2" type="radio" class="radio-box" name="pwd">
										<label class="radio-lbl margin-right-10" for="pwd2">No</label>
									</div>
									<div class="clear"></div>
								</div>
							</div>
						</div>

						<div>
							<label class="margin-top-10 no-margin-bottom">CARDS: </label>
							<br />
							<!-- happy plus card -->
							<div class="margin-right-20">
								<div class="bggray-dark margin-top-15">
									<p class="font-12 f-left margin-left-10 margin-top-10"><strong>Is this customer a Happy Plus Card holder?</strong></p>
									<div class="form-group f-right padding-top-10 no-margin-bottom">
										<input id="happy1" type="radio" class="radio-box" name="happyplus">
										<label class="radio-lbl margin-right-10" for="happy1">Yes</label>
										<input id="happy2" type="radio" class="radio-box" name="happyplus">
										<label class="radio-lbl margin-right-10" for="happy2">No</label>
									</div>
									<div class="clear" ></div>
									<hr class="margin-top-15 margin-bottom-15"/>

									<!-- happy card plus display bottom -->
									<div>
										<table border="0" class="margin-left-10">
											<tr>
												<td><label class="padding-left-10"><strong>CARD NUMBER: <span class="red-color">*</span></strong></label></td>
												<td><label class="padding-left-10"><strong>EXPIRY DATE: <span class="red-color">*</span></strong></label></td>
											</tr>
											<tr>
												<td>
													<input type="text" class="small">
												</td>
												<td>
													<div class="date-picker margin-left-10">
														<input type="text" data-date-format="MM/DD/YYYY" >
														<span class="fa fa-calendar red-color"></span> 
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class="form-group padding-top-10" >
														<input class="chck-box " id="card-number"  type="checkbox">
														<label class="chck-lbl" for="card-number">Is the card number <br /> smudged  or not visible?</label>
													</div>
												</td>
												<td>
													<label class="margin-left-10"> REMARKS:</label>                                             
													<input type="text" class="small margin-left-10">
												</td>
											</tr>
										</table>
									</div>
									
									<hr class="margin-top-15 margin-bottom-15"/>

									<div>
										<table border="0" class="margin-left-10">
											<tr>
												<td><label class="padding-left-10"><strong>CARD NUMBER: <span class="red-color">*</span></strong></label></td>
												<td><label class="padding-left-10"><strong>EXPIRY DATE: <span class="red-color">*</span></strong></label></td>
											</tr>
											<tr>
												<td>
													<input type="text" class="small" disabled>
												</td>
												<td>
													<div class="date-picker margin-left-10">
														<input type="text" data-date-format="MM/DD/YYYY" disabled>
														<span class="fa fa-calendar red-color"></span>
													</div>
												</td>
											</tr>
											<tr>
												<td>
													<div class="form-group padding-top-10">
														<input class="chck-box " id="card-number"  type="checkbox">
														<label class="chck-lbl" for="card-number">Is the card number <br /> smudged  or not visible?</label>
													</div>
												</td>
												<td>
													<label class="margin-left-10 ">REMARKS:</label>                                             
													<input type="text" class="small margin-left-10 ">
												</td>
											</tr>
										</table>                                    
									</div>
									<a href="#" class="margin-bottom-15 margin-right-15 font-12 f-right">+ Add Another Happy Plus Card</a>
									<div class="clear"> </div>
								</div>
							</div>

							<div class="margin-right-20">
								<div class="bggray-dark margin-top-15">
									<p class="font-12 f-left margin-left-10 margin-top-10"><strong>Is this customer a Loyalty Card holder?</strong></p>
									<div class="form-group f-right padding-top-10 no-margin-bottom">
										<input id="loyalty1" type="radio" class="radio-box" name="loyalty">
										<label class="radio-lbl margin-right-10" for="loyalty1">Yes</label>
										<input id="loyalty2" type="radio" class="radio-box" name="loyalty">
										<label class="radio-lbl margin-right-10" for="loyalty2">No</label>
									</div>
									<div class="clear" > </div>
								</div>
							</div>
						</div>
					</div>

					<div class="data-container split">
						<label>set customer behavior<span class="red-color">*</span></label><br/>
						<div class="select width-100per">
							<select>
								<option value="All Province">All Province</option>
								<option value="Abra">Abra</option>
								<option value="Batangas">Batangas</option>
								<option value="Bulacan">Bulacan</option>
								<option value="Bicol">Bicol</option>
								<option value="Cavite">Cavite</option>
								<option value="Bulacan">Laguna</option>
							</select>

							<label class="margin-top-20">address book</label><br/>

							<div class="bggray-light padding-all-10 font-14 small-curved-border">
								<div class="display-inline-mid margin-right-10 padding-left-20 width-20per text-center">
									<img src="../assets/images/work-icon.png" alt="work icon">
									<p class="font-12 no-margin-all"><strong>WORK</strong></p>
								</div>
								<div class="display-inline-mid padding-left-10 width-80per">
									<p class="no-margin-all"><strong>Cr8v Web Solutions, Inc. - <span class="light-red-color">Default</span></strong> <br>66C &amp; 66D, San Rafael St. Brgy. Kapitolyo, Pasig City. - NCR</p>
									<p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
								</div>
								<div class="margin-top-10">
									<button type="button" class="btn btn-dark margin-right-10" disabled>Set as default Address</button>
									<button type="button" class="btn btn-dark margin-right-10">Update</button>
									<button type="button" class="btn btn-dark">Archive</button>
								</div>
							</div>

							<div class="bggray-light padding-all-10 font-14 small-curved-border margin-top-15">
								<div class="display-inline-mid margin-right-10 padding-left-20 width-20per text-center">
									<img src="../assets/images/Home2.svg" alt="work icon" class="small-thumb">
									<p class="font-12 no-margin-all"><strong>home</strong></p>
								</div>
								<div class="display-inline-mid padding-left-10 width-80per">
									<p class="no-margin-all"><strong>Cr8v Web Solutions, Inc. - <span class="light-red-color">Default</span></strong> <br>66C &amp; 66D, San Rafael St. Brgy. Kapitolyo, Pasig City. - NCR</p>
									<p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
								</div>
								<div class="margin-top-10">
									<button type="button" class="btn btn-dark margin-right-10">Set as default Address</button>
									<button type="button" class="btn btn-dark margin-right-10">Update</button>
									<button type="button" class="btn btn-dark">Archive</button>
								</div>
							</div>

							<a class="f-right font-14 margin-top-15" href="#">+ Add New Address</a>
							<div class="clear"></div>
						</div>
					</div>
				</div>      
				<!-- button bottom -->
				<div class="f-right margin-top-20">
					<button type="button" class="btn btn-dark margin-right-10">Save Changes</button>
					<button type="button" class="btn btn-dark">Cancel</button>
				</div>
				<div class="clear"></div>

			</div>  
		</div>
		</div>
	</section>
	

<?php include "../construct/footer.php"; ?>