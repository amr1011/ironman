<?php include "../construct/header.php"; ?>
<section class="container-fluid" section-style="top-panel">

        <!-- customers list-->
        <div class="row header-container">
            <div class="contents">
                <h1 class="f-left">Customers List</h1>
                <div class="f-right margin-top-20">
                    <button class="btn btn-light margin-right-10">Add New Customer</button>
                    <button class="btn btn-light margin-right-10">Download Customers List</button>
                    <button class="btn btn-dark ">FAQ</button>

                </div>
                <div class="clear"></div>
            </div>
        </div>

        <div class="row">
            <div class="contents margin-top-20">
                <div class="f-left">
                    <label class="margin-bottom-5 ">search:</label><br>
                    <input class="normal f-left" type="text">
                </div>
                <div class="f-left margin-left-20">
                    <label class="margin-bottom-5">search by:</label><br>
                    <div class="select">
                        <select>
                            <option value="All Categories">Contact Number</option>
                            <option value="Burgers">Address</option>
                        </select>
                    </div>
                </div>
                <div class="f-left margin-left-20">
                    <label class="margin-bottom-5">Province:</label><br>
                    <div class="select">
                        <select>
                            <option value="All Province">All Province</option>
                            <option value="Abra">Abra</option>
                            <option value="Batangas">Batangas</option>
                            <option value="Bulacan">Bulacan</option>
                            <option value="Bicol">Bicol</option>
                            <option value="Cavite">Cavite</option>
                            <option value="Bulacan">Laguna</option>
                        </select>
                    </div>
                </div>
                <div class="f-left margin-left-20">
                    <label class="margin-bottom-5">Last Ordered:</label><br>
                    <div class="select">
                        <select>
                            <option value="Today">Today</option>                            
                        </select>
                    </div>
                </div>
                <button class="f-left btn btn-light margin-top-20 margin-left-20">Search</button>
            </div>
        </div>


        
        <div class="row margin-top-20">
            <div class="contents line">
                <div class="select larger">
                    <select>
                        <option value="All Item Types">Show All Customer</option>
                        <option value="1pc Chicken"></option>
                    </select>
                </div>
                <span class="white-space"></span>
                
                

                
                <div class="f-right bggray-white">
                    <p class="f-left font-12 padding-left-10 padding-top-5">
                        <strong>Sort By:</strong>
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">   
                        <strong>Customer Name</strong>  
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">  
                        <strong>Behavior</strong>     
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-top-5">
                        <a class="red-color active" href="">
                            <strong>Recently Ordered</strong>
                            <img src="../assets/images/ui/sort-top-arrow.png">
                        </a>
                    </p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </section>

    <section class="container-fluid" section-style="content-panel">
        <div class="row">
            <!-- sample-1-OPAQUE-->
            <div class="content-container opaque">
                <div>
                    <div class="width-40per f-left">
                        
                        <p class="font-16 margin-bottom-5"><strong>Mark Anthony D. Dulay</strong></p>
                        <p><strong><span class="red-color">Contact Num: </span></strong> (+63) 915-516-6153<i class="fa fa-mobile margin-left-5"></i> Globe</p>
                    </div>
                    
                    <div class=" f-left">
                        <p class="margin-bottom-5"><strong><span class="red-color">THIS CUSTOMER IS DIFFICULT TO HANDLE</span></strong> </p>
                        <p class="margin-bottom-5"><strong><span class="red-color">Address: </span></strong>98 San Rafael Street, Brgy. Kapitolyo, Pasig City - NCR</p>
                        
                    </div>
               
                    <div class="clear"></div>
                </div>
            </div>


           <!-- sample 2 -->
            <div class="content-container">
                <div>
                    <div class="width-40per f-left">
                        <p class="font-16 "><strong>Audrey D. Hepburn</strong></p>                        
                    </div>                                        
                    <div class="width-20per f-right">
                        <p class="font-16  green-color"><strong>THIS CUSTOMER IS EASY TO HANDLE</strong></p>                        
                    </div>                    
                    <div class="clear"></div>
                    
                    <hr />                                
                    <div class="data-container split">
                        <!-- client's information -->
                        <div class="margin-top-20">
                            
                            <p class="f-left red-color font-12"><strong>Contact Number: </strong></p>                        
                            <p class="f-right font-12">(02) 257-68-95 <i class="fa fa-phone margin-right-5"></i>PLDT</p>                            
                            <div class="clear"></div>
                            <p class="f-right font-12">(+63) 915-516-6153 <i class="fa fa-mobile margin-right-5"></i>Globe</p>

                            <div class="clear"></div>

                            <p class="f-left red-color font-12"><strong>Email Address:</strong></p>
                            <p class="f-right font-12">audrey@gmail.com</p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12"><strong>Date of Birth: </strong></p>
                            <p class="f-right font-12">May 4, 1956</p>
                            <div class="clear"></div>
                        </div>

                        <!-- delivery address -->
                        <div>
                            <label>Discounts</label>
                             <div class="bggray-light padding-all-5 font-14 small-curved-border">
                                <div class="display-inline-mid ">
                                   <p class="font-12 no-margin-all"><strong>Senior Citizen</strong></p>
                                </div>
                                <div class="display-inline-mid margin-left-10 margin-top-10 margin-bottom-10 divider padding-left-10">
                                    <p class="no-margin-all font-12">Audrey Dean Hepburn</p>
                                    <p class="font-12 no-margin-all"><strong><span class="red-color">OSCA Number: </span>0014-7845</strong></p>
                                </div>
                            
                            </div>
                            <label class="margin-top-10">Cards:</label>
                            <!-- happy plus picture -->
                            <div class="bggray-light padding-all-5 font-14 small-curved-border">
                                <div class="display-inline-mid margin-left-10">
                                    <img class="thumb " src="../assets/images/happy-plus.jpg" alt="happy-plus"/>
                                </div>
                                <div class="display-inline-mid margin-left-10 divider padding-left-10">
                                    <p class="no-margin-all">008-248-369-154<br/>
                                    <span class="red-color"><strong>Exp. Date:</strong></span> September 20, 2016</p>
                                </div>
                                <div class="display-inline-mid text-center margin-left-10">
                                    <a href="#">Show Card<br/>History</a>
                                </div>
                            </div>
                            <!-- happy plus picture -->
                            <div class="bggray-light padding-all-5 margin-top-10 font-14 small-curved-border">
                                <div class="display-inline-mid margin-left-10">
                                    <img class="thumb" src="../assets/images/happy-plus.jpg" alt="happy-plus"/>
                                </div>
                                <div class="display-inline-mid margin-left-10 divider padding-left-10">
                                    <p class="no-margin-all">003-275-472-124<br/>
                                    <span class="red-color"><strong>Exp. Date:</strong></span> Ocotober 18, 2016</p>
                                </div>
                                <div class="display-inline-mid text-center margin-left-20">
                                    <a href="#">Show Card<br/>History</a>
                                </div>
                            </div>

                            <!-- address book -->
                            
                            <label class="margin-top-10">Address Book: </label>
                            <div class="bggray-light padding-all-5 font-14 small-curved-border margin-bottom-10">
                                <div class="display-inline-mid margin-right-10 padding-left-20">
                                    <img src="../assets/images/work-icon.png" alt="work icon"/>
                                    <p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
                                </div>                                
                                <div class="display-inline-mid margin-left-10  padding-left-10">
                                    <p class="no-margin-all"><strong>Cr8v Web Solutions, Inc. - <span class="red-color">Default</span></strong></p>
                                    <p class="no-margin-all">66C &amp; 66D, San Rafael St. <br/> Brgy. Kapitolyo, Pasig City. - NCR</p>
                                    <p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
                                </div>
                            </div>  


                        
                            <div class="bggray-light padding-all-5 font-14 small-curved-border ">
                                <div class="display-inline-mid margin-right-10 padding-left-20">
                                    <img src="../assets/images/Home2.svg" alt="work icon" class="small-thumb"/>
                                    <p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
                                </div>
                                <div class="display-inline-mid margin-left-10  padding-left-10">
                                    <p class="no-margin-all"><strong>House Address # 1 </strong></p>
                                    <p class="no-margin-all">Rm. 404 3rd Floor, 691 Eduard Bldg, <br />Green Field St, Kapitolyo, Pasig City - NCR</p>
                                    <p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
                                </div>                                             
                            </div>
                        </div>                                        
                </div>
                    


            <div class="data-container split margin-left-15">
                <div class="margin-top-20">
                    <div class="category-item f-left">
                        <img src="../assets/images/Customer_Call.svg" alt="customer logo calling"  />
                        <p class="font-12 f-right margin-top-10 padding-right-10"><strong>Voice Customer</strong></p>                        
                        <div class="clear"></div>

                    </div>
                    
                    <div class="category-item f-right">
                        <img src="../assets/images/Customer_Web.svg" alt="customer logo calling" class="padding-left-10" />
                        <p class="font-12 f-right margin-top-10"><strong>Web Customer</strong></p>                        
                    </div>
                    <div class="clear"></div>
                    
                </div>
                <label class="margin-top-15">order history:</label>
                     <div class="arrow-selector">
                        <div class="arrow-left no-padding-all"></div>
                        <div class="select">
                            <select>
                                <option>April 27, 2015 | 4:20 PM</option>
                                <option>April 28, 2015 | 4:20 PM</option>
                            </select>
                        </div>
                        <div class="arrow-right no-padding-all"></div>
                    </div>

                    

                <div class="small-curved-border margin-top-20">
                    <table class="font-14">
                        <thead class="bggray-dark">
                            <tr>
                                <th class="padding-all-10">Quantitiy</th>
                                <th class="padding-all-10">Product</th>
                                <th class="padding-all-10">Price</th>
                                <th class="padding-all-10">Subtotal</th>
                            </tr>
                        </thead>
                        <tbody class="bggray-light">
                            <tr>
                                <td class="padding-all-10 text-center">2</td>
                                <td class="padding-all-10"><div class="arrow-down"></div> Champ Amazing aloha</td>
                                <td class="padding-all-10">203.50</td>
                                <td class="padding-all-10">407.00</td>
                            </tr>
                            <tr>
                                <td class="padding-all-10 text-center gray-color">2</td>
                                <td class="padding-all-10 padding-left-35">Champ Amazing aloha</td>
                                <td class="padding-all-10">17.60</td>
                                <td class="padding-all-10">35.20</td>
                            </tr>
                            <tr>
                                <td class="padding-all-10 text-center gray-color">2</td>
                                <td class="padding-all-10 padding-left-35">Regular French Fries</td>
                                <td class="padding-all-10">0.00</td>
                                <td class="padding-all-10">0.00</td>
                            </tr>
                            <tr>
                                <td class="padding-all-10" colspan="4"><hr/></td>
                            </tr>
                            <tr class="">
                                <td class="padding-all-10 text-center">2</td>
                                <td class="padding-all-10">Sundae Strawberry</td>
                                <td class="padding-all-10">30.80</td>
                                <td class="padding-all-10">61.60</td>
                            </tr>
                            <tr>
                                <td class="padding-all-10" colspan="4"><hr/></td>
                            </tr>
                            <tr>
                                <td class="padding-all-10 text-center">2</td>
                                <td class="padding-all-10">Yum with Cheese</td>
                                <td class="padding-all-10">44.00</td>
                                <td class="padding-all-10">88.60</td>
                            </tr>
                            <tr class="bggray-middark">
                                <td colspan="2" class="padding-all-10 text-left">Total Cost</td>
                                <td colspan="2" class="padding-all-10 text-right">591.80 PHP</td>
                            </tr>
                            <tr class="bggray-middark">
                                <td colspan="2" class="padding-all-10 text-left">Added VAT</td>
                                <td colspan="2" class="padding-all-10 text-right">29.59 PHP</td>
                            </tr>
                            <tr class="bggray-middark">
                                <td colspan="2" class="padding-all-10 text-left">Delivery Charge</td>
                                <td colspan="2" class="padding-all-10 text-right">20.00 PHP</td>
                            </tr>
                            <tr class="bggray-dark">
                                <td colspan="2" class="padding-all-10 text-left font-16"><strong>Total Bill</strong></td>
                                <td colspan="2" class="padding-all-10 text-right font-14"><strong>641.39 PHP</strong></td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <label class="margin-top-15">other brand order history</label>
                        <div class="bggray-light">
                            <table class="font-14 width-100per">
                                <tbody>
                                    <tr>
                                        <td class="padding-all-10"><img class="small-thumb" src="../assets/images/affliates/bk-logo.png" alt="burger king"></td>
                                        <td class="padding-all-10">Burger King</td>
                                        <td class="padding-all-10 text-right">May 7, 2015 | 12:25 PM</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10"><img class="small-thumb" src="../assets/images/affliates/gw-logo.png" alt="greenwich"></td>
                                        <td class="padding-all-10">Greenwich</td>
                                        <td class="padding-all-10 text-right">April 27, 2015 | 9:14 AM</td>
                                    </tr>
                                    <tr>
                                        <td class="padding-all-10"><img class="small-thumb" src="../assets/images/affliates/ck-logo.png" alt="chowking"></td>
                                        <td class="padding-all-10">Chowking</td>
                                        <td class="padding-all-10 text-right">April 25, 2015 | 10:11 AM</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    <div class="f-right margin-top-20">
                        <button type="button" class="btn btn-light margin-right-10">Update Customer Information</button>              
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
	

<?php include "../construct/footer.php"; ?>