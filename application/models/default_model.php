<?php

class Default_model extends CI_Model
{

    public function __construct()
    {
        parent :: __construct();
    }

    public function get_whitelist_ip() {
        $sql = "SELECT * from ip_whitelist";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

}