<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);
define('EMAIL_FROM', 'nkag-emailer@gmail.com');
define('EMAIL_NAME', 'NKAG');
define('CREDIT_TERM_ALLOWANCE', 0); //days

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');



define('PAGINATION_LIMIT',10);

define('VAT_VALUE', 12);

define('DIR_TEMP_PATH', 'temp/');

define('PUSHER_APP_ID','138101');
define('PUSHER_APP_KEY','16b2efccdf1b1e9212e4');
define('PUSHER_APP_SECRET','23c82f1bf4c3a1e0e004');


/*jech pusher*/
/*define('PUSHER_APP_ID','136512');
define('PUSHER_APP_KEY','7e65b7224a1127d99e02');
define('PUSHER_APP_SECRET','3b49676c6a639664484a');*/

define('GIS_IP','173.255.206.64');
/* End of file constants.php */
/* Location: ./application/config/constants.php */