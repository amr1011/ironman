<?php

if ( ! function_exists('assets_url')) {
	function assets_url() {
		$ci =& get_instance();
		return $ci->config->item('assets_url');
	}
}

if ( ! function_exists('add_product_image_path')) {
	
	function add_product_image_path ($product_id=0)
	{
		/* if($product_id > 0)
		{
			$product = new Product_image_map();

			$product->get_where(array('item_id' => $product_id));

			if($product->exists())
			{
				return $product->image_path;
			}

			$product->clear();
		} */

		return assets_url() . "images/food/no-image_4.png";
	}
}