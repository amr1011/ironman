<?php
if ( ! function_exists( 'validate_date' ) )
{
    /**
     * Validates date if it is valid or not
     * Sample usage:
     * validateDate('2012-02-28 12:12:12')
     *
     * @param string $datetime required The date to be validate
     * @param string $format   optional Date format
     *
     * @return bool
     */
    function validate_date( $datetime, $format = 'Y-m-d H:i:s' )
    {
        $d = DateTime::createFromFormat( $format, $datetime );

        return $d && $d->format( $format ) == $datetime;
    }
}

if ( ! function_exists( 'to_full_date' ) )
{
    /**
     * Formats the date to 'January 1, 2000'
     *
     * @param string $datetime Date to be formated
     * @param string $format   Default value 'F d, Y'
     *
     * @return bool|string If bool date is invalid
     */
    function to_full_date( $datetime, $format = 'F d, Y' )
    {
        // validate first the datetime
        if ( validate_date( $datetime ) )
        {
            return date( $format, strtotime( $datetime ) );
        }

        return FALSE;
    }
}

if ( ! function_exists( 'to_short_date' ) )
{
    /**
     * Formats the date to 'Jan 1, 2000'
     *
     * @param string $datetime Date to be formated
     *
     * @return bool|string If bool date is invalid
     */
    function to_short_date( $datetime )
    {
        // validate first the datetime
        return to_full_date($datetime, 'M d, Y');
    }
}

if ( ! function_exists( 'to_proper_time' ) )
{
    /**
     * Formats the time to '12:00 pm'
     *
     * @param $datetime
     *
     * @return bool|string If bool date is invalid
     */
    function to_proper_time( $datetime )
    {
        return to_full_date($datetime, 'h:i A');
    }
}

if ( ! function_exists( 'is_weekend' ) )
{
    /**
     * Checks if the current day is weekend
     *
     * @param date
     *
     * @return bool 
     **/
    function is_weekend( $date ) 
    {
        // $result_date = date( 'N', strtotime($date) );
        return ( (int)date( 'w', strtotime($date) ) >= 6 ) ? TRUE : FALSE;
    }
}

if ( ! function_exists( 'is_end_of_month' ) )
{
    /**
     * Checks if the current day is last day of the current month
     *
     * @param date
     *
     * @return bool 
     **/
    function is_end_of_month( $date ) 
    {
        // return ( date( 'j', strtotime($date) ) == date( 't', date( 'n', strtotime($date) ) ) );
        return ( date( 'j', strtotime($date) ) == date( 't', date( 'n', strtotime($date) ) ) ) ? TRUE : FALSE;
    }
}

if ( ! function_exists( 'convert_datetime_mmddyy' ) )
{
    /**
     *
     * @param date
     * @param delimiter
     *
     * @return date(Y-m-d)
     **/
    
    function convert_datetime_mmddyy($date, $delimiter='/'){
        /*
            function that converts 12/31/2009 12:00 PM to 2009-12-31 12:00:00
        */
        $chunks = explode($delimiter, $date);
        
        if(count($chunks)==3){
            $year_chunks = explode(' ', $chunks[2]);
            $month = $chunks[0];
            $day = $chunks[1];
            $time = $year_chunks[1] . $year_chunks[2];

            $temp_date = $year_chunks[0].'-'.$month.'-'.$day;

            $new_date = /*$temp_date . ' ' . */date('Y-m-d H:i:s', strtotime($temp_date . ' ' . $time));

            return $new_date;
        }
        else{
            return $date;
        }
    }
}