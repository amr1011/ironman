<?php

if( ! function_exists( 'format_tomoney' ))
{
    /**
     * Format the number to money format (01.50)
     *
     * @param int|float|string $amount
     *
     * @return string
     */
     function format_tomoney( $amount )
    {
        return sprintf("%01.2f", $amount);
    }

    /**
     * Format the passed value to 000,000.00
     *
     * @param int|float|string $amount
     *
     * @return string
     **/
     function to_money( $amount )
    {
        return number_format($amount, '2', '.', ',');
    }
}