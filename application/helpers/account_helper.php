<?php
if ( ! function_exists( 'is_representative_primary' ) )
{
    /**
     * Checks if account is primary representative
     * Sample usage:
     *
     *
     */
    function is_representative_primary($id)
    {
        $CI = get_instance();
        $CI->load->model('users/user');
        $representative = $CI->user->get_representative($id);
        if($representative->is_primary == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}

if ( ! function_exists( 'is_representative_client_onhold' ) )
{
    /**
     * Checks if account is primary representative
     * Sample usage:
     *
     *
     */
    function is_representative_client_onhold($id)
    {
        $CI = get_instance();
        $CI->load->model('users/user');
        $representative = $CI->user->get_representative_client_suspension($id);
        if($representative->is_suspended == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}

if ( ! function_exists( 'is_representative_department_onhold' ) )
{
    /**
     * Checks if account is primary representative
     * Sample usage:
     *
     *
     */
    function is_representative_department_onhold($id)
    {
        $CI = get_instance();
        $CI->load->model('users/user');
        $representative = $CI->user->get_representative_department_suspension($id);
        if($representative->is_suspended == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}

if ( ! function_exists( 'is_allowed_in_page' ) )
{
    /**
     * Checks if account is primary representative
     * Sample usage:
     *
     *
     */
    function is_allowed_in_page( $page = '', $user = array() )
    {
        $CI = get_instance();
        $main_url = $page; //primary url
        $user_id = $user->id;
        $role_id = $user->role_id;
        $CI->load->model( 'users/user' );

        if ( strlen( $main_url ) > 0 AND $main_url != 'dashboard' AND ( $role_id == 1 || $role_id == 2 || $role_id == 7 ) ) //nkag admin or client only
        {
            $table = ( $role_id == 2 || $role_id == 1 ) ? 'admin_tabs' : 'user_tabs';

            $user_tabs = $CI->user->check_user_allowed_tab( $main_url, $table, $user_id );
            if ( count( $user_tabs ) == 0 )
            {
                show_404();
            }
        }
    }

}