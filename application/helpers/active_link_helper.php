<?php

if ( ! function_exists( 'active_link' ) )
{
    /**
     * Checks if last uri is matched
     *
     * @param string $mylinks
     * @param string $current_uri
     *
     * @return bool
     */
    function active_link( $mylinks, $current_uri )
    {
        $is_active = FALSE;

        $uri = explode('/', $mylinks);
        $end = end($uri);
        // echo $end;
        // exit();

        $current_uri_e = explode( '/', $current_uri );
        $end_cur_uri = end( $current_uri_e );

        array_pop($uri);
        $uri = implode('\/', $uri);

        switch ( $end )
        {
            case '(:num)':
                $is_active = preg_match('/^[0-9]+$/', $end_cur_uri);
                break;
            case '(:any)':
                $is_active = preg_match('/^'.$uri.'\/[\S]+$/', $current_uri);
                break;
        }

        array_pop($current_uri_e);
        $current_uri_e = implode('/', $current_uri_e);
        if($current_uri_e == $mylinks)
        {
            $is_active = TRUE;
        }

        return $is_active;
    }
}