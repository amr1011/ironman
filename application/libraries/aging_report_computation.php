<?php

/**
 * Class Aging_report_computation
 * Computes the Total Outstanding Receivables based on the DUE DATE, principal amount (total amount of the order)
 *  and the date today
 *
 * @author Eric Nilo
 */
class Aging_report_computation {
    /**
     * The computed total_outstanding_receivables
     *
     * @var string
     */
    public $total_outstanding_receivables;

    /**
     * Credit Term is 30 days  - Credit Terms determined by NKAG/SBU Finance
     * Penalty is 2% per month - Penalty rate determined by SBU Finance
     *
     * @var float
     */
    public $penalty_per_month = 2;

    /**
     * If penalty per month is the default which is 0.2 then this will return 0.0658%
     *  Computation is (2*12)/365
     *
     * @var float
     */
    public $penalty_per_day;

    /**
     * Computed penalty based on number of days late and penalty per day
     *
     * @var float
     */
    public $total_penalty_per_day = 0;

    /**
     * The number of days late based on the due date and the current date
     *
     * @var int
     */
    public $number_of_days_late = 0;

    /**
     * Container of the error messages
     *
     * @var array
     */
    private $ERROR_MESSAGES;

    public function __construct()
    {
        $this->ERROR_MESSAGES = array(
            'date_format_invalid' => 'Date is either has wrong format (it should be in this format "Y-m-d" or datetime) or invalid.'
        );

        $this->penalty_per_day = ( ( $this->penalty_per_month / 100 ) * 12 ) / 365;
    }

    /**
     * Sets the penalty per month
     *
     * @param float $penalty_per_month If set then overrides the default 0.02 penalty per month
     */
    public function set_penalty_per_month($penalty_per_month)
    {
        if ( $penalty_per_month !== NULL AND is_float( $penalty_per_month ) )
        {
            $this->penalty_per_month = $penalty_per_month;
        }
    }

    /**
     * Computes the overdue of a specific order this also serve as the index or main processor of this class
     *
     * @param string    $due_date         Should be in this format 'Y-m-d'. Also accepts datetime format.
     * @param float|int $principal_amount The total amount of a specific order
     */
    public function compute( $due_date, $principal_amount )
    {
        $tot_out_re = $principal_amount;

        if ( $this->get_days_late( $due_date ) > 0)
        {
            $this->total_penalty_per_day = $this->penalty_per_day * $this->number_of_days_late;
            $tot_out_re = $principal_amount + ($principal_amount * $this->total_penalty_per_day);
        }

        $this->total_outstanding_receivables = to_money($tot_out_re);
    }

    /**
     * Get the number of days late based on the due date
     *
     * @param string $due_date Should be in this format 'Y-m-d'. Also accepts datetime format.
     *
     * @return int The number of days late
     */
    public function get_days_late( $due_date )
    {
        if ( validate_date( $due_date ) )
        {
            $validated_due_date = date( 'Y-m-d', strtotime( $due_date ) );
        }
        else if ( validate_date( $due_date, 'Y-m-d' ) )
        {
            $validated_due_date = $due_date;
        }
        else
        {
            return $this->ERROR_MESSAGES['date_format_invalid'];
        }

        $days_late = 0;

        $today = new DateTime( date( 'Y-m-d' ) );
        $due_date_datetime = new DateTime( $validated_due_date );

        $days_late_object = $due_date_datetime->diff($today);

        if ( ! $days_late_object->invert )
        {
            $days_late = $days_late_object->days;
        }

        $this->number_of_days_late = $days_late;

        return $days_late;
    }
}