<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Location
{
 	protected 	$ci;

	public function __construct()
	{
        $this->ci =& get_instance();
	}

	/**
	 * provinces function
	 *
	 * Get xavier provinces through api
	 *
	 * @return array,json or html dropdown
	 * @author Ivan Gutierrez
	 **/
	public function provinces ($type='json',$id=0,$default=false)
	{
		// $url = $this->ci->config->item('api_url') . 'locations/provinces';
		// $provinces = curl_api($url);
		// kprint($provinces);exit;
		$this->ci->load->model('clients/location_model');
		$provinces = $this->ci->location_model->get_provinces();
		$provinces = json_encode($provinces);
		if($provinces)
		{
			if($type == 'array')
			{
				if($id > 0)
				{
					$result = json_decode($provinces);
					foreach ($result as $key => $row) {
						if($row->id === $id)
						{
							return $row;
						}
					}
				}
				return json_decode($provinces);
			}
			elseif($type == 'dropdown')
			{
				return dropdown_array(json_decode($provinces),'name');
			}
			
			return $provinces;
		}
		else
		{
			return FALSE;
		}
	}
	/**
	 * cities function
	 *
	 * Get xavier cities through api
	 *
	 * @return array,json or html dropdown
	 * @author Ivan Gutierrez
	 **/
	public function cities ($province_id=0,$type='json',$default=false)
	{
		// $url = $this->ci->config->item('api_url') . 'locations/cities';
		// $cities = curl_api($url,array('province_id' => $province_id));
		$this->ci->load->model('clients/location_model');
		$cities = $this->ci->location_model->get_cities($province_id);
		$cities = json_encode($cities);
		if($cities)
		{
			if($type == 'array')
			{
				return json_decode($cities, true);
			}
			elseif($type == 'dropdown')
			{
				return cities_array(json_decode($cities),'name');
			}
			
			return $cities;
		}
		else
		{
			return FALSE;
		}
	}

	/**
	 * barangay function
	 *
	 * Get xavier barangay through api
	 *
	 * @return array or json
	 * @author Ivan Gutierrez
	 **/
	public function barangay ($city_id=0,$autocomplete=false)
	{
		$result = array();

		$this->ci->load->model('clients/location_model');
		$barangay = $this->ci->location_model->get_barangays($city_id);
		$barangay = json_encode($barangay);
		if($barangay)
		{	
			$result = location_array(json_decode($barangay),'label');
			
			if($autocomplete)
			{
				$result = autocomplete_array(json_decode($barangay),'label');
			}			
		}
		
		return $result;
	}

	/**
	 * subdivisions function
	 *
	 * Get xavier subdivisions through api
	 *
	 * @return array or json
	 * @author Ivan Gutierrez
	 **/
	public function subdivisions ($city_id=0,$autocomplete=false)
	{
		$result = array();

		$this->ci->load->model('clients/location_model');
		$subdivisions = $this->ci->location_model->get_subdivisions($city_id);
		$subdivisions = json_encode($subdivisions);
		if($subdivisions)
		{	
			$result = location_array(json_decode($subdivisions),'label');
			
			if($autocomplete)
			{
				$result = autocomplete_array(json_decode($subdivisions),'label');
			}
				
		}
		return $result;
	}

	/**
	 * streets function
	 *
	 * Get xavier streets through api
	 *
	 * @return array,json or html dropdown
	 * @author Ivan Gutierrez
	 **/
	public function streets ($city_id=0,$autocomplete=false)
	{
		$result = array();

		$this->ci->load->model('clients/location_model');
		$streets = $this->ci->location_model->get_streets($city_id);
		$streets = json_encode($streets);
		if($streets)
		{	
			$result = location_array(json_decode($streets),'label');
			
			if($autocomplete)
			{
				$result = autocomplete_array(json_decode($streets),'label');
			}		
		}
		return $result;
	}

	/**
	 * second_streets function
	 *
	 * Get xavier second_streets through api
	 *
	 * @return array or json
	 * @author Ivan Gutierrez
	 **/
	public function second_streets ($city_id=0,$autocomplete=false)
	{
		$result = array();

		$this->ci->load->model('clients/location_model');
		$second_streets = $this->ci->location_model->get_second_streets($city_id);
		$second_streets = json_encode($second_streets);
		if($second_streets)
		{
			$result = location_array(json_decode($second_streets),'label');

			if($autocomplete)
			{
				$result = autocomplete_array(json_decode($second_streets),'label');
			}			
		}
		return $result;
	}

	public function buildings ($city_id=0,$autocomplete=false)
	{
		$result = array();

		$this->ci->load->model('clients/location_model');
		$buildings = $this->ci->location_model->get_buildings($city_id);
		$buildings = json_encode($buildings);
		if($buildings)
		{
			$result = location_array(json_decode($buildings),'label');

			if($autocomplete)
			{
				$result = autocomplete_array(json_decode($buildings),'label');
			}
		}
		return $result;
	}

	public function barangay_with_id ($city_id=0,$autocomplete=false)
	{
		$result = array();

		$this->ci->load->model('clients/location_model');
		$barangay = $this->ci->location_model->get_barangays($city_id);
		if(count($barangay) > 0)
		{
			$result = $barangay;
		}

		return $result;
	}

	public function subdivisions_with_id ($city_id=0,$autocomplete=false)
	{
		$result = array();

		$this->ci->load->model('clients/location_model');
		$subdivisions = $this->ci->location_model->get_subdivisions($city_id);
		if(count($subdivisions) > 0)
		{
			$result = $subdivisions;
		}

		return $result;
	}

	public function streets_with_id ($city_id=0,$autocomplete=false)
	{
		$result = array();

		$this->ci->load->model('clients/location_model');
		$streets = $this->ci->location_model->get_streets($city_id);
		if(count($streets) > 0)
		{
			$result = $streets;
		}

		return $result;
	}

	public function second_streets_with_id ($city_id=0,$autocomplete=false)
	{
		$result = array();

		$this->ci->load->model('clients/location_model');
		$second_streets = $this->ci->location_model->get_second_streets($city_id);
		if(count($second_streets) > 0)
		{
			$result = $second_streets;
		}

		return $result;
	}

	public function buildings_with_id ($city_id=0,$autocomplete=false)
	{
		$result = array();

		$this->ci->load->model('clients/location_model');
		$buildings = $this->ci->location_model->get_buildings($city_id);
		if(count($buildings) > 0)
		{
			$result = $buildings;
		}

		return $result;
	}
}

/* End of file location.php */
/* Location: ./application/libraries/location.php */
