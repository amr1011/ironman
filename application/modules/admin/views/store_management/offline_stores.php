	
	<!--Online Stores, Offline Stores-->
	<section id="offline_stores_count_container" class="container-fluid hidden store_management" section-style="top-panel">
		<div class="row header-container">
			<div class="contents padding-all-20">
				<!-- <button class="f-right btn btn-dark margin-left-15">FAQ</button> -->
				<button class="f-right btn btn-dark modal-trigger margin-left-15" modal-target="messenger">Messenger<div class="notify hidden">0</div></button>
				<button class="f-right btn btn-dark modal-trigger" modal-target="messenger">Send SMS Remininder to all Offline Stores</button>
				<div class="clear"></div>

				<div class="data-container margin-top-15 text-center data-container-offline-store">

                    <!--<div class="data-content">
						<p class="title">metro manila</p>
						<p class="desc">10</p>
					</div>-->
					<!--<div class="data-content">
						<p class="title">bacolod</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">bataan</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">batangas</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">bulacan</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">CDO</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">cavite</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">cebu</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">davao</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">ilo-ilo</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">laguna</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">nueva ecija</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">pampanga</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">pangasinan</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">tarlac</p>
						<p class="desc">10</p>
					</div>
					<div class="data-content">
						<p class="title">zambales</p>
						<p class="desc">10</p>
					</div>-->


				</div>
			</div>
		</div>

		<div class="row">
			<div class="contents margin-top-20">
				<label class="margin-bottom-5">search</label><br>
				<input class="search" type="text" id="offline-store-filter">
				<div class="select margin-left-20">
					<select name="offline_store_list_filter">
						<option value="Store Name">Store Name</option>
						<option value="Store Code">Store Code</option>
					</select>
				</div>
				<button class="btn btn-dark margin-left-20 search_offline_store">Search</button>
			</div>

			<div class="contents margin-top-20 line">
				<p class="bggray-white margin-top-5 f-right gray-color"><strong class="count_stores"><span class="offline_store_count"></span> / <span class="all_store_count"></span> Offline Stores</strong></p>
				<span class="white-space f-right"></span>
				<div class="clear"></div>
			</div>
		</div>
	</section>
	
	<section id="offline_stores_search_container" class="container-fluid hidden store_management" section-style="content-panel">
		<div class="row">
			<div class="content-container unboxed">
				<table class="width-100per offline-store-table">
					<thead class="white-color">
						<tr>
							<th class="padding-all-10 light-red-bg color sort_offline_store" data-sort="asc">System ID<i class="fa-asc hidden">&#9650;</i><i class="fa-desc hidden">&#9660;</i> </th>
							<th class="padding-all-10 dark-red-bg text-center sort_offline_store" data-sort="asc">Store Name<i class="fa-asc hidden">&#9650;</i><i class="fa-desc hidden">&#9660;</i> </th>
							<th class="padding-all-10 light-red-bg text-center sort_offline_store" data-sort="asc">Store Code<i class="fa-asc hidden">&#9650;</i><i class="fa-desc hidden">&#9660;</i> </th>
							<th class="padding-all-10 dark-red-bg text-center sort_offline_store" data-sort="asc">Area<i class="fa-asc hidden">&#9650;</i><i class="fa-desc hidden">&#9660;</i>  </th>
							<th class="padding-all-10 light-red-bg text-center sort_offline_store" data-sort="asc">Ownership<i class="fa-asc hidden">&#9650;</i><i class="fa-desc hidden">&#9660;</i>  </th>
							<th class="padding-all-10 dark-red-bg text-center sort_offline_store" data-sort="asc">Telco<i class="fa-asc hidden">&#9650;</i><i class="fa-desc hidden">&#9660;</i>  </th>
							<th class="padding-all-10 light-red-bg text-center sort_offline_store" data-sort="asc">Trading Time<i class="fa-asc hidden">&#9650;</i><i class="fa-desc hidden">&#9660;</i>  </th>
							<th class="padding-all-10 dark-red-bg text-center sort_offline_store" data-sort="asc">On Time<i class="fa-asc hidden">&#9650;</i><i class="fa-desc hidden">&#9660;</i>  </th>
							<th class="padding-all-10 light-red-bg text-center sort_offline_store" data-sort="asc">Off Time<i class="fa-asc hidden">&#9650;</i><i class="fa-desc hidden">&#9660;</i>  </th>
						</tr>
					</thead>
					<tbody class="text-center offline_store_container">

					</tbody>
				</table>
			</div>
		</div>
	</section>
	<!--Online Stores, Offline Stores-->
	
	
	
	
	
	
	