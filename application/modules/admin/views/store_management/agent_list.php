<!--Agent List-->
<section id="agent_list_count_container" class="container-fluid hidden store_management" section-style="top-panel">
    <div class="row header-container">
        <div class="contents padding-all-20">
            <div class="select f-left">
                <div class="frm-custom-dropdown">
                    <div class="frm-custom-dropdown-txt">
                        <input type="text" class="dd-txt" name="undefined" value="show-all"></div>
                    <div class="frm-custom-icon"></div>
                    <div class="frm-custom-dropdown-option" style="display: none;">
                        <div class="option agent-selection" data-value="show-all">Show All Agents</div>
                        <div class="option agent-selection" data-value="show-offline">Show Offline Agents</div>
                        <div class="option agent-selection" data-value="show-online">Show Online Agents</div>
                    </div>
                </div>
                <select class="frm-custom-dropdown-origin" style="display: none;">
                    <option value="show-all">Show All Agents</option>
                    <option value="show-offline">Show Offline Agents</option>
                    <option value="show-online">Show Online Agents</option>
                </select>
            </div>
            <!-- <button class="f-right btn btn-dark margin-left-15">FAQ</button> -->
            <button class="f-right btn btn-dark modal-trigger" modal-target="messenger">Messenger<div class="notify hidden">0</div>

            </button>
            <div class="clear"></div>

            <div class="data-container margin-top-15 text-center data-agent-province-container">
                <!--					<div class="data-content">-->
                <!--						<p class="title">metro manila</p>-->
                <!--						<p class="desc">10</p>-->
                <!--					</div>-->
                <!--					<div class="data-content">-->
                <!--						<p class="title">bacolod</p>-->
                <!--						<p class="desc">10</p>-->
                <!--					</div>-->
                <!--					<div class="data-content">-->
                <!--						<p class="title">bataan</p>-->
                <!--						<p class="desc">10</p>-->
                <!--					</div>-->
                <!--					<div class="data-content">-->
                <!--						<p class="title">batangas</p>-->
                <!--						<p class="desc">10</p>-->
                <!--					</div>-->
                <!--					<div class="data-content">-->
                <!--						<p class="title">bulacan</p>-->
                <!--						<p class="desc">10</p>-->
                <!--					</div>-->
                <!--					<div class="data-content">-->
                <!--						<p class="title">CDO</p>-->
                <!--						<p class="desc">10</p>-->
                <!--					</div>-->
                <!--					<div class="data-content">-->
                <!--						<p class="title">cavite</p>-->
                <!--						<p class="desc">10</p>-->
                <!--					</div>-->
                <!--					<div class="data-content">-->
                <!--						<p class="title">cebu</p>-->
                <!--						<p class="desc">10</p>-->
                <!--					</div>-->
                <!--					<div class="data-content">-->
                <!--						<p class="title">davao</p>-->
                <!--						<p class="desc">10</p>-->
                <!--					</div>-->
                <!--					<div class="data-content">-->
                <!--						<p class="title">ilo-ilo</p>-->
                <!--						<p class="desc">10</p>-->
                <!--					</div>-->
                <!--					<div class="data-content">-->
                <!--						<p class="title">laguna</p>-->
                <!--						<p class="desc">10</p>-->
                <!--					</div>-->
                <!--					<div class="data-content">-->
                <!--						<p class="title">nueva ecija</p>-->
                <!--						<p class="desc">10</p>-->
                <!--					</div>-->
                <!--					<div class="data-content">-->
                <!--						<p class="title">pampanga</p>-->
                <!--						<p class="desc">10</p>-->
                <!--					</div>-->
                <!--					<div class="data-content">-->
                <!--						<p class="title">pangasinan</p>-->
                <!--						<p class="desc">10</p>-->
                <!--					</div>-->
                <!--					<div class="data-content">-->
                <!--						<p class="title">tarlac</p>-->
                <!--						<p class="desc">10</p>-->
                <!--					</div>-->
                <!--					<div class="data-content">-->
                <!--						<p class="title">zambales</p>-->
                <!--						<p class="desc">10</p>-->
                <!--					</div>-->


            </div>
        </div>
    </div>

    <div class="row">
        <div class="contents margin-top-20">
            <label class="margin-bottom-5">search</label><br>
            <input class="search" type="text" id="agent-list-filter">

            <div class="select margin-left-20">
                <select name="agent_list_filter_column">
                    <option value="Username">Username</option>
                    <option value="Agent Number">Agent Number</option>
                </select>
            </div>
            <button class="btn btn-dark margin-left-20 search_agent">Search</button>
        </div>

        <div class="contents margin-top-20 line">
            <p class="bggray-white margin-top-5 f-right gray-color"><strong class="agents_count">119 Online Agents</strong></p>
            <span class="white-space f-right"></span>

            <div class="clear"></div>
        </div>
    </div>
</section>

<section id="agent_list_search_container" class="container-fluid hidden store_management" section-style="content-panel">
    <div class="row">
        <div class="content-container unboxed">
            <table class="width-100per agent-list-table">
                <thead class="white-color">
                    <tr>
                        <th class="padding-all-10 light-red-bg color width-10per sort_agent" data-sort="asc">Agent #<i class="fa-asc">&#9650;</i><i class="fa-desc hidden">&#9660;</i> </th>
                        <th class="padding-all-10 dark-red-bg text-center width-45per sort_agent" data-sort="asc">User Name<i class="fa-asc hidden">&#9650;</i><i class="fa-desc hidden">&#9660;</i> </th>
                        <th class="padding-all-10 light-red-bg text-center width-45per sort_agent" data-sort="asc">Area<i class="fa-asc hidden">&#9650;</i><i class="fa-desc hidden">&#9660;</i> </th>
                    </tr>
                </thead>
                <tbody class="text-center data-table-agent">
                    <!--						<tr class="bottom-border">-->
                    <!--							<td class="padding-all-10">473</td>-->
                    <!--							<td class="padding-all-10">JFC_Coord201</td>-->
                    <!--							<td class="padding-all-10">Metro Manila</td>-->
                    <!--						</tr>-->
                    <!--						<tr class="bottom-border">-->
                    <!--							<td class="padding-all-10">473</td>-->
                    <!--							<td class="padding-all-10">JFC_Coord201</td>-->
                    <!--							<td class="padding-all-10">Metro Manila</td>-->
                    <!--						</tr>-->
                    <!--						<tr class="bottom-border">-->
                    <!--							<td class="padding-all-10">473</td>-->
                    <!--							<td class="padding-all-10">JFC_Coord201</td>-->
                    <!--							<td class="padding-all-10">Metro Manila</td>-->
                    <!--						</tr>-->
                    <!--						<tr class="bottom-border">-->
                    <!--							<td class="padding-all-10">473</td>-->
                    <!--							<td class="padding-all-10">JFC_Coord201</td>-->
                    <!--							<td class="padding-all-10">Metro Manila</td>-->
                    <!--						</tr>-->
                    <!--						<tr class="bottom-border">-->
                    <!--							<td class="padding-all-10">473</td>-->
                    <!--							<td class="padding-all-10">JFC_Coord201</td>-->
                    <!--							<td class="padding-all-10">Metro Manila</td>-->
                    <!--						</tr>-->
                    <!--						<tr class="bottom-border">-->
                    <!--							<td class="padding-all-10">473</td>-->
                    <!--							<td class="padding-all-10">JFC_Coord201</td>-->
                    <!--							<td class="padding-all-10">Metro Manila</td>-->
                    <!--						</tr>-->
                    <!--						<tr class="bottom-border">-->
                    <!--							<td class="padding-all-10">473</td>-->
                    <!--							<td class="padding-all-10">JFC_Coord201</td>-->
                    <!--							<td class="padding-all-10">Metro Manila</td>-->
                    <!--						</tr>-->
                    <!--						<tr class="bottom-border">-->
                    <!--							<td class="padding-all-10">473</td>-->
                    <!--							<td class="padding-all-10">JFC_Coord201</td>-->
                    <!--							<td class="padding-all-10">Metro Manila</td>-->
                    <!--						</tr>-->
                    <!--						<tr class="bottom-border">-->
                    <!--							<td class="padding-all-10">473</td>-->
                    <!--							<td class="padding-all-10">JFC_Coord201</td>-->
                    <!--							<td class="padding-all-10">Metro Manila</td>-->
                    <!--						</tr>-->
                    <!--						<tr class="bottom-border">-->
                    <!--							<td class="padding-all-10">473</td>-->
                    <!--							<td class="padding-all-10">JFC_Coord201</td>-->
                    <!--							<td class="padding-all-10">Metro Manila</td>-->
                    <!--						</tr>-->
                    <!--						<tr class="bottom-border">-->
                    <!--							<td class="padding-all-10">473</td>-->
                    <!--							<td class="padding-all-10">JFC_Coord201</td>-->
                    <!--							<td class="padding-all-10">Metro Manila</td>-->
                    <!--						</tr>-->
                    <!--						<tr class="bottom-border">-->
                    <!--							<td class="padding-all-10">473</td>-->
                    <!--							<td class="padding-all-10">JFC_Coord201</td>-->
                    <!--							<td class="padding-all-10">Metro Manila</td>-->
                    <!--						</tr>-->
                    <!--						<tr class="bottom-border">-->
                    <!--							<td class="padding-all-10">473</td>-->
                    <!--							<td class="padding-all-10">JFC_Coord201</td>-->
                    <!--							<td class="padding-all-10">Metro Manila</td>-->
                    <!--						</tr>-->
                    <!--						<tr class="bottom-border">-->
                    <!--							<td class="padding-all-10">473</td>-->
                    <!--							<td class="padding-all-10">JFC_Coord201</td>-->
                    <!--							<td class="padding-all-10">Metro Manila</td>-->
                    <!--						</tr>-->
                    <!--						<tr class="bottom-border">-->
                    <!--							<td class="padding-all-10">473</td>-->
                    <!--							<td class="padding-all-10">JFC_Coord201</td>-->
                    <!--							<td class="padding-all-10">Metro Manila</td>-->
                    <!--						</tr>-->
                    <!--						<tr class="bottom-border">-->
                    <!--							<td class="padding-all-10">473</td>-->
                    <!--							<td class="padding-all-10">JFC_Coord201</td>-->
                    <!--							<td class="padding-all-10">Metro Manila</td>-->
                    <!--						</tr>-->
                    <!--						<tr class="bottom-border">-->
                    <!--							<td class="padding-all-10">473</td>-->
                    <!--							<td class="padding-all-10">JFC_Coord201</td>-->
                    <!--							<td class="padding-all-10">Metro Manila</td>-->
                    <!--						</tr>-->
                </tbody>
            </table>
        </div>
    </div>
</section>
<!--Agent List-->
	
	
	
	
	