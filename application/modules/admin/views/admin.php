<script type="text/javascript"></script>
<style> /*temporary will be transferred to cutomer custom css*/

    div.template {
        display: none;
    }

    .template {
        display: none;
    }

    tr.template {
        display: none;
    }

    tr.unavailable {
        border-bottom:1pt solid red;
        border-top:1pt solid red;
        color: red;
    }

    div.success-msg {
        background-color: #00cc00;
        color: #fff;
        font-size: 18px;
        padding: 5px;
        text-align: center;
        width: 100%;
    }

    div.address-success-msg {
        background-color: #00cc00;
        color: #fff;
        font-size: 14px;
        padding: 5px;
        text-align: center;
        width: 100%;
    }

    p.info_address_complete {
        max-width: 225px;
    }

    p.default_address_complete {
        max-width: 225px;
    }

    p.address_complete {
        max-width: 225px;
    }

    p.error_messages {
        text-align: left;
        margin-left: 25px;
        font-size: 12px !important;
    }

    .update-customer-error-msg {
        text-align: left;
        font-size: 12px !important;
    }

    .customer_secondary_address .customer_address:hover {
        background-color: #efefed !important;
    }

    .found-retail {
        display: none;
    }

    .show_map {
        cursor: pointer;
    }

    .paganation {
        width: 100%;
        margin: 10px 0;
    }

    div.has-error {
        border: 1px solid red;
        border-radius: 4px;
    }

    /*pagination*/
    .paginations {
        padding: 0;
        margin-bottom: 80px !important;
    }

    #paginations > li {
        list-style: none;
        color: white;
        float: left;
        background: #888888;
        margin-right: 2px;
        padding: 10px 15px;
        transition: background .5s linear;
        text-align: center;
        font-size: 15px;
        margin-top: 5px;

    }

    #pagination-3 > li {
        font-size: 15px;
    }

    #pagination-3 > li:hover , #paginations > li:hover {
        cursor: pointer;
        background: #6D0D03;

    }

    #pagination-3 > li.page:first, #paginations > li.page:first {
        margin-top:50px;
    }

    #pagination-3 > li.page-active, #paginations > li.page-active {
        background: #9b3126;
    }

    .page-none {
        display: none;
    }

    .records {
        padding: 10px;
        background: #DFE4DF;
        border-bottom: 1px solid #ccc;
    }

    .records:first-child {
        margin-top: 40px;
    }

    .template {
        display: none;
    }

    header-group.header, tr.store_route {
        cursor: pointer;
    }

    button-group {
        width: 0;
        height: 0;
        clear: both;
    }

    a {
        cursor: pointer;
    }

    div.store-msg-notify {
        padding-top: 2px;
        width: 20px;
        height: 20px;
        font-size: 12px;
        border-radius: 50%;
        background-color: #D65D51;
        color: #FFF;
        text-align: center;
        z-index: 999;
        display: inline;
        float: right;
    }

    table.table_store_messenger_container td.store-selected {
        background-color: #FBD8A1;
    }

    table.table_store_messenger_container tr {
        cursor : pointer;
    }
    .datepicker-days td.day.disabled{
        color: #999999;
    }
    .datepicker-months span.month.disabled{
        color: #999999;
    }
    .datepicker-years span.year.disabled{
        color: #999999;
    }
    .datepicker-days td.day.today{
        font-weight: bold;
    }

    input[datavalid].error {
        border-color : red !important;
    }

    .tbl table thead th {
        color: #fff;
        padding: 5px 10px;
    }

    textarea.error{
        border-color : red !important;
    }

    .tbl table tbody tr td {
        /*padding: 0 !important;*/
        font-weight: 500;
    }

    .datepicker .datepicker-months tbody .month {
        display: inline-block;
        text-align: center;
        width: 25%;
        padding: 10px;
    }

    .select input[datavalid].error {
        border: solid 1.5px red !important;
    }

    /*prod availability report pagination*/
    .prod-avail-prod-avail-custom-pagination1 {
        display: inline-block ;
        padding-left: 0 ;
        margin: 20px 0 ;
        border-radius: 4px ;
    }
    .prod-avail-custom-pagination1 > li {
        display: inline ;
    }
    .prod-avail-custom-pagination1 > li > a,
    .prod-avail-custom-pagination1 > li > span {
        position: relative ;
        float: left ;
        padding: 6px 12px ;
        margin-left: -1px ;
        line-height: 1.42857143 ;
        color: #fff ;
        text-decoration: none ;
        background-color: #aaa ;
        border: 1px solid #ddd ;
    }
    .prod-avail-custom-pagination1 > li:first-child > a,
    .prod-avail-custom-pagination1 > li:first-child > span {
        margin-left: 0 ;
        border-top-left-radius: 4px ;
        border-bottom-left-radius: 4px ;
    }
    .prod-avail-custom-pagination1 > li:last-child > a,
    .prod-avail-custom-pagination1 > li:last-child > span {
        border-top-right-radius: 4px ;
        border-bottom-right-radius: 4px ;
    }
    .prod-avail-custom-pagination1 > li > a:hover,
    .prod-avail-custom-pagination1 > li > span:hover,
    .prod-avail-custom-pagination1 > li > a:focus,
    .prod-avail-custom-pagination1 > li > span:focus {
        color: #fff ;
        background-color: #9B3126 ;
        border-color: #9B3126 ;
    }
    .prod-avail-custom-pagination1 > .active > a,
    .prod-avail-custom-pagination1 > .active > span,
    .prod-avail-custom-pagination1 > .active > a:hover,
    .prod-avail-custom-pagination1 > .active > span:hover,
    .prod-avail-custom-pagination1 > .active > a:focus,
    .prod-avail-custom-pagination1 > .active > span:focus {
        z-index: 2 ;
        color: #fff ;
        cursor: default ;
        background-color: #9B3126 ;
        border-color: #9B3126 ;
    }
    .prod-avail-custom-pagination1 > .disabled > span,
    .prod-avail-custom-pagination1 > .disabled > span:hover,
    .prod-avail-custom-pagination1 > .disabled > span:focus,
    .prod-avail-custom-pagination1 > .disabled > a,
    .prod-avail-custom-pagination1 > .disabled > a:hover,
    .prod-avail-custom-pagination1 > .disabled > a:focus {
        color: #777 ;
        cursor: not-allowed ;
        background-color: #aaa ;
        border-color: #ddd ;
    }
</style>

<!--HEADERS-->
<section header-content="agent_list" id="add_new_agent_header" class="container-fluid add_new_agent headers" section-style="top-panel">

    <div class="row agent agent_list header-container">
        <div class="contents">
            <h1 class="f-left content">Users List</h1>
            <div class="f-right margin-top-20">
                <!--<button class="btn btn-dark margin-right-10">View User Archived</button>-->
                <button class="btn btn-dark margin-right-10 add_agents">Add Users</button>
                <!--<button class="btn btn-dark margin-right-10 notify_agents">Notify All Offline Agents</button>-->
               <!-- <button class="btn btn-dark">FAQ</button>-->
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <div class="row">
        <div class="contents margin-top-20">
            <!-- search -->
            <div class="f-left">
                <label class="margin-bottom-5">search:</label><br>
                <input class="search f-left large" type="text" name="agent_search">
            </div>
            <!-- search by -->
            <div class="f-left margin-left-20">
                <label class="margin-bottom-5">search by:</label><br>

                <div class="select">
                    <div class="frm-custom-dropdown font-0">
                        <div class="frm-custom-dropdown-txt">
                            <input type="text" class="dd-txt" name="agent_search_by" value="Username" autocomplete="off">
                        </div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option" style="display: none;">
                            <div class="option agent-filter" data-value="Username">Username</div>
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                        <option value="Contact-Num">Contact Number</option>
                    </select>
                </div>
            </div>
            <!-- province -->
            <div class="f-left margin-left-20">
                <label class="margin-bottom-5">Province</label><br>

                <div class="select provinces-agent-list">
                    <div class="frm-custom-dropdown font-0">
                        <div class="frm-custom-dropdown-txt">
                            <input type="text" class="dd-txt" name="agent_province" value="All Province" autocomplete="off" int-value="0">
                        </div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option" style="display: none;">
                            <div class="option province-select" data-value="0">All Province</div>
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                        <option value="All-Province">All Province</option>
                    </select>
                </div>
            </div>
            <button class="f-left btn btn-dark margin-top-20 margin-left-20 search_agent">Search<i class="fa fa-spinner fa-pulse hidden"></i></button>
            <div class="clear"></div>
        </div>
    </div>

    <div class="row margin-top-20">
        <div class="contents line">
            <div class="select">
                <div class="frm-custom-dropdown font-0">
                    <div class="frm-custom-dropdown-txt">
                        <input type="text" class="dd-txt" name="agent_status" value="Show All Agents" autocomplete="off">
                    </div>
                    <div class="frm-custom-icon"></div>
                    <div class="frm-custom-dropdown-option" style="display: none;">
                        <div class="option" data-value="Show All Agents">Show All Agents</div>
                        <div class="option" data-value="Show Offline Agents">Show Offline Agents</div>
                        <div class="option" data-value="Show Online Agents">Show Online Agents</div>
                    </div>
                </div>
                <select class="frm-custom-dropdown-origin" style="display: none;">
                    <option value="Offline-Agents">Show All Agents</option>
                </select>
            </div>
            <span class="white-space"></span>

            <div class="select callcenters">
                <div class="frm-custom-dropdown font-0">
                    <div class="frm-custom-dropdown-txt">
                        <input type="text" class="dd-txt" name="agent_callcenter" value="All Callcenters" autocomplete="off">
                    </div>
                    <div class="frm-custom-icon"></div>
                    <div class="frm-custom-dropdown-option" style="display: none;">
                        <div class="option callcenter-select" data-value="0">All Callcenters</div>
                    </div>
                </div>
                <select class="frm-custom-dropdown-origin" style="display: none;">
                    <option value="All Callcenters">All Callcenters</option>
                </select>
            </div>
            <span class="white-space"></span>

            <div class="f-right bggray-white sort-container">
                <p class="f-left font-12 padding-left-10 padding-top-5">
                    <strong>Sort By:</strong>
                </p>
                <p class="f-left font-12 padding-left-5 padding-top-5">
                    <a class="active" href="#">
                        <strong class="sort asc" sort="data-agent-name">Username</strong>
                        <img class="asc hidden" src="<?php echo assets_url() ?>images/ui/sort-top-arrow.png">
                        <img class="desc hidden" src="<?php echo assets_url() ?>images/ui/sort-down-arrow.png" width="16" height="9">
                    </a>
                </p>
                <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                <p class="f-left font-12 padding-top-5">
                    <a class="active" href="#">
                        <strong class="sort" sort="data-agent-id">User ID</strong>
                        <img class="asc hidden" src="<?php echo assets_url() ?>images/ui/sort-top-arrow.png">
                        <img class="desc hidden" src="<?php echo assets_url() ?>images/ui/sort-down-arrow.png" width="16" height="9">
                        <!--                        <img src="http://localhost/store/assets/images/ui/sort-top-arrow.png">-->
                    </a>
                </p>
                <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                <p class="f-left font-12 padding-left-5 padding-top-5">
                    <a class="active" href="#">
                        <strong class="sort asc" sort="data-callcenter">Call Center</strong>
                        <img class="asc hidden" src="<?php echo assets_url() ?>images/ui/sort-top-arrow.png">
                        <img class="desc hidden" src="<?php echo assets_url() ?>images/ui/sort-down-arrow.png" width="16" height="9">
                    </a>
                </p>

            </div>
            <div class="clear"></div>
        </div>
    </div>

</section>

<section header-content="archive_agent_list" id="add_new_agent_header" class="container-fluid hidden add_new_agent headers" section-style="top-panel">

    <div  class="row agent archived_agent_list header-container">

        <div class="contents">
            <!--<div class="breadcrumbs f-left  margin-top-20">
                <p><a href="#">Agents List</a> <i class="fa fa-angle-right"></i> <strong>Archived Agents Lists</strong></p>
            </div>-->
            <div class="clear"></div>
            <h1 class="f-left">Archived Users List</h1>
            <div class="f-right margin-top-20">

                <button class="btn btn-dark margin-right-10 ">Back to List</button>
<!--                <button class="btn btn-dark">FAQ</button>-->
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <div class="row">
        <div class="contents margin-top-20">
            <!-- search -->
            <div class="f-left">
                <label class="margin-bottom-5">search:</label><br>
                <input class="search f-left large" type="text">
            </div>
            <!-- search by -->
            <div class="f-left margin-left-20">
                <label class="margin-bottom-5">search by:</label><br>
                <div class="select ">
                    <select>
                        <option value="Contact-Num">Contact Number</option>
                    </select>
                </div>
            </div>
            <!-- province -->
            <div class="f-left margin-left-20">
                <label class="margin-bottom-5">Province</label><br>
                <div class="select ">
                    <select>
                        <option value="All-Province">All Province</option>
                    </select>
                </div>
            </div>
            <button class="f-left btn btn-dark margin-top-20 margin-left-20">Search</button>
            <div class="clear"></div>
        </div>
    </div>

    <div class="row margin-top-20">
        <div class="contents line">
            <div class="select">
                <select>
                    <option value="Offline-Agents">Show Offline Users</option>
                </select>
            </div>
            <span class="white-space"></span>
            <div class="select ">
                <select>
                    <option value="Offline-Agents">PHUB Agents</option>
                </select>
            </div>
            <span class="white-space"></span>

            <div class="f-right bggray-white">
                <p class="f-right margin-top-5 bggray-white font-14">
                    <strong>Sort By: Store Name | Names | <span class="red-color">Telco Provider
                        <img src="<?php echo assets_url() ?>images/ui/sort-top-arrow.png"></span>
                    </strong>
                </p>
            </div>
            <div class="clear"></div>
        </div>
    </div>

</section>

<section header-content="data_manager_list" id="add_new_agent_header" class="container-fluid hidden add_new_agent headers" section-style="top-panel">

    <div  class="row agent data_manager_list header-container">
        <div class="contents">
            <h1 class="f-left">Data Manager List</h1>
            <div class="f-right margin-top-20">
                <button class="btn btn-dark margin-right-20">View User Archived</button>
                <button class="btn btn-dark margin-right-20 ">Add Data Manager</button>
<!--                <button class="btn btn-dark">FAQ</button>-->
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <div class="row">
        <div class="contents margin-top-20">
            <!-- search -->
            <div class="f-left">
                <label class="margin-bottom-5">search:</label><br>
                <input class="search f-left large" type="text">
            </div>
            <!-- search by -->
            <div class="f-left margin-left-20">
                <label class="margin-bottom-5">search by:</label><br>
                <div class="select ">
                    <select>
                        <option value="Contact-Num">Contact Number</option>
                    </select>
                </div>
            </div>
            <!-- province -->
            <div class="f-left margin-left-20">
                <label class="margin-bottom-5">Province</label><br>
                <div class="select ">
                    <select>
                        <option value="All-Province">All Province</option>
                    </select>
                </div>
            </div>
            <button class="f-left btn btn-dark margin-top-20 margin-left-20">Search</button>
            <div class="clear"></div>
        </div>
    </div>

    <div class="row margin-top-20">
        <div class="contents line">
            <div class="select">
                <select>
                    <option value="Offline-Agents">Show Offline Users</option>
                </select>
            </div>
            <span class="white-space"></span>
            <div class="select ">
                <select>
                    <option value="Offline-Agents">PHUB Agents</option>
                </select>
            </div>
            <span class="white-space"></span>

            <div class="f-right bggray-white">
                <p class="f-right margin-top-5 bggray-white font-14">
                    <strong>Sort By: Store Name | Names | <span class="red-color">Telco Provider
                        <img src="<?php echo assets_url() ?>images/ui/sort-top-arrow.png"></span>
                    </strong>
                </p>
            </div>
            <div class="clear"></div>
        </div>
    </div>

</section>

<section header-content="store_management_list" id="store_management" class="container-fluid hidden" section-style="top-panel">

    <!-- search order -->
    <div class="row header-container">
        <div class="contents">
            <h1 class="f-left">Store Management</h1>
            <div class="f-right margin-top-20">
                <!--<button class="btn btn-dark margin-right-20">View Store Archives</button>-->
                <button class="btn btn-dark margin-right-20 add_stores">Add Stores</button>
                <!--<button class="btn btn-dark margin-right-20 notify_stores">Notify All Offline Stores</button>-->
<!--                <button class="btn btn-dark">FAQ</button>-->
            </div>
            <div class="clear"></div>
        </div>
    </div>


    <div class="row">
        <div class="contents margin-top-20">
            <!-- search -->
            <div class="f-left">
                <label class="margin-bottom-5">search:</label><br>
                <input class="search f-left large" type="text" name="store_search">
            </div>
            <!-- search by -->
            <div class="f-left margin-left-20">
                <label class="margin-bottom-5">search by:</label><br>

                <div class="select">
                    <div class="frm-custom-dropdown font-0">
                        <div class="frm-custom-dropdown-txt">
                            <input type="text" class="dd-txt" name="store_search_by" value="Store Name" autocomplete="off">
                        </div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option" style="display: none;">
                            <div class="option store-filter" data-value="Store Name">Store Name</div>
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                        <option value="Contact-Num">Contact Number</option>
                    </select>
                </div>
            </div>
            <!-- province -->
            <div class="f-left margin-left-20">
                <label class="margin-bottom-5">Province</label><br>

                <div class="select provinces-store-list">
                    <div class="frm-custom-dropdown font-0">
                        <div class="frm-custom-dropdown-txt">
                            <input type="text" class="dd-txt" name="store_province" value="All Province" autocomplete="off" int-value="0">
                        </div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option" style="display: none;">
                            <div class="option province-select" data-value="0">All Province</div>
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                        <option value="All-Province">All Province</option>
                    </select>
                </div>
            </div>
            <button class="f-left btn btn-dark margin-top-20 margin-left-20 search_store">Search<i class="fa fa-spinner fa-pulse hidden"></i></button>
            <div class="clear"></div>
        </div>
    </div>

    <div class="row margin-top-20">
        <div class="contents line">
            <div class="select xlarge">
                <div class="frm-custom-dropdown-option" style="display: none;">
                    <div class="option" data-value="Show All Store">Show All Store</div>
                    <div class="option" data-value="Show Offline Store">Show Offline Store</div>
                    <div class="option" data-value="Show Online Store">Show Online Store</div>
                </div>
            </div>


            <div class="f-right bggray-white sort-container">
                <p class="f-left font-12 padding-left-10 padding-top-5">
                    <strong>Sort By:</strong>
                </p>
                <p class="f-left font-12 padding-left-5 padding-top-5">
                    <a class="active" href="#">
                        <strong class="sort asc" sort="data-store-name">Store Name</strong>
                        <img class="asc hidden" src="<?php echo assets_url() ?>images/ui/sort-top-arrow.png">
                        <img class="desc hidden" src="<?php echo assets_url() ?>images/ui/sort-down-arrow.png" width="16" height="9">
                    </a>
                </p>

                <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                <p class="f-left font-12 padding-top-5">
                    <a class="active" href="#">
                        <strong class="sort" sort="data-store-id">Store ID</strong>
                        <img class="asc hidden" src="<?php echo assets_url() ?>images/ui/sort-top-arrow.png">
                        <img class="desc hidden" src="<?php echo assets_url() ?>images/ui/sort-down-arrow.png" width="16" height="9">
                        <!--                        <img src="http://localhost/store/assets/images/ui/sort-top-arrow.png">-->
                    </a>
                </p>
                <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                <p class="f-left font-12 padding-left-5 padding-top-5">
                    <a class="active" href="#">
                        <strong class="sort asc" sort="data-store-province">Province</strong>
                        <img class="asc hidden" src="<?php echo assets_url() ?>images/ui/sort-top-arrow.png">
                        <img class="desc hidden" src="<?php echo assets_url() ?>images/ui/sort-down-arrow.png" width="16" height="9">
                    </a>
                </p>

            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>

<section header-content="archive_store_list" class="container-fluid hidden" section-style="top-panel">

    <!-- search order -->
    <div class="row header-container">

        <div class="contents">
            <div class="breadcrumbs f-left  margin-top-20">
                <p><a href="#">Store List</a> <i class="fa fa-angle-right"></i> <strong>Archived Store Lists</strong></p>
            </div>
            <div class="clear"></div>
            <h1 class="f-left">Archived Store List</h1>
            <div class="f-right margin-top-20">


                <button class="btn btn-dark margin-right-10 ">Back To List</button>
<!--                <button class="btn btn-dark">FAQ</button>-->
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="row">
        <div class="contents margin-top-20">
            <!-- search -->
            <div class="f-left">
                <label class="margin-bottom-5">search:</label><br>
                <input class="search f-left large" type="text">
            </div>
            <!-- search by -->
            <div class="f-left margin-left-20">
                <label class="margin-bottom-5">search by:</label><br>
                <div class="select ">
                    <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt"></div><div class="frm-custom-icon"></div><div class="frm-custom-dropdown-option"><div class="option" data-value="Contact-Num">Store Name</div></div></div><select class="frm-custom-dropdown-origin" style="display: none;">
                        <option value="Contact-Num">Store Name</option>
                    </select>
                </div>
            </div>
            <!-- province -->
            <div class="f-left margin-left-20">
                <label class="margin-bottom-5">Province</label><br>
                <div class="select ">
                    <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt"></div><div class="frm-custom-icon"></div><div class="frm-custom-dropdown-option"><div class="option" data-value="All-Province">All Province</div></div></div><select class="frm-custom-dropdown-origin" style="display: none;">
                        <option value="All-Province">All Province</option>
                    </select>
                </div>
            </div>
            <button class="f-left btn btn-dark margin-top-20 margin-left-20">Search</button>
            <div class="clear"></div>
        </div>
    </div>

    <div class="row margin-top-20">
        <div class="contents line">


            <div class="f-right bggray-white">
                <p class="f-right margin-top-0 margin-left-5 bggray-white font-14">
                    <strong>Sort By: Sotre Name | Province | <span class="red-color">Date Archived
                        <img src="<?php echo assets_url() ?>/images/ui/sort-top-arrow.png"></span>
                    </strong>
                </p>
            </div>
            <div class="clear"></div>
        </div>
    </div>

</section>

<section header-content="messaging" id="messaging" class="container-fluid hidden" section-style="top-panel">


    <div class="row header-container margin-bottom-20">
        <div class="contents">
            <h1 class="f-left">Messaging</h1>
            <div class="f-right margin-top-20">
<!--                <button class="btn btn-dark ">FAQ</button>-->
            </div>
            <div class="clear"></div>
        </div>
    </div>

</section>

<section header-content="store_operations" id="store_operations" class="container-fluid hidden" section-style="top-panel">
    <div class="row header-container margin-bottom-20">
        <div class="contents">
            <h1 class="f-left">Store Operation</h1>
            <div class="f-right margin-top-20">
<!--                <button class="btn btn-dark ">FAQ</button>-->
            </div>
        </div>
    </div>
    <div class="clear"></div>
</section>

<section header-content="promos" id="promos" class="container-fluid hidden" section-style="top-panel">
    <div class="row header-container">
        <div class="contents">
            <h1 class="f-left">Upsell & Promos</h1>
            <div class="f-right margin-top-20">
<!--                <button class="btn btn-dark ">FAQ</button>-->
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>

<section header-content="upsell" id="upsell" class="container-fluid hidden" section-style="top-panel">
    <div class="row header-container">
        <div class="contents">
            <h1 class="f-left">Upsell</h1>
            <div class="f-right margin-top-20">
<!--                <button class="btn btn-dark ">FAQ</button>-->
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>

<section header-content="contact"  class="container-fluid hidden" section-style="top-panel">
    <div class="row header-container">
        <div class="contents">
            <h1 class="f-left">Contact Number Reports</h1>
            <div class="f-right margin-top-20">
                <button class="btn btn-dark margin-right-20 cn_report_xls_btn">Download Excel File <i class="fa fa-spinner fa-pulse hidden"></i> </button>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="row">
        <div class="contents margin-top-20">

            <div class="display-inline-mid cn_generate_by_date">
                <!-- date from -->
                <div class="f-left margin-right-10 ">
                    <label class="margin-bottom-5">Date From:</label><br>
                    <div class="date-picker" id="cn-report-date-from">
                        <input type="text" name="cn_report_date_from">
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                </div>
                <!-- date from -->
                <div class="f-left">
                    <label class="margin-bottom-5">Date To:</label><br>
                    <div class="date-picker" id="cn-report-date-to">
                        <input type="text" name="cn_report_date_to">
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                </div>
                <div class="f-left margin-left-10">
                    <label class="margin-bottom-5">Contact Type:</label><br>

                    <div class="select type-contact-num-report">
                        <div class="frm-custom-dropdown font-0">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" class="dd-txt" name="cn_reports_type" value="Mobile" autocomplete="off" data-value="mobile" readonly>
                            </div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option" style="display: none;">
                                <div class="option type-select" data-value="mobile">Mobile</div>
                                <div class="option type-select" data-value="landline">Landline</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="f-left margin-left-10">
                    <label class="margin-bottom-5">Province:</label><br>

                    <div class="select small provinces-contact-num-report">
                        <div class="frm-custom-dropdown font-0">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" class="dd-txt" name="cn_reports_province" value="" autocomplete="off" int-value="0" readonly value="Show All Provinces">
                            </div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option" style="display: none;">
                                <div class="option province-select" data-value="0">Show All Provinces</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="All-Province">All Province</option>
                        </select>
                    </div>
                </div>
                <!-- generate report -->
                <button type="button" class="btn btn-dark / margin-left-20 margin-top-20 cn_generate_by_date"><i class="fa fa-spinner fa-pulse hidden"></i>Generate Report</button>
                <div class="clear"></div>
            </div>
            <div class="margin-top-20">
                <hr>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="contents text-center margin-top-100 no_generate_report">
            <p class="font-50"><img alt="report image" src="<?php echo base_url();?>assets/images/report_1_.svg"></p>
            <h4 class="gray-color margin-top-30 font-20 error_message">No results found.</h4>
        </div>
    </div>
</section>

<section header-content="repeat" id="repeat"  class="container-fluid hidden" section-style="top-panel">
    <div class="row header-container">
        <div class="contents">
            <h1 class="f-left">Repeating Customer Reports</h1>
            <div class="f-right margin-top-20">
                <button class="btn btn-dark margin-right-20 repeat_xls_btn">Download Excel File <i class="fa fa-spinner fa-pulse hidden"></i> </button>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="row">
        <div class="contents margin-top-20">

            <div class="display-inline-mid cn_generate_by_date">
                <!-- date from -->
                <div class="f-left margin-right-10 ">
                    <label class="margin-bottom-5">Date From:</label><br>
                    <div class="date-picker" id="repeat-date-from">
                        <input type="text" name="repeat_date_from">
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                </div>
                <!-- date from -->
                <div class="f-left">
                    <label class="margin-bottom-5">Date To:</label><br>
                    <div class="date-picker" id="repeat-date-to">
                        <input type="text" name="repeat_date_to">
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                </div>
                <div class="f-left margin-left-20">
                    <label class="margin-bottom-5">Province:</label><br>

                    <div class="select provinces-repeating-customer-report">
                        <div class="frm-custom-dropdown font-0">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" class="dd-txt" name="repeat_date_province" value="All Province" autocomplete="off" int-value="0" readonly>
                            </div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option" style="display: none;">
                                <div class="option province-select" data-value="0">All Province</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="All-Province">All Province</option>
                        </select>
                    </div>
                </div>

                <!-- generate report -->
                <button type="button" class="btn btn-dark / margin-left-20 margin-top-20 repeat_generate_report"><i class="fa fa-spinner fa-pulse hidden"></i>Generate Report</button>
                <div class="clear"></div>
            </div>
            <div class="margin-top-20">
                <hr>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="contents text-center margin-top-100 no_generate_report">
            <p class="font-50"><img alt="report image" src="<?php echo base_url();?>assets/images/report_1_.svg"></p>
            <h4 class="gray-color margin-top-30 font-20 error_message">No results found.</h4>
        </div>
    </div>
</section>

<section header-content="security" id="security" class="container-fluid hidden" section-style="top-panel">
    <div class="row header-container">
        <div class="contents">
            <h1 class="f-left">Security</h1>
            <div class="f-right margin-top-20">
<!--                <button class="btn btn-dark ">FAQ</button>-->
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>

<section header-content="audit_trails" id="audit_trails" class="container-fluid hidden" section-style="top-panel">
    <div class="row header-container">
        <div class="contents">
            <h1 class="f-left">Audit Trails</h1>
            <div class="f-right margin-top-20">
<!--                <button class="btn btn-dark ">FAQ</button>-->
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>

<section header-content="reports" id="reports" class="container-fluid header hidden" section-style="top-panel">
    <div class="row header-container">
        <div class="contents">
            <h1 class="f-left" data-label="report_header">Order Reports</h1>
            <div class="f-right margin-top-20">
                <button class="btn btn-dark margin-right-10 download_excel" disabled>Download Excel File</button>
                <!-- <button class="btn btn-dark ">FAQ</button> -->
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="row">
        <div class="contents margin-top-20">

            <div class="display-inline-mid generate_by_date width-100per">
                <!-- date from -->
                <div class="f-left margin-right-10 ">
                    <label class="margin-bottom-5">Date From:</label><br>
                    <div class="date-picker" id="order_reports_date_from">
                        <input type="text" name="order_reports_date_from" readonly>
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                </div>
                <!-- date from -->
                <div class="f-left">
                    <label class="margin-bottom-5">Date To:</label><br>
                    <div class="date-picker" id="order_reports_date_to">
                        <input type="text" name="order_reports_date_to" readonly>
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                </div>

                <div class="f-left select medium order_reports margin-left-10 hidden">
                    <label class="margin-bottom-5">Order Type:</label><br>
                    <div class="frm-custom-dropdown">
                        <div class="frm-custom-dropdown-txt">
                            <input type="text" class="dd-txt" data-value="1" name="order_type" value="Rerouted Orders" readonly>
                        </div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option">
                            <div class="option" data-value="1">Rerouted Orders</div>
                            <div class="option" data-value="2">Manually Relayed Orders</div>
                            <div class="option" data-value="3">Rejected Orders</div>
                            <div class="option" data-value="4">Follow Up Orders</div>
                            <div class="option" data-value="5">NKAG Orders</div>
                            <!--<div class="option" data-value="Monthly">Monthly</div>-->
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                        <option value="Daily">Daily</option>
                        <option value="Weekly">Weekly</option>
                        <option value="Monthly">Monthly</option>
                    </select>
                </div>

                <div class="f-left margin-left-10">
                    <label class="margin-bottom-5">Province:</label><br>
                    <div class="select select-filter-provinces" is-dirty="true">
                        <div class="frm-custom-dropdown font-0">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" class="dd-txt" data-value="0" name="province_id" value="Show All Provinces" autocomplete="off" readonly="">
                            </div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option option-visible" style="display: block;">
                                <div class="option custom-filter" data-value="0">Show All Provinces</div>

                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option data-value="Show All Provinces" selected="">Show All Provinces</option>
                        </select>
                    </div>
                </div>

                <!-- <div class="f-left select order_reports select_provinces province-select-order-reports margin-left-10">
                    <label class="margin-bottom-5">Province:</label><br>
                    <div class="frm-custom-dropdown">
                        <div class="frm-custom-dropdown-txt">
                            <input type="text" class="dd-txt" name="province_id" value="Show All Provinces" readonly data-value="0">
                        </div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option">
                            <div class="option" data-value="0">All Provinces</div>
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                        <option data-value="Show All Provinces" selected="">Show All Provinces</option>
                    </select>
                </div> -->

                <!-- filter by callcenters-->
                <div class="f-left margin-left-10">
                    <label class="margin-bottom-5">Callcenter:</label><br>
                    <div class="select callcenter-selection-product-sales-report margin-right-10">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt"><input type="text" int-value="0" class="dd-txt" name="callcenter_select" value="Show All Callcenter"></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option">
                                <div class="option" int-value="0">Show All Callcenter</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                        </select>
                    </div>
                </div>

                <!-- filter by transactions-->
                <div class="f-left margin-top-20 transaction-type-dropdown">
                    <label class="margin-bottom-5">Type:</label><br>
                    <div class="select transaction-selection-order-report margin-right-10">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt"><input type="text" data-value="All Transaction" class="dd-txt" name="transaction_select" value="Show All Transactions"></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option">
                                <div class="option" data-value="All Transaction">All Transaction</div>
                                <div class="option" data-value="Store Channel">Call Order</div>
                                <div class="option" data-value="Web">Web Order</div>
                                <div class="option" data-value="SMS">SMS Order</div>
                                <div class="option" data-value="nkag">NKAG Order</div>
                                <div class="option" data-value="Food Panda">Food Panda</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                        </select>
                    </div>
                </div>

                <!-- filter by store-->
                <div class="f-left margin-top-20">
                    <label class="margin-bottom-5">Store:</label><br>
                    <div class="select store-selection-product-sales-report margin-right-10">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt"><input type="text" int-value="0" class="dd-txt" name="store_select" value="Show All Store"></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option">
                               <div class="option" int-value="0">Show All Store</div> 
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                        </select>
                    </div>
                </div>

                <!-- generate report -->
                <button type="button" class="btn btn-dark / margin-left-20 margin-top-40 generate_by_date"><i class="fa fa-spinner fa-pulse hidden"></i>Generate Report</button>
                <div class="clear"></div>
            </div>

            <div class="margin-top-20">
                <hr>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="contents text-center margin-top-100 no_generate_report">
            <p class="font-50"><img alt="report image" src="<?php echo base_url();?>assets/images/report_1_.svg"></p>
            <h4 class="gray-color margin-top-30 font-20">Please select date range <br>to generate report </h4>
        </div>

        <!--<div class="contents margin-top-20 show_generate_report">
            <div class="rounded-container / f-left / margin-right-20 / padding-left-15">
                <div class="rounded-box / xmedium">
                    <p class="font-16 margin-top-10 / padding-top-10">Date</p>
                    <br>
                    <p class="font-16 / margin-left-20 / margin-right-20 " data-label='date_from'>May 1, 2015 to</p>
                    <br>
                    <p class="font-16 / margin-left-20 / margin-right-20 / padding-bottom-10" data-label='date_to'>May 18, 2015</p>
                </div>
            </div>
            <div class="rounded-container / f-left / margin-right-20">
                <div class="rounded-box / xmedium">
                    <p class="font-16 margin-top-10 / padding-top-10">Total Sales</p>
                    <br>
                    <p class="font-20 / margin-top-10 / padding-bottom-15 " data-label='total_sales'>501,487.00 PHP</p>
                </div>
            </div>
            <div class="rounded-container / f-left / margin-right-20">
                <div class="rounded-box / xmedium">
                    <p class="font-16 margin-top-10 / padding-top-10">Food Sales</p>
                    <br>
                    <p class="font-20 / margin-top-10 / padding-bottom-15" data-label='food_sales'>501,487.00 PHP</p>
                </div>
            </div>
            <div class="rounded-container / f-left / ">
                <div class="rounded-box / xmedium">
                    <p class="font-16 margin-top-10 / padding-top-10">Transaction Count</p>
                    <br>
                    <p class="font-20 / margin-top-10 / padding-bottom-15" data-label='transaction_count'>4,578</p>
                </div>
            </div>
        </div>-->
    </div>
</section>

<section header-content="prod_unavailability_reports" class="container-fluid header hidden" section-style="top-panel" >
    <div class="row header-container">
        <div class="contents">
            <h1 class="f-left">Product Availability Reports</h1>
            <div class="f-right margin-top-20">
                <button class="btn btn-dark margin-right-10 pu_report_xls_button" disabled>Download Excel File</button>
                <!-- <button class="btn btn-dark ">FAQ</button> -->
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="row">
        <div class="contents margin-top-20">

            <div class="display-inline-mid pu_generate_by_date">
                <!-- date from -->
                <div class="f-left margin-right-10 ">
                    <label class="margin-bottom-5">Date From:</label><br>
                    <div class="date-picker" id="pu-report-date-from">
                        <input type="text" name="pu_report_date_from" readonly>
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                </div>
                <!-- date from -->
                <div class="f-left margin-right-10 ">
                    <label class="margin-bottom-5">Date To:</label><br>
                    <div class="date-picker" id="pu-report-date-to">
                        <input type="text" name="pu_report_date_to" readonly>
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                </div>
                <div class="f-left margin-right-10">
                    <label class="margin-bottom-5">Province:</label><br>
                    <div class="select pu_province_selection">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt"><input type="text" int-value="0" class="dd-txt" name="pu_provinces" value="Show All Provinces" readonly></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option">
                                <div class="option province_select" int-value="0">Show All Provinces</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="f-left margin-right-10 ">
                    <label class="margin-bottom-5">Store:</label><br>
                    <div class="select pu_store_selection">
                        <div class="frm-custom-dropdown font-0">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" class="dd-txt" name="pu_stores" value="Show All Stores" autocomplete="off" int-value="0" readonly>
                            </div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option" style="display: none;">
                                <div class="option store_select" int-value="0">Show All Stores</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="Show All Stores" selected="">Show All Stores</option>
                        </select>
                    </div>
                </div>
                <div class="f-left">
                    <!-- generate report -->
                    <button type="button" class="btn btn-dark / margin-top-20 pu_generate_by_date"><i class="fa fa-spinner fa-pulse hidden"></i> Generate Report</button>
                </div>
                <div class="clear"></div>
            </div>


            <!-- generate by period -->
            <!--             <div class="display-inline-mid pu_generate_by_period">
                            <section class="container-fluid" section-style="content-panel">
                                <div class="divider padding-left-20">
                                    <label>Generate by Period:</label><br>

                                    <div class="select small select_period">
                                        <div class="frm-custom-dropdown">
                                            <div class="frm-custom-dropdown-txt">
                                                <input type="text" class="dd-txt" name="period" value="Daily" readonly>
                                            </div>
                                            <div class="frm-custom-icon"></div>
                                            <div class="frm-custom-dropdown-option">
                                                <div class="option" data-value="Daily">Daily</div>
                                                <div class="option" data-value="Weekly">Weekly</div>
                                                <div class="option" data-value="Monthly">Monthly</div>
                                            </div>
                                        </div>
                                        <select class="frm-custom-dropdown-origin" style="display: none;">
                                            <option value="Daily">Daily</option>
                                            <option value="Weekly">Weekly</option>
                                            <option value="Monthly">Monthly</option>
                                        </select>
                                    </div>

                                    <button type="button" class="btn / btn-dark / margin-left-20 pu_generate_by_period"><i class="fa fa-spinner fa-pulse hidden"></i>Generate Report</button>
                                </div>
                            </section>

                        </div> -->
            <div class="margin-top-20">
                <hr>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="contents text-center margin-top-100 no_generate_report">
            <p class="font-50"><img alt="report image" src="<?php echo base_url();?>assets/images/report_1_.svg"></p>
            <h4 class="gray-color margin-top-30 font-20">Please select date range <br>to generate report </h4>
        </div>

        <div class="contents margin-top-20 show_generate_report">
            <div class="contents padding-top-20">
                <!-- <div class="f-left">
                    <label class="margin-bottom-5">search:</label><br>
                    <input class="search width-800px" type="text" style="width:800px;">
                </div>
                <button class="f-right btn btn-dark margin-top-20 margin-left-20">Search</button>
                
                <div class="clear"></div> -->

                <!-- <div class="f-left margin-top-20">
                    <div class="select pu_province_selection margin-right-10">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt"><input type="text" int-value="0" class="dd-txt" name="pu_provinces" value="Show All Provinces"></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option">
                                <div class="option province_select" int-value="0">Show All Provinces</div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <!--
                <div class="f-left margin-left-10 margin-top-20">
                    <div class="select">
                        <select>
                            <option value="Show All Transactions" selected="">Show All Transactions</option>
                        </select>
                    </div>
                    <div class="select pu_transaction_selection">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" class="dd-txt" name="pu_transactions" value="Call Orders" autocomplete="off" readonly="" data-value="Call Orders">
                            </div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option option-visible" style="display: block;">
                                <div class="option transaction_select" data-value="Store Channel">Call Orders</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="Show All Transaction" selected="">Show All Transaction</option>
                            <option value="Call Orders" data-value="Store Channel">Call Orders</option>
                            <option value="Web Orders" data-value="Web Orders">Web Orders</option>
                            <option value="Sms Orders" data-value="sms">Sms Orders</option>
                            <option value="Nkag Orders" data-value="nkag">Nkag Orders</option>
                        </select>
                    </div>
                </div>
                -->
                <!-- 
                <div class="f-left margin-left-10 margin-top-20">
                    <div class="select">
                        <select>
                            <option value="All Area" selected="">Show All Call Center</option>
                        </select>
                    </div>

                    <div class="select pu_callcenter_selection">
                        <div class="frm-custom-dropdown font-0">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" class="dd-txt" name="pu_callcenters" value="Show All Call Centers" autocomplete="off" int-value="0">
                            </div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option" style="display: none;">
                                <div class="option callcenter_select" int-value="0">Show All Call Centers</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="Show All Call Centers" selected="">Show All Call Centers</option>
                        </select>
                    </div>
                </div>
                -->
                <!-- <div class="f-left margin-left-10 margin-top-20">
                    <div class="select pu_store_selection">
                        <div class="frm-custom-dropdown font-0">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" class="dd-txt" name="pu_stores" value="Show All Stores" autocomplete="off" int-value="0">
                            </div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option" style="display: none;">
                                <div class="option store_select" int-value="0">Show All Stores</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="Show All Stores" selected="">Show All Stores</option>
                        </select>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</section>

<section header-content="dst_per_day" class="container-fluid header hidden" section-style="top-panel" id="dst_reports_header">
    <div class="row header-container">
        <div class="contents">
            <h1 class="f-left">DST Reports per Day</h1>
            <div class="f-right margin-top-20">
                <button class="btn btn-dark margin-right-10 disabled excel_dst">Download Excel File</button>
<!--                <button class="btn btn-dark ">FAQ</button>-->
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <!--<div class="row">
        <div class="contents margin-top-20">

            <div class="display-inline-mid">
                 
                <div class="f-left margin-right-10 ">
                    <label class="margin-bottom-5">Date From:</label><br>
                    <div id="dst_date_from" class="date-picker">
                        <input type="text" readonly>
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                </div>
                 
                <div class="f-left">
                    <label class="margin-bottom-5">Date To:</label><br>
                    <div id="dst_date_to" class="date-picker">
                        <input type="text" readonly>
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                </div>

                <div class="f-left padding-left-10">
                    <label class="margin-bottom-5">Store:</label><br>
                    <div class="select xlarge store-selection">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt"><input type="text" int-value="0" class="dd-txt" name="generate_store" value="All Stores"></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option">

                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="Daily">Daily</option>
                            <option value="Weekly">Weekly</option>
                            <option value="Monthly">Monthly</option>
                        </select>
                    </div>
                </div>
                <button type="button" class="btn btn-dark margin-left-20 margin-top-20 generate-dst">Generate Report</button>

                <div class="clear"></div>
            </div>


            <div class="display-inline-mid">
                <section class="container-fluid" section-style="content-panel">


                </section>

            </div>
            <div class="margin-top-20">
                <hr>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="contents dst-not-generated text-center margin-top-100">

            <p class="font-50"><img src="<?php echo assets_url() ?>/images/report_1_.svg" alt="report image"></p>
            <h4 class="gray-color margin-top-30 font-20">Please select date range <br>to generate report </h4>

        </div>

        <div class="contents dst-generated hidden">
            <div class="tbl / text-center ">
                <table class="viewable" id="excel-dst-table">
                    <thead class="font-10">
                        <tr>
                            <th class="text-center / min-width-20 notthis" rowspan="2">Date</th>
                            <th class="text-center / min-width-20 hidden" rowspan="2">Date</th>
                            <th class="text-center / " colspan="4">20 Minutes 0 to 500 Php</th>

                            <th class="text-center / " colspan="4">30 Minutes 0 to 1100 Php</th>

                            <th class="text-center / " colspan="4">45 Minutes 0 to 1100 Php</th>





                            <th class="text-center / min-width-20" rowspan="2">Advance Orders</th>

                            <th class="text-center / min-width-20" rowspan="2">Bulk Orders</th>

                            <th class="text-center / min-width-20" rowspan="2">Exceptions</th>

                            <th class="text-center" colspan="4">Total Hit Rates</th>

                            <th class="text-center / min-width-20" rowspan="2">Total Hitrate Percentage</th>
                        </tr>
                        <tr>

                            <th class="text-center / min-width-20 / bggray-darkred">Y</th>
                            <th class="text-center / min-width-20 / bggray-brightred">N</th>
                            <th class="text-center / min-width-20 / bggray-darkred">Total</th>
                            <th class="text-center / min-width-20 / bggray-brightred">%</th>

                            <th class="text-center / min-width-20 / bggray-darkred ">Y</th>
                            <th class="text-center / min-width-20 / bggray-brightred">N</th>
                            <th class="text-center / min-width-20 / bggray-darkred">Total</th>
                            <th class="text-center / min-width-20 / bggray-brightred">%</th>

                            <th class="text-center / min-width-20 / bggray-darkred">Y</th>
                            <th class="text-center / min-width-20 / bggray-brightred">N</th>
                            <th class="text-center / min-width-20 / bggray-darkred">Total</th>
                            <th class="text-center / min-width-20 / bggray-brightred">%</th>


                            <th class="text-center / min-width-20 / bggray-darkred">Y</th>
                            <th class="text-center / min-width-20 / bggray-brightred">N</th>
                            <th class="text-center / min-width-20 / bggray-darkred">Total</th>
                            <th class="text-center / min-width-20 / bggray-brightred">%</th>

                        </tr>
                    </thead>
                    <tbody class="font-15 dst-container">
                        <tr class="dst-row template">
                            <td class="notthis">
                                <div class="f-left">
                                    <p class="f-left font-10 info_month_year">May<br>2015</p>
                                    <p class="f-left font-20"><strong class="info_date">01</strong></p>
                                    <div class="clear"></div>
                                    <p class="font-10 margin-left-5 / margin-right-10 info_day">Friday</p>
                                </div>
                            </td>
                            <td class="info_date hidden">10</td>
                            <td class="yes_20">10</td>
                            <td class="no_20">15</td>
                            <td class="order_20">25</td>
                            <td><strong class="percent_20">95.3</strong></td>
                            <td class="yes_30">10</td>
                            <td class="no_30">15</td>
                            <td class="order_30">25</td>
                            <td><strong class="percent_30">95.3</strong></td>
                            <td class="yes_45">10</td>
                            <td class="no_45">15</td>
                            <td class="order_45">25</td>
                            <td><strong class="percent_45">95.3</strong></td>
                            <td class="advance_order">10</td>
                            <td class="bulk_order">0</td>
                            <td class="exceptions">0</td>
                            <td class="yes_all">10</td>
                            <td class="no_all">15</td>
                            <td class="total_all">25</td>
                            <td><strong class="percent_all">95.3</strong></td>
                            <td><span class="font-20 percent_all">100.0%</span> <a href="#" class="link-view-data">View Data</a></td>
                        </tr>
                    </tbody>
                </table>
                <table class="viewable hidden" id="download-excel-dst-table">
                    <thead class="font-10">
                        <tr>
                            <th class="text-center / min-width-20 notthis" rowspan="2">Date</th>
                            <th class="text-center / min-width-20 hidden" rowspan="2">Date</th>
                            <th class="text-center / " colspan="4">20 Minutes 0 to 500 Php</th>

                            <th class="text-center / " colspan="4">30 Minutes 0 to 1100 Php</th>

                            <th class="text-center / " colspan="4">45 Minutes 0 to 1100 Php</th>





                            <th class="text-center / min-width-20" rowspan="2">Advance Orders</th>

                            <th class="text-center / min-width-20" rowspan="2">Bulk Orders</th>

                            <th class="text-center / min-width-20" rowspan="2">Exceptions</th>

                            <th class="text-center" colspan="4">Total Hit Rates</th>

                            <th class="text-center / min-width-20" rowspan="2">Total Hitrate Percentage</th>
                        </tr>
                        <tr>

                            <th class="text-center / min-width-20 / bggray-darkred">Y</th>
                            <th class="text-center / min-width-20 / bggray-brightred">N</th>
                            <th class="text-center / min-width-20 / bggray-darkred">Total</th>
                            <th class="text-center / min-width-20 / bggray-brightred">%</th>

                            <th class="text-center / min-width-20 / bggray-darkred ">Y</th>
                            <th class="text-center / min-width-20 / bggray-brightred">N</th>
                            <th class="text-center / min-width-20 / bggray-darkred">Total</th>
                            <th class="text-center / min-width-20 / bggray-brightred">%</th>

                            <th class="text-center / min-width-20 / bggray-darkred">Y</th>
                            <th class="text-center / min-width-20 / bggray-brightred">N</th>
                            <th class="text-center / min-width-20 / bggray-darkred">Total</th>
                            <th class="text-center / min-width-20 / bggray-brightred">%</th>


                            <th class="text-center / min-width-20 / bggray-darkred">Y</th>
                            <th class="text-center / min-width-20 / bggray-brightred">N</th>
                            <th class="text-center / min-width-20 / bggray-darkred">Total</th>
                            <th class="text-center / min-width-20 / bggray-brightred">%</th>

                        </tr>
                    </thead>
                    <tbody class="font-15 download-dst-container">
                        <tr class="dst-row-download template notthis">
                            <td class="notthis">
                                <info class="full_date"></info>
                            </td>
                            <td class="info_date hidden">10</td>
                            <td class="yes_20">10</td>
                            <td class="no_20">15</td>
                            <td class="order_20">25</td>
                            <td><strong class="percent_20">95.3</strong></td>
                            <td class="yes_30">10</td>
                            <td class="no_30">15</td>
                            <td class="order_30">25</td>
                            <td><strong class="percent_30">95.3</strong></td>
                            <td class="yes_45">10</td>
                            <td class="no_45">15</td>
                            <td class="order_45">25</td>
                            <td><strong class="percent_45">95.3</strong></td>
                            <td class="advance_order">10</td>
                            <td class="bulk_order">0</td>
                            <td class="exceptions">0</td>
                            <td class="yes_all">10</td>
                            <td class="no_all">15</td>
                            <td class="total_all">25</td>
                            <td><strong class="percent_all">95.3</strong></td>
                            <td><span class="font-20 percent_all">100.0%</span> <a href="#" class="link-view-data">View Data</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div> -->

</section>

<section header-content="dst_per_day_view_data" class="container-fluid header hidden" section-style="top-panel" id="dst_per_day_view_data_header">
    <div class="row header-container">
        <div class="contents">
            <div class="breadcrumbs  margin-top-20">
                <p><a href="dst-report-per-day.php">DST Reports</a> <i class="fa fa-angle-right"></i> <strong id="smal_date"></strong></p>
            </div>
            <h1 class="f-left" id="big_date"></h1>
            <div class="f-right margin-top-20">
                <a href="dst-report-per-day.php"><button class="btn btn-dark margin-right-10" id="backto_dst">Back to DST Report</button></a>
                <button class="btn btn-dark margin-right-10 excel_dst" id="download_excel">Download Excel File</button>
<!--                <button class="btn btn-dark ">FAQ</button>-->
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>

<section header-content="dst_for_store" id="dst_for_store" class="container-fluid header hidden" section-style="top-panel">


    <div class="row header-container">
        <div class="contents">
            <h1 class="f-left">DST Reports for Store</h1>
            <div class="f-right margin-top-20">
                <button id="dst_for_store_download_excel_file" class="btn btn-dark margin-right-10" disabled>Download Excel File</button>
                <!--<button class="btn btn-dark ">FAQ</button>-->
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <div class="row">
        <div class="contents margin-top-20">
            <div class="display-inline-mid">
                <!-- date from -->
                <div class="f-left margin-right-20 ">
                    <label class="margin-bottom-5">Date From:</label><br>
                    <div class="date-picker" id="dst_for_store_date_from_dp">
                        <input id="dst_for_store_date_from" type="text" readonly>
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                </div>
                <!-- date from -->
                <div class="f-left margin-right-20">
                    <label class="margin-bottom-5">Date To:</label><br>
                    <div class="date-picker" id="dst_for_store_date_to_dp">
                        <input id="dst_for_store_date_to" type="text" readonly>
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                </div>

                <div class="f-left margin-right-20">
                    <label class="margin-bottom-5">Province:</label><br>
                    <div class="select custom-province-filter margin-right-10">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt"><input type="text" id="dst_per_store_province" data-value="0" class="dd-txt" name="province_select" value="Show All Provinces"></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option">
                                <div class="option custom-filter-option" data-value="0">All Provinces</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                        </select>
                    </div>
                </div>

                <!-- filter by transactions-->
                <div class="f-left">
                    <label class="margin-bottom-5">Transactions:</label><br>
                    <div class="select custom-transaction-filter margin-right-10">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt"><input type="text" id="dst_per_store_type" data-value="All Transaction" class="dd-txt" name="transaction_select" value="Show All Transactions"></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option">
                                <div class="option custom-filter-option" data-value="All Transaction">All Transactions</div>
                                <div class="option custom-filter-option" data-value="Store Channel">Call Order</div>
                                <div class="option custom-filter-option" data-value="Web">Web Order</div>
                                <div class="option custom-filter-option" data-value="SMS">SMS Order</div>
                                <div class="option custom-filter-option" data-value="nkag">NKAG Order</div>
                                <div class="option custom-filter-option" data-value="Food Panda">Food Panda</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                        </select>
                    </div>
                </div>

                <!-- filter by callcenters-->
                <div class="f-left margin-right-20">
                    <label class="margin-bottom-5">Callcenter:</label><br>
                    <div class="select custom-callcenter-filter margin-right-10">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt"><input type="text" id="dst_per_store_callcenter" data-value="0" class="dd-txt" name="callcenter_select" value="Show All Callcenter"></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option">
                                <div class="option custom-filter-option" data-value="0">All Callcenter</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                        </select>
                    </div>
                </div>

                <!-- filter by store-->
                <div class="f-left">
                    <label class="margin-bottom-5">Store:</label><br>
                    <div class="select custom-store-filter margin-right-10">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt"><input type="text" id="dst_per_store_store"  data-value="0" class="dd-txt" name="store_select" value="Show All Store"></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option">
                                <div class="option custom-filter-option" data-value="0">All Store</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                        </select>
                    </div>
                </div>
                <!-- generate report -->
                <button id="btn_dst_report_for_store_generate_report" type="button" class="btn btn-dark / margin-left-20 margin-top-20"><i class="fa fa-spinner fa-pulse hidden"></i> Generate Report</button>
                <div class="clear"></div>
            </div>
<!--            <div class="display-inline-mid padding-left-10 margin-left-10 f-right hidden">-->
<!--                <section class="container-fluid" section-style="content-panel">-->
<!--                    <!-- generate by period -->
<!--                    <div class="divider padding-left-20">-->
<!--                        <label>Generate by Period:</label><br>-->
<!---->
<!--                        <div class="select small">-->
<!--                            <div class="frm-custom-dropdown font-0"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" name="undefined" value="Daily" autocomplete="off"></div><div class="frm-custom-icon"></div><div class="frm-custom-dropdown-option" style="display: none;"><div class="option" data-value="Daily">Daily</div><div class="option" data-value="Weekly">Weekly</div><div class="option" data-value="Monthly">Monthly</div></div></div><select class="frm-custom-dropdown-origin" style="display: none;">-->
<!--                                <option value="Daily">Daily</option>-->
<!--                                <option value="Weekly">Weekly</option>-->
<!--                                <option value="Monthly">Monthly</option>-->
<!--                            </select>-->
<!--                        </div>-->
<!---->
<!--                        <button type="button" class="btn / btn-dark / margin-left-20">Generate Report</button>-->
<!--                    </div>-->
<!--                </section>-->
<!--            </div>-->
            <div class="margin-top-20">
                <hr>
            </div>
        </div>
    </div>

    <div id="dst_for_store_table_data_container" class="hidden">
        <div class="row">
            <div class="contents">
                <div id="dst_for_store_date_range" class="rounded-container f-left margin-right-10 margin-top-20 padding-left-15">
                    <div class="rounded-box xmedium padding-bottom-40">
                        <p class="font-20 margin-top-25 padding-top-10">Date Range</p>
                        <p class="font-16 margin-top-10 margin-left-20 margin-right-20" data-label="date_from">May 1, 2015 to</p>
                        <p class="font-16 margin-left-20 margin-right-20 padding-bottom-10" data-label="date_to" >May 18, 2015</p>
                    </div>
                </div>
                <div id="dst_for_store_20_mins" class="rounded-container f-left margin-right-10 margin-top-20 padding-left-15">
                    <div class="rounded-box xmedium padding-bottom-20">
                        <p class="font-18 margin-top-10 margin-bottom-10 padding-top-10">20 Mins | 0 - 500 <small>PHP</small></p>
                        <table class="font-16 width-100per">
                            <tbody>
                            <tr>
                                <td class="red-color padding-left-20 text-left"><strong>Yes</strong></td>
                                <td class="padding-right-20 text-right" data-label="yes">10</td>
                            </tr>
                            <tr>
                                <td class="red-color padding-left-20 text-left"><strong>No</strong></td>
                                <td class="padding-right-20 text-right" data-label="no">15</td>
                            </tr>
                            <tr>
                                <td class="red-color padding-left-20 text-left"><strong>Total</strong></td>
                                <td class="padding-right-20 text-right" data-label="total">25</td>
                            </tr>
                            <tr>
                                <td class="red-color padding-left-20 text-left"><strong>Percentage</strong></td>
                                <td class="padding-right-20 text-right" data-label="percentage">95.3%</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="dst_for_store_30_mins" class="rounded-container f-left margin-right-10 margin-top-20 padding-left-15">
                    <div class="rounded-box xmedium padding-bottom-20">
                        <p class="font-18 margin-top-10 margin-bottom-10 padding-top-10">30 Mins | 0 - 1000 <small>PHP</small></p>
                        <table class="font-16 width-100per">
                            <tbody>
                            <tr>
                                <td class="red-color padding-left-20 text-left"><strong>Yes</strong></td>
                                <td class="padding-right-20 text-right" data-label="yes">10</td>
                            </tr>
                            <tr>
                                <td class="red-color padding-left-20 text-left"><strong>No</strong></td>
                                <td class="padding-right-20 text-right" data-label="no">15</td>
                            </tr>
                            <tr>
                                <td class="red-color padding-left-20 text-left"><strong>Total</strong></td>
                                <td class="padding-right-20 text-right" data-label="total">25</td>
                            </tr>
                            <tr>
                                <td class="red-color padding-left-20 text-left"><strong>Percentage</strong></td>
                                <td class="padding-right-20 text-right" data-label="percentage">95.3%</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="dst_for_store_45_mins" class="rounded-container f-left margin-right-10 margin-top-20 padding-left-15">
                    <div class="rounded-box xmedium padding-bottom-20">
                        <p class="font-18 margin-top-10 margin-bottom-10 padding-top-10">45 Mins | 0 - 3299 <small>PHP</small></p>
                        <table class="font-16 width-100per">
                            <tbody>
                            <tr>
                                <td class="red-color padding-left-20 text-left"><strong>Yes</strong></td>
                                <td class="padding-right-20 text-right" data-label="yes">10</td>
                            </tr>
                            <tr>
                                <td class="red-color padding-left-20 text-left"><strong>No</strong></td>
                                <td class="padding-right-20 text-right" data-label="no">15</td>
                            </tr>
                            <tr>
                                <td class="red-color padding-left-20 text-left"><strong>Total</strong></td>
                                <td class="padding-right-20 text-right" data-label="total">25</td>
                            </tr>
                            <tr>
                                <td class="red-color padding-left-20 text-left"><strong>Percentage</strong></td>
                                <td class="padding-right-20 text-right" data-label="percentage">95.3%</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="clear"></div>
                <div id="dst_for_store_advance_order" class="rounded-container f-left margin-right-10 margin-top-20 margin-bottom-10 padding-left-10">
                    <div class="rounded-box small padding-bottom-30">
                        <p class="font-20 margin-top-25 padding-top-10">Advance Order</p>
                        <p class="font-30 margin-top-10 margin-left-20 margin-right-20 " data-label="no_advance_order">10</p>
                    </div>
                </div>
                <div id="dst_for_store_bulk_order" class="rounded-container f-left margin-right-10 margin-top-20 margin-bottom-10 padding-left-10">
                    <div class="rounded-box small padding-bottom-30">
                        <p class="font-20 margin-top-25 padding-top-10">Big<br>Order</p>
                        <p class="font-30 margin-top-10 margin-left-20 margin-right-20 " data-label="no_bulk_order">10</p>
                    </div>
                </div>
                <div id="dst_for_store_exception" class="rounded-container f-left margin-right-10 margin-top-20 margin-bottom-10 padding-left-10">
                    <div class="rounded-box small padding-bottom-50">
                        <p class="font-20 margin-top-25 padding-top-20">Exempted</p>
                        <p class="font-30 margin-top-10 margin-left-20 margin-right-20 " data-label="no_exception">10</p>
                    </div>
                </div>
                <div id="dst_for_store_total_mins" class="rounded-container f-left margin-right-10 margin-top-20 padding-left-10">
                    <div class="rounded-box small padding-bottom-30">
                        <p class="font-18 margin-top-30 margin-bottom-10 padding-top-15">Total Hitrate</p>
                        <table class="font-16 width-100per">
                            <tbody>
                            <tr>
                                <td class="red-color padding-left-20 text-left"><strong>Yes</strong></td>
                                <td class="padding-right-20 text-right" data-label="yes">30</td>
                            </tr>
                            <tr>
                                <td class="red-color padding-left-20 text-left"><strong>No</strong></td>
                                <td class="padding-right-20 text-right" data-label="no">45</td>
                            </tr>
                            <tr>
                                <td class="red-color padding-left-20 text-left"><strong>Total</strong></td>
                                <td class="padding-right-20 text-right" data-label="total">25</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="dst_for_store_total_hitrate_percentage" class="rounded-container f-left margin-right-10 margin-top-20 padding-left-10">
                    <div class="rounded-box medium padding-bottom-30">
                        <p class="font-18 margin-top-30 margin-bottom-10 padding-top-10">Total Hitrate Percentage</p>
                        <p class="font-50 red-color" data-label="total_hitrate_percentage">94.5%</p>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>

<!--        <div class="row margin-top-20">-->
<!--            <div class="contents">-->
<!--                <div class="f-left">-->
<!--                    <label class="margin-bottom-5">search:</label><br>-->
<!--                    <input class="search f-left large" type="text" name="agent_search" style="width: 600px">-->
<!--                </div>-->
<!--                <div class="f-left margin-left-20">-->
<!--                    <label class="margin-bottom-5">search by:</label><br>-->
<!--                    <div class="select provinces-agent-list">-->
<!--                        <div class="frm-custom-dropdown font-0">-->
<!--                            <div class="frm-custom-dropdown-txt">-->
<!--                                <input type="text" class="dd-txt" name="agent_province" value="All Province" autocomplete="off" int-value="0">-->
<!--                            </div>-->
<!--                            <div class="frm-custom-icon"></div>-->
<!--                            <div class="frm-custom-dropdown-option" style="display: none;">-->
<!--                                <div class="option province-select" data-value="0">All Province</div>-->
<!--                                <div class="option province-select" data-value="4">Bataan</div><div class="option province-select" data-value="5">Batangas</div><div class="option province-select" data-value="3">Bulacan</div><div class="option province-select" data-value="8">Cavite</div><div class="option province-select" data-value="7">Cebu</div><div class="option province-select" data-value="6">Laguna</div><div class="option province-select" data-value="1">Metro Manila</div><div class="option province-select" data-value="9">Pampanga</div><div class="option province-select" data-value="2">Pangasinan</div></div>-->
<!--                        </div>-->
<!--                        <select class="frm-custom-dropdown-origin" style="display: none;">-->
<!--                            <option value="All-Province">All Province</option>-->
<!--                        </select>-->
<!--                    </div></div>-->
<!--                <button class="f-left btn btn-dark margin-top-20 margin-left-20 search_agent">Search<i class="fa fa-spinner fa-pulse hidden"></i></button>-->
<!--            </div>-->
<!--        </div>-->

        <div class="row margin-top-20">
            <div class="contents line">
                <!--<div class="f-left">
                    <label class="margin-bottom-5">Filter Province:</label><br>
                    <div class="select select-filter-provinces per_store_province" is-dirty="true">
                        <div class="frm-custom-dropdown font-0">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" class="dd-txt" name="undefined" value="Show All Provinces" autocomplete="off" readonly="">
                            </div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option option-visible" style="display: block;">
                                <div class="option custom-filter" data-value="0">Show All Provinces</div>

                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option data-value="Show All Provinces" selected="">Show All Provinces</option>
                        </select>
                    </div>
                </div>
                <div class="f-left margin-left-10">
                    <label class="margin-bottom-5">Filter Transaction:</label><br>
                    <div class="select select-filter-transactions per_store_transactions">
                        <div class="frm-custom-dropdown font-0">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" class="dd-txt" name="undefined" value="Show All Transaction" autocomplete="off" readonly="" data-value="Store Channel">
                            </div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option option-visible" style="display: block;">
                                <div class="option custom-filter" data-value="Show All Transaction">Show All Transaction</div>
                                <div class="option custom-filter" data-value="Store Channel">Call Orders</div>
                                <div class="option custom-filter" data-value="Web Orders">Web Orders</div>
                                <div class="option custom-filter" data-value="Sms Orders">Sms Orders</div>
                                <div class="option custom-filter" data-value="nkag">Nkag Orders</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="Show All Transaction" selected="">Show All Transaction</option>
                            <option value="Call Orders" data-value="Store Channel">Call Orders</option>
                            <option value="Web Orders" data-value="Web Orders">Web Orders</option>
                            <option value="Sms Orders" data-value="sms">Sms Orders</option>
                            <option value="Nkag Orders" data-value="nkag">Nkag Orders</option>
                        </select>
                    </div>
                </div>
                 <div class="f-left margin-left-10">
                    <label class="margin-bottom-5">Filter Callcenter:</label><br>
                    <div class="select select-filter-call-centers per_store_callcenter">
                        <div class="frm-custom-dropdown font-0">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" class="dd-txt" name="undefined" value="Show All Call Centers" autocomplete="off" data-value="0">
                            </div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option" style="display: none;">
                                <div class="option custom-filter" data-value="0">Show All Call Centers</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="Show All Call Centers" selected="">Show All Call Centers</option>
                        </select>
                    </div>
                </div>-->
                <div class="f-right"><button id="btn_dst_for_store_view_data" class="btn btn-dark font-14 margin-top-20">View Data</button></div>
                <div class="clear"></div>
            </div>
        </div>

        <div class="row">
            <div class="contents">
                <!-- End of Paganation -->

                <!-- Reports Table -->
                <div class="tbl text-center margin-bottom-50">
                    <table id="dst_for_store_table_hidden" class="hidden">
                        <thead class="font-10">
                            <tr>
                                <th class="text-center">Store Name</th>
                                <th>20 Minutes 0 to 500 Php Yes / Total</th>
                                <th>30 Minutes 0 to 1100 Php Yes / Total</th>
                                <th>45Minutes 0 to 1100 Php Yes / Total</th>
                                <th>Advance Orders Yes / Total</th>
                                <th>Big Orders Yes / Total</th>
                                <th>Not Included Yes / Total</th>
                                <th>Total Hitrate Yes / Total</th>
                                <th>Total Hitrate Percentage</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="template excel_row notthis">
                                <td data-label="store_name" class="text-center">Store Name</td>
                                <td data-label="20_rate_yes_excel"></td>
                                <td data-label="30_rate_yes_excel"></td>
                                <td data-label="45_rate_yes_excel"></td>
                                <td data-label="advance_orders_excel">Advance Orders Yes / Total</td>
                                <td data-label="bulk_orders_excel">>Big Orders Yes / Tota</td>
                                <td data-label="exceptions_excel">Not Included Yes / Total</td>
                                <td data-label="total_rate_yes_excel">Total Hitrate Yes / Total</th>
                                <td data-label="total_rate_yes_excel_per">Total Hitrate Yes / Total</th>
                            </tr>
                        </tbody>
                    </table>
                    <table id="dst_for_store_table" class="viewable">
                        <thead class="font-10">
                        <tr>
                            <th class="text-center no-width" colspan="4">20 Minutes 0 to 500 Php</th>
                            <th class="text-center" colspan="4">30 Minutes 0 to 1100 Php</th>
                            <th class="text-center" colspan="4">45 Minutes 0 to 3299 Php</th>
                            <th class="text-center min-width-20" rowspan="2">Advance Orders</th>
                            <th class="text-center  min-width-20" rowspan="2">Big Orders</th>
                            <th class="text-center min-width-20" rowspan="2">Exempted</th>
                            <th class="text-center" colspan="4">Total Hit Rates</th>
                            <th class="text-center min-width-20" rowspan="4">Total Hitrate Percentatge</th>
                        </tr>
                        <tr>
                            <th class="text-center min-width-20 bggray-darkred no-width">Y</th>
                            <th class="text-center min-width-20 bggray-brightred">N</th>
                            <th class="text-center min-width-20 bggray-darkred">Total</th>
                            <th class="text-center min-width-20 bggray-brightred">%</th>

                            <th class="text-center min-width-20 bggray-darkred ">Y</th>
                            <th class="text-center min-width-20 bggray-brightred">N</th>
                            <th class="text-center min-width-20 bggray-darkred">Total</th>
                            <th class="text-center min-width-20 bggray-brightred">%</th>

                            <th class="text-center min-width-20 bggray-darkred">Y</th>
                            <th class="text-center min-width-20 bggray-brightred">N</th>
                            <th class="text-center min-width-20 bggray-darkred">Total</th>
                            <th class="text-center min-width-20 bggray-brightred">%</th>


                            <th class="text-center min-width-20 bggray-darkred">Y</th>
                            <th class="text-center min-width-20 bggray-brightred">N</th>
                            <th class="text-center min-width-20 bggray-darkred">Total</th>
                            <th class="text-center min-width-20 bggray-brightred">%</th>
                        </tr>
                        </thead>
                        <tbody class="font-14">
                            <tr data-store-id="" class="template" data-template="dst_for_store_one_row">
                                <td colspan="20" class="no-padding-all">
                                    <table class="no-bg">
                                        <tbody class="font-14 padding-bottom-15">
                                        <tr>
                                            <td colspan="20">
                                                <p class="text-left"><span><strong data-label="store_name">Balibago, Angeles - Pampanga | JB0893<strong></strong></strong></span></p><strong><strong>
                                                    </strong></strong></td>
                                        </tr>
                                        <tr>
                                            <td class="width-25px" data-label="20_rate_yes">10</td>
                                            <td class="width-25px" data-label="20_rate_no">15</td>
                                            <td class="width-45px" data-label="20_rate_total">25</td>
                                            <td class="width-20px"><strong data-label="20_rate_per">95.3</strong></td>

                                            <td class="width-25px" data-label="30_rate_yes">10</td>
                                            <td class="width-25px" data-label="30_rate_no">15</td>
                                            <td class="width-45px" data-label="30_rate_total">25</td>
                                            <td class="width-20px"><strong data-label="30_rate_per">95.3</strong></td>

                                            <td class="width-25px" data-label="45_rate_yes">10</td>
                                            <td class="width-25px" data-label="45_rate_no">15</td>
                                            <td class="width-45px" data-label="45_rate_total">25</td>
                                            <td class="width-20px"><strong data-label="45_rate_per">95.3</strong></td>

                                            <td class="width-90px" data-label="advance_orders">10</td>
                                            <td class="width-75px" data-label="bulk_orders">13</td>
                                            <td class="width-70px" data-label="exceptions">5</td>

                                            <td class="width-25px" data-label="total_rate_yes">10</td>
                                            <td class="width-25px" data-label="total_rate_no">5</td>
                                            <td class="width-40px" data-label="total_rate_total">15</td>
                                            <td class="width-30px"><strong data-label="total_rate_per">95.3</strong></td>

                                            <td class="width-120px"><span class="font-20" data-label="total_hitrate_per">100.0%</span><br><a href="javascript:void(0)" class="viewdata">View Data</a></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- End of Reports -->
            </div>
        </div>

    </div>

    <div id="dst_for_store_no_data_container" class="row">
        <div class="contents text-center margin-top-100 no_generate_report">
            <p class="font-50"><img alt="report image" src="<?php echo base_url()?>assets/images/report_1_.svg"></p>
            <h4 class="gray-color margin-top-30 font-20">Please select date range <br>to generate report </h4>
        </div>
    </div>
</section>

<section header-content="dst_for_store_view_data" id="dst_for_store_view_data" class="container-fluid header hidden" section-style="top-panel">
    <section class="container-fluid" section-style="top-panel">
        <div class="row header-container">
            <div class="contents">
                <div class="breadcrumbs  margin-top-20">
                    <p><a class="dst-for-store-view-data-back-to-main">DST Reports</a> <i class="fa fa-angle-right"></i> <strong><span data-label="date_from">May 1, 2015</span> to <span data-label="date_to">May 30, 2015</span> (View Data)</strong></p>
                </div>
                <h1 class="f-left"><span data-label="date_from">May 1, 2015</span> to <span data-label="date_to">May 30, 2015</span> </h1>
                <div class="f-right margin-top-20">
                    <a><button class="btn btn-dark margin-right-10 dst-for-store-view-data-back-to-main">Back to DST report per store</button></a>
                    <a><button class="btn btn-dark margin-right-10 dst-for-store-download">Download Excel</button></a>
                    <!--<button class="btn btn-dark ">FAQ</button>-->
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="row">
            <div class="contents margin-top-20">
                <div class="rounded-container width-100per margin-right-10 margin-top-20">
                    <div class="rounded-box ">
                        <p class="font-20 margin-top-10 margin-bottom-10 padding-top-10">Total Hitrate Percentage</p>
                        <p class="font-50 margin-bottom-10 red-color" data-label="total_hitrate_percentage">94.5%</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="contents padding-top-20">
                <!-- Paganation -->
               <!-- <div class="no-padding-all">
                    <div class="paganation">
                        <ul>
                            <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                            <li><a class="active" href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">6</a></li>
                            <li><a href="#">7</a></li>
                            <li><a href="#">8</a></li>
                            <li><a href="#">9</a></li>
                            <li><a href="#">10</a></li>
                            <li><a href="#">11</a></li>
                            <li><a href="#">12</a></li>
                            <li><a href="#">13</a></li>
                            <li><a class="continue" href="#">...</a></li>
                            <li><a href="#">14</a></li>
                            <li><a href="#">15</a></li>
                            <li><a href="#">16</a></li>
                            <li><a href="#">17</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                        </ul>
                    </div>
                </div>-->

                <div style="width: 100%; max-width: 900px; 	box-shadow: 0 0px 0px rgba(0,0,0,0) !important;" class="content-container custom-pagination">
                    <ul class="paganation-3 pagination content-container prod-avail-custom-pagination1" id="pagination-3">

                    </ul>
                </div>

                <div class="hidden">
                    <ul class="paganations-3-hidden content-container">

                    </ul>
                </div>

                <!-- End of Paganation -->
                <!--<div class="f-left">
                    <label class="margin-bottom-5">search:</label><br>
                    <input class="search" type="text">
                </div>-->
                <!--<button class="f-right btn btn-dark margin-top-20 margin-left-20">Search</button>-->
                <!--<div class="f-right margin-top-20">
                    <div class="select">
                        <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt"></div><div class="frm-custom-icon"></div><div class="frm-custom-dropdown-option"><div class="option" data-value="All Area">Order ID</div></div></div><select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="All Area" selected="">Order ID</option>
                        </select>
                    </div>
                </div>-->
                <div class="clear"></div>

                <div class="row margin-top-20">
                    <div class="contents line">
                        <!--<div class="select larger">
                            <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt"></div><div class="frm-custom-icon"></div><div class="frm-custom-dropdown-option"><div class="option" data-value="Show All Transactions">Show All Transactions</div></div></div><select class="frm-custom-dropdown-origin" style="display: none;">
                                <option value="Show All Transactions">Show All Transactions</option>
                            </select>
                        </div>-->
                        <!--<span class="white-space"></span>-->

                        <div class="f-right bggray-white sort-container">
                            <p class="f-left font-12 padding-left-10 padding-top-5">
                                <strong>Sort By:</strong>
                            </p>
                            <p class="f-left font-12 padding-left-5 padding-top-5">
                                <a class="active" href="#">
                                    <strong class="dst-sort-store asc" sort="data-customer-name">Customer Name</strong>
                                    <img class="asc hidden" src="<?php echo assets_url() ?>/images/ui/sort-top-arrow.png">
                                    <img class="desc hidden" src="<?php echo assets_url() ?>/images/ui/sort-down-arrow.png" width="16" height="9">
                                </a>
                            </p>
                            <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                            <p class="f-left font-12 padding-left-5 padding-top-5">
                                <a class="active" href="#">
                                    <strong class="dst-sort-store asc" sort="data-order-id">Order ID</strong>
                                    <img class="asc hidden" src="<?php echo assets_url() ?>/images/ui/sort-top-arrow.png">
                                    <img class="desc hidden" src="<?php echo assets_url() ?>/images/ui/sort-down-arrow.png" width="16" height="9">
                                </a>
                            </p>
                            <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                            <p class="f-left font-12 padding-left-5 padding-top-5">
                                <a class="active" href="#">
                                    <strong class="dst-sort-store asc" sort="data-order-id">Order Date</strong>
                                    <img class="asc hidden" src="<?php echo assets_url() ?>/images/ui/sort-top-arrow.png">
                                    <img class="desc hidden" src="<?php echo assets_url() ?>/images/ui/sort-down-arrow.png" width="16" height="9">
                                </a>
                            </p>
                            <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                            <p class="f-left font-12 padding-top-5">
                                <a class="active" href="#">
                                    <strong class="dst-sort-store" sort="data-hit-rate">Hit-Rate</strong>
                                    <img class="asc hidden" src="<?php echo assets_url() ?>/images/ui/sort-top-arrow.png">
                                    <img class="desc hidden" src="<?php echo assets_url() ?>/images/ui/sort-down-arrow.png" width="16" height="9">
                                    <!--                        <img src="http://localhost/store/assets/images/ui/sort-top-arrow.png">-->
                                </a>
                            </p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container-fluid" section-style="content-panel">
        <table id="dst_reports_specific_content_table" style="display:none">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Order ID</th>
                    <th>Customer Name</th>
                    <th>Customer Number</th>
                    <th>Customer Address</th>
                    <th>Order Details</th>
                    <th>Gross Sales</th>
                    <th>Food Sales</th>
                    <th>Grid</th>
                    <th>Date/Time Ordered</th>
                    <th>Accepted</th>
                    <th>Assembled</th>
                    <th>Drive Out</th>
                    <th>Date/Time </th>
                    <th>Drive In</th>
                    <th>Kitchen Time</th>
                    <th>Delivery Time</th>
                    <th>P0-P500 <= 20 MINS. Y or N </th>
                    <th>P0 - P1100 <= 30 MINS. Y or N </th>
                    <th>P1101 - UP <= 45 MINS. Y or N </th>
                    <th>Advance Orders</th>
                    <th>Big Orders</th>
                    <th>Not Included</th>
                    <th>Branch</th>
                    <th>Time Limit</th>
                    <th>Hitrate</th>
                </tr>
            </thead>


            <tbody class="dst_reports_specific_content_table_container">
                <tr class="template detailed notthis">
                    <td class="no">No.</td>
                    <td class="order_id">Order ID</td>
                    <td class="customer_name">Customer Name</td>
                    <td class="customer_number">Customer Number</td>
                    <td class="customer_address">Customer Address</td>
                    <td class="details">Order Details</td>
                    <td class="gross_sales">Gross Sales</td>
                    <td class="food_sales">Food Sales</td>
                    <td class="grid"></td>
                    <td class="transaction_time">Date/Time Ordered</td>
                    <td class="transaction_accepted">Accepted</td>
                    <td class="assembled">Assembled</td>
                    <td class="drive_out">Drive Out</td>
                    <td class="date_time_delivered">Date/Time Delivered </td>
                    <td class="drive_in">Drive In</td>
                    <td class="kitchen_time">Kitchen Time</td>
                    <td class="delivery_time">Delivery Time</td>
                    <td class="20mins"></td>
                    <td class="30mins"></td>
                    <td class="45mins"></td>
                    <td class="advanced"></td>
                    <td class="big_rder"></td>
                    <td class="exempted"></td>
                    <td class="branch">Branch</td>
                    <td class="serving_time">Time Limit</td>
                    <td class="hit">Hitrate</td>
                </tr>
            </tbody>
        </table>
        <div id="dst_reports_for_stores_orders_container" class="row">
            <div id="dst_reports_for_stores_orders_template" class="dst-reports-for-stores-orders template">
                <div class="content-container viewable">
                    <div>
                        <div class="width-40per display-inline-bot">
                            <p class="font-20 margin-bottom-5">Order ID: <span data-label="order_id">954861</span></p>

                            <p class="margin-bottom-5"><strong class="red-color">Name: </strong><span data-label="name">Mark Anthony D. Dulay</span></p>

                            <p><strong><span class="red-color">Contact Num: </span></strong><span data-label="contact_num">(+63) 915-516-6153
  <i class="fa fa-mobile margin-right-5"></i>Globe</span></p>
                        </div>
                        <div class="width-40per display-inline-bot">
                            <p class=" margin-bottom-5"><strong class="red-color">Date/Time Accepted:</strong><span data-label="date_time_accepted">May 18,
  2015 | 11:42:02</span></p>

                            <p class=" margin-bottom-5"><strong class="red-color">Date/Time Delivered:</strong><span data-label="date_time_delivered">May 18,
  2015 | 11:42:02</span></p>
                        </div>
                        <div class="width-20per display-inline-mid text-center">
                            <p class="font-16 margin-bottom-5 gray-color"><strong>HITRATE</strong></p>

                            <p class="font-30 margin-bottom-5" data-label="hitrate">100%</p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="content-container">
                    <div>
                        <div class="width-50per f-left">
                            <p class="font-20 margin-bottom-5">Order ID: <span data-label="order_id">954861</span></p>
                            <table class="width-100per">
                                <tbody>
                                <tr>
                                    <td><strong class="red-color">Name:</strong></td>
                                    <td><span data-label="name">Mark Anthony D. Dulay</span></td>
                                </tr>
                                <tr>
                                    <td><strong class="red-color">Contact Num:</strong></td>
                                    <td><span data-label="contact_num">(+63) 915-516-6153 <i class="fa fa-mobile margin-right-5"></i>Globe</span></td>
                                </tr>
                                <tr>
                                    <td><strong class="red-color">Order Type:</strong></td>
                                    <td data-label="order_type">Delivery</td>
                                </tr>
                                <tr>
                                    <td><strong class="red-color">Date/Time Accepted:</strong></td>
                                    <td data-label="date_time_accepted">May 18, 2015 | 11:42:02</td>
                                </tr>
                                <tr>
                                    <td><strong class="red-color">Date/Time Delivered:</strong></td>
                                    <td data-label="date_time_delivered">May 18, 2015 | 11:42:02</td>
                                </tr>
                                <tr>
                                    <td><strong class="red-color">Hitrate</strong></td>
                                    <td class="font-20" data-label="hitrate">100%</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <div class="width-50per f-left">
                            <div class="tbl">
                                <table>
                                    <thead>
                                    <tr>
                                        <th class="width-50per text-left">Order Log</th>
                                        <th class="text-center">Time Completed</th>
                                        <th class="text-center">Duration</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Order Accepted</td>
                                        <td class="text-center">11:24 AM</td>
                                        <td class="text-center">00:00:35</td>
                                    </tr>
                                    <tr>
                                        <td>Order Assemble</td>
                                        <td class="text-center">11:24 AM</td>
                                        <td class="text-center">00:00:35</td>
                                    </tr>
                                    <tr>
                                        <td>Driver Out</td>
                                        <td class="text-center">11:24 AM</td>
                                        <td class="text-center">00:00:35</td>
                                    </tr>
                                    <tr>
                                        <td>Driver In</td>
                                        <td class="text-center">11:24 AM</td>
                                        <td class="text-center">00:00:35</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="content">
                        <h4>Order</h4>
                        <hr>
                        <div class="tbl">
                            <table>
                                <thead>
                                <tr>
                                    <th class="width-100px text-left">Quantity</th>
                                    <th class="width-100per text-center">Product</th>
                                    <th class="text-center">Price</th>
                                    <th class="text-center">Subtotal</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td class="text-left">Sundae Strawberry</td>
                                    <td class="text-right">30.80</td>
                                    <td class="text-right">30.80</td>
                                </tr>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td class="text-left">
                                        <i class="fa fa-chevron-down"></i> Chickenjoy 1pc w/ Rice + Choice of Drink<br>

                                        <p class="margin-left-15">Large Iced Tea</p>
                                    </td>
                                    <td class="text-right">95.70<br>17.50</td>
                                    <td class="text-right">95.70<br>17.50</td>
                                </tr>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td class="text-left">Sundae Strawberry</td>
                                    <td class="text-right">30.80</td>
                                    <td class="text-right">30.80</td>
                                </tr>
                                <tr>
                                    <td class="text-center">1</td>
                                    <td class="text-left">Sundae Strawberry</td>
                                    <td class="text-right">30.80</td>
                                    <td class="text-right">30.80</td>
                                </tr>
                                <tr class="light-red-bg white-color">
                                    <td class="text-left" colspan="3">Total Cost</td>
                                    <td class="text-right" data-label="total_cost">0 PHP</td>
                                </tr>
                                <tr class="light-red-bg white-color">
                                    <td class="text-left" colspan="3">Added VAT</td>
                                    <td class="text-right" data-label="added_vat">0 PHP</td>
                                </tr>
                                <tr class="light-red-bg white-color">
                                    <td class="text-left" colspan="3">Delivery Charge</td>
                                    <td class="text-right" data-label="delivery_charge">0 PHP</td>
                                </tr>
                                <tr class="dark-red-bg white-color">
                                    <td class="text-left" colspan="3">Total Bill</td>
                                    <td class="text-right" data-label="total_bill">0 PHP</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

<section header-content="dst_spec" class="container-fluid header hidden" section-style="top-panel" id = 'dst_reports_specific_header'>


    <!-- search order -->
    <div class="row header-container">


        <div class="contents">
            <div class="breadcrumbs f-left  margin-top-20">
                <p><a href="#">DST Report</a> <i class="fa fa-angle-right"></i> <strong class="info_date">May 02, 2015 (View Data)</strong></p>
            </div>
            <div class="clear"></div>
        </div>

        <div class="contents">

            <h1 class="f-left info_date">May 02, 2015</h1>
            <div class="f-right margin-top-20">
                <button class="btn btn-dark / margin-right-10 back_to_dst_report">Back to DST Report</button>
                <!--<button class="btn btn-dark">FAQ</button>-->
            </div>
            <div class="clear"></div>
        </div>
    </div>


    <div class="row">
        <div class="contents margin-top-20">


            <!-- rounded boxes -->
            <div class=" margin-bottom-20">
                <div class="rounded-container / f-left / margin-right-15">
                    <div class="rounded-box / medium  ">
                        <p class="font-16 / padding-top-15">20 Minutes | 0 - 500 PHP</p>
                        <br>
                        <div><strong>
                                <p class="f-left / red-color / margin-left-20">Yes:</p>
                                <p class="f-right / margin-right-20 yes_20">10</p>
                                <div class="clear"></div>
                                <br>

                                <p class="f-left / red-color / margin-left-20">No:</p>
                                <p class="f-right / margin-right-20 no_20">15</p>
                                <div class="clear"></div>
                                <br>

                                <p class="f-left / red-color / margin-left-20">Total:</p>
                                <p class="f-right / margin-right-20 order_20">25</p>
                                <div class="clear"></div>
                                <br>

                                <p class="f-left / red-color / margin-left-20">Percentage:</p>
                                <p class="f-right / margin-right-20 / margin-bottom-20 percent_20">95.3%</p>
                                <div class="clear"></div>
                                <br>


                            </strong>
                        </div>
                    </div>
                </div>
                <div class="rounded-container / f-left / margin-right-15">
                    <div class="rounded-box / medium">
                        <p class="font-16 / padding-top-15">30 Minutes | 0 - 1100 PHP</p>
                        <br>
                        <div><strong>
                                <p class="f-left / red-color / margin-left-20">Yes:</p>
                                <p class="f-right / margin-right-20 yes_30">10</p>
                                <div class="clear"></div>
                                <br>

                                <p class="f-left / red-color / margin-left-20">No:</p>
                                <p class="f-right / margin-right-20 no_30">15</p>
                                <div class="clear"></div>
                                <br>

                                <p class="f-left / red-color / margin-left-20">Total:</p>
                                <p class="f-right / margin-right-20 order_30">25</p>
                                <div class="clear"></div>
                                <br>

                                <p class="f-left / red-color / margin-left-20">Percentage:</p>
                                <p class="f-right / margin-right-20 / margin-bottom-20 percent_30">95.3%</p>
                                <div class="clear"></div>
                                <br>


                            </strong>
                        </div>
                    </div>
                </div>

                <div class="rounded-container / f-left / margin-right-15">
                    <div class="rounded-box / medium">
                        <p class="font-16 / padding-top-15">45 Minutes | 0 - 3299 PHP</p>
                        <br>
                        <div><strong>
                                <p class="f-left / red-color / margin-left-20">Yes:</p>
                                <p class="f-right / margin-right-20 yes_45">10</p>
                                <div class="clear"></div>
                                <br>

                                <p class="f-left / red-color / margin-left-20">No:</p>
                                <p class="f-right / margin-right-20 no_45">15</p>
                                <div class="clear"></div>
                                <br>

                                <p class="f-left / red-color / margin-left-20">Total:</p>
                                <p class="f-right / margin-right-20 order_45">25</p>
                                <div class="clear"></div>
                                <br>

                                <p class="f-left / red-color / margin-left-20">Percentage:</p>
                                <p class="f-right / margin-right-20 / margin-bottom-20 percent_45">95.3%</p>
                                <div class="clear"></div>
                                <br>

                            </strong>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>

                <!-- rounded small boxes -->
                <div class="margin-bottom-20 / margin-top-20">
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / small">
                            <p class="font-20 padding-top-20 / padding-bottom-10">Advance Order</p>
                            <br>
                            <p class="font-20 / padding-bottom-15 advance_order">10</p>
                        </div>
                    </div>
                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / small">
                            <p class="font-20 padding-top-20 / padding-bottom-10">Big Order</p>
                            <br>
                            <p class="font-20 / padding-bottom-10 / padding-top-30 bulk_order">0</p>
                        </div>
                    </div>

                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / small">
                            <p class="font-20 padding-top-20 / padding-bottom-10 ">Exempted</p>
                            <br>
                            <p class="font-20 padding-bottom-10 / padding-top-30 exception">0</p>
                        </div>
                    </div>

                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / small">
                            <p class="font-20 padding-top-20 / padding-bottom-10">Total Hitrate</p>
                            <br>
                            <div class="margin-left-10 / margin-right-10 / padding-bottom-10">
                                <strong>
                                    <p class="f-left / red-color">Yes:</p>
                                    <p class="f-right yes_all">20</p>
                                    <div class="clear"></div>

                                    <p class="f-left / red-color">No:</p>
                                    <p class="f-right no_all">45</p>
                                    <div class="clear"></div>

                                    <p class="f-left / red-color">Total:</p>
                                    <p class="f-right total_all">25</p>
                                    <div class="clear"></div>
                                </strong>
                            </div>
                        </div>
                    </div>

                    <div class="rounded-container / f-left / margin-right-15">
                        <div class="rounded-box / medium">
                            <p class="font-20 padding-top-15">Total Hitrate Percentage</p>
                            <br>
                            <h1 class=" / red-color / padding-bottom-35 percent_all">94.5%</h1>
                        </div>
                    </div>

                    <div class="clear"></div>
                </div>

                <!-- search -->
                <div class="f-left">
                    <label class="margin-bottom-5">search:</label><br>
                    <input class="search f-left xlarge" type="text" name="dst-filter">
                </div>
                <!-- search by -->
                <div class="f-left margin-left-20">
                    <label class="margin-bottom-5">search by:</label><br>
                    <div class="select xlarge dst-filter">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" name="filter-by"></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option">
                                <div class="option dst-filter" data-value="data-customer-name">Customer Name</div>
                                <div class="option dst-filter" data-value="data-order-id">Order ID</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="Order ID">Order ID</option>
                        </select>
                    </div>
                    <button type="button" class="btn btn-dark / margin-left-20 filter-dst">Search</button>
                </div>
                <div class="clear"></div>
            </div>
        </div>

        <div class="row margin-top-20">
            <div class="contents line">
                <!--<div class="select xlarge">
                    <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt"></div><div class="frm-custom-icon"></div><div class="frm-custom-dropdown-option"><div class="option" data-value="All Transaction">Show all Transaction</div><div class="option" data-value="Accepted Transaction">Show Accepted Transaction</div><div class="option" data-value="Assembling Transaction">Show Assembling Transaction</div><div class="option" data-value="Dispatch Transaction">Show for Dispatch Transaction</div><div class="option" data-value="Rider Out">Show Rider Out Transaction</div><div class="option" data-value="Rider In">Show RIder In Transaction</div><div class="option" data-value="Completed Transaction">Show Completed Transaction</div></div></div><select class="frm-custom-dropdown-origin" style="display: none;">
                        <option value="All Transaction">Show all Transaction</option>
                        <option value="Accepted Transaction">Show Accepted Transaction</option>
                        <option value="Assembling Transaction">Show Assembling Transaction</option>
                        <option value="Dispatch Transaction">Show for Dispatch Transaction</option>
                        <option value="Rider Out">Show Rider Out Transaction</option>
                        <option value="Rider In">Show RIder In Transaction</option>
                        <option value="Completed Transaction">Show Completed Transaction</option>
                    </select>
                </div>-->
                <!--<span class="white-space"></span>


                <span class="white-space"></span>-->

                <div class="f-right bggray-white sort-container">
                    <p class="f-left font-12 padding-left-10 padding-top-5">
                        <strong>Sort By:</strong>
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">
                        <a class="active" href="#">
                            <strong class="dst-sort asc" sort="data-customer-name">Customer Name</strong>
                            <img class="asc hidden" src="<?php echo assets_url() ?>/images/ui/sort-top-arrow.png">
                            <img class="desc hidden" src="<?php echo assets_url() ?>/images/ui/sort-down-arrow.png" width="16" height="9">
                        </a>
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">
                        <a class="active" href="#">
                            <strong class="dst-sort asc" sort="data-order-id">Order ID</strong>
                            <img class="asc hidden" src="<?php echo assets_url() ?>/images/ui/sort-top-arrow.png">
                            <img class="desc hidden" src="<?php echo assets_url() ?>/images/ui/sort-down-arrow.png" width="16" height="9">
                        </a>
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">
                        <a class="active" href="#">
                            <strong class="dst-sort asc" sort="data-order-id">Order Date</strong>
                            <img class="asc hidden" src="<?php echo assets_url() ?>/images/ui/sort-top-arrow.png">
                            <img class="desc hidden" src="<?php echo assets_url() ?>/images/ui/sort-down-arrow.png" width="16" height="9">
                        </a>
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-top-5">
                        <a class="active" href="#">
                            <strong class="dst-sort" sort="data-hit-rate">Hit-Rate</strong>
                            <img class="asc hidden" src="<?php echo assets_url() ?>/images/ui/sort-top-arrow.png">
                            <img class="desc hidden" src="<?php echo assets_url() ?>/images/ui/sort-down-arrow.png" width="16" height="9">
                            <!--                        <img src="http://localhost/store/assets/images/ui/sort-top-arrow.png">-->
                        </a>
                    </p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div></section>

<section header-content="sales" class="container-fluid header hidden" section-style="top-panel" id = 'sales_reports_header'>
    <div class="row header-container">
        <div class="contents">
            <h1 class="f-left">Sales Summary Reports</h1>
            <div class="f-right margin-top-20">
                <button class="btn btn-dark margin-right-10 sales_report_xls_button" disabled>Download Excel File</button>
                <!-- <button class="btn btn-dark ">FAQ</button> -->
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="row">
        <div class="contents margin-top-20">
            <!--
            <div class="f-left padding-left-10">
                <label class="margin-bottom-5">Store:</label><br>
                <div class="select xlarge sales-report-store-selection margin-right-10">
                    <div class="frm-custom-dropdown">
                        <div class="frm-custom-dropdown-txt"><input type="text" int-value="0" class="dd-txt" name="sales_report_store" value="All Stores"></div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option">

                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                        <option value="Daily">Daily</option>
                        <option value="Weekly">Weekly</option>
                        <option value="Monthly">Monthly</option>
                    </select>
                </div>
            </div>
            -->
            <div class="display-inline-mid generate_by_date">
                <!-- date from -->
                <div class="f-left margin-right-10 ">
                    <label class="margin-bottom-5">Date From:</label><br>
                    <div class="date-picker" id="sales-report-date-from">
                        <input type="text" name="sales_report_date_from" readonly>
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                </div>
                <!-- date from -->
                <div class="f-left margin-right-10 ">
                    <label class="margin-bottom-5">Date To:</label><br>
                    <div class="date-picker" id="sales-report-date-to">
                        <input type="text" name="sales_report_date_to" readonly>
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                </div>
                <div class="f-left margin-right-10 ">
                    <label class="margin-bottom-5">Province:</label><br/>
                    <div class="select sales_summary_province_selection">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt"><input type="text" int-value="0" class="dd-txt" name="sales_summary_provinces" value="Show All Provinces" readonly></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option">
                                <div class="option province_select" int-value="0">Show All Provinces</div>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="f-left margin-right-10 ">
                    <label class="margin-bottom-5">Type:</label><br/>
                    <div class="select sales_summary_transaction_selection">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" class="dd-txt" name="sales_summary_transactions" value="Show All Transactions" autocomplete="off" readonly="" data-value="Show All Transactions" readonly>
                            </div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option option-visible" style="display: block;">
                                <div class="option transaction_select" data-value="Show All Transactions">Show All Transactions</div>
                                <div class="option transaction_select" data-value="Store Channel">Call Orders</div>
                                <div class="option transaction_select" data-value="Web">Web Orders</div>
                                <div class="option transaction_select" data-value="SMS">Sms Orders</div>
                                <div class="option transaction_select" data-value="nkag">Nkag Orders</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="Show All Transactions" data-value="Show All Transactions" selected="">Show All Transaction</option>
                            <option value="Call Orders" data-value="Store Channel">Call Orders</option>
                            <option value="Web Orders" data-value="Web">Web Orders</option>
                            <option value="Sms Orders" data-value="SMS">Sms Orders</option>
                            <option value="Nkag Orders" data-value="nkag">Nkag Orders</option>
                        </select>
                    </div>
                </div>
                <div class="f-left margin-right-10 margin-top-20">
                    <label class="margin-bottom-5">Callcenter:</label><br/>
                    <div class="select sales_summary_callcenter_selection">
                        <div class="frm-custom-dropdown font-0">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" class="dd-txt" name="sales_summary_callcenters" value="Show All Call Centers" autocomplete="off" int-value="0" readonly>
                            </div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option" style="display: none;">
                                <div class="option callcenter_select" int-value="0">Show All Call Centers</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="Show All Call Centers" selected="">Show All Call Centers</option>
                        </select>
                    </div>
                </div>
                <div class="f-left margin-right-10 margin-top-20">
                    <label class="margin-bottom-5">Store:</label><br/>
                    <div class="select sales_summary_store_selection">
                        <div class="frm-custom-dropdown font-0">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" class="dd-txt" name="sales_summary_stores" value="Show All Stores" autocomplete="off" int-value="0" readonly>
                            </div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option" style="display: none;">
                                <div class="option store_select" int-value="0">Show All Stores</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="Show All Stores" selected="">Show All Stores</option>
                        </select>
                    </div>
                </div>
                <!-- <div class="clear"></div> -->
                <div class="f-left margin-right-10 margin-top-20">
                    <label class="margin-bottom-5">Group By:</label><br/>
                    <div class="select sales_summary_group_selection">
                        <div class="frm-custom-dropdown font-0">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" class="dd-txt" name="sales_summary_group" value="Date" autocomplete="off" data-value="date" readonly>
                            </div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option" style="display: none;">
                                <div class="option group_select" data-value="date">Date</div>
                                <div class="option group_select" data-value="store">Stores</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="Date" data-value="date" selected="">Date</option>
                            <option value="Stores" data-value="store">Stores</option>
                        </select>
                    </div>
                </div>
                <div class="f-left margin-right-10 margin-top-20">
                    <!-- generate report -->
                    <button type="button" class="btn btn-dark  / margin-top-20 generate_by_date"><i class="fa fa-spinner fa-pulse hidden"></i> Generate Report</button>
                </div>
                <div class="clear"></div>
            </div>


            <div class=" generate_by_period"><!-- display-inline-mid -->
                <section class="container-fluid" section-style="content-panel">
                    <!-- generate by period -->
<!--                    <div class="margin-top-10"><!-- divider padding-left-20 -->
<!--                        <label>Generate by Period:</label><br>-->
<!---->
<!--                        <div class="select xlarge select_period">-->
<!--                            <div class="frm-custom-dropdown">-->
<!--                                <div class="frm-custom-dropdown-txt">-->
<!--                                    <input type="text" class="dd-txt" name="period" value="Daily" readonly>-->
<!--                                </div>-->
<!--                                <div class="frm-custom-icon"></div>-->
<!--                                <div class="frm-custom-dropdown-option">-->
<!--                                    <div class="option" data-value="Daily">Daily</div>-->
<!--                                    <div class="option" data-value="Weekly">Weekly</div>-->
<!--                                    <div class="option" data-value="Monthly">Monthly</div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <select class="frm-custom-dropdown-origin" style="display: none;">-->
<!--                                <option value="Daily">Daily</option>-->
<!--                                <option value="Weekly">Weekly</option>-->
<!--                                <option value="Monthly">Monthly</option>-->
<!--                            </select>-->
<!--                        </div>-->
<!---->
<!--                        <button type="button" class="btn / btn-dark / margin-left-20 generate_by_period"><i class="fa fa-spinner fa-pulse hidden"></i>Generate Report</button>-->
<!--                    </div>-->
                </section>

            </div>
            <div class="margin-top-20">
                <hr>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="contents text-center margin-top-100 no_generate_report">
            <p class="font-50"><img alt="report image" src="<?php echo base_url();?>assets/images/report_1_.svg"></p>
            <h4 class="gray-color margin-top-30 font-20">Please select date range <br>to generate report </h4>
        </div>

        <div class="contents margin-top-20 show_generate_report">
            <div class="rounded-container / f-left / margin-right-20 / padding-left-15">
                <div class="rounded-box / xmedium">
                    <p class="font-16 margin-top-10 / padding-top-10">Date</p>
                    <br>
                    <p class="font-16 / margin-left-20 / margin-right-20 " data-label='date_from'>May 1, 2015 to</p>
                    <br>
                    <p class="font-16 / margin-left-20 / margin-right-20 / padding-bottom-10" data-label='date_to'>May 18, 2015</p>
                </div>
            </div>
            <div class="rounded-container / f-left / margin-right-20">
                <div class="rounded-box / xmedium">
                    <p class="font-16 margin-top-10 / padding-top-10">Total Sales</p>
                    <br>
                    <p class="font-20 / margin-top-10 / padding-bottom-15 " data-label='total_sales'>501,487.00 PHP</p>
                </div>
            </div>
            <div class="rounded-container / f-left / margin-right-20">
                <div class="rounded-box / xmedium">
                    <p class="font-16 margin-top-10 / padding-top-10">Food Sales</p>
                    <br>
                    <p class="font-20 / margin-top-10 / padding-bottom-15" data-label='food_sales'>501,487.00 PHP</p>
                </div>
            </div>
            <div class="rounded-container / f-left / ">
                <div class="rounded-box / xmedium">
                    <p class="font-16 margin-top-10 / padding-top-10">Transaction Count</p>
                    <br>
                    <p class="font-20 / margin-top-10 / padding-bottom-15" data-label='transaction_count'>4,578</p>
                </div>
            </div>

            <div class="clear"></div>
            <!-- <div class="f-left margin-left-10 margin-top-20">
                <div class="f-left">
                    <label class="margin-top-20">Search:</label><br/>
                    <input class="f-left xlarge" type="text" style="width:600px;"/>
                </div>
                <div class="f-left margin-left-10">
                     <label class="margin-top-20">Search By:</label><br/>
                    <div class="select">
                        <select>
                            <option value="Date of Sale" selected>Date of Sale</option>
                            <option value="Store Name">Store Name</option>
                        </select>
                    </div>
                </div>
                <button type="button" class="btn btn-dark margin-left-20 margin-top-40 f-right">Search</button>
                <div class="clear"></div>
            </div> -->
<!--             <div class="f-left margin-left-10 margin-top-20">
                <div class="select sales_summary_province_selection margin-right-10">
                    <div class="frm-custom-dropdown">
                        <div class="frm-custom-dropdown-txt"><input type="text" int-value="0" class="dd-txt" name="sales_summary_provinces" value="Show All Provinces"></div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option">
                            <div class="option province_select" int-value="0">Show All Provinces</div>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="f-left margin-left-10 margin-top-20">
                <div class="select sales_summary_transaction_selection">
                    <div class="frm-custom-dropdown">
                        <div class="frm-custom-dropdown-txt">
                            <input type="text" class="dd-txt" name="sales_summary_transactions" value="Show All Transactions" autocomplete="off" readonly="" data-value="Show All Transactions">
                        </div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option option-visible" style="display: block;">
                            <div class="option transaction_select" data-value="Show All Transactions">Show All Transactions</div>
                            <div class="option transaction_select" data-value="Store Channel">Call Orders</div>
                            <div class="option transaction_select" data-value="Web Orders">Web Orders</div>
                            <div class="option transaction_select" data-value="Sms Orders">Sms Orders</div>
                            <div class="option transaction_select" data-value="nkag">Nkag Orders</div>
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                        <option value="Show All Transactions" data-value="Show All Transactions" selected="">Show All Transaction</option>
                        <option value="Call Orders" data-value="Store Channel">Call Orders</option>
                        <option value="Web Orders" data-value="Web Orders">Web Orders</option>
                        <option value="Sms Orders" data-value="sms">Sms Orders</option>
                        <option value="Nkag Orders" data-value="nkag">Nkag Orders</option>
                    </select>
                </div>
            </div>
            <div class="f-left margin-left-10 margin-top-20">
                <div class="select sales_summary_callcenter_selection">
                    <div class="frm-custom-dropdown font-0">
                        <div class="frm-custom-dropdown-txt">
                            <input type="text" class="dd-txt" name="sales_summary_callcenters" value="Show All Call Centers" autocomplete="off" int-value="0">
                        </div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option" style="display: none;">
                            <div class="option callcenter_select" int-value="0">Show All Call Centers</div>
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                        <option value="Show All Call Centers" selected="">Show All Call Centers</option>
                    </select>
                </div>
            </div>
            <div class="f-left margin-left-10 margin-top-20">
                <div class="select sales_summary_store_selection">
                    <div class="frm-custom-dropdown font-0">
                        <div class="frm-custom-dropdown-txt">
                            <input type="text" class="dd-txt" name="sales_summary_stores" value="Show All Stores" autocomplete="off" int-value="0">
                        </div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option" style="display: none;">
                            <div class="option store_select" int-value="0">Show All Stores</div>
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                        <option value="Show All Stores" selected="">Show All Stores</option>
                    </select>
                </div>
            </div>
            <div class="clear"></div>
            <div class="f-left margin-left-10 margin-top-20">
                <label class="margin-top-10">Group By:</label><br/>
                <div class="select sales_summary_group_selection">
                    <div class="frm-custom-dropdown font-0">
                        <div class="frm-custom-dropdown-txt">
                            <input type="text" class="dd-txt" name="sales_summary_group" value="Date" autocomplete="off" data-value="date" readonly>
                        </div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option" style="display: none;">
                            <div class="option group_select" data-value="date">Date</div>
                            <div class="option group_select" data-value="store">Stores</option>
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                        <option value="Date" data-value="date" selected="">Date</option>
                        <option value="Stores" data-value="store">Stores</option>
                    </select>
                </div>
            </div> -->
        </div>
    </div>
</section>

<section header-content="sales_prod" class="container-fluid header hidden" section-style="top-panel" id = 'sales_reports_by_product_header'>
    <div class="row header-container">
        <div class="contents">
            <div class="clear"></div>
            <h1 class="f-left">Item Sales Report</h1>
            <div class="f-right margin-top-20">
                <button class="btn btn-dark margin-right-20 download_excel_file">Download Excel File <i class="fa fa-spinner fa-pulse hidden"></i> </button>
                <!-- <button class="btn btn-dark">FAQ</button>-->
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="row">
        <div class="contents margin-top-20">

            <div class="display-inline-mid generate_by_date">
                <!-- date from -->
                <div class="f-left margin-right-10 ">
                    <label class="margin-bottom-5">Date From:</label><br>
                    <div class="date-picker" id="psr_date_from">
                        <input type="text" name="psr_date_from" readonly>
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                </div>
                <!-- date from -->
                <div class="f-left">
                    <label class="margin-bottom-5">Date To:</label><br>
                    <div class="date-picker" id="psr_date_to">
                        <input type="text" name="psr_date_to" readonly>
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                </div>
                <!-- generate report -->
                <!-- <button type="button" class="btn btn-dark margin-left-10 margin-top-20 generate_by_date"><i class="fa fa-spinner fa-pulse hidden"></i> Generate Report</button> -->
                <div class="clear"></div>
            </div>


            <div class="display-inline-mid generate_by_period">
                <section class="container-fluid" section-style="content-panel">
                    <!-- generate by period -->
<!--                    <div class="divider padding-left-20">-->
<!--                        <label>Generate by Period:</label>-->
<!--                        <br>-->
<!--                        <div class="select small">-->
<!--                            <div class="frm-custom-dropdown">-->
<!--                                <div class="frm-custom-dropdown-txt">-->
<!--                                    <input type="text" class="dd-txt"  id="psr_period_date_to">-->
<!--                                </div>-->
<!--                                <div class="frm-custom-icon"></div>-->
<!--                                <div class="frm-custom-dropdown-option">-->
<!--                                    <div class="option" data-value="Hourly">Hourly</div>-->
<!--                                    <div class="option" data-value="Daily">Daily</div>-->
<!--                                    <div class="option" data-value="weekly">Weekly</div>-->
<!--                                    <div class="option" data-value="monthly">Monthly</div>-->
<!--                                </div>-->
<!--                                <select class="frm-custom-dropdown-origin" style="display: none;">-->
<!--                                    <option value="monthly">Monthly</option>-->
<!--                                </select>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <button type="button" class="btn btn-dark margin-left-20 generate_by_period">Generate Report<i class="fa fa-spinner fa-pulse hidden"></i></button>-->
<!--                    </div>-->
                </section>

            </div>
            <!-- <div class="margin-top-20">
                <hr>
            </div> -->
        </div>
    </div>
    <div class="row filter_table_container">
        <div class="contents margin-top-15 ">
            <!-- search -->
            <div class="f-left hidden">
                <label class="margin-bottom-5">search:</label><br>
                <input class="search f-left medium" type="text" name="search_filter">
            </div>
            <!-- search by -->
            <div class="f-left margin-left-20 hidden">
                <label class="margin-bottom-5">search by:</label><br>
                <div class="select medium">
                    <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" name="search_by_filter" value="Product Name" ></div><div class="frm-custom-icon"></div><div class="frm-custom-dropdown-option"><div class="option" data-value="Product Name">Product Name</div></div></div><select class="frm-custom-dropdown-origin" style="display: none;">
                        <option value="Product Name">Product Name</option>
                    </select>
                </div>
            </div>

            <button class="f-left btn btn-dark margin-top-20 margin-left-20 filter_table hidden">Search <i class="fa fa-spinner fa-pulse hidden"></i></button>
            <div class="clear"></div>
            <!-- filter by provinces-->
            <div class="f-left margin-top-20">
                <label class="margin-bottom-5">Province:</label><br>
                <div class="select province-selection-product-sales-report margin-right-10 small">
                    <div class="frm-custom-dropdown">
                        <div class="frm-custom-dropdown-txt"><input type="text" int-value="0" class="dd-txt" name="province_select" value="All Provinces"></div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option">
                            <div class="option" int-value="0">All Provinces</div>
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                    </select>
                </div>
            </div>

            <!-- filter by transactions-->
            <div class="f-left margin-top-20">
                <label class="margin-bottom-5">Type:</label><br>
                <div class="select transaction-selection-product-sales-report margin-right-10">
                    <div class="frm-custom-dropdown">
                        <div class="frm-custom-dropdown-txt"><input type="text" data-value="All Transaction" class="dd-txt" name="transaction_select" value="All Transactions"></div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option">
                            <div class="option" data-value="All Transaction">All Transactions</div>
                            <div class="option" data-value="Store Channel">Call Order</div>
                            <div class="option" data-value="Web">Web Order</div>
                            <div class="option" data-value="SMS">SMS Order</div>
                            <div class="option" data-value="nkag">NKAG Order</div>
                            <div class="option" data-value="Food Panda">Food Panda</div>
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                    </select>
                </div>
            </div>

            <!-- filter by callcenters-->
            <div class="f-left margin-top-20">
                <label class="margin-bottom-5">Callcenter:</label><br>
                <div class="select callcenter-selection-product-sales-report margin-right-10">
                    <div class="frm-custom-dropdown">
                        <div class="frm-custom-dropdown-txt"><input type="text" int-value="0" class="dd-txt" name="callcenter_select" value="All Callcenter"></div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option">
                            <div class="option" int-value="0">All Callcenter</div>
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                    </select>
                </div>
            </div>

            <!-- filter by store-->
            <div class="f-left margin-top-20">
                <label class="margin-bottom-5">Store:</label><br>
                <div class="select store-selection-product-sales-report margin-right-10">
                    <div class="frm-custom-dropdown">
                        <div class="frm-custom-dropdown-txt"><input type="text" int-value="0" class="dd-txt" name="store_select" value="All Store"></div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option">
                           <div class="option" int-value="0">All Store</div> 
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                    </select>
                </div>
            </div>

            <button type="button" class="btn btn-dark margin-left-10 margin-top-40 generate_by_date"><i class="fa fa-spinner fa-pulse hidden"></i> Generate Report</button>

        </div>  

        <div class="row margin-top-40">
            <div class="contents line">
                <hr>
            </div>
        </div>

    </div>
    
    <div class="contents text-center margin-top-100 no_report">
        <p class="font-50"><img alt="report image" src="<?php echo base_url();?>assets/images/report_1_.svg"></p>
        <h4 class="gray-color margin-top-30 font-20">Please select date range <br>to generate report </h4>
    </div>
</section>


<section header-content="category_report" class="container-fluid header hidden" section-style="top-panel" id = 'sales_reports_by_category_header'>
    <div class="row header-container">
        <div class="contents">
            <div class="clear"></div>
            <h1 class="f-left">Category Sales Report</h1>
            <div class="f-right margin-top-20">
                <button class="btn btn-dark margin-right-20 download_excel_file">Download Excel File <i class="fa fa-spinner fa-pulse hidden"></i> </button>
                <!-- <button class="btn btn-dark">FAQ</button>-->
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="row">
        <div class="contents margin-top-20">

            <div class="display-inline-mid generate_by_date">
                <!-- date from -->
                <div class="f-left margin-right-10 ">
                    <label class="margin-bottom-5">Date From:</label><br>
                    <div class="date-picker" id="csr_date_from">
                        <input type="text" name="psr_date_from" readonly>
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                </div>
                <!-- date from -->
                <div class="f-left">
                    <label class="margin-bottom-5">Date To:</label><br>
                    <div class="date-picker" id="csr_date_to">
                        <input type="text" name="psr_date_to" readonly>
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                </div>
                <!-- generate report -->
                <!-- <button type="button" class="btn btn-dark margin-left-10 margin-top-20 generate_by_date">Generate Report<i class="fa fa-spinner fa-pulse hidden"></i></button> -->
                <div class="clear"></div>
            </div>


            <div class="display-inline-mid generate_by_period">
                <section class="container-fluid" section-style="content-panel">
                    <!-- generate by period -->
<!--                    <div class="divider padding-left-20">-->
<!--                        <label>Generate by Period:</label>-->
<!--                        <br>-->
<!--                        <div class="select small">-->
<!--                            <div class="frm-custom-dropdown">-->
<!--                                <div class="frm-custom-dropdown-txt">-->
<!--                                    <input type="text" class="dd-txt"  id="psr_period_date_to">-->
<!--                                </div>-->
<!--                                <div class="frm-custom-icon"></div>-->
<!--                                <div class="frm-custom-dropdown-option">-->
<!--                                    <div class="option" data-value="Hourly">Hourly</div>-->
<!--                                    <div class="option" data-value="Daily">Daily</div>-->
<!--                                    <div class="option" data-value="weekly">Weekly</div>-->
<!--                                    <div class="option" data-value="monthly">Monthly</div>-->
<!--                                </div>-->
<!--                                <select class="frm-custom-dropdown-origin" style="display: none;">-->
<!--                                    <option value="monthly">Monthly</option>-->
<!--                                </select>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <button type="button" class="btn btn-dark margin-left-20 generate_by_period">Generate Report<i class="fa fa-spinner fa-pulse hidden"></i></button>-->
<!--                    </div>-->
                </section>

            </div>
            <!-- <div class="margin-top-20">
                <hr>
            </div> -->
        </div>
    </div>
    <div class="row filter_table_container">
        <div class="contents margin-top-15 ">
            <!-- search -->
            <div class="f-left hidden">
                <label class="margin-bottom-5">search:</label><br>
                <input class="search f-left medium" type="text" name="search_filter">
            </div>
            <!-- search by -->
            <div class="f-left margin-left-20 hidden">
                <label class="margin-bottom-5">search by:</label><br>
                <div class="select medium">
                    <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" name="search_by_filter" value="Product Name" ></div><div class="frm-custom-icon"></div><div class="frm-custom-dropdown-option"><div class="option" data-value="Product Name">Product Name</div></div></div><select class="frm-custom-dropdown-origin" style="display: none;">
                        <option value="Product Name">Product Name</option>
                    </select>
                </div>
            </div>

            <button class="f-left btn btn-dark margin-top-20 margin-left-20 filter_table hidden">Search <i class="fa fa-spinner fa-pulse hidden"></i></button>
            <div class="clear"></div>
            <!-- filter by provinces-->
            <div class="f-left margin-top-20">
                <label class="margin-bottom-5">Province:</label><br>
                <div class="select province-selection-category-sales-report margin-right-10 small">
                    <div class="frm-custom-dropdown">
                        <div class="frm-custom-dropdown-txt"><input type="text" int-value="0" class="dd-txt" name="province_select" value="All Provinces"></div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option">
                            <div class="option" int-value="0">All Provinces</div>
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                    </select>
                </div>
            </div>

            <!-- filter by transactions-->
            <div class="f-left margin-top-20">
                <label class="margin-bottom-5">Type:</label><br>
                <div class="select transaction-selection-category-sales-report margin-right-10">
                    <div class="frm-custom-dropdown">
                        <div class="frm-custom-dropdown-txt"><input type="text" data-value="All Transaction" class="dd-txt" name="transaction_select" value="All Transactions"></div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option">
                            <div class="option" data-value="All Transaction">All Transactions</div>
                            <div class="option" data-value="Store Channel">Call Order</div>
                            <div class="option" data-value="Web">Web Order</div>
                            <div class="option" data-value="SMS">SMS Order</div>
                            <div class="option" data-value="nkag">NKAG Order</div>
                            <div class="option" data-value="Food Panda">Food Panda</div>
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                    </select>
                </div>
            </div>

            <!-- filter by callcenters-->
            <div class="f-left margin-top-20">
                <label class="margin-bottom-5">Callcenter:</label><br>
                <div class="select callcenter-selection-category-sales-report margin-right-10">
                    <div class="frm-custom-dropdown">
                        <div class="frm-custom-dropdown-txt"><input type="text" int-value="0" class="dd-txt" name="callcenter_select" value="All Callcenter"></div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option">
                        <div class="option" int-value="0">Show All Callcenter</div>
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                    </select>
                </div>
            </div>

            <!-- filter by store-->
            <div class="f-left margin-top-20">
                <label class="margin-bottom-5">Store:</label><br>
                <div class="select store-selection-category-sales-report margin-right-10">
                    <div class="frm-custom-dropdown">
                        <div class="frm-custom-dropdown-txt"><input type="text" int-value="0" class="dd-txt" name="store_select" value="All Store"></div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option">
                            <div class="option" int-value="0">Show All Store</div>
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                    </select>
                </div>
            </div>

            <button type="button" class="btn btn-dark margin-left-10 margin-top-40 generate_by_date">Generate Report<i class="fa fa-spinner fa-pulse hidden"></i></button>

        </div>

        <div class="row margin-top-40">
            <div class="contents line">
                <hr>
            </div>
        </div>

    </div>
    
    <div class="contents text-center margin-top-100 no_report">
        <p class="font-50"><img alt="report image" src="<?php echo base_url();?>assets/images/report_1_.svg"></p>
        <h4 class="gray-color margin-top-30 font-20">Please select date range <br>to generate report </h4>
    </div>

</section>

<section header-content="hourly_report" class="container-fluid header hidden" section-style="top-panel" id = 'hourly_report_header'>
    <div class="row header-container">
        <div class="contents">
            <h1 class="f-left">Hourly Sales Report</h1>
            <div class="f-right margin-top-20">
                <button class="btn btn-dark margin-right-10 hourly_sales_report_xls_button">Download Excel File</button>
                <!-- <button class="btn btn-dark ">FAQ</button> -->
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="row">
        <div class="contents margin-top-20">
            <div class="display-inline-mid generate_by_date">
                <!-- date from -->
                <div class="f-left margin-right-10 ">
                    <label class="margin-bottom-5">Date From:</label><br>
                    <div class="date-picker" id="hourly-sales-report-date-from">
                        <input type="text" name="hourly_sales_report_date_from" readonly>
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                </div>
                <!-- date from -->
                <div class="f-left margin-right-10 ">
                    <label class="margin-bottom-5">Date To:</label><br>
                    <div class="date-picker" id="hourly-sales-report-date-to">
                        <input type="text" name="hourly_sales_report_date_to" readonly>
                        <span class="fa fa-calendar text-center red-color"></span>
                    </div>
                </div>
                <div class="f-left margin-right-10 ">
                    <label class="margin-bottom-5">Province:</label><br>
                    <div class="select hourly_province_selection">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt"><input type="text" int-value="0" class="dd-txt" name="hourly_provinces" value="Show All Provinces" readonly></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option">
                                <div class="option province_select" int-value="0">Show All Provinces</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="f-left margin-right-10 ">
                    <label class="margin-bottom-5">Type:</label><br>
                    <div class="select hourly_transaction_selection">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" class="dd-txt" name="hourly_transactions" value="Show All Transactions" autocomplete="off" readonly="" data-value="Show All Transactions">
                            </div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option option-visible" style="display: block;">
                                <div class="option transaction_select" data-value="Show All Transactions">Show All Transactions</div>
                                <div class="option transaction_select" data-value="Store Channel">Call Orders</div>
                                <div class="option transaction_select" data-value="Web">Web Orders</div>
                                <div class="option transaction_select" data-value="SMS">Sms Orders</div>
                                <div class="option transaction_select" data-value="nkag">Nkag Orders</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="Show All Transactions" selected="">Show All Transactions</option>
                            <option value="Call Orders" data-value="Store Channel">Call Orders</option>
                            <option value="Web Orders" data-value="Web">Web Orders</option>
                            <option value="Sms Orders" data-value="SMS">Sms Orders</option>
                            <option value="Nkag Orders" data-value="nkag">Nkag Orders</option>
                        </select>
                    </div>
                </div>
                <div class="f-left margin-right-10 margin-top-20">
                    <label class="margin-bottom-5">Callcenter:</label><br>
                    <div class="select hourly_callcenter_selection">
                        <div class="frm-custom-dropdown font-0">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" class="dd-txt" name="hourly_callcenters" value="Show All Call Centers" autocomplete="off" int-value="0" readonly>
                            </div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option" style="display: none;">
                                <div class="option callcenter_select" int-value="0">Show All Call Centers</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="Show All Call Centers" selected="">Show All Call Centers</option>
                        </select>
                    </div>
                </div>
                <div class="f-left margin-right-10 margin-top-20">
                    <label class="margin-bottom-5">Store:</label><br>
                    <div class="select hourly_store_selection">
                        <div class="frm-custom-dropdown font-0">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" class="dd-txt" name="hourly_stores" value="Show All Stores" autocomplete="off" int-value="0" readonly>
                            </div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option" style="display: none;">
                                <div class="option store_select" int-value="0">Show All Stores</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="Show All Stores" selected="">Show All Stores</option>
                        </select>
                    </div>
                </div>
                <div class="f-left margin-right-10 margin-top-20">
                    <!-- generate report -->
                    <button type="button" class="btn btn-dark / margin-top-20 generate_by_date"><i class="fa fa-spinner fa-pulse hidden"></i> Generate Report</button>
                </div>
                <div class="clear"></div>
            </div>
            <div class="display-inline-mid  padding-left-10 margin-left-10">
                <section class="container-fluid" section-style="content-panel">        
                    <!-- generate by period -->
<!--                    <div class="divider padding-left-20">-->
<!--                        <label>Generate by Period:</label><br>                            -->
<!--                        <div class="select small">-->
<!--                            <select>-->
<!--                                <option value="Daily">Daily</option>-->
<!--                                <option value="Weekly">Weekly</option>-->
<!--                                <option value="Monthly">Monthly</option>-->
<!--                            </select>-->
<!--                        </div>-->
<!--                        -->
<!--                        <button type="button" class="btn / btn-dark / margin-left-20 generate_by_period">Generate Report</button>-->
<!--                    </div>                                    -->
                </section>
            </div>
            <div class="margin-top-20">
                <hr>
            </div>
        </div>            
    </div>
</section>
<!--END HEADERS-->


<!--CONTENT START-->
<section class="container-fluid content" section-style="content-panel" >
    <!--agent lists-->

    <div content="agent_list" class="row agent_list agent-list-container ">

        <!-- add data manager -->
        <div style="width: 100%; max-width: 900px; 	box-shadow: 0 0px 0px rgba(0,0,0,0) !important;" class="content-container custom-pagination">
            <ul class="paganationss prod-avail-custom-pagination1" id="paginationss">

            </ul>
            <div class="f-right">
                <p class="bggray-white font-14 black-color"></p>
                <strong> <span class="agent_search_result">0</span> Search Result</strong><p></p>
            </div>
        </div>

        <div class="hidden">
            <ul class="paganations-hidden content-container">

            </ul>
        </div>

        <div class="content-container hidden add-agent-container">
            <div>
                <section class="notification">
                    <div class="error-msg dark-red-bg hidden">
                        <div class=" margin-left-10  margin-left-20">
                            <i class="fa fa-exclamation-triangle fa-2x  f-left  margin-top-5 "></i>
                            <span class=" f-left  margin-top-10  margin-bottom-10  margin-left-20 error-message-text">Please fill up all required fields.</span>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <div class="success-msg green-bg hidden">
                        <div class=" margin-left-10  margin-left-20">
                            <i class="fa fa-check fa-2x  f-left  margin-top-5 "></i>
                            <span class=" f-left  margin-top-10  margin-bottom-10  margin-left-20">Adding Users Successful!</span>
                            <div class="clear"></div>
                        </div>
                    </div>
                </section>


                <h2>Add New User</h2>
                <div>

                    <div class="f-left  margin-right-50  margin-left-30  ">

                        <!-- upload image -->
                        <div class="margin-left-50  padding-left-40  margin-top-15">
                            <div class="sphere-camera  margin-left-50">
                                <i class="fa fa-camera fa-2x  gray-color  margin-top-20"></i>
                                <p class="gray-color font-12"><strong>Upload Image</strong></p>
                            </div>
                            <input type="file" name="client_img" class="hidden" />
                        </div>
                        <div class="clear  margin-bottom-20"></div>
                        <form id="add_agent_form">


                        <!-- forms -->
                        <label>Username: <span style="color:red; font-size: 16px; ">*</span></label>
                        <br>
                        <input type="text" class="large" name="username" datavalid="required" labelinput="Username" autocomplete="off"><!-- <span style="color:red; font-size: 20px; ">*</span> -->
                        <br>
                        <label>Password: <span style="color:red; font-size: 16px; ">*</span></label>
                        <br>
                        <input type="password" name="password" datavalid="required" labelinput="Password" class="large" autocomplete="off"><!-- <span style="color:red; font-size: 20px; ">*</span> -->
                        <br>
                        <label>Repeat Password: <span style="color:red; font-size: 16px; ">*</span></label>
                        <br>
                            <input type="password" name="confirm_password" datavalid="required" labelinput="Confirm Password" class="large"><!-- <span style="color:red; font-size: 20px; ">*</span> -->
                        <br>
                        <label>Firstname: <span style="color:red; font-size: 16px; ">*</span></label>
                        <br>
                        <input type="text" class="large" name="first_name" datavalid="alpha_numeric required" labelinput="First Name" ><!-- <span style="color:red; font-size: 20px; ">*</span> -->
                        <br>
                        <label>Middle name: <span style="color:red; font-size: 16px; ">*</span></label>
                        <br>
                        <input type="text" class="large" name="middle_name" datavalid="alpha_numeric required" labelinput="Middle Name"><!-- <span style="color:red; font-size: 20px; ">*</span> -->
                        <br>
                        <label>Last Name: <span style="color:red; font-size: 16px; ">*</span></label>
                        <br>
                        <input type="text" class="large" name="last_name" datavalid="alpha_numeric required" labelinput="Last Name"><!-- <span style="color:red; font-size: 20px; ">*</span> -->
                        <br>
                        <label>Email Address: <span style="color:red; font-size: 16px; ">*</span></label>
                        <br>
                        <input type="text" class="large" name="email_address" datavalid="email required" labelinput="Email Address"><!-- <span style="color:red; font-size: 20px; ">*</span> -->
                        <br>
                        <label>Call Center: <span style="color:red; font-size: 16px; ">*</span></label>
                        <br>

                            <div class="select large width-230px callcenters-choices">
                                <div class="frm-custom-dropdown">
                                    <div class="frm-custom-dropdown-txt">
                                        <input type="text" class="dd-txt" labelinput="Callcenter" name="callcenter_id" datavalid="required"></div>
                                    <div class="frm-custom-icon f-right"></div>
                                    <div class="frm-custom-dropdown-option">

                                    </div>
                                </div>
                                <select class="frm-custom-dropdown-origin" style="display: none;">
                                    <option value="PHUB-Agents">PHUB Agents</option>
                                </select>
                            </div><!-- <span style="color:red; font-size: 20px;">*</span> -->
                            <br>
                            <label>User Type: <span style="color:red; font-size: 16px; ">*</span></label>
                            <br>

                            <div class="select large width-230px callcenters-role">
                                <div class="frm-custom-dropdown">
                                    <div class="frm-custom-dropdown-txt">
                                        <input type="text" class="dd-txt" labelinput="Callcenter" name="callcenter_role" int-value="1" value="Agent" datavalid="required" labelinput="Role"></div>
                                    <div class="frm-custom-icon f-right"></div>
                                    <div class="frm-custom-dropdown-option">
                                        <div class="option agent-type" data-value="1"> Agent </div>
                                        <div class="option agent-type" data-value="2"> Coordinator </div>
                                        <?php if($this->session->userdata('user_level') != 4) { ?>
                                        <div class="option agent-type" data-value="3"> Admin </div>
                                        <div class="option agent-type" data-value="4"> Management </div>
                                        <div class="option agent-type" data-value="6"> Store Admin </div>
                                        <?php }; ?>
                                    </div>
                                </div>
                                <select class="frm-custom-dropdown-origin" style="display: none;">
                                    <option value="PHUB-Agents">PHUB Agents</option>
                                </select>
                            </div><!-- <span style="color:red; font-size: 20px;">*</span> -->
                    </div>
                    <div class="f-left">
                        <p class="font-20  margin-bottom-10  margin-top-20">Assign Area Coverage</p>
                        <div class="bggray-dark ">
                            <div class="padding-all-5">
                                <label>Search Province</label>
                                <br>
                                <input type="text" class="large" id="add_agent_province">
                            </div>
                            <div class="div-inside extralong">
                                <table class="table-inside provinces-choices">
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            <div>
                                <p class="padding-all-5"><!--<strong>Selected Area: <info class="area-count">0</info></strong>--></p>
                            </div>
                        </div>
                        <div class="form-group padding-top-10">
                            <input class="chck-box" name="24_hours" id="ip-whitelist-exclude" type="checkbox">
                            <label class="chck-lbl" for="ip-whitelist-exclude">Exclude from IP Whitelist</label>
                        </div>
                    </div>

                    <div class="clear"></div>

                </div>
            </div>
            </form>
            <div class="f-right  margin-right-10  margin-top-20">
                <button type="button" class="btn btn-dark margin-right-10 add_agent_button">Submit</button>
                <button type="button" class="btn btn-dark close_add_agent">Cancel</button>
            </div>

            <div class="clear"></div>
        </div>

        <div class="content-container hidden edit-agent-container">
            <div>
                <section class="notification">
                    <div class="error-msg dark-red-bg hidden">
                        <div class=" margin-left-10  margin-left-20">
                            <i class="fa fa-exclamation-triangle fa-2x  f-left  margin-top-5 "></i>
                            <span class=" f-left  margin-top-10  margin-bottom-10  margin-left-20 error-message-text">Please fill up all required fields.</span>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <div class="success-msg green-bg hidden">
                        <div class=" margin-left-10  margin-left-20">
                            <i class="fa fa-check fa-2x  f-left  margin-top-5 "></i>
                            <span class=" f-left  margin-top-10  margin-bottom-10  margin-left-20">Editing User Successful!</span>
                            <div class="clear"></div>
                        </div>
                    </div>
                </section>


                <h2>Edit User</h2>
                <div>

                    <div class="f-left  margin-right-50  margin-left-30  ">

                        <!-- upload image -->
                        <div class="margin-left-50  padding-left-40  margin-top-15">
                            <div class="sphere-camera  margin-left-50">
                                <i class="fa fa-camera fa-2x  gray-color  margin-top-20"></i>
                                <p class="gray-color font-12"><strong>Upload Image</strong></p>
                            </div>
                            <input type="file" name="client_img" class="hidden" />
                        </div>
                        <div class="clear  margin-bottom-20"></div>
                        <form id="edit_agent_form">


                            <!-- forms -->
                            <label>Username: <span style="color:red; font-size: 16px; ">*</span></label>
                            <br>
                            <input type="hidden" name="user_id" />
                            <input type="text" class="large" name="username" datavalid="required" labelinput="Username" autocomplete="off"><!-- <span style="color:red; font-size: 20px; ">*</span> -->
                            <br>
                            <label>New Password:<!--  <span style="color:red; font-size: 16px; ">*</span> --></label>
                            <br>
                            <input type="password" name="password" placeholder="New Password" labelinput="Password" class="large" autocomplete="off"><!-- <span style="color:red; font-size: 20px; ">*</span> -->
                            <br>
                            <label>Repeat New Password:<!--  <span style="color:red; font-size: 16px; ">*</span> --></label>
                            <br>
                            <input type="password" name="confirm_password" placeholder="Confirm New Password" labelinput="Confirm Password" class="large"><!-- <span style="color:red; font-size: 20px; ">*</span> -->
                            <br>
                            <label>Firstname: <span style="color:red; font-size: 16px; ">*</span></label>
                            <br>
                            <input type="text" class="large" name="first_name" datavalid="required" labelinput="First Name" ><!-- <span style="color:red; font-size: 20px; ">*</span> -->
                            <br>
                            <label>Middle name: <!-- <span style="color:red; font-size: 16px; ">*</span> --></label>
                            <br>
                            <input type="text" class="large" name="middle_name" labelinput="Middle Name"><!-- <span style="color:red; font-size: 20px; ">*</span> -->
                            <br>
                            <label>Last Name: <span style="color:red; font-size: 16px; ">*</span></label>
                            <br>
                            <input type="text" class="large" name="last_name" datavalid="required" labelinput="Last Name"><!-- <span style="color:red; font-size: 20px; ">*</span> -->
                            <br>
                            <label>Email Address: <span style="color:red; font-size: 16px; ">*</span></label>
                            <br>
                            <input type="text" class="large" name="email_address" datavalid="email required" labelinput="Email Address"><!-- <span style="color:red; font-size: 20px; ">*</span> -->
                            <br>
                            <label>Call Center: <span style="color:red; font-size: 16px; ">*</span></label>
                            <br>

                            <div class="select large width-230px callcenters-choices">
                                <div class="frm-custom-dropdown">
                                    <div class="frm-custom-dropdown-txt">
                                        <input type="text" class="dd-txt" labelinput="Callcenter" name="callcenter_id" labelinput="Callcenter" datavalid="required"></div>
                                    <div class="frm-custom-icon f-right"></div>
                                    <div class="frm-custom-dropdown-option">

                                    </div>
                                </div>
                                <select class="frm-custom-dropdown-origin" style="display: none;">
                                    <option value="PHUB-Agents">PHUB Agents</option>
                                </select>
                            </div><!-- <span style="color:red; font-size: 20px;">*</span> -->

                            <br>
                            <label>User Type: <span style="color:red; font-size: 16px; ">*</span></label>
                            <br>

                            <div class="select large width-230px callcenters-role">
                                <div class="frm-custom-dropdown">
                                    <div class="frm-custom-dropdown-txt">
                                        <input type="text" class="dd-txt" labelinput="Callcenter" name="callcenter_role" labelinput="Role" datavalid="required" int-value="1" value="Agent"></div>
                                    <div class="frm-custom-icon f-right"></div>
                                    <div class="frm-custom-dropdown-option">
                                        <div class="option agent-type" data-value="1"> Agent </div>
                                        <div class="option agent-type" data-value="2"> Coordinator </div>
                                        <?php if($this->session->userdata('user_level') != 4) { ?>
                                            <div class="option agent-type" data-value="3"> Admin </div>
                                            <div class="option agent-type" data-value="4"> Management </div>
                                            <div class="option agent-type" data-value="6"> Store Admin </div>
                                        <?php }; ?>
                                    </div>
                                </div>
                                <select class="frm-custom-dropdown-origin" style="display: none;">
                                    <option value="PHUB-Agents">PHUB Agents</option>
                                </select>
                            </div><!-- <span style="color:red; font-size: 20px;">*</span> -->
                    </div>
                    <div class="f-left">
                        <p class="font-20  margin-bottom-10  margin-top-20">Assign Area Coverage</p>
                        <div class="bggray-dark ">
                            <div class="padding-all-5">
                                <label>Search Province</label>
                                <br>
                                <input type="text" class="large" id="edit_agent_province">
                            </div>
                            <div class="div-inside extralong">
                                <table class="table-inside provinces-choices-edit">
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                            <div>
                                <p class="padding-all-5"><!--<strong>Selected Area: <info class="area-count">0</info></strong>--></p>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="form-group padding-top-10">
                        <input class="chck-box" name="24_hours" id="ip-whitelist-exclude-edit" type="checkbox">
                        <label class="chck-lbl" for="ip-whitelist-exclude-edit">Exclude from IP Whitelist</label>

                        <input class="chck-box" name="24_hours" id="is-blacklisted" type="checkbox">
                        <label class="chck-lbl margin-left-50" for="is-blacklisted">Blacklisted User</label>
                    </div> -->

                    <div class="form-group f-right margin-top-20 no-margin-bottom">
                            <input id="ip-whitelist-exclude-edit" type="radio" class="radio-box" name="ip-whitelist-exclude-edit" checked="true" />
                            <label class="radio-lbl padding-right-50" for="ip-whitelist-exclude-edit">Exclude from IP Whitelist</label>
                            <input id="is-blacklisted" type="radio" class="radio-box" name="ip-whitelist-exclude-edit"/>
                            <label class="radio-lbl padding-right-50" for="is-blacklisted">Blacklist User</label>
                    </div>

                    <div class="clear"></div>

                </div>
            </div>
            </form>
            <div class="f-right  margin-right-10  margin-top-20">
                <button type="button" class="btn btn-dark margin-right-10 edit_agent_button">Submit <i class="fa fa-spinner fa-pulse hidden"></i></button>
                <button type="button" class="btn btn-dark close_add_agent">Cancel</button>
            </div>

            <div class="clear"></div>
        </div>



        <div class="content-container hidden notification-container">
            <div class="error-msg light-yellow-bg">
                <img src="<?php echo assets_url() ?>/images/warn.svg" alt="warning logo" class="f-left  margin-right-10" />
                <p class="f-left  font-16  margin-top-5">Carolyn Mendoza has been moved to the archives</p>
                <p class="f-right  margin-top-5 "><a href="#" class="white-color  underline  ">UNDO</a>
                <div class="clear"></div>
            </div>
        </div>

    </div>
    <!--agent lists end-->

    <!--archived agent list-->
    <div content="archive_agent_list" class="row hidden archive_agent_list">

        <!-- sample-1-->
        <div class="content-container viewable">
            <div>

                <h2 class="f-left">Carolyn A. Mendoza</h2>
                <h2 class="f-right">ARCHIVED</h2>
                <div class="clear  margin-bottom-10"></div>

                <div class="f-left   margin-right-30  margin-left-10 sphere-big">
                    <h1>CM</h1>
                </div>

                <div class=" f-left  width-15per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username:</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Email Address: </span> </strong> </p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Password: </span> </strong></p>
                </div>


                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>phub_cmendoza02</strong></p>
                    <p class="font-14 margin-bottom-5"><strong>cmendoza@gmail.com </strong> </p>
                    <p class="font-14 margin-bottom-5"><strong>********** <!--<a href="#" class="margin-left-10"><i class="fa fa-eye disabled"></i>Show Password</a>--> </strong></p>
                </div>


                <div class=" f-left width-15per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Call Center: </span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Last Since: </span></strong></p>

                </div>

                <div class=" f-left  ">
                    <p class="font-14 margin-bottom-5"><strong>Agent</strong></p>
                    <p class="font-14 margin-bottom-5"><strong>PHUB</strong></p>
                    <p class="font-14 margin-bottom-5"><strong>05-17-16 | 3:23 </strong></p>

                </div>

                <div class="clear"></div>

                <div class="f-right  margin-top-10">
                    <button type="button" class="btn btn-dark margin-right-10">Edit User Information</button>
                    <button type="button" class="btn btn-dark">Activate Account</button>
                </div>
                <div class="clear"></div>

                <div class="bggray-dark  margin-top-20  padding-all-20">
                    <p class="f-left"><strong>Assigned Area Coverage</strong></p>
                    <p class="f-right"><strong>4 Areas</strong></p>
                    <div class="clear"></div>

                    <div class="margin-top-10">
                        <button type="button" class="btn btn-dark  margin-right-10" disabled="">Metro Manila</button>
                        <button type="button" class="btn btn-dark  margin-right-10" disabled="">Bulacan</button>
                        <button type="button" class="btn btn-dark  margin-right-10" disabled="">Batangas</button>
                        <button type="button" class="btn btn-dark  margin-right-10" disabled="">Cagayan</button>
                    </div>

                </div>

            </div>

        </div>
        <!-- sample 2 -->
        <div class="content-container viewable">
            <div>
                <div class="f-left  sphere-small  margin-right-20">
                    <p class="font-20">LM</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Lance Madrona</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Agent </p>
                </div>
                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span></strong>phub_lmadrona14</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Call Center: </span> </strong>PHUB</p>

                </div>

                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong>ARCHIVED</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Since: </span>05-17-16 | 3:23 AM</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- sample 3 -->
        <div class="content-container viewable">
            <div>
                <div class="f-left  sphere-small margin-right-20">
                    <p class="font-20">MM</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Maria Mamita</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Agent</p>
                </div>

                <div class=" f-left width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span></strong>phub_mmammita14</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Call Center: </span> </strong>PHUB</p>

                </div>
                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong>ARCHIVED</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Since: </span>05-17-16 | 3:23 AM</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- sample 4 -->
        <div class="content-container viewable">
            <div>
                <div class="f-left  sphere-small  margin-right-20">
                    <p class="font-20">KA</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Katheleen Kaye Aliman</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Agent</p>
                </div>

                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span></strong>phub_kaliman01</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Call Center: </span> </strong>PHUB</p>

                </div>

                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong>ARCHIVED</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Since: </span>05-17-16 | 3:23 AM</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- sample 4 -->
        <div class="content-container viewable">
            <div>
                <div class="f-left  sphere-small  margin-right-20">
                    <p class="font-20">GG</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Gary Guitierez</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Agent </p>
                </div>
                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span></strong>phub_gguitierrez57</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Call Center: </span> </strong>PHUB</p>

                </div>

                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong>ARCHIVED</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Since: </span>05-17-16 | 3:23 AM</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- sample 5 -->
        <div class="content-container viewable">
            <div>
                <div class="f-left sphere-small margin-right-20">
                    <p class="font-20">WP</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Warren Pando</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Agent</p>
                </div>

                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span></strong>phub_cmendoza02</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Call Center: </span> </strong>PHUB</p>

                </div>

                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong>ARCHIVED</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Since: </span>05-17-16 | 3:23 AM</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- sample 6 -->
        <div class="content-container viewable">
            <div>
                <div class="f-left  sphere-small  margin-right-20">
                    <p class="font-20">TL</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Thomas Lapid</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Agent</p>
                </div>

                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span></strong>phub_tlapid89</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Call Center: </span> </strong>PHUB</p>

                </div>

                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong>ARCHIVED</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Since: </span>05-17-16 | 3:23 AM</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- sample 7 -->
        <div class="content-container viewable">
            <div>
                <div class="f-left  sphere-small  margin-right-20">
                    <p class="font-20">DL</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Darren Lopez</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Agent </p>
                </div>
                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span></strong>phub_cmendoza02</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Call Center: </span> </strong>PHUB</p>

                </div>

                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong>ARCHIVED</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Since: </span>05-17-16 | 3:23 AM</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <!--archived agent list end-->

    <!--data managet list-->
    <div content="data_manager_list" class="row hidden data_manager_list">

        <div class="content-container">
            <div>
                <div class="error-msg dark-red-bg ">
                    <div class=" margin-left-10  margin-left-20">
                        <i class="fa fa-exclamation-triangle fa-2x  f-left  margin-top-5 "></i>
                        <span class=" f-left  margin-top-10  margin-bottom-10  margin-left-20">Opps! The entered password do not match. Please correct them and click submit.</span>
                        <div class="clear"></div>
                    </div>

                </div>

                <h1>Add Data Manager</h1>
                <div class="width-60per  margin-bottom-20  margin-top-">
                    <div class="sphere-camera  margin-right-30  f-right">
                        <i class="fa fa-camera fa-2x  gray-color  margin-top-20"></i>
                        <p class="gray-color  font-12"><strong>Upload Image</strong></p>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="">
                    <div class="f-left  margin-right-50  margin-left-30 /">
                        <label>Username:</label>
                        <br>
                        <input type="text" class="large">
                        <br>
                        <label>Password:</label>
                        <br>
                        <input type="password" class="large  error-form">
                        <br>
                        <label>Repeat Password:</label>
                        <br>
                        <input type="password" class="large  error-form">
                    </div>

                    <div class="f-left ">
                        <label>Firstname:</label>
                        <br>
                        <input type="text" class="large">
                        <br>
                        <label>Middle name:</label>
                        <br>
                        <input type="text" class="large">
                        <br>
                        <label>Last Name:</label>
                        <br>
                        <input type="text" class="large">
                        <br>
                        <label>Email Address:</label>
                        <br>
                        <input type="text" class="large">
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="f-right  margin-right-10  margin-top-20">
                    <button type="button" class="btn btn-dark  margin-right-10">Submit</button>
                    <button type="button" class="btn btn-dark">Cancel</button>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- big sample -->
        <div class="content-container">
            <div>

                <h2 class="f-left">Carolyn A. Mendoza</h2>
                <h2 class="f-right red-color status">OFFLINE</h2>
                <div class="clear  margin-bottom-10"></div>

                <div class="f-left   margin-right-30  margin-left-10  sphere-big">
                    <h1>CM</h1>
                </div>

                <div class=" f-left  width-15per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username:</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Email Address: </span> </strong> </p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Password: </span> </strong></p>
                </div>


                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>phub_cmendoza02</strong></p>
                    <p class="font-14 margin-bottom-5"><strong>cmendoza@gmail.com </strong> </p>
                    <p class="font-14 margin-bottom-5"><strong>********** <!--<a href="#" class="margin-left-10"><i class="fa fa-eye disabled"></i>Show Password</a> --></strong></p>
                </div>


                <div class=" f-left  width-15per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Last Online: </span></strong></p>

                </div>

                <div class=" f-left ">
                    <p class="font-14 margin-bottom-5"><strong>Data Manager</strong></p>
                    <p class="font-14 margin-bottom-5"><strong>05-17-16 | 3:23 PM</strong></p>

                </div>


                <div class="clear"></div>
                <div class="f-right">
                    <button type="button" class="btn btn-dark  margin-right-10">Archived User</button>
                    <button type="button" class="btn btn-dark">Edit User Information</button>
                </div>
                <div class="clear"></div>

            </div>
        </div>
        <!-- sample-1-->
        <div class="content-container">
            <div>
                <div class="f-left sphere-small  margin-right-20">
                    <p class="font-20">ML</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Mario I. lopez<span class="red-color"> New!</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Data Manager </p>
                </div>

                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span>cmendoza02</strong></p>

                </div>

                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color status">OFFLINE</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Last Online: </span>- | -</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- sample 2 -->
        <div class="content-container">
            <div>
                <div class="f-left  sphere-small  margin-right-20">
                    <p class="font-20">LM</p>
                </div>
                <div class=" f-left width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Lance Madrona</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Data Manager </p>
                </div>

                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span>madrona14</strong></p>

                </div>

                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">OFFLINE</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Last Online: </span>05-17-16 | 3:23 AM</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- sample 3 -->
        <div class="content-container">
            <div>
                <div class="f-left  sphere-small  margin-right-20">
                    <p class="font-20">MM</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Maria Mamita</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Data Manager </p>
                </div>

                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span>mmamita14</strong></p>

                </div>

                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">OFFLINE</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Last Online: </span>05-17-16 | 3:23 AM</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- sample 4 -->
        <div class="content-container">
            <div>
                <div class="f-left  sphere-small  margin-right-20">
                    <p class="font-20">KA</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Katheleen Kaye Aliman</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Data Manager </p>
                </div>

                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span>kaliman01</strong></p>

                </div>

                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">OFFLINE</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Last Online: </span>05-17-16 | 3:23 AM</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- sample 4 -->
        <div class="content-container">
            <div>
                <div class="f-left  sphere-small  margin-right-20">
                    <p class="font-20">GG</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Gary Guitierez</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Data Manager </p>
                </div>

                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span>gguitierez57</strong></p>

                </div>

                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">OFFLINE</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Last Online: </span>05-17-16 | 3:23 AM</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- sample 5 -->
        <div class="content-container">
            <div>
                <div class="f-left  sphere-small  margin-right-20">
                    <p class="font-20">WP</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Warren Pando</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Data Manager </p>
                </div>

                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span>wpando09</strong></p>

                </div>

                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">OFFLINE</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Last Online: </span>05-17-16 | 3:23 AM</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- sample 6 -->
        <div class="content-container">
            <div>
                <div class="f-left  sphere-small  margin-right-20">
                    <p class="font-20">TL</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Thomas Lapid<span class="red-color"> New!</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Data Manager </p>
                </div>

                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span>cmendoza02</strong></p>

                </div>

                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">OFFLINE</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Last Online: </span>05-17-16 | 3:23 AM</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- sample 7 -->
        <div class="content-container">
            <div>
                <div class="f-left  sphere-small  margin-right-20">
                    <p class="font-20">DL</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Darren Lopez<span class="red-color"> New!</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Data Manager </p>
                </div>

                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span>dlopez09</strong></p>

                </div>

                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">OFFLINE</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Last Online: </span>05-17-16 | 3:23 AM</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
    <!--data manager list end-->

    <!--store management list-->
    <div content="store_management_list" class="row hidden store_management_list">
        <div style="width: 100%; max-width: 900px; 	box-shadow: 0 0px 0px rgba(0,0,0,0) !important;" class="content-container custom-pagination">
            <ul class="paganationss prod-avail-custom-pagination1" id="store-paginationss">

            </ul>
            <div class="f-right">
                <p class="bggray-white font-14 black-color"></p>
                <strong> <span class="store_search_result">0</span> Search Result</strong><p></p>
            </div>
        </div>

        <div class="hidden">
            <ul class="paganations-hidden content-container" id="paginations" style="box-shadow: none !important; min-height: 40px; }">

            </ul>
        </div>
        <!--add store list-->
        <div class="content-container padding-top-30 add_store_list hidden">

            <section class="notification">
                    <div class="error-msg dark-red-bg hidden">
                        <div class=" margin-left-10  margin-left-20">
                            <i class="fa fa-exclamation-triangle fa-2x  f-left  margin-top-5 "></i>
                            <span class=" f-left  margin-top-10  margin-bottom-10  margin-left-20 error-message-text">Please fill up all required fields.</span>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <div class="success-msg green-bg hidden">
                        <div class=" margin-left-10  margin-left-20">
                            <i class="fa fa-check fa-2x  f-left  margin-top-5 "></i>
                            <span class=" f-left  margin-top-10  margin-bottom-10  margin-left-20">Adding Store Successful!</span>
                            <div class="clear"></div>
                        </div>
                    </div>
                </section>

            <h2 class="margin-left-50">Add New Store Account</h2>
            <form id="add_store">
            <div class="f-left  margin-left-30">
                <div> <!-- store name -->
                    <label>Store Name: <span style="color:red; font-size: 16px; ">*</span></label>
                    <br>
                    <input type="text" class="large" name="store_name" datavalid="required" labelinput="Store Name">
                </div>

                <div> <!-- store code -->
                    <label>Store code: <span style="color:red; font-size: 16px; ">*</span></label>
                    <br>
                    <input type="text" class="large" name="store_code" datavalid="required" labelinput="Store Code">
                </div>


                <div> <!-- area -->
                    <label>AREA:</label>
                    <br>
                     <div class="select xlarge width-350px provinces-store-list">
                        <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" int-value="1" value="Metro Manila" class="dd-txt" name="province"  datavalid="required" labelinput="Province"></div><div class="frm-custom-icon"></div><div class="frm-custom-dropdown-option"><div class="option" data-value="1">Metro Manila</div></div></div><select class="frm-custom-dropdown-origin" style="display: none;">

                        </select>
                    </div>
                </div>


                <div> <!-- rbu -->
                    <label>RBU:</label>
                    <br>
                    <div class="select xlarge width-350px">
                        <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" value="RBU1"></div><div class="frm-custom-icon"></div><div class="frm-custom-dropdown-option"><div class="option" data-value="RBU1">RBU1</div></div></div><select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="RBU1">RBU1</option>
                        </select>
                    </div>
                </div>

                <div> <!-- sbu -->
                    <label>SBU:</label>
                    <br>
                    <div class="select xlarge width-350px">
                        <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" value="Jollibee"></div><div class="frm-custom-icon"></div><div class="frm-custom-dropdown-option"><div class="option" data-value="Jollibee">Jollibee</div></div></div><select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="SBU1">SBU1</option>
                        </select>
                    </div>
                </div>

                <div> <!-- group -->
                    <label>Group:</label>
                    <br>
                    <div class="select xlarge width-350px">
                        <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" value="Store"></div><div class="frm-custom-icon"></div><div class="frm-custom-dropdown-option"><div class="option" data-value="Store">Store</div></div></div><select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="Goru">Group</option>
                        </select>
                    </div>
                </div>


                <div> <!-- district -->
                    <label>District:</label>
                    <br>
                    <div class="select xlarge width-350px">
                        <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" value="District 1"></div><div class="frm-custom-icon"></div><div class="frm-custom-dropdown-option"><div class="option" data-value="District-1">District 1</div><div class="option" data-value="District-2">District 2</div></div></div><select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="District-9">District 9</option>
                        </select>
                    </div>
                </div>


                <div> <!-- maximum -->
                    <label>Maximum Order Threshold: <span style="color:red; font-size: 16px; ">*</span></label>
                    <br>
                    <input type="text" class="large" name="max_order" datavalid="required numeric" labelinput="Max Order">
                </div>
            </div>

            <div class="f-left margin-left-20">

                <div > <!-- username -->
                    <label>Username: <span style="color:red; font-size: 16px; ">*</span></label>
                    <br>
                    <input type="text" class="large" name="username" datavalid="required" labelinput="Username">
                </div>
                <label>Password: <span style="color:red; font-size: 16px; ">*</span></label>
                <br>
                <input type="password" name="password" datavalid="required" labelinput="Password" class="large" autocomplete="off">
                <br>
                <label>Repeat Password: <span style="color:red; font-size: 16px; ">*</span></label>
                <br>
                <input type="password" name="confirm_password" datavalid="required" labelinput="Confirm Password" class="large">
                <br>
                <label>Email: <span style="color:red; font-size: 16px; ">*</span></label>
                <br>
                <input type="email" name="email" datavalid="required" labelinput="Email" class="large">

                <div> <!-- store ownership -->
                    <label>Store Ownership:</label>
                    <br>
                    <div class="select xlarge width-350px add_store_ownership">
                        <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" value="Company" name="ownership"></div><div class="frm-custom-icon"></div><div class="frm-custom-dropdown-option"><div class="option" data-value="Company">Company</div><div class="option" data-value="Franchise">Franchise</div></div></div><select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="Store Ownership">Store Ownership</option>
                        </select>
                    </div>
                </div>

                <label>Store Telco Provider: <span style="color:red; font-size: 16px; ">*</span></label>
                <br>
                <input type="text" name="store_telco_provider" datavalid="required" labelinput="Store Telco Provider" class="large">
                <br/>
                <label>Service IP: <span style="color:red; font-size: 16px; ">*</span></label>
                <br>
                <input type="text" name="service_ip" datavalid="required" labelinput="Servcie IP" class="large">

                <table>
                    <thead>
                        <tr>
                            <th colspan="2">
                                <label class="margin-top-10">Trading Hours:</label>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="2" class="gray-color font-bold">OPEN: <span style="color:red; font-size: 16px; ">*</span></td>
                            <td colspan="2" class="gray-color font-bold">CLOSE: <span style="color:red; font-size: 16px; ">*</span></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="date-picker-store time margin-right-15">
                                    <input type="text" data-date-format="MM/DD/YYYY" class=" width-80px" name="time_start" datavalid="required" labelinput="Operation Time Start">
                                    <span class="fa fa-clock-o text-center red-color"></span>
                                </div>
                            </td>

                            <td>to
                            </td>

                            <td>
                                <div class="date-picker-store time margin-left-15 margin-right-10">
                                    <input type="text" data-date-format="MM/DD/YYYY" class=" width-80px" name="time_end" datavalid="required" labelinput="Operation Time End">
                                    <span class="fa fa-clock-o text-center red-color"></span>
                                </div>
                            </td>

                            <td>
                                <div class="form-group padding-top-10">
                                    <input class="chck-box" name="24_hours" id="add-trading-time" type="checkbox">
                                    <label class="chck-lbl add-trading-time" for="add-trading-time">24 Hours</label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <!-- <table>
                    <thead>
                        <tr>
                            <th colspan="2">
                                <label class="margin-top-10">Operating Hours:</label>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="2" class="gray-color font-bold">OPEN: <span style="color:red; font-size: 16px; ">*</span></td>
                            <td colspan="2" class="gray-color font-bold">CLOSE: <span style="color:red; font-size: 16px; ">*</span></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="date-picker-store time margin-right-15">
                                    <input type="text" data-date-format="MM/DD/YYYY" class=" width-80px" datavalid="required" labelinput="Operation Time Start">
                                    <span class="fa fa-clock-o text-center red-color"></span>
                                </div>
                            </td>

                            <td>to
                            </td>

                            <td>
                                <div class="date-picker-store time margin-left-15 margin-right-10">
                                    <input type="text" data-date-format="MM/DD/YYYY" class=" width-80px" datavalid="required" labelinput="Operation Time End">
                                    <span class="fa fa-clock-o text-center red-color"></span>
                                </div>
                            </td>

                            <td>
                                <div class="form-group padding-top-10">
                                    <input class="chck-box" name="24_hours" type="checkbox" id="add-operating-time">
                                    <label class="chck-lbl" for="add-operating-time">24 Hours</label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table> -->

                <div class="form-group padding-top-10">
                    <input class="chck-box" name="24_hours" id="ip-whitelist-exclude-store-add" type="checkbox">
                    <label class="chck-lbl" for="ip-whitelist-exclude-store-add">Exclude from IP Whitelist</label>
                </div>

                <div class="f-right margin-top-10 margin-bottom-20">
                    <button type="button" class="btn btn-dark margin-right-10 add_store_submit">Submit</button>
                    <button type="button" class="btn btn-dark add_store_cancel">Cancel</button>
                </div>
                <div class="clear"></div>
                </form>
            </div>

            <div class="clear"></div>

        </div>


        <!--edit store list-->
        <div class="content-container padding-top-30 edit_store_list hidden">

            <section class="notification">
                    <div class="error-msg dark-red-bg hidden">
                        <div class=" margin-left-10  margin-left-20">
                            <i class="fa fa-exclamation-triangle fa-2x  f-left  margin-top-5 "></i>
                            <span class=" f-left  margin-top-10  margin-bottom-10  margin-left-20 error-message-text">Please fill up all required fields.</span>
                            <div class="clear"></div>
                        </div>
                    </div>

                    <div class="success-msg green-bg hidden">
                        <div class=" margin-left-10  margin-left-20">
                            <i class="fa fa-check fa-2x  f-left  margin-top-5 "></i>
                            <span class=" f-left  margin-top-10  margin-bottom-10  margin-left-20">Edit Store Successful!</span>
                            <div class="clear"></div>
                        </div>
                    </div>
                </section>

            <h2 class="margin-left-50">Edit Store Account</h2>
            <form id="edit_store">
            <input type="hidden" name="store_id" />
            <input type="hidden" name="user_id" />
            <div class="f-left  margin-left-30">
                <div> <!-- store name -->
                    <label>Store Name: <span style="color:red; font-size: 16px; ">*</span></label>
                    <br>
                    <input type="text" class="large" name="store_name" datavalid="required" labelinput="Store Name">
                </div>

                <div> <!-- store code -->
                    <label>Store code: <span style="color:red; font-size: 16px; ">*</span></label>
                    <br>
                    <input type="text" class="large" name="store_code" datavalid="required" labelinput="Store Code">
                </div>


                <div> <!-- area -->
                    <label>AREA:</label>
                    <br>
                     <div class="select xlarge width-350px provinces-store-list">
                        <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" int-value="1" value="Metro Manila" class="dd-txt" name="province"  datavalid="required" labelinput="Province"></div><div class="frm-custom-icon"></div><div class="frm-custom-dropdown-option"><div class="option province-select" data-value="1">Metro Manila</div></div></div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">

                        </select>
                    </div>
                </div>


                <div> <!-- rbu -->
                    <label>RBU:</label>
                    <br>
                    <div class="select xlarge width-350px">
                        <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" value="RBU1"></div><div class="frm-custom-icon"></div><div class="frm-custom-dropdown-option"><div class="option" data-value="RBU1">RBU1</div></div></div><select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="RBU1">RBU1</option>
                        </select>
                    </div>
                </div>

                <div> <!-- sbu -->
                    <label>SBU:</label>
                    <br>
                    <div class="select xlarge width-350px">
                        <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" value="Jollibee"></div><div class="frm-custom-icon"></div><div class="frm-custom-dropdown-option"><div class="option" data-value="Jollibee">Jollibee</div></div></div><select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="SBU1">SBU1</option>
                        </select>
                    </div>
                </div>

                <div> <!-- group -->
                    <label>Group:</label>
                    <br>
                    <div class="select xlarge width-350px">
                        <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" value="Store"></div><div class="frm-custom-icon"></div><div class="frm-custom-dropdown-option"><div class="option" data-value="Store">Store</div></div></div><select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="Goru">Group</option>
                        </select>
                    </div>
                </div>


                <div> <!-- district -->
                    <label>District:</label>
                    <br>
                    <div class="select xlarge width-350px">
                        <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" value="District 1"></div><div class="frm-custom-icon"></div><div class="frm-custom-dropdown-option"><div class="option" data-value="District-1">District 1</div><div class="option" data-value="District-2">District 2</div></div></div><select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="District-9">District 9</option>
                        </select>
                    </div>
                </div>


                <div> <!-- maximum -->
                    <label>Maximum Order Threshold: <span style="color:red; font-size: 16px; ">*</span></label>
                    <br>
                    <input type="text" class="large" name="max_order" datavalid="required numeric" labelinput="Max Order">
                </div>
            </div>

            <div class="f-left margin-left-20">

                <div > <!-- username -->
                    <label>Username: <span style="color:red; font-size: 16px; ">*</span></label>
                    <br>
                    <input type="text" class="large" name="username" datavalid="required" labelinput="Username">
                </div>
                <label>Password:<!--  <span style="color:red; font-size: 16px; ">*</span> --></label>
                <br>
                <input type="password" name="password" labelinput="Password" class="large" autocomplete="off">
                <br>
                <label>Repeat Password:<!--  <span style="color:red; font-size: 16px; ">*</span> --></label>
                <br>
                <input type="password" name="confirm_password" labelinput="Confirm Password" class="large">
                <br>
                <label>Email: <span style="color:red; font-size: 16px; ">*</span></label>
                <br>
                <input type="email" name="email" datavalid="required" labelinput="Email" class="large">
                <br>
                <label>Contact: <span style="color:red; font-size: 16px; ">*</span></label>
                <br>
                <input type="text" name="contact_number" datavalid="required" labelinput="Contact Number" class="large">

                <div> <!-- store ownership -->
                    <label>Store Ownership:</label>
                    <br>
                    <div class="select xlarge width-350px edit_store_ownership">
                        <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" value="" name="ownership"></div><div class="frm-custom-icon"></div><div class="frm-custom-dropdown-option"><div class="option" data-value="Company">Company</div><div class="option" data-value="Franchise">Franchise</div></div></div><select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="Store Ownership">Store Ownership</option>
                        </select>
                    </div>
                </div>

                <label>Store Telco Provider: <span style="color:red; font-size: 16px; ">*</span></label>
                <br>
                <input type="text" name="store_telco_provider" datavalid="required" labelinput="Store Telco Provider" class="large">
                <br/>
                <label>Service IP: <span style="color:red; font-size: 16px; ">*</span></label>
                <br>
                <input type="text" name="service_ip" datavalid="required" labelinput="Servcie IP" class="large">

                <table>
                    <thead>
                        <tr>
                            <th colspan="2">
                                <label class="margin-top-10">Trading Hours:</label>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="2" class="gray-color font-bold">OPEN: <span style="color:red; font-size: 16px; ">*</span></td>
                            <td colspan="2" class="gray-color font-bold">CLOSE: <span style="color:red; font-size: 16px; ">*</span></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="date-picker-store time margin-right-15">
                                    <input type="text" data-date-format="MM/DD/YYYY" class=" width-80px" name="time_start" datavalid="required" labelinput="Operation Time Start">
                                    <span class="fa fa-clock-o text-center red-color"></span>
                                </div>
                            </td>

                            <td>to
                            </td>

                            <td>
                                <div class="date-picker-store time margin-left-15 margin-right-10">
                                    <input type="text" data-date-format="MM/DD/YYYY" class=" width-80px" name="time_end" datavalid="required" labelinput="Operation Time End">
                                    <span class="fa fa-clock-o text-center red-color"></span>
                                </div>
                            </td>

                            <td>
                                <div class="form-group padding-top-10">
                                    <input class="chck-box" name="24_hours" id="edit-trading-time" type="checkbox">
                                    <label class="chck-lbl edit-trading-time" for="edit-trading-time">24 Hours</label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <!-- <table>
                    <thead>
                        <tr>
                            <th colspan="2">
                                <label class="margin-top-10">Operating Hours:</label>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="2" class="gray-color font-bold">OPEN: <span style="color:red; font-size: 16px; ">*</span></td>
                            <td colspan="2" class="gray-color font-bold">CLOSE: <span style="color:red; font-size: 16px; ">*</span></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="date-picker-store time margin-right-15">
                                    <input type="text" data-date-format="MM/DD/YYYY" class=" width-80px" datavalid="required" labelinput="Operation Time Start">
                                    <span class="fa fa-clock-o text-center red-color"></span>
                                </div>
                            </td>

                            <td>to
                            </td>

                            <td>
                                <div class="date-picker-store time margin-left-15 margin-right-10">
                                    <input type="text" data-date-format="MM/DD/YYYY" class=" width-80px" datavalid="required" labelinput="Operation Time End">
                                    <span class="fa fa-clock-o text-center red-color"></span>
                                </div>
                            </td>

                            <td>
                                <div class="form-group padding-top-10">
                                    <input class="chck-box" name="24_hours" type="checkbox" id="edit-operating-time">
                                    <label class="chck-lbl edit-operating-time" for="edit-operating-time">24 Hours</label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table> -->

                <!-- <div class="form-group padding-top-10">
                    <input class="chck-box" name="24_hours" id="ip-whitelist-exclude-store-edit" type="checkbox">
                    <label class="chck-lbl" for="ip-whitelist-exclude-store-edit">Exclude from IP Whitelist</label>

                    <input class="chck-box" name="24_hours" id="is_blacklisted" type="checkbox">
                    <label class="chck-lbl margin-left-30" for="is_blacklisted">Blacklist User</label>
                </div> -->

                <div class="form-group f-right margin-top-20 no-margin-bottom">
                            <input id="ip-whitelist-exclude-store-edit" type="radio" class="radio-box" name="ip-whitelist-exclude-store-edit" checked="true" />
                            <label class="radio-lbl padding-right-50" for="ip-whitelist-exclude-store-edit">Exclude from IP Whitelist</label>
                            <input id="is_blacklisted" type="radio" class="radio-box" name="ip-whitelist-exclude-store-edit"/>
                            <label class="radio-lbl padding-right-50" for="is_blacklisted">Blacklist User</label>
                </div>
                <div class="clear"></div>

                <div class="f-right margin-top-10 margin-bottom-20">
                    <button type="button" class="btn btn-dark margin-right-10 edit_store_submit"> Save  <i class="fa fa-spinner fa-pulse hidden"></i></button>
                    <button type="button" class="btn btn-dark edit_store_cancel">Cancel</button>
                </div>
                <div class="clear"></div>
                </form>
            </div>

            <div class="clear"></div>

        </div>

    </div>
    <!--store management list end-->

    <div content="archive_store_list" class="row hidden archive_store_list">

        <!-- sample 1 -->
        <div class="content-container viewable">
            <div>
                <div class="width-35per f-left">
                    <p class="font-14 margin-bottom-5"><strong>MM Harbour Square</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Store Code:</span> </strong>JB1141</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Store ID:</span></strong>12EDF45</p>
                </div>

                <div class="width-40per f-left">
                    <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Area: </span>NCR</strong></p>
                    <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Operating Time: </span></strong>06:00 AM - 10:00 PM</p>
                    <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Trading Time: </span></strong>06:00 AM - 08:00 PM</p>
                </div>

                <div class="width-25per f-left margin-top-20    ">
                    <p class="font-16 margin-bottom-5 "><strong>ARCHIVED</strong></p>
                    <p><strong><span class="red-color">Since: </span></strong>05-17-16 | 3:23 AM</p>
                </div>
                <div class="clear"></div>
            </div>
        </div>


        <!-- sample 2 -->
        <div class="content-container">
            <div>
                <div class="margin-bottom-30">
                    <h3 class="f-left">Pasig Simbahan</h3>
                    <h3 class="f-right  margin-right-10">ARCHIVED</h3>
                    <div class="clear"></div>
                </div>
                <div class="width-50per f-left">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Store Code:</span> </strong> JB1180</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Store ID:</span> </strong> 13SDE21</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Area:</span></strong> NCR</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Operating Hours:</span></strong> 06:00 AM - 10:00 PM</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Trading Hours:</span></strong> 06:00 AM - 10:00 PM</p>
                </div>

                <div class="width-40per f-left">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Name:</span> </strong> jbncr</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Password: </span> </strong> ***** <!--<a href="#">--><!--<i class="fa fa-eye disabled"></i> Show Password</a>--></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Maximum Order Limit: </span></strong> 10 Orders</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Last Online: </span></strong>05-17-16 | 3:23 AM</p>
                </div>
                <div class="clear"></div>
                <div class="f-right   margin-right-10 margin-bottom-20 ">
                    <button type="button" class="btn btn-dark  margin-right-10">Edit Information</button>
                    <button type="button" class="btn btn-dark">Activate Store</button>

                </div>
                <div class="clear"></div>
            </div>
        </div>

        <!-- sample 3 -->
        <div class="content-container viewable">
            <div>
                <div class="width-35per f-left">
                    <p class="font-14 margin-bottom-5"><strong>MM Tayuman</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Store Code:</span> </strong>JB1141</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Store ID:</span></strong>12EDF45</p>
                </div>

                <div class="width-40per f-left">
                    <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Area: </span>NCR</strong></p>
                    <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Operating Time: </span></strong>06:00 AM - 10:00 PM</p>
                    <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Trading Time: </span></strong>06:00 AM - 08:00 PM</p>
                </div>

                <div class="width-25per f-left margin-top-20    ">
                    <p class="font-16 margin-bottom-5 "><strong>ARCHIVED</strong></p>
                    <p><strong><span class="red-color">Since: </span></strong>05-17-16 | 3:23 AM</p>
                </div>
                <div class="clear"></div>
            </div>
        </div>


        <!-- sample 4 -->
        <div class="content-container viewable">
            <div>
                <div class="width-35per f-left">
                    <p class="font-14 margin-bottom-5"><strong>Cavite Silang</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Store Code:</span> </strong>JB1141</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Store ID:</span></strong>12EDF45</p>
                </div>

                <div class="width-40per f-left">
                    <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Area: </span>NCR</strong></p>
                    <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Operating Time: </span></strong>06:00 AM - 10:00 PM</p>
                    <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Trading Time: </span></strong>06:00 AM - 08:00 PM</p>
                </div>

                <div class="width-25per f-left margin-top-20    ">
                    <p class="font-16 margin-bottom-5 "><strong>ARCHIVED</strong></p>
                    <p><strong><span class="red-color">Since: </span></strong>05-17-16 | 3:23 AM</p>
                </div>
                <div class="clear"></div>
            </div>
        </div>


        <!-- sample 5 -->
        <div class="content-container viewable">
            <div>
                <div class="width-35per f-left">
                    <p class="font-14 margin-bottom-5"><strong>GMA Cavite</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Store Code:</span> </strong>JB1141</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Store ID:</span></strong>12EDF45</p>
                </div>

                <div class="width-40per f-left">
                    <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Area: </span>NCR</strong></p>
                    <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Operating Time: </span></strong>06:00 AM - 10:00 PM</p>
                    <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Trading Time: </span></strong>06:00 AM - 08:00 PM</p>
                </div>

                <div class="width-25per f-left margin-top-20    ">
                    <p class="font-16 margin-bottom-5 "><strong>ARCHIVED</strong></p>
                    <p><strong><span class="red-color">Since: </span></strong>05-17-16 | 3:23 AM</p>
                </div>
                <div class="clear"></div>
            </div>
        </div>


    </div>

    <!--messaging content start-->
    <div content="messaging" class="messaging hidden centerer">

        <!-- left content -->
        <div class="content-container unboxed f-left width-50per padding-top-20 ">
            <p class="f-left font-18">Global Announcement</p>
            <p class="f-right gray-color margin-top-5 font-12">
                <a href="javascript:void(0)" class="create_new_global">
                    Create New Global Announcement
                </a>
            </p>
            <div class="clear"></div>
        </div>

        <!-- right content -->
        <div class="content-container unboxed f-right width-50per padding-top-20 ">

            <p class="f-left font-18">SMS Blast</p>
            <p class="f-right gray-color margin-top-5 font-12">Create New Global Announcement</p>
            <div class="clear"></div>

        </div>
        <div class="clear"></div>


        <div class="f-left  width-50per"> <!-- left -->

            <!-- left content -->
            <form id="add_announcement">
                <div class="bggray-middark content-container f-left padding-left-15 padding-top-15 padding-bottom-10 width-100per new_announcement_container" style="display:none;">

                    <p class=" font-16">New Announcement</p>
                    <div class="form-group no-padding-all messaging_radio_select">
                        <input id="store1" type="radio" class="radio-box" name="store" value="1">
                        <label class="radio-lbl margin-right-10 " for="store1">All Store</label>
                        <input id="store2" type="radio" class="radio-box" name="store" value="2">
                        <label class="radio-lbl margin-right-10" for="store2">Store Area</label>
                        <input id="store3" type="radio" class="radio-box" name="store" value="3">
                        <label class="radio-lbl margin-right-10" for="store3">Stores with specific Trading Time</label>
                        <input id="store4" type="radio" class="radio-box" name="store" value="4">
                        <label class="radio-lbl margin-right-10" for="store4">All Callcenter</label>
                        <input id="store5" type="radio" class="radio-box" name="store" value="5">
                        <label class="radio-lbl margin-right-10" for="store5">Callcenter Area</label>
                    </div>
                    
                    <!-- error message start -->
                    <div class="add_announcement_error_msg error-msg margin-bottom-30">
                        <i class="fa fa-exclamation-triangle"></i>
                        Please fill up all required fields.
                    </div>
                    <!-- error message end-->
                    <!-- success message start -->
                    <div class="add_announcement_success_msg success-msg margin-bottom-30">
                        <i class="fa fa-exclamation-triangle"></i>
                        Announcement added successfully.
                    </div>
                    <!-- success message end -->

                    <div class="select width-400px no-padding-all messaging-province-select">
                        <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" name="messaging_province" readonly></div><div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option"><!-- <div class="option" data-value="bat">Bataan</div> --></div></div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="bat">Bataan</option>
                        </select>
                    </div>
                    <div class="select width-400px no-padding-all messaging-trading-time-select">
                        <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" name="messaging_trading_time" readonly></div><div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option"><!-- <div class="option" data-value="bat">Bataan</div> --></div></div>
                    </div>
                    <div class="select width-400px no-padding-all messaging-callcenter-select">
                        <div class="frm-custom-dropdown"><div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" name="messaging_callcenter_area" readonly></div><div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option"><!-- <div class="option" data-value="bat">Bataan</div> --></div></div>
                    </div>

                    <label>Subject</label>
                    <input type="text" class="large" name="announcement_subject" datavalid="required" labelinput="Subject">
                    <br>
                    <label>Announcement</label>
                    <textarea class="font-12 padding-all-5 width-400px no-margin-bottom" name="announcement_message" datavalid="required" labelinput="Message"></textarea>

                    <div class="f-right">
                        <button type="button" class="btn btn-dark margin-right-10 send_new_announcement_btn">Send Global Announcement</button>
                        <button type="button" class="btn btn-dark cancel_new_announcement_btn">Cancel</button>
                    </div>
                    <div class="clear"></div>
                </div>
            </form>

            <div class="  unboxed content-container f-left  ">
                <p class=" font-16">Announcement this Month</p>
            </div>
            <div class="clear"></div>

            <div class="announcement_container">
                <div class=" content-container f-left  padding-all-10 padding-left-20 announcement template width-100per">
                    <p class="font-bold padding-top-10 subject">Price Change for all Chicken Products</p>
                    <p class="padding-top-10 padding-bottom-10 message">This is notify all Call Center and Stores that all Chicekn Products will
                        marked down by 2 pesos from the current price</p>
                    <table class="margin-bottom-10">
                        <tbody>
                            <tr>
                                <td class="red-color width-150px font-bold">Date Created:</td>
                                <td class="date_added">May 01, 2015</td>
                            </tr>
                            <tr>
                                <td class="red-color padding-top-5 font-bold">Recipient:</td>
                                <td class="padding-top-5 recipient">All Stores</td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="f-right">
                        <button type="button" class="btn btn-dark margin-right-10 archive_announcement_btn modal-trigger" modal-target="confirm-archive-announcement">Archive</button>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <!--
            <div class="   content-container f-left  padding-all-10 padding-left-20">
                <p class="font-bold padding-top-10">Price Change for all Chicken Products</p>
                <p class="padding-top-10 padding-bottom-10">This is notify all Call Center and Stores that all Chicekn Products will
                    marked down by 2 pesos from the current price</p>
                <table class="margin-bottom-10">
                    <tbody>
                        <tr>
                            <td class="red-color width-150px font-bold">Date Created:</td>
                            <td>May 01, 2015</td>
                        </tr>
                        <tr>
                            <td class="red-color padding-top-5 font-bold">Recipient:</td>
                            <td class="padding-top-5">NCR Stores</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="clear"></div>


            <div class="content-container f-left  padding-all-10 padding-left-20">
                <p class="font-bold padding-top-10">Price Change for all Chicken Products</p>
                <p class="padding-top-10 padding-bottom-10">This is notify all Call Center and Stores that all Chicekn Products will
                    marked down by 2 pesos from the current price</p>
                <table class="margin-bottom-10">
                    <tbody>
                        <tr>
                            <td class="red-color width-150px font-bold">Date Created:</td>
                            <td>May 01, 2015</td>
                        </tr>
                        <tr>
                            <td class="red-color padding-top-5 font-bold">Recipient:</td>
                            <td class="padding-top-5">Stores with Trading time of 08:00 AM to 10:00 PM</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="clear"></div>


            <div class=" content-container f-left padding-all-10 padding-left-20">
                <p class="font-bold padding-top-10">Price Change for all Chicken Products</p>
                <p class="padding-top-10 padding-bottom-10">This is notify all Call Center and Stores that all Chicekn Products will
                    marked down by 2 pesos from the current price</p>
                <table class="margin-bottom-10">
                    <tbody>
                        <tr>
                            <td class="red-color width-150px font-bold">Date Created:</td>
                            <td>May 01, 2015</td>
                        </tr>
                        <tr>
                            <td class="red-color padding-top-5 font-bold">Recipient:</td>
                            <td class="padding-top-5">All Stories</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="clear"></div>


            <div class="   content-container f-left  padding-all-10 padding-left-20">
                <p class="font-bold padding-top-10">Price Change for all Chicken Products</p>
                <p class="padding-top-10 padding-bottom-10">This is notify all Call Center and Stores that all Chicekn Products will
                    marked down by 2 pesos from the current price</p>
                <table class="margin-bottom-10">
                    <tbody>
                        <tr>
                            <td class="red-color width-150px font-bold">Date Created:</td>
                            <td>May 01, 2015</td>
                        </tr>
                        <tr>
                            <td class="red-color padding-top-5 font-bold">Recipient:</td>
                            <td class="padding-top-5">NCR Stores</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="clear"></div>
             -->



            <div class=" content-container unboxed f-left width-100per text-center ">
                <button type="button" class="btn btn-dark load_more">Load More Announcement</button>
            </div>
            <div class="clear"></div>
        </div>

        <div class="f-left  margin-left-20 width-50per"><!-- right -->


            <div class=" yellow-bg content-container f-left padding-left-15  padding-top-15 padding-bottom-10 padding-right-15 width-100per ">

                <label>New sms blast message</label>
                <textarea></textarea>
                <p>Character Remaining: <span>2</span></p>


                <div class="f-right no-padding-all">
                    <button type="button" class="btn btn-dark margin-right-10 margin-bottom-10">Save</button>
                    <button type="button" class="btn btn-dark margin-bottom-10">Cancel</button>
                </div>
                <div class="clear"></div>


            </div>
            <div class="clear"></div>

            <div class="  yellow-bg content-container f-left padding-left-15  padding-top-15 padding-bottom-10 padding-right-15 width-100per ">
                <p class="margin-bottom-10">IrReesesistible Deal!! Buy the Reese's Peanut Butter Jollibee promo pack
                    and SAVE P20 when you buy a Jolibee's Reese Mix-ins.
                </p>
                <p class="margin-bottom-10">Visit: http://www.jolibee.com/promos/for-more-information.</p>

                <hr class="margin-top-10 border-all-white">
                <p class="f-left red-color margin-top-15 margin-right-20 font-bold">Date Created:</p>
                <p class="f-left margin-top-15 margin-bottom-20 "> May 01, 2015</p>
                <div class="clear"></div>

            </div>
            <div class="clear"></div>

            <div class="  yellow-bg content-container f-left padding-left-15  padding-top-15 padding-bottom-10 padding-right-15 width-100per ">
                <p class="margin-bottom-10">IrReesesistible Deal!! Buy the Reese's Peanut Butter Jollibee promo pack
                    and SAVE P20 when you buy a Jolibee's Reese Mix-ins.
                </p>
                <p class="margin-bottom-10">Visit: http://www.jolibee.com/promos/for-more-information.</p>

                <hr class="margin-top-10 border-all-white">
                <p class="f-left red-color margin-top-15 margin-right-20 font-bold">Date Created:</p>
                <p class="f-left margin-top-15 margin-bottom-20 "> May 01, 2015</p>
                <div class="clear"></div>

            </div>
            <div class="clear"></div>


            <div class="  yellow-bg content-container f-left padding-left-15  padding-top-15 padding-bottom-10 padding-right-15 width-100per ">
                <p class="margin-bottom-10">IrReesesistible Deal!! Buy the Reese's Peanut Butter Jollibee promo pack
                    and SAVE P20 when you buy a Jolibee's Reese Mix-ins.
                </p>
                <p class="margin-bottom-10">Visit: http://www.jolibee.com/promos/for-more-information.</p>

                <hr class="margin-top-10 border-all-white">
                <p class="f-left red-color margin-top-15 margin-right-20 font-bold">Date Created:</p>
                <p class="f-left margin-top-15 margin-bottom-20 "> May 01, 2015</p>
                <div class="clear"></div>

            </div>
            <div class="clear"></div>


            <div class="  yellow-bg content-container f-left padding-left-15  padding-top-15 padding-bottom-10 padding-right-15 width-100per ">
                <p class="margin-bottom-10">IrReesesistible Deal!! Buy the Reese's Peanut Butter Jollibee promo pack
                    and SAVE P20 when you buy a Jolibee's Reese Mix-ins.
                </p>
                <p class="margin-bottom-10">Visit: http://www.jolibee.com/promos/for-more-information.</p>

                <hr class="margin-top-10 border-all-white">
                <p class="f-left red-color margin-top-15 margin-right-20 font-bold">Date Created:</p>
                <p class="f-left margin-top-15 margin-bottom-20 "> May 01, 2015</p>
                <div class="clear"></div>

            </div>
            <div class="clear"></div>


            <div class="  unboxed content-container f-left text-center   width-100per ">
                <button type="button" class="btn btn-dark ">Load SMS Blast</button>

            </div>
            <div class="clear"></div>




        </div>
        <div class="clear"></div>

    </div>
    <!--messaging content end-->

    <!--store operations start-->
    <div content="store_operations" class="store_operations hidden row">

        <div class="centerer">


            <!-- left content -->
            <div class="content-container unboxed f-left width-50per padding-top-30 business_rule_settings">
                <p class="f-left font-18 font-bold margin-bottom-10">Business Rule Settings</p>
                <!--interchanging-->
                <a href="javascript:void(0)" class="f-right  margin-top-5 font-12 edit-values">Edit Business Rule Values</a>
                <!--interchanging-->
                <div class="f-right">
                    <button type="button" class="btn btn-dark save-values hidden">Save <i class="fa fa-spinner fa-pulse hidden"></i></button>
                    <button type="button" class="btn btn-dark cancel-edit hidden">Cancel</button>
                </div>
                <div class="clear"></div>
                <div class="tbl no-padding-all ">
                    <table class="margin-top-10 business-rules-table-display">
                        <thead>
                            <tr>
                                <th class="border-radius-left">Business Rules</th>
                                <th class="border-radius-right text-center">Value</th>
                            </tr>
                        </thead>
                        <tbody class="sbu_settings_container">
                            <tr class="rule_tr template">
                                <td data-label="rule-text" class="border-radius-right business_rules"></td>
                                <td data-label="value" class="border-radius-right text-center"></td>
                            </tr>
                        </tbody>
                    </table>

                    <!--Hidden Editable Table-->
                    <table class="margin-top-10 business-rules-table-editable hidden">
                        <thead>
                            <tr>
                                <th class="border-radius-left">Business Rules</th>
                                <th class="border-radius-right text-center">Value</th>
                            </tr>
                        </thead>
                        <tbody class="sbu_settings_editable_container">
                            <tr class="rule_editable_tr template">
                                <td data-label="rule-text"></td>
                                <td class="text-center"><input data-input="value" data-input-name="rule" type="text" class="xsmall f-left input_numeric required" value=""></td>
                            </tr>
                        </tbody>
                    </table>

                </div>

            </div>

            <!-- right content -->
            <div class="content-container unboxed f-right width-50per padding-top-30 bulk-order-discount">
                <p class="f-left font-18 font-bold margin-bottom-10">Bulk Order Discount</p>
                <!--interchanging-->
                <a href="javascript:void(0)" class="f-right  margin-top-5 font-12 edit-values">Edit Bulk Order Discount List</a>
                <!--interchanging-->
                <div class="f-right">
                    <button type="button" class="btn btn-dark save-values hidden">Save  <i class="fa fa-spinner fa-pulse hidden"></i></button>
                    <button type="button" class="btn btn-dark cancel-edit hidden">Cancel</button>
                </div>
                <div class="clear"></div>
                <div class="tbl no-padding-all ">
                    <table class="text-center bulk-order-table-display">
                        <thead>
                            <tr>
                                <th class="border-radius-left text-center">Minimum Bulk Order Cost</th>
                                <th class="border-radius-right text-center">Discount Value</th>
                            </tr>
                        </thead>
                        <tbody class="sbu_settings_container">
                            <tr class="rule_tr template">
                                <td data-label="rule-text" class="border-radius-right"></td>
                                <td data-label="discount_value" class="border-radius-right text-center"></td>
                            </tr>
                        </tbody>
                        <tbody class="sbu_settings_container_add_row hidden">
                            <tr class="">
                                <td class="text-center"><input name="minimum_bulk_order_cost" type="text" class="small input_numeric required" value=""></td>
                                <td class="text-center"><input name="discount_value" type="text" class="xsmall input_numeric required" value=""></td>
                            </tr>
                        </tbody>
                    </table>

                    <!--Hidden Editable Table-->

                    <table class="text-center bulk-order-table-editable hidden">
                        <thead>
                            <tr>
                                <th class="border-radius-left text-center">Minimum Bulk Order Cost</th>
                                <th class="border-radius-right text-center">Discount Value</th>
                            </tr>
                        </thead>
                        <tbody class="sbu_settings_editable_container">
                            <tr class="rule_editable_tr template">
                                <td data-label="rule-text"></td>
                                <td class="text-center"><input data-input="discount_value" data-input-name="rule" type="text" class="xsmall f-left input_numeric required" value=""></td>
                            </tr>
                        </tbody>
                    </table>
                    <a href="javascript:void(0)" class="f-right margin-top-10 font-12 add-bulk-order-discount">+ Add Bulk Order Discount List</a>

                    <div class="f-right margin-top-5">
                        <button type="button" class="btn btn-dark add-value hidden">Save  <i class="fa fa-spinner fa-pulse hidden"></i></button>
                        <button type="button" class="btn btn-dark cancel-add hidden">Cancel</button>
                    </div>

                    <div class="clear"></div>
                </div>

            </div>
            <div class="clear"></div>

            <!-- ***************************** next layer ************************ -->
            <!-- left content -->
            <div class="content-container unboxed f-left width-50per operation-settings">
                <div class="tbl no-padding-all ">
                    <p class="f-left font-18 font-bold margin-bottom-10">Operation Settings</p>
                    <!--interchanging-->
                    <a href="javascript:void(0)" class="f-right  margin-top-5 font-12 edit-values margin-right-5">Edit Operation Settings</a>
                    <!--interchanging-->
                    <div class="f-right">
                        <button type="button" class="btn btn-dark save-values hidden">Save <i class="fa fa-spinner fa-pulse hidden"></i></button>
                        <button type="button" class="btn btn-dark cancel-edit hidden">Cancel</button>
                    </div>
                    <div class="clear"></div>
                    <table class="operation-settings-table-display">
                        <thead>
                            <tr>
                                <th class="border-radius-left width-80per text-center">Operation Item</th>
                                <th class="text-center">Duration Time</th>
                                <th class="text-center">Interval (Secs)</th>
                                <th class="border-radius-right text-center">Alarm</th>
                            </tr>
                        </thead>
                        <tbody class="sbu_settings_container">
                            <tr class="rule_tr template">
                                <td data-label="rule-text" class="border-radius-right text-center"></td>
                                <td data-label="timer" class="border-radius-right text-center"></td>
                                <td data-label="time_interval" class="border-radius-right text-center"></td>
                                <td data-label="is_alarm_on" class="border-radius-right text-center"></td>
                            </tr>
                        </tbody>

                    </table>

                    <!-- Hidden Field for Editable -->

                     <table class="operation-settings-table-editable hidden">
                        <thead>
                            <tr>
                                <th class="border-radius-left width-80per text-center">Operation Item</th>
                                <th class="text-center">Duration Time</th>
                                <th class="text-center">Interval (Secs)</th>
                                <th class="border-radius-right text-center">Alarm</th>
                            </tr>
                        </thead>
                        <tbody class="sbu_settings_editable_container">
                            <tr class="rule_editable_tr template">
                                <td data-label="rule-text" class="text-center"></td>
                                <td class="text-center"><input data-input="timer" data-input-name="rule" type="text" class="small required input_time" maxlength="8" value=""></td>
                                <td class="text-center"><input data-input="time_interval" data-input-name="rule" type="text" class="xsmall required input_numeric" maxlength="3" value=""></td>
                                <td class="text-center"><input type="checkbox" checked="false"></td>
                            </tr>
                        </tbody>
                    </table>

                    <!-- <label class="margin-top-10">Alarm Tone:</label>
                    <div class="padding-top-10 padding-bottom-10 padding-left-20 padding-right-20 bggray-light">
                        <p class="f-left"><span class="red-color">File:</span> Default_alarm.mp3</p>
                        <i class="fa fa-play-circle font-20 f-right"></i>
                        <div class="clear"></div>
                    </div> -->
                </div>

            </div>


            <!-- ***************************** next layer ************************ -->
            <!-- left content -->
            <div class="content-container unboxed f-right width-50per rbu-list">
                <div class="tbl no-padding-all ">
                    <p class="f-left font-18 font-bold margin-bottom-10">Regional Business Units</p>
                    <!--interchanging-->
                    <a href="javascript:void(0)" class="f-right  margin-top-5 font-12 edit-values">Edit Values</a>
                    <!--interchanging-->
                    <div class="f-right">
                        <button type="button" class="btn btn-dark save-values hidden">Save <i class="fa fa-spinner fa-pulse hidden"></i></button>
                        <button type="button" class="btn btn-dark cancel-edit hidden">Cancel</button>
                    </div>
                    <div class="clear"></div>
                    <table class="rbu-list-table-display">
                        <thead>
                            <tr>
                                <th class="border-radius-left width-20per text-center">ID</th>
                                <th class="text-center width-80per">Name</th>
                                <!-- <th class="width-10per"></th> -->
                            </tr>
                        </thead>
                        <tbody class="sbu_settings_container">
                            <tr class="rule_tr template">
                                <td data-label="id" class="border-radius-right text-center"></td>
                                <td data-label="name" class="border-radius-right text-center"></td>
                                <!-- <td data-label="" class="border-radius-right text-center"><a class="red-color delete"><i class="fa fa-times-circle"></i></a></td> -->
                            </tr>
                        </tbody>
                        <tbody class="sbu_settings_container_add_row hidden">
                            <tr class="">
                                <td class="text-center"></td>
                                <td class="text-center"><input data-input="name" name="name" type="text" class="small required" maxlength="50"></td>
                                <!-- <td data-label="" class="border-radius-right text-center"><a class="red-color delete"><i class="fa fa-times-circle"></i></a></td> -->
                            </tr>
                        </tbody>

                    </table>

                    <!-- Hidden Field for Editable -->

                     <table class="rbu-list-table-editable hidden">
                        <thead>
                            <tr>
                                <th class="border-radius-left width-20per text-center">ID</th>
                                <th class="text-center width-80per">Name</th>
                                <!-- <th class="text-center width-10per"></th> -->
                            </tr>
                        </thead>
                        <tbody class="sbu_settings_editable_container">
                            <tr class="rule_editable_tr template">
                                <td data-label="id" class="text-center"></td>
                                <td class="text-center"><input data-input="name" data-input-name="name" type="text" class="small required" maxlength="50" value=""></td>
                                <!-- <td class="text-center"></td> -->
                            </tr>
                        </tbody>
                    </table>

                    <a href="javascript:void(0)" class="f-right margin-top-10 font-12 add-rbu-to-list">+ Add New RBU</a>

                    <div class="f-right margin-top-5">
                        <button type="button" class="btn btn-dark add-value hidden">Save  <i class="fa fa-spinner fa-pulse hidden"></i></button>
                        <button type="button" class="btn btn-dark cancel-add hidden">Cancel</button>
                    </div>
                    <div class="clear"></div>
                    

                </div>

            </div>

            <!-- right content -->
            <div class="content-container unboxed f-right width-50per rbu-districts-list">
                <p class="f-left font-18 font-bold margin-bottom-10">RBU Districts</p>
                <!--interchanging-->
                <a href="javascript:void(0)" class="f-right  margin-top-5 font-12 edit-values">Edit Values</a>
                <!--================================-->
                <!--interchanging-->
                <div class="f-right">
                    <button type="button" class="btn btn-dark save-values hidden">Save <i class="fa fa-spinner fa-pulse hidden"></i></button>
                    <button type="button" class="btn btn-dark cancel-edit hidden">Cancel</button>
                </div>
                <div class="clear"></div>
                <div class="tbl no-padding-all ">
                    <table class="rbu-districts-list-table-display">
                        <thead>
                            <tr>
                                <th class="border-radius-left width-10per text-center">ID</th>
                                <th class="text-center width-40per">RBU</th>
                                <th class="text-center width-40per">District</th>
                                <!-- <th class="width-10per"></th> -->
                            </tr>
                        </thead>
                        <tbody class="sbu_settings_container">
                            <tr class="rule_tr template">
                                <td data-label="id" class="border-radius-right text-center"></td>
                                <td data-label="rbu_name" class="border-radius-right text-center"></td>
                                <td data-label="name" class="border-radius-right text-center"></td>
                                <!-- <td data-label="" class="border-radius-right text-center"><a class="red-color delete"><i class="fa fa-times-circle"></i></a></td> -->
                            </tr>
                        </tbody>
                        <tbody class="sbu_settings_container_add_row hidden">
                            <tr class="">
                                <td data-label="id"></td>
                                <td class="">
                                    <div class="f-left select small margin-left-10 rbu-list-select" style="width:calc(100% - 34px)!important">
                                        <div class="frm-custom-dropdown">
                                            <div class="frm-custom-dropdown-txt">
                                                <input data-input-rbu="rbu_id" data-input-name="rule" type="text" class="dd-txt required" name="rbu_id" value="" readonly data-value="1">
                                            </div>
                                            <div class="frm-custom-icon" style="margin-left:-4px"></div>
                                            <div class="frm-custom-dropdown-option">
                                                <!-- <div class="option province_select" int-value="0">RBU 41</div> -->
                                            </div>
                                        </div>
                                        <select class="frm-custom-dropdown-origin" style="display: none;">
                                        </select>
                                    </div>
                                </td>
                                <td class="text-center"><input data-input="name" data-input-name="rule" type="text" class="small required" maxlength="50" value=""></td>
                                <!-- <td class="text-center"><a class="red-color delete"><i class="fa fa-times-circle"></i></a></td> -->
                            </tr>
                        </tbody>

                    </table>

                    <!-- Hidden Field for Editable -->

                     <table class="rbu-districts-list-table-editable hidden">
                        <thead>
                            <tr>
                                <th class="border-radius-left width-10per text-center">ID</th>
                                <th class="">RBU</th>
                                <th class="">District</th>
                                <!-- <th class="text-center width-10per">Remove</th> -->
                            </tr>
                        </thead>
                        <tbody class="sbu_settings_editable_container">
                            <tr class="rule_editable_tr template">
                                <td data-label="id" class="text-center"></td>
                                <td class="">
                                <!-- <input data-input="rbu_id" data-input-name="rule" type="text" class="small required" maxlength="50" value=""> -->
                                    <div class="f-left select small margin-left-10 rbu-list-select" style="width:calc(100% - 34px)!important">
                                        <div class="frm-custom-dropdown">
                                            <div class="frm-custom-dropdown-txt">
                                                <input data-input-rbu="rbu_id" data-input-name="rule" type="text" class="dd-txt" name="rbu_id" value="Select RBU" readonly data-value="1">
                                            </div>
                                            <div class="frm-custom-icon" style="margin-left:-4px"></div>
                                            <div class="frm-custom-dropdown-option">
                                                <!-- <div class="option province_select" int-value="0">RBU 41</div> -->
                                            </div>
                                        </div>
                                        <select class="frm-custom-dropdown-origin" style="display: none;">
                                        </select>
                                    </div>
                                </td>
                                <td class="text-center"><input data-input="name" data-input-name="rule" type="text" class="small required" maxlength="50" value=""></td>
                                <!-- <td class="text-center"><a class="red-color delete"><i class="fa fa-times-circle"></i></a></td> -->
                            </tr>
                        </tbody>
                    </table>

                    <a href="javascript:void(0)" class="f-right margin-top-10 font-12 add-rbu-districts-to-list">+ Add New RBU District</a>

                    <div class="f-right margin-top-5">
                        <button type="button" class="btn btn-dark add-value hidden">Save  <i class="fa fa-spinner fa-pulse hidden"></i></button>
                        <button type="button" class="btn btn-dark cancel-add hidden">Cancel</button>
                    </div>
                    <div class="clear"></div>

                </div>

            </div>
            <div class="clear"></div>

            </div>            

        </div>
    </div>
    <!--store operations end-->


    <!--security_settings content start-->
    <div content="security_settings" class="security_settings hidden row">
        <div class="clear"></div>

        <div class="centerer">
            <!-- left content -->
            <div class="content-container unboxed f-left width-50per ip-whitelist padding-top-30">
                <p class="f-left font-18 font-bold margin-bottom-10">IP Whitelist</p>
                <!--interchanging-->
                <a href="javascript:void(0)" class="f-right  margin-top-5 font-12 edit-values">Edit IP Whitelist</a>
                <!--================================-->
                <!--interchanging-->
                <div class="f-right">
                    <button type="button" class="btn btn-dark save-values hidden">Save <i class="fa fa-spinner fa-pulse hidden"></i></button>
                    <button type="button" class="btn btn-dark cancel-edit hidden">Cancel</button>
                </div>
                <div class="clear"></div>
                <div class="tbl no-padding-all ">
                    <table class="text-center ip-whitelist-table-display">
                        <thead>
                            <tr>
                                <th class="border-radius-left width-80per text-center">Description</th>
                                <th class="border-radius-right text-center">Allowed IP Addresses</th>
                            </tr>
                        </thead>
                        <tbody class="sbu_settings_container">
                            <tr class="rule_tr template">
                                <td data-label="rule-text" class="border-radius-right"></td>
                                <td data-label="ip_address" class="border-radius-right text-center"></td>
                            </tr>
                        </tbody>
                        <tbody class="sbu_settings_container_add_row hidden">
                            <tr class="">
                                <td class="text-center"><input data-input="description" name="description" type="text" class="small required" value=""></td>
                                <td class="text-center"><input data-input="ip_address" name="ip_address" type="text" class="small input_numeric required" value=""></td>
                            </tr>
                        </tbody>
                    </table>

                    <!-- Hidden Field for Editable -->
                     <table class="text-center ip-whitelist-table-editable hidden">
                        <thead>
                            <tr>
                                <th class="border-radius-left width-80per text-center">Description</th>
                                <th class="border-radius-right text-center">Blacklisted IP Addresses</th>
                            </tr>
                        </thead>
                        <tbody class="sbu_settings_editable_container">
                           <tr class="rule_editable_tr template">
                                <td data-label="rule-text"></td>
                                <td class="text-center"><input data-input="ip_address" data-input-name="rule-text" type="text" class="small input_numeric required" value=""></td>
                            </tr>
                        </tbody>
                    </table>

                    <a href="javascript:void(0)" class="f-right margin-top-10 font-12 add-ip-address-to-list">+ Add IP Address to List</a>

                    <div class="f-right margin-top-5">
                        <button type="button" class="btn btn-dark add-value hidden">Save  <i class="fa fa-spinner fa-pulse hidden"></i></button>
                        <button type="button" class="btn btn-dark cancel-add hidden">Cancel</button>
                    </div>
                    <div class="clear"></div>
                </div>

            </div>


            <div class="content-container unboxed f-right width-50per padding-top-30">
                <p class="f-left font-18 font-bold margin-bottom-10">IP Whitelist Excluded Users</p>
                <!--interchanging-->
                <!--<a href="javascript:void(0)" class="f-right  margin-top-5 font-12 edit-values">Edit Values</a>-->
                <!--================================-->
                <!--interchanging-->
                <!--<div class="f-right">
                    <button type="button" class="btn btn-dark save-values hidden">Save <i class="fa fa-spinner fa-pulse hidden"></i></button>
                    <button type="button" class="btn btn-dark cancel-edit hidden">Cancel</button>
                </div>-->
                <div class="clear"></div>
                <div class="tbl no-padding-all ">
                    <table class="ip_whitelist_excluded_table">
                        <thead>
                            <tr>
                                <th class="border-radius-left width-10per text-center">User ID</th>
                                <th class="text-center width-20per">Channel</th>
                                <th class="text-center width-40per">Username</th>
                                <!-- <th class="text-center width-40per">District</th>
                                 <th class="width-10per"></th>-->
                            </tr>
                        </thead>
                        <tbody class="ip_whitelist_excluded_container">
                            <tr class="ip-whitelist-user template">
                                <td data-label="user_id" class="border-radius-right text-center"></td>
                                <td data-label="channel" class="border-radius-right text-center">Callcenter</td>
                                <td  class="border-radius-right text-center"><info data-label="username"></info><a class="red-color f-right modal-trigger" modal-target="confirm-remove-user"><i class="fa fa-times-circle"></i></a> <a class="red-color delete f-right hidden"><i class="fa fa-times-circle"></i></a></td>
                            </tr>
                            <tr class="ip-whitelist-store-user template">
                                <td data-label="user_id" class="border-radius-right text-center"></td>
                                <td data-label="channel" class="border-radius-right text-center">Store</td>
                                <td  class="border-radius-right text-center"><info data-label="username"></info><a class="red-color f-right modal-trigger-store-user" modal-target="confirm-remove-user"><i class="fa fa-times-circle"></i></a> <a class="red-color delete_store_user f-right hidden"><i class="fa fa-times-circle"></i></a></td>
                            </tr>
                        </tbody>

                    </table>

                    <div class="clear"></div>

                </div>

            </div>

            <div class="clear"></div>
            <div class="content-container unboxed f-left width-50per padding-top-30">
                <p class="f-left font-18 font-bold margin-bottom-10">Blacklisted Users</p>
                <!--interchanging-->
                <!--<a href="javascript:void(0)" class="f-right  margin-top-5 font-12 edit-values">Edit Values</a>-->
                <!--================================-->
                <!--interchanging-->
                <!--<div class="f-right">
                    <button type="button" class="btn btn-dark save-values hidden">Save <i class="fa fa-spinner fa-pulse hidden"></i></button>
                    <button type="button" class="btn btn-dark cancel-edit hidden">Cancel</button>
                </div>-->
                <div class="clear"></div>
                <div class="tbl no-padding-all ">
                    <table class="blacklisted_users_table">
                        <thead>
                            <tr>
                                <th class="border-radius-left width-10per text-center">User ID</th>
                                <th class="text-center width-20per">Channel</th>
                                <th class="text-center width-40per">Username</th>
                                <!-- <th class="text-center width-40per">District</th>
                                 <th class="width-10per"></th>-->
                            </tr>
                        </thead>
                        <tbody class="blacklisted_user_container">
                            <tr class="blacklisted-user template">
                                <td data-label="user_id" class="border-radius-right text-center"></td>
                                <td data-label="channel" class="border-radius-right text-center">Callcenter</td>
                                <td  class="border-radius-right text-center"><info data-label="username"></info><a class="red-color f-right modal-trigger" modal-target="confirm-remove-blacklisted-user"><i class="fa fa-times-circle"></i></a> <a class="red-color delete f-right hidden"><i class="fa fa-times-circle"></i></a></td>
                            </tr>
                            <tr class="blacklisted-store-user template">
                                <td data-label="user_id" class="border-radius-right text-center"></td>
                                <td data-label="channel" class="border-radius-right text-center">Store</td>
                                <td  class="border-radius-right text-center"><info data-label="username"></info><a class="red-color f-right modal-trigger-store-user" modal-target="confirm-remove-blacklisted-user"><i class="fa fa-times-circle"></i></a> <a class="red-color delete_store_user f-right hidden"><i class="fa fa-times-circle"></i></a></td>
                            </tr>
                        </tbody>

                    </table>

                    <div class="clear"></div>

                </div>

            </div>
            
            <!-- left content for blacklisted ip's-->
            <div class="content-container unboxed f-right width-50per ip-blacklist hidden padding-top-30">
                <p class="f-left font-18 font-bold margin-bottom-10">IP Blacklist</p>
                <!--interchanging-->
                <a href="javascript:void(0)" class="f-right  margin-top-5 font-12 edit-values">Edit IP Blacklist</a>
                <!--================================-->
                <!--interchanging-->
                <div class="f-right">
                    <button type="button" class="btn btn-dark save-values hidden">Save <i class="fa fa-spinner fa-pulse hidden"></i></button>
                    <button type="button" class="btn btn-dark cancel-edit hidden">Cancel</button>
                </div>
                <div class="clear"></div>
                <div class="tbl no-padding-all ">
                    <table class="text-center ip-blacklist-table-display">
                        <thead>
                            <tr>
                                <th class="border-radius-left width-80per text-center">Description</th>
                                <th class="border-radius-right text-center">Allowed IP Addresses</th>
                            </tr>
                        </thead>
                        <tbody class="sbu_settings_container">
                            <tr class="rule_tr template">
                                <td data-label="rule-text" class="border-radius-right"></td>
                                <td data-label="ip_address" class="border-radius-right text-center"></td>
                            </tr>
                        </tbody>
                        <tbody class="sbu_settings_container_add_row hidden">
                            <tr class="">
                                <td class="text-center"><input data-input="description" name="description" type="text" class="small required" value=""></td>
                                <td class="text-center"><input data-input="ip_address" name="ip_address" type="text" class="small input_numeric required" value=""></td>
                            </tr>
                        </tbody>
                    </table>

                    <!-- Hidden Field for Editable -->
                     <table class="text-center ip-blacklist-table-editable hidden">
                        <thead>
                            <tr>
                                <th class="border-radius-left width-80per text-center">Description</th>
                                <th class="border-radius-right text-center">Allowed IP Addresses</th>
                            </tr>
                        </thead>
                        <tbody class="sbu_settings_editable_container">
                           <tr class="rule_editable_tr template">
                                <td data-label="rule-text"></td>
                                <td class="text-center"><input data-input="ip_address" data-input-name="rule-text" type="text" class="small input_numeric required" value=""></td>
                            </tr>
                        </tbody>
                    </table>

                    <a href="javascript:void(0)" class="f-right margin-top-10 font-12 add-ip-address-to-list">+ Add IP Address to List</a>

                    <div class="f-right margin-top-5">
                        <button type="button" class="btn btn-dark add-value hidden">Save  <i class="fa fa-spinner fa-pulse hidden"></i></button>
                        <button type="button" class="btn btn-dark cancel-add hidden">Cancel</button>
                    </div>
                    <div class="clear"></div>
                </div>

            </div>

        </div>            

        </div>
    </div>
    <!--security_settings content end-->

    <!--promos start-->
    <div content="promos" class="promos hidden row">
        <div class="centerer margin-top-10">

            <!-- left content -->
            <div class="content-container unboxed f-left width-50per padding-top-20 ">
                <p class="f-left font-18">Upsells</p>
                <a href="javascript:void(0)" class="f-right upload_new_upsell">Upload New Upsell</a>
                <div class="clear"></div>
            </div> 

            <!-- right content -->
            <div class="content-container unboxed f-right width-50per padding-top-20 ">
                <p class="f-left font-18">Promos</p>
                <a href="javascript:void(0)" class="f-right create_new_promo">Create New Promo</a>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>

            <div class="f-left  width-50per">

                <div class="content-container  no-margin-top upload_new_upsell_container hidden">

                    <p class="red-color font-bold f-left margin-left-20 margin-top-10">File Name:</p>
                    <p class="gray-color font-bold f-right margin-right-20 margin-top-10 new_upsell_name">No File Chosen</p>
                    <br>
                    <br>
                    <div class="add_upsell_success_msg success-msg margin-bottom-30" style="display:none;">
                        <i class="fa fa-exclamation-triangle"></i>
                        Upsell added successfully.
                    </div>
                    <div class="center-block margin-top-20 padding-all-10 default-cursor gray-color">
                        <p class="upsell_camera">
                            <img class="ad-content img-responsive curved-border upsell_camera_image" src="" style="display:none;">
                        </p>
                    </div>
                    <div class="clear"></div>

                    <div class="f-right">
                        <button type="button" class="btn btn-dark upload_new_upsell_save_btn" style="display:none;">Save</button>
                        <button type="button" class="btn btn-dark choose_upload_new_upsell_btn">Choose from File</button>
                        <button type="button" class="btn btn-dark cancel_upload_new_upsell_btn">Cancel</button>
                    </div>
                    <input type="file" name="upsell_image_input" class="hidden" />
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>

                <div class="content-container f-left  unboxed no-margin-top upsell_container width-100per">
                    <div class="upsell template">
                        <img class="ad-content img-responsive curved-border upsell_image" src="<?php echo assets_url() ?>images/jollibee-ad.jpg" alt="jollibee ads">

                        <div class="f-left margin-top-20">
                            <p class="red-color font-bold">Date Uploaded:</p>
                            <p class="font-bold upsell_date_uploaded">May 10, 2015 | 12:34 PM</p>
                        </div>
                        <div class="f-right margin-top-20">
                            <button type="button" class="btn btn-dark archive_upsell_btn modal-trigger" modal-target="confirm-archive-upsell">Archive</button>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <!-- 
                    <img class="ad-content img-responsive curved-border" src="<?php echo assets_url() ?>images/jollibee-ad.jpg" alt="jollibee ads">
                    <div class="f-left">
                        <p class="red-color font-bold">Date Uploaded:</p>
                        <p class="font-bold">May 10, 2015 | 12:34 PM</p>
                    </div>
                    <div class="f-right">
                        <button type="button" class="btn btn-dark">Archive</button>
                    </div>
                    <div class="clear"></div>
                    <img class="ad-content img-responsive curved-border " src="<?php echo assets_url() ?>images/jollibee-ad.jpg" alt="jollibee ads">
                    <div class="f-left">
                        <p class="red-color font-bold">Date Uploaded:</p>
                        <p class="font-bold">May 10, 2015 | 12:34 PM</p>
                    </div>
                    <div class="f-right">
                        <button type="button" class="btn btn-dark">Archive</button>
                    </div>
                    <div class="clear"></div>
                    <img class="ad-content img-responsive curved-border " src="<?php echo assets_url() ?>images/jollibee-ad.jpg" alt="jollibee ads">
                    <div class="f-left">
                        <p class="red-color font-bold">Date Uploaded:</p>
                        <p class="font-bold">May 10, 2015 | 12:34 PM</p>
                    </div>
                    <div class="f-right">
                        <button type="button" class="btn btn-dark">Archive</button>
                    </div>
                    <div class="clear"></div>
                    <img class="ad-content img-responsive curved-border" src="<?php echo assets_url() ?>images/jollibee-ad.jpg" alt="jollibee ads">
                    <div class="f-left">
                        <p class="red-color font-bold">Date Uploaded:</p>
                        <p class="font-bold">May 10, 2015 | 12:34 PM</p>
                    </div>
                    <div class="f-right">
                        <button type="button" class="btn btn-dark">Archive</button>
                    </div>
                    <div class="clear"></div>
                    -->

                </div>
                <div class="clear"></div>
            </div>

            <div class="f-left  margin-left-20 width-50per"><!-- right -->

                <form id="add_promo">
                    <div class="content-container   f-right width-100per padding-all-20 create_new_promo_container hidden"><!-- hidden -->
                        <p class="f-left font-20">Create New Promo</p>
                        <label class="f-right red-color margin-top-5 margin-bottom-10">Fields with * are required</label>
                        <div class="clear"></div>
                        <hr class="margin-top-30">

                        <!-- error message start -->
                        <div class="add_promo_error_msg error-msg margin-bottom-30">
                            <i class="fa fa-exclamation-triangle"></i>
                            Please fill up all required fields.
                        </div>

                        <!-- error message end-->
                        <!-- success message start -->
                        <div class="add_promo_success_msg success-msg margin-bottom-30">
                            <i class="fa fa-exclamation-triangle"></i>
                            Promo added successfully.
                        </div>
                        <!-- success message end -->

                        <div class="center-block margin-top-20 width-100px border-dashed padding-all-10 default-cursor gray-color add_promo_camera">
                            <p class="camera">
                                <img class="img-responsive center-block promo_image" src="<?php echo assets_url() ?>images/icon-camera.png">
                                <span class="text-center font-12">Upload Image</span>
                            </p>
                            <input type="file" name="new_promo_image" class="hidden" />
                        </div>
                        <label>Promo Name<span class="red-color margin-left-5 font-bold font-20">*</span></label>
                        <input type="text" class="large new_promo_name" name="new_promo_name" datavalid="required" labelinput="Promo Name">
                        <label class="margin-top-10">Description<span class="red-color margin-left-5 font-bold font-20">*</span></label>
                        <textarea class="new_promo_description" name="new_promo_description" datavalid="required" labelinput="Promo Description"></textarea>
                        <label>Promo Mechanics<span class="red-color margin-left-5 font-bold font-20">*</span></label>
                        <br>
                        <div class="new_promo_mechanics_container mechanics_container">
                            <div class="new_promo_mechanics promo_mechanics">
                                <p class="display-inline-mid font-16 new_promo_mechanics_num promo_mechanics_num">1. </p>
                                <input type="text" class="large display-inline-mid" name="new_promo_mechanics[]" datavalid="required" labelinput="Promo Mechanics">
                                <br>
                                <a href="javascript:void(0)" class="f-right margin-top-5 add_another_mechanics">+ Add Another Mechanics</a>
                            </div>
                            <div class="new_promo_mechanics promo_mechanics template"><!-- template -->
                                <p class="display-inline-mid font-16 new_promo_mechanics_num promo_mechanics_num">2. </p>
                                <input type="text" class="large display-inline-mid" name="new_promo_mechanics[]" datavalid="required" labelinput="Promo Mechanics">
                                <br>
                                <a href="javascript:void(0)" class="f-right margin-top-5 remove_another_mechanics">Remove</a>
                            </div>
                        </div>
                        <div class="clear"></div>

                        <div class="f-right">
                            <button type="button" class="btn btn-dark save_promo_btn">Save Promo</button>
                            <button type="button" class=" btn btn-dark cancel_promo_btn">Cancel</button>
                        </div>
                        <div class="clear"></div>
                    </div>
                </form>
                <div class="content-container   f-right width-100per padding-all-20 edit_promo_container template"><!-- hidden -->
                    <form id="edit_promo">
                        <p class="f-left font-20">Edit Promo</p>
                        <label class="f-right red-color margin-top-5 margin-bottom-10">Fields with * are required</label>
                        <div class="clear"></div>
                        <hr class="margin-top-30">

                        <!-- error message start -->
                        <div class="edit_promo_error_msg error-msg margin-bottom-30">
                            <i class="fa fa-exclamation-triangle"></i>
                            Please fill up all required fields.
                        </div>

                        <!-- error message end-->
                        <!-- success message start -->
                        <div class="edit_promo_success_msg success-msg margin-bottom-30">
                            <i class="fa fa-exclamation-triangle"></i>
                            Promo updated successfully.
                        </div>
                        <!-- success message end -->

                        <div class="center-block margin-top-20 width-100px border-dashed padding-all-10 default-cursor gray-color edit_promo_camera">

                            <p class="edit_camera">
                                <img class="img-responsive center-block promo_image" src="<?php echo assets_url() ?>images/icon-camera.png">
                                <span class="text-center font-12">Upload Image</span>
                            </p>
                            <input type="file" name="edit_promo_image" class="hidden" />
                        </div>
                        <label>Promo Name<span class="red-color margin-left-5 font-bold font-20">*</span></label>
                        <input type="text" class="large edit_promo_name" name="edit_promo_name" datavalid="required" labelinput="Promo Name">
                        <label class="margin-top-10">Description<span class="red-color margin-left-5 font-bold font-20">*</span></label>
                        <textarea class="edit_promo_description" name="edit_promo_description" datavalid="required" labelinput="Promo Description"></textarea>
                        <label>Promo Mechanics<span class="red-color margin-left-5 font-bold font-20">*</span></label>
                        <br>
                        <div class="edit_promo_mechanics_container mechanics_container">
                            <div class="edit_promo_mechanics promo_mechanics">
                                <p class="display-inline-mid font-16 edit_promo_mechanics_num promo_mechanics_num">1. </p>
                                <br>
                                <input type="text" class="large display-inline-mid" name="edit_promo_mechanics[]" datavalid="required" labelinput="Promo Mechanics">
                                <br>
                                <a href="javascript:void(0)" class="f-right margin-top-5 add_another_mechanics">+ Add Another Mechanics</a>
                            </div>
                            <div class="edit_promo_mechanics promo_mechanics template"><!-- template -->
                                <p class="display-inline-mid font-16 edit_promo_mechanics_num promo_mechanics_num">2. </p>
                                <input type="text" class="large display-inline-mid" name="edit_promo_mechanics[]" datavalid="required" labelinput="Promo Mechanics">
                                <br>
                                <a href="javascript:void(0)" class="f-right margin-top-5 remove_another_mechanics">Remove</a>
                            </div>
                        </div>
                        <div class="clear"></div>

                        <div class="f-right">
                            <button type="button" class="btn btn-dark edit_promo_save_btn">Save Promo</button>
                            <button type="button" class=" btn btn-dark cancel_edit_promo_btn">Cancel</button>
                        </div>
                        <div class="clear"></div>
                    </form>
                </div>

                <div class="promo_container">
                    <div class="promo template"><!-- template -->
                        <div class="content-container f-right no-margin-top promo_collapsed width-100per"><!-- hidden -->
                            <div class="image-gallery">
                                <div class="img-container text-center">
                                    <p class="red-color font-16 promo_name"><strong>irReese'sitible Deal!</strong></p>
                                    <img class="img-responsive active center-block width-70per margin-top-15 promo_image" src="<?php echo assets_url() ?>images/jollibee-promo-1.jpg" alt="advertisement">
                                </div>

                                <div class="desc-container">
                                    <div class="desc-content active" gallery-desc="1">
                                        <p class="font-12 promo_description">Buy the Reese's Peanut Butter Jollibee promo pack save 20 pesos when you buy Jollibee Reese's Mix-ins.</p>
                                        <p class="font-12 light-red-color">Promo Mechanics</p>
                                        <ul class="promo_mechanics">
                                            <!--
                                            <li class="font-12">Promo is from March 30 to May 31, 2015.</li>
                                            <li class="font-12">Promo is valid for Jollibee Dine-in, Take Out or Drive Thru transactions only.</li>
                                            <li class="font-12">The Reese's promo bundle consists of 2 pieces 34g Reese's Peanut Butter Cup and a serialized promo coupon.</li>
                                            <li class="font-12">The promo bundles are sold at selected supermarkets nationwide (see list below).</li>
                                            <li class="font-12">The promo coupon should be surrendered to any Jollibee outlet nationwide to be able to availa of a discount for every purchase of a Jollibee Reese's Mix-ins.</li>
                                            <li class="font-12">Only one promo coupon will be honored for discount for every Reese's Mix-ins purchase.</li>
                                            <li class="font-12">Promo one coupon will not be honored if it has been</li>
                                            -->
                                        </ul>
                                    </div>
                                <!--
                                    <div class="desc-content" gallery-desc="2">
                                        <p class="font-12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eget molestie massa.</p>
                                        <p class="font-12 red-color">Aenean ut erat</p>
                                        <ul>
                                            <li class="font-12">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eget molestie massa. Aenean ut erat mi. Maecenas a accumsan elit.</li>
                                            <li class="font-12">Nullam quis feugiat neque. Quisque eget mi non ex suscipit tincidunt nec eget elit. Suspendisse molestie lacus eu aliquam cursus.</li>
                                            <li class="font-12">Morbi eleifend faucibus nibh, eu sodales mauris gravida sed. Mauris in quam vitae ex volutpat facilisis eget id ipsum. Sed ut laoreet felis.</li>
                                            <li class="font-12">Fusce porta, orci ut ornare tristique, nunc purus feugiat dolor, a malesuada purus mi et justo. Sed non imperdiet elit. Pellentesque mollis venenatis tempus. </li>
                                            <li class="font-12">Donec vel tortor non arcu pellentesque volutpat in vel diam. Ut sit amet consequat massa.</li>
                                            <li class="font-12">Nullam varius mattis nibh convallis accumsan. Nunc vel velit ultricies dolor commodo tincidunt. Fusce congue felis est, sed lacinia tortor auctor vel.</li>
                                            <li class="font-12">In purus lorem, faucibus vel finibus at, bibendum vel magna. Donec tempus urna nec odio pellentesque, sed pulvinar leo lacinia.</li>
                                        </ul>
                                    </div>
                                    <div class="desc-content" gallery-desc="3">
                                        <p class="font-12">Cras euismod odio sed porttitor dictum. Vivamus id </p>
                                        <p class="font-12 red-color">Suspendisse potenti</p>
                                        <ul>
                                            <li class="font-12">Vestibulum ipsum turpis, dignissim vel imperdiet a, tempor ut lorem. In ullamcorper, leo nec</li>
                                            <li class="font-12">hendrerit pulvinar, enim tellus tristique nibh, eget sollicitudin urna nisi nec turpis. Nunc in tincidunt leo, ultricies feugiat libero.</li>
                                            <li class="font-12">Fusce eros est, bibendum sit amet tincidunt ut, hendrerit sed nulla. Aenean mollis leo et mi suscipit</li>
                                            <li class="font-12">suscipit imperdiet ligula elementum. Morbi laoreet lobortis neque, at rutrum sem venenatis lacinia</li>
                                        </ul>
                                    </div>
                                -->
                                </div>
                                <div class="f-right margin-top-10 margin-right-10 promo_buttons">
                                    <button type="button" class="btn btn-dark margin-right-10 edit_promo_btn">Edit Promo</button>
                                    <button type="button" class="btn btn-dark margin-right-10 archive_promo_btn modal-trigger" modal-target="confirm-archive-promo">Archive</button>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>

                        <div class="content-container opaque f-right padding-all-20 promo_uncollapsed width-100per"><!-- hidden -->
                            <p class="font-bold font-18 promo_name">irReese'sitible Deal!</p>
                            <p class="margin-top-10 promo_description">Buy the Reese's Peanut Butter Jollibee promo pack and save 20 pesos when you buy Jollibee Reese's Mix-ins.</p>
                        </div>
                    </div>

                    <!--
                    <div class="content-container opaque  f-right padding-all-20">
                        <p class="font-bold font-18">irReese'sitible Deal!</p>
                        <p class="margin-top-10">Buy the Reese's Peanut Butter Jollibee promo pack and save 20 pesos when you buy Jollibee Reese's Mix-ins.</p>
                    </div>


                    <div class="content-container opaque  f-right padding-all-20">
                        <p class="font-bold font-18">irReese'sitible Deal!</p>
                        <p class="margin-top-10">Buy the Reese's Peanut Butter Jollibee promo pack and save 20 pesos when you buy Jollibee Reese's Mix-ins.</p>
                    </div>


                    <div class="content-container opaque f-right padding-all-20">
                        <p class="font-bold font-18">irReese'sitible Deal!</p>
                        <p class="margin-top-10">Buy the Reese's Peanut Butter Jollibee promo pack and save 20 pesos when you buy Jollibee Reese's Mix-ins.</p>
                    </div>
                    -->
                </div>
            </div>
            <div class="clear"></div>




            <div class="clear"></div>
        </div>
    </div>
    <!--promos end-->
    
    <div class="row hidden prod_unavailability_reports" content="prod_unavailability_reports">
        <div class="content-container unboxed">
            <!-- start pagination -->
            <!-- <div class="no-padding-all">
                 <div pagination-container="true"></div>
            </div> -->

            <div style="width: 100%; max-width: 900px;  box-shadow: 0 0px 0px rgba(0,0,0,0) !important; padding-bottom:0; padding-top:0;" class="">
                <ul class="prod-avail-custom-pagination1" id="prod_paginations">

                </ul>
            </div>

            <div class="hidden">
                <ul class="prod-paganations-hidden content-container">

                </ul>
            </div>
            <!-- end pagination -->
            <div class="tbl / text-center" style="margin-top:-40px;">
                <table id="pu_report_table">
                    <thead>
                        <tr>
                            <th class="width-10per / text-center">No.</th>
                            <th class="text-center">Store</th>
                            <!-- <th class="text-center">Type</th> -->
                            <th class="text-center">Product Name</th>
                            <!-- <th class="text-center">Core Product</th> -->
                            <!-- <th class="text-center">Unavailable Products</th> -->
                            <th class="text-center">Items</th>
                            <th class="text-center">Date/Time</th>
                        </tr>
                    </thead>
                    <tbody class="pu_report_tr_container">
                        <tr class="template pu_report_tr notthis">
                            <td data-label="pu_report_number">1</td>
                            <td data-label="pu_report_store">JB NCR</td>
                            <!-- <td data-label="pu_report_type">Individual</td> -->
                            <td data-label="pu_report_product_info">Chicken</td>
                            <!-- <td data-label="pu_report_product_items"></td> -->
                            <td data-label="pu_report_items">On</td>
                            <td data-label="pu_report_date_time">May 1, 1992</td>
                        </tr>
                    </tbody>
                </table>
                <table id="pu_report_table_hidden" class="template" style="display:none;">
                    <thead>
                        <tr>
                            <th class="width-10per / text-center">No.</th>
                            <th class="text-center">Store</th>
                            <!-- <th class="text-center">Type</th> -->
                            <th class="text-center">Product Name</th>
                            <!-- <th class="text-center">Core Product</th> -->
                            <!-- <th class="text-center">Unavailable Products</th> -->
                            <th class="text-center">Items</th>
                            <th class="text-center">Date/Time</th>
                        </tr>
                    </thead>
                    <tbody class="pu_report_tr_container_hidden">
                        <tr class="template pu_report_tr_hidden notthis">
                            <td data-label="pu_report_number">1</td>
                            <td data-label="pu_report_store">JB NCR</td>
                            <!-- <td data-label="pu_report_type">Individual</td> -->
                            <td data-label="pu_report_product_info">Chicken</td>
                            <!-- <td data-label="pu_report_product_items"></td> -->
                            <td data-label="pu_report_items">On</td>
                            <td data-label="pu_report_date_time">May 1, 1992</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row reports" id="reports"  content="reports">
        <div class="content-container unboxed">
            <div class="tbl / text-center" style="padding: 0px !important">
                <table id="reroute_orders_table" class="hidden" >
                    <thead>
                        <tr>
                            <th class="width-10per / text-center">Date</th>
                            <th class="text-center">Order ID</th>
                            <th class="text-center">Store Origin</th>
                            <th class="text-center">Reroute Store</th>
                        </tr>
                    </thead>
                    <tbody class="reroute_orders_container">
                        <tr class="template reroute_orders_tr notthis">
                            <td data-label="reroute_date"></td>
                            <td data-label="reroute_order_id"></td>
                            <td data-label="reroute_store_origin"></td>
                            <td data-label="reroute_store_destination"></td>
                        </tr>
                    </tbody>
                </table>

                <table id="manually_relayed_table" class="hidden">
                    <thead>
                        <tr>
                            <th class="width-10per / text-center">Order ID</th>
                            <th class="text-center">Store</th>
                            <th class="text-center">Customer Name</th>
                            <th class="text-center">Customer Contact</th>
                            <th class="text-center">Customer Address</th>
                            <th class="text-center">Sales</th>
                            <th class="text-center">Transaction Time</th>
                        </tr>
                    </thead>
                    <tbody class="manually_relayed_container">
                        <tr class="template manually_relayed_tr notthis">
                            <td data-label="manually_relayed_order_id"></td>
                            <td data-label="manually_relayed_store_name"></td>
                            <td data-label="manually_relayed_customer_name"></td>
                            <td data-label="manually_relayed_customer_contact"></td>
                            <td data-label="manually_relayed_customer_address"></td>
                            <td data-label="manually_relayed_sales"></td>
                            <td data-label="manually_relayed_transaction_time"></td>
                        </tr>
                    </tbody>
                </table>

                <table id="rejected_orders_table" class="hidden">
                    <thead>
                        <tr>
                            <th class="width-10per / text-center">Store Name</th>
                            <th class="text-center">Customer Name</th>
                            <th class="text-center">Order ID</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Time</th>
                            <th class="text-center">Reason for Rejection</th>
                        </tr>
                    </thead>
                    <tbody class="rejected_orders_container" >
                        <tr class="template rejected_orders_tr notthis">
                            <td data-label="rejected_orders_store_name"></td>
                            <td data-label="rejected_orders_customer_name"></td>
                            <td data-label="rejected_orders_order_id"></td>
                            <td data-label="rejected_orders_date"></td>
                            <td data-label="rejected_orders_time"></td>
                            <td data-label="rejected_orders_reason"></td>
                        </tr>
                    </tbody>
                </table>

                <table id="follow_up_order_table" class="hidden" style="margin-left:-150px">
                    <thead style="font-size:12px">
                        <tr>
                            <th class="width-10per / text-center">Date</th>
                            <th class="text-center" colspan="4">Follow Up Information</th>
                            <th class="text-center">Customer Name</th>
                            <th class="text-center">Store Code</th>
                            <th class="text-center">Store Name</th>
                            <th class="text-center">Province</th>
                            <th class="text-center">Delivery Guarantee</th>
                            <th class="text-center">Order ID</th>
                            <th class="text-center" colspan="2">Transaction Received</th>
                            <th class="text-center" colspan="2">Transaction Completed</th>
                        </tr>
                        <tr>
                            <th class="width-10per / text-center"></th>
                            <th class="text-center">Agent</th>
                            <th class="text-center">Count</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Time</th>
                            <th class="text-center"></th>
                            <th class="text-center"></th>
                            <th class="text-center"></th>
                            <th class="text-center"></th>
                            <th class="text-center"></th>
                            <th class="text-center"></th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Time</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Time</th>
                        </tr>
                    </thead>
                    <tbody class="follow_up_order_container" style="font-size:12px">
                        <tr class="template follow_up_order_tr notthis">
                            <td data-label="follow_up_date" class="width-10per / text-center"></td>
                            <td data-label="follow_up_agent" class="text-center">Agent</td>
                            <td data-label="follow_up_count" class="text-center">Count</td>
                            <td data-label="follow_up_fu_date" class="text-center">Date</td>
                            <td data-label="follow_up_fu_time" class="text-center">Time</td>
                            <td data-label="follow_up_customer_name" class="text-center"></td>
                            <td data-label="follow_up_store_code" class="text-center"></td>
                            <td data-label="follow_up_store_name" class="text-center"></td>
                            <td data-label="follow_up_province" class="text-center"></td>
                            <td data-label="follow_up_delivery_guarantee" class="text-center"></td>
                            <td data-label="follow_up_order_id" class="text-center"></td>
                            <td data-label="follow_up_date_recieved" class="text-center">Date</td>
                            <td data-label="follow_up_time_recieved" class="text-center">Time</td>
                            <td data-label="follow_up_date_completed" class="text-center">Date</td>
                            <td data-label="follow_up_time_completed" class="text-center">Time</td>
                        </tr>
                    </tbody>
                </table>

                <table id="nkag_report_table" class="hidden">
                    <thead>
                        <tr>
                            <th class="width-10per / text-center">Order ID</th>
                            <th class="text-center">Store</th>
                            <th class="text-center">NKAG</th>
                            <th class="text-center">Customer Information</th>
                            <!-- <th class="text-center">Customer Name</th>
                            <th class="text-center">Customer Contact</th>
                            <th class="text-center">Customer Address</th> -->
                            <th class="text-center">Items</th>
                            <th class="text-center">Sales</th>
                            <th class="text-center">Transaction Time</th>
                        </tr>
                    </thead>
                    <tbody class="nkag_report_container">
                        <tr class="template nkag_report_tr notthis">
                            <td data-label="nkag_report_order_id"></td>
                            <td data-label="nkag_report_store_name"></td>
                            <td data-label="nkag_report_nkag_account"></td>
                            <td data-label="nkag_report_customer_information"></td>
                            <!-- <td data-label="nkag_report_customer_name"></td>
                            <td data-label="nkag_report_customer_contact"></td>
                            <td data-label="nkag_report_customer_address"></td> -->
                            <td data-label="nkag_report_items"></td>
                            <td data-label="nkag_report_sales"></td>
                            <td data-label="nkag_report_transaction_time"></td>
                        </tr>
                    </tbody>
                </table>
                <!-- <table id="sales_report_table_hidden" class="template" style="display:none;">
                     <thead class="hidden_header">
                         <tr>
                             <th class="width-10per / text-center">No.</th>
                             <th class="text-center">Date</th>
                             <th class="text-center">Total Sales</th>
                             <th class="text-center"> Food Sales</th>
                             <th class="text-center">Transaction Count</th>
                         </tr>
                     </thead>
                     <tbody class="sales_report_tr_container_hidden">
                         <tr class="template sales_report_tr_hidden notthis">
                             <td data-label="sales_report_number">1</td>
                             <td data-label="sales_report_date">May 1, 2015</td>
                             <td data-label="sales_report_total_cost">15,021.25 Php</td>
                             <td data-label="sales_report_total_bill">15,124.75 Php</td>
                             <td data-label="sales_report_count">215</td>
                         </tr>
                     </tbody>
                     <tfoot class="hidden_footer">
                         <tr class="template sales_report_tr_total_hidden notthis">
                             <th data-label="total">Total</th>
                             <th data-label="date_range">May 1, 2015</th>
                             <th data-label="total_cost">15,021.25 Php</th>
                             <th data-label="total_bill">15,124.75 Php</th>
                             <th data-label="total_count" style="text-align:right">215</th>
                         </tr>
                     </tfoot>
                 </table>-->

            </div>
        </div>
    </div>

    <div class="row hidden contact" content="contact">
        <div class="content-container unboxed">
            <div class="tbl / text-center">
                <table id="cn_report_table">
                    <thead>
                        <tr>
                            <th class="text-center">Customer Name</th>
                            <th class="text-center">Contact Number</th>
                            <!-- <th class="text-center">Type</th> -->
                        </tr>
                    </thead>
                    <tbody class="cn_report_tr_container">
                        <tr class="template cn_report_tr notthis">
                            <td data-label="cn_report_name">Joanathan Ornido</td>
                            <td data-label="cn_report_number">09271231234</td>
                            <!-- <td data-label="cn_report_type">Mobile</td> -->
                        </tr>
                    </tbody>
                </table>
                <table id="cn_report_table_hidden" class="template" style="display:none;">
                    <thead>
                        <tr>
                            <th class="text-center">Customer Name</th>
                            <th class="text-center">Contact Number</th>
                            <!-- <th class="text-center">Type</th> -->
                        </tr>
                    </thead>
                    <tbody class="cn_report_tr_container_hidden">
                        <tr class="template cn_report_tr_hidden notthis">
                            <td data-label="cn_report_name">Joanathan Ornido</td>
                            <td data-label="cn_report_number">09271231234</td>
                            <!-- <td data-label="cn_report_type">Mobile</td> -->
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row hidden repeat" content="repeat" id="repeat">
        <div class="content-container unboxed">
            <div class="tbl / text-center">
                <table id="repeat_table" class="hidden">
                    <thead>
                        <tr>
                            <th class="text-center">Month</th>
                            <th class="text-center">New Customers</th>
                            <th class="text-center">Percentage</th>
                            <th class="text-center">Total Previous Customers</th>
                            <th class="text-center">Percentage</th>
                            <th class="text-center">Total Customers</th>
                            <th class="text-center">Percentage</th>
                            <!-- <th class="text-center">Type</th> -->
                        </tr>
                    </thead>
                    <tbody class="repeat_tr_container">
                        <tr class="template repeat_tr notthis">
                            <td data-label="repeat_month">Joanathan Ornido</td>
                            <td data-label="repeat_new_customers">09271231234</td>
                            <td data-label="repeat_new_customers_per">09271231234</td>
                            <td data-label="repeat_previous_customers">09271231234</td>
                            <td data-label="repeat_previous_customers_per">09271231234</td>
                            <td data-label="repeat_total_customers">09271231234</td>
                            <td data-label="repeat_percentage">09271231234</td>
                            <!-- <td data-label="cn_report_type">Mobile</td> -->
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row hidden dst_spec" content="dst_spec"  id="dst_reports_specific_content">

        <!-- sample-1-->
        <section class="section-order template">
            <div class="order-overview content-container viewable ">
                <div>
                    <div class="width-35per f-left">
                        <p class="font-14 margin-bottom-5"><strong>Order ID: <info class="data-order-id">954861</info></strong></p>
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong><info class="data-full-name"></info></p>
                        <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong><info class="data-contact-number">(+63) 915-516-6153 </info><!--<i class="fa fa-mobile"></i> Globe--></p>
                    </div>

                    <div class="width-40per / f-left / margin-top-25">
                        <p class="font-14 margin-bottom-5"><strong><span class="red-color">Date/Time Accepted: </span></strong><info class="data-order-date">May 18, 2015 | 11:42:02</info></p>
                        <!--<p class="font-14 margin-bottom-5"><strong><span class="red-color">Date/Time Accepted: </span></strong>May 18, 2015 | 11:42:02</p>-->
                    </div>

                    <div class="width-25per f-right">
                        <label class="font-20 / gray-color / margin-left-10">Hitrate</label>
                        <h1 class="no-margin-all data-hit-rate"></h1>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            <div class="order-detailed content-container hidden">
                <div>
                    <div class="f-left margin-top-15">
                        <p class="font-20"><strong>Order ID: <info class="data-order-id">954861</info></strong></p>

                    </div>

                    <div class="width-25per f-right / margin-bottom-10">
                        <label class="font-20 / gray-color / margin-left-15">Hitrate</label>
                        <h1 class="no-margin-all data-hit-rate"></h1>
                    </div>



                    <div class="clear"></div>
                    <hr>

                    <div class="data-container split">

                        <!-- clients information -->
                        <div class="margin-top-20">
                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Name: </strong></p>
                            <p class="f-right font-12"><info class="data-full-name">Jonathan R. Ornido</info></p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Contact Number: </strong></p>
                            <p class="f-right font-12"><info class="data-contact-number">(+63) 915-516-6153 </info></p>
                            <div class="clear"></div>

                            <p class="f-left red-color font-12 margin-bottom-5"><strong>Transaction Time:</strong></p>
                            <p class="f-right font-12"><info class="data-order-date">May 18, 2015 | 11:42:02</info>                                                                                  </p>
                            <div class="clear"></div>
                        </div>

                        <!-- delivery address -->
                        <div>
                            <label class="margin-top-10 margin-bottom-10 ">Delivery Address:</label>
                            <div class="bggray-light padding-all-5 font-14 small-curved-border">
                                <div class="display-inline-mid padding-left-20">
                                    <img src="<?php echo assets_url() ?>/images/work-icon.png" alt="work icon">
                                    <p class="font-12 text-center no-margin-all"><strong>HOME</strong></p>
                                </div>
                                <div class="display-inline-mid f-right width-85per">
                                    <!--<p class="no-margin-all"><strong class="data-address-label">Address: </strong></p>-->
                                    <p class="no-margin-all data-full-address">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR</p>
                                    <p class="no-margin-all gray-color data-land-mark"></p>
                                </div>
                            </div>
                        </div>

                        <!-- cards -->
                        <label class="margin-top-15">CARDS:</label>

                        <div class="happy_plus_cards_container">
                            <div class="bggray-light margin-bottom-10 padding-all-5 font-14 small-curved-border happy_plus_cards template">
                                <div class="display-inline-mid margin-left-10">
                                    <img class="thumb " src="<?php echo base_url() ?>/assets/images/happy-plus.jpg" alt="happy-plus">
                                </div>
                                <div class="display-inline-mid margin-left-10 divider padding-left-10">
                                    <p data-label="card_number" class="no-margin-all">008-248-369-154</p>
                                    <span class="red-color"><strong>Exp. Date:</strong></span>
                                    <p data-label="card_expiration" style="display: inline;"> September 20, 2016</p>
                                </div>
                                <div class="display-inline-mid text-center margin-left-10">
                                    <!-- <a href="#">Show Card<br>History</a> -->
                                </div>
                            </div>
                        </div>


                        <div class="margin-top-10">
                            <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Payment Method: </strong></p>
                            <p class="f-right / font-12">Cash</p>
                            <div class="clear"></div>

                            <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Change For: </strong></p>
                            <p class="f-right / font-12"><info class="change_for">500.00 PHP</info></p>
                            <div class="clear"></div>

                            <p class="f-left font-12 red-color  margin-bottom-5"><strong>Remarks: </strong></p>
                            <p class="f-right font-12 gray-color">No Remarks</p>
                            <div class="clear"></div>

                            <p class="f-left font-12 red-color  margin-bottom-5"><strong>Grid: </strong></p>
                            <p class="f-right font-12 gray-color"><info class="grid"></info></p>
                            <div class="clear"></div>

                            <p class="f-left font-12 red-color  margin-bottom-5"><strong>Serving Time: </strong></p>
                            <p class="f-right font-12 gray-color"><info class="serving_time"></info></p>
                            <div class="clear"></div>

                        </div>
                    </div>

                    <div class="data-container split margin-left-15">

                        <label class="margin-top-15">order: </label>

                        <div class="small-curved-border">
                            <table class="font-14" style="width: 100%; display:block;">
                                <thead class="bggray-dark">
                                    <tr>
                                        <th class="padding-all-10" style="width:10%;">Quantity</th>
                                        <th class="padding-all-10" style="width:30%;">Product</th>
                                        <th class="padding-all-10" style="width:10%;">Price</th>
                                        <th class="padding-all-10" style="width:15%;">Subtotal</th>
                                    </tr>
                                </thead>
                                <tbody class="bggray-light" data-transaction-orders="detailed_orders" style="display:block; max-height: 300px; overflow-y: auto; width:100%;">
                                    <tr data-template-transaction-for="order" class="template">
                                        <td class="padding-all-10 text-center" data-label="quantity"></td>
                                        <td class="padding-all-10 padding-left-35" data-label="product"></td>
                                        <td class="padding-all-10" data-label="price"></td>
                                        <td class="padding-all-10" data-label="sub_total"></td>
                                    </tr>
                                </tbody>
                                <tbody class="bggray-light">
                                    <tr class="bggray-middark">
                                        <td colspan="3" style="width:40%;" class="padding-all-10 text-left">Total Cost</td>
                                        <td colspan="2" style="width:60%;" class="padding-all-10 text-right" data-label="total_cost">1852.40 PHP</td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" style="width:50%;" class="padding-all-10 text-left">Added VAT</td>
                                        <td colspan="3" style="width:50%;" class="padding-all-10 text-right" data-label="added_vat">222.29 PHP</td>
                                    </tr>
                                    <tr class="bggray-dark">
                                        <td colspan="2" style="width:40%;" class="padding-all-10 text-left font-16"><strong>Total Bill</strong>
                                        </td>
                                        <td colspan="3" style="width:60%;" class="padding-all-10 text-right font-14">
                                            <strong data-label="total_bill">1481.92 PHP</strong></td>
                                    </tr>
                                    <tr class="bggray-middark">
                                        <td colspan="2" style="width:50%;" class="padding-all-10 text-left">Delivery Charge</td>
                                        <td colspan="3" style="width:50%;" class="padding-all-10 text-right" data-label="delivery_charge">168.40 PHP</td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>

                    </div>
                    <div class="margin-top-20">


                        <button type="button" class="btn btn-dark f-right margin-right-10 modal-trigger transaction-logs" modal-target="order-logs">Logs</button>

                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </section>

        <!--<div class="content-container viewable ">
            <div>
                <div class="width-35per f-left">
                    <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                    <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                </div>

                <div class="width-40per / f-left / margin-top-25">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Date/Time Accepted: </span></strong>May 18, 2015 | 11:42:02</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Date/Time Accepted: </span></strong>May 18, 2015 | 11:42:02</p>
                </div>

                <div class="width-25per f-right">
                    <label class="font-20 / gray-color / margin-left-10">Hitrate</label>
                    <h1 class="no-margin-all">100.0 %</h1>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="content-container viewable ">
            <div>
                <div class="width-35per f-left">
                    <p class="font-14 margin-bottom-5"><strong>Order ID: 954861</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong>Jonathan R. Ornido</p>
                    <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong>(+63) 915-516-6153 <i class="fa fa-mobile"></i> Globe</p>
                </div>

                <div class="width-40per / f-left / margin-top-25">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Date/Time Accepted: </span></strong>May 18, 2015 | 11:42:02</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Date/Time Accepted: </span></strong>May 18, 2015 | 11:42:02</p>
                </div>

                <div class="width-25per f-right">
                    <label class="font-20 / gray-color / margin-left-10">Hitrate</label>
                    <h1 class="no-margin-all">100.0 %</h1>
                </div>
                <div class="clear"></div>
            </div>
        </div>-->

    </div>

    <div class="row hidden sales_prod" content="sales_prod" id="sales_reports_by_products_content">
        <div class="content-container unboxed">
            <!-- start pagination -->
            <!-- <div class="no-padding-all">
                 <div pagination-container="true"></div>
            </div> -->

            <div style="width: 100%; max-width: 900px;  box-shadow: 0 0px 0px rgba(0,0,0,0) !important; padding-bottom:0; padding-top:0;" class="">
                <ul class="prod-avail-custom-pagination1" id="item_paginations">

                </ul>
            </div>

            <div class="hidden">
                <ul class="item-paganations-hidden content-container">

                </ul>
            </div>
            <!-- end pagination -->
        
            <div class="tbl no-padding-all">
                <table id="product_sales_report_table">
                    <thead class="font-10">
                            <tr>
                                <th class="font-14" rowspan="2"><p class="text-center">Product Name</p></th>
                                <th class="font-14 hidden" rowspan="2"><p class="text-center">Store</p></th>
                                <th class="font-14 hidden" rowspan="2"><p class="text-center">Date</p></th>
                                <th class="text-center font-14" colspan="2">Food Sales</th>
                                <th class="text-center font-14" colspan="2">Total Sales</th>                                
                                <th class="text-center font-14" colspan="2">Quantity</th>                                
                            </tr>                         
                            <tr>
                                <th class="text-center bggray-darkred">Amount</th>
                                <th class="text-center bggray-brightred">Percentage</th>
                                <th class="text-center bggray-darkred">Amount</th>
                                <th class="text-center bggray-brightred">Percentage</th>
                                <th class="text-center bggray-darkred">Amount</th>
                                <th class="text-center bggray-brightred">Percentage</th>
                            </tr>
                    </thead>
                    <tbody class="product-sales-report-tbody font-14">
                        <!-- <tr>
                            <td>
                                <div class="margin-left-20">
                                    <div class="f-left">
                                        <p class="f-left font-10">May <br>2015</p>
                                        <p class="f-left font-16"><strong>01</strong></p>
                                        <div class="clear"></div>
                                        <p class="font-10 margin-left-10">Friday</p>
                                    </div>
                                    <div class="f-left margin-left-20 margin-right-20">
                                        <p class="font-20"><strong>-</strong></p>
                                    </div>
                                    <div class="f-left">
                                        <p class="f-left font-10">May <br>2015</p>
                                        <p class="f-left font-16"><strong>01</strong></p>
                                        <div class="clear"></div>
                                        <p class="font-10 margin-left-10 ">Friday</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </td>
                            <td><span class="product-name">Coca-Cola</span></td>
                            <td>15,487.75 PHP </td>
                            <td>15,487.75 PHP</td>
                            <td>584</td>
                            <td>84.2 %</td>
                            <td>75.3 %</td>
                        </tr> -->
                    </tbody>
                </table>

                <div id="product_sales_report_table_excel_container"></div>

                <table id="product_sales_report_table_template">
                    <tr class="template">
                        <td data-label-for="product-name">Coca-Cola</td>
                        <td data-label-for="store-name" class="hidden"></td>
                        <td data-label-for="date-range" class="hidden"></td>
                        <td data-label-for="food-sales-count" class="text-center">1219.00 PHP</td>
                        <td data-label-for="sales-percentage"class="text-center">0 %</td>
                        <td data-label-for="sales-count"class="text-center">1219.00 PHP</td>
                        <td data-label-for="sales-percentage"class="text-center">0 %</td>
                        <td data-label-for="quantity-count"class="text-center">584</td>
                        <td data-label-for="quantity-percentage"class="text-center">0 %</td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

    <div class="row hidden sales" content="sales" id="sales_reports_content">
        <div class="content-container unboxed">
            <div class="tbl / text-center">
                <table class="viewable hidden" id="sales_report_table">
                    <thead>
                        <tr>
                            <th class="width-10per / text-center">No.</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Total Sales</th>
                            <th class="text-center"> Food Sales</th>
                            <th class="text-center">Transaction Count</th>
                        </tr>
                    </thead>
                    <tbody class="sales_report_tr_container">
                        <tr class="template sales_report_tr notthis">
                            <td data-label="sales_report_number">1</td>
                            <td data-label="sales_report_date">May 1, 2015</td>
                            <td data-label="sales_report_total_cost">15,021.25 Php</td>
                            <td data-label="sales_report_total_bill">15,124.75 Php</td>
                            <td data-label="sales_report_count">215</td>
                        </tr>
                        <tr class="template sales_report_tr_total notthis">
                            <th class="text-center" ></th>
                            <th class="text-center" >Net Sales</th>
                            <th class="text-center" data-label="total_cost">15,021.25 Php</th>
                            <th class="text-center" data-label="total_bill">15,124.75 Php</th>
                            <th class="text-center" data-label="total_count">215</th>
                        </tr>
                    </tbody>
                </table>
                <table id="sales_report_table_hidden" class="template" style="display:none;">
                    <thead class="hidden_header">
                        <tr>
                            <th class="width-10per / text-center">No.</th>
                            <th class="text-center">Date</th>
                            <th class="text-center">Total Sales</th>
                            <th class="text-center"> Food Sales</th>
                            <th class="text-center">Transaction Count</th>
                        </tr>
                    </thead>
                    <tbody class="sales_report_tr_container_hidden">
                        <tr class="template sales_report_tr_hidden notthis">
                            <td data-label="sales_report_number">1</td>
                            <td data-label="sales_report_date">May 1, 2015</td>
                            <td data-label="sales_report_total_cost">15,021.25 Php</td>
                            <td data-label="sales_report_total_bill">15,124.75 Php</td>
                            <td data-label="sales_report_count">215</td>
                        </tr>
                    </tbody>
                    <tfoot class="hidden_footer">
                        <tr class="template sales_report_tr_total_hidden notthis">
                            <th data-label="total">Net Sales</th>
                            <th data-label="date_range">May 1, 2015</th>
                            <th data-label="total_cost">15,021.25 Php</th>
                            <th data-label="total_bill">15,124.75 Php</th>
                            <th data-label="total_count" style="text-align:right">215</th>
                        </tr>
                    </tfoot>
                </table>
                <table class="viewable hidden" id="sales_report_table_store">
                    <thead class="font-14">
                        <tr>
                            <th class="width-30px text-center">No.</th>
                            <th class="text-center">Store Name</th>
                            <th class="text-center">Store Code</th>
                            <th class="text-center">Total Sales</th>
                            <th class="text-center">Food Sales</th>
                            <th class="text-center">Transaction Count</th>
                        </tr>
                    </thead>
                    <tbody class="font-14 sales_report_tr_container_store">
                        <tr class="template sales_report_tr_store notthis">
                            <td class="width-30px text-center" data-label="sales_report_number_store">1</td>
                            <td class="text-center" data-label="sales_report_name_store">MM Ortigas Roosevelt</td>
                            <td class="text-center" data-label="sales_report_code_store">JB0444</td>
                            <td class="text-center" data-label="sales_report_total_sales_store">15,021,25 PHP</td>
                            <td class="text-center" data-label="sales_report_food_sales_store">15,124,75 PHP</td>
                            <td class="text-center" data-label="sales_report_count_store">215</td>
                        </tr>
                        <tr class="template sales_report_tr_total_store notthis">
                            <th class="text-center" ></th>
                            <th class="text-center" ></th>
                            <th class="text-center" >Net Sales</th>
                            <th class="text-center" data-label="total_cost">15,021.25 Php</th>
                            <th class="text-center" data-label="total_bill">15,124.75 Php</th>
                            <th class="text-center" data-label="total_count">215</th>
                        </tr>
                    </tbody>
                </table>
                <table class="hidden template" style="display:none;" id="sales_report_table_store_hidden">
                    <thead class="font-14">
                        <tr>
                            <!-- <th class="width-30px text-center">No.</th> -->
                            <th class="text-center">Dates</th>
                            <th class="text-center">Store Name</th>
                            <th class="text-center">Store Code</th>
                            <th class="text-center">Total Sales</th>
                            <th class="text-center">Food Sales</th>
                            <th class="text-center">Transaction Count</th>
                        </tr>
                    </thead>
                    <tbody class="font-14 sales_report_tr_container_store_hidden">
                        <tr class="template sales_report_tr_store_hidden notthis">
                            <!-- <td class="width-30px text-center" data-label="sales_report_number_store">1</td> -->
                            <td class="text-center" data-label="sales_report_date_store"></td>
                            <td class="text-center" data-label="sales_report_name_store">MM Ortigas Roosevelt</td>
                            <td class="text-center" data-label="sales_report_code_store">JB0444</td>
                            <td class="text-center" data-label="sales_report_total_sales_store">15,021,25 PHP</td>
                            <td class="text-center" data-label="sales_report_food_sales_store">15,124,75 PHP</td>
                            <td class="text-center" data-label="sales_report_count_store">215</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row hidden" content="dst_per_day" id="dst_per_day">
        <section class="container-fluid" section-style="top-panel" id="dst_per_day_reports_content">

            <div class="container-fluid">
                <div class="row">
                    <div class="contents margin-top-20">
                        <div class="display-inline-mid">
                            
                            <div id="dst_date_from" class="f-left margin-right-20 ">
                                <label class="margin-bottom-5">Date From:</label><br>
                                <div class="date-picker" id="dst_for_day_date_from_dp">
                                    <input type="text" readonly>
                                    <span class="fa fa-calendar text-center red-color"></span>
                                </div>
                            </div>
                            
                            <div id="dst_date_to" class="f-left margin-right-20">
                                <label class="margin-bottom-5">Date To:</label><br>
                                <div class="date-picker" id="dst_for_day_date_to_dp">
                                    <input type="text" readonly>
                                    <span class="fa fa-calendar text-center red-color"></span>
                                </div>
                            </div>

                            <div class="f-left margin-right-20">
                                <label class="margin-bottom-5">Province:</label><br>
                                <div class="select custom-province-filter margin-right-10">
                                    <div class="frm-custom-dropdown">
                                        <div class="frm-custom-dropdown-txt"><input type="text" id="dst_per_day_province" data-value="0" class="dd-txt" name="province_select" value="Show All Provinces"></div>
                                        <div class="frm-custom-icon"></div>
                                        <div class="frm-custom-dropdown-option">
                                            <div class="option custom-filter-option" data-value="0">All Provinces</div>
                                        </div>
                                    </div>
                                    <select class="frm-custom-dropdown-origin" style="display: none;">
                                    </select>
                                </div>
                            </div>

                            <!-- filter by transactions-->
                            <div class="f-left">
                                <label class="margin-bottom-5">Transactions:</label><br>
                                <div class="select custom-transaction-filter margin-right-10">
                                    <div class="frm-custom-dropdown">
                                        <div class="frm-custom-dropdown-txt"><input type="text" id="dst_per_day_type" data-value="All Transaction" class="dd-txt" name="transaction_select" value="Show All Transactions"></div>
                                        <div class="frm-custom-icon"></div>
                                        <div class="frm-custom-dropdown-option">
                                            <div class="option custom-filter-option" data-value="All Transaction">All Transactions</div>
                                            <div class="option custom-filter-option" data-value="Store Channel">Call Order</div>
                                            <div class="option custom-filter-option" data-value="Web">Web Order</div>
                                            <div class="option custom-filter-option" data-value="SMS">SMS Order</div>
                                            <div class="option custom-filter-option" data-value="nkag">NKAG Order</div>
                                            <div class="option custom-filter-option" data-value="Food Panda">Food Panda</div>
                                        </div>
                                    </div>
                                    <select class="frm-custom-dropdown-origin" style="display: none;">
                                    </select>
                                </div>
                            </div>

                            <!-- filter by callcenters-->
                            <div class="f-left margin-right-20">
                                <label class="margin-bottom-5">Callcenter:</label><br>
                                <div class="select custom-callcenter-filter margin-right-10">
                                    <div class="frm-custom-dropdown">
                                        <div class="frm-custom-dropdown-txt"><input type="text" id="dst_per_day_callcenter" data-value="0" class="dd-txt" name="callcenter_select" value="Show All Callcenter"></div>
                                        <div class="frm-custom-icon"></div>
                                        <div class="frm-custom-dropdown-option">
                                            <div class="option custom-filter-option" data-value="0">All Callcenter</div>
                                        </div>
                                    </div>
                                    <select class="frm-custom-dropdown-origin" style="display: none;">
                                    </select>
                                </div>
                            </div>

                            <!-- filter by store-->
                            <div class="f-left">
                                <label class="margin-bottom-5">Store:</label><br>
                                <div class="select custom-store-filter margin-right-10">
                                    <div class="frm-custom-dropdown">
                                        <div class="frm-custom-dropdown-txt"><input type="text" id="dst_per_day_store"  data-value="0" class="dd-txt" name="store_select" value="Show All Store"></div>
                                        <div class="frm-custom-icon"></div>
                                        <div class="frm-custom-dropdown-option">
                                            <div class="option custom-filter-option" data-value="0">All Store</div>
                                        </div>
                                    </div>
                                    <select class="frm-custom-dropdown-origin" style="display: none;">
                                    </select>
                                </div>
                            </div>
                            <!-- generate report -->
                            <button type="button" class="btn btn-dark / margin-left-20 margin-top-20 generate-dst"><i class="fa fa-spinner fa-pulse hidden"></i> Generate Report</button>
                            <div class="clear"></div>
                        </div>
<!--                        <div class="display-inline-mid  padding-left-10 margin-left-10">-->
<!--                            <section class="container-fluid" section-style="content-panel">        -->
<!--                                <!-- generate by period -->
<!--                                <div class="divider padding-left-20">-->
<!--                                    <label>Generate by Period:</label><br />-->
<!--                                    -->
<!--                                    <div class="select small ">-->
<!--                                        <select>-->
<!--                                            <option value="Daily">Daily</option>-->
<!--                                            <option value="Weekly">Weekly</option>-->
<!--                                            <option value="Monthly">Monthly</option>-->
<!--                                        </select>-->
<!--                                    </div>-->
<!--                                    -->
<!--                                    <button type="button" class="btn / btn-dark / margin-left-20 generate-dst-period">Generate Report</button>-->
<!--                                </div>                                    -->
<!--                            </section>-->
<!--                        </div>-->
                        <div class="margin-top-20">
                            <hr/>
                        </div>
                    </div>            
                </div>

                <div class="contents dst-not-generated text-center margin-top-100">

                    <p class="font-50"><img src="<?php echo assets_url() ?>/images/report_1_.svg" alt="report image"></p>
                    <h4 class="gray-color margin-top-30 font-20">Please select date range <br>to generate report </h4>

                </div>

                <div class="row">
                    <div class="contents hidden report-pagination-table-wrapper">
                        <!-- Paganation -->
                        <div class="no-padding-all">
                            <div pagination-container="true"></div>
                        </div>
                        <!-- End of Paganation -->

                        <!-- Reports Table -->
                        <div class="tbl text-center margin-bottom-50">
                            <table class="viewable hidden">
                                <thead class="font-10">
                                    <tr>
                                        <th class="text-center min-width-20" rowspan="2">Date</th>
                                        <th class="text-center" colspan="4">20 Minutes 0 to 500 Php</th>
                                        <th class="text-center" colspan="4">30 Minutes 0 to 1100 Php</th>
                                        <th class="text-center" colspan="4">45 Minutes 0 to 3299 Php</th>
                                        <th class="text-center min-width-20" rowspan="2">Advance Orders</th>
                                        <th class="text-center  min-width-20" rowspan="2">Big Orders</th>
                                        <th class="text-center min-width-20" rowspan="2">Exempted</th>
                                        <th class="text-center" colspan="4">Total Hit Rates</th>
                                        <th class="text-center min-width-20" rowspan="4">Total Hit Rate Percentatge</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center min-width-20 bggray-darkred">Y</th>
                                        <th class="text-center min-width-20 bggray-brightred">N</th>
                                        <th class="text-center min-width-20 bggray-darkred">Total</th>
                                        <th class="text-center min-width-20 bggray-brightred">%</th>

                                        <th class="text-center min-width-20 bggray-darkred ">Y</th>
                                        <th class="text-center min-width-20 bggray-brightred">N</th>
                                        <th class="text-center min-width-20 bggray-darkred">Total</th>
                                        <th class="text-center min-width-20 bggray-brightred">%</th>

                                        <th class="text-center min-width-20 bggray-darkred">Y</th>
                                        <th class="text-center min-width-20 bggray-brightred">N</th>
                                        <th class="text-center min-width-20 bggray-darkred">Total</th>
                                        <th class="text-center min-width-20 bggray-brightred">%</th>

                                                                        
                                        <th class="text-center min-width-20 bggray-darkred">Y</th>
                                        <th class="text-center min-width-20 bggray-brightred">N</th>
                                        <th class="text-center min-width-20 bggray-darkred">Total</th>
                                        <th class="text-center min-width-20 bggray-brightred">%</th>
                                    </tr>
                                </thead>
                                <tbody class="font-14">
                                    <tr>
                                        <td >
                                            <div class="f-left">
                                                <p class="f-left font-10">May <br>2015</p>
                                                <p class="f-left font-20"><strong>01</strong></p>
                                                <div class="clear"></div>
                                                <p class="font-10 margin-left-5 margin-right-10">Friday</p>                                        
                                            </div>
                                        </td>
                                        <td>10</td>
                                        <td>15</td>
                                        <td>25</td>
                                        <td><strong>95.3</strong></td>
                                        <td>10</td>
                                        <td>13</td>
                                        <td>23</td>
                                        <td><strong>95.3</strong></td>
                                        <td>10</td>
                                        <td>0</td>
                                        <td>10</td>
                                        <td><strong>95.3</strong></td>
                                        <td>10</td>
                                        <td>13</td>
                                        <td>5</td>
                                        <td>10</td>
                                        <td>5</td>
                                        <td>15</td>
                                        <td><strong>95.3</strong></td>
                                        <td><span class="font-20">100.0%</span><br/><a href="dst-report-per-day-view-data.php">View Data</a></td>
                                    </tr>
                                </tbody>
                            </table>

                            <table class="hidden to-excel">
                                <thead>
                                    <tr>
                                        <th class="text-center min-width-20" rowspan="2">Date</th>
                                        <th class="text-center" colspan="4">20 Minutes 0 to 500 Php</th>
                                        <th class="text-center" colspan="4">30 Minutes 0 to 1100 Php</th>
                                        <th class="text-center" colspan="4">45 Minutes 0 to 3299 Php</th>
                                        <th class="text-center min-width-20" rowspan="2">Advance Orders</th>
                                        <th class="text-center  min-width-20" rowspan="2">Big Orders</th>
                                        <th class="text-center min-width-20" rowspan="2">Exempted</th>
                                        <th class="text-center" colspan="4">Total Hit Rates</th>
                                        <th class="text-center min-width-20" >Total Hit Rate Percentatge</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center min-width-20 bggray-darkred">Y</th>
                                        <th class="text-center min-width-20 bggray-brightred">N</th>
                                        <th class="text-center min-width-20 bggray-darkred">Total</th>
                                        <th class="text-center min-width-20 bggray-brightred">%</th>

                                        <th class="text-center min-width-20 bggray-darkred ">Y</th>
                                        <th class="text-center min-width-20 bggray-brightred">N</th>
                                        <th class="text-center min-width-20 bggray-darkred">Total</th>
                                        <th class="text-center min-width-20 bggray-brightred">%</th>

                                        <th class="text-center min-width-20 bggray-darkred">Y</th>
                                        <th class="text-center min-width-20 bggray-brightred">N</th>
                                        <th class="text-center min-width-20 bggray-darkred">Total</th>
                                        <th class="text-center min-width-20 bggray-brightred">%</th>

                                                                        
                                        <th class="text-center min-width-20 bggray-darkred">Y</th>
                                        <th class="text-center min-width-20 bggray-brightred">N</th>
                                        <th class="text-center min-width-20 bggray-darkred">Total</th>
                                        <th class="text-center min-width-20 bggray-brightred">%</th>
                                    </tr>
                                </thead>
                                <tbody report-body="content" ></tbody>
                            </table>
                        </div>
                        <!-- End of Reports -->
                    </div>
                </div>
            </div>


        </section>
    </div>

    <div class="row hidden" content="dst_per_day_view_data" id="dst_per_day_view_data">
        <section class="container-fluid" section-style="top-panel" id="dst_per_day_view_data_content">
            <!--<div class="row">
                <div class="contents padding-top-20">
                    <div class="f-left">
                        <label class="margin-bottom-5">search:</label><br>
                        <input class="search" type="text">
                    </div>
                    <button class="f-right btn btn-dark margin-top-20 margin-left-20">Search</button>
                    <div class="f-right margin-top-20">
                        <div class="select select-provinces">
                            <select>
                                <option value="All Provinces" selected>Province</option>
                            </select>
                        </div>
                    </div>
                    <div class="clear"></div>

                    <div class="f-left margin-top-20">
                        <div class="select select-filter-provinces">
                            <select>
                                <option value="Show All Provinces" selected>Show All Provinces</option>
                            </select>
                        </div>
                    </div>
                    <div class="f-left margin-left-10 margin-top-20">
                        <div class="select select-filter-transactions">
                            <select>
                                <option value="Show All Transaction" selected>Show All Transaction</option>
                                <option value="Call Orders" >Call Orders</option>
                                <option value="Web Orders" >Web Orders</option>
                                <option value="Sms Orders" >Sms Orders</option>
                                <option value="Nkag Orders" >Nkag Orders</option>
                            </select>
                        </div>
                    </div>
                    <div class="f-left margin-left-10 margin-top-20">
                        <div class="select select-filter-call-centers">
                            <select>
                                <option value="Show All Call Centers" selected>Show All Call Centers</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>-->

            <div class="row">
                <div class="contents">
                    <!-- Paganation -->
                    <div class="no-padding-all">
                         <div pagination-container="true"></div>
                    </div>
                    <!-- End of Paganation -->

                    <!-- Reports Table -->
                    <div class="tbl text-center margin-bottom-50">
                        <table class="viewable hidden template">
                            <thead class="font-10">
                                <tr>
                                    <th class="text-center no-width" colspan="4">20 Minutes 0 to 500 Php</th>
                                    <th class="text-center" colspan="4">30 Minutes 0 to 1100 Php</th>
                                    <th class="text-center" colspan="4">45 Minutes 0 to 3299 Php</th>
                                    <th class="text-center min-width-20" rowspan="2">Advance Orders</th>
                                    <th class="text-center  min-width-20" rowspan="2">Big Orders</th>
                                    <th class="text-center min-width-20" rowspan="2">Exempted</th>
                                    <th class="text-center" colspan="4">Total Hit Rates</th>
                                    <th class="text-center min-width-20" rowspan="4">Total Hitrate Percentatge</th>
                                </tr>
                                <tr>
                                    <th class="text-center min-width-20 bggray-darkred no-width">Y</th>
                                    <th class="text-center min-width-20 bggray-brightred">N</th>
                                    <th class="text-center min-width-20 bggray-darkred">Total</th>
                                    <th class="text-center min-width-20 bggray-brightred">%</th>

                                    <th class="text-center min-width-20 bggray-darkred ">Y</th>
                                    <th class="text-center min-width-20 bggray-brightred">N</th>
                                    <th class="text-center min-width-20 bggray-darkred">Total</th>
                                    <th class="text-center min-width-20 bggray-brightred">%</th>

                                    <th class="text-center min-width-20 bggray-darkred">Y</th>
                                    <th class="text-center min-width-20 bggray-brightred">N</th>
                                    <th class="text-center min-width-20 bggray-darkred">Total</th>
                                    <th class="text-center min-width-20 bggray-brightred">%</th>

                                                                    
                                    <th class="text-center min-width-20 bggray-darkred">Y</th>
                                    <th class="text-center min-width-20 bggray-brightred">N</th>
                                    <th class="text-center min-width-20 bggray-darkred">Total</th>
                                    <th class="text-center min-width-20 bggray-brightred">%</th>
                                </tr>
                            </thead>
                            <tbody class="font-14">
                            </tbody>
                        </table>

                        <table class="hidden to-excel">
                            <thead class="font-10">
                                <tr>
                                    <th class="text-center no-width" colspan="4">20 Minutes 0 to 500 Php</th>
                                    <th class="text-center" colspan="4">30 Minutes 0 to 1100 Php</th>
                                    <th class="text-center" colspan="4">45 Minutes 0 to 3299 Php</th>
                                    <th class="text-center min-width-20" rowspan="2">Advance Orders</th>
                                    <th class="text-center  min-width-20" rowspan="2">Big Orders</th>
                                    <th class="text-center min-width-20" rowspan="2">Exempted</th>
                                    <th class="text-center" colspan="4">Total Hit Rates</th>
                                    <th class="text-center min-width-20">Total Hitrate Percentatge</th>
                                </tr>
                                <tr>
                                    <th class="text-center min-width-20 bggray-darkred no-width">Y</th>
                                    <th class="text-center min-width-20 bggray-brightred">N</th>
                                    <th class="text-center min-width-20 bggray-darkred">Total</th>
                                    <th class="text-center min-width-20 bggray-brightred">%</th>

                                    <th class="text-center min-width-20 bggray-darkred ">Y</th>
                                    <th class="text-center min-width-20 bggray-brightred">N</th>
                                    <th class="text-center min-width-20 bggray-darkred">Total</th>
                                    <th class="text-center min-width-20 bggray-brightred">%</th>

                                    <th class="text-center min-width-20 bggray-darkred">Y</th>
                                    <th class="text-center min-width-20 bggray-brightred">N</th>
                                    <th class="text-center min-width-20 bggray-darkred">Total</th>
                                    <th class="text-center min-width-20 bggray-brightred">%</th>

                                                                    
                                    <th class="text-center min-width-20 bggray-darkred">Y</th>
                                    <th class="text-center min-width-20 bggray-brightred">N</th>
                                    <th class="text-center min-width-20 bggray-darkred">Total</th>
                                    <th class="text-center min-width-20 bggray-brightred">%</th>
                                </tr>
                            </thead>
                            <tbody class="font-14" report-body="content">
                            </tbody>
                        </table>
                    </div>
                    <!-- End of Reports -->
                </div>
            </div>
        </section>
    </div>

    <div class="row hidden category_report" content="category_report" id="sales_reports_by_category_content">
        <div class="content-container unboxed">
            <div class="tbl no-padding-all">

                <table id="category_sales_report_table">
                    <thead class="font-10">
                            <tr>
                                <th class="font-14" rowspan="2"><p class="text-center">Product Name</p></th>
                                <th class="text-center font-14" colspan="2">Food Sales</th>
                                <th class="text-center font-14" colspan="2">Total Sales</th>                                
                                <th class="text-center font-14" colspan="2">Quantity</th>                                
                            </tr>                         
                            <tr>
                                <th class="text-center bggray-darkred">Amount</th>
                                <th class="text-center bggray-brightred">Percentage</th>
                                <th class="text-center bggray-darkred">Amount</th>
                                <th class="text-center bggray-brightred">Percentage</th>
                                <th class="text-center bggray-darkred">Amount</th>
                                <th class="text-center bggray-brightred">Percentage</th>
                            </tr>
                    </thead>
                    <tbody class="category-sales-report-tbody font-14">
                        <!-- <tr>
                            <td>
                                <div class="margin-left-20">
                                    <div class="f-left">
                                        <p class="f-left font-10">May <br>2015</p>
                                        <p class="f-left font-16"><strong>01</strong></p>
                                        <div class="clear"></div>
                                        <p class="font-10 margin-left-10">Friday</p>
                                    </div>
                                    <div class="f-left margin-left-20 margin-right-20">
                                        <p class="font-20"><strong>-</strong></p>
                                    </div>
                                    <div class="f-left">
                                        <p class="f-left font-10">May <br>2015</p>
                                        <p class="f-left font-16"><strong>01</strong></p>
                                        <div class="clear"></div>
                                        <p class="font-10 margin-left-10 ">Friday</p>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </td>
                            <td><span class="product-name">Coca-Cola</span></td>
                            <td>15,487.75 PHP </td>
                            <td>15,487.75 PHP</td>
                            <td>584</td>
                            <td>84.2 %</td>
                            <td>75.3 %</td>
                        </tr> -->
                    </tbody>
                </table>

                <div id="category_sales_report_table_excel_container"></div>

                <table id="category_sales_report_table_template">
                    <tr class="template">
                        <td data-label-for="product-name">Coca-Cola</td>
                        <td data-label-for="food-sales-count" class="text-center">1219.00 PHP</td>
                        <td data-label-for="sales-percentage"class="text-center">0 %</td>
                        <td data-label-for="sales-count"class="text-center">1219.00 PHP</td>
                        <td data-label-for="sales-percentage"class="text-center">0 %</td>
                        <td data-label-for="quantity-count"class="text-center">584</td>
                        <td data-label-for="quantity-percentage"class="text-center">0 %</td>
                    </tr>
                </table>

            </div>
        </div>
    </div>

    <div class="row hidden" content="hourly_report">
        <section class="container-fluid" section-style="top-panel">
            <div class="contents hidden show_generate_report">
                <div class="row">
                    <div class="contents padding-top-20">
                        <!-- <div class="f-left">
                            <label class="margin-bottom-5">search:</label><br>
                            <input class="search width-800px" type="text" style="width:800px;">
                        </div>
                        <button class="f-right btn btn-dark margin-top-20 margin-left-20">Search</button>
                        
                        <div class="clear"></div> -->

                        <!-- <div class="f-left margin-top-20">
                            <div class="select hourly_province_selection margin-right-10">
                                <div class="frm-custom-dropdown">
                                    <div class="frm-custom-dropdown-txt"><input type="text" int-value="0" class="dd-txt" name="hourly_provinces" value="Show All Provinces"></div>
                                    <div class="frm-custom-icon"></div>
                                    <div class="frm-custom-dropdown-option">
                                        <div class="option province_select" int-value="0">Show All Provinces</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="f-left margin-left-10 margin-top-20">
                            <div class="select hourly_transaction_selection">
                                <div class="frm-custom-dropdown">
                                    <div class="frm-custom-dropdown-txt">
                                        <input type="text" class="dd-txt" name="hourly_transactions" value="Show All Transactions" autocomplete="off" readonly="" data-value="Show All Transactions">
                                    </div>
                                    <div class="frm-custom-icon"></div>
                                    <div class="frm-custom-dropdown-option option-visible" style="display: block;">
                                        <div class="option transaction_select" data-value="Show All Transactions">Show All Transactions</div>
                                        <div class="option transaction_select" data-value="Store Channel">Call Orders</div>
                                        <div class="option transaction_select" data-value="Web Orders">Web Orders</div>
                                        <div class="option transaction_select" data-value="Sms Orders">Sms Orders</div>
                                        <div class="option transaction_select" data-value="nkag">Nkag Orders</div>
                                    </div>
                                </div>
                                <select class="frm-custom-dropdown-origin" style="display: none;">
                                    <option value="Show All Transactions" selected="">Show All Transactions</option>
                                    <option value="Call Orders" data-value="Store Channel">Call Orders</option>
                                    <option value="Web Orders" data-value="Web Orders">Web Orders</option>
                                    <option value="Sms Orders" data-value="sms">Sms Orders</option>
                                    <option value="Nkag Orders" data-value="nkag">Nkag Orders</option>
                                </select>
                            </div>
                        </div>
                        <div class="f-left margin-left-10 margin-top-20">
                            <div class="select hourly_callcenter_selection">
                                <div class="frm-custom-dropdown font-0">
                                    <div class="frm-custom-dropdown-txt">
                                        <input type="text" class="dd-txt" name="hourly_callcenters" value="Show All Call Centers" autocomplete="off" int-value="0">
                                    </div>
                                    <div class="frm-custom-icon"></div>
                                    <div class="frm-custom-dropdown-option" style="display: none;">
                                        <div class="option callcenter_select" int-value="0">Show All Call Centers</div>
                                    </div>
                                </div>
                                <select class="frm-custom-dropdown-origin" style="display: none;">
                                    <option value="Show All Call Centers" selected="">Show All Call Centers</option>
                                </select>
                            </div>
                        </div>
                        <div class="f-left margin-left-10 margin-top-20">
                            <div class="select hourly_store_selection">
                                <div class="frm-custom-dropdown font-0">
                                    <div class="frm-custom-dropdown-txt">
                                        <input type="text" class="dd-txt" name="hourly_stores" value="Show All Stores" autocomplete="off" int-value="0">
                                    </div>
                                    <div class="frm-custom-icon"></div>
                                    <div class="frm-custom-dropdown-option" style="display: none;">
                                        <div class="option store_select" int-value="0">Show All Stores</div>
                                    </div>
                                </div>
                                <select class="frm-custom-dropdown-origin" style="display: none;">
                                    <option value="Show All Stores" selected="">Show All Stores</option>
                                </select>
                            </div>
                        </div> -->
                    </div>
                </div>
                <div class="row">
                    <div class="contents">
                        <div class="tbl text-center margin-bottom-50">
                            <table class="viewable" id="hourly_sales_report_table">
                                <thead class="font-10">
                                    <tr>
                                        <th class="text-center font-14 width-100px">Date</th>
                                        <th class="text-center font-14 width-100px">Hour</th>
                                        <th class="text-center font-14 width-200px">Store Name</th>
                                        <th class="text-center font-14">Total Sales</th>
                                        <th class="text-center font-14">Food Sales</th>
                                        <th class="text-center font-14">Transaction Count</th>
                                        <th class="text-center font-14">Percentage</th>
                                        <th class="text-center font-14">Average Order</th>
                                    </tr>
                                </thead>
                                <tbody class="font-14 hourly_sales_report_tr_container">
                                    <tr class="template hourly_sales_report_tr notthis">
                                        <td class="date_range">                                    
                                            09-01-2015
                                            <br />to<br />
                                            09-30-2015                                    
                                        </td>
                                        <td class="hour_interval">                                    
                                            00-00-00
                                            <br />to<br />
                                            00-59-59                                    
                                        </td>
                                        <td class="store_name">JB NCR</td>
                                        <td class="total_sales">PHP 5000.00</td>
                                        <td class="food_sales">PHP 4000.00</td>
                                        <td class="transaction_count">05</td>
                                        <td class="percentage">41.23 %</td>
                                        <td class="average_order">PHP 709.76</td>
                                    </tr>
                                    <!--
                                     <tr>
                                        <td>                                    
                                            09-01-2015
                                            <br />to<br />
                                            09-30-2015                                    
                                        </td>
                                        <td>                                    
                                            00-00-00
                                            <br />to<br />
                                            00-59-59                                    
                                        </td>
                                        <td>JB NCR</td>
                                        <td>PHP 5000.00</td>
                                        <td>PHP 4000.00</td>
                                        <td>05</td>
                                        <td>41.23 %</td>
                                        <td>PHP 709.76</td>
                                    </tr>
                                     <tr>
                                        <td>                                    
                                            09-01-2015
                                            <br />to<br />
                                            09-30-2015                                    
                                        </td>
                                        <td>                                    
                                            00-00-00
                                            <br />to<br />
                                            00-59-59                                    
                                        </td>
                                        <td>JB NCR</td>
                                        <td>PHP 5000.00</td>
                                        <td>PHP 4000.00</td>
                                        <td>05</td>
                                        <td>41.23 %</td>
                                        <td>PHP 709.76</td>
                                    </tr>
                                     <tr>
                                        <td>                                    
                                            09-01-2015
                                            <br />to<br />
                                            09-30-2015                                    
                                        </td>
                                        <td>                                    
                                            00-00-00
                                            <br />to<br />
                                            00-59-59                                    
                                        </td>
                                        <td>JB NCR</td>
                                        <td>PHP 5000.00</td>
                                        <td>PHP 4000.00</td>
                                        <td>05</td>
                                        <td>41.23 %</td>
                                        <td>PHP 709.76</td>
                                    </tr>                 
                                     <tr>
                                        <td>                                    
                                            09-01-2015
                                            <br />to<br />
                                            09-30-2015                                    
                                        </td>
                                        <td>                                    
                                            00-00-00
                                            <br />to<br />
                                            00-59-59                                    
                                        </td>
                                        <td>JB NCR</td>
                                        <td>PHP 5000.00</td>
                                        <td>PHP 4000.00</td>
                                        <td>05</td>
                                        <td>41.23 %</td>
                                        <td>PHP 709.76</td>
                                    </tr>
                                     <tr>
                                        <td>                                    
                                            09-01-2015
                                            <br />to<br />
                                            09-30-2015                                    
                                        </td>
                                        <td>                                    
                                            00-00-00
                                            <br />to<br />
                                            00-59-59                                    
                                        </td>
                                        <td>JB NCR</td>
                                        <td>PHP 5000.00</td>
                                        <td>PHP 4000.00</td>
                                        <td>05</td>
                                        <td>41.23 %</td>
                                        <td>PHP 709.76</td>
                                    </tr>
                                     <tr>
                                        <td>                                    
                                            09-01-2015
                                            <br />to<br />
                                            09-30-2015                                    
                                        </td>
                                        <td>                                    
                                            00-00-00
                                            <br />to<br />
                                            00-59-59                                    
                                        </td>
                                        <td>JB NCR</td>
                                        <td>PHP 5000.00</td>
                                        <td>PHP 4000.00</td>
                                        <td>05</td>
                                        <td>41.23 %</td>
                                        <td>PHP 709.76</td>
                                    </tr>
                                     <tr>
                                        <td>                                    
                                            09-01-2015
                                            <br />to<br />
                                            09-30-2015                                    
                                        </td>
                                        <td>                                    
                                            00-00-00
                                            <br />to<br />
                                            00-59-59                                    
                                        </td>
                                        <td>JB NCR</td>
                                        <td>PHP 5000.00</td>
                                        <td>PHP 4000.00</td>
                                        <td>05</td>
                                        <td>41.23 %</td>
                                        <td>PHP 709.76</td>
                                    </tr>
                                    -->
                                </tbody>
                            </table>
                            <table class="viewable hidden" id="hourly_sales_report_table_hidden">
                                <thead class="font-10">
                                    <tr>
                                        <th class="text-center font-14 width-100px">Date</th>
                                        <th class="text-center font-14 width-100px">Hour</th>
                                        <th class="text-center font-14 width-200px">Store Name</th>
                                        <th class="text-center font-14">Total Sales</th>
                                        <th class="text-center font-14">Food Sales</th>
                                        <th class="text-center font-14">Transaction Count</th>
                                        <!-- <th class="text-center font-14">Percentage</th> -->
                                        <!-- <th class="text-center font-14">Average Order</th> -->
                                    </tr>
                                </thead>
                                <tbody class="font-14 hourly_sales_report_tr_container_hidden">
                                    <tr class="template hourly_sales_report_tr_hidden notthis">
                                        <td class="date_range">                                    
                                            09-01-2015
                                            <br />to<br />
                                            09-30-2015                                    
                                        </td>
                                        <td class="hour_interval">                                    
                                            00-00-00
                                            <br />to<br />
                                            00-59-59                                    
                                        </td>
                                        <td class="store_name">JB NCR</td>
                                        <td class="total_sales">PHP 5000.00</td>
                                        <td class="food_sales">PHP 4000.00</td>
                                        <td class="transaction_count">05</td>
                                        <!-- <td class="percentage">41.23 %</td> -->
                                        <!-- <td class="average_order">PHP 709.76</td> -->
                                    </tr>
                                </tbody>
                            </table>
                            <table class="viewable" id="hourly_sales_report_table_day_part">
                                <thead class="font-10">
                                    <tr>
                                        <th class="text-center font-14 width-100px" colspan="8">Day Part View</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center font-14 width-100px">Date</th>
                                        <th class="text-center font-14 width-100px">Hour</th>
                                        <th class="text-center font-14 width-200px">Store Name</th>
                                        <th class="text-center font-14">Total Sales</th>
                                        <th class="text-center font-14">Food Sales</th>
                                        <th class="text-center font-14">Transaction Count</th>
                                        <th class="text-center font-14">Percentage</th>
                                        <th class="text-center font-14">Average Order</th>
                                    </tr>
                                </thead>
                                <tbody class="font-14 hourly_sales_report_tr_container_day_part">
                                    <tr class="template hourly_sales_report_tr_day_part notthis">
                                        <td class="date_range">                                    
                                            09-01-2015
                                            <br />to<br />
                                            09-30-2015                                    
                                        </td>
                                        <td class="hour_interval">                                    
                                            00-00-00
                                            <br />to<br />
                                            00-59-59                                    
                                        </td>
                                        <td class="store_name">JB NCR</td>
                                        <td class="total_sales">PHP 5000.00</td>
                                        <td class="food_sales">PHP 4000.00</td>
                                        <td class="transaction_count">05</td>
                                        <td class="percentage">41.23 %</td>
                                        <td class="average_order">PHP 709.76</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contents text-center margin-top-100 no_generate_report">
                <p class="font-50"><img alt="report image" src="<?php echo base_url();?>/assets/images/report_1_.svg"></p>
                <h4 class="gray-color margin-top-30 font-20">Please select date range <br>to generate report </h4>
            </div>
        </section>
    </div>

    <div class="row hidden" content="audit_trails" id="audit_trails">
        <section class="container-fluid" section-style="top-panel" id="audit_trails">

            <div class="container-fluid">
                <div class="row">
                    <div class="contents margin-top-20">
                        <div class="display-inline-mid">

                            <div id="dst_date_from" class="f-left margin-right-10 ">
                                <label class="margin-bottom-5">Date From:</label><br>
                                <div class="date-picker" id="audit_trails_date_from">
                                    <input type="text" readonly>
                                    <span class="fa fa-calendar text-center red-color"></span>
                                </div>
                            </div>

                            <div id="dst_date_to" class="f-left">
                                <label class="margin-bottom-5">Date To:</label><br>
                                <div class="date-picker" id="audit_trails_date_to">
                                    <input type="text" readonly>
                                    <span class="fa fa-calendar text-center red-color"></span>
                                </div>
                            </div>

                            <div class="f-left select margin-left-20">
                                <label class="margin-bottom-5">Type:</label><br>
                                <div class="frm-custom-dropdown font-0">
                                    <div class="frm-custom-dropdown-txt">
                                        <input type="text" class="dd-txt" name="audit_trail_type" value="All Audit Trails" autocomplete="off">
                                    </div>
                                    <div class="frm-custom-icon"></div>
                                    <div class="frm-custom-dropdown-option option-visible" style="display: block;">
                                        <div class="option audit_trail_type" data-value="All Audit Trails" selected="">All Audit Trails</div>
                                        <div class="option audit_trail_type" data-value="User Management">User Management</div>
                                        <div class="option audit_trail_type" data-value="Store Management">Store Management</div>
                                        <div class="option audit_trail_type" data-value="Upsells And Promos">Upsells And Promos</div>
                                        <div class="option audit_trail_type" data-value="Store Operations">Store Operations</div>
                                        <div class="option audit_trail_type" data-value="Reports">Reports</div>
                                    </div>
                                </div>
                                <select class="frm-custom-dropdown-origin" style="display: none;">
                                    <option value="All Callcenters">All Callcenters</option>
                                </select>
                            </div>

                            <!-- generate report -->
                            <button type="button" class="btn btn-dark / margin-left-20 margin-top-20 generate_audit_trails"><i class="fa fa-spinner fa-pulse hidden"></i> Generate Audit Trails</button>
                            <div class="clear"></div>
                        </div>

                        <div class="margin-top-20">
                            <hr/>
                        </div>
                    </div>
                </div>

                <div class="contents dst-not-generated text-center margin-top-100">

                    <p class="font-50"><img src="<?php echo assets_url() ?>/images/report_1_.svg" alt="report image"></p>
                    <h4 class="gray-color margin-top-30 font-20">Please select date range <br>to generate audit trails </h4>

                </div>

                <div class="row">
                    <div class="contents hidden audit-trail-content">

                        <!-- Reports Table -->
                        <div class="tbl text-center margin-bottom-50">
                            <table class="viewable hidden" id="audit_trail_table">
                                <thead class="font-10">
                                    <tr>
                                        <th class="text-center" style="width: 10% !important">Date</th>
                                        <th class="text-center" style="width: 10% !important ">Username</th>
                                        <th class="text-center" style="width: 10% !important">Type</th>
                                        <th class="text-center" style="width: 10% !important">IP Address</th>
                                        <th class="text-center" style="width: 50% !important">Data</th>
                                        <th class="text-center" style="width: 10% !important">Action Note</th>
                                    </tr>
                                </thead>
                                <tbody class="font-14">
                                    <tr class="audit_trail_tr template">
                                        <td class="text-center" data-label="date_created">Date</td>
                                        <td class="text-center" data-label="username">Username</td>
                                        <td class="text-center" data-label="type">Type</td>
                                        <td class="text-center" data-label="ip_address">IP Address</td>
                                        <td class="text-center" data-label="value"></td>
                                        <td class="text-center" data-label="action_note">Action Note</td>

                                    </tr>
                                </tbody>
                            </table>

                            <table class="hidden to-excel">
                                <thead>
                                    <tr>
                                        <th class="text-center min-width-20">Date</th>
                                        <th class="text-center">Username</th>
                                        <th class="text-center">Type</th>
                                        <th class="text-center">IP Address</th>
                                        <th class="text-center">Data</th>
                                        <th class="text-center">Action Note</th>
                                    </tr>
                                </thead>
                                <tbody report-body="content" >

                                </tbody>
                            </table>
                        </div>
                        <!-- End of Reports -->
                    </div>
                </div>
            </div>


        </section>
    </div>
</section>
<!--CONTENT END-->


<!--Modals-->

<!-- modal confirm archive announcement -->
<div class="modal-container" modal-id="confirm-archive-announcement">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Archive announcement</h4>

            <div class="modal-close close-me"></div>
        </div>

        <div class="modal-content no-margin-all">

            <!-- lots of radio button -->
            <table>
                <tr>
                    <td>
                        <p class="red-color / margin-right-30  / margin-bottom-50">
                            <i class="fa fa-question-circle fa-5x"></i></p>
                    </td>
                    <td>
                        <p class="font-14 / margin-bottom-10 ">Are you sure you want to archive this announcement?</p>
                    </td>
                </tr>
            </table>
        </div>

        <!-- button -->
        <div class="margin-top-20">
            <button type="button" class="btn btn-dark f-right margin-right-20 close-me">Cancel</button>
            <button type="button" class="btn btn-dark f-right margin-right-20 archive_announcement_btn_confirm">Yes</button>
            <div class="clear"></div>
        </div>
    </div>
</div>
<!-- modal confirm archive promo -->
<div class="modal-container" modal-id="confirm-archive-promo">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Archive Promo</h4>

            <div class="modal-close close-me"></div>
        </div>

        <div class="modal-content no-margin-all">

            <!-- lots of radio button -->
            <table>
                <tr>
                    <td>
                        <p class="red-color / margin-right-30  / margin-bottom-50">
                            <i class="fa fa-question-circle fa-5x"></i></p>
                    </td>
                    <td>
                        <p class="font-14 / margin-bottom-10 ">Are you sure you want to archive this promo?</p>
                    </td>
                </tr>
            </table>
        </div>

        <!-- button -->
        <div class="margin-top-20">
            <button type="button" class="btn btn-dark f-right margin-right-20 close-me">Cancel</button>
            <button type="button" class="btn btn-dark f-right margin-right-20 archive_promo_btn_confirm">Yes</button>
            <div class="clear"></div>
        </div>
    </div>
</div>

<!-- modal confirm archive upsell -->
<div class="modal-container" modal-id="confirm-archive-upsell">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Archive Upsell</h4>

            <div class="modal-close close-me"></div>
        </div>

        <div class="modal-content no-margin-all">

            <!-- lots of radio button -->
            <table>
                <tr>
                    <td>
                        <p class="red-color / margin-right-30  / margin-bottom-50">
                            <i class="fa fa-question-circle fa-5x"></i></p>
                    </td>
                    <td>
                        <p class="font-14 / margin-bottom-10 ">Are you sure you want to archive this upsell?</p>
                    </td>
                </tr>
            </table>
        </div>

        <!-- button -->
        <div class="margin-top-20">
            <button type="button" class="btn btn-dark f-right margin-right-20 close-me">Cancel</button>
            <button type="button" class="btn btn-dark f-right margin-right-20 archive_upsell_btn_confirm">Yes</button>
            <div class="clear"></div>
        </div>
    </div>
</div>

<!-- modal confirm remove user -->
<div class="modal-container" modal-id="confirm-remove-user">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Remove User</h4>

            <div class="modal-close close-me"></div>
        </div>

        <div class="modal-content no-margin-all">

            <!-- lots of radio button -->
            <table>
                <tr>
                    <td>
                        <p class="red-color / margin-right-30  / margin-bottom-50">
                            <i class="fa fa-question-circle fa-5x"></i></p>
                    </td>
                    <td>
                        <p class="font-14 / margin-bottom-10 ">Are you sure you want to remove this user?</p>
                    </td>
                </tr>
            </table>
        </div>

        <!-- button -->
        <div class="margin-top-20">
            <button type="button" class="btn btn-dark f-right margin-right-20 close-me">Cancel</button>
            <button type="button" class="btn btn-dark f-right margin-right-20 remove_user_btn_confirm close-me">Yes</button>
            <div class="clear"></div>
        </div>
    </div>
</div>

<!-- modal confirm remove store user -->
<div class="modal-container" modal-id="confirm-remove-store-user">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Remove User</h4>

            <div class="modal-close close-me"></div>
        </div>

        <div class="modal-content no-margin-all">

            <!-- lots of radio button -->
            <table>
                <tr>
                    <td>
                        <p class="red-color / margin-right-30  / margin-bottom-50">
                            <i class="fa fa-question-circle fa-5x"></i></p>
                    </td>
                    <td>
                        <p class="font-14 / margin-bottom-10 ">Are you sure you want to remove this user?</p>
                    </td>
                </tr>
            </table>
        </div>

        <!-- button -->
        <div class="margin-top-20">
            <button type="button" class="btn btn-dark f-right margin-right-20 close-me">Cancel</button>
            <button type="button" class="btn btn-dark f-right margin-right-20 remove_user_btn_confirm close-me">Yes</button>
            <div class="clear"></div>
        </div>
    </div>
</div>

<!-- modal confirm remove blacklisted user -->
<div class="modal-container" modal-id="confirm-remove-blacklisted-user">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Remove User</h4>

            <div class="modal-close close-me"></div>
        </div>

        <div class="modal-content no-margin-all">

            <!-- lots of radio button -->
            <table>
                <tr>
                    <td>
                        <p class="red-color / margin-right-30  / margin-bottom-50">
                            <i class="fa fa-question-circle fa-5x"></i></p>
                    </td>
                    <td>
                        <p class="font-14 / margin-bottom-10 ">Are you sure you want to remove this user?</p>
                    </td>
                </tr>
            </table>
        </div>

        <!-- button -->
        <div class="margin-top-20">
            <button type="button" class="btn btn-dark f-right margin-right-20 close-me">Cancel</button>
            <button type="button" class="btn btn-dark f-right margin-right-20 remove_user_btn_confirm close-me">Yes</button>
            <div class="clear"></div>
        </div>
    </div>
</div>

<!-- modal confirm remove blacklisted store user -->
<div class="modal-container" modal-id="confirm-remove-blacklisted-store-user">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Remove User</h4>

            <div class="modal-close close-me"></div>
        </div>

        <div class="modal-content no-margin-all">

            <!-- lots of radio button -->
            <table>
                <tr>
                    <td>
                        <p class="red-color / margin-right-30  / margin-bottom-50">
                            <i class="fa fa-question-circle fa-5x"></i></p>
                    </td>
                    <td>
                        <p class="font-14 / margin-bottom-10 ">Are you sure you want to remove this user?</p>
                    </td>
                </tr>
            </table>
        </div>

        <!-- button -->
        <div class="margin-top-20">
            <button type="button" class="btn btn-dark f-right margin-right-20 close-me">Cancel</button>
            <button type="button" class="btn btn-dark f-right margin-right-20 remove_user_btn_confirm close-me">Yes</button>
            <div class="clear"></div>
        </div>
    </div>
</div>

<!--order summary modal-->
<div class="modal-container" modal-id="manual-order-summary">
    <div class="modal-body">

        <div class="modal-head ">
            <h4 class="text-left">Order Summary</h4>

            <div class="modal-close close-me"></div>
        </div>

        <!-- content -->
        <div class="modal-content">
            <!-- error message -->
            <div class="modal-error-msg manual-order-error-msg error-msg margin-bottom-10" style="font-size: 12px">
                <i class="fa fa-exclamation-triangle margin-right-10"></i>Please fill up all requred fields.
            </div>

            <!-- left content -->
            <div class="data-container display-inline-mid width-50per customer-information-container">


                <!-- contact information -->

            </div>


            <!-- right content -->
            <div class="data-container display-inline-mid width-50per margin-left-15">
                <label>Order:</label>
                <!-- item order -->
                <div class="small-curved-border cart-container">

                    <label class="margin-top-20">Change for: <span class="red-color">*</span></label>

                    <!-- <div class="price full"> -->
                    <!-- <input type="text" name="change_for" class="input_numeric"/> -->
                    <div class="select width-100per change_for">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" class="dd-txt" name="change_for" class="input_numeric" value="500"></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option" style="display: none;">
                                <div class="option" data-value="500">500</div>
                                <div class="option" data-value="1000">1000</div>
                            </div>
                        </div>
                        <select id="change_for" name="change_for" class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="500">500</option>
                            <option value="1000">1000</option>
                        </select>
                    </div>
                    <!-- </div> -->
                </div>
            </div>
        </div>

        <!-- button -->
        <div class="f-right margin-right-30 margin-bottom-10">
            <button type="button" class="btn btn-dark  modal-trigger confirm_order">Confirm Order</button>
            <button type="button" class="btn btn-dark close-me margin-left-15">Cancel</button>
        </div>
        <div class="clear"></div>

    </div>
</div>

<!--customer match modal-->
<div class="modal-container" modal-id="information-match">

    <div class="modal-body small">
        <div class="modal-head ">
            <p>Information Match</p>

            <div class="modal-close close-me"></div>
        </div>

        <div class="modal-content">
            <div class="col-sm-2">
                <img src="<?php echo base_url() ?>/assets/images/ui/warning.svg" alt="warning logo" class="img-responsive"/>
            </div>
            <div class="col-sm-10 ">
                <p>An existing customer have the same information. <br/>Are you sure you want proceed</p>
            </div>
            <div class="clear"></div>

            <div class="row bggray-white margin-top-10  padding-bottom-20 no-padding">
                <p class="font-18 no-margin-bottom margin-left-20 padding-top-20"><strong>Samuel O. Hernandez</strong>
                </p>

                <p class="margin-left-20"><strong>DISCOUNTS:</strong></p>

                <div class="bggray-dark padding-top-10 padding-left-10 padding-bottom-20 margin-left-20 margin-right-20">
                    <div class="f-left font-12 margin-right-20"><strong>Senior <br/>Citizen</strong></div>
                    <div class="f-left font-12">
                        <strong>Samuel O. Hernandez<br/><span class="red-color">OSCA Number: </span>0014-7845</strong>
                    </div>
                    <div class="clear"></div>
                </div>

            </div>

        </div>

        <div class="f-right margin-right-30 margin-bottom-10">
            <button type="button" class="btn btn-dark margin-right-10">Confirm &amp; Proceed to Order</button>
            <button type="button" class="btn btn-dark">Cancel</button>
        </div>
        <div class="clear"></div>
    </div>
</div>

<!--additional address modal-->
<div class="modal-container" modal-id="deliver-to-different">
    <div class="modal-body small">
        <div class="modal-head ">
            <p>Deliver to a Different Address</p>

            <div class="modal-close close-me"></div>
        </div>

        <div class="modal-content">

            <!-- error message -->
            <div class="modal-error-msg address-error-msg error-msg margin-bottom-10" style="font-size: 12px">
                <i class="fa fa-exclamation-triangle margin-right-10"></i>Please fill up all requred fields.
            </div>

            <div class="address-success-msg success-msg margin-bottom-30">
                <i class="fa fa-exclamation-triangle"></i>
            </div>

            <!-- address -->
            <form id="add_address">
                <input type="hidden" name="customer_id"/>
                <input type="hidden" name="address_id"/>
                <table class="margin-bottom-15 margin-left-15">
                    <tr>
                        <td><label>Address Type: <!-- <span class="red-color">*</span> --></label></td>
                        <td><label>Address Label: <!-- <span class="red-color">*</span> --></label></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="select margin-right-50">
                                <select name="address_type" labelinput="Address Type">
                                    <option value="Home">Home</option>
                                    <option value="Work">Work</option>
                                    <option value="Business">Business</option>
                                </select>
                            </div>
                        </td>
                        <td>
                            <input type="text" name="address_label" class="normal" labelinput="Address Label">
                        </td>
                    </tr>
                </table>
                <hr/>
                <!-- province -->
                <table class="margin-top-15 margin-left-15">
                    <tr>
                        <td><label>Province: <span class="red-color">*</span></label></td>
                        <td><label>City: <span class="red-color">*</span></label></td>
                    </tr>
                    <tr>
                        <td>
                            <div id="modal_select_province">
                                <div class="select margin-right-50">
                                    <select name="province" labelinput="Province" datavalid="required">
                                        <option value="">Select Province</option>
                                    </select>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div id="modal_select_city">
                                <div class="select">
                                    <select name="city" labelinput="City" datavalid="required">
                                        <option value="">Select City</option>
                                    </select>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>

                <div class="margin-left-15	">
                    <label class="margin-top-10">Landmark:</label>
                    <input type="text" class="xlarge" name="landmark">
                </div>

                <!-- 2 column table -->
                <table class="margin-top-10 margin-left-15">
                    <tr>
                        <td><label>House Number: </label></td>
                        <td><label>Building: </label></td>
                    </tr>
                    <tr>
                        <td><input type="text" name="house_number" class="normal margin-right-30"></td>
                        <td>
                            <input type="text" id="modal_search_building" name="building" class="input_modal_autocomplete normal">

                            <div class="result-list"></div>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="margin-top-10">Unit: </label></td>
                        <td><label>Floor: </label></td>
                    </tr>
                    <tr>
                        <td><input type="text" name="unit" class="normal"></td>
                        <td><input type="text" name="floor" class="normal"></td>
                    </tr>
                </table>

                <!-- single column table -->
                <table class="margin-left-15">
                    <tr>
                        <td><label class="margin-top-10">Street:</label></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="modal_search_street" class="input_modal_autocomplete xlarge" name="street">

                            <div class="result-list"></div>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="margin-top-10">Secondary Street:</label></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="modal_search_second_street" class="input_modal_autocomplete xlarge" name="second_street">

                            <div class="result-list"></div>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="margin-top-10">Subdivision:</label></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="modal_search_subdivision" class="input_modal_autocomplete xlarge" name="subdivision">

                            <div class="result-list"></div>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="margin-top-10">Barangay:</label></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="modal_search_barangay" class="input_modal_autocomplete xlarge" name="barangay">

                            <div class="result-list"></div>
                        </td>
                    </tr>
                </table>
                <div class="form-group margin-top-10 margin-left-15">
                    <input class="chck-box" id="card-number" type="checkbox" name="is_default">
                    <label class="chck-lbl" for="card-number">Set this address as the default for this customer</label>
                </div>
            </form>
        </div>

        <div class="f-right margin-right-30 margin-bottom-10">
            <button type="button" class="btn btn-dark margin-right-10 add_address_button">
                <i class="fa fa-spinner fa-pulse hidden"></i> Confirm
            </button>
            <button type="button" class="btn btn-dark close-me">Cancel</button>
        </div>
        <div class="clear"></div>
    </div>
</div>

<!-- order complete modal-->
<div class="modal-container" modal-id="manual-order-complete">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left order_type">Manual Order Complete</h4>

            <div class="modal-close close-me"></div>
        </div>

        <!-- content -->
        <div class="modal-content">
            <table>
                <tbody>
                    <tr>
                        <td><i class="fa fa-check-circle green-color font-50 margin-right-10 margin-bottom-20 "></i>
                        </td>
                        <td><p class="font-14">
                                <strong class="customer-full-name">Mark Anthony D. Dulay's</strong> Order has been successfully processed!
                            </p>
                            <p class="font-14">
                                <strong>Order ID : </strong><strong class="customer-order-id"></strong>
                            </p></td>
                    </tr>
                </tbody>
            </table>

            <hr>
            <div class="text-left">
                <label class="margin-top-10">How do you feel about this customer?</label>
                <br>

                <div class="select xlarge">
                    <div class="frm-custom-dropdown">
                        <div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt"></div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option">
                            <div class="option" data-value="easy-to-handle">This Customer is easy to handle</div>
                            <div class="option" data-value="difficult-to-handle">This Customer is difficult to handle</div>
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                        <option value="easy-to-handle">This Customer is easy to handle</option>
                        <option value="difficult-to-handle">This Customer is difficult to handle</option>
                    </select>
                </div>
            </div>
        </div>
        <!-- button -->
        <div class="f-right margin-right-20 margin-bottom-10">
            <button type="button" class="btn btn-dark margin-right-10 close-me modal-close confirm_behavior_modal">Confirm</button>
            <button type="button" class="btn btn-dark close-me modal-close back_to_customer_search confirm_behavior_modal">Back to Customer Search</button>
        </div>
        <div class="clear"></div>
    </div>
</div>
<!-- end order complete modal-->

<!--reroute order-->
<div class="modal-container" modal-id="reroute-order">

    <!-- modal body -->
    <div class="modal-body small">

        <!-- modal head -->
        <div class="modal-head ">
            <h4 class="text-left">Re-route Order</h4>

            <div class="modal-close close-me"></div>
        </div>

        <!--         <div class="bggray-light">
                    <div class="text-left margin-left-20 margin-right-20">
                        <label class="margin-top-10">Re-route to nearest available store: </label>

                        <div class="bggray-dark padding-all-20 form-group">
                            <div class="f-left">
                                <p class="font-12 "><strong>MM Ortigas Roosevelt | JB0444</strong></p>
                                <p class="font-12 "><strong><span class="red-color">Delivery Time:</span></strong> 20 Minutes
                                </p>
                            </div>
                            <div class="f-right margin-top-10">
                                <input id="option1" type="radio" class="radio-box" name="option">
                                <label class="radio-lbl " for="option1"></label>
                            </div>
                            <div class="clear"></div>
                        </div>

                        <div class="bggray-dark padding-all-20 margin-top-10 margin-bottom-10 form-group">
                            <div class="f-left">
                                <p class="font-12 "><strong>MM Pasig Kapitolyo | JB1124</strong></p>
                                <p class="font-12 "><strong><span class="red-color">Delivery Time:</span></strong> 20 Minutes
                                </p>
                            </div>
                            <div class="f-right margin-top-10">
                                <input id="option2" type="radio" class="radio-box" name="option">
                                <label class="radio-lbl " for="option2"></label>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>


                    <div>
                        <div class="contents line">
                            <label class=" font-14 padding-all-10 bggray-light">OR</label>
                        </div>
                    </div>
                </div> -->


        <!-- modal content -->
        <div class="modal-content no-margin-all">
            <div class="bggray-dark padding-bottom-20">
                <label class="margin-top-10 margin-left-10">Search Store:</label>
                <br>
                <input type="text" class="xlarge margin-left-10" name="search_store" style="margin-left: 5px;"/>

                <div class="result-list route-store hidden">

                </div>

                <input type="hidden" name="reoute_store_id"/>


                <div class="margin-top-10 margin-left-10 margin-right-20">
                    <div class="div-inside" style="margin-left:6px">
                        <table class="table-inside table_store_container">
                            <thead>
                                <tr>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody class="content">
                                <tr>
                                    <td><p class="font-12">MM Shopwise Sucat | JB0749</p></td>
                                </tr>
                                <tr>
                                    <td><p class="font-12">MM Alabang Madrigal | JB0749</p></td>
                                </tr>
                                <tr>
                                    <td><p class="font-12">MM New MCU | JB0237</p></td>
                                </tr>
                                <tr>
                                    <td><p class="font-12 light-red-color">MM Muntinlupa Bayan | JB0749</p></td>
                                </tr>
                                <tr>
                                    <td class="selected">
                                        <p class="font-12  f-left">MM SM Ayala | JB0749</p>
                                        <i class="fa fa-check font-18 margin-right-10 margin-top-5 f-right green-color"></i>

                                        <div class="clear"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><p class="font-12 light-red-color">MM Ortigas Roosevelt | JB0749</p></td>
                                </tr>
                                <tr>
                                    <td><p class="font-12">MM Libertad Commercial | JB0749</p></td>
                                </tr>
                                <tr>
                                    <td><p class="font-12">MM Libertad Commercial | JB0749</p></td>
                                </tr>
                                <tr>
                                    <td><p class="font-12">MM Libertad Commercial | JB0749</p></td>
                                </tr>
                                <tr>
                                    <td><p class="font-12">MM Libertad Commercial | JB0749</p></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <hr>

        <div class="bggray-light">
            <label class="f-left margin-left-20 margin-top-20">Re-route schedule:</label>

            <div class="clear"></div>

            <div class="form-group f-left margin-left-20 margin-top-10">
                <input id="reroute1" type="radio" class="radio-box" name="reroute">
                <label class="radio-lbl margin-right-10 " for="reroute1">Scheduled Re-routing</label>
                <input id="reroute2" type="radio" class="radio-box" name="reroute" checked>
                <label class="radio-lbl margin-right-10" for="reroute2">One-Time Re-routing</label>
            </div>

            <div class="clear"></div>



            <section class="route-schedule" style="display:none;">
                <label class="f-left margin-left-20">Schedule Expiry:</label>

                <div class="clear"></div>
                <div class="date-picker f-left margin-left-20 margin-bottom-10">
                    <input type="text">
                    <span class="fa fa-calendar text-center red-color"></span>
                </div>

                <div class="date-pickertime f-left margin-left-20">
                    <input type="text">
                    <span class="fa fa-clock-o text-center red-color"></span>
                </div>
            </section>


            <div class="clear"></div>
        </div>

        <div class="f-right margin-right-40 margin-bottom-10 margin-top-20">
            <button type="button" class="btn btn-dark margin-right-10 confirm_reroute" disabled>Send Order</button>
            <button type="button" class="btn btn-dark close-me">Cancel</button>
        </div>
        <div class="clear"></div>

    </div>
</div>

<!-- modal  MANUAL RELAY  -->
<div class="modal-container " modal-id="manual-relay">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Manually Relay</h4>

            <div class="modal-close close-me"></div>
        </div>

        <div class="modal-content no-margin-all">

            <img src="<?php echo assets_url() ?>/images/warning.svg" alt="warning message" class="width-20per f-left margin-bottom-20">
            <p class="font-14  f-left margin-top-20 margin-left-10	">Are you sure you want to manually relay
                <br/><strong>
                    <info class="customer_name">Jonathan R. Ornido's</info>
                </strong> order?
            </p>
            <div class="clear"></div>
        </div>

        <div class="margin-top-20">
            <button type="button" class="btn btn-dark f-right margin-right-20 close-me">Cancel</button>
            <button type="button" class="btn btn-dark f-right margin-right-20 confirm-relay"><i class="fa fa-spinner fa-pulse hidden"></i>Confirm</button>
            <div class="clear"></div>
        </div>

    </div>
</div>

<div class="modal-container" modal-id="archive-store">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left order_type">Archive <info class="entity">Store</info></h4>

            <div class="modal-close close-me"></div>
        </div>

        <!-- content -->
        <div class="modal-content">
            <table>
                <tbody>
                    <tr>
                        <td><i class="fa fa-close red-color font-50 margin-right-10 margin-bottom-20 " style="
    margin-top: 5px;
"></i>
                        </td>
                        <td style="
    vertical-align: middle;
">
                            <p class="font-14" style="
    margin-bottom: 10px;
">
                                <strong class="customer-full-name">Are you sure you want to archive this <info class="entity">Store</info></strong></p>
                        </td>
                    </tr>
                </tbody>
            </table>



        </div>
        <!-- button -->
        <div class="f-right margin-right-20 margin-bottom-10">
            <button type="button" class="btn btn-dark margin-right-10 close-me modal-close confirm-archive">Confirm</button>
            <button type="button" class="btn btn-dark margin-right-10 close-me modal-close">Cancel</button>

        </div>
        <div class="clear"></div>
    </div>
</div>

<div class="modal-container" modal-id="archive">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left order_type">Archive <info class="entity">Agent</info></h4>

            <div class="modal-close close-me"></div>
        </div>

        <!-- content -->
        <div class="modal-content">
            <table>
                <tbody>
                    <tr>
                        <td><i class="fa fa-close red-color font-50 margin-right-10 margin-bottom-20 " style="
    margin-top: 5px;
"></i>
                        </td>
                        <td style="
    vertical-align: middle;
">
                            <p class="font-14" style="
    margin-bottom: 10px;
">
                                <strong class="customer-full-name">Are you sure you want to archive this <info class="entity">Agent</info></strong></p>
                        </td>
                    </tr>
                </tbody>
            </table>



        </div>
        <!-- button -->
        <div class="f-right margin-right-20 margin-bottom-10">
            <button type="button" class="btn btn-dark margin-right-10 close-me modal-close confirm-archive">Confirm</button>
            <button type="button" class="btn btn-dark margin-right-10 close-me modal-close">Cancel</button>

        </div>
        <div class="clear"></div>
    </div>
</div>

<!-- update address-->
<div class="modal-container" modal-id="update-address">

    <!-- modal body -->
    <div class="modal-body small">

        <!-- modal head -->
        <div class="modal-head ">
            <h4 class="text-left">Deliver to Different Address</h4>

            <div class="modal-close close-me"></div>
        </div>
        <form id="coordinator_update_address">
            <div class="bggray-light">
                <div class="form-group f-left margin-left-20 margin-top-20">
                    <input id="choose" type="radio" class="radio-box" name="addressbook">
                    <label class="radio-lbl margin-right-10 " for="choose">Choose from Address Book</label>
                    <input id="enter" type="radio" class="radio-box" name="addressbook">
                    <label class="radio-lbl margin-right-10" for="enter">Enter New Delivery Address</label>
                </div>
                <div class="clear"></div>

                <hr>
            </div>
            <div id="city_select"><input type="hidden" name="city" value="169"></div>
            <div class="hidden_fields_for_rta">
                <input type="hidden" name="building" value="zxavier Building"/>

                <input type="hidden" name="province" value="1"/>
                <input type="hidden" name="street" value=""/>
                <input type="hidden" name="barangay" value=""/>
                <input type="hidden" name="subdivision"/>
            </div>

            <div class="bggray-light text-left">

                <!-- delivery address -->
                <container class="customer_address_block">

                </container>
                <!--<div class="margin-left-20 margin-right-20">
                <label>Delivery Address:</label>

                <div class="bggray-dark padding-all-5 font-14 small-curved-border margin-bottom-20">
                    <div class="display-inline-mid margin-right-10 padding-left-20">
                        <img src="<?php /*echo assets_url() */ ?>/images/work-icon.png" alt="work icon">
                        <p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
                    </div>
                    <div class="display-inline-mid margin-left-10  padding-left-10">
                        <p class="no-margin-all"><strong>Cr8v Web Solutions, Inc.</strong></p>
                        <p class="no-margin-all">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR</p>
                        <p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
                    </div>
                    <div class="display-inline-mid text-center margin-left-30">
                        <a class="red-color" href="#">Change <br>Address <br>
                            <img src="<?php /*echo assets_url() */ ?>/images/ui/select-arrow.png">
                        </a>
                    </div>
                </div>
            </div>-->

                <hr>
            </div>


            <!-- modal content -->
            <div class="modal-content no-margin-all">

                <!-- error message -->
                <div class="modal-error-msg update-address-error-msg error-msg margin-bottom-10" style="font-size: 12px">
                    <i class="fa fa-exclamation-triangle margin-right-10"></i>Please fill up all requred fields.
                </div>

                <!-- address -->
                <table class="margin-bottom-15 margin-left-15">
                    <tbody>
                        <tr>
                            <td><label>Address Type: <!-- <span class="red-color">*</span> --></label></td>
                            <td><label>Address Label: <!-- <span class="red-color">*</span> --></label></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="select margin-right-50">
                                    <div class="frm-custom-dropdown">
                                        <div class="frm-custom-dropdown-txt">
                                            <input type="text" class="dd-txt" name="address_type" labelinput="Address Type">
                                        </div>
                                        <div class="frm-custom-icon"></div>
                                        <div class="frm-custom-dropdown-option">
                                            <div class="option" data-value="Home">Home</div>
                                        </div>
                                    </div>
                                    <select class="frm-custom-dropdown-origin" style="display: none;">
                                        <option value="Home">Home</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <input type="text" class="normal" name="address_label" labelinput="Address Label">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <!-- province -->
                <table class="margin-top-15 margin-left-15">
                    <tbody>
                        <tr>
                            <td><label>Province: <span class="red-color">*</span></label></td>
                            <td><label>City: <span class="red-color">*</span></label></td>
                        </tr>
                        <tr>
                            <td>
                                <div id="coordinator_modal_select_province">
                                    <div class="select margin-right-50">
                                        <div class="frm-custom-dropdown">
                                            <div class="frm-custom-dropdown-txt">
                                                <input type="text" class="dd-txt" name="province" labelinput="Province" datavalid="required">
                                            </div>
                                            <div class="frm-custom-icon"></div>
                                            <div class="frm-custom-dropdown-option">
                                            </div>
                                        </div>
                                        <select class="frm-custom-dropdown-origin" style="display: none;">
                                            <option value="">Select Province</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div id="coordinator_modal_select_city">
                                    <div class="select">
                                        <div class="frm-custom-dropdown">
                                            <div class="frm-custom-dropdown-txt">
                                                <input type="text" class="dd-txt" name="city" labelinput="City" datavalid="required"></div>
                                            <div class="frm-custom-icon"></div>
                                            <div class="frm-custom-dropdown-option">
                                            </div>
                                        </div>
                                    </div>
                                    <select class="frm-custom-dropdown-origin" style="display: none;">
                                        <option value="">Select City</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="margin-left-15	">
                    <label class="margin-top-10">Landmark:</label>
                    <input type="text" class="xlarge" name="landmark">
                </div>

                <!-- 2 column table -->
                <table class="margin-top-10 margin-left-15">
                    <tbody>
                        <tr>
                            <td><label>House Number: </label></td>
                            <td><label>Building: </label></td>
                        </tr>
                        <tr>
                            <td><input type="text" class="normal margin-right-30" name="house_number"></td>
                            <td>
                                <input type="text" class="normal input_modal_autocomplete" id="coordinator_modal_search_building" name="building"/>

                                <div class="result-list"></div>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="margin-top-10">Unit: </label></td>
                            <td><label>Floor: </label></td>
                        </tr>
                        <tr>
                            <td><input type="text" class="normal" name="unit"></td>
                            <td><input type="text" class="normal" name="floor"></td>
                        </tr>
                    </tbody>
                </table>

                <!-- single column table -->
                <table class="margin-left-15">
                    <tbody>
                        <tr>
                            <td><label class="margin-top-10">Street:</label></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" class="xlarge input_modal_autocomplete" id="coordinator_modal_search_street" name="street">

                                <div class="result-list"></div>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="margin-top-10">Secondary Street:</label></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" class="xlarge input_modal_autocomplete" id="coordinator_modal_search_second_street" name="second_street">

                                <div class="result-list"></div>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="margin-top-10">Subdivision:</label></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" class="xlarge input_modal_autocomplete" id="coordinator_modal_search_subdivision" name="subdivision">

                                <div class="result-list"></div>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="margin-top-10">Barangay:</label></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" class="normal input_modal_autocomplete" id="coordinator_modal_search_barangay" name="barangay">

                                <div class="result-list"></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="bggray-light padding-bottom-20 rta-address-info-container">
                <hr>
                <div class="text-left margin-left-40 margin-right-40 margin-top-15">
                    <label>Nearest Store:</label>
                    <br>

                    <div class="bggray-light padding-all-10 font-14 text-center small-curved-border find-rta-container">
                        <a class="btn btn-light find_retail_search"><i class="fa fa-spinner fa-pulse hidden"></i> Find Retail Trade Area</a>

                        <a class="btn btn-light margin-left-20 show_map">Show Map</a>
                    </div>
                    <!--withput nearest store end-->

                    <!--with nearest store -->
                    <div class="bggray-dark found-retail">
                        <div>
                            <p class="font-14 f-left margin-top-20 margin-left-15">
                                <strong>
                                    <span class="retail-store-name">MM Ortigas Roosevelt | MI0444</span>
                                </strong>
                            </p>
                            <a class="font-14 f-right margin-top-20 margin-right-15 show_map">
                                <strong>
                                    <i class="fa fa-map-marker font-16 margin-right-5 "></i>Show Map
                                </strong>
                            </a>

                            <div class="clear"></div>
                        </div>
                        <div class="rta-min-container">
                            <p class="font-14 f-left green-color margin-left-15">
                                <strong>
								<span class="retail-store-time-label">Delivery Time:<span>
                                </strong>
                            </p>
                            <p class="font-14 f-right margin-right-15 margin-bottom-20">
                                <strong>
                                    <span class="retail-store-time">N/A</span>
                                </strong>
                            </p>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <div class="notify-msg margin-top-20 hidden">
                        <strong>Store Announcement!</strong> Delivery will be delayed for 10 minutes due to bad weather. Thank you.
                    </div>


                </div>
            </div>


            <div class="f-right margin-right-40 margin-bottom-10 margin-top-20">
                <button type="button" class="btn btn-dark margin-right-10 confirm_update_address">Send Order</button>
                <button type="button" class="btn btn-dark close-me cancel_update_address">Cancel</button>
            </div>
        </form>
        <div class="clear"></div>

    </div>
</div>

<!-- void order-->
<div class="modal-container " modal-id="void-order">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Void Order</h4>

            <div class="modal-close close-me"></div>
        </div>

        <div class="modal-content no-margin-all">

            <!-- logo and name -->

            <img style="margin-top: 55px;" src="<?php echo assets_url() ?>/images/warning.svg" alt="warning message" class="width-20per f-left">

            <table>
                <tbody>
                    <tr>
                        <td>
                            <p class="font-12 f-left margin-top-20 margin-left-20">Are you sure you want to void
                                <strong>
                                    <info class="customer_name"></info>
                                </strong> order?
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="bggray-dark margin-left-20 margin-top-10 padding-all-10">
                                <p class="font-14 f-left margin-bottom-5"><strong>Order ID:
                                        <info class="order_id"></info>
                                    </strong></p>

                                <div class="clear"></div>

                                <p class="font-12 f-left margin-bottom-5 margin-right-50">
                                    <strong><span class="red-color">Name: </span></strong></p>
                                <p class="font-12 f-left">
                                    <info class="customer_name"></info>
                                </p>

                                <div class="clear"></div>

                                <p class="font-12 f-left margin-bottom-5 margin-right-20">
                                    <strong><span class="red-color">Contact No: </span></strong></p>
                                <p class="font-12 f-left">(+63) 915-578-6147 <i class="fa fa-mobile"></i> Globe</p>

                                <div class="clear"></div>
                            </div>
                        </td>
                    </tr>

                </tbody>
            </table>

            <div class="clear"></div>

        </div>
        <!-- button -->
        <div class="margin-top-20">
            <button type="button" class="btn btn-dark f-right margin-right-20 close-me ">Cancel</button>
            <button type="button" class="btn btn-dark f-right margin-right-20 confirm_void">Confirm</button>
            <div class="clear"></div>
        </div>


    </div>
</div>

<!--process orders-->
<div class="modal-container" modal-id="process-order">

    <!-- modal body -->
    <div class="modal-body small">

        <!-- modal head -->
        <div class="modal-head ">
            <h4 class="text-left">Process Order</h4>

            <div class="modal-close close-me"></div>
        </div>

        <div class="bggray-light margin-top-20">
            <p class="font-12 text-left margin-left-40 margin-right-40 margin-bottom-10 padding-top-15">Please enter the correct address information inorder to
                find proper Retail Trade Area (RTA) for this order.</p>
            <hr>
        </div>

        <form id="coordinator_process_order_address">
            <div id="city_select_process_order_address"><input type="hidden" name="city" value="169"></div>
            <div class="hidden_fields_for_rta">
                <input type="hidden" name="building" value="zxavier Building"/>
                <input type="hidden" name="province" value="1"/>
                <input type="hidden" name="street" value=""/>
                <input type="hidden" name="barangay" value=""/>
                <input type="hidden" name="subdivision"/>
            </div>

            <div class="bggray-light text-left">

                <!-- delivery address -->
                <container class="customer_address_block">

                </container>
                <!--<div class="margin-left-20 margin-right-20">
                <label>Delivery Address:</label>

                <div class="bggray-dark padding-all-5 font-14 small-curved-border margin-bottom-20">
                    <div class="display-inline-mid margin-right-10 padding-left-20">
                        <img src="<?php /*echo assets_url() */ ?>/images/work-icon.png" alt="work icon">
                        <p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
                    </div>
                    <div class="display-inline-mid margin-left-10  padding-left-10">
                        <p class="no-margin-all"><strong>Cr8v Web Solutions, Inc.</strong></p>
                        <p class="no-margin-all">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR</p>
                        <p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
                    </div>
                    <div class="display-inline-mid text-center margin-left-30">
                        <a class="red-color" href="#">Change <br>Address <br>
                            <img src="<?php /*echo assets_url() */ ?>/images/ui/select-arrow.png">
                        </a>
                    </div>
                </div>
            </div>-->

                <hr>
            </div>


            <!-- modal content -->
            <div class="modal-content no-margin-all">

                <!-- error message -->
                <div class="modal-error-msg update-address-error-msg error-msg margin-bottom-10" style="font-size: 12px">
                    <i class="fa fa-exclamation-triangle margin-right-10"></i>Please fill up all requred fields.
                </div>

                <!-- address -->
                <table class="margin-bottom-15 margin-left-15">
                    <tbody>
                        <tr>
                            <td><label>Address Type: <!-- <span class="red-color">*</span> --></label></td>
                            <td><label>Address Label: <!-- <span class="red-color">*</span> --></label></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="select margin-right-50">
                                    <div class="frm-custom-dropdown">
                                        <div class="frm-custom-dropdown-txt">
                                            <input type="text" class="dd-txt" name="address_type" labelinput="Address Type">
                                        </div>
                                        <div class="frm-custom-icon"></div>
                                        <div class="frm-custom-dropdown-option">
                                            <div class="option" data-value="Home">Home</div>
                                        </div>
                                    </div>
                                    <select class="frm-custom-dropdown-origin" style="display: none;">
                                        <option value="Home">Home</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <input type="text" class="normal" name="address_label" labelinput="Address Label">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <!-- province -->
                <table class="margin-top-15 margin-left-15">
                    <tbody>
                        <tr>
                            <td><label>Province: <span class="red-color">*</span></label></td>
                            <td><label>City: <span class="red-color">*</span></label></td>
                        </tr>
                        <tr>
                            <td>
                                <div id="coordinator_po_modal_select_province">
                                    <div class="select margin-right-50">
                                        <div class="frm-custom-dropdown">
                                            <div class="frm-custom-dropdown-txt">
                                                <input type="text" class="dd-txt" name="province" labelinput="Province" datavalid="required">
                                            </div>
                                            <div class="frm-custom-icon"></div>
                                            <div class="frm-custom-dropdown-option">
                                            </div>
                                        </div>
                                        <select class="frm-custom-dropdown-origin" style="display: none;">
                                            <option value="">Select Province</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div id="coordinator_po_modal_select_city">
                                    <div class="select">
                                        <div class="frm-custom-dropdown">
                                            <div class="frm-custom-dropdown-txt">
                                                <input type="text" class="dd-txt" name="city" labelinput="City" datavalid="required"></div>
                                            <div class="frm-custom-icon"></div>
                                            <div class="frm-custom-dropdown-option">
                                            </div>
                                        </div>
                                    </div>
                                    <select class="frm-custom-dropdown-origin" style="display: none;">
                                        <option value="">Select City</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="margin-left-15  ">
                    <label class="margin-top-10">Landmark:</label>
                    <input type="text" class="xlarge" name="landmark">
                </div>

                <!-- 2 column table -->
                <table class="margin-top-10 margin-left-15">
                    <tbody>
                        <tr>
                            <td><label>House Number: </label></td>
                            <td><label>Building: </label></td>
                        </tr>
                        <tr>
                            <td><input type="text" class="normal margin-right-30" name="house_number"></td>
                            <td>
                                <input type="text" class="normal input_modal_autocomplete" id="coordinator_po_modal_search_building" name="building"/>

                                <div class="result-list"></div>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="margin-top-10">Unit: </label></td>
                            <td><label>Floor: </label></td>
                        </tr>
                        <tr>
                            <td><input type="text" class="normal" name="unit"></td>
                            <td><input type="text" class="normal" name="floor"></td>
                        </tr>
                    </tbody>
                </table>

                <!-- single column table -->
                <table class="margin-left-15">
                    <tbody>
                        <tr>
                            <td><label class="margin-top-10">Street:</label></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" class="xlarge input_modal_autocomplete" id="coordinator_po_modal_search_street" name="street">

                                <div class="result-list"></div>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="margin-top-10">Secondary Street:</label></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" class="xlarge input_modal_autocomplete" id="coordinator_po_modal_search_second_street" name="second_street">

                                <div class="result-list"></div>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="margin-top-10">Subdivision:</label></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" class="xlarge input_modal_autocomplete" id="coordinator_po_modal_search_subdivision" name="subdivision">

                                <div class="result-list"></div>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="margin-top-10">Barangay:</label></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" class="normal input_modal_autocomplete" id="coordinator_po_modal_search_barangay" name="barangay">

                                <div class="result-list"></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="bggray-light padding-bottom-20 rta-address-info-container">
                <hr>
                <div class="text-left margin-left-40 margin-right-40 margin-top-15">
                    <label>Nearest Store:</label>
                    <br>

                    <div class="bggray-light padding-all-10 font-14 text-center small-curved-border find-rta-container">
                        <a class="btn btn-light find_retail_search"><i class="fa fa-spinner fa-pulse hidden"></i> Find Retail Trade Area</a>

                        <a class="btn btn-light margin-left-20 show_map">Show Map</a>
                    </div>
                    <!--withput nearest store end-->

                    <!--with nearest store -->
                    <div class="bggray-dark found-retail">
                        <div>
                            <p class="font-14 f-left margin-top-20 margin-left-15">
                                <strong>
                                    <span class="retail-store-name">MM Ortigas Roosevelt | MI0444</span>
                                </strong>
                            </p>
                            <a class="font-14 f-right margin-top-20 margin-right-15 show_map">
                                <strong>
                                    <i class="fa fa-map-marker font-16 margin-right-5 "></i>Show Map
                                </strong>
                            </a>

                            <div class="clear"></div>
                        </div>
                        <div class="rta-min-container">
                            <p class="font-14 f-left green-color margin-left-15">
                                <strong>
                                <span class="retail-store-time-label">Delivery Time:<span>
                                </strong>
                            </p>
                            <p class="font-14 f-right margin-right-15 margin-bottom-20">
                                <strong>
                                    <span class="retail-store-time">N/A</span>
                                </strong>
                            </p>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <div class="notify-msg margin-top-20 hidden">
                        <strong>Store Announcement!</strong> Delivery will be delayed for 10 minutes due to bad weather. Thank you.
                    </div>


                </div>
            </div>


            <div class="f-right margin-right-40 margin-bottom-10 margin-top-20">
                <button type="button" class="btn btn-dark margin-right-10 confirm_process" disabled>Send Order</button>
                <button type="button" class="btn btn-dark close-me">Cancel</button>
            </div>
        </form>
        <div class="clear"></div>


    </div>
</div>

<!--logs-->
<div class="modal-container" modal-id="order-logs">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Order Logs</h4>

            <div class="modal-close close-me"></div>
        </div>
        <div class="modal-content">
            <label class="margin-bottom-10">ORDER LOGS:</label>

            <div class="tbl">
                <table class="no-margin-all">
                    <thead>
                        <tr>
                            <th class="width-50per">Logs</th>
                            <th>Date / Time</th>
                            <th>Username</th>
                            <!--<th>Call Center</th>-->
                        </tr>
                    </thead>
                    <tbody class="logs_container">
                        <tr class="has_spinner hidden">
                            <td colspan="4" class="text-center"><i class="fa fa-spinner fa-pulse hidden"></i></td>
                        </tr>
                        <!--                        <tr>-->
                        <!--                            <td>Start Over</td>-->
                        <!--                            <td class="text-center">05-18-2015 | 12:41 PM</td>-->
                        <!--                            <td class="text-center">Stephanie Jocson</td>-->
                        <!--                            <td class="text-center">PHUB</td>-->
                        <!--                        </tr>-->
                        <!--                        <tr>-->
                        <!--                            <td>Send Order</td>-->
                        <!--                            <td class="text-center">05-18-2015 | 12:43 PM</td>-->
                        <!--                            <td class="text-center">Stephanie Jocson</td>-->
                        <!--                            <td class="text-center">PHUB</td>-->
                        <!--                        </tr>-->
                        <!--                        <tr>-->
                        <!--                            <td>Lock Order</td>-->
                        <!--                            <td class="text-center">05-18-2015 | 12:43 PM</td>-->
                        <!--                            <td class="text-center">Kevin Miranda</td>-->
                        <!--                            <td class="text-center">PHUB</td>-->
                        <!--                        </tr>-->
                    </tbody>
                </table>
            </div>

            <!-- <label class="margin-top-10 margin-bottom-10">Store Logs:</label> -->

            <!-- <div class="tbl">
                <table class="no-margin-all">
                    <thead>
                        <tr>
                            <th class="width-70per">Store Name</th>
                            <th>Logs</th>
                            <th>Date / Time</th>
                        </tr>
                    </thead>
                    <tbody class="store_logs_container">
                        <tr>
                            <td>MM Ortigas Roosevelt | JB0444</td>
                            <td>Received Order</td>
                            <td>05-18-2015 | 12:41 PM</td>
                        </tr>
                        <tr>
                            <td>MM Ortigas Roosevelt | JB0444</td>
                            <td>Received Order - <br>Wrong Routing</td>
                            <td>05-18-2015 | 12:43 PM</td>
                        </tr>
                    </tbody>
                </table>
            </div> -->
        </div>
        <button type="button" class="btn btn-dark f-right margin-right-20 close-me">Close</button>
        <div class="clear"></div>
    </div>
</div>

<!--messenger-->
<div class="modal-container" modal-id="messenger">
    <div class="modal-body">
        <div class="modal-head ">
            <h4 class="text-left">Messenger</h4>
            <div class="modal-close close-me"></div>
        </div>

        <!-- content -->
        <div class="modal-content messenger">
            <!-- messages -->
            <div class="msg-content">
                <div class="chat-box chat_box_container">







                    <!--<p class="text-left font-12"><span class="gray-color">May 18, 2015 (12:57 PM)</span><span> - MM Sampaloc | JB1021 </span></p>
                    <div class="sender">
                        <img class="profile" src="<?php echo assets_url() ?>images/jollibe-face.png">
                        <div class="msg">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis felis consectetur, sodales nunc quis, sagittis augue. Proin eu molestie libero, vitae volutpat eros.</p><br>
                        </div>
                    </div>

                    <p class="text-right font-12"><span class="gray-color">May 18, 2015 (1:45 PM)</span> - MM Q.C. | JB1021</p>
                    <div class="receiver">
                        <div class="msg">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis felis consectetur, sodales nunc quis, sagittis augue. Proin eu molestie libero, vitae volutpat eros.</p><br>
                            <p>Sodales nunc quis, sagittis augue. Proin eu molestie libero, vitae volutpat eros.</p><br>
                            <p>TY</p>
                        </div>
                        <img class="profile" src="<?php /*echo assets_url() */?>images/profile-pic.jpg">
                    </div>

                    <p class="text-left font-12"><span class="gray-color">May 18, 2015 (1:46 PM)</span> - MM Sampaloc | JB1021</p>
                    <div class="sender">
                        <img class="profile" src="<?php /*echo assets_url() */?>images/jollibe-face.png">
                        <div class="msg">
                            <p>Thanks :)</p>
                        </div>
                    </div>-->
                </div>
                <!-- text input -->
                <div class="text-input padding-all-10">
                    <textarea row="5" class="margin-top-20 message_content" placeholder="Type your message here..."></textarea>
                    <div class="btn-container padding-top-20 margin-top-20">
                        <button class="btn btn-dark width-100per send_message">Send</button>
                        <button class="btn btn-dark margin-top-10 margin-right-10 width-50per"><img class="width-90per" src="<?php echo assets_url() ?>images/ui/smile.png"></button>
                        <button class="btn btn-dark margin-top-10 margin-left-10 no-padding-hor width-50per close-me">Cancel</button>
                    </div>
                </div>
            </div>

            <!-- contacts -->
            <div class="contacts">
                <label>Search Store:</label>
                <input type="text" class="width-100per normal"  name="search_store" id="messenger_search_store">
                <div class="content">
                    <table class="width-100per table_store_messenger_container table-inside">
                        <thead>
                            <tr>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody class="store_messenger_container">

                            <!--                            <tr>-->
                            <!--                                <td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>-->
                            <!--                            </tr>-->
                        </tbody></table>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end Modals-->


<!-- Templates -->

<!--search result template-->
<div class="search_result_container template">

    <div class="content-container viewable">
        <div class="header-search">
            <div class="width-40per f-left name_and_contact">
                <p class="font-16 name"><strong class="info_name">Ron Frederick T. San Juan</strong></p>
                <p class="font-14 margin-top-10 contact">
                    <span class="red-color"><strong class="contact_number">Contact Num:</strong></span>
                    <i class="fa fa-mobile"></i></p>
            </div>
            <div class="width-60per f-right remarks_and_address">
                <p class="font-14 no-margin-bottom remarks">
                    <strong><span class="info-remarks">THIS CUSTOMER IS EASY TO HANDLE</span></strong></p>
                <p class="font-14 margin-top-10 address">
                    <span class="red-color info_address"><strong class="">Address:</strong></span>
                </p>
            </div>
            <div class="clear"></div>

            <input type="hidden" name="street" value=""/>
            <input type="hidden" name="subdivision" value=""/>
            <input type="hidden" name="building" value=""/>
            <input type="hidden" name="barangay" value=""/>
            <input type="hidden" name="city"/>
            <input type="hidden" name="province"/>

        </div>

        <div class="collapsible" style="display: none;">
            <div class="second_header margin-bottom-10" data-customer-id="">
                <div class="width-40per f-left">
                    <p class="font-16 "><strong class="info_name">Audrey D. Hepburn</strong></p>
                </div>
                <div class="f-right">
                    <p class="font-16">
                        <strong><span class="info-remarks">THIS CUSTOMER IS EASY TO HANDLE</span></strong></p>
                </div>
                <div class="clear"></div>
            </div>
            <hr>

            <!--success messages part-->
            <div class="success-msg margin-bottom-20 margin-top-20">
                <i class="fa fa-exclamation-triangle"></i>
                Customer has been updated successfully
            </div>
            <!--end success message-->

            <!-- 1st half -->
            <div class="data-container split">
                <div class="margin-top-15 contact_number">
                    <label>Contact Number</label><br>

                    <div class="select large">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" name="undefined" class="dd-txt" readonly></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option" style="display: none;">
                                <div data-value="undefined" class="option">
                                    <span class="skype_c2c_print_container notranslate">(+63) 910-516-6153</span><span data-ismobile="false" data-isrtl="false" data-isfreecall="false" data-numbertype="paid" data-numbertocall="+639105166153" onclick="SkypeClick2Call.MenuInjectionHandler.makeCall(this, event)" onmouseout="SkypeClick2Call.MenuInjectionHandler.hideMenu(this, event)" onmouseover="SkypeClick2Call.MenuInjectionHandler.showMenu(this, event)" tabindex="-1" dir="ltr" class="skype_c2c_container notranslate" id="skype_c2c_container"><span skypeaction="skype_dropdown" dir="ltr" class="skype_c2c_highlighting_inactive_common"><span id="non_free_num_ui" class="skype_c2c_textarea_span"><img width="0" height="0" class="skype_c2c_logo_img"><span class="skype_c2c_text_span">(+63) 910-516-6153</span><span class="skype_c2c_free_text_span"></span></span></span></span> Smart
                                </div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin contact_numbers" name="contact_numbers" style="display: none;">
                            <option>(+63) 915-516-6153 Globe</option>
                            <option>(+63) 910-516-6153 Smart</option>
                        </select>
                    </div>
                </div>
                <div class="margin-top-15 alternate_contact_number">
                    <label>alternate Number</label><br>

                    <div class="select large">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" name="undefined" class="dd-txt" readonly></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option" style="display: none;">
                                <div data-value="undefined" class="option">
                                    (+63) 910-516-6153
                                </div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;" name="contact_numbers">
                            <option>(+63) 915-516-6153 Globe</option>
                            <option>(+63) 910-516-6153 Smart</option>
                        </select>
                    </div>
                </div>

                <div class="margin-top-15">
                    <p class="f-left red-color font-14"><strong>Email Address:</strong></p>
                    <p class="f-right font-14 email_address">markdulay@gmail.com</p>

                    <div class="clear"></div>
                </div>

                <label class="margin-top-15">CARDS:</label>

                <div class="happy_plus_card_container">
                    <div class="bggray-light margin-bottom-10 padding-all-5 font-14 small-curved-border happy_plus_card template">
                        <div class="display-inline-mid margin-right-10">
                            <img alt="happy-plus" src="<?php echo base_url() ?>/assets/images/happy-plus.jpg" class="thumb">
                        </div>
                        <div class="display-inline-mid margin-left-10 divider padding-left-10">
                            <p class="no-margin-all info-happy-plus-number">0083-123456-46578</p>
                            <span class="red-color"><strong>Exp. Date:</strong></span>
                            <p class="expiration_date" style="display:inline"> September 20, 2016</p></p>
                        </div>
                        <div class="display-inline-mid text-center margin-left-10 f-right">
                            <!-- <a href="#">Show Card<br>History</a> -->
                        </div>
                    </div>
                </div>


                <label class="margin-top-15">DELIVERY ADDRESS:</label>

                <div class="customer_address_container">
                    <div class="bggray-light padding-all-5 font-14 small-curved-border template customer_address">
                        <div class="display-inline-mid padding-left-20">
                            <img alt="work icon" class="info_address_type" width="35px" height="33px" src="<?php echo base_url() ?>/assets/images/work-icon.png">
                            <p class="font-12 text-center no-margin-all"><strong class="info_address_type"></strong></p>
                        </div>
                        <div class="display-inline-mid padding-left-10" style="max-width: 245px">
                            <p class="no-margin-all">
                                <strong class="info_address_label">Cr8v Web Solutions, Inc.</strong></p>
                            <p class="no-margin-all info_address_complete">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR
                            </p>
                            <p class="no-margin-all gray-color info_address_landmark"></p>
                        </div>
                        <div class="display-inline-mid text-center margin-left-10 edit_container">
                            <a class="red-color edit_address">Change<br>Address<br>

                                <div class="arrow-down"></div>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="customer_secondary_address">

                    <div class="margin-top-10 margin-left-20 deliver-to-different">
                        <p class="font-12"><i class="fa fa-plus-circle fa-3x margin-right-10  yellow-color f-left"></i>
                        </p>
                        <p modal-target="deliver-to-different" class="font-12 f-left margin-top-10 margin-left-10 modal-trigger">
                            <strong>Deliver to a Different Address</strong></p>

                        <div class="clear"></div>
                    </div>
                </div>

                <hr>


                <label class="margin-top-15">NEAREST STORE:</label>

                <!--without nearest store -->

                <div class="bggray-light padding-all-10 font-14 text-center small-curved-border find-rta-container rta-address-info-container">
                    <button class="btn btn-light find_retail_search"><i class="fa fa-spinner fa-pulse hidden"></i> Find Retail Trade Area</button>

                    <button class="btn btn-light margin-left-20 show_map">Show Map</button>
                </div>
                <!--withput nearest store end-->

                <!--with nearest store -->
                <div class="bggray-dark found-retail rta-address-info-container">
                    <div>
                        <p class="font-14 f-left margin-top-20 margin-left-15">
                            <strong>
                                <span class="retail-store-name">MM Ortigas Roosevelt | MI0444</span>
                            </strong>
                        </p>
                        <a class="font-14 f-right margin-top-20 margin-right-15 show_map">
                            <strong>
                                <i class="fa fa-map-marker font-16 margin-right-5"></i>Show Map
                            </strong>
                        </a>

                        <div class="clear"></div>
                    </div>
                    <div class="rta-min-container">
                        <p class="font-14 f-left green-color margin-left-15">
                            <strong>
								<span class="retail-store-time-label">Delivery Time:<span>
                            </strong>
                        </p>
                        <p class="font-14 f-right margin-right-15 margin-bottom-20">
                            <strong>
                                <span class="retail-store-time">N/A</span>
                            </strong>
                        </p>

                        <div class="clear"></div>
                    </div>
                </div>

                <div class="notify-msg margin-top-20 hidden">
                    <strong>Store Announcement!</strong> Delivery will be delayed for 10 minutes due to bad weather. Thank you.
                </div>

                <!--with nearest store end-->


            </div>
            <!-- End 1st Half -->

            <!-- 2nd half -->
            <div class="data-container split margin-left-15 order_history">
                <label class="margin-top-15">order history:</label>

                <div class="arrow-selector">
                    <div class="arrow-left"><i class="fa "></i></div>
                    <div class="select">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" name="date-selected" class="dd-txt" readonly></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option" style="display: none;">

                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">

                        </select>
                    </div>
                    <div class="arrow-right"><i class="fa "></i></div>
                </div>

                <div class="margin-top-15 font-12">
                    <p class="red-color f-left "><strong>Last Payment Method Used:</strong> <strong class="last_payment_method_used"></strong></p>
                    <p class="red-color f-left font-10">
                        <!-- <strong>Items with red text means they are unvavailable today</strong> --></p>
                    <p class="f-right"></p>

                    <div class="clear"></div>
                </div>
                <div class="order_history_container">

                </div>
                <div class="margin-top-15 font-12">
                    <p class="red-color f-left "><strong>Last Order Date:</strong></p><p class="f-left"><strong class="last_order_date"></strong></p>
                    <p class="red-color f-left font-10">
                        <!-- <strong>Items with red text means they are unvavailable today</strong> --></p>
                    <p class="f-right"></p>

                    <div class="clear"></div>
                </div>


                <!-- <label class="margin-top-15">other brand order history</label> -->

                <!--                <div class="bggray-light">-->
                <!--                    <table class="font-14 width-100per">-->
                <!--                        <tbody>-->
                <!--                            <tr>-->
                <!--                                <td class="padding-all-10">-->
                <!--                                    <img alt="burger king" src="http://localhost/callcenter/assets/images/affliates/bk-logo.png" class="small-thumb">-->
                <!--                                </td>-->
                <!--                                <td class="padding-all-10">Burger King</td>-->
                <!--                                <td class="padding-all-10 text-right">May 1, 2015 | 4:20 PM</td>-->
                <!--                            </tr>-->
                <!--                            <tr>-->
                <!--                                <td class="padding-all-10">-->
                <!--                                    <img alt="greenwich" src="http://localhost/callcenter/assets/images/affliates/gw-logo.png" class="small-thumb">-->
                <!--                                </td>-->
                <!--                                <td class="padding-all-10">Greenwich</td>-->
                <!--                                <td class="padding-all-10 text-right">May 1, 2015 | 4:20 PM</td>-->
                <!--                            </tr>-->
                <!--                            <tr>-->
                <!--                                <td class="padding-all-10">-->
                <!--                                    <img alt="chowking" src="http://localhost/callcenter/assets/images/affliates/ck-logo.png" class="small-thumb">-->
                <!--                                </td>-->
                <!--                                <td class="padding-all-10">Chowking</td>-->
                <!--                                <td class="padding-all-10 text-right">May 1, 2015 | 4:20 PM</td>-->
                <!--                            </tr>-->
                <!--                        </tbody>-->
                <!--                    </table>-->
                <!--                </div>-->
            </div>

            <div class="text-right margin-top-20">
                <button class="btn btn-dark margin-right-10 btn-update" type="button">Update Customer Information</button>
                <button class="btn btn-dark margin-right-10 add_last_meal_to_order" type="button" disabled>Add Last Order to Cart</button>
                <button class="btn btn-dark order_process proceed_order_from_search" type="button" disabled>Proceed to Order</button>
            </div>
        </div>
    </div>

    <!--collapsible content-->


    <!--end collapsible content-->

</div>
<!--search result teamplte end-->

<!--customer address template-->
<div class="bggray-light padding-all-5 font-14 small-curved-border template customer_address">
    <div class="display-inline-mid margin-right-10 padding-left-20">
        <img alt="work icon" src="<?php echo base_url() ?>/assets/images/work-icon.png">
        <p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
    </div>
    <div class="display-inline-mid margin-left-10  padding-left-10">
        <p class="no-margin-all"><strong>Cr8v Web Solutions, Inc.</strong></p>
        <p class="no-margin-all">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR</p>
        <p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
    </div>
    <div class="display-inline-mid text-center margin-left-25">
        <a href="#" class="red-color">Change<br>Address<br>

            <div class="arrow-down"></div>
        </a>
    </div>
</div>
<!--end customer address template-->

<!--customer list search result template-->
<div class="search_result_customer_list template" id="customer_list_search_result">
    <!-- sample-1-->
    <div class="content-container viewable">
        <div class="header-search">
            <div class="width-40per f-left name_and_contact">
                <p class="font-16 name"><strong class="info_name">Ron Frederick T. San Juan</strong></p>
                <p class="font-14 margin-top-10 contact">
                    <span class="red-color"><strong class="contact_number">Contact Num:</strong></span>
                    <i class="fa fa-mobile"></i></p>
            </div>
            <div class="width-60per f-right remarks_and_address">
                <p class="font-14 no-margin-bottom remarks">
                    <strong><span class="info-remarks">THIS CUSTOMER IS EASY TO HANDLE</span></strong></p>
                <p class="font-14 margin-top-10 address">
                    <span class="red-color info_address"><strong class="">Address: </strong></span>
                </p>
            </div>
            <div class="clear"></div>
        </div>


        <div class="collapsible" style="display: none;">
            <div class="second_header margin-bottom-10" data-customer-list-id="">
                <div class="width-40per f-left">
                    <p class="font-16 "><strong class="info_name">Audrey D. Hepburn</strong></p>
                </div>
                <div class="f-right">
                    <p class="font-16">
                        <strong><span class="info-remarks">THIS CUSTOMER IS EASY TO HANDLE</span></strong></p>
                </div>
                <div class="clear"></div>
            </div>

            <hr>

            <!--success messages part-->
            <div class="success-msg margin-bottom-20 margin-top-20">
                <i class="fa fa-exclamation-triangle"></i>
                Customer has been updated successfully
            </div>
            <!--end success message-->

            <div class="data-container split">
                <!-- client's information -->
                <div class="margin-top-20 customer_list_client_info">

                    <p class="f-left red-color font-12 customer_list_contact_label"><strong>Contact Number: </strong>
                    </p>

                    <div class="customer_list_contact_number_container">
                    </div>
                    <!--
                                        <p class="f-right font-12 customer_list_contact_number_phone">(02) 257-68-95
                                            <i class="fa fa-phone margin-right-5"></i>PLDT</p>
                                        <div class="clear"></div>
                                        <p class="f-right font-12 customer_list_contact_number_mobile">(+63) 915-516-6153
                                            <i class="fa fa-mobile margin-right-5"></i>Globe</p>
                    -->
                    <div class="clear"></div>

                    <p class="f-left red-color font-12 margin-top-10 customer_list_email_address_label">
                        <strong>Email Address:</strong></p>
                    <p class="f-right font-12 margin-top-10 customer_list_email_address">audrey@gmail.com</p>

                    <div class="clear"></div>

                    <p class="f-left red-color font-12 margin-top-10 customer_list_date_of_birth_label">
                        <strong>Date of Birth: </strong></p>
                    <p class="f-right font-12 margin-top-10 customer_list_date_of_birth">May 4, 1916</p>

                    <div class="clear"></div>
                </div>

                <!-- delivery address -->
                <div>
                    <label class="discounts margin-top-10">Discounts:</label>

                    <div class="bggray-light padding-all-5 font-14 small-curved-border customer_list_discounts template">
                        <div class="display-inline-mid customer_list_discounts_label">
                            <p class="font-12 no-margin-all"><strong>Senior Citizen</strong></p>
                        </div>
                        <div class="display-inline-mid margin-left-10 margin-top-10 margin-bottom-10 divider padding-left-10">
                            <p class="no-margin-all font-12 customer_list_discounts_name">Audrey Dean Hepburn</p>
                            <p class="font-12 no-margin-all">
                                <strong class="customer_list_discounts_number"><span class="red-color">OSCA Number: </span>0014-7845</strong>
                            </p>
                        </div>
                    </div>

                    <label class="margin-top-10 cards">Cards:</label>
                    <!-- happy plus picture -->
                    <div class="customer_list_cards_container">
                        <div class="bggray-light margin-bottom-10 padding-all-5 font-14 small-curved-border customer_list_cards template">
                            <div class="display-inline-mid margin-left-10">
                                <img class="thumb " src="<?php echo base_url() ?>/assets/images/happy-plus.jpg" alt="happy-plus">
                            </div>
                            <div class="display-inline-mid margin-left-10 divider padding-left-10">
                                <p class="no-margin-all customer_list_card_number">008-248-369-154</p>
                                <span class="red-color"><strong>Exp. Date:</strong></span>
                                <p class="customer_list_card_number_expiration" style="display: inline;"> September 20, 2016</p>
                            </div>
                            <div class="display-inline-mid text-center margin-left-10">
                                <!-- <a href="#">Show Card<br>History</a> -->
                            </div>
                        </div>
                    </div>
                    <!-- happy plus picture -->
                    <div class="bggray-light padding-all-5 margin-top-10 font-14 small-curved-border template">
                        <div class="display-inline-mid margin-left-10">
                            <img class="thumb" src="<?php echo base_url() ?>/assets/images/happy-plus.jpg" alt="happy-plus">
                        </div>
                        <div class="display-inline-mid margin-left-10 divider padding-left-10">
                            <p class="no-margin-all">003-275-472-124<br>
                                <span class="red-color"><strong>Exp. Date:</strong></span> Ocotober 18, 2016</p>
                        </div>
                        <div class="display-inline-mid text-center margin-left-20">
                            <!-- <a href="#">Show Card<br>History</a> -->
                        </div>
                    </div>

                    <!-- address book -->

                    <label class="margin-top-10">Address Book: </label>

                    <div class="default_address_container">
                        <div class="bggray-light padding-all-5 font-14 small-curved-border margin-bottom-10  customer_list_default_address template">
                            <div class="display-inline-mid margin-right-10 padding-left-20">
                                <img src="<?php echo base_url() ?>/assets/images/work-icon.png" alt="work icon" class="default_address_image">
                                <p class="font-12 text-center no-margin-all">
                                    <strong class="default_address_type">WORK</strong></p>
                            </div>
                            <div class="display-inline-mid margin-left-10 padding-left-10 customer_list_default_address_text">
                                <p class="no-margin-all default_address_label">
                                    <strong class="default_address_label">Cr8v Web Solutions, Inc. -
                                        <span class="red-color">Default</span></strong></p>
                                <p class="no-margin-all default_address_complete">66C &amp; 66D, San Rafael St.
                                    <br> Brgy. Kapitolyo, Pasig City. - NCR</p>
                                <p class="no-margin-all gray-color default_address_landmark">- Near Jewels Convinient Store</p>
                            </div>
                        </div>
                    </div>

                    <div class="other_address_container">
                        <div class="bggray-light padding-all-5 font-14 small-curved-border margin-bottom-10 customer_list_address template">
                            <div class="display-inline-mid margin-right-10 padding-left-20">
                                <img src="<?php echo base_url() ?>/assets/images/Home2.svg" alt="work icon" class="small-thumb address_image">
                                <p class="font-12 text-center no-margin-all"><strong class="address_type">WORK</strong>
                                </p>
                            </div>
                            <div class="display-inline-mid margin-left-10  padding-left-10 customer_list_address_text">
                                <p class="no-margin-all address_label">
                                    <strong class="address_label">House Address # 1 </strong></p>
                                <p class="no-margin-all address_complete">Rm. 404 3rd Floor, 691 Eduard Bldg,
                                    <br>Green Field St, Kapitolyo, Pasig City - NCR</p>
                                <p class="no-margin-all gray-color address_landmark">- Near Jewels Convinient Store</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="data-container split margin-left-15">
                <div class="margin-top-20">
                    <div class="category-item f-left">
                        <img src="<?php echo base_url() ?>/assets/images/Customer_Call.svg" alt="customer logo calling">
                        <p class="font-12 f-right margin-top-10 padding-right-10"><strong>Voice Customer</strong></p>

                        <div class="clear"></div>

                    </div>

                    <div class="category-item f-right">
                        <img src="<?php echo base_url() ?>/assets/images/Customer_Web.svg" alt="customer logo calling" class="padding-left-10">
                        <p class="font-12 f-right margin-top-10"><strong>Web Customer</strong></p>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="order_history">
                    <label class="margin-top-15">order history:</label>
                    <div class="clear"></div>

                    <div class="arrow-selector margin-bottom-20">
                        <div class="arrow-left"><i class="fa "></i></div>
                        <div class="select">
                            <div class="frm-custom-dropdown">
                                <div class="frm-custom-dropdown-txt">
                                    <input type="text" name="date-selected" class="dd-txt"></div>
                                <div class="frm-custom-icon"></div>
                                <div class="frm-custom-dropdown-option" style="display: none;">
                                    <!-- <div data-value="undefined" class="option">April 28, 2015 | 4:20 PM</div>-->
                                </div>
                            </div>
                            <select class="frm-custom-dropdown-origin" style="display: none;">
                                <!-- <option>April 27, 2015 | 4:20 PM</option>
                                 <option>April 28, 2015 | 4:20 PM</option>-->
                            </select>
                        </div>
                        <div class="arrow-right"><i class="fa "></i></div>
                    </div>

                    <div class="order_history_container">

                    </div>
                </div>

                <!--                 <label class="margin-top-15">other brand order history</label>

                <div class="bggray-light">
                    <table class="font-14 width-100per">
                        <tbody>
                            <tr>
                                <td class="padding-all-10">
                                    <img class="small-thumb" src="<?php echo base_url() ?>/assets/images/affliates/bk-logo.png" alt="burger king">
                                </td>
                                <td class="padding-all-10">Burger King</td>
                                <td class="padding-all-10 text-right">May 7, 2015 | 12:25 PM</td>
                            </tr>
                            <tr>
                                <td class="padding-all-10">
                                    <img class="small-thumb" src="<?php echo base_url() ?>/assets/images/affliates/gw-logo.png" alt="greenwich">
                                </td>
                                <td class="padding-all-10">Greenwich</td>
                                <td class="padding-all-10 text-right">April 27, 2015 | 9:14 AM</td>
                            </tr>
                            <tr>
                                <td class="padding-all-10">
                                    <img class="small-thumb" src="<?php echo base_url() ?>/assets/images/affliates/ck-logo.png" alt="chowking">
                                </td>
                                <td class="padding-all-10">Chowking</td>
                                <td class="padding-all-10 text-right">April 25, 2015 | 10:11 AM</td>
                            </tr>
                        </tbody>
                    </table>
                </div> -->

            </div>
            <div class="f-right margin-top-20">
                <button type="button" class="btn btn-light margin-right-10 btn-update">Update Customer Information</button>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</div>
<!--end customer list search result template-->

<!--customer information template in orders page-->
<div class="order_customer_information template">
    <table class="width-100per">
        <tbody>
            <tr>
                <td class="light-red-color padding-all-5">Name:</td>
                <td class="text-right padding-all-5 info-customer-full-name">Mark Anthony D. Dulay</td>
            </tr>
            <tr>
                <td class="light-red-color padding-all-5">Contact Num:</td>
                <td class="text-right padding-all-5 info-customer-mobile">(+63) 940-848-1458
                    <i class="fa fa-mobile"></i> TNT
                </td>
            </tr>
            <tr>
                <td class="light-red-color padding-all-5">Alternate Num:</td>
                <td class="text-right padding-all-5 info-customer-mobile-alternate">(+63) 910-576-1248
                    <i class="fa fa-mobile"></i> SMART
                </td>
            </tr>
        </tbody>
    </table>
</div>
<!--end customer information template in orders page-->

<!-- found rta store in order page template-->
<div class="rta_store template">
    <table class="width-100per">
        <tbody>
            <tr>
                <td class="padding-all-5"><p class="panel-title no-padding-bottom f-left">
                        <strong>Store Information</strong></p></td>
                <td class="text-right padding-all-5">
                    <a class="f-right" href="#"><i class="fa fa-map-marker"></i> Show Map</a></td>
            </tr>
            <tr>
                <td class="padding-all-5"><strong class="store_name">MM Ortigas Roosevelt</strong></td>
                <td class="text-right padding-all-5"><strong class="store_code">JB0044</strong></td>
            </tr>
            <tr>
                <td class="padding-all-5 light-red-color"><strong class="store_time_label">Delivery Time:</strong></td>
                <td class="text-right padding-all-5 store_time">40 Minutes</td>
            </tr>
        </tbody>
    </table>

    <div class="store_messages">
    </div>

    <div class="warn-msg margin-top-15 unavailable_products">
        <p class="f-left"><strong>Unvailable Products</strong></p>
        <p class="f-right"><a href="javascript:void(0)" class="toggle_unavailable_products">Show</a></p>
        <div class="clear"></div>

        <div class="unavailable_products_container hidden" style="max-height: 300px; overflow-y: scroll;" ></div>
    </div>
</div>
<!--found rta store in order page template end-->

<!-- not found rta store in order page template-->
<div class="no_rta_store template">
    <table class="width-100per">
        <tbody>
            <tr>
                <td class="padding-all-5"><p class="panel-title no-padding-bottom f-left">
                        <strong>Store Information</strong></p></td>
                <td class="text-right padding-all-5"
                </td>
            </tr>
            <tr>
                <td class="padding-all-5 light-red-color"><strong class="has_spinner">No Stores Available at this moment <i class="fa fa-spinner fa-pulse hidden"></i></strong></td>
                <td class="text-right padding-all-5"></td>
            </tr>
        </tbody>
    </table>

</div>
<!--not found rta store in order page template end-->

<!--order history template-->
<div class="small-curved-border order_history template">
    <table class="font-14" style="width: 100%; display:block;">
        <thead class="bggray-dark">
            <tr>
                <th class="padding-all-10" style="width:10%;" >Quantity</th>
                <th class="padding-all-10" style="width:30%;" >Product</th>
                <th class="padding-all-10" style="width:10%;" >Price</th>
                <th class="padding-all-10" style="width:15%;"  colspan="2">Subtotal</th>
            </tr>
        </thead>

        <tbody class="bggray-light cart-container" style="display:block; max-height: 300px; overflow-y: auto; width:100%;">
            <tr class="product-breakdown-item template">
                <td class="padding-all-10 text-center product-quantity">2</td>
                <td class="padding-all-10 product-name">
                    <!--<div class="arrow-down"></div>-->
                    Champ Amazing aloha
                </td>
                <td class="padding-all-10 product-price">203.50</td>
                <td class="padding-all-10 product-total">407.00</td>
            </tr>
        </tbody>

        <tbody>
            <tr class="bggray-middark">
                <td colspan="2" style="width:20%;" class="padding-all-10 text-left">Total Cost</td>
                <td colspan="3" style="width:80%;" class="padding-all-10 text-right total-cost">591.80 PHP</td>
            </tr>
            <tr class="bggray-middark">
                <td class="padding-all-10 text-left" colspan="2" style="width:20%">Added VAT</td>
                <td class="padding-all-10 text-right total-vat" colspan="3">95.04 PHP</td>
            </tr>
            <tr class="bggray-dark">
                <td class="padding-all-10 text-left font-16" colspan="2">
                    <strong>Total Bill</strong></td>
                <td class="padding-all-10 text-right font-14" colspan="2" style="width:60%">
                    <strong class="total-bill">887.04 PHP</strong></td>
            </tr>
            <tr class="bggray-middark">
                <td class="padding-all-10 text-left" colspan="2">Delivery Charge</td>
                <td class="padding-all-10 text-right delivery-charge" colspan="3">72.00 PHP</td>
            </tr>
        </tbody>

    </table>
</div>
<!--end order history template-->

<!--coordinator order template-->
<div class="order_search_result_block template">
    <div class="row content-container viewable block-header">
        <div>
            <div class="width-40per f-left">
                <p class="font-16 margin-bottom-5"><strong>Order ID:
                        <info class="order_id">41</info>
                    </strong></p>
                <p class="font-16 margin-bottom-5"><strong>
                        <info class="store_summary">MM Ortigas Roosevelt | JB0444</info>
                    </strong></p>
                <p><strong><span class="red-color">Transaction Time:</span>
                        <info class="date_added"> May 18, 2015 | 11:24:00</info>
                    </strong></p>
            </div>

            <div class="width-40per f-left">
                <p class="margin-bottom-5"><strong><span><info class="order_type">MANUAL ORDER</info></span></strong>
                </p>
                <p class="margin-bottom-5"><strong><span class="red-color">Name:</span></strong>
                    <info class="customer_name">Jonathan R. Ornido</info>
                </p>
                <p class="margin-bottom-5"><strong><span class="red-color">Contact No.:</span></strong>
                    <info class="customer_contact"> (+63) 915-516-6153 | Globe</info>
                </p>
            </div>

            <div class="f-left margin-left-20 locked hidden">
                <p class=""><i class="fa fa-lock fa-5x red-color margin-left-30"></i></p>
                <p class="red-color font-12"><strong>
                        <info class="agent_locker"></info>
                    </strong></p>

            </div>

            <div class="clear"></div>
        </div>
    </div>
    <div class="hidden_fields_for_rta">
        <input type="hidden" name="street" value=""/>
        <input type="hidden" name="subdivision" value=""/>
        <input type="hidden" name="building" value=""/>
        <input type="hidden" name="barangay" value=""/>
        <input type="hidden" name="city"/>
        <input type="hidden" name="province"/>
    </div>
    <div class="content-container hidden block-content">
        <div>
            <header-group class="header">
                <div class="f-left margin-bottom-10">
                    <p class="font-16"><strong class="order_id">Order ID:
                            <info class="order_id">734784</info>
                        </strong></p>
                    <p class="font-16 no-margin-all"><strong class="store_name">
                            <info class="store_summary">734784</info>
                        </strong></p>
                </div>
                <p class="no-margin-bottom f-right">
                    <strong><span><info class="order_type">MANUAL ORDER</info></span></strong>
                </p>
                <br>
                <p class="font-12 f-right margin-top-5">
                    <strong><span class="red-color">Elapsed Time: </span></strong>
                    <info class="elapsed_time"></info>
                </p>

                <div class="clear"></div>
            </header-group>

            <hr>

            <div class="data-container split">

                <!-- clients information -->
                <div class="margin-top-20">
                    <p class="f-left red-color font-12 margin-bottom-5"><strong>Name: </strong></p>
                    <p class="f-right font-12 ">
                        <info class="customer_name"></info>
                    </p>

                    <div class="clear"></div>

                    <p class="f-left red-color font-12 margin-bottom-5"><strong>Contact Number: </strong></p>
                    <p class="f-right font-12 ">
                        <info class="customer_contact">MANUAL ORDER</info>
                    </p>

                    <div class="clear"></div>

                    <p class="f-left red-color font-12 margin-bottom-5"><strong>Transaction Time:</strong></p>
                    <p class="f-right font-12 ">
                        <info class="date_added">MANUAL ORDER</info>
                    </p>

                    <div class="clear"></div>
                </div>

                <!-- delivery address -->
                <label class="margin-top-10 margin-bottom-10 ">Delivery Address:</label>
                <container class="customer_address_block">


                    <div class="customer_address_container" style="padding:10px">
                        <div class="bggray-light padding-all-5 font-14 small-curved-border template customer_address">
                            <div class="display-inline-mid margin-right-10 padding-left-20">
                                <img alt="work icon" class="info_address_type" width="35px" height="33px" src="<?php echo base_url() ?>/assets/images/work-icon.png">
                                <p class="font-12 text-center no-margin-all"><strong class="info_address_type"></strong>
                                </p>
                            </div>
                            <div class="display-inline-mid margin-left-10  padding-left-10" style="max-width: 245px">
                                <p class="no-margin-all">
                                    <strong class="info_address_label">Cr8v Web Solutions, Inc.</strong></p>
                                <p class="no-margin-all info_address_complete">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR
                                </p>
                                <p class="no-margin-all gray-color info_address_landmark"></p>
                            </div>
                            <div class="display-inline-mid text-center margin-left-10 edit_container hidden">
                                <a class="red-color edit_address">Change<br>Address<br>

                                    <div class="arrow-down"></div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="customer_secondary_address hidden">


                    </div>
                </container>

                <!-- cards -->
                <label class="margin-top-15">CARDS:</label>

                <div class="happy_plus_card_container">
                    <div class="bggray-light margin-bottom-10 padding-all-5 font-14 small-curved-border happy_plus_card template">
                        <div class="display-inline-mid margin-right-10">
                            <img alt="happy-plus" src="<?php echo base_url() ?>/assets/images/happy-plus.jpg" class="thumb">
                        </div>
                        <div class="display-inline-mid margin-left-10 divider padding-left-10">
                            <p class="no-margin-all info-happy-plus-number">0083-123456-46578</p>
                            <span class="red-color"><strong>Exp. Date:</strong></span>
                            <p class="expiration_date" style="display:inline"> September 20, 2016</p></p>
                        </div>
                        <div class="display-inline-mid text-center margin-left-10 f-right">
                            <!-- <a href="#">Show Card<br>History</a> -->
                        </div>
                    </div>
                </div>


                <div class="margin-top-10">
                    <p class="f-left font-12 red-color  margin-bottom-5"><strong>Payment Method: </strong></p>
                    <p class="f-right font-12">
                        <info class="payment_mode">Cash</info>
                    </p>

                    <div class="clear"></div>

                    <p class="f-left font-12 red-color  margin-bottom-5"><strong>Change For: </strong></p>
                    <p class="f-right font-12">
                        <info class="change_for">Cash</info>
                    </p>

                    <div class="clear"></div>

                    <p class="f-left font-12 red-color  margin-bottom-5"><strong>Remarks: </strong></p>
                    <p class="f-right font-12 gray-color">No Remarks</p>

                    <div class="clear"></div>

                    <!--show if advanced order-->
                    <info class="advanced_order" style="display:none">
                        <p class="f-left font-12 red-color  margin-bottom-5"><strong>Flagged for Follow Up: </strong>
                        </p>
                        <p class="f-right font-12 gray-color">
                            <info class="follow_up"></info>
                        </p>

                        <div class="clear"></div>

                        <p class="f-left font-12 red-color  margin-bottom-5"><strong>Delivery Date / Time: </strong></p>
                        <p class="f-right font-12 gray-color">
                            <info class="delivery_date"></info>
                        </p>
                    </info>


                    <div class="clear"></div>

                </div>
            </div>


            <div class="data-container split margin-left-15">
                <label class="margin-top-15">order: </label>

                <div class="order_history_container">


                </div>

            </div>

            <button-group class="locked hidden">
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 modal-trigger order_logs complete_order manual_order rejected_order search_order completed_order verification_order" modal-target="order-logs">Logs</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 modal-trigger manual_order rejected_order manually_relay" modal-target="manual-relay">Manually Relay</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 modal-trigger rejected_order  manual_order verification_order search_order" modal-target="void-order">Void Order</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 editing_order">Cancel Edit Order</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 manual_order edit_order"><i class="fa fa-spinner fa-pulse hidden"></i>Edit Order</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 modal-trigger rejected_order" modal-target="update-address">Update Address</button>
                <?php if ( $this->session->userdata('user_level') == 2 ){ ?> <!--coordinator-->
                    <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 modal-trigger manual_order rejected_order verification_order" modal-target="reroute-order">Re-route Order</button>
                <?php } ?>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 modal-trigger verification_order" modal-target="process-order">Process Order</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 order_lock manual_order rejected_order verification_order" dolock="0">Unlock</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 search_order followup-order">Follow Up Order</button>
            </button-group>
            <div class="clear"></div>

            <button-group class="unlocked hidden">
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 modal-trigger order_logs complete_order manual_order rejected_order search_order completed_order verification_order" modal-target="order-logs">Logs</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 modal-trigger disabled manual_order rejected_order">Manually Relay</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 modal-trigger disabled manual_order verification_order rejected_order search_order" modal-target="void-order">Void Order</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 disabled editing_order">Cancel Edit Order</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 disabled manual_order edit_order">Edit Order</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 modal-trigger rejected_order disabled">Update Address</button>
                <?php if ( $this->session->userdata('user_level') == 2 ){ ?> <!--coordinator-->
                    <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 modal-trigger disabled manual_order rejected_order verification_order">Re-route Order</button>
                <?php } ?>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 modal-trigger disabled verification_order" modal-target="">Process Order</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 order_lock manual_order rejected_order verification_order" dolock="1">Lock</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 search_order followup-order">Follow Up Order</button>
            </button-group>

            <div class="clear"></div>
        </div>
    </div>
</div>
<!--end coordinator order template-->

<div class="billbreakdown template">
    <p class="font-12 red-color f-left margin-bottom-5"><strong>Name:</strong></p>
    <p class="font-12 f-right"><strong class="info-customer-full-name">Mark Anthony D. Dulay</strong></p>

    <div class="clear"></div>

    <p class="font-12 red-color f-left margin-bottom-5"><strong>Contact Num: </strong></p>
    <p class="font-12 f-right"><strong class="info-customer-mobile">(+63) 939-848-1458
            <i class="fa fa-mobile font-16"></i> TNT</strong></p>

    <div class="clear"></div>

    <p class="font-12 red-color f-left margin-bottom-5"><strong>Alternate Num: </strong></p>
    <p class="font-12 f-right"><strong class="info-customer-mobile-alternate">(+63) 910-576-1248 <i class="fa fa-mobile font-16"></i> Smart</strong></p>

    <div class="clear"></div>

    <!-- address -->
    <div class="bggray-dark">
        <table>
            <tbody>
                <tr>
                    <td>
                        <img src="<?php echo base_url() ?>/assets/images/work-icon.png" alt="working icon" class="margin-top-20 margin-left-10 margin-right-10 address_type_image">
                        <p class="font-12 text-center no-margin-all"><strong class="address_type">WORK</strong></p>
                        <br>
                        <!--                                    <label class="margin-left-10 margin-bottom-20 address_label">Work</label>-->
                    </td>
                    <td>
                        <p class="font-12 margin-left-10"><strong class="address_label">Cr8v Websolutions, Inc.</strong>
                        </p>
                        <p class="font-12 margin-left-10 address_text">66D 2nd Floor, 591 Cr8v Bldg, San Rafael
                            <br>St, Brgy.
                            Kapitolyo, Pasig City - NCR</p>
                        <p class="font-12 gray-color margin-left-10 address_landmark">- Near Jewels Convinience Store</p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <p class="font-12 red-color f-left margin-top-15 margin-bottom-10"><strong>Serving Time:</strong></p>
    <p class="font-12 f-right margin-top-15">20 Minutes</p>

    <div class="clear"></div>

    <hr>
    <div class="margin-top-10">
        <p class="font-12 red-color f-left "><strong>Order Mode: </strong></p>
        <p class="font-12 f-right order_mode">Delivery</p>

        <div class="clear"></div>
    </div>

    <div class="margin-top-5">
        <p class="font-12 red-color f-left"><strong>Transaction Time:</strong></p>
        <p class="font-12 f-right transaction_time"> May 18, 2012 | 1:01 PM</p>

        <div class="clear"></div>
    </div>

    <div class="margin-top-5">
        <p class="font-12 red-color f-left"><strong>Pricing: </strong></p>
        <p class="font-12 f-right pricing_diff"> 10% Delivery Charge with VAT</p>

        <div class="clear"></div>
    </div>
    <div class="margin-top-5">
        <p class="font-12 red-color f-left"><strong>For Special Events? </strong></p>
        <p class="font-12 f-right">No</p>

        <div class="clear"></div>
    </div>
    <div class="margin-top-5">
        <p class="font-12 red-color f-left"><strong>Payment Mode: </strong></p>
        <p class="font-12 f-right payment_type">Cash</p>

        <div class="clear"></div>
    </div>

    <advance class="hidden">


        <div class="margin-top-5 margin-right-5 display-inline-top">
            <label class="margin-top-10">Delivery Date</label>
            <div class="date-picker" id="order-summary-date-picker">
                <input type="text" name="delivery_date" data-date-format="MM/DD/YYYY" readonly>
                <span class="fa fa-calendar text-center red-color"></span>
            </div>
        </div>
        <div class="margin-top-5 display-inline-top">
            <label class="margin-top-10">Delivery Time</label>
            <div class="date-pickertime" id="order-summary-time-picker">
                <input type="text" name="delivery_time" data-date-format="MM/DD/YYYY" readonly>
                <span class="fa fa-clock-o text-center red-color"></span>
            </div>
        </div>

    </advance>

</div>
<!-- end Templates-->

<div class="chat_message template">
    <p class="font-12 data-date-indention">
        <span class="gray-color data-date-sent">May 18, 2015 (12:57 PM)</span><span class="data-username-details"> - MM Sampaloc | JB1021 </span></p>
    <div class="message-identity">
        <img class="sender-profile hidden profile" src="<?php echo assets_url() ?>images/jollibe-face.png">

        <div class="msg">
            <p class="data-message-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis felis consectetur, sodales nunc quis, sagittis augue. Proin eu molestie libero, vitae volutpat eros.</p>
        </div>

        <img class="receiver-profile hidden profile" src="<?php echo assets_url() ?>images/jollibe-face.png">
    </div>
</div>

<section class="agent-list template" style="clear:both">
    <div class="content-container viewable agent-overview">
        <div>
            <div class="f-left  sphere-small  margin-right-20">
                <p class="font-20 info_initials">ML</p>
            </div>
            <div class=" f-left  width-35per">
                <p class="font-14 margin-bottom-5"><strong class="info_full_name">Carolyn A. Mendoza <span class="red-color"> New!</span></strong></p>
                <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span></strong><info class="info_user_level">Agent</info></p>
            </div>

            <div class=" f-left  width-30per">
                <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span></strong><info class="info_username">Agent</info></p>
                <p class="font-14 margin-bottom-5"><strong><span class="red-color">Call Center: </span> </strong><info class="info_callcenter_name">Agent</info></p>

            </div>

            <div class=" f-left">
                <p class="font-16 margin-bottom-5"><strong><span class="red-color status">OFFLINE</span></strong></p>
                <!-- <p class="font-14 margin-bottom-5"><strong><span class="red-color">Last Online: </span>- | -</strong></p> -->
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="content-container hidden viewable agent-detailed">
        <div>

            <h2 class="f-left info_full_name">Carolyn A. Mendoza</h2>
            <h2 class="f-right red-color status">OFFLINE</h2>
            <div class="clear  margin-bottom-10"></div>

            <div class="f-left margin-right-30  margin-left-10 sphere-big">
                <h1 class="info_initials_inside">CM</h1>
            </div>

            <div class=" f-left  width-25per">
                <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username:</span></strong></p>
                <p class="font-14 margin-bottom-5"><strong><span class="red-color">Email Address: </span> </strong> </p>
                <!-- <p class="font-14 margin-bottom-5"><strong><span class="red-color">Password: </span> </strong></p> -->
                <p class="font-14 margin-bottom-5"><strong><span class="red-color">Processed Order (Today): </span> </strong></p>
            </div>


            <div class=" f-left  width-35per">
                <p class="font-14 margin-bottom-5"><strong class="info_username">phub_cmendoza02</strong></p>
                <p class="font-14 margin-bottom-5"><strong class="info_email_address">cmendoza@gmail.com </strong> </p>
                <!-- <p class="font-14 margin-bottom-5"><strong>********** <a href="#" class="margin-left-10"><i class="fa fa-eye"></i>Show Password</a> </strong></p> -->
                <p class="font-14 margin-bottom-5"><strong class="info_order_processed_count"> 0</strong> </p>
            </div>


            <div class=" f-left width-15per">
                <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span></strong></p>
                <p class="font-14 margin-bottom-5"><strong><span class="red-color">Call Center: </span></strong></p>
                <!-- <p class="font-14 margin-bottom-5"><strong><span class="red-color">Last Online: </span></strong></p> -->

            </div>

            <div class=" f-left  ">
                <p class="font-14 margin-bottom-5"><strong class="info_user_level">Agent</strong></p>
                <p class="font-14 margin-bottom-5"><strong class="info_callcenter_name">PHUB</strong></p>
                <!-- <p class="font-14 margin-bottom-5"><strong>Not Available</strong></p> -->

            </div>

            <div class="clear"></div>

            <div class="f-right  margin-top-10">
                <button type="button" class="btn btn-dark margin-right-10 archive_user">Archive User</button>
                <button type="button" class="btn btn-dark margin-right-10 edit_user">Edit User Information</button>
               <!-- <button type="button" class="btn btn-dark notify_agent">Notify Offline Agent</button>-->
            </div>
            <div class="clear"></div>

            <div class="bggray-dark  margin-top-20  padding-all-20">
                <p class="f-left"><strong>Assigned Area Coverage</strong></p>
                <p class="f-right"><strong><info class="info_provinces">4</info> Area(s)</strong></p>
                <div class="clear"></div>

                <div class="margin-top-10 province-container">
                    <!--<button type="button" class="btn btn-dark  margin-right-10" disabled="">Metro Manila</button>
                    <button type="button" class="btn btn-dark  margin-right-10" disabled="">Bulacan</button>
                    <button type="button" class="btn btn-dark  margin-right-10" disabled="">Batangas</button>
                    <button type="button" class="btn btn-dark  margin-right-10" disabled="">Cagayan</button>-->
                </div>

            </div>

        </div>

    </div>
</section>

<section class="store-list template">
    <div class="content-container viewable store-overview">
        <div>
            <div class="width-30per f-left">
                <p class="font-14 margin-bottom-5"><strong data-label="name">MM Harbour Square</strong></p>
                <p class="font-14 margin-bottom-5"><strong><span class="red-color">Store Code:</span> </strong><info data-label="code">JB1141</info></p>
                <p class="font-14 margin-bottom-5"><strong><span class="red-color">Store ID:</span></strong><info data-label="id">12EDF45</info></p>
            </div>

            <div class="width-40per f-left">
                <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Area: </span><info data-label="province">NCR</info></strong></p>
                <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Store Hours: </span></strong><info class="serving_time_start">06:00 AM</info> - <info class="serving_time_end">10:00 PM</info></p>
                <p class=" margin-left-10 margin-bottom-5"><strong><span class="red-color">Delivery Hours: </span></strong ><info class="serving_time_start">06:00 AM</info> - <info class="serving_time_end">10:00 PM</info></p>
            </div>

            <div class="width-30per f-left margin-top-20 ">
                <p class="font-20 margin-bottom-5 red-color"><strong class="status">OFFLINE</strong></p>
                <p><strong><span class="red-color">Last Online: </span></strong><info class="last_online"> May 17, 2015 | 9:58 PM</info></p>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="content-container hidden viewable store-detailed padding-all-10">
        <div class="f-left">
            <h3><info data-label="name">Pasig Simbahan</info> | <info data-label="code">JB1180</info></h3>
            <p class="font-bold"><span class="red-color">Store ID:</span> <info data-label="id">13SDE21</info></p>
        </div>
        <div class="f-right">
            <h3 class="red-color status">OFFLINE</h3>
            <p class="font-bold"><span class="red-color">Last Online:</span><info class="last_online"> Not Available</info></p>
        </div>
        <div class="clear"></div>
        <hr>

        <!-- for table -->
        <div class="f-left width-50per">
            <table border="0" class="width-100per">
                <tbody>
                    <tr>
                        <td valign="top" class="red-color font-bold padding-all-5">Contact Number:</td>
                        <td align="right" class="padding-all-5">
                            <p class="margin-bottom-10 contact_number"><info class="contact_number">(02) 257-68-95 <i class="fa fa-phone"></i></info></p>
                            <!-- <p>(+63) 915-516-6153 <i class="fa fa-mobile font-20 margin-right-5"></i>Globe</p> -->
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" class="padding-all-5 red-color font-bold">
                            Email Address:
                        </td>
                        <td align="right" class="padding-all-5">
                            <p class="margin-bottom-5 email_address">jb.pasig.simbahan@jfc.com</p>
                        </td>
                    </tr>
                    <tr>
                        <td class="padding-all-5 red-color font-bold">Area:</td>
                        <td align="right" class="padding-all-5 "><info data-label="province">NCR</info></td>
                    </tr>
                    <tr>
                        <td class="padding-all-5 red-color font-bold">Store Hours:</td>
                        <td align="right" class="padding-all-5"><info class="serving_time_start">06:00 AM</info> - <info class="serving_time_end">10:00 PM</info></td>
                    </tr>
                    <tr>
                        <td class="padding-all-5 red-color font-bold">Delivery Hours:</td>
                        <td align="right" class="padding-all-5"><info class="serving_time_start">06:00 AM</info> - <info class="serving_time_end">10:00 PM</info></td>
                    </tr>
                    <tr>
                        <td class="padding-all-5 red-color font-bold">Max Order Threshold:</td>
                        <td align="right" class="padding-all-5"><info class="order_limit">10</info> Orders</td>
                    </tr>



                </tbody>
            </table>

        </div>
        <!-- for container -->
        <div class="f-left width-50per">
            <label>Store Manager Accounts:</label>
            <section class="store-user-container">
                <section class="store-user template">
                 <div class="padding-all-10 bggray-middark margin-bottom-10">
                <table class="width-100per margin-top-5" border="0">
                    <tbody>
                        <tr>
                            <td class="red-color font-bold padding-bottom-10 padding-left-5">Username:</td>
                            <td class="padding-bottom-10 "><p class="margin-left-50 padding-left-30 username">jbpasig1</p></td>
                        </tr>
                        <tr>
                            <td class="red-color font-bold padding-bottom-10 padding-left-5">Password:</td>
                            <td class="padding-bottom-10">
                                <p class="margin-left-50 padding-left-30"> ***********
                                    <!-- <a href="#"><i class="fa fa-eye disabled"></i> Show Password</a> -->
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table>

                </div>
            </section>
            </section>
            
           

           <!--  <div class="padding-all-10 bggray-dark margin-bottom-10">
                <table class="width-100per margin-top-5" border="0">
                    <tbody>
                        <tr>
                            <td class="red-color font-bold padding-bottom-10 padding-left-5">Username:</td>
                            <td class="padding-bottom-10 "><p class="margin-left-50 padding-left-30">jbpasig2</p></td>
                        </tr>
                        <tr>
                            <td class="red-color font-bold padding-bottom-10 padding-left-5">Password:</td>
                            <td class="padding-bottom-10">
                                <p class="margin-left-50 padding-left-30"> ***********
                                    <a href="#"><i class="fa fa-eye disabled"></i> Show Password</a>
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div> -->
           <!--  <a href="#" class="f-right">+ Add New Store Manager Account</a> -->
            <div class="clear"></div>


        </div>
        <div class="clear"></div>

        <!-- buttons -->
        <div class="f-right">
            <button type="button" class="btn btn-dark modal-trigger margin-right-10 archive_store" >Archive Store</button>
            <button type="button" class="btn btn-dark modal-trigger margin-right-10" modal-target="flag-closure" disabled>Flag for Closure</button>
            <button type="button" class="btn btn-dark margin-right-10 edit_store">Edit Information</button>
            <!--<button type="button" class="btn btn-dark" disabled>Notify Offline Store</button>-->
        </div>
        <div class="clear"></div>
    </div>
</section>

<section class="section-order template">
    <div class="order-overview content-container viewable ">
        <div>
            <div class="width-35per f-left">
                <p class="font-14 margin-bottom-5"><strong>Order ID: <info class="data-order-id">954861</info></strong></p>
                <p class="font-14 margin-bottom-5"><strong><span class="red-color">Name: </span></strong><info class="data-full-name"></info></p>
                <p class="font-14"><strong><span class="red-color">Contact Num:</span></strong><info class="data-contact-number">(+63) 915-516-6153 </info><!--<i class="fa fa-mobile"></i> Globe--></p>
            </div>

            <div class="width-40per / f-left / margin-top-25">
                <p class="font-14 margin-bottom-5"><strong><span class="red-color">Date/Time Accepted: </span></strong><info class="data-order-date">May 18, 2015 | 11:42:02</info></p>
                <!--<p class="font-14 margin-bottom-5"><strong><span class="red-color">Date/Time Accepted: </span></strong>May 18, 2015 | 11:42:02</p>-->
            </div>

            <div class="width-25per f-right">
                <label class="font-20 / gray-color / margin-left-10">Hitrate</label>
                <h1 class="no-margin-all data-hit-rate"></h1>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="order-detailed content-container hidden">
        <div>
            <div class="f-left margin-top-15">
                <p class="font-20"><strong>Order ID: <info class="data-order-id">954861</info></strong></p>
            </div>

            <div class="width-25per f-right / margin-bottom-10">
                <label class="font-20 / gray-color / margin-left-15">Hitrate</label>
                <h1 class="no-margin-all data-hit-rate"></h1>
            </div>



            <div class="clear"></div>
            <hr>

            <div class="data-container split">

                <!-- clients information -->
                <div class="margin-top-20">
                    <p class="f-left red-color font-12 margin-bottom-5"><strong>Name: </strong></p>
                    <p class="f-right font-12"><info class="data-full-name">Jonathan R. Ornido</info></p>
                    <div class="clear"></div>

                    <p class="f-left red-color font-12 margin-bottom-5"><strong>Contact Number: </strong></p>
                    <p class="f-right font-12"><info class="data-contact-number">(+63) 915-516-6153 </info></p>
                    <div class="clear"></div>

                    <p class="f-left red-color font-12 margin-bottom-5"><strong>Transaction Time:</strong></p>
                    <p class="f-right font-12"><info class="data-order-date">May 18, 2015 | 11:42:02</info>                                                                                  </p>
                    <div class="clear"></div>
                </div>

                <!-- delivery address -->
                <div>
                    <label class="margin-top-10 margin-bottom-10 ">Delivery Address:</label>
                    <div class="bggray-light padding-all-5 font-14 small-curved-border">
                        <div class="display-inline-mid padding-left-20">
                            <img src="<?php echo assets_url() ?>/images/work-icon.png" alt="work icon">
                            <p class="font-12 text-center no-margin-all"><strong>HOME</strong></p>
                        </div>
                        <div class="display-inline-mid f-right width-85per">
                            <!--<p class="no-margin-all"><strong class="data-address-label">Address: </strong></p>-->
                            <p class="no-margin-all data-full-address">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR</p>
                            <p class="no-margin-all gray-color data-land-mark"></p>
                        </div>
                    </div>
                </div>

                <!-- cards -->
                <label class="margin-top-15" data-label="cards">CARDS:</label>

                <div class="happy_plus_cards_container">
                    <div class="bggray-light margin-bottom-10 padding-all-5 font-14 small-curved-border happy_plus_cards template">
                        <div class="display-inline-mid margin-left-10">
                            <img class="thumb " src="<?php echo base_url() ?>/assets/images/happy-plus.jpg" alt="happy-plus">
                        </div>
                        <div class="display-inline-mid margin-left-10 divider padding-left-10">
                            <p data-label="card_number" class="no-margin-all">008-248-369-154</p>
                            <span class="red-color"><strong>Exp. Date:</strong></span>
                            <p data-label="card_expiration" style="display: inline;"> September 20, 2016</p>
                        </div>
                        <div class="display-inline-mid text-center margin-left-10">
                            <!-- <a href="#">Show Card<br>History</a> -->
                        </div>
                    </div>
                </div>


                <div class="margin-top-10">
                    <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Payment Method: </strong></p>
                    <p class="f-right / font-12">Cash</p>
                    <div class="clear"></div>

                    <p class="f-left / font-12 / red-color / margin-bottom-5"><strong>Change For: </strong></p>
                    <p class="f-right / font-12"><info class="change_for">500.00 PHP</info></p>
                    <div class="clear"></div>

                    <p class="f-left font-12 red-color  margin-bottom-5"><strong>Remarks: </strong></p>
                    <p class="f-right font-12 gray-color">No Remarks</p>
                    <div class="clear"></div>



                </div>
            </div>

            <div class="data-container split margin-left-15">

                <label class="margin-top-15">order: </label>

                <div class="small-curved-border">
                    <table class="font-14" style="width: 100%; display:block;">
                        <thead class="bggray-dark">
                            <tr>
                                <th class="padding-all-10" style="width:10%;">Quantity</th>
                                <th class="padding-all-10" style="width:30%;">Product</th>
                                <th class="padding-all-10" style="width:10%;">Price</th>
                                <th class="padding-all-10" style="width:15%;">Subtotal</th>
                            </tr>
                        </thead>
                        <tbody class="bggray-light" data-transaction-orders="detailed_orders" style="display:block; max-height: 300px; overflow-y: auto; width:100%;">
                            <tr data-template-transaction-for="order" class="template">
                                <td style="width:15%;" class="padding-all-10 text-center" data-label="quantity"></td>
                                <td style="width:30%;" class="padding-all-10" data-label="product"></td>
                                <td style="width:10%;" class="padding-all-10" data-label="price"></td>
                                <td style="width:15%;" class="padding-all-10" data-label="sub_total"></td>
                            </tr>
                        </tbody>
                        <tbody class="bggray-light">
                            <tr class="bggray-middark">
                                <td colspan="3" style="width:40%;" class="padding-all-10 text-left">Total Cost</td>
                                <td colspan="2" style="width:60%;" class="padding-all-10 text-right" data-label="total_cost">1852.40 PHP</td>
                            </tr>
                            <tr class="bggray-middark">
                                <td colspan="2" style="width:50%;" class="padding-all-10 text-left">Added VAT</td>
                                <td colspan="3" style="width:50%;" class="padding-all-10 text-right" data-label="added_vat">222.29 PHP</td>
                            </tr>
                            <tr class="bggray-dark">
                                <td colspan="2" style="width:40%;" class="padding-all-10 text-left font-16"><strong>Total Bill</strong>
                                </td>
                                <td colspan="3" style="width:60%;" class="padding-all-10 text-right font-14">
                                    <strong data-label="total_bill">1481.92 PHP</strong></td>
                            </tr>
                            <tr class="bggray-middark">
                                <td colspan="2" style="width:50%;" class="padding-all-10 text-left">Delivery Charge</td>
                                <td colspan="3" style="width:50%;" class="padding-all-10 text-right" data-label="delivery_charge">168.40 PHP</td>
                            </tr>
                        </tbody>
                    </table>

                </div>

            </div>
            <div class="margin-top-20">


                <button type="button" class="btn btn-dark f-right margin-right-10 modal-trigger transaction-logs" modal-target="order-logs">Logs</button>

                <div class="clear"></div>
            </div>
        </div>
    </div>
</section>

