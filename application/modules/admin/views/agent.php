<script type="text/javascript"></script>
<style> /*temporary will be transferred to cutomer custom css*/

    div.template {
        display: none;
    }

    tr.template {
        display: none;
    }

    tr.unavailable {
        border-bottom:1pt solid red;
        border-top:1pt solid red;
        color: red;
    }

    div.success-msg {
        background-color: #00cc00;
        color: #fff;
        font-size: 18px;
        padding: 5px;
        text-align: center;
        width: 100%;
    }

    div.address-success-msg {
        background-color: #00cc00;
        color: #fff;
        font-size: 14px;
        padding: 5px;
        text-align: center;
        width: 100%;
    }

    p.info_address_complete {
        max-width: 225px;
    }

    p.default_address_complete {
        max-width: 225px;
    }

    p.address_complete {
        max-width: 225px;
    }

    p.error_messages {
        text-align: left;
        margin-left: 25px;
        font-size: 12px !important;
    }

    .update-customer-error-msg {
        text-align: left;
        font-size: 12px !important;
    }

    .customer_secondary_address .customer_address:hover {
        background-color: #efefed !important;
    }

    .found-retail {
        display: none;
    }

    .show_map {
        cursor: pointer;
    }

    .paganation {
        width: 100%;
        margin: 10px 0;
    }

    div.has-error {
        border: 1px solid red;
        border-radius: 4px;
    }

    /*pagination*/
    #paginations {
        padding: 0;
        margin: 0px 0px 15px 0px;
    }

    #paginations > li {
        list-style: none;
        color: white;
        float: left;
        background: #888888;
        margin-right: 2px;
        padding: 10px 15px;
        transition: background .5s linear;
        text-align: center;
        font-size: 15px;

    }

    #paginations > li:hover {
        cursor: pointer;
        background: #6D0D03;

    }

    #paginations > li.page-active {
        background: #9b3126;
    }

    .page-none {
        display: none;
    }

    .records {
        padding: 10px;
        background: #DFE4DF;
        border-bottom: 1px solid #ccc;
    }

    .records:first-child {
        margin-top: 40px;
    }

    .template {
        display: none;
    }

    header-group.header, tr.store_route {
        cursor: pointer;
    }

    button-group {
        width: 0;
        height: 0;
        clear: both;
    }

    a {
        cursor: pointer;
    }

    div.store-msg-notify {
        padding-top: 2px;
        width: 20px;
        height: 20px;
        font-size: 12px;
        border-radius: 50%;
        background-color: #D65D51;
        color: #FFF;
        text-align: center;
        z-index: 999;
        display: inline;
        float: right;
    }

    table.table_store_messenger_container td.store-selected {
        background-color: #FBD8A1;
    }

    table.table_store_messenger_container tr {
        cursor : pointer;
    }
    .datepicker-days td.day.disabled{
        color: #999999;
    }
    .datepicker-months span.month.disabled{
        color: #999999;
    }
    .datepicker-years span.year.disabled{
        color: #999999;
    }
    .datepicker-days td.day.today{
        font-weight: bold;
    }

</style>

<!--HEADERS-->
<!--Add New Customer, Search Customer, Order Process Header-->
<section id="add_new_customer_header" class="container-fluid add_new_customer headers" section-style="top-panel">

    <div class="row header-container">
        <div class="contents ">
            <h1 class="f-left">Search Customer</h1>

            <div class="f-right">
                <button type="button" class="btn btn-dark margin-top-20 margin-right-10 add_new_customer">Add New Customer</button>
                <button type="button" class="btn btn-dark margin-top-20 margin-right-10 product_list">Skip to Cart</button>
                <!-- <button type="button" class="btn btn-dark margin-top-20 margin-right-10">FAQ</button> -->
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <!-- search -->
    <div class="row">
        <div class="contents">
            <div class="f-left margin-top-20 margin-right-20">
                <label class="">SEARCH</label>
                <br/>


                <input type="text" class="search" id='search_customer'/>

                <div class="result-list">
                </div>

            </div>
            <div class="f-left margin-top-20 margin-right-20">
                <label>SEARCH BY:</label>
                <br/>

                <div class="select">
                    <select name="search_by">
                        <option value="Contact Number" selected>Contact Number</option>
                        <option value="Customer Name">Customer Name</option>
                    </select>
                </div>
            </div>
            <div id="search_customer_select_province" class="f-left margin-top-20">
                <label>PROVINCE:</label>
                <br/>

                <div class="select margin-right-20">
                    <select name="province">
                        <option value="">Select Province</option>
                    </select>
                </div>
                <button type="button" class="btn btn-dark search_button"><i class="fa fa-spinner fa-pulse hidden"></i>Search
                </button>
            </div>
        </div>
    </div>

    <!-- add new customer -->
    <div class="row">
        <div class="contents margin-top-20 line text-right">
            <span class="white-space"> </span>
            <p class="f-right bggray-white font-14 black-color margin-top-5 count_search_result">
                <strong>No Search Result</strong></p>
        </div>
    </div>
</section>

<!--Customer List Header, Update Customer Header-->
<section id="customer_list_header" class="container-fluid hidden customer_list headers" section-style="top-panel">

    <!-- search order -->
    <div class="row header-container">
        <div class="contents">
            <h1 class="f-left">Customer List</h1>

            <div class="f-right margin-top-20">
                <button class="btn btn-dark margin-right-10 add_new_customer">Add New Customer</button>
                <button class="btn btn-dark margin-right-10 download_customer">Download Customer List</button>
                <!-- <button class="btn btn-dark ">FAQ</button> -->
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="row">
        <div class="contents margin-top-20">
            <!-- search -->
            <div class="f-left">
                <label class="margin-bottom-5">search:</label><br>
                <input id="search_customer_list" class="small f-left" type="text">

                <div class="result-list" style="margin-top:30px;">
                </div>
            </div>
            <!-- search by -->
            <div id="customer_list_search_by_filter" class="f-left margin-left-20">
                <label class="margin-bottom-5">search by:</label><br>

                <div class="select" name="search_by">
                    <div class="frm-custom-dropdown">
                        <div class="frm-custom-dropdown-txt">
                            <input type="text" class="dd-txt" name="search_by" id="customer_list_search_by"  value="Contact Number"></div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option">
                            <div class="option" data-value="Contact Number">Contact Number</div>
                            <div class="option" data-value="Customer Name">Customer Name</div>
                            <div class="option" data-value="Address">Address</div>
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                        <option value="Contact Number">Contact Number</option>
                    </select>
                </div>
            </div>
            <!-- province -->
            <div id="customer_list_search_by_province" class="f-left margin-left-20">
                <label class="margin-bottom-5">Province:</label><br>

                <div class="select">
                    <div class="frm-custom-dropdown">
                        <div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" value="All Province"></div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option">
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                        <option value="All Province">All Province</option>
                    </select>
                </div>
            </div>

            <!-- last ordered -->
            <div id="customer_list_search_by_last_ordered" class="f-left margin-left-20">
                <label class="margin-bottom-5">Last Ordered:</label><br>

                <div class="select">
                    <div class="frm-custom-dropdown">
                        <div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt"  value="Today"></div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option">
                            <div class="option" data-value="Today">Today</div>
                            <div class="option" data-value="This_Week">This Week</div>
                            <div class="option" data-value="This_Month">This Month</div>
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                        <option value="">Today</option>
                    </select>
                </div>
            </div>


            <!-- button -->
            <button class="f-left btn btn-dark margin-top-20 margin-left-20 search_button">
                <i class="fa fa-spinner fa-pulse hidden"></i> Search
            </button>
            <div class="clear"></div>
        </div>
    </div>
    <div class="row margin-top-20">
        <div id="customer_list_search_by_behavior" class="contents line">
            <div class="select larger">
                <div class="frm-custom-dropdown">
                    <div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" value="Show All Customer"></div>
                    <div class="frm-custom-icon"></div>
                    <div class="frm-custom-dropdown-option">
                        <div class="option" data-value="Show All Customer">Show All Customer</div>
                    </div>
                </div>
                <select class="frm-custom-dropdown-origin" style="display: none;">
                    <option value="">Show All Customer</option>
                </select>
            </div>
            <span class="white-space"></span>


            <span class="white-space"></span>

            <div class="f-right bggray-white">
                <p class="f-left font-12 padding-left-10 padding-top-5">
                    <strong>Sort By:</strong>
                    <input type="hidden" id="customer_list_orderby"/>
                    <input type="hidden" id="customer_list_sorting"/>
                </p>
                <p class="f-left font-12 padding-left-5 padding-top-5">
                    <a class="customer_list_sort customer_list_sortby_name" data-value="asc" href="javascript:void(0)">
                        <strong>Customer Name </strong><i class="fa"></i>
                    </a>
                </p>
                <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                <p class="f-left font-12 padding-left-5 padding-top-5">
                    <a class="customer_list_sort customer_list_sortby_behavior" data-value="asc" href="javascript:void(0)">
                        <strong>Behavior </strong><i class="fa"></i>
                    </a>
                </p>
                <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                <p class="f-left font-12 padding-top-5">
                    <a class="customer_list_sort customer_list_sortby_recent_ordered" data-value="asc" href="javascript:void(0)">
                        <strong>Recent Ordered </strong><i class="fa"></i>
                        <!--img src="<?php echo base_url() ?>/assets/images/ui/sort-top-arrow.png"-->
                    </a>
                </p>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</section>



<!--END HEADERS-->


<!--CONTENT START-->
<section class="container-fluid content" section-style="content-panel">

    <div class="row add_agent">


        <div class="content-container">
            <div class="error-msg  light-yellow-bg">
                <img src="../assets/images/warn.svg" alt="warning logo" class="f-left  margin-right-10" />
                <p class="f-left  font-16  margin-top-5">Carolyn Mendoza has been moved to the archives</p>
                <p class="f-right  margin-top-5 "><a href="#" class="white-color  underline  ">UNDO</a>
                <div class="clear"></div>
            </div>




        </div>

        <!-- sample-1-->
        <div class="content-container viewable">
            <div>
                <div class="f-left  sphere-small  margin-right-20">
                    <p class="font-20">ML</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Carolyn A. Mendoza <span class="red-color"> New!</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span></strong> Agent</p>
                </div>

                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span></strong>phub_cmendoza02</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Call Center: </span> </strong>PHUB</p>

                </div>

                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">OFFLINE</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Last Online: </span>- | -</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- sample 2 -->
        <div class="content-container viewable">
            <div>
                <div class="f-left  sphere-small  margin-right-20">
                    <p class="font-20">LM</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Lance Madrona</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Agent </p>
                </div>
                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span></strong>phub_lmadrona14</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Call Center: </span> </strong>PHUB</p>

                </div>

                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">OFFLINE</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Last Online: </span>05-17-16 | 3:23 AM</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- sample 3 -->
        <div class="content-container viewable">
            <div>
                <div class="f-left  sphere-small  margin-right-20">
                    <p class="font-20">MM</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Maria Mamita</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Agent</p>
                </div>

                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span></strong>phub_mmammita14</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Call Center: </span> </strong>PHUB</p>

                </div>
                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">OFFLINE</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Last Online: </span>05-17-16 | 3:23 AM</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- sample 4 -->
        <div class="content-container viewable">
            <div>
                <div class="f-left  sphere-small  margin-right-20">
                    <p class="font-20">KA</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Katheleen Kaye Aliman</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Agent</p>
                </div>

                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span></strong>phub_kaliman01</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Call Center: </span> </strong>PHUB</p>

                </div>

                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong><span class="green-color">ONLINE</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Last Online: </span>05-17-16 | 3:23 AM</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- sample 4 -->
        <div class="content-container viewable">
            <div>
                <div class="f-left  sphere-small  margin-right-20">
                    <p class="font-20">GG</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Gary Guitierez</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Agent </p>
                </div>
                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span></strong>phub_gguitierrez57</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Call Center: </span> </strong>PHUB</p>

                </div>

                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong><span class="green-color">ONLINE</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Last Online: </span>05-17-16 | 3:23 AM</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- sample 5 -->
        <div class="content-container viewable">
            <div>
                <div class="f-left  sphere-small  margin-right-20">
                    <p class="font-20">WP</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Warren Pando</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Agent</p>
                </div>

                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span></strong>phub_cmendoza02</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Call Center: </span> </strong>PHUB</p>

                </div>

                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">OFFLINE</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Last Online: </span>05-17-16 | 3:23 AM</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- sample 6 -->
        <div class="content-container viewable">
            <div>
                <div class="f-left  sphere-small  margin-right-20">
                    <p class="font-20">TL</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Thomas Lapid</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Agent</p>
                </div>

                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span></strong>phub_tlapid89</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Call Center: </span> </strong>PHUB</p>

                </div>

                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong><span class="green-color">ONLINE</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Last Online: </span>05-17-16 | 3:23 AM</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <!-- sample 7 -->
        <div class="content-container viewable">
            <div>
                <div class="f-left sphere-small  margin-right-20">
                    <p class="font-20">DL</p>
                </div>
                <div class=" f-left  width-35per">
                    <p class="font-14 margin-bottom-5"><strong>Darren Lopez</strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">User Role: </span> </strong> Agent </p>
                </div>
                <div class=" f-left  width-30per">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Username: </span></strong>phub_cmendoza02</p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Call Center: </span> </strong>PHUB</p>

                </div>

                <div class=" f-left">
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">OFFLINE</span></strong></p>
                    <p class="font-14 margin-bottom-5"><strong><span class="red-color">Last Online: </span>05-17-16 | 3:23 AM</strong></p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>



</section>
<!--CONTENT END-->


<!--Modals-->

<!--order summary modal-->
<div class="modal-container" modal-id="manual-order-summary">
    <div class="modal-body">

        <div class="modal-head ">
            <h4 class="text-left">Order Summary</h4>

            <div class="modal-close close-me"></div>
        </div>

        <!-- content -->
        <div class="modal-content">
            <!-- error message -->
            <div class="modal-error-msg manual-order-error-msg error-msg margin-bottom-10" style="font-size: 12px">
                <i class="fa fa-exclamation-triangle margin-right-10"></i>Please fill up all requred fields.
            </div>

            <!-- left content -->
            <div class="data-container display-inline-mid width-50per customer-information-container">


                <!-- contact information -->

            </div>


            <!-- right content -->
            <div class="data-container display-inline-mid width-50per margin-left-15">
                <label>Order:</label>
                <!-- item order -->
                <div class="small-curved-border cart-container">

                    <label class="margin-top-20">Change for: <span class="red-color">*</span></label>

                    <!-- <div class="price full"> -->
                        <!-- <input type="text" name="change_for" class="input_numeric"/> -->
                        <div class="select width-100per change_for">
                            <div class="frm-custom-dropdown">
                                <div class="frm-custom-dropdown-txt">
                                    <input type="text" class="dd-txt" name="change_for" class="input_numeric" value="500"></div>
                                <div class="frm-custom-icon"></div>
                                <div class="frm-custom-dropdown-option" style="display: none;">
                                    <div class="option" data-value="500">500</div>
                                    <div class="option" data-value="1000">1000</div>
                                </div>
                            </div>
                            <select id="change_for" name="change_for" class="frm-custom-dropdown-origin" style="display: none;">
                                <option value="500">500</option>
                                <option value="1000">1000</option>
                            </select>
                        </div>
                    <!-- </div> -->
                </div>
            </div>
        </div>

        <!-- button -->
        <div class="f-right margin-right-30 margin-bottom-10">
            <button type="button" class="btn btn-dark  modal-trigger confirm_order">Confirm Order</button>
            <button type="button" class="btn btn-dark close-me margin-left-15">Cancel</button>
        </div>
        <div class="clear"></div>

    </div>
</div>

<!--customer match modal-->
<div class="modal-container" modal-id="information-match">

    <div class="modal-body small">
        <div class="modal-head ">
            <p>Information Match</p>

            <div class="modal-close close-me"></div>
        </div>

        <div class="modal-content">
            <div class="col-sm-2">
                <img src="<?php echo base_url() ?>/assets/images/ui/warning.svg" alt="warning logo" class="img-responsive"/>
            </div>
            <div class="col-sm-10 ">
                <p>An existing customer have the same information. <br/>Are you sure you want proceed</p>
            </div>
            <div class="clear"></div>

            <div class="row bggray-white margin-top-10  padding-bottom-20 no-padding">
                <p class="font-18 no-margin-bottom margin-left-20 padding-top-20"><strong>Samuel O. Hernandez</strong>
                </p>

                <p class="margin-left-20"><strong>DISCOUNTS:</strong></p>

                <div class="bggray-dark padding-top-10 padding-left-10 padding-bottom-20 margin-left-20 margin-right-20">
                    <div class="f-left font-12 margin-right-20"><strong>Senior <br/>Citizen</strong></div>
                    <div class="f-left font-12">
                        <strong>Samuel O. Hernandez<br/><span class="red-color">OSCA Number: </span>0014-7845</strong>
                    </div>
                    <div class="clear"></div>
                </div>

            </div>

        </div>

        <div class="f-right margin-right-30 margin-bottom-10">
            <button type="button" class="btn btn-dark margin-right-10">Confirm &amp; Proceed to Order</button>
            <button type="button" class="btn btn-dark">Cancel</button>
        </div>
        <div class="clear"></div>
    </div>
</div>

<!--additional address modal-->
<div class="modal-container" modal-id="deliver-to-different">
    <div class="modal-body small">
        <div class="modal-head ">
            <p>Deliver to a Different Address</p>

            <div class="modal-close close-me"></div>
        </div>

        <div class="modal-content">

            <!-- error message -->
            <div class="modal-error-msg address-error-msg error-msg margin-bottom-10" style="font-size: 12px">
                <i class="fa fa-exclamation-triangle margin-right-10"></i>Please fill up all requred fields.
            </div>

            <div class="address-success-msg success-msg margin-bottom-30">
                <i class="fa fa-exclamation-triangle"></i>
            </div>

            <!-- address -->
            <form id="add_address">
                <input type="hidden" name="customer_id"/>
                <input type="hidden" name="address_id"/>
                <table class="margin-bottom-15 margin-left-15">
                    <tr>
                        <td><label>Address Type: <!-- <span class="red-color">*</span> --></label></td>
                        <td><label>Address Label: <!-- <span class="red-color">*</span> --></label></td>
                    </tr>
                    <tr>
                        <td>
                            <div class="select margin-right-50">
                                <select name="address_type" labelinput="Address Type">
                                    <option value="Home">Home</option>
                                    <option value="Work">Work</option>
                                    <option value="Business">Business</option>
                                </select>
                            </div>
                        </td>
                        <td>
                            <input type="text" name="address_label" class="normal" labelinput="Address Label">
                        </td>
                    </tr>
                </table>
                <hr/>
                <!-- province -->
                <table class="margin-top-15 margin-left-15">
                    <tr>
                        <td><label>Province: <span class="red-color">*</span></label></td>
                        <td><label>City: <span class="red-color">*</span></label></td>
                    </tr>
                    <tr>
                        <td>
                            <div id="modal_select_province">
                                <div class="select margin-right-50">
                                    <select name="province" labelinput="Province" datavalid="required">
                                        <option value="">Select Province</option>
                                    </select>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div id="modal_select_city">
                                <div class="select">
                                    <select name="city" labelinput="City" datavalid="required">
                                        <option value="">Select City</option>
                                    </select>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>

                <div class="margin-left-15	">
                    <label class="margin-top-10">Landmark:</label>
                    <input type="text" class="xlarge" name="landmark">
                </div>

                <!-- 2 column table -->
                <table class="margin-top-10 margin-left-15">
                    <tr>
                        <td><label>House Number: </label></td>
                        <td><label>Building: </label></td>
                    </tr>
                    <tr>
                        <td><input type="text" name="house_number" class="normal margin-right-30"></td>
                        <td>
                            <input type="text" id="modal_search_building" name="building" class="input_modal_autocomplete normal">

                            <div class="result-list"></div>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="margin-top-10">Unit: </label></td>
                        <td><label>Floor: </label></td>
                    </tr>
                    <tr>
                        <td><input type="text" name="unit" class="normal"></td>
                        <td><input type="text" name="floor" class="normal"></td>
                    </tr>
                </table>

                <!-- single column table -->
                <table class="margin-left-15">
                    <tr>
                        <td><label class="margin-top-10">Street:</label></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="modal_search_street" class="input_modal_autocomplete xlarge" name="street">

                            <div class="result-list"></div>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="margin-top-10">Secondary Street:</label></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="modal_search_second_street" class="input_modal_autocomplete xlarge" name="second_street">

                            <div class="result-list"></div>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="margin-top-10">Subdivision:</label></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="modal_search_subdivision" class="input_modal_autocomplete xlarge" name="subdivision">

                            <div class="result-list"></div>
                        </td>
                    </tr>
                    <tr>
                        <td><label class="margin-top-10">Barangay:</label></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="text" id="modal_search_barangay" class="input_modal_autocomplete xlarge" name="barangay">

                            <div class="result-list"></div>
                        </td>
                    </tr>
                </table>
                <div class="form-group margin-top-10 margin-left-15">
                    <input class="chck-box" id="card-number" type="checkbox" name="is_default">
                    <label class="chck-lbl" for="card-number">Set this address as the default for this customer</label>
                </div>
            </form>
        </div>

        <div class="f-right margin-right-30 margin-bottom-10">
            <button type="button" class="btn btn-dark margin-right-10 add_address_button">
                <i class="fa fa-spinner fa-pulse hidden"></i> Confirm
            </button>
            <button type="button" class="btn btn-dark close-me">Cancel</button>
        </div>
        <div class="clear"></div>
    </div>
</div>

<!-- order complete modal-->
<div class="modal-container" modal-id="manual-order-complete">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left order_type">Manual Order Complete</h4>

            <div class="modal-close close-me"></div>
        </div>

        <!-- content -->
        <div class="modal-content">
            <table>
                <tbody>
                    <tr>
                        <td><i class="fa fa-check-circle green-color font-50 margin-right-10 margin-bottom-20 "></i>
                        </td>
                        <td><p class="font-14">
                                <strong class="customer-full-name">Mark Anthony D. Dulay's</strong> Order has been successfully processed!
                            </p>
                            <p class="font-14">
                                <strong>Order ID : </strong><strong class="customer-order-id"></strong>
                            </p></td>
                    </tr>
                </tbody>
            </table>

            <hr>
            <div class="text-left">
                <label class="margin-top-10">How do you feel about this customer?</label>
                <br>

                <div class="select xlarge">
                    <div class="frm-custom-dropdown">
                        <div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt"></div>
                        <div class="frm-custom-icon"></div>
                        <div class="frm-custom-dropdown-option">
                            <div class="option" data-value="easy-to-handle">This Customer is easy to handle</div>
                            <div class="option" data-value="difficult-to-handle">This Customer is difficult to handle</div>
                        </div>
                    </div>
                    <select class="frm-custom-dropdown-origin" style="display: none;">
                        <option value="easy-to-handle">This Customer is easy to handle</option>
                        <option value="difficult-to-handle">This Customer is difficult to handle</option>
                    </select>
                </div>
            </div>
        </div>
        <!-- button -->
        <div class="f-right margin-right-20 margin-bottom-10">
            <button type="button" class="btn btn-dark margin-right-10 close-me modal-close confirm_behavior_modal">Confirm</button>
            <button type="button" class="btn btn-dark close-me modal-close back_to_customer_search confirm_behavior_modal">Back to Customer Search</button>
        </div>
        <div class="clear"></div>
    </div>
</div>
<!-- end order complete modal-->

<!--reroute order-->
<div class="modal-container" modal-id="reroute-order">

    <!-- modal body -->
    <div class="modal-body small">

        <!-- modal head -->
        <div class="modal-head ">
            <h4 class="text-left">Re-route Order</h4>

            <div class="modal-close close-me"></div>
        </div>

<!--         <div class="bggray-light">
            <div class="text-left margin-left-20 margin-right-20">
                <label class="margin-top-10">Re-route to nearest available store: </label>

                <div class="bggray-dark padding-all-20 form-group">
                    <div class="f-left">
                        <p class="font-12 "><strong>MM Ortigas Roosevelt | JB0444</strong></p>
                        <p class="font-12 "><strong><span class="red-color">Delivery Time:</span></strong> 20 Minutes
                        </p>
                    </div>
                    <div class="f-right margin-top-10">
                        <input id="option1" type="radio" class="radio-box" name="option">
                        <label class="radio-lbl " for="option1"></label>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="bggray-dark padding-all-20 margin-top-10 margin-bottom-10 form-group">
                    <div class="f-left">
                        <p class="font-12 "><strong>MM Pasig Kapitolyo | JB1124</strong></p>
                        <p class="font-12 "><strong><span class="red-color">Delivery Time:</span></strong> 20 Minutes
                        </p>
                    </div>
                    <div class="f-right margin-top-10">
                        <input id="option2" type="radio" class="radio-box" name="option">
                        <label class="radio-lbl " for="option2"></label>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            

            <div>
                <div class="contents line">
                    <label class=" font-14 padding-all-10 bggray-light">OR</label>
                </div>
            </div>
        </div> -->


        <!-- modal content -->
        <div class="modal-content no-margin-all">
            <div class="bggray-dark padding-bottom-20">
                <label class="margin-top-10 margin-left-10">Search Store:</label>
                <br>
                <input type="text" class="xlarge margin-left-10" name="search_store" style="margin-left: 5px;"/>

                <div class="result-list route-store hidden">

                </div>

                <input type="hidden" name="reoute_store_id"/>


                <div class="margin-top-10 margin-left-10 margin-right-20">
                    <div class="div-inside" style="margin-left:6px">
                        <table class="table-inside table_store_container">
                            <thead>
                                <tr>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody class="content">
                                <tr>
                                    <td><p class="font-12">MM Shopwise Sucat | JB0749</p></td>
                                </tr>
                                <tr>
                                    <td><p class="font-12">MM Alabang Madrigal | JB0749</p></td>
                                </tr>
                                <tr>
                                    <td><p class="font-12">MM New MCU | JB0237</p></td>
                                </tr>
                                <tr>
                                    <td><p class="font-12 light-red-color">MM Muntinlupa Bayan | JB0749</p></td>
                                </tr>
                                <tr>
                                    <td class="selected">
                                        <p class="font-12  f-left">MM SM Ayala | JB0749</p>
                                        <i class="fa fa-check font-18 margin-right-10 margin-top-5 f-right green-color"></i>

                                        <div class="clear"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td><p class="font-12 light-red-color">MM Ortigas Roosevelt | JB0749</p></td>
                                </tr>
                                <tr>
                                    <td><p class="font-12">MM Libertad Commercial | JB0749</p></td>
                                </tr>
                                <tr>
                                    <td><p class="font-12">MM Libertad Commercial | JB0749</p></td>
                                </tr>
                                <tr>
                                    <td><p class="font-12">MM Libertad Commercial | JB0749</p></td>
                                </tr>
                                <tr>
                                    <td><p class="font-12">MM Libertad Commercial | JB0749</p></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


        <hr>

        <div class="bggray-light">
            <label class="f-left margin-left-20 margin-top-20">Re-route schedule:</label>

            <div class="clear"></div>

            <div class="form-group f-left margin-left-20 margin-top-10">
                <input id="reroute1" type="radio" class="radio-box" name="reroute">
                <label class="radio-lbl margin-right-10 " for="reroute1">Scheduled Re-routing</label>
                <input id="reroute2" type="radio" class="radio-box" name="reroute" checked>
                <label class="radio-lbl margin-right-10" for="reroute2">One-Time Re-routing</label>
            </div>

            <div class="clear"></div>



            <section class="route-schedule" style="display:none;">
                <label class="f-left margin-left-20">Schedule Expiry:</label>

                <div class="clear"></div>
                <div class="date-picker f-left margin-left-20 margin-bottom-10">
                    <input type="text">
                    <span class="fa fa-calendar text-center red-color"></span>
                </div>

                <div class="date-pickertime f-left margin-left-20">
                    <input type="text">
                    <span class="fa fa-clock-o text-center red-color"></span>
                </div>
            </section>


            <div class="clear"></div>
        </div>

        <div class="f-right margin-right-40 margin-bottom-10 margin-top-20">
            <button type="button" class="btn btn-dark margin-right-10 confirm_reroute" disabled>Send Order</button>
            <button type="button" class="btn btn-dark close-me">Cancel</button>
        </div>
        <div class="clear"></div>

    </div>
</div>

<!-- modal  MANUAL RELAY  -->
<div class="modal-container " modal-id="manual-relay">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Manually Relay</h4>

            <div class="modal-close close-me"></div>
        </div>

        <div class="modal-content no-margin-all">

            <img src="<?php echo assets_url() ?>/images/warning.svg" alt="warning message" class="width-20per f-left margin-bottom-20">
            <p class="font-14  f-left margin-top-20 margin-left-10	">Are you sure you want to manually relay
                <br/><strong>
                    <info class="customer_name">Jonathan R. Ornido's</info>
                </strong> order?
            </p>
            <div class="clear"></div>
        </div>

        <div class="margin-top-20">
            <button type="button" class="btn btn-dark f-right margin-right-20 close-me">Cancel</button>
            <button type="button" class="btn btn-dark f-right margin-right-20 confirm-relay"><i class="fa fa-spinner fa-pulse hidden"></i>Confirm</button>
            <div class="clear"></div>
        </div>

    </div>
</div>

<!-- update address-->
<div class="modal-container" modal-id="update-address">

    <!-- modal body -->
    <div class="modal-body small">

        <!-- modal head -->
        <div class="modal-head ">
            <h4 class="text-left">Deliver to Different Address</h4>

            <div class="modal-close close-me"></div>
        </div>
        <form id="coordinator_update_address">
            <div class="bggray-light">
                <div class="form-group f-left margin-left-20 margin-top-20">
                    <input id="choose" type="radio" class="radio-box" name="addressbook">
                    <label class="radio-lbl margin-right-10 " for="choose">Choose from Address Book</label>
                    <input id="enter" type="radio" class="radio-box" name="addressbook">
                    <label class="radio-lbl margin-right-10" for="enter">Enter New Delivery Address</label>
                </div>
                <div class="clear"></div>

                <hr>
            </div>
            <div id="city_select"><input type="hidden" name="city" value="169"></div>
            <div class="hidden_fields_for_rta">
                <input type="hidden" name="building" value="zxavier Building"/>

                <input type="hidden" name="province" value="1"/>
                <input type="hidden" name="street" value=""/>
                <input type="hidden" name="barangay" value=""/>
                <input type="hidden" name="subdivision"/>
            </div>

            <div class="bggray-light text-left">

                <!-- delivery address -->
                <container class="customer_address_block">

                </container>
                <!--<div class="margin-left-20 margin-right-20">
                <label>Delivery Address:</label>

                <div class="bggray-dark padding-all-5 font-14 small-curved-border margin-bottom-20">
                    <div class="display-inline-mid margin-right-10 padding-left-20">
                        <img src="<?php /*echo assets_url() */ ?>/images/work-icon.png" alt="work icon">
                        <p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
                    </div>
                    <div class="display-inline-mid margin-left-10  padding-left-10">
                        <p class="no-margin-all"><strong>Cr8v Web Solutions, Inc.</strong></p>
                        <p class="no-margin-all">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR</p>
                        <p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
                    </div>
                    <div class="display-inline-mid text-center margin-left-30">
                        <a class="red-color" href="#">Change <br>Address <br>
                            <img src="<?php /*echo assets_url() */ ?>/images/ui/select-arrow.png">
                        </a>
                    </div>
                </div>
            </div>-->

                <hr>
            </div>


            <!-- modal content -->
            <div class="modal-content no-margin-all">

                <!-- error message -->
                <div class="modal-error-msg update-address-error-msg error-msg margin-bottom-10" style="font-size: 12px">
                    <i class="fa fa-exclamation-triangle margin-right-10"></i>Please fill up all requred fields.
                </div>

                <!-- address -->
                <table class="margin-bottom-15 margin-left-15">
                    <tbody>
                        <tr>
                            <td><label>Address Type: <!-- <span class="red-color">*</span> --></label></td>
                            <td><label>Address Label: <!-- <span class="red-color">*</span> --></label></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="select margin-right-50">
                                    <div class="frm-custom-dropdown">
                                        <div class="frm-custom-dropdown-txt">
                                            <input type="text" class="dd-txt" name="address_type" labelinput="Address Type">
                                        </div>
                                        <div class="frm-custom-icon"></div>
                                        <div class="frm-custom-dropdown-option">
                                            <div class="option" data-value="Home">Home</div>
                                        </div>
                                    </div>
                                    <select class="frm-custom-dropdown-origin" style="display: none;">
                                        <option value="Home">Home</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <input type="text" class="normal" name="address_label" labelinput="Address Label">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <!-- province -->
                <table class="margin-top-15 margin-left-15">
                    <tbody>
                        <tr>
                            <td><label>Province: <span class="red-color">*</span></label></td>
                            <td><label>City: <span class="red-color">*</span></label></td>
                        </tr>
                        <tr>
                            <td>
                                <div id="coordinator_modal_select_province">
                                    <div class="select margin-right-50">
                                        <div class="frm-custom-dropdown">
                                            <div class="frm-custom-dropdown-txt">
                                                <input type="text" class="dd-txt" name="province" labelinput="Province" datavalid="required">
                                            </div>
                                            <div class="frm-custom-icon"></div>
                                            <div class="frm-custom-dropdown-option">
                                            </div>
                                        </div>
                                        <select class="frm-custom-dropdown-origin" style="display: none;">
                                            <option value="">Select Province</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div id="coordinator_modal_select_city">
                                    <div class="select">
                                        <div class="frm-custom-dropdown">
                                            <div class="frm-custom-dropdown-txt">
                                                <input type="text" class="dd-txt" name="city" labelinput="City" datavalid="required"></div>
                                            <div class="frm-custom-icon"></div>
                                            <div class="frm-custom-dropdown-option">
                                            </div>
                                        </div>
                                    </div>
                                    <select class="frm-custom-dropdown-origin" style="display: none;">
                                        <option value="">Select City</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="margin-left-15	">
                    <label class="margin-top-10">Landmark:</label>
                    <input type="text" class="xlarge" name="landmark">
                </div>

                <!-- 2 column table -->
                <table class="margin-top-10 margin-left-15">
                    <tbody>
                        <tr>
                            <td><label>House Number: </label></td>
                            <td><label>Building: </label></td>
                        </tr>
                        <tr>
                            <td><input type="text" class="normal margin-right-30" name="house_number"></td>
                            <td>
                                <input type="text" class="normal input_modal_autocomplete" id="coordinator_modal_search_building" name="building"/>

                                <div class="result-list"></div>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="margin-top-10">Unit: </label></td>
                            <td><label>Floor: </label></td>
                        </tr>
                        <tr>
                            <td><input type="text" class="normal" name="unit"></td>
                            <td><input type="text" class="normal" name="floor"></td>
                        </tr>
                    </tbody>
                </table>

                <!-- single column table -->
                <table class="margin-left-15">
                    <tbody>
                        <tr>
                            <td><label class="margin-top-10">Street:</label></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" class="xlarge input_modal_autocomplete" id="coordinator_modal_search_street" name="street">

                                <div class="result-list"></div>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="margin-top-10">Secondary Street:</label></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" class="xlarge input_modal_autocomplete" id="coordinator_modal_search_second_street" name="second_street">

                                <div class="result-list"></div>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="margin-top-10">Subdivision:</label></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" class="xlarge input_modal_autocomplete" id="coordinator_modal_search_subdivision" name="subdivision">

                                <div class="result-list"></div>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="margin-top-10">Barangay:</label></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" class="normal input_modal_autocomplete" id="coordinator_modal_search_barangay" name="barangay">

                                <div class="result-list"></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="bggray-light padding-bottom-20 rta-address-info-container">
                <hr>
                <div class="text-left margin-left-40 margin-right-40 margin-top-15">
                    <label>Nearest Store:</label>
                    <br>

                    <div class="bggray-light padding-all-10 font-14 text-center small-curved-border find-rta-container">
                        <a class="btn btn-light find_retail_search"><i class="fa fa-spinner fa-pulse hidden"></i> Find Retail Trade Area</a>

                        <a class="btn btn-light margin-left-20 show_map">Show Map</a>
                    </div>
                    <!--withput nearest store end-->

                    <!--with nearest store -->
                    <div class="bggray-dark found-retail">
                        <div>
                            <p class="font-14 f-left margin-top-20 margin-left-15">
                                <strong>
                                    <span class="retail-store-name">MM Ortigas Roosevelt | MI0444</span>
                                </strong>
                            </p>
                            <a class="font-14 f-right margin-top-20 margin-right-15 show_map">
                                <strong>
                                    <i class="fa fa-map-marker font-16 margin-right-5 "></i>Show Map
                                </strong>
                            </a>

                            <div class="clear"></div>
                        </div>
                        <div class="rta-min-container">
                            <p class="font-14 f-left green-color margin-left-15">
                                <strong>
								<span class="retail-store-time-label">Delivery Time:<span>
                                </strong>
                            </p>
                            <p class="font-14 f-right margin-right-15 margin-bottom-20">
                                <strong>
                                    <span class="retail-store-time">N/A</span>
                                </strong>
                            </p>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <div class="notify-msg margin-top-20 hidden">
                        <strong>Store Announcement!</strong> Delivery will be delayed for 10 minutes due to bad weather. Thank you.
                    </div>


                </div>
            </div>


            <div class="f-right margin-right-40 margin-bottom-10 margin-top-20">
                <button type="button" class="btn btn-dark margin-right-10 confirm_update_address">Send Order</button>
                <button type="button" class="btn btn-dark close-me cancel_update_address">Cancel</button>
            </div>
        </form>
        <div class="clear"></div>

    </div>
</div>

<!-- void order-->
<div class="modal-container " modal-id="void-order">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Void Order</h4>

            <div class="modal-close close-me"></div>
        </div>

        <div class="modal-content no-margin-all">

            <!-- logo and name -->

            <img style="margin-top: 55px;" src="<?php echo assets_url() ?>/images/warning.svg" alt="warning message" class="width-20per f-left">

            <table>
                <tbody>
                    <tr>
                        <td>
                            <p class="font-12 f-left margin-top-20 margin-left-20">Are you sure you want to void
                                <strong>
                                    <info class="customer_name"></info>
                                </strong> order?
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="bggray-dark margin-left-20 margin-top-10 padding-all-10">
                                <p class="font-14 f-left margin-bottom-5"><strong>Order ID:
                                        <info class="order_id"></info>
                                    </strong></p>

                                <div class="clear"></div>

                                <p class="font-12 f-left margin-bottom-5 margin-right-50">
                                    <strong><span class="red-color">Name: </span></strong></p>
                                <p class="font-12 f-left">
                                    <info class="customer_name"></info>
                                </p>

                                <div class="clear"></div>

                                <p class="font-12 f-left margin-bottom-5 margin-right-20">
                                    <strong><span class="red-color">Contact No: </span></strong></p>
                                <p class="font-12 f-left">(+63) 915-578-6147 <i class="fa fa-mobile"></i> Globe</p>

                                <div class="clear"></div>
                            </div>
                        </td>
                    </tr>

                </tbody>
            </table>

            <div class="clear"></div>

        </div>
        <!-- button -->
        <div class="margin-top-20">
            <button type="button" class="btn btn-dark f-right margin-right-20 close-me ">Cancel</button>
            <button type="button" class="btn btn-dark f-right margin-right-20 confirm_void">Confirm</button>
            <div class="clear"></div>
        </div>


    </div>
</div>

<!--process orders-->
<div class="modal-container" modal-id="process-order">

    <!-- modal body -->
    <div class="modal-body small">

        <!-- modal head -->
        <div class="modal-head ">
            <h4 class="text-left">Process Order</h4>

            <div class="modal-close close-me"></div>
        </div>

        <div class="bggray-light margin-top-20">
            <p class="font-12 text-left margin-left-40 margin-right-40 margin-bottom-10 padding-top-15">Please enter the correct address information inorder to
                find proper Retail Trade Area (RTA) for this order.</p>
            <hr>
        </div>

         <form id="coordinator_process_order_address">
            <div id="city_select_process_order_address"><input type="hidden" name="city" value="169"></div>
            <div class="hidden_fields_for_rta">
                <input type="hidden" name="building" value="zxavier Building"/>
                <input type="hidden" name="province" value="1"/>
                <input type="hidden" name="street" value=""/>
                <input type="hidden" name="barangay" value=""/>
                <input type="hidden" name="subdivision"/>
            </div>

            <div class="bggray-light text-left">

                <!-- delivery address -->
                <container class="customer_address_block">

                </container>
                <!--<div class="margin-left-20 margin-right-20">
                <label>Delivery Address:</label>

                <div class="bggray-dark padding-all-5 font-14 small-curved-border margin-bottom-20">
                    <div class="display-inline-mid margin-right-10 padding-left-20">
                        <img src="<?php /*echo assets_url() */ ?>/images/work-icon.png" alt="work icon">
                        <p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
                    </div>
                    <div class="display-inline-mid margin-left-10  padding-left-10">
                        <p class="no-margin-all"><strong>Cr8v Web Solutions, Inc.</strong></p>
                        <p class="no-margin-all">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR</p>
                        <p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
                    </div>
                    <div class="display-inline-mid text-center margin-left-30">
                        <a class="red-color" href="#">Change <br>Address <br>
                            <img src="<?php /*echo assets_url() */ ?>/images/ui/select-arrow.png">
                        </a>
                    </div>
                </div>
            </div>-->

                <hr>
            </div>


            <!-- modal content -->
            <div class="modal-content no-margin-all">

                <!-- error message -->
                <div class="modal-error-msg update-address-error-msg error-msg margin-bottom-10" style="font-size: 12px">
                    <i class="fa fa-exclamation-triangle margin-right-10"></i>Please fill up all requred fields.
                </div>

                <!-- address -->
                <table class="margin-bottom-15 margin-left-15">
                    <tbody>
                        <tr>
                            <td><label>Address Type: <!-- <span class="red-color">*</span> --></label></td>
                            <td><label>Address Label: <!-- <span class="red-color">*</span> --></label></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="select margin-right-50">
                                    <div class="frm-custom-dropdown">
                                        <div class="frm-custom-dropdown-txt">
                                            <input type="text" class="dd-txt" name="address_type" labelinput="Address Type">
                                        </div>
                                        <div class="frm-custom-icon"></div>
                                        <div class="frm-custom-dropdown-option">
                                            <div class="option" data-value="Home">Home</div>
                                        </div>
                                    </div>
                                    <select class="frm-custom-dropdown-origin" style="display: none;">
                                        <option value="Home">Home</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <input type="text" class="normal" name="address_label" labelinput="Address Label">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <!-- province -->
                <table class="margin-top-15 margin-left-15">
                    <tbody>
                        <tr>
                            <td><label>Province: <span class="red-color">*</span></label></td>
                            <td><label>City: <span class="red-color">*</span></label></td>
                        </tr>
                        <tr>
                            <td>
                                <div id="coordinator_po_modal_select_province">
                                    <div class="select margin-right-50">
                                        <div class="frm-custom-dropdown">
                                            <div class="frm-custom-dropdown-txt">
                                                <input type="text" class="dd-txt" name="province" labelinput="Province" datavalid="required">
                                            </div>
                                            <div class="frm-custom-icon"></div>
                                            <div class="frm-custom-dropdown-option">
                                            </div>
                                        </div>
                                        <select class="frm-custom-dropdown-origin" style="display: none;">
                                            <option value="">Select Province</option>
                                        </select>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div id="coordinator_po_modal_select_city">
                                    <div class="select">
                                        <div class="frm-custom-dropdown">
                                            <div class="frm-custom-dropdown-txt">
                                                <input type="text" class="dd-txt" name="city" labelinput="City" datavalid="required"></div>
                                            <div class="frm-custom-icon"></div>
                                            <div class="frm-custom-dropdown-option">
                                            </div>
                                        </div>
                                    </div>
                                    <select class="frm-custom-dropdown-origin" style="display: none;">
                                        <option value="">Select City</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="margin-left-15  ">
                    <label class="margin-top-10">Landmark:</label>
                    <input type="text" class="xlarge" name="landmark">
                </div>

                <!-- 2 column table -->
                <table class="margin-top-10 margin-left-15">
                    <tbody>
                        <tr>
                            <td><label>House Number: </label></td>
                            <td><label>Building: </label></td>
                        </tr>
                        <tr>
                            <td><input type="text" class="normal margin-right-30" name="house_number"></td>
                            <td>
                                <input type="text" class="normal input_modal_autocomplete" id="coordinator_po_modal_search_building" name="building"/>

                                <div class="result-list"></div>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="margin-top-10">Unit: </label></td>
                            <td><label>Floor: </label></td>
                        </tr>
                        <tr>
                            <td><input type="text" class="normal" name="unit"></td>
                            <td><input type="text" class="normal" name="floor"></td>
                        </tr>
                    </tbody>
                </table>

                <!-- single column table -->
                <table class="margin-left-15">
                    <tbody>
                        <tr>
                            <td><label class="margin-top-10">Street:</label></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" class="xlarge input_modal_autocomplete" id="coordinator_po_modal_search_street" name="street">

                                <div class="result-list"></div>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="margin-top-10">Secondary Street:</label></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" class="xlarge input_modal_autocomplete" id="coordinator_po_modal_search_second_street" name="second_street">

                                <div class="result-list"></div>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="margin-top-10">Subdivision:</label></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" class="xlarge input_modal_autocomplete" id="coordinator_po_modal_search_subdivision" name="subdivision">

                                <div class="result-list"></div>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="margin-top-10">Barangay:</label></td>
                        </tr>
                        <tr>
                            <td>
                                <input type="text" class="normal input_modal_autocomplete" id="coordinator_po_modal_search_barangay" name="barangay">

                                <div class="result-list"></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

                <div class="bggray-light padding-bottom-20 rta-address-info-container">
                <hr>
                <div class="text-left margin-left-40 margin-right-40 margin-top-15">
                    <label>Nearest Store:</label>
                    <br>

                    <div class="bggray-light padding-all-10 font-14 text-center small-curved-border find-rta-container">
                        <a class="btn btn-light find_retail_search"><i class="fa fa-spinner fa-pulse hidden"></i> Find Retail Trade Area</a>

                        <a class="btn btn-light margin-left-20 show_map">Show Map</a>
                    </div>
                    <!--withput nearest store end-->

                    <!--with nearest store -->
                    <div class="bggray-dark found-retail">
                        <div>
                            <p class="font-14 f-left margin-top-20 margin-left-15">
                                <strong>
                                    <span class="retail-store-name">MM Ortigas Roosevelt | MI0444</span>
                                </strong>
                            </p>
                            <a class="font-14 f-right margin-top-20 margin-right-15 show_map">
                                <strong>
                                    <i class="fa fa-map-marker font-16 margin-right-5 "></i>Show Map
                                </strong>
                            </a>

                            <div class="clear"></div>
                        </div>
                        <div class="rta-min-container">
                            <p class="font-14 f-left green-color margin-left-15">
                                <strong>
                                <span class="retail-store-time-label">Delivery Time:<span>
                                </strong>
                            </p>
                            <p class="font-14 f-right margin-right-15 margin-bottom-20">
                                <strong>
                                    <span class="retail-store-time">N/A</span>
                                </strong>
                            </p>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <div class="notify-msg margin-top-20 hidden">
                        <strong>Store Announcement!</strong> Delivery will be delayed for 10 minutes due to bad weather. Thank you.
                    </div>


                </div>
            </div>


    <div class="f-right margin-right-40 margin-bottom-10 margin-top-20">
        <button type="button" class="btn btn-dark margin-right-10 confirm_process" disabled>Send Order</button>
        <button type="button" class="btn btn-dark close-me">Cancel</button>
    </div>
    </form>
    <div class="clear"></div>


</div>
</div>

<!--logs-->
<div class="modal-container " modal-id="order-logs">
    <div class="modal-body small">
        <div class="modal-head">
            <h4 class="text-left">Order Logs</h4>

            <div class="modal-close close-me"></div>
        </div>
        <div class="modal-content">
            <label class="margin-bottom-10">ORDER LOGS:</label>

            <div class="tbl">
                <table class="no-margin-all">
                    <thead>
                        <tr>
                            <th class="width-50per">Logs</th>
                            <th>Date / Time</th>
                            <th>Username</th>
                            <!--<th>Call Center</th>-->
                        </tr>
                    </thead>
                    <tbody class="agent_logs_container">
<!--                        <tr>-->
<!--                            <td>Start Over</td>-->
<!--                            <td class="text-center">05-18-2015 | 12:41 PM</td>-->
<!--                            <td class="text-center">Stephanie Jocson</td>-->
<!--                            <td class="text-center">PHUB</td>-->
<!--                        </tr>-->
<!--                        <tr>-->
<!--                            <td>Send Order</td>-->
<!--                            <td class="text-center">05-18-2015 | 12:43 PM</td>-->
<!--                            <td class="text-center">Stephanie Jocson</td>-->
<!--                            <td class="text-center">PHUB</td>-->
<!--                        </tr>-->
<!--                        <tr>-->
<!--                            <td>Lock Order</td>-->
<!--                            <td class="text-center">05-18-2015 | 12:43 PM</td>-->
<!--                            <td class="text-center">Kevin Miranda</td>-->
<!--                            <td class="text-center">PHUB</td>-->
<!--                        </tr>-->
                    </tbody>
                </table>
            </div>

            <!-- <label class="margin-top-10 margin-bottom-10">Store Logs:</label> -->

            <!-- <div class="tbl">
                <table class="no-margin-all">
                    <thead>
                        <tr>
                            <th class="width-70per">Store Name</th>
                            <th>Logs</th>
                            <th>Date / Time</th>
                        </tr>
                    </thead>
                    <tbody class="store_logs_container">
                        <tr>
                            <td>MM Ortigas Roosevelt | JB0444</td>
                            <td>Received Order</td>
                            <td>05-18-2015 | 12:41 PM</td>
                        </tr>
                        <tr>
                            <td>MM Ortigas Roosevelt | JB0444</td>
                            <td>Received Order - <br>Wrong Routing</td>
                            <td>05-18-2015 | 12:43 PM</td>
                        </tr>
                    </tbody>
                </table>
            </div> -->
        </div>
        <button type="button" class="btn btn-dark f-right margin-right-20 close-me">Close</button>
        <div class="clear"></div>
    </div>
</div>

<!--messenger-->
<div class="modal-container" modal-id="messenger">
    <div class="modal-body">
        <div class="modal-head ">
            <h4 class="text-left">Messenger</h4>
            <div class="modal-close close-me"></div>
        </div>

        <!-- content -->
        <div class="modal-content messenger">
            <!-- messages -->
            <div class="msg-content">
                <div class="chat-box chat_box_container">







                    <!--<p class="text-left font-12"><span class="gray-color">May 18, 2015 (12:57 PM)</span><span> - MM Sampaloc | JB1021 </span></p>
                    <div class="sender">
                        <img class="profile" src="<?php echo assets_url() ?>images/jollibe-face.png">
                        <div class="msg">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis felis consectetur, sodales nunc quis, sagittis augue. Proin eu molestie libero, vitae volutpat eros.</p><br>
                        </div>
                    </div>

                    <p class="text-right font-12"><span class="gray-color">May 18, 2015 (1:45 PM)</span> - MM Q.C. | JB1021</p>
                    <div class="receiver">
                        <div class="msg">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis felis consectetur, sodales nunc quis, sagittis augue. Proin eu molestie libero, vitae volutpat eros.</p><br>
                            <p>Sodales nunc quis, sagittis augue. Proin eu molestie libero, vitae volutpat eros.</p><br>
                            <p>TY</p>
                        </div>
                        <img class="profile" src="<?php /*echo assets_url() */?>images/profile-pic.jpg">
                    </div>

                    <p class="text-left font-12"><span class="gray-color">May 18, 2015 (1:46 PM)</span> - MM Sampaloc | JB1021</p>
                    <div class="sender">
                        <img class="profile" src="<?php /*echo assets_url() */?>images/jollibe-face.png">
                        <div class="msg">
                            <p>Thanks :)</p>
                        </div>
                    </div>-->
                </div>
                <!-- text input -->
                <div class="text-input padding-all-10">
                    <textarea row="5" class="margin-top-20 message_content" placeholder="Type your message here..."></textarea>
                    <div class="btn-container padding-top-20 margin-top-20">
                        <button class="btn btn-dark width-100per send_message">Send</button>
                        <button class="btn btn-dark margin-top-10 margin-right-10 width-50per"><img class="width-90per" src="<?php echo assets_url() ?>images/ui/smile.png"></button>
                        <button class="btn btn-dark margin-top-10 margin-left-10 no-padding-hor width-50per close-me">Cancel</button>
                    </div>
                </div>
            </div>

            <!-- contacts -->
            <div class="contacts">
                <label>Search Store:</label>
                <input type="text" class="width-100per normal"  name="search_store" id="messenger_search_store">
                <div class="content">
                    <table class="width-100per table_store_messenger_container table-inside">
                        <thead>
                            <tr>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody class="store_messenger_container">

<!--                            <tr>-->
<!--                                <td class="padding-all-10">JB1214 - J.P. Rizal, Makati</td>-->
<!--                            </tr>-->
                        </tbody></table>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end Modals-->


<!-- Templates -->

<!--search result template-->
<div class="search_result_container template">

    <div class="content-container viewable">
        <div class="header-search">
            <div class="width-40per f-left name_and_contact">
                <p class="font-16 name"><strong class="info_name">Ron Frederick T. San Juan</strong></p>
                <p class="font-14 margin-top-10 contact">
                    <span class="red-color"><strong class="contact_number">Contact Num:</strong></span>
                    <i class="fa fa-mobile"></i></p>
            </div>
            <div class="width-60per f-right remarks_and_address">
                <p class="font-14 no-margin-bottom remarks">
                    <strong><span class="info-remarks">THIS CUSTOMER IS EASY TO HANDLE</span></strong></p>
                <p class="font-14 margin-top-10 address">
                    <span class="red-color info_address"><strong class="">Address:</strong></span>
                </p>
            </div>
            <div class="clear"></div>

            <input type="hidden" name="street" value=""/>
            <input type="hidden" name="subdivision" value=""/>
            <input type="hidden" name="building" value=""/>
            <input type="hidden" name="barangay" value=""/>
            <input type="hidden" name="city"/>
            <input type="hidden" name="province"/>

        </div>

        <div class="collapsible" style="display: none;">
            <div class="second_header margin-bottom-10" data-customer-id="">
                <div class="width-40per f-left">
                    <p class="font-16 "><strong class="info_name">Audrey D. Hepburn</strong></p>
                </div>
                <div class="f-right">
                    <p class="font-16">
                        <strong><span class="info-remarks">THIS CUSTOMER IS EASY TO HANDLE</span></strong></p>
                </div>
                <div class="clear"></div>
            </div>
            <hr>

            <!--success messages part-->
            <div class="success-msg margin-bottom-20 margin-top-20">
                <i class="fa fa-exclamation-triangle"></i>
                Customer has been updated successfully
            </div>
            <!--end success message-->

            <!-- 1st half -->
            <div class="data-container split">
                <div class="margin-top-15 contact_number">
                    <label>Contact Number</label><br>

                    <div class="select large">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" name="undefined" class="dd-txt" readonly></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option" style="display: none;">
                                <div data-value="undefined" class="option">
                                    <span class="skype_c2c_print_container notranslate">(+63) 910-516-6153</span><span data-ismobile="false" data-isrtl="false" data-isfreecall="false" data-numbertype="paid" data-numbertocall="+639105166153" onclick="SkypeClick2Call.MenuInjectionHandler.makeCall(this, event)" onmouseout="SkypeClick2Call.MenuInjectionHandler.hideMenu(this, event)" onmouseover="SkypeClick2Call.MenuInjectionHandler.showMenu(this, event)" tabindex="-1" dir="ltr" class="skype_c2c_container notranslate" id="skype_c2c_container"><span skypeaction="skype_dropdown" dir="ltr" class="skype_c2c_highlighting_inactive_common"><span id="non_free_num_ui" class="skype_c2c_textarea_span"><img width="0" height="0" class="skype_c2c_logo_img"><span class="skype_c2c_text_span">(+63) 910-516-6153</span><span class="skype_c2c_free_text_span"></span></span></span></span> Smart
                                </div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin contact_numbers" name="contact_numbers" style="display: none;">
                            <option>(+63) 915-516-6153 Globe</option>
                            <option>(+63) 910-516-6153 Smart</option>
                        </select>
                    </div>
                </div>
                <div class="margin-top-15 alternate_contact_number">
                    <label>alternate Number</label><br>

                    <div class="select large">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" name="undefined" class="dd-txt" readonly></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option" style="display: none;">
                                <div data-value="undefined" class="option">
                                    (+63) 910-516-6153
                                </div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;" name="contact_numbers">
                            <option>(+63) 915-516-6153 Globe</option>
                            <option>(+63) 910-516-6153 Smart</option>
                        </select>
                    </div>
                </div>

                <div class="margin-top-15">
                    <p class="f-left red-color font-14"><strong>Email Address:</strong></p>
                    <p class="f-right font-14 email_address">markdulay@gmail.com</p>

                    <div class="clear"></div>
                </div>

                <label class="margin-top-15">CARDS:</label>

                <div class="happy_plus_card_container">
                    <div class="bggray-light margin-bottom-10 padding-all-5 font-14 small-curved-border happy_plus_card template">
                        <div class="display-inline-mid margin-right-10">
                            <img alt="happy-plus" src="<?php echo base_url() ?>/assets/images/happy-plus.jpg" class="thumb">
                        </div>
                        <div class="display-inline-mid margin-left-10 divider padding-left-10">
                            <p class="no-margin-all info-happy-plus-number">0083-123456-46578</p>
                            <span class="red-color"><strong>Exp. Date:</strong></span>
                            <p class="expiration_date" style="display:inline"> September 20, 2016</p></p>
                        </div>
                        <div class="display-inline-mid text-center margin-left-10 f-right">
                            <!-- <a href="#">Show Card<br>History</a> -->
                        </div>
                    </div>
                </div>


                <label class="margin-top-15">DELIVERY ADDRESS:</label>

                <div class="customer_address_container">
                    <div class="bggray-light padding-all-5 font-14 small-curved-border template customer_address">
                        <div class="display-inline-mid padding-left-20">
                            <img alt="work icon" class="info_address_type" width="35px" height="33px" src="<?php echo base_url() ?>/assets/images/work-icon.png">
                            <p class="font-12 text-center no-margin-all"><strong class="info_address_type"></strong></p>
                        </div>
                        <div class="display-inline-mid padding-left-10" style="max-width: 245px">
                            <p class="no-margin-all">
                                <strong class="info_address_label">Cr8v Web Solutions, Inc.</strong></p>
                            <p class="no-margin-all info_address_complete">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR
                            </p>
                            <p class="no-margin-all gray-color info_address_landmark"></p>
                        </div>
                        <div class="display-inline-mid text-center margin-left-10 edit_container">
                            <a class="red-color edit_address">Change<br>Address<br>

                                <div class="arrow-down"></div>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="customer_secondary_address">

                    <div class="margin-top-10 margin-left-20 deliver-to-different">
                        <p class="font-12"><i class="fa fa-plus-circle fa-3x margin-right-10  yellow-color f-left"></i>
                        </p>
                        <p modal-target="deliver-to-different" class="font-12 f-left margin-top-10 margin-left-10 modal-trigger">
                            <strong>Deliver to a Different Address</strong></p>

                        <div class="clear"></div>
                    </div>
                </div>

                <hr>


                <label class="margin-top-15">NEAREST STORE:</label>

                <!--without nearest store -->

                <div class="bggray-light padding-all-10 font-14 text-center small-curved-border find-rta-container rta-address-info-container">
                    <button class="btn btn-light find_retail_search"><i class="fa fa-spinner fa-pulse hidden"></i> Find Retail Trade Area</button>

                    <button class="btn btn-light margin-left-20 show_map">Show Map</button>
                </div>
                <!--withput nearest store end-->

                <!--with nearest store -->
                <div class="bggray-dark found-retail rta-address-info-container">
                    <div>
                        <p class="font-14 f-left margin-top-20 margin-left-15">
                            <strong>
                                <span class="retail-store-name">MM Ortigas Roosevelt | MI0444</span>
                            </strong>
                        </p>
                        <a class="font-14 f-right margin-top-20 margin-right-15 show_map">
                            <strong>
                                <i class="fa fa-map-marker font-16 margin-right-5"></i>Show Map
                            </strong>
                        </a>

                        <div class="clear"></div>
                    </div>
                    <div class="rta-min-container">
                        <p class="font-14 f-left green-color margin-left-15">
                            <strong>
								<span class="retail-store-time-label">Delivery Time:<span>
                            </strong>
                        </p>
                        <p class="font-14 f-right margin-right-15 margin-bottom-20">
                            <strong>
                                <span class="retail-store-time">N/A</span>
                            </strong>
                        </p>

                        <div class="clear"></div>
                    </div>
                </div>

                <div class="notify-msg margin-top-20 hidden">
                    <strong>Store Announcement!</strong> Delivery will be delayed for 10 minutes due to bad weather. Thank you.
                </div>

                <!--with nearest store end-->


            </div>
            <!-- End 1st Half -->

            <!-- 2nd half -->
            <div class="data-container split margin-left-15 order_history">
                <label class="margin-top-15">order history:</label>

                <div class="arrow-selector">
                    <div class="arrow-left"><i class="fa "></i></div>
                    <div class="select">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" name="date-selected" class="dd-txt" readonly></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option" style="display: none;">

                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">

                        </select>
                    </div>
                    <div class="arrow-right"><i class="fa "></i></div>
                </div>

                <div class="margin-top-15 font-12">
                    <p class="red-color f-left "><strong>Last Payment Method Used:</strong> <strong class="last_payment_method_used"></strong></p>
                    <p class="red-color f-left font-10">
                        <!-- <strong>Items with red text means they are unvavailable today</strong> --></p>
                    <p class="f-right"></p>

                    <div class="clear"></div>
                </div>
                <div class="order_history_container">

                </div>
                <div class="margin-top-15 font-12">
                    <p class="red-color f-left "><strong>Last Order Date:</strong></p><p class="f-left"><strong class="last_order_date"></strong></p>
                    <p class="red-color f-left font-10">
                        <!-- <strong>Items with red text means they are unvavailable today</strong> --></p>
                    <p class="f-right"></p>

                    <div class="clear"></div>
                </div>


                <!-- <label class="margin-top-15">other brand order history</label> -->

<!--                <div class="bggray-light">-->
<!--                    <table class="font-14 width-100per">-->
<!--                        <tbody>-->
<!--                            <tr>-->
<!--                                <td class="padding-all-10">-->
<!--                                    <img alt="burger king" src="http://localhost/callcenter/assets/images/affliates/bk-logo.png" class="small-thumb">-->
<!--                                </td>-->
<!--                                <td class="padding-all-10">Burger King</td>-->
<!--                                <td class="padding-all-10 text-right">May 1, 2015 | 4:20 PM</td>-->
<!--                            </tr>-->
<!--                            <tr>-->
<!--                                <td class="padding-all-10">-->
<!--                                    <img alt="greenwich" src="http://localhost/callcenter/assets/images/affliates/gw-logo.png" class="small-thumb">-->
<!--                                </td>-->
<!--                                <td class="padding-all-10">Greenwich</td>-->
<!--                                <td class="padding-all-10 text-right">May 1, 2015 | 4:20 PM</td>-->
<!--                            </tr>-->
<!--                            <tr>-->
<!--                                <td class="padding-all-10">-->
<!--                                    <img alt="chowking" src="http://localhost/callcenter/assets/images/affliates/ck-logo.png" class="small-thumb">-->
<!--                                </td>-->
<!--                                <td class="padding-all-10">Chowking</td>-->
<!--                                <td class="padding-all-10 text-right">May 1, 2015 | 4:20 PM</td>-->
<!--                            </tr>-->
<!--                        </tbody>-->
<!--                    </table>-->
<!--                </div>-->
            </div>

            <div class="text-right margin-top-20">
                <button class="btn btn-dark margin-right-10 btn-update" type="button">Update Customer Information</button>
                <button class="btn btn-dark margin-right-10 add_last_meal_to_order" type="button" disabled>Add Last Order to Cart</button>
                <button class="btn btn-dark order_process proceed_order_from_search" type="button" disabled>Proceed to Order</button>
            </div>
        </div>
    </div>

    <!--collapsible content-->


    <!--end collapsible content-->

</div>
<!--search result teamplte end-->

<!--customer address template-->
<div class="bggray-light padding-all-5 font-14 small-curved-border template customer_address">
    <div class="display-inline-mid margin-right-10 padding-left-20">
        <img alt="work icon" src="<?php echo base_url() ?>/assets/images/work-icon.png">
        <p class="font-12 text-center no-margin-all"><strong>WORK</strong></p>
    </div>
    <div class="display-inline-mid margin-left-10  padding-left-10">
        <p class="no-margin-all"><strong>Cr8v Web Solutions, Inc.</strong></p>
        <p class="no-margin-all">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR</p>
        <p class="no-margin-all gray-color">- Near Jewels Convinient Store</p>
    </div>
    <div class="display-inline-mid text-center margin-left-25">
        <a href="#" class="red-color">Change<br>Address<br>

            <div class="arrow-down"></div>
        </a>
    </div>
</div>
<!--end customer address template-->

<!--customer list search result template-->
<div class="search_result_customer_list template" id="customer_list_search_result">
    <!-- sample-1-->
    <div class="content-container viewable">
        <div class="header-search">
            <div class="width-40per f-left name_and_contact">
                <p class="font-16 name"><strong class="info_name">Ron Frederick T. San Juan</strong></p>
                <p class="font-14 margin-top-10 contact">
                    <span class="red-color"><strong class="contact_number">Contact Num:</strong></span>
                    <i class="fa fa-mobile"></i></p>
            </div>
            <div class="width-60per f-right remarks_and_address">
                <p class="font-14 no-margin-bottom remarks">
                    <strong><span class="info-remarks">THIS CUSTOMER IS EASY TO HANDLE</span></strong></p>
                <p class="font-14 margin-top-10 address">
                    <span class="red-color info_address"><strong class="">Address: </strong></span>
                </p>
            </div>
            <div class="clear"></div>
        </div>


        <div class="collapsible" style="display: none;">
            <div class="second_header margin-bottom-10" data-customer-list-id="">
                <div class="width-40per f-left">
                    <p class="font-16 "><strong class="info_name">Audrey D. Hepburn</strong></p>
                </div>
                <div class="f-right">
                    <p class="font-16">
                        <strong><span class="info-remarks">THIS CUSTOMER IS EASY TO HANDLE</span></strong></p>
                </div>
                <div class="clear"></div>
            </div>

            <hr>

            <!--success messages part-->
            <div class="success-msg margin-bottom-20 margin-top-20">
                <i class="fa fa-exclamation-triangle"></i>
                Customer has been updated successfully
            </div>
            <!--end success message-->

            <div class="data-container split">
                <!-- client's information -->
                <div class="margin-top-20 customer_list_client_info">

                    <p class="f-left red-color font-12 customer_list_contact_label"><strong>Contact Number: </strong>
                    </p>
                    
                    <div class="customer_list_contact_number_container">
                    </div>
<!--  
                    <p class="f-right font-12 customer_list_contact_number_phone">(02) 257-68-95
                        <i class="fa fa-phone margin-right-5"></i>PLDT</p>
                    <div class="clear"></div>
                    <p class="f-right font-12 customer_list_contact_number_mobile">(+63) 915-516-6153
                        <i class="fa fa-mobile margin-right-5"></i>Globe</p>
-->
                    <div class="clear"></div>

                    <p class="f-left red-color font-12 margin-top-10 customer_list_email_address_label">
                        <strong>Email Address:</strong></p>
                    <p class="f-right font-12 margin-top-10 customer_list_email_address">audrey@gmail.com</p>

                    <div class="clear"></div>

                    <p class="f-left red-color font-12 margin-top-10 customer_list_date_of_birth_label">
                        <strong>Date of Birth: </strong></p>
                    <p class="f-right font-12 margin-top-10 customer_list_date_of_birth">May 4, 1916</p>

                    <div class="clear"></div>
                </div>

                <!-- delivery address -->
                <div>
                    <label class="discounts margin-top-10">Discounts:</label>

                    <div class="bggray-light padding-all-5 font-14 small-curved-border customer_list_discounts template">
                        <div class="display-inline-mid customer_list_discounts_label">
                            <p class="font-12 no-margin-all"><strong>Senior Citizen</strong></p>
                        </div>
                        <div class="display-inline-mid margin-left-10 margin-top-10 margin-bottom-10 divider padding-left-10">
                            <p class="no-margin-all font-12 customer_list_discounts_name">Audrey Dean Hepburn</p>
                            <p class="font-12 no-margin-all">
                                <strong class="customer_list_discounts_number"><span class="red-color">OSCA Number: </span>0014-7845</strong>
                            </p>
                        </div>
                    </div>

                    <label class="margin-top-10 cards">Cards:</label>
                    <!-- happy plus picture -->
                    <div class="customer_list_cards_container">
                        <div class="bggray-light margin-bottom-10 padding-all-5 font-14 small-curved-border customer_list_cards template">
                            <div class="display-inline-mid margin-left-10">
                                <img class="thumb " src="<?php echo base_url() ?>/assets/images/happy-plus.jpg" alt="happy-plus">
                            </div>
                            <div class="display-inline-mid margin-left-10 divider padding-left-10">
                                <p class="no-margin-all customer_list_card_number">008-248-369-154</p>
                                <span class="red-color"><strong>Exp. Date:</strong></span>
                                <p class="customer_list_card_number_expiration" style="display: inline;"> September 20, 2016</p>
                            </div>
                            <div class="display-inline-mid text-center margin-left-10">
                                <!-- <a href="#">Show Card<br>History</a> -->
                            </div>
                        </div>
                    </div>
                    <!-- happy plus picture -->
                    <div class="bggray-light padding-all-5 margin-top-10 font-14 small-curved-border template">
                        <div class="display-inline-mid margin-left-10">
                            <img class="thumb" src="<?php echo base_url() ?>/assets/images/happy-plus.jpg" alt="happy-plus">
                        </div>
                        <div class="display-inline-mid margin-left-10 divider padding-left-10">
                            <p class="no-margin-all">003-275-472-124<br>
                                <span class="red-color"><strong>Exp. Date:</strong></span> Ocotober 18, 2016</p>
                        </div>
                        <div class="display-inline-mid text-center margin-left-20">
                            <!-- <a href="#">Show Card<br>History</a> -->
                        </div>
                    </div>

                    <!-- address book -->

                    <label class="margin-top-10">Address Book: </label>

                    <div class="default_address_container">
                        <div class="bggray-light padding-all-5 font-14 small-curved-border margin-bottom-10  customer_list_default_address template">
                            <div class="display-inline-mid margin-right-10 padding-left-20">
                                <img src="<?php echo base_url() ?>/assets/images/work-icon.png" alt="work icon" class="default_address_image">
                                <p class="font-12 text-center no-margin-all">
                                    <strong class="default_address_type">WORK</strong></p>
                            </div>
                            <div class="display-inline-mid margin-left-10 padding-left-10 customer_list_default_address_text">
                                <p class="no-margin-all default_address_label">
                                    <strong class="default_address_label">Cr8v Web Solutions, Inc. -
                                        <span class="red-color">Default</span></strong></p>
                                <p class="no-margin-all default_address_complete">66C &amp; 66D, San Rafael St.
                                    <br> Brgy. Kapitolyo, Pasig City. - NCR</p>
                                <p class="no-margin-all gray-color default_address_landmark">- Near Jewels Convinient Store</p>
                            </div>
                        </div>
                    </div>

                    <div class="other_address_container">
                        <div class="bggray-light padding-all-5 font-14 small-curved-border margin-bottom-10 customer_list_address template">
                            <div class="display-inline-mid margin-right-10 padding-left-20">
                                <img src="<?php echo base_url() ?>/assets/images/Home2.svg" alt="work icon" class="small-thumb address_image">
                                <p class="font-12 text-center no-margin-all"><strong class="address_type">WORK</strong>
                                </p>
                            </div>
                            <div class="display-inline-mid margin-left-10  padding-left-10 customer_list_address_text">
                                <p class="no-margin-all address_label">
                                    <strong class="address_label">House Address # 1 </strong></p>
                                <p class="no-margin-all address_complete">Rm. 404 3rd Floor, 691 Eduard Bldg,
                                    <br>Green Field St, Kapitolyo, Pasig City - NCR</p>
                                <p class="no-margin-all gray-color address_landmark">- Near Jewels Convinient Store</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="data-container split margin-left-15">
                <div class="margin-top-20">
                    <div class="category-item f-left">
                        <img src="<?php echo base_url() ?>/assets/images/Customer_Call.svg" alt="customer logo calling">
                        <p class="font-12 f-right margin-top-10 padding-right-10"><strong>Voice Customer</strong></p>

                        <div class="clear"></div>

                    </div>

                    <div class="category-item f-right">
                        <img src="<?php echo base_url() ?>/assets/images/Customer_Web.svg" alt="customer logo calling" class="padding-left-10">
                        <p class="font-12 f-right margin-top-10"><strong>Web Customer</strong></p>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="order_history">
                    <label class="margin-top-15">order history:</label>
                    <div class="clear"></div>

                    <div class="arrow-selector margin-bottom-20">
                        <div class="arrow-left"><i class="fa "></i></div>
                        <div class="select">
                            <div class="frm-custom-dropdown">
                                <div class="frm-custom-dropdown-txt">
                                    <input type="text" name="date-selected" class="dd-txt"></div>
                                <div class="frm-custom-icon"></div>
                                <div class="frm-custom-dropdown-option" style="display: none;">
                                   <!-- <div data-value="undefined" class="option">April 28, 2015 | 4:20 PM</div>-->
                                </div>
                            </div>
                            <select class="frm-custom-dropdown-origin" style="display: none;">
                               <!-- <option>April 27, 2015 | 4:20 PM</option>
                                <option>April 28, 2015 | 4:20 PM</option>-->
                            </select>
                        </div>
                        <div class="arrow-right"><i class="fa "></i></div>
                    </div>

                    <div class="order_history_container">

                    </div>
                </div>

<!--                 <label class="margin-top-15">other brand order history</label>

                <div class="bggray-light">
                    <table class="font-14 width-100per">
                        <tbody>
                            <tr>
                                <td class="padding-all-10">
                                    <img class="small-thumb" src="<?php echo base_url() ?>/assets/images/affliates/bk-logo.png" alt="burger king">
                                </td>
                                <td class="padding-all-10">Burger King</td>
                                <td class="padding-all-10 text-right">May 7, 2015 | 12:25 PM</td>
                            </tr>
                            <tr>
                                <td class="padding-all-10">
                                    <img class="small-thumb" src="<?php echo base_url() ?>/assets/images/affliates/gw-logo.png" alt="greenwich">
                                </td>
                                <td class="padding-all-10">Greenwich</td>
                                <td class="padding-all-10 text-right">April 27, 2015 | 9:14 AM</td>
                            </tr>
                            <tr>
                                <td class="padding-all-10">
                                    <img class="small-thumb" src="<?php echo base_url() ?>/assets/images/affliates/ck-logo.png" alt="chowking">
                                </td>
                                <td class="padding-all-10">Chowking</td>
                                <td class="padding-all-10 text-right">April 25, 2015 | 10:11 AM</td>
                            </tr>
                        </tbody>
                    </table>
                </div> -->

            </div>
            <div class="f-right margin-top-20">
                <button type="button" class="btn btn-light margin-right-10 btn-update">Update Customer Information</button>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
</div>
<!--end customer list search result template-->

<!--customer information template in orders page-->
<div class="order_customer_information template">
    <table class="width-100per">
        <tbody>
            <tr>
                <td class="light-red-color padding-all-5">Name:</td>
                <td class="text-right padding-all-5 info-customer-full-name">Mark Anthony D. Dulay</td>
            </tr>
            <tr>
                <td class="light-red-color padding-all-5">Contact Num:</td>
                <td class="text-right padding-all-5 info-customer-mobile">(+63) 940-848-1458
                    <i class="fa fa-mobile"></i> TNT
                </td>
            </tr>
            <tr>
                <td class="light-red-color padding-all-5">Alternate Num:</td>
                <td class="text-right padding-all-5 info-customer-mobile-alternate">(+63) 910-576-1248
                    <i class="fa fa-mobile"></i> SMART
                </td>
            </tr>
        </tbody>
    </table>
</div>
<!--end customer information template in orders page-->

<!-- found rta store in order page template-->
<div class="rta_store template">
    <table class="width-100per">
        <tbody>
            <tr>
                <td class="padding-all-5"><p class="panel-title no-padding-bottom f-left">
                        <strong>Store Information</strong></p></td>
                <td class="text-right padding-all-5">
                    <a class="f-right" href="#"><i class="fa fa-map-marker"></i> Show Map</a></td>
            </tr>
            <tr>
                <td class="padding-all-5"><strong class="store_name">MM Ortigas Roosevelt</strong></td>
                <td class="text-right padding-all-5"><strong class="store_code">JB0044</strong></td>
            </tr>
            <tr>
                <td class="padding-all-5 light-red-color"><strong class="store_time_label">Delivery Time:</strong></td>
                <td class="text-right padding-all-5 store_time">40 Minutes</td>
            </tr>
        </tbody>
    </table>

    <div class="store_messages">
    </div>

    <div class="warn-msg margin-top-15 unavailable_products">
        <p class="f-left"><strong>Unvailable Products</strong></p>
        <p class="f-right"><a href="javascript:void(0)" class="toggle_unavailable_products">Show</a></p>
        <div class="clear"></div>

        <div class="unavailable_products_container hidden" style="max-height: 300px; overflow-y: scroll;" ></div>
    </div>
</div>
<!--found rta store in order page template end-->

<!-- not found rta store in order page template-->
<div class="no_rta_store template">
    <table class="width-100per">
        <tbody>
            <tr>
                <td class="padding-all-5"><p class="panel-title no-padding-bottom f-left">
                        <strong>Store Information</strong></p></td>
                <td class="text-right padding-all-5"
                </td>
            </tr>
            <tr>
                <td class="padding-all-5 light-red-color"><strong class="has_spinner">No Stores Available at this moment <i class="fa fa-spinner fa-pulse hidden"></i></strong></td>
                <td class="text-right padding-all-5"></td>
            </tr>
        </tbody>
    </table>

</div>
<!--not found rta store in order page template end-->

<!--order history template-->
<div class="small-curved-border order_history template">
    <table class="font-14" style="width: 100%; display:block;">
        <thead class="bggray-dark">
            <tr>
                <th class="padding-all-10" style="width:10%;" >Quantity</th>
                <th class="padding-all-10" style="width:30%;" >Product</th>
                <th class="padding-all-10" style="width:10%;" >Price</th>
                <th class="padding-all-10" style="width:15%;"  colspan="2">Subtotal</th>
            </tr>
        </thead>

        <tbody class="bggray-light cart-container" style="display:block; max-height: 300px; overflow-y: auto; width:100%;">
            <tr class="product-breakdown-item template">
                <td class="padding-all-10 text-center product-quantity">2</td>
                <td class="padding-all-10 product-name">
                    <!--<div class="arrow-down"></div>-->
                    Champ Amazing aloha
                </td>
                <td class="padding-all-10 product-price">203.50</td>
                <td class="padding-all-10 product-total">407.00</td>
            </tr>
        </tbody>

        <tbody>
            <tr class="bggray-middark">
                <td colspan="2" style="width:20%;" class="padding-all-10 text-left">Total Cost</td>
                <td colspan="3" style="width:80%;" class="padding-all-10 text-right total-cost">591.80 PHP</td>
            </tr>
            <tr class="bggray-middark">
                <td class="padding-all-10 text-left" colspan="2" style="width:20%">Added VAT</td>
                <td class="padding-all-10 text-right total-vat" colspan="3">95.04 PHP</td>
            </tr>
            <tr class="bggray-dark">
                <td class="padding-all-10 text-left font-16" colspan="2">
                    <strong>Total Bill</strong></td>
                <td class="padding-all-10 text-right font-14" colspan="2" style="width:60%">
                    <strong class="total-bill">887.04 PHP</strong></td>
            </tr>
             <tr class="bggray-middark">
                <td class="padding-all-10 text-left" colspan="2">Delivery Charge</td>
                <td class="padding-all-10 text-right delivery-charge" colspan="3">72.00 PHP</td>
            </tr>
        </tbody>

    </table>
</div>
<!--end order history template-->

<!--coordinator order template-->
<div class="order_search_result_block template">
    <div class="row content-container viewable block-header">
        <div>
            <div class="width-40per f-left">
                <p class="font-16 margin-bottom-5"><strong>Order ID:
                        <info class="order_id">41</info>
                    </strong></p>
                <p class="font-16 margin-bottom-5"><strong>
                        <info class="store_summary">MM Ortigas Roosevelt | JB0444</info>
                    </strong></p>
                <p><strong><span class="red-color">Transaction Time:</span>
                        <info class="date_added"> May 18, 2015 | 11:24:00</info>
                    </strong></p>
            </div>

            <div class="width-40per f-left">
                <p class="margin-bottom-5"><strong><span><info class="order_type">MANUAL ORDER</info></span></strong>
                </p>
                <p class="margin-bottom-5"><strong><span class="red-color">Name:</span></strong>
                    <info class="customer_name">Jonathan R. Ornido</info>
                </p>
                <p class="margin-bottom-5"><strong><span class="red-color">Contact No.:</span></strong>
                    <info class="customer_contact"> (+63) 915-516-6153 | Globe</info>
                </p>
            </div>

            <div class="f-left margin-left-20 locked hidden">
                <p class=""><i class="fa fa-lock fa-5x red-color margin-left-30"></i></p>
                <p class="red-color font-12"><strong>
                        <info class="agent_locker"></info>
                    </strong></p>

            </div>

            <div class="clear"></div>
        </div>
    </div>
    <div class="hidden_fields_for_rta">
        <input type="hidden" name="street" value=""/>
        <input type="hidden" name="subdivision" value=""/>
        <input type="hidden" name="building" value=""/>
        <input type="hidden" name="barangay" value=""/>
        <input type="hidden" name="city"/>
        <input type="hidden" name="province"/>
    </div>
    <div class="content-container hidden block-content">
        <div>
            <header-group class="header">
                <div class="f-left margin-bottom-10">
                    <p class="font-16"><strong class="order_id">Order ID:
                            <info class="order_id">734784</info>
                        </strong></p>
                    <p class="font-16 no-margin-all"><strong class="store_name">
                            <info class="store_summary">734784</info>
                        </strong></p>
                </div>
                <p class="no-margin-bottom f-right">
                    <strong><span><info class="order_type">MANUAL ORDER</info></span></strong>
                </p>
                <br>
                <p class="font-12 f-right margin-top-5">
                    <strong><span class="red-color">Elapsed Time: </span></strong>
                    <info class="elapsed_time"></info>
                </p>

                <div class="clear"></div>
            </header-group>

            <hr>

            <div class="data-container split">

                <!-- clients information -->
                <div class="margin-top-20">
                    <p class="f-left red-color font-12 margin-bottom-5"><strong>Name: </strong></p>
                    <p class="f-right font-12 ">
                        <info class="customer_name"></info>
                    </p>

                    <div class="clear"></div>

                    <p class="f-left red-color font-12 margin-bottom-5"><strong>Contact Number: </strong></p>
                    <p class="f-right font-12 ">
                        <info class="customer_contact">MANUAL ORDER</info>
                    </p>

                    <div class="clear"></div>

                    <p class="f-left red-color font-12 margin-bottom-5"><strong>Transaction Time:</strong></p>
                    <p class="f-right font-12 ">
                        <info class="date_added">MANUAL ORDER</info>
                    </p>

                    <div class="clear"></div>
                </div>

                <!-- delivery address -->
                <label class="margin-top-10 margin-bottom-10 ">Delivery Address:</label>
                <container class="customer_address_block">


                    <div class="customer_address_container" style="padding:10px">
                        <div class="bggray-light padding-all-5 font-14 small-curved-border template customer_address">
                            <div class="display-inline-mid margin-right-10 padding-left-20">
                                <img alt="work icon" class="info_address_type" width="35px" height="33px" src="<?php echo base_url() ?>/assets/images/work-icon.png">
                                <p class="font-12 text-center no-margin-all"><strong class="info_address_type"></strong>
                                </p>
                            </div>
                            <div class="display-inline-mid margin-left-10  padding-left-10" style="max-width: 245px">
                                <p class="no-margin-all">
                                    <strong class="info_address_label">Cr8v Web Solutions, Inc.</strong></p>
                                <p class="no-margin-all info_address_complete">66C &amp; 66D, San Rafael St.<br>Brgy. Kapitolyo, Pasig City. - NCR
                                </p>
                                <p class="no-margin-all gray-color info_address_landmark"></p>
                            </div>
                            <div class="display-inline-mid text-center margin-left-10 edit_container hidden">
                                <a class="red-color edit_address">Change<br>Address<br>

                                    <div class="arrow-down"></div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="customer_secondary_address hidden">


                    </div>
                </container>

                <!-- cards -->
                <label class="margin-top-15">CARDS:</label>

                <div class="happy_plus_card_container">
                    <div class="bggray-light margin-bottom-10 padding-all-5 font-14 small-curved-border happy_plus_card template">
                        <div class="display-inline-mid margin-right-10">
                            <img alt="happy-plus" src="<?php echo base_url() ?>/assets/images/happy-plus.jpg" class="thumb">
                        </div>
                        <div class="display-inline-mid margin-left-10 divider padding-left-10">
                            <p class="no-margin-all info-happy-plus-number">0083-123456-46578</p>
                            <span class="red-color"><strong>Exp. Date:</strong></span>
                            <p class="expiration_date" style="display:inline"> September 20, 2016</p></p>
                        </div>
                        <div class="display-inline-mid text-center margin-left-10 f-right">
                            <!-- <a href="#">Show Card<br>History</a> -->
                        </div>
                    </div>
                </div>


                <div class="margin-top-10">
                    <p class="f-left font-12 red-color  margin-bottom-5"><strong>Payment Method: </strong></p>
                    <p class="f-right font-12">
                        <info class="payment_mode">Cash</info>
                    </p>

                    <div class="clear"></div>

                    <p class="f-left font-12 red-color  margin-bottom-5"><strong>Change For: </strong></p>
                    <p class="f-right font-12">
                        <info class="change_for">Cash</info>
                    </p>

                    <div class="clear"></div>

                    <p class="f-left font-12 red-color  margin-bottom-5"><strong>Remarks: </strong></p>
                    <p class="f-right font-12 gray-color">No Remarks</p>

                    <div class="clear"></div>

                    <!--show if advanced order-->
                    <info class="advanced_order" style="display:none">
                        <p class="f-left font-12 red-color  margin-bottom-5"><strong>Flagged for Follow Up: </strong>
                        </p>
                        <p class="f-right font-12 gray-color">
                            <info class="follow_up"></info>
                        </p>

                        <div class="clear"></div>

                        <p class="f-left font-12 red-color  margin-bottom-5"><strong>Delivery Date / Time: </strong></p>
                        <p class="f-right font-12 gray-color">
                            <info class="delivery_date"></info>
                        </p>
                    </info>


                    <div class="clear"></div>

                </div>
            </div>


            <div class="data-container split margin-left-15">
                <label class="margin-top-15">order: </label>

                <div class="order_history_container">


                </div>

            </div>

            <button-group class="locked hidden">
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 modal-trigger order_logs complete_order manual_order rejected_order search_order completed_order verification_order" modal-target="order-logs">Logs</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 modal-trigger manual_order rejected_order manually_relay" modal-target="manual-relay">Manually Relay</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 modal-trigger rejected_order  manual_order verification_order search_order" modal-target="void-order">Void Order</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 editing_order">Cancel Edit Order</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 manual_order edit_order"><i class="fa fa-spinner fa-pulse hidden"></i>Edit Order</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 modal-trigger rejected_order" modal-target="update-address">Update Address</button>
                <?php if ( $this->session->userdata('user_level') == 2 ){ ?> <!--coordinator-->
                    <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 modal-trigger manual_order rejected_order verification_order" modal-target="reroute-order">Re-route Order</button>
                <?php } ?>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 modal-trigger verification_order" modal-target="process-order">Process Order</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 order_lock manual_order rejected_order verification_order" dolock="0">Unlock</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 search_order followup-order">Follow Up Order</button>
            </button-group>
            <div class="clear"></div>

            <button-group class="unlocked hidden">
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 modal-trigger order_logs complete_order manual_order rejected_order search_order completed_order verification_order" modal-target="order-logs">Logs</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 modal-trigger disabled manual_order rejected_order">Manually Relay</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 modal-trigger disabled manual_order verification_order rejected_order search_order" modal-target="void-order">Void Order</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 disabled editing_order">Cancel Edit Order</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 disabled manual_order edit_order">Edit Order</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 modal-trigger rejected_order disabled">Update Address</button>
                <?php if ( $this->session->userdata('user_level') == 2 ){ ?> <!--coordinator-->
                    <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 modal-trigger disabled manual_order rejected_order verification_order">Re-route Order</button>
                <?php } ?>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 modal-trigger disabled verification_order" modal-target="">Process Order</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 order_lock manual_order rejected_order verification_order" dolock="1">Lock</button>
                <button type="button" class="btn hidden btn-dark f-right margin-top-20 margin-right-10 search_order followup-order">Follow Up Order</button>
            </button-group>

            <div class="clear"></div>
        </div>
    </div>
</div>
<!--end coordinator order template-->

<div class="billbreakdown template">
    <p class="font-12 red-color f-left margin-bottom-5"><strong>Name:</strong></p>
    <p class="font-12 f-right"><strong class="info-customer-full-name">Mark Anthony D. Dulay</strong></p>

    <div class="clear"></div>

    <p class="font-12 red-color f-left margin-bottom-5"><strong>Contact Num: </strong></p>
    <p class="font-12 f-right"><strong class="info-customer-mobile">(+63) 939-848-1458
            <i class="fa fa-mobile font-16"></i> TNT</strong></p>

    <div class="clear"></div>

    <p class="font-12 red-color f-left margin-bottom-5"><strong>Alternate Num: </strong></p>
    <p class="font-12 f-right"><strong class="info-customer-mobile-alternate">(+63) 910-576-1248 <i class="fa fa-mobile font-16"></i> Smart</strong></p>

    <div class="clear"></div>

    <!-- address -->
    <div class="bggray-dark">
        <table>
            <tbody>
                <tr>
                    <td>
                        <img src="<?php echo base_url() ?>/assets/images/work-icon.png" alt="working icon" class="margin-top-20 margin-left-10 margin-right-10 address_type_image">
                        <p class="font-12 text-center no-margin-all"><strong class="address_type">WORK</strong></p>
                        <br>
                        <!--                                    <label class="margin-left-10 margin-bottom-20 address_label">Work</label>-->
                    </td>
                    <td>
                        <p class="font-12 margin-left-10"><strong class="address_label">Cr8v Websolutions, Inc.</strong>
                        </p>
                        <p class="font-12 margin-left-10 address_text">66D 2nd Floor, 591 Cr8v Bldg, San Rafael
                            <br>St, Brgy.
                            Kapitolyo, Pasig City - NCR</p>
                        <p class="font-12 gray-color margin-left-10 address_landmark">- Near Jewels Convinience Store</p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <p class="font-12 red-color f-left margin-top-15 margin-bottom-10"><strong>Serving Time:</strong></p>
    <p class="font-12 f-right margin-top-15">20 Minutes</p>

    <div class="clear"></div>

    <hr>
    <div class="margin-top-10">
        <p class="font-12 red-color f-left "><strong>Order Mode: </strong></p>
        <p class="font-12 f-right order_mode">Delivery</p>

        <div class="clear"></div>
    </div>

    <div class="margin-top-5">
        <p class="font-12 red-color f-left"><strong>Transaction Time:</strong></p>
        <p class="font-12 f-right transaction_time"> May 18, 2012 | 1:01 PM</p>

        <div class="clear"></div>
    </div>

    <div class="margin-top-5">
        <p class="font-12 red-color f-left"><strong>Pricing: </strong></p>
        <p class="font-12 f-right pricing_diff"> 10% Delivery Charge with VAT</p>

        <div class="clear"></div>
    </div>
    <div class="margin-top-5">
        <p class="font-12 red-color f-left"><strong>For Special Events? </strong></p>
        <p class="font-12 f-right">No</p>

        <div class="clear"></div>
    </div>
    <div class="margin-top-5">
        <p class="font-12 red-color f-left"><strong>Payment Mode: </strong></p>
        <p class="font-12 f-right payment_type">Cash</p>

        <div class="clear"></div>
    </div>

    <advance class="hidden">


        <div class="margin-top-5 margin-right-5 display-inline-top">
            <label class="margin-top-10">Delivery Date</label>
            <div class="date-picker" id="order-summary-date-picker">
                <input type="text" name="delivery_date" data-date-format="MM/DD/YYYY" readonly>
                <span class="fa fa-calendar text-center red-color"></span>
            </div>
        </div>
        <div class="margin-top-5 display-inline-top">
            <label class="margin-top-10">Delivery Time</label>
            <div class="date-pickertime" id="order-summary-time-picker">
                <input type="text" name="delivery_time" data-date-format="MM/DD/YYYY" readonly>
                <span class="fa fa-clock-o text-center red-color"></span>
            </div>
        </div>

    </advance>

</div>
<!-- end Templates-->

<div class="chat_message template">
    <p class="font-12 data-date-indention">
        <span class="gray-color data-date-sent">May 18, 2015 (12:57 PM)</span><span class="data-username-details"> - MM Sampaloc | JB1021 </span></p>
    <div class="message-identity">
        <img class="sender-profile hidden profile" src="<?php echo assets_url() ?>images/jollibe-face.png">

        <div class="msg">
            <p class="data-message-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis felis consectetur, sodales nunc quis, sagittis augue. Proin eu molestie libero, vitae volutpat eros.</p>
        </div>

        <img class="receiver-profile hidden profile" src="<?php echo assets_url() ?>images/jollibe-face.png">
    </div>
</div>

