<!--Search Order-->
<section id="search_order_form_container" class="container-fluid hidden" section-style="top-panel">
    <!--form search params-->
    <form id="search_order_form_params">
        <input type="hidden" name="order_id"/>
        <input type="hidden" name="province"/>


        <!-- search order -->
        <div class="row header-container">
            <div class="contents">
                <h1 class="f-left"><info class="coordinator-management-header">Search Order</info></h1>

                <div class="f-right">
                    <button class="btn btn-dark modal-trigger margin-top-20 margin-right-10 archive"  modal-target="messenger">Messenger<div class="notify hidden">0</div></button>
                    <!-- <button class="btn btn-dark margin-top-20">FAQ</button> -->
                </div>
                <div class="clear"></div>


            </div>
        </div>


        <div class="row">
            <div class="contents margin-top-20">

                <form-group class="archive_order hidden">
                    <div class="f-left">
                        <label>Date from</label><br>
                        <div class="date-picker">
                            <input data-date-format="MM/DD/YYYY" type="text" name="date_from" readonly="">
                            <span class="fa fa-calendar text-center red-color"></span>
                        </div>
                    </div>

                    <div class="f-left margin-left-15">
                        <label>Date to</label><br>
                        <div class="date-picker">
                            <input data-date-format="MM/DD/YYYY" type="text" name="date_to" readonly="">
                            <span class="fa fa-calendar text-center red-color"></span>
                        </div>
                    </div>
                    <button class="f-right btn btn-dark margin-top-20 margin-left-20 generate_search_order_button">Generate</button>
                    <div class="clear"></div>
                    <hr class="margin-top-15 margin-bottom-15">
                </form-group>


                <div class="f-left">
                    <label class="margin-bottom-5">search:</label><br>
                    <input class="search f-left" type="text" name="search_string">
                    <input class="search f-left" type="hidden" name="order_type">
                    <input class="search f-left" type="hidden" name="province_id" value="All Areas">
                    <input class="search f-left" type="hidden" name="store_id" value="All Stores">
                    <input class="search f-left" type="hidden" name="callcenter_id" value="All Call Centers">
                </div>
                <div class="f-left margin-left-20">
                    <label class="margin-bottom-5">search by:</label><br>

                    <div class="select">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" class="dd-txt" name="search_by" value="Contact Number"></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option" style="display: none;">
                                <div class="option" data-value="Contact Number">Contact Number</div>
                                <div class="option" data-value="Order ID">Order ID</div>
                                <div class="option" data-value="Customer Name">Customer Name</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="">Order ID</option>
                        </select>
                    </div>
                </div>
                <div class="f-left margin-left-20 province_select">
                    <label class="margin-bottom-5">Area:</label><br>

                    <div class="select">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt">
                                <input type="text" class="dd-txt" name="province" value="All Areas"></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option" style="display: none;">
                                <div class="option" data-value="All Areas">All Areas</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="All Areas">All Areas</option>

                        </select>
                    </div>
                </div>
                <button class="f-left btn btn-dark margin-top-20 margin-left-20 search_order_button">
                    <i class="fa fa-spinner fa-pulse hidden"></i>Search
                </button>
            </div>
        </div>

        <div class="row margin-top-10">
            <div class="contents">
                <div class="f-left margin-right-15 stores_select">
                    <label>Stores:</label>
                    <br>

                    <div class="select">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" value="All Stores"></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option">
                                <div class="option" data-value="All Stores">All Stores</div>
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="All Stores">All Stores</option>
                        </select>
                    </div>
                </div>
                <span class="white-space"></span>

                <div class="f-left margin-right-15 callcenters_select">
                    <label>Call Center:</label>
                    <br>

                    <div class="select callcenter-select">
                        <div class="frm-custom-dropdown">
                            <div class="frm-custom-dropdown-txt"><input type="text" class="dd-txt" value="All Call Centers"></div>
                            <div class="frm-custom-icon"></div>
                            <div class="frm-custom-dropdown-option">
                                <div class="option" data-value="All Call Centers">All Call Centers</div>
<!--                                <div class="option" data-value="1">phub</div>-->
<!--                                <div class="option" data-value="2">phub2</div>-->
                            </div>
                        </div>
                        <select class="frm-custom-dropdown-origin" style="display: none;">
                            <option value="All Call Centers">All Call Centers</option>
                        </select>
                    </div>
                </div>
                <span class="white-space"></span>
            </div>
        </div>

        <div class="row margin-top-10 search_result">
            <div class="contents line">

                <div class="f-right bggray-white">
                    <p class="f-left font-12 padding-left-10 padding-top-5">
                        <strong>Sort By:</strong>
                        <input type="hidden" name="search_order_list_orderby" value="" />
                        <input type="hidden" name="search_order_list_sorting" value=""/>
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">
                        <a class="search_order_list_sort search_order_list_sortby_name" data-value="asc" href="javascript:void(0)">
                            <strong>Customer Name </strong><i class="fa"></i>
                        </a>
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-left-5 padding-top-5">
                        <a class="search_order_list_sort search_order_list_sortby_store" data-value="asc" href="javascript:void(0)">
                            <strong>Store </strong><i class="fa"></i>
                        </a>
                    </p>
                    <p class="f-left font-12 padding-left-5 padding-right-5 padding-top-5">|</p>
                    <p class="f-left font-12 padding-top-5">
                        <a class="search_order_list_sort search_order_list_sortby_criticality" data-value="asc" href="javascript:void(0)">
                            <strong>Criticality </strong><i class="fa"></i>
                            <!--img src="<?php echo base_url() ?>/assets/images/ui/sort-top-arrow.png"-->
                        </a>
                    </p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="row margin-top-10 no_search_result hidden">
            <div class="contents line text-right">
                <span class="white-space"> </span>
                <p class="f-right bggray-white font-14 black-color">
                    <strong class="no_search_result">No Search Result</strong></p>
            </div>
        </div>
    </form>
</section>

<section id="search_order_list_container" class="container-fluid hidden" section-style="content-panel">
    <div class="row search_result_container">
        <!-- sample-1-->


    </div>
</section>

<!--Search Order-->

<!--Templates-->

<!--search result-->

