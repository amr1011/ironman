<?php

$lang['save_success'] = "The client %s has been successfully saved";
$lang['dept_save_success'] = "The %s department has been successfully saved";
$lang['rep_save_success'] = "The representative has been successfully saved";
$lang['client_save_success'] = "Client %s has been successfully saved";