<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Authenticated_Controller {

	public function __construct()
	{
		parent::__construct();
        
        $this->load->library('session');

        $allowed_methods = array(
            'index',
            'ajax_add',
            'login',
            'download_csv',
            'delete_csv'
    	);

		if(!isset($_SESSION['crm_user']['sbu'])) {
			$_SESSION['crm_user']['sbu'] = 1;
		}
		
		if(!isset($_SESSION['crm_user']['id'])) {
			$_SESSION['crm_user']['id'] = 1;
		}
		
		if(!isset($_SESSION['crm_user']['province_id'])) {
			$_SESSION['crm_user']['province_id'] = 1;
		}
		
		if(!isset($_SESSION['crm_user']['role_id'])) {
			//1 = admin
			//2 = coordinator
			//3 = admin
			//4 = store
			
			$_SESSION['crm_user']['role_id'] = 3;
		}

        if(!isset($_SESSION['crm_user']['role'])) {
            //1 = super admin
            //2 = coordinator
            //3 = admin
            //4 = store

            $_SESSION['crm_user']['role'] = 1;
        }

        if( $this->session->userdata('username') == '' )
        {
            redirect(base_url()."auth/login");
        }

    }

    public function index ()
    {
        $data = array();
       /* $this->template->add_script(assets_url().'/js/libraries/call_orders.js');
		$this->template->add_script(assets_url().'/js/libraries/customer_list.js');
        $this->template->add_script(assets_url().'/js/libraries/gis.js');
        $this->template->add_script(assets_url().'/js/libraries/order_process.js');*/
        $this->template->add_script(assets_url().'/js/libraries/agents.js');
        $this->template->add_script(assets_url().'/js/libraries/navigation.js');
        $this->template->add_content( $this->load->view( 'admin', $data, TRUE ) );
        $this->template->draw();
    }

    /*adding of customers*/
    public function ajax_add(){
        $customer_api = $this->config->item( 'customers_api' );
        $happy_plus_api  = $this->config->item( 'happy_plus_api' );
        if($this->input->is_ajax_request())
        {
            if($this->input->post('customer_id') != '')
            {
                //edit
            }
            else
            {
                $data = $_POST;

                $api_ironman_return = curl_api( $customer_api . 'customers/add',array(), $data );
                $arr_ironman_return = json_decode( $api_ironman_return );

                if($arr_ironman_return->status == 1)
                {
                    $customer_id = $arr_ironman_return->data->id;
                    $happy_plus_map = array();
                    $arr_ironman_return->data->happy_plus = array();
                    $happy_plus_cards = $this->input->post('happy_plus_cards');
                    if(is_array($happy_plus_cards) AND count($happy_plus_cards) > 0)
                    {
                        foreach($happy_plus_cards as $i => $happy_plus_card)
                        {
                            $happy_return = curl_api( $happy_plus_api . 'happy_plus/add',array(), $happy_plus_card );
                            $happy_return = json_decode( $happy_return );
                            $arr_ironman_return->data->happy_plus[] = $happy_return;

                            if(isset($happy_return) AND $happy_return->status == 1)
                            {
                                $happy_plus_id = $happy_return->data->happy_plus_id;
                                $happy_plus_map['data'][] = array(
                                    'customer_id' => $customer_id,
                                    'happy_plus_id' => $happy_plus_id
                                );
                            }
                            if(isset($happy_return) AND $happy_return->status == 0)
                            {
                                $happy_plus_id = $happy_return->data->happy_plus_id;
                                $happy_plus_map['data'][] = array(
                                    'customer_id' => $customer_id,
                                    'happy_plus_id' => $happy_plus_id
                                );
                            }

                        }

                        $happy_map_return = curl_api( $happy_plus_api . 'happy_plus/map',array(), $happy_plus_map );
                        $happy_map_return = json_decode( $happy_map_return );
                    }
                }


                $finish = 0;
                echo json_encode($arr_ironman_return);
            }

        }
    }

	/* 
		the function that will be used to authenticate pusher private channel
	*/

    public function auth_pusher()
    {
        $channel = $this->input->post("channel_name");
        $socket_id = $this->input->post("socket_id");

        $session = $this->session->userdata;
        $key 	= PUSHER_APP_KEY;
        $secret = PUSHER_APP_SECRET;
        $ap_id 	= PUSHER_APP_ID;
        $pusher = new Pusher($key, $secret, $ap_id);
        echo $pusher->presence_auth($channel, $socket_id, $session, $session);

    }


    public function download_csv(){
        if($this->input->is_ajax_request())
        {
            $post = $this->input->post();

            $userData['headers'] = array('Customer Id', 'First Name', 'Middle Name', 'Last Name', 'Birthdate', 'Email Address', 'Last Order Date', 'Is Senior',
                                        'OSCA Number', 'Is PWD', 'Is Loyalty Card Holder', 'Last Behavior', 'Address', 'Contact Number/s', 'Happy Plus Card/s');
            $userInfo = $post['data'];

            // $user['data'] = explode(', ', $userInfo);

            foreach ($userInfo as $key => $value) {
                $userData['user_'.($key + 1)] = explode(',', $value);
            }

            if(!file_exists($_SERVER['DOCUMENT_ROOT'].'/callcenter/csv_download/'))
            {
                mkdir($_SERVER['DOCUMENT_ROOT'].'/callcenter/csv_download/', 0777, true);
            }

            $fileName = './csv_download/CustomerList_'.date('his').'.csv';

            if ($fileName != "")
            {
                header('Content-Type: application/csv; charset=utf-8');
                header('Content-Disposition: attachement; filename="' . $fileName . '"');
            }

            ob_start();
            $f = fopen($fileName, 'w') or show_error("Can't open php://output");
            $n = 0;
            foreach ($userData as $line)
            {
                $n++;
                if ( ! fputcsv($f, $line))
                {
                    show_error("Can't write line $n: $line");
                }
            }
            fclose($f) or show_error("Can't close php://output");
            $str = ob_get_contents();
            ob_end_clean();

            if ($fileName == "")
            {
                return $str;
            }
            else
            {
                // echo $str;
                echo $file = $fileName;
            }
        }
    }

    public function delete_csv(){
        $this->load->helper('file');
        if($this->input->is_ajax_request())
        {
            if($this->input->post('action') == "delete")
            {
                $filename = $this->input->post('filename');
                unlink($filename);
            }
        }
    }

    public function ajax_upload_logo()
    {
        $return = array();

        if($this->input->is_ajax_request())
        {
            $path_infos = pathinfo('./assets/images/agents');
            if(isset($path_infos['dirname']) AND $path_infos['dirname'] != '' AND file_exists($path_infos['dirname']))
            {
                foreach ($path_infos as $key => $folder)
                {
                    if($key != 'dirname')
                    {
                        @mkdir($path_infos['dirname']."/".$folder, 0777); // this will make/not make the folder, with no errors
                    }
                }
            }
            else
            {
                echo json_encode(array('status'=>FALSE, 'message'=>'Error updating image, please try again later.', 'data'=>array()));
                exit();
            }

            echo json_encode($this->_upload_image($_FILES));

        }
    }

    private function _upload_image($file)
    {
        $_FILES = $file;

        $file_name = uniqid( TRUE );

        if ( ! isset( $_FILES['client_img']['name'] ) )
        {
            return array( 'status' => FALSE );
        }

        $file       = pathinfo( $_FILES['client_img']['name'] );
        $extensions = array( 'jpg', 'jpeg', 'gif', 'png' );
        // kprint($file); exit;
        if ( isset( $file['extension'] ) AND in_array( strtolower( $file['extension'] ), $extensions ) )
        {

            //get width and height of selected image
            list( $width, $height ) = getimagesize( $_FILES['client_img']['tmp_name'] );

            //replace . with _ when found on $file_name
            $file_name = str_replace( '.', '_', $file_name ) . '.' . $file['extension'];

            if(move_uploaded_file($_FILES['client_img']['tmp_name'], './assets/images/agents/' . $file_name))
            {
                $data['image_height'] = $height;
                $data['image_width'] = $width;
                $data['image_name'] = $file_name;
                $data['image_path'] = base_url() . '/assets/images/agents/' . $file_name;

                return array('status'=>TRUE, 'message'=>'Image file successfully updated!', 'data'=>$data);
            }
            else
            {
                return array('status'=>FALSE, 'message'=>'Error adding image file, please try again later.', 'data'=>array());
            }

        }
        else
        {
            return array('status'=>FALSE, 'message'=>'Invalid image file, please try again.', 'data'=>array());
        }
    }

    public function ajax_upload_promo()
    {
        $return = array();

        if($this->input->is_ajax_request())
        {
            $path_infos = pathinfo('./assets/images/promos');
            if(isset($path_infos['dirname']) AND $path_infos['dirname'] != '' AND file_exists($path_infos['dirname']))
            {
                foreach ($path_infos as $key => $folder)
                {
                    if($key != 'dirname')
                    {
                        @mkdir($path_infos['dirname']."/".$folder, 0777); // this will make/not make the folder, with no errors
                    }
                }
            }
            else
            {
                echo json_encode(array('status'=>FALSE, 'message'=>'Error updating image, please try again later.', 'data'=>array()));
                exit();
            }

            echo json_encode($this->_upload_promo_image($_FILES));

        }
    }

    private function _upload_promo_image($file)
    {
        $_FILES = $file;

        $file_name = uniqid( TRUE );

        if ( ! isset( $_FILES['client_img']['name'] ) )
        {
            return array( 'status' => FALSE );
        }

        $file       = pathinfo( $_FILES['client_img']['name'] );
        $extensions = array( 'jpg', 'jpeg', 'gif', 'png' );
        // kprint($file); exit;
        if ( isset( $file['extension'] ) AND in_array( strtolower( $file['extension'] ), $extensions ) )
        {

            //get width and height of selected image
            list( $width, $height ) = getimagesize( $_FILES['client_img']['tmp_name'] );

            //replace . with _ when found on $file_name
            $file_name = str_replace( '.', '_', $file_name ) . '.' . $file['extension'];

            if(move_uploaded_file($_FILES['client_img']['tmp_name'], './assets/images/promos/' . $file_name))
            {
                $data['image_height'] = $height;
                $data['image_width'] = $width;
                $data['image_name'] = $file_name;
                $data['image_path'] = base_url() . 'assets/images/promos/' . $file_name;

                return array('status'=>TRUE, 'message'=>'Image file successfully updated!', 'data'=>$data);
            }
            else
            {
                return array('status'=>FALSE, 'message'=>'Error adding image file, please try again later.', 'data'=>array());
            }

        }
        else
        {
            return array('status'=>FALSE, 'message'=>'Invalid image file, please try again.', 'data'=>array());
        }
    }

	
}

/* End of file clients.php */
/* Location: ./application/modules/clients/controllers/clients.php */