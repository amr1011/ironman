<?php 

class write_file extends Authenticated_Controller
{
	public function __construct()
	{
		parent :: __construct();
		
		$this->load->helper ('file');
	}
	
	public function write_province()
	{
		$response_data = array();
		
		/* get callcenters */
		$users_api = 'http://ni-users-service-app.cr8vwebsolutions.net/api.jfc.users/';
		$callcenters = curl_api($users_api . "users/callcenter/call" , array('params' => array()));
		if (isset($callcenters) && $callcenters !== NULL)
		{
			$arr_callcenter = json_decode($callcenters, TRUE);
			if ( isset($arr_callcenter['data']))
			{
				foreach ($arr_callcenter['data'] as $c)
				{
					/* generate provinces based on the callcenter id */
					$qualifier = array(
											'call_center_user_id' => $c['id'],
											'limit' => 9999999999999999
									   );
					
					/* assembly of provinces */
					$provinces = curl_api($this->config->item('gis_api') . "gis/province_address", $qualifier);
					$p = json_decode($provinces, TRUE);
				
					$content = "var oFileProvince = " . json_encode($p['data']['provinces']) . ";";
				
					
					$path = $_SERVER['DOCUMENT_ROOT'] . "callcenter-domski/assets/js/cache_files/";
				
					$write_file_province = write_file($path . "province_". $c['id']. ".js", $content);
					
					echo "<h3> Writing Provinces:</h3>";
					echo "writing file to : " . $path;
					echo "<br/>" ;
					echo "writeable:" . (int)is_writable($path);
					echo "<br/>";
					echo "exists:" . (int)file_exists($path);
					
					if ($write_file_province)
					{
						echo $path . "province_". $c['id']. ".js successfully created.";
					} else {
						echo "failed to create file: " . $path . "province_". $c['id']. ".js successfully created.";
					}
					echo " <br/>=========================<br/>";
				}	
			}
		}
	}	
		
		public function write_cities ()
		{
			$users_api = 'http://ni-users-service-app.cr8vwebsolutions.net/api.jfc.users/';
			$callcenters = curl_api($users_api . "users/callcenter/call" , array('params' => array()));
			if (isset($callcenters) && $callcenters !== NULL)
			{
			
				$arr_callcenter = json_decode($callcenters, TRUE);
				if ( isset($arr_callcenter['data']))
				{
					foreach ($arr_callcenter['data'] as $c)
					{
						$qualifier = array(
							'call_center_user_id' => $c['id'],
							'limit' => 9999999999999999
							);
						/* writing Cities */
					
						$cities = curl_api($this->config->item('gis_api') . "gis/city_address", $qualifier);
							$path = $_SERVER['DOCUMENT_ROOT'] . "callcenter-domski/assets/js/cache_files/";
				
						$arr_cities = json_decode($cities, TRUE);
						$content = "var oFileCities = " . json_encode($arr_cities['data']['cities']) . ";";
						
						$write_file_cities = write_file($path . "cities_". $c['id']. ".js", $content);
						echo "<h3> Writing Cities:</h3>";
						echo "writing file to : " . $path;
						echo "<br/>" ;
						echo "writeable:" . (int)is_writable($path);
						echo "<br/>";
						echo "exists:" . (int)file_exists($path);
						
						if ($write_file_cities)
						{
							echo $path . "cities_". $c['id']. ".js successfully created.";
						} else {
							echo "failed to create file: " . $path . "cities_". $c['id']. ".js successfully created.";
						}
						echo " <br/>=========================<br/>";
					
					}
				}
			}
		}

		
		public function write_barangays()
		{
			$users_api = 'http://ni-users-service-app.cr8vwebsolutions.net/api.jfc.users/';
			$callcenters = curl_api($users_api . "users/callcenter/call" , array('params' => array()));
			if (isset($callcenters) && $callcenters !== NULL)
			{
			
				$arr_callcenter = json_decode($callcenters, TRUE);
				if ( isset($arr_callcenter['data']))
				{
					foreach ($arr_callcenter['data'] as $c)
					{
						/* for other address - Barangay */
						$qualifiers = array(
							 'location_type_id'   => 4,
							'call_center_user_id' => $c['id'],
							'limit' => 9999999999999999
						);
						$barangay = curl_api($this->config->item('gis_api') . "gis/other_address", $qualifiers);
						
						$arr_barangay = json_decode($barangay, TRUE);
						$content = "var oFileBarangays = " . json_encode($arr_barangay['data']['barangays']) . ";";
						$path = $_SERVER['DOCUMENT_ROOT'] . "callcenter-domski/assets/js/cache_files/";
						
						$write_file = write_file($path . "barangays_". $c['id']. ".js", $content);
						echo "<h3> Writing Barangay:</h3>";
						echo "writing file to : " . $path;
						echo "<br/>" ;
						echo "writeable:" . (int)is_writable($path);
						echo "<br/>";
						echo "exists:" . (int)file_exists($path);
						
						if ($write_file)
						{
							echo $path . "barangays_". $c['id']. ".js successfully created.";
						} else {
							echo "failed to create file: " . $path . "barangays_". $c['id']. ".js successfully created.";
						}
						echo " <br/>=========================<br/>";
					}
				}
			}
		}
		
	
	
	public function write_subdivisions()
	{
		$users_api = 'http://ni-users-service-app.cr8vwebsolutions.net/api.jfc.users/';
		$callcenters = curl_api($users_api . "users/callcenter/call" , array('params' => array()));
		if (isset($callcenters) && $callcenters !== NULL)
		{
		
			$arr_callcenter = json_decode($callcenters, TRUE);
			if ( isset($arr_callcenter['data']))
			{
				foreach ($arr_callcenter['data'] as $c)
				{
						/* for other address - subdivision */
					$qualifiers = array(
						 'location_type_id'   => 3,
						'call_center_user_id' => $c['id'],
						'limit' => 9999999999999999
					);
					$barangay = curl_api($this->config->item('gis_api') . "gis/other_address", $qualifiers);
					
					$arr_barangay = json_decode($barangay, TRUE);
					// echo "<pre>";
					// print_r($arr_barangay);exit;
					$content = "var oFileSubdivisions = " . json_encode($arr_barangay['data']['subdivisions']) . ";";
					$this->load->helper('file');
					
					$path = $_SERVER['DOCUMENT_ROOT'] . "callcenter-domski/assets/js/cache_files/";
				
					$write_file = write_file($path . "subdivisions_". $c['id']. ".js", $content);
					echo "<h3> Writing Cities:</h3>";
					echo "writing file to : " . $path;
					echo "<br/>" ;
					echo "writeable:" . (int)is_writable($path);
					echo "<br/>";
					echo "exists:" . (int)file_exists($path);
					
					if ($write_file)
					{
						echo $path . "subdivisions_". $c['id']. ".js successfully created.";
					} else {
						echo "failed to create file: " . $path . "subdivisions_". $c['id']. ".js successfully created.";
					}
					echo " <br/>=========================<br/>";
					
					
					
				}
			}
		}
	}

	
	
	public function write_streets()
	{
		$users_api = 'http://ni-users-service-app.cr8vwebsolutions.net/api.jfc.users/';
		$callcenters = curl_api($users_api . "users/callcenter/call" , array('params' => array()));
		if (isset($callcenters) && $callcenters !== NULL)
		{
		
			$arr_callcenter = json_decode($callcenters, TRUE);
			if ( isset($arr_callcenter['data']))
			{
				foreach ($arr_callcenter['data'] as $c)
				{
						/* for other address - subdivision */
					$qualifiers = array(
						 'location_type_id'   => 2,
						'call_center_user_id' => $c['id'],
						'limit' => 9999999999999999
					);
					$barangay = curl_api($this->config->item('gis_api') . "gis/other_address", $qualifiers);
					
					$arr_barangay = json_decode($barangay, TRUE);
					// echo "<pre>";
					// print_r($arr_barangay);exit;
					$content = "var oFileStreets = " . json_encode($arr_barangay['data']['streets']) . ";";
					$this->load->helper('file');
					
					$path = $_SERVER['DOCUMENT_ROOT'] . "callcenter-domski/assets/js/cache_files/";
				
					$write_file = write_file($path . "streets_". $c['id']. ".js", $content);
					echo "<h3> Writing Cities:</h3>";
					echo "writing file to : " . $path;
					echo "<br/>" ;
					echo "writeable:" . (int)is_writable($path);
					echo "<br/>";
					echo "exists:" . (int)file_exists($path);
					
					if ($write_file)
					{
						echo $path . "streets_". $c['id']. ".js successfully created.";
					} else {
						echo "failed to create file: " . $path . "streets_". $c['id']. ".js successfully created.";
					}
					echo " <br/>=========================<br/>";

				}
			}
		}
	}
	
	
	public function write_buildings($callcenter_id = 0)
	{
		$users_api = 'http://ni-users-service-app.cr8vwebsolutions.net/api.jfc.users/';
		$callcenters = curl_api($users_api . "users/callcenter/call" , array('params' => array()));
		if (isset($callcenters) && $callcenters !== NULL)
		{
		
			$arr_callcenter = json_decode($callcenters, TRUE);
			if ( isset($arr_callcenter['data']))
			{
				/* foreach ($arr_callcenter['data'] as $c)
				{ */
						/* for other address - subdivision */
					$qualifiers = array(
						 'location_type_id'   => 1,
						'call_center_user_id' => $callcenter_id,
						'limit' => 9999999999999999
					);
					$barangay = curl_api($this->config->item('gis_api') . "gis/other_address", $qualifiers);
					
					$arr_barangay = json_decode($barangay, TRUE);
					// echo "<pre>";
					// print_r($arr_barangay);exit;
					$content = "var oFilePOI = " . json_encode($arr_barangay['data']['buildings']) . ";";
					$this->load->helper('file');
					
					$path = $_SERVER['DOCUMENT_ROOT'] . "callcenter-domski/assets/js/cache_files/";
				
					$write_file = write_file($path . "poi_". $callcenter_id. ".js", $content);
					echo "<h3> Writing Cities:</h3>";
					echo "writing file to : " . $path;
					echo "<br/>" ;
					echo "writeable:" . (int)is_writable($path);
					echo "<br/>";
					echo "exists:" . (int)file_exists($path);
					
					if ($write_file)
					{
						echo $path . "poi_". $callcenter_id . ".js successfully created.";
					} else {
						echo "failed to create file: " . $path . "poi_". $callcenter_id. ".js successfully created.";
					}
					echo " <br/>=========================<br/>";

				/* } */
			}
		}
	}
}