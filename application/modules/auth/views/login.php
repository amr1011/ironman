
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keywords" content="">
        <link rel="shortcut icon" href="assets/ico/favicon.ico">

        <title>Ironman Category Management | Add Category</title>

        <script type="text/javascript" src="<?php echo assets_url() ?>js/jQueries/jquery-1.11.1.js"></script>
        <script type="text/javascript" src="<?php echo assets_url() ?>js/jQueries/jquery.cookie.js"></script>
        <script type="text/javascript" src="<?php echo assets_url() ?>js/core/platform.js"></script>
        <script type="text/javascript" src="<?php echo assets_url() ?>js/plugins/CconnectionDetector.js"></script>
        <script type="text/javascript" src="<?php echo assets_url() ?>js/libraries/auth.js"></script>
        <script type="text/javascript" src="<?php echo assets_url() ?>js/core/config.js"></script>
        <script type="text/javascript" src="<?php echo assets_url() ?>js/plugins/ajaxq.js"></script>
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url()?>assets/css/bootstrap/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/css/bootstrap/bootstrap-theme.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="<?php echo base_url()?>assets/css/font-awesome/font-awesome.min.css" rel="stylesheet">

        <!-- Core CSS -->
        <link href="<?php echo base_url()?>assets/css/global/commons.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="<?php echo base_url()?>assets/css/global/header.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/css/global/ui-widgets.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/css/global/unique-widget-style.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/css/global/section-top-panel.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/css/global/section-content-panel.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/css/global/login.css" rel="stylesheet">
        <link href="<?php echo base_url()?>assets/css/global/login.css" rel="stylesheet">

    </head>
    <body>
        <section class="login-parent">
            <div class="login-form">
                <form id="login-form">
                    <img src="<?php echo base_url()?>assets/images/affliates/jb-logo.png" alt="jollibee logo">
                    <div class="error-msg login-error-msg no-margin-bottom">
                        <i class="fa fa-exclamation-triangle"></i>
                        Please fill up all required fields.
                    </div>
                    <div class="form-group">
                        <p>Username</p>
                        <input type="text" class="normal" name="username">
                    </div>
                    <div class="form-group">
                        <p>Password</p>
                        <input type="password" class="normal" name="password">
                    </div>
                    
                    <input type="hidden" class="normal" name="type">
                    
                    <!-- <p><a href="#">Forgot your password?</a></p> -->
                    <button type="button" class="btn btn-dark btn-login"> Sign in</button>
                </form>

                <div class="image-content1">
                    <table border="0">
                        <tr>
                            <td>
                                <a href="#" alt="Jollibee Link">
                                    <img src="<?php echo base_url()?>assets/images/affliates/jb-logo.png" alt="Jollibee Logo">
                                </a>
                            </td>
                            <td>
                                <a href="#" alt="Greenwich Link">
                                    <img src="<?php echo base_url()?>assets/images/affliates/gw-logo.png" alt="Greenwich Logo">
                                </a>
                            </td>
                            <td>
                                <a href="#" alt="Chowking Link">
                                    <img src="<?php echo base_url()?>assets/images/affliates/ck-logo.png" alt="Chowking Logo">
                                </a>
                            </td>

                            <td>
                                <a href="#" alt="Mang Inasal Link">
                                    <img src="<?php echo base_url()?>assets/images/affliates/mi-logo.png" alt="MangInasal Logo">
                                </a>
                            </td>
                            <td>
                                <a href="#" alt="RedRibbon Logo">
                                    <img src="<?php echo base_url()?>assets/images/affliates/rb-logo.png" alt="RedRibbon Logo">
                                </a>
                            </td>
                            <td>
                                <a href="#" alt="Burger King Logo">
                                    <img src="<?php echo base_url()?>assets/images/affliates/bk-logo.png" alt="BurgerKing Logo" style="width:54px;">
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- <div class="image-content2">
                    <table border="0">
                        <tr>
                            <td>
                            </td>
                            <td>
                                <a href="#" alt="RedRibbon Logo">
                                    <img src="<?php echo base_url()?>assets/images/affliates/rb-logo.png" alt="RedRibbon Logo">
                                </a>
                            </td>
                            <td>
                                <a href="#" alt="Burger King Logo">
                                    <img src="<?php echo base_url()?>assets/images/affliates/bk-logo.png" alt="BurgerKing Logo">
                                </a>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div> -->
            </div> 

        </section>
    </body>
</html>