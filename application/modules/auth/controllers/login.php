<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends Authenticated_Controller {

	public function index()
    {
        $data = array();
//        $this->template->add_style(assets_url().'/css/font-awesome/font-awesome.css');
//        $this->template->add_script(assets_url().'/js/jQueries/jquery-1.11.1.js');
//        $this->template->add_script(assets_url().'/js/jQueries/jquery-te-1.4.0.min.js');
//        $this->template->add_script(assets_url().'/js/extras/php.full.min.js');
//        $this->template->add_script(assets_url().'/js/bootstrap/bootstrap.min.js');
//        $this->template->add_script(assets_url().'/js/jQueries/jquery.cookie.js');
//        $this->template->add_script(assets_url().'/js/jQueries/jquery.tinysort.min.js');
//
//        $this->template->add_script(assets_url().'/js/plugins/integr8formvalidation.js',true);
//        $this->template->add_script(assets_url().'/js/core/platform.js');
//        $this->template->add_script(assets_url().'/js/core/config.js');
//        $this->template->add_script(assets_url().'/js/core/memory_storage_engine.js');
//        $this->template->add_script(assets_url().'/js/core/ajax.js');
//        $this->template->add_script(assets_url().'/js/core/ui.js');
//        $this->template->add_script(assets_url().'/js/core/memory_storage_engine.js');
//        $this->template->add_script(assets_url().'/js/core/errors.js');
//        $this->template->add_script(assets_url().'/js/plugins/ajaxq.js');
//        $this->template->add_script(assets_url().'/js/plugins/offline.min.js');
//        $this->template->add_script(assets_url().'/js/plugins/CconnectionDetector.js');
//
//        $this->template->add_script(assets_url().'/js/libraries/call_orders.js');
//        $this->template->add_script(assets_url().'/js/libraries/customer_list.js');
//        $this->template->add_script(assets_url().'/js/libraries/gis.js');
//        $this->template->add_script(assets_url().'js/libraries/auth.js');
//
//        $this->template->add_content(  );
//        $this->template->draw();

        $this->load->view( 'auth/login', $data );
    }

    // public function landing_page($user_info)
    // {
    //     $url = urldecode($user_info);
    //     $data = json_decode($url);
    //     $array = array();

    //     foreach ($data as $k => $v)
    //     {
    //         $array['user_info'][$k] = $v;
    //     }

    //     $this->load->view('landing_page', $array);
    // }

    public function landing_page()
    {
        $return = array();
        $data = array();
        $response = array(
            "status" => FALSE,
            "message" => array(),
            "data" => array()
        );

        if($this->input->is_ajax_request())
        {
            $params = $this->input->post("params");
            $user_info = $params;
    
            $newdata = array(
                'user_id'            => $user_info['user_id'],
                'username'           => $user_info['username'],
                'first_name'         => $user_info['first_name'],
                'last_name'          => $user_info['last_name'],
                'callcenter_id'      => $user_info['callcenter_id'],
                'callcenter_user_id' => $user_info['callcenter_user_id'],
                'user_level'         => $user_info['user_level'],
                'agent_provinces'    => $user_info['agent_provinces'],
                'image_file'         => $user_info['image_file'],
                'logged_in'          => TRUE
            );

            $this->session->set_userdata($newdata);
            
            // echo "<h2>SESSIONS</h2>";
            // var_dump($this->session->userdata);
            $response['status'] = TRUE;
        }
        echo $response;
    }

    public function logout(){
        $newdata = array(
            'user_id'               => $this->session->userdata('user_id'),
            'username'              => $this->session->userdata('username'),
            'first_name'            => $this->session->userdata('first_name'),
            'last_name'             => $this->session->userdata('last_name'),
            'callcenter_id'         => $this->session->userdata('callcenter_id'),
            'callcenter_user_id'    => $this->session->userdata('callcenter_user_id'),
            'user_level'            => $this->session->userdata('user_level'),
            'image_file'            => $this->session->userdata('image_file'),
            'logged_in'             => FALSE
        );

        $this->session->unset_userdata($newdata);
        $this->session->sess_destroy();
        redirect(base_url()."auth/login");
    }

    public function server_checklist() {

        if($this->input->is_ajax_request())
        {
            $access = ip_check();
            echo json_encode($access);
        }

    }

}
