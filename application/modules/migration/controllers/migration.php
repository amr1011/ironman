<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        
        $this->load->library('session');

        $allowed_methods = array(
            'index',
            
    	);
		
		if(!isset($_SESSION['crm_user']['sbu'])) {
			$_SESSION['crm_user']['sbu'] = 1;
		}
		
		if(!isset($_SESSION['crm_user']['id'])) {
			$_SESSION['crm_user']['id'] = 1;
		}
		
		if(!isset($_SESSION['crm_user']['province_id'])) {
			$_SESSION['crm_user']['province_id'] = 1;
		}
		
		if(!isset($_SESSION['crm_user']['role_id'])) {
			//1 = admin
			//2 = coordinator
			//3 = admin
			//4 = store
			
			$_SESSION['crm_user']['role_id'] = 3;
		}

        if(!isset($_SESSION['crm_user']['role'])) {
            //1 = super admin
            //2 = coordinator
            //3 = admin
            //4 = store

            $_SESSION['crm_user']['role'] = 1;
        }

    }


	public function users()
    {
        $data = array();
        
		$this->load->view('users',$data);
       
    }
	
	public function customers()
    {
        $data = array();
        
		$this->load->view('customers',$data);
       
    }
	
	public function orders($order_type = '')
    {
        $data = array();
		
		if($order_type == 'manual'){
			$order_val = 3;
		}elseif($order_type == 'advance'){
			$order_val = 2;
		/* }elseif($order_type == 'normal'){
			$order_val = 1; */
		}else{
			// $order_val = 0;
			$order_val = 1;
		}
		
		$data['order_type'] = $order_val;
		
        
		$this->load->view('orders',$data);
       
    }
	
	public function products()
    {
        $data = array();
        
		$this->load->view('products',$data);
       
    }
	
	public function ironman_to_xavier_user(){
		$data = array();
        
		$this->load->view('nx_users',$data);
	}

}
