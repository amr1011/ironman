	</body>
	<script src="<?php echo base_url() ?>/assets/js/commons/main.js"></script>
	<script src="<?php echo base_url() ?>/assets/js/plugins/moment.min.js"></script>	
	<script src="<?php echo base_url() ?>/assets/js/plugins/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo base_url() ?>/assets/js/libraries/customdropdown.js"></script>
	<script src="<?php echo base_url() ?>/assets/js/libraries/search_autocomplete.js"></script>
	<script src="<?php echo base_url() ?>/assets/js/libraries/dst_reports_store/reports_store_core.js"></script>
	<script src="<?php echo base_url() ?>/assets/js/libraries/dst_reports_store/reports_store_supply_data.js"></script>
	<script src="<?php echo base_url() ?>/assets/js/libraries/dst_reports_store/reports_store_events.js"></script>
	<script src="<?php echo base_url() ?>/assets/js/plugins/jquery.mask.min.js"></script>
	<script src="<?php echo base_url() ?>/assets/js/plugins/jquery.tooltipster.min.js"></script>

<!--	<script src="--><?php //echo base_url() ?><!--/assets/js/extras/websocket_events.js"></script>-->
<!--	<script src="--><?php //echo base_url() ?><!--/assets/js/extras/timer.js"></script>-->
	<script>
		/** 
		 *	subscription to websocket channel
		 *	
		 *	see event bindings at: 
		 *		assets/js/extras/websocket_events.js
		*/
//		var sCallcenter = "spi"; // the callcenter name
//		oSock.subscribe("presence-" + sCallcenter);

		$('body').find('input[type="text"]').attr('autocomplete', 'off');

	</script>
<div id = 'benchmark-realtime-update'>
	
</div>
</html> 