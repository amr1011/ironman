<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">

    <title>Ironman Category Management | Add Category</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url() ?>/assets/css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/assets/css/bootstrap/bootstrap-theme.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url() ?>/assets/css/font-awesome/font-awesome.min.css" rel="stylesheet">

    <!-- Core CSS -->
    <link href="<?php echo base_url() ?>/assets/css/global/commons.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url() ?>/assets/css/global/header.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/assets/css/global/ui-widgets.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/assets/css/global/unique-widget-style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/assets/css/global/section-top-panel.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/assets/css/global/section-content-panel.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/assets/css/tooltipster/tooltipster.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>/assets/css/tooltipster/themes/tooltipster-shadow.css" rel="stylesheet">
    <script type='text/javascript' src='<?php echo base_url(); ?>assets/js/libraries/pusher.js'></script>
    <script type='text/javascript' src='<?php echo base_url(); ?>assets/js/libraries/websocket.js'></script>
    <script type='text/javascript' src='<?php echo base_url(); ?>assets/js/extras/websocket_events.js'></script>
    <script type='text/javascript' src='<?php echo base_url(); ?>assets/js/libraries/upsell_promos.js'></script>
    <script type='text/javascript' src='<?php echo base_url(); ?>assets/js/libraries/contact_reports.js'></script>
    <script type='text/javascript' src='<?php echo base_url(); ?>assets/js/libraries/product_unavailability_report.js'></script>
    <script type='text/javascript' src='<?php echo base_url(); ?>assets/js/libraries/order_reports.js'></script>
    <script type='text/javascript' src='<?php echo base_url(); ?>assets/js/plugins/jquery.table2excel.js'></script>
    <script type='text/javascript' src='<?php echo base_url(); ?>assets/js/libraries/auth.js'></script>
    <script type='text/javascript' src='<?php echo base_url(); ?>assets/js/libraries/stores.js'></script>
    <script type='text/javascript' src='<?php echo base_url(); ?>assets/js/libraries/messaging.js'></script>
    <script type='text/javascript' src='<?php echo base_url(); ?>assets/js/libraries/hourly_sales_report.js'></script>
    <script type='text/javascript' src='<?php echo base_url(); ?>assets/js/libraries/audit_trails.js'></script>
    <script src="<?php echo base_url() ?>/assets/js/plugins/moment.min.js"></script>

   
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<?php
	/*
		INCLUDING THE CACHE FILE BASED ON THE CALLCETER ID OF THE USER.
	*/
	?>
	<script type = 'text/javascript' src = '<?php echo assets_url();?>js/cache_files/province_1.js'></script>
	<script type = 'text/javascript' src = '<?php echo assets_url();?>js/cache_files/cities_1.js'></script>
	<script type = 'text/javascript' src = '<?php echo assets_url();?>js/cache_files/streets_1.js'></script>
	<script type = 'text/javascript' src = '<?php echo assets_url();?>js/cache_files/barangays_1.js'></script>
	<script type = 'text/javascript' src = '<?php echo assets_url();?>js/cache_files/poi_1.js'></script>
	<script type = 'text/javascript' src = '<?php echo assets_url();?>js/cache_files/subdivisions_1.js'></script>
	
	
     <script type='text/javascript'>
        localStorage.setItem("base_url", "<?php echo base_url(); ?>");
		
		var iConstantUserId = <?php echo $this->session->userdata('user_id'); ?>;
		var sConstantUserName = '<?php echo $this->session->userdata('username'); ?>';
		var sConstantFirstName = '<?php echo $this->session->userdata('first_name'); ?>';
		var sConstantLastname = '<?php echo $this->session->userdata('last_name'); ?>';
		var iConstantCallcenterId = '<?php echo $this->session->userdata('callcenter_id'); ?>';;
		var iConstantCallcenterUserId = 3305;
		var iConstantUserLevel = <?php echo $this->session->userdata('user_level'); ?>;
        var arrConstantCallcenterProvinces = [1,2];
        var iConstantSbuId = 1;
        var oProvinces;
        var iGrandTotal;
        var oPusherStatus;
		/* do not delete this will be used to catch local file data */
		 var oAutoComplete = {
				oProvinces  : oFileProvince,
				oCities     : oFileCities,
				oPOI        : oFilePOI,
				oBarangay   : oFileBarangays,
				oStreet     : oFileStreets,
				oSubdivision: oFileSubdivisions,

				oPOIFiltered        : [],
				oBarangayFiltered   : [],
				oStreetFiltered     : [],
				oSubdivisionFiltered: []
			};

        //for the api redirection on callcenter module
        var oUserData = {
            'user_id' : '<?php echo $this->session->userdata('user_id'); ?>',
            'username' : '<?php echo $this->session->userdata('username'); ?>',
            'first_name' : '<?php echo $this->session->userdata('first_name'); ?>',
            'last_name' : '<?php echo $this->session->userdata('last_name'); ?>',
            'callcenter_id' : '<?php echo $this->session->userdata('callcenter_id'); ?>',
            'callcenter_user_id' : '<?php echo $this->session->userdata('callcenter_user_id'); ?>',
            'user_level' : '<?php echo $this->session->userdata('user_level'); ?>',
            'image_file' : '<?php echo $this->session->userdata('image_file'); ?>',
            'province_id' : '<?php echo $this->session->userdata('province_id'); ?>',
            'agent_provinces' : '<?php echo $this->session->userdata('agent_provinces'); ?>',
            'role_id' : '<?php echo $this->session->userdata('user_level'); ?>'
        };
        var oUserInfo = encodeURIComponent(JSON.stringify(oUserData));
    </script>
    <script>
        $(document).ready(function(){
            var sCurrent = moment().format('MMMM Do YYYY hh:mm:ss A');
            setInterval(function() {
                sCurrent = moment().format('MMMM Do YYYY hh:mm:ss A');
                $('div.profile-container').find('p.current_time').text(sCurrent);
                $('div.profile-container').find('.pusher_status').text(oPusherStatus).css('text-transform', 'capitalize');
            }, 1000);
        })
    </script>

    <!-- Common Libraries -->
    <script type = 'text/javascript' src = '<?php echo assets_url();?>js/libraries/store_operations.js'></script>
    <script type = 'text/javascript' src = '<?php echo assets_url();?>js/libraries/security_settings.js'></script>
    <script type = 'text/javascript' src = '<?php echo assets_url();?>js/libraries/dst_reports.js'></script>
    <script type = 'text/javascript' src = '<?php echo assets_url();?>js/libraries/product_sales_report.js'></script>
    <script type = 'text/javascript' src = '<?php echo assets_url();?>js/libraries/sales_report.js'></script>
    <script type = 'text/javascript' src = '<?php echo assets_url();?>js/libraries/category_sales_report.js'></script>
    <script type = 'text/javascript' src = '<?php echo assets_url();?>js/plugins/jquery.twbsPagination.js'></script>

    <!-- Pagination For Reports -->
    <script type='text/javascript' src='<?php echo base_url(); ?>assets/js/libraries/pagination_report.js'></script>

    <style id="holderjs-style" type="text/css"></style>
</head>
<link href="<?php echo base_url() ?>/assets/css/global/splash.css" rel="stylesheet">
<section class="splash-container">
    <div class="splash-content text-center">
       <img src="<?php echo base_url() ?>assets/images/jollibee-splash.png"/>
       <div class="loader">
            <div class="prog-bar" style="width:0%"></div>
        </div>
        <h3><span class='loaded'>0</span>% Loaded</h3>

    </div>
</section>
<header class="container-fluid hidden" custom-style="header">
    <div class="row">
        <div class="col-xs-12">
            <h1 style="
                position: absolute;
                font-size: 2vw;
                color: #fff;
                width: calc(100% - 642px);
                margin-left: 150px;
                text-align: center;">
                ADMIN
            </h1>
            <div class="logo-container f-left">
                <img class="img-responsive" src="<?php echo base_url() ?>/assets/images/jollibe-logo-call-center.svg" alt="jolibee logo"/>
            </div>

            <div class="profile-container f-right">


                <div class="text-right">
                    <p>Status: <span class="pusher_status">Online</span> <!-- | 100ms --></p><br/>
                    <p class="current_time"></p>
                </div>

                <!-- NOTE: **required to input user's profile name inttials(str) at hte 'prof-name' attr -->
                <?php session_start(); ?>
                <?php if(strlen($this->session->userdata('image_file')) > 0) { ?>

                    <div class="profile-pic" prof-name="HP">
                        <img class="img-circle img-responsive" style="vertical-align: middle;height: 40px;" src="<?php echo base_url(); ?>assets/images/agents/<?php echo $this->session->userdata('image_file') ?>">
                    </div>

                <?php } else { ?>

                    <div class="sphere-small">
                        <p class="font-20 info_initials" style="font-size:20px !important; padding-top: 10px;  font-size: 17px !important"><?php echo substr(ucfirst($this->session->userdata('first_name')), 0, 1) ?><?php echo substr(ucfirst($this->session->userdata('last_name')), 0, 1) ?></p>
                    </div>

                <?php } ?>

                <div class="text-left">
                    <p>
                        <?php echo ($this->session->userdata('first_name') !== '' ? $this->session->userdata('first_name') : '')." ".($this->session->userdata('last_name') !== '' ? $this->session->userdata('last_name') : '');?>
                        <br/>
                        <sub><?php echo ($this->session->userdata('user_level') === '1' ? 'Agent': ''); echo ($this->session->userdata('user_level') === '2' ? 'Coordinator': ''); echo ($this->session->userdata('user_level') === '3' ? 'Admin': ''); echo ($this->session->userdata('user_level') === '4' ? 'Management': ''); echo ($this->session->userdata('user_level') === '6' ? 'Store Admin': '');?></sub>
                    </p>
                </div>

                <button class="btn btn-logout">Logout</button>
            </div>
        </div>
    </div>

    <div class="row">
            <nav>
                <ul class="main_nav" id="main_nav_headers">
                    <?php if($this->session->userdata('user_level') !== '6') {//exclude store admin ?>
                    <li content="agent_list" class="active"><a>Users List</a></li>
                    <?php } ?>
                    <!--<li content="data_manager_list"><a>Data Managers List</a></li>-->
                    <li content="store_management_list"><a>Store Management</a></li>
                    <?php if($this->session->userdata('user_level') !== '6') {//exclude store admin ?>

                        <li content="messaging"><a>Messaging</a></li>
                        <li content="promos"><a>Upsell & Promos</a></li>

                        <?php if($this->session->userdata('user_level') !== '4'){//exclude management?>
                        <li content="store_operations"><a>Store Operations</a></li>
                        <li content="security_settings"><a>Security Settings</a></li>
                        <li content="audit_trails"><a>Audit Trails</a></li>
                        <?php }?>

                        <li content="reports"><a>Reports</a></li>
                        <li><a>Order Management</a></li>
                        <li><a>Coordinator Management</a></li>
                    

                    <?php } ?>
                </ul>
            </nav>
    </div>

    <?php if($this->session->userdata('user_level') == '6') { ?>
        <script>
         /*$(document).ready(function() {
            setTimeout(function() {
                $('ul#main_nav_headers').find('li[content="store_management_list"]').trigger('click');
            }, 5000)
         })*/
        </script>
    <?php }?>


    <!--SUB NAVIGATIONS-->

    <!--sub navigation for coordinator management tabs-->
    <div class="row sub-nav" content="agent_list" contents="archive_agent_list">
        <nav>
            <ul class="sub_nav">
                <li content="agent_list" contents="Agent List" class="sub-nav-coordinator-order active"><a href="#">Users List</a></li>
                <li contents="Archived Agent List" class="sub-nav-coordinator-order"><a href="#">Archived Users List</a></li>
            </ul>
        </nav>
    </div>

    <div class="row sub-nav hidden" content="store_management_list" contents="archive_store_list">
        <nav>
            <ul class="sub_nav">
                <li content="store_management_list" class="sub-nav-coordinator-order active"><a href="#">Store List</a></li>
                <li contents="Online Stores" class="sub-nav-coordinator-order"><a href="#">Online Stores</a></li>
                <li contents="Offline Stores" class="sub-nav-coordinator-order"><a href="#">Offline Stores</a></li>
                <li contents="Archive Store List" class="sub-nav-coordinator-order"><a href="#">Archived Stores List</a></li>
            </ul>
        </nav>
    </div>

    <div class="row sub-nav hidden" content="reports" contents="prod_unavailability_reports" contentss="contact" contentsss="repeat" content-1="dst" content-2="dst_spec" content-3="sales" content-4="sales_prod">
        <nav>
            <!--<ul class="sub_nav">
                <li content="dst_per_day" class = 'sub-nav-coordinator-order'><a>DST Report per Day</a></li>
                <li content="dst_for_store" class = 'sub-nav-coordinator-order'><a>DST Report for Store</a></li>
                <li content="sales" class = 'sub-nav-coordinator-order'> <a>Sales Summary Reports</a></li>
                <li content="sales_prod" class = 'sub-nav-coordinator-order'> <a>Item Sales Report</a></li>
                <li content="reroute_report" class = 'sub-nav-coordinator-order'> <a>Reroute Report</a></li>
                <li content="hourly_report" class="sub-nav-coordinator-order active"><a href="#">Hourly Sales Report</a></li>
                <li content="man_relayed_report" class="sub-nav-coordinator-order"><a href="#">Manually Relayed Report</a></li>
                <li content="category_report" class="sub-nav-coordinator-order"><a href="#">Category Sales Report</a></li>
                <li content="prod_unavailability_reports" class="sub-nav-coordinator-order"><a href="#">Product Availability Reports</a></li>
            </ul>-->
            <ul class="report-tabs sub_nav ">
                <li content="dst_per_day" class="tabs sub-nav-coordinator-order active">
                    <p><a href="#">DST Reports per day</a></p>
                </li>
                <li content="dst_for_store" class="tabs sub-nav-coordinator-order">
                    <p><a href="#">DST Reports for stores</a></p>
                </li>
                <li content="sales" class="tabs sub-nav-coordinator-order">
                    <p><a href="#">Sales Summary Report</a></p>
                </li>
                <li content="sales_prod" class="tabs sub-nav-coordinator-order">
                    <p><a href="#">Item Sales Report</a></p>
                </li>
                <li content="reports" class="tabs sub-nav-coordinator-order">
                    <p><a href="#">Reroute Report</a></p>
                </li>
                <li content="hourly_report" class="tabs sub-nav-coordinator-order">
                    <p><a href="#">Hourly Sales Report</a></p>
                </li>
                <li content="reports" class="tabs sub-nav-coordinator-order">
                    <p><a href="#">Manually Relayed Report</a></p>
                </li>
                <li content="category_report" class="tabs sub-nav-coordinator-order">
                    <p><a href="#">Category Sales report</a></p>
                </li>
                <li content="reports" class="tabs sub-nav-coordinator-order">
                    <p><a href="#">NKAG Report</a></p>
                </li>
                <li content="prod_unavailability_reports" class="tabs sub-nav-coordinator-order">
                    <p><a href="#">Product Availabilty Report</a></p>
                </li>
                <li content="reports" class="tabs sub-nav-coordinator-order">
                    <p><a href="#">Follow Up Orders</a></p>
                </li>
                <li content="contact" class="tabs sub-nav-coordinator-order">
                    <p><a href="#">Customer Contact Number Reports</a></p>
                </li>
                <li content="repeat" class="tabs sub-nav-coordinator-order">
                    <p><a href="#">Repeating Customer Reports</a></p>
                </li>
                <li content="reports" class="tabs sub-nav-coordinator-order">
                    <p><a href="#">Rejected Report</a></p>
                </li>
            </ul>
        </nav>
    </div>
    <!--END SUB NAVIGATIONS-->


</header>