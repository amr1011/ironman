<!DOCTYPE html>
<!-- <html lang="en" manifest = 'callcenter.manifest'> -->
<html lang="en">

<head>
    <title>{title}</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <base href="<?php // echo assets_url();?>images/logo/" target="_top"></base> -->
    <script type="text/javascript">
        var sBaseUrl = '<?php echo base_url();?>';
        var iUserId = '<?php echo $_SESSION['crm_user']['id'];?>';
        var iProvinceId = '<?php echo $_SESSION['crm_user']['province_id'];?>';
        var iSBUId = '<?php echo $_SESSION['crm_user']['sbu'];?>';
    </script>
    {meta}
    <meta name="{name}" content="{content}" />
    {/meta}

    {style}
    <link rel="stylesheet" type="text/css" href="{href}" media="{media}">
    {/style}

    {javascript}
    <script src="{src}" type="text/javascript"></script>
    {/javascript}
</head>
<body>


    {header}


<!--	<div id="loader">-->
<!--		<div class="content">-->
<!--			<i class="fa fa-spinner fa-spin fa-lg"></i> Loading...-->
<!--		</div>-->
<!--	</div>-->

<div class="clear"></div>

<section class="first-child-section hidden">
    {contents}
    {row_content}
    {/contents}
</section>

<footer>
    {footer}
</footer>

{page_javascript}
<script src="{src}" type="text/javascript"></script>
{/page_javascript}
</body>
</html>