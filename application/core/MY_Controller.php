<?php if ( !defined( 'BASEPATH' ) )
{
    exit( 'No direct script access allowed' );
}


//
class MY_Controller extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        Datamapper::add_model_path( array(
                APPPATH . 'modules/users',
                APPPATH . 'modules/clients',
                APPPATH . 'modules/orders',
                APPPATH . 'modules/products',
                APPPATH . 'modules/settings',
                APPPATH . 'modules/stores',
                APPPATH . 'modules/sales',
                APPPATH . 'modules/tickets'
            )
        );
    }

    public function get_navigation()
    {

    }
}


class Authenticated_Controller extends MY_Controller
{
    /*protected $user;
    var $nav;*/

    public function __construct()
    {
        parent::__construct();

        /*user authentication can be put here*/

        $this->load->library( 'template' );

        if ( !isset( $_SESSION['crm_user']['sbu'] ) )
        {
            $_SESSION['crm_user']['sbu'] = 1;
        }

        if ( !isset( $_SESSION['crm_user']['id'] ) )
        {
            $_SESSION['crm_user']['id'] = 1;
        }

        if ( !isset( $_SESSION['crm_user']['province_id'] ) )
        {
            $_SESSION['crm_user']['province_id'] = 1;
        }

        if ( !isset( $_SESSION['crm_user']['role_id'] ) )
        {
            //1 = admin
            //2 = coordinator
            //3 = admin
            //4 = store

            $_SESSION['crm_user']['role_id'] = 3;
        }

        if ( !isset( $_SESSION['crm_user']['role'] ) )
        {
            //1 = super admin
            //2 = coordinator
            //3 = admin
            //4 = store

            $_SESSION['crm_user']['role'] = 1;
        }

        $this->template->set_title( 'Ironman - Admin' );

        $this->template->add_style( assets_url() . '/css/font-awesome/font-awesome.min.css' );
        $this->template->add_style( assets_url() . '/css/bootstrap/bootstrap.min.css' );
        $this->template->add_style( assets_url() . '/css/bootstrap/bootstrap-theme.min.css' );

        if ( defined( "ENVIRONMENT" ) AND ENVIRONMENT != 'development' )
        {
            $this->template->add_style( 'http://fonts.googleapis.com/css?family=Lato:400,700,900' );
            $this->template->add_style( 'http://fonts.googleapis.com/css?family=Bowlby+One' );
            $this->template->add_script( assets_url() . '/js/jQueries/jquery-1.11.1.js' );
        }
        else
        {
            $this->template->add_style( assets_url() . '/css/font-awesome/font-awesome.css' );
            $this->template->add_script( assets_url() . '/js/jQueries/jquery-1.11.1.js' );
            $this->template->add_script( assets_url() . '/js/jQueries/jquery-te-1.4.0.min.js' );
        }

        $this->template->add_script( assets_url() . '/js/extras/php.full.min.js' );
        $this->template->add_script( assets_url() . '/js/bootstrap/bootstrap.min.js' );
        $this->template->add_script( assets_url() . '/js/jQueries/jquery.cookie.js' );
        $this->template->add_script( assets_url() . '/js/jQueries/jquery.tinysort.min.js' );

        $this->template->add_script( assets_url() . '/js/plugins/integr8formvalidation.js', true );
        $this->template->add_script( assets_url() . '/js/core/platform.js' );
        $this->template->add_script( assets_url() . '/js/core/config.js' );
        $this->template->add_script( assets_url() . '/js/core/memory_storage_engine.js' );
        $this->template->add_script( assets_url() . '/js/core/ajax.js' );
        $this->template->add_script( assets_url() . '/js/core/ui.js' );
        $this->template->add_script( assets_url() . '/js/core/memory_storage_engine.js' );
        $this->template->add_script( assets_url() . '/js/core/errors.js' );
        $this->template->add_script( assets_url() . '/js/plugins/ajaxq.js' );
        $this->template->add_script( assets_url() . '/js/plugins/offline.min.js' );
        $this->template->add_script( assets_url() . '/js/plugins/pusher.js' );
        $this->template->add_script( assets_url() . '/js/plugins/CconnectionDetector.js' );
        $this->template->add_script( assets_url() . '/js/plugins/jquery.dataTables.min.js' );
        $this->template->add_script( assets_url() . '/js/plugins/jquery.filtertable.min.js' );
        $this->template->add_script( assets_url() . '/js/libraries/websocket.js' );
        $this->template->add_script( assets_url() . '/js/extras/websocket_events.js' );

        $this->template->set_header( 'template/header' );
        $this->template->set_footer( 'template/footer' );
        $this->template->set_template( 'template/index' );
    }
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */